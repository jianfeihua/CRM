import { IndexedDBServiceBase } from './indexeddb-service-base';

/**
 * IndexedDB数据库服务
 *
 * @export
 * @class IndexedDBService
 * @extends {IndexedDBServiceBase}
 */
export class IndexedDBService extends IndexedDBServiceBase {

    /**
     * 唯一实例
     *
     * @private
     * @static
     * @type {IndexedDBService}
     * @memberof IndexedDBService
     */
    private static readonly instance: IndexedDBService = new IndexedDBService();

    /**
     * Creates an instance of IndexedDBService.
     * @memberof IndexedDBService
     */
    protected constructor() {
        if (IndexedDBService.instance) {
            return IndexedDBService.instance;
        }
        super('2020-5-20');
    }

    /**
     * 实体配置初始化
     *
     * @protected
     * @memberof IndexedDBServiceBase
     */
    protected entityConfigInit(): void {
        this.entityConfigs.push({ name: 'dynachart', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_lost_reason', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'product_pricelist', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'product_price_list', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_lead2opportunity_partner', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'mail_activity', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_lead2opportunity_partner_mass', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'res_users', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_stage', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'mail_message', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'res_partner', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'mail_activity_type', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_partner_binding', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_merge_opportunity', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'calendar_event', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'account_invoice_line', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'account_invoice', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_lead_lost', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'uom_uom', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_team', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'account_journal', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'account_register_payments', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'dynadashboard', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'product_category', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_lead_tag', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_lead', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'crm_activity_report', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'sale_order_line', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'product_pricelist_item', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'res_company', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'product_product', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'sale_order', keyPath: 'srfsessionkey' });
        this.entityConfigs.push({ name: 'account_payment', keyPath: 'srfsessionkey' });
    }

    /**
     * 获取实例
     *
     * @returns {IndexedDBService}
     * @memberof IndexedDBService
     */
    public static getInstance(): IndexedDBService {
        return IndexedDBService.instance;
    }

}