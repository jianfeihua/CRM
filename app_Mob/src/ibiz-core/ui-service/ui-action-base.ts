import { ViewOpenService } from '../utils/service/view-open-service';

/**
 * 界面行为服务基类
 *
 * @export
 * @class UIActionBase
 */
export class UIActionBase {

    /**
     * 视图打开服务
     *
     * @protected
     * @type {ViewOpenService}
     * @memberof UIActionBase
     */
    protected openService: ViewOpenService = ViewOpenService.getInstance();

    /**
     * 处理应用上下文参数
     * 
     * @protected
     * @param actionTarget 数据目标
     * @param args  传入数据对象
     * @param param 传入应用上下数据参数
     */
    protected handleContextParam(actionTarget: any, args: any, context: any) {
        return this.formatData(actionTarget, args, context);
    }

    /**
     * 处理界面行为参数
     * 
     * @protected
     * @param actionTarget 数据目标
     * @param args  传入数据对象
     * @param param 传入界面行为附加参数
     */
    protected handleActionParam(actionTarget: any, args: any, params: any) {
        return this.formatData(actionTarget, args, params);
    }

    /**
     * 格式化数据
     *
     * @protected
     * @param {*} actionTarget
     * @param {*} args
     * @param {*} params
     * @returns {*}
     * @memberof UIActionTool
     */
    protected formatData(actionTarget: any, args: any, params: any): any {
        const data: any = {};
        if (Object.is(actionTarget, 'SINGLEKEY') && args && args.length > 0) {
            const arg: any = args[0];
            Object.keys(params).forEach((name: string) => {
                if (!name) {
                    return;
                }
                let value: string | null = params[name];
                if (value && value.startsWith('%') && value.endsWith('%')) {
                    const key = value.substring(1, value.length - 1);
                    if (arg && arg.hasOwnProperty(key)) {
                        value = (arg[key] !== null && arg[key] !== undefined) ? arg[key] : null;
                    } else {
                        value = null;
                    }
                }
                Object.assign(data, { [name]: value });
            });
        } else if (Object.is(actionTarget, 'MULTIKEY')) {
            Object.keys(params).forEach((name: string) => {
                if (!name) {
                    return;
                }
                let value: string | null = params[name];
                let values: any[] = [];
                if (value && value.startsWith('%') && value.endsWith('%')) {
                    const key = value.substring(1, value.length - 1);
                    args.forEach((arg: any) => {
                        if (arg && arg.hasOwnProperty(key)) {
                            value = (arg[key] !== null && arg[key] !== undefined) ? arg[key] : null;
                        } else {
                            value = null;
                        }
                        values.push(value);
                    });
                }
                Object.assign(data, { [name]: values.length > 0 ? values.join(';') : value });
            });
        }
        return data;
    }
}