import { Util } from '@/ibiz-core/utils';
import UIActionBase from '@/utils/ui-service-base/ui-action-base';

/**
 * 全局界面行为基类
 *
 * @export
 * @class GlobalUiServiceBase
 * @extends {UIActionBase}
 */
export default class GlobalUiServiceBase extends UIActionBase {

    /**
     * Creates an instance of UiServiceBase.
     * 
     * @memberof UiServiceBase
     */
    constructor() {
        super();
    }


    /**
     * 删除
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof 
     */
    public async Remove(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        const _this: any = this;
        if (!xData || !(xData.remove instanceof Function)) {
            return ;
        }
        xData.remove(args);
    }


    /**
     * 保存并关闭
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof 
     */
    public async SaveAndExit(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        // _this 指向容器对象
        // xData 数据对象
        const _this: any = this;
        if (xData && xData.saveAndExit instanceof Function) {
            const _data = {};
            xData.saveAndExit(_data).then((response: any) => {
                if (!response || response.status !== 200) {
                    container.$emit('viewdataschange', JSON.stringify({status:'error',action:'saveAndExit'}));
                    return;
                }
                container.$emit('viewdataschange', JSON.stringify({status:'success',action:'saveAndExit',data:response.data}));
            });
        } else if (container.saveAndExit && container.saveAndExit instanceof Function) {
            container.saveAndExit().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                container.$emit('closeview', [{ ...response.data }]);
            });
        }
    }

    /**
     * 拷贝
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof 
     */
    public async Copy(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        if (args.length === 0) {
            return;
        }
        const _this: any = this;
        if (container.newdata && container.newdata instanceof Function) {
            const data: any = {};
            if (args.length > 0) {
                Object.assign(data, { srfsourcekey: args[0].srfkey })
            }
            container.newdata(args, contextJO, paramJO, $event, xData, container, srfParentDeName);
        } else if (xData && xData.copy instanceof Function) {
            const data2: any = {};
            if (args.length > 0) {
                Object.assign(data2, { srfsourcekey: args[0].srfkey })
            }
            xData.copy(data2);
        } else {
            this.notice.error('opendata 视图处理逻辑不存在，请添加!');
        }
    }

    /**
     * 编辑
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof 
     */
    public async Edit(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        if (args.length === 0) {
            return;
        }
        const _this: any = this;
        if (container.opendata && container.opendata instanceof Function) {
            const data: any = { };
            container.opendata(args, contextJO, paramJO, $event, xData, container, srfParentDeName);
        } else {
            this.notice.error('opendata 视图处理逻辑不存在，请添加!');
        }
    }

    /**
     * 关闭
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof 
     */
    public async Exit(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
        container.closeView(args);
    }


    /**
     * 新建
     *
     * @param {any[]} args 数据
     * @param {*} [contextJO={}] 行为上下文
     * @param {*} [paramJO={}] 行为参数
     * @param {*} [$event] 事件
     * @param {*} [xData] 数据目标
     * @param {*} [container] 行为容器对象
     * @param {string} [srfParentDeName]
     * @returns {Promise<any>}
     * @memberof 
     */
    public async New(args: any[], contextJO: any = {}, paramJO: any = {}, $event?: any, xData?: any, container?: any, srfParentDeName?: string): Promise<any> {
         const _this: any = this;
        if (container.newdata && container.newdata instanceof Function) {
            const data: any = {};
            container.newdata(args, contextJO, paramJO, $event, xData, container, srfParentDeName);
        } else {
            this.notice.error('newdata 视图处理逻辑不存在，请添加!');
        }
    }
}