import Vue, { VNode, CreateElement } from "vue";
import { UIActionTool } from "@/ibiz-core/utils";

declare module "vue/types/vue" {
    interface Vue {
        /**
         * 代码表绘制对象
         *
         * @type {CodeList}
         * @memberof Vue
         */
        $uiActionTool: UIActionTool;
    }
}
