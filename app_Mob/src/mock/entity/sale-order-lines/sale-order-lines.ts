import { MockAdapter } from '@/mock/mock-adapter';
import { Util } from '@/ibiz-core/utils';
const mock = MockAdapter.getInstance();

// 模拟数据
const mockDatas: Array<any> = [
];

/**
 * 解析参数
 * 
 * @param paramArray 参数数组
 * @param regStr 路径正则
 * @param url 访问路径
 */
const parsingParameters = (paramArray: Array<string>, regStr: RegExp, url: string): any => {
    let params: any = {};
    const matchArray: any = new RegExp(regStr).exec(url);
    if (matchArray && matchArray.length > 1 && paramArray && paramArray.length > 0) {
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(params, item, {
                enumerable: true,
                value: matchArray[index + 1],
            });
        });
    }
    return params;
};

/**
 * 排队路径参数是否存在 null、undefined 或者空字符串等异常情况 
 * 
 * @param paramArray 
 * @param params 
 */
const judgmentParametersException = (paramArray: Array<string>, params: any): boolean => {
    let isParametersException = false;
    if (paramArray.length > 0) {
        let state = paramArray.some((key: string) => {
            let _value = params[key];
            if (!_value || Object.is(_value, '')) {
                return true;
            }
            return false;
        });
        if (state) {
            isParametersException = true;;
        }
    }
    return isParametersException;
};












































































