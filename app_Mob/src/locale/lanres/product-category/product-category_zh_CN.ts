export default {
	views: {
		mobeditview: {
			caption: '产品种类',
		},
		mobmdview: {
			caption: '产品种类',
		},
	},
	mobmain_form: {
		details: {
			group1: '产品种类基本信息', 
			group2: '操作信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '名称', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: '新建',
			tip: '新建',
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: '保存',
			tip: '保存',
		},
	},
};