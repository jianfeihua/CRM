/**
 * QuickMenu 部件模型
 *
 * @export
 * @class QuickMenuModel
 */
export class QuickMenuModel {

    /**
     * 菜单项集合
     *
     * @private
     * @type {any[]}
     * @memberof QuickMenuModel
     */
    private items: any[] = [
        {
	id: '12f340b635481cc9c1083a83d9829b22',
	name: 'menuitem1',
	text: '客户信息',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '客户信息',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'iconfont icon-gongsi',
	icon: '',
	textcls: '',
	appfunctag: 'Auto11',
	resourcetag: '',
},
        {
	id: '283ac77f105a56c67c040190248e45a5',
	name: 'menuitem2',
	text: '商机',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '商机',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'iconfont icon-tongjifenxi',
	icon: '',
	textcls: '',
	appfunctag: 'Auto16',
	resourcetag: '',
},
        {
	id: '408c7f990a8e5e369057be331ad097f8',
	name: 'menuitem4',
	text: '账单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '账单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'iconfont icon-fankui',
	icon: '',
	textcls: '',
	appfunctag: 'Auto29',
	resourcetag: '',
},
        {
	id: '68a35e22fc1043eca53ecae747014fd5',
	name: 'menuitem3',
	text: '开票单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '开票单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'iconfont icon-qingjiabiangeng',
	icon: '',
	textcls: '',
	appfunctag: 'Auto40',
	resourcetag: '',
},
        {
	id: '441724369b3822c1a441d18e753e4072',
	name: 'menuitem7',
	text: '合同',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '合同',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'iconfont icon-paihangbang',
	icon: '',
	textcls: '',
	appfunctag: 'Auto42',
	resourcetag: '',
},
        {
	id: 'af1526ba60a066d74fb9cb51ee876e66',
	name: 'menuitem5',
	text: '付款登记',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '付款登记',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'iconfont icon-richeng',
	icon: '',
	textcls: '',
	appfunctag: 'Auto28',
	resourcetag: '',
},
        {
	id: 'c784342f0164f9416587c4e18a24d093',
	name: 'menuitem6',
	text: '产品种类',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '产品种类',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'iconfont icon-xitongshezhi',
	icon: '',
	textcls: '',
	appfunctag: 'Auto25',
	resourcetag: '',
},
    ];

	/**
	 * 应用功能集合
	 *
	 * @private
	 * @type {any[]}
	 * @memberof QuickMenuModel
	 */
	private funcs: any[] = [
	];

    /**
     * 获取所有菜单项集合
     *
     * @returns {any[]}
     * @memberof QuickMenuModel
     */
    public getAppMenuItems(): any[] {
        return this.items;
    }

    /**
     * 获取所有应用功能集合
     *
     * @returns {any[]}
     * @memberof QuickMenuModel
     */
    public getAppFuncs(): any[] {
        return this.funcs;
    }
}
// 默认导出
export default QuickMenuModel;