/**
 * MobFunnel 部件模型
 *
 * @export
 * @class MobFunnelModel
 */
export class MobFunnelModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobFunnelDb_sysportlet1_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name:'query',
				prop:'query'
			},
		]
	}

}
// 默认导出
export default MobFunnelModel;