/**
 * MobMain 部件模型
 *
 * @export
 * @class MobMainModel
 */
export class MobMainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MobMainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'stage_id_text',
        prop: 'stage_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'planned_revenue',
        prop: 'planned_revenue',
        dataType: 'DECIMAL',
      },
      {
        name: 'probability',
        prop: 'probability',
        dataType: 'FLOAT',
      },
      {
        name: 'partner_name',
        prop: 'partner_name',
        dataType: 'TEXT',
      },
      {
        name: 'date_deadline',
        prop: 'date_deadline',
        dataType: 'DATE',
      },
      {
        name: 'priority',
        prop: 'priority',
        dataType: 'SSCODELIST',
      },
      {
        name: 'campaign_id_text',
        prop: 'campaign_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'medium_id_text',
        prop: 'medium_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'source_id_text',
        prop: 'source_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'crm_lead',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
// 默认导出
export default MobMainModel;