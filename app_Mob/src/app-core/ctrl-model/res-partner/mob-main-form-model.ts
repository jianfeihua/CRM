/**
 * MobMain 部件模型
 *
 * @export
 * @class MobMainModel
 */
export class MobMainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MobMainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'country_id_text',
        prop: 'country_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'state_id_text',
        prop: 'state_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'city',
        prop: 'city',
        dataType: 'TEXT',
      },
      {
        name: 'zip',
        prop: 'zip',
        dataType: 'TEXT',
      },
      {
        name: 'street',
        prop: 'street',
        dataType: 'TEXT',
      },
      {
        name: 'street2',
        prop: 'street2',
        dataType: 'TEXT',
      },
      {
        name: 'phone',
        prop: 'phone',
        dataType: 'TEXT',
      },
      {
        name: 'mobile',
        prop: 'mobile',
        dataType: 'TEXT',
      },
      {
        name: 'email',
        prop: 'email',
        dataType: 'TEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'res_partner',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
// 默认导出
export default MobMainModel;