/**
 * Mob 部件模型
 *
 * @export
 * @class MobModel
 */
export class MobModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobMdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'LONGTEXT',
			},
			{
				name: 'salesman_id',
				prop: 'salesman_id',
				dataType: 'PICKUP',
			},
			{
				name: 'currency_id',
				prop: 'currency_id',
				dataType: 'PICKUP',
			},
			{
				name: 'order_id',
				prop: 'order_id',
				dataType: 'PICKUP',
			},
			{
				name: 'event_id',
				prop: 'event_id',
				dataType: 'PICKUP',
			},
			{
				name: 'linked_line_id',
				prop: 'linked_line_id',
				dataType: 'PICKUP',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'route_id',
				prop: 'route_id',
				dataType: 'PICKUP',
			},
			{
				name: 'order_partner_id',
				prop: 'order_partner_id',
				dataType: 'PICKUP',
			},
			{
				name: 'product_uom',
				prop: 'product_uom',
				dataType: 'PICKUP',
			},
			{
				name: 'product_id',
				prop: 'product_id',
				dataType: 'PICKUP',
			},
			{
				name: 'company_id',
				prop: 'company_id',
				dataType: 'PICKUP',
			},
			{
				name: 'product_packaging',
				prop: 'product_packaging',
				dataType: 'PICKUP',
			},
			{
				name: 'event_ticket_id',
				prop: 'event_ticket_id',
				dataType: 'PICKUP',
			},
			{
				name: 'sale_order',
				prop: 'id',
                dataType: 'FONTKEY',
			},
			{
				name: 'sale_order_line',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default MobModel;