/**
 * MobMain 部件模型
 *
 * @export
 * @class MobMainModel
 */
export class MobMainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MobMainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'LONGTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'product_id_text',
        prop: 'product_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price_unit',
        prop: 'price_unit',
        dataType: 'FLOAT',
      },
      {
        name: 'product_uom_qty',
        prop: 'product_uom_qty',
        dataType: 'FLOAT',
      },
      {
        name: 'product_uom',
        prop: 'product_uom',
        dataType: 'PICKUP',
      },
      {
        name: 'product_uom_text',
        prop: 'product_uom_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price_total',
        prop: 'price_total',
        dataType: 'DECIMAL',
      },
      {
        name: 'customer_lead',
        prop: 'customer_lead',
        dataType: 'FLOAT',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'LONGTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'product_id',
        prop: 'product_id',
        dataType: 'PICKUP',
      },
      {
        name: 'sale_order',
        prop: 'id',
        dataType: 'FONTKEY',
      },
      {
        name: 'sale_order_line',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
// 默认导出
export default MobMainModel;