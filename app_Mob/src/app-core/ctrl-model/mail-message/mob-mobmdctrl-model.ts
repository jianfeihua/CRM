/**
 * Mob 部件模型
 *
 * @export
 * @class MobModel
 */
export class MobModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobMdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'mail_activity_type_id',
				prop: 'mail_activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'moderator_id',
				prop: 'moderator_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'author_id',
				prop: 'author_id',
				dataType: 'PICKUP',
			},
			{
				name: 'parent_id',
				prop: 'parent_id',
				dataType: 'PICKUP',
			},
			{
				name: 'subtype_id',
				prop: 'subtype_id',
				dataType: 'PICKUP',
			},
			{
				name: 'mail_message',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default MobModel;