import { CounterServiceBase } from '@/ibiz-core';

 /**
 * 门户界面计数器计数器服务对象基类
 *
 * @export
 * @class PortalCounterServiceBase
 * @extends {CounterServiceBase}
 */
export class PortalCounterServiceBase extends CounterServiceBase { }