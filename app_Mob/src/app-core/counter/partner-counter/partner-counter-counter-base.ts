import { CounterServiceBase } from '@/ibiz-core';

 /**
 * 客户统计摘要计数器计数器服务对象基类
 *
 * @export
 * @class PartnerCounterCounterServiceBase
 * @extends {CounterServiceBase}
 */
export class PartnerCounterCounterServiceBase extends CounterServiceBase { }