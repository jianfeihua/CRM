import { DynaDashboardServiceBase } from './dyna-dashboard-service-base';

/**
 * 动态数据看板服务对象
 *
 * @export
 * @class DynaDashboardService
 * @extends {DynaDashboardServiceBase}
 */
export class DynaDashboardService extends DynaDashboardServiceBase { }
// 默认导出
export default DynaDashboardService;