import { Mail_messageServiceBase } from './mail-message-service-base';

/**
 * 消息服务对象
 *
 * @export
 * @class Mail_messageService
 * @extends {Mail_messageServiceBase}
 */
export class Mail_messageService extends Mail_messageServiceBase { }
// 默认导出
export default Mail_messageService;