import { Crm_leadServiceBase } from './crm-lead-service-base';

/**
 * 线索/商机服务对象
 *
 * @export
 * @class Crm_leadService
 * @extends {Crm_leadServiceBase}
 */
export class Crm_leadService extends Crm_leadServiceBase { }
// 默认导出
export default Crm_leadService;