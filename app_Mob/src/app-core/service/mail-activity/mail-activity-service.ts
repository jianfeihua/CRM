import { Mail_activityServiceBase } from './mail-activity-service-base';

/**
 * 活动服务对象
 *
 * @export
 * @class Mail_activityService
 * @extends {Mail_activityServiceBase}
 */
export class Mail_activityService extends Mail_activityServiceBase { }
// 默认导出
export default Mail_activityService;