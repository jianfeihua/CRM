import { Crm_lost_reasonServiceBase } from './crm-lost-reason-service-base';

/**
 * 丢单原因服务对象
 *
 * @export
 * @class Crm_lost_reasonService
 * @extends {Crm_lost_reasonServiceBase}
 */
export class Crm_lost_reasonService extends Crm_lost_reasonServiceBase { }
// 默认导出
export default Crm_lost_reasonService;