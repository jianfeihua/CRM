import { Sale_order_lineServiceBase } from './sale-order-line-service-base';

/**
 * 销售订单行服务对象
 *
 * @export
 * @class Sale_order_lineService
 * @extends {Sale_order_lineServiceBase}
 */
export class Sale_order_lineService extends Sale_order_lineServiceBase { }
// 默认导出
export default Sale_order_lineService;