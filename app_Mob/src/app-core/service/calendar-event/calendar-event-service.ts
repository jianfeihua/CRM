import { Calendar_eventServiceBase } from './calendar-event-service-base';

/**
 * 活动服务对象
 *
 * @export
 * @class Calendar_eventService
 * @extends {Calendar_eventServiceBase}
 */
export class Calendar_eventService extends Calendar_eventServiceBase { }
// 默认导出
export default Calendar_eventService;