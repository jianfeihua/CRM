import { Res_partnerServiceBase } from './res-partner-service-base';

/**
 * 客户服务对象
 *
 * @export
 * @class Res_partnerService
 * @extends {Res_partnerServiceBase}
 */
export class Res_partnerService extends Res_partnerServiceBase { }
// 默认导出
export default Res_partnerService;