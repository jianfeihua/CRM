import { Account_register_paymentsServiceBase } from './account-register-payments-service-base';

/**
 * 登记付款服务对象
 *
 * @export
 * @class Account_register_paymentsService
 * @extends {Account_register_paymentsServiceBase}
 */
export class Account_register_paymentsService extends Account_register_paymentsServiceBase { }
// 默认导出
export default Account_register_paymentsService;