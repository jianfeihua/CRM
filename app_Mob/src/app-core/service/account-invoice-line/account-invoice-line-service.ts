import { Account_invoice_lineServiceBase } from './account-invoice-line-service-base';

/**
 * 发票行服务对象
 *
 * @export
 * @class Account_invoice_lineService
 * @extends {Account_invoice_lineServiceBase}
 */
export class Account_invoice_lineService extends Account_invoice_lineServiceBase { }
// 默认导出
export default Account_invoice_lineService;