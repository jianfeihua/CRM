import { Product_productServiceBase } from './product-product-service-base';

/**
 * 产品服务对象
 *
 * @export
 * @class Product_productService
 * @extends {Product_productServiceBase}
 */
export class Product_productService extends Product_productServiceBase { }
// 默认导出
export default Product_productService;