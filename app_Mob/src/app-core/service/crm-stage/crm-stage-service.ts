import { Crm_stageServiceBase } from './crm-stage-service-base';

/**
 * CRM 阶段服务对象
 *
 * @export
 * @class Crm_stageService
 * @extends {Crm_stageServiceBase}
 */
export class Crm_stageService extends Crm_stageServiceBase { }
// 默认导出
export default Crm_stageService;