import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 销售订单服务对象基类
 *
 * @export
 * @class Sale_orderServiceBase
 * @extends {EntityServiceBase}
 */
export class Sale_orderServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Sale_orderServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Sale_orderServiceBase
     */
    protected readonly dePath: string = 'sale_orders';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Sale_orderServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Sale_orderServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Sale_orderServiceBase
     */
    protected allMinorAppEntity: any = {
        'sale_order_lines': {
            name: 'sale_order_line'
        },
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('sale_order');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/select`, data);
            }
            return await this.http.post(`/sale_orders/${context.sale_order}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.put(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/updatebatch`, data);
            }
            return await this.http.put(`/sale_orders/${context.sale_order}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.delete(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/removebatch`);
            }
            return await this.http.delete(`/sale_orders/${context.sale_order}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.put(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/sale_orders/${context.sale_order}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.get(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}`);
            }
            const res: any = await this.http.get(`/sale_orders/${context.sale_order}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async Save(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/save`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.post(`/sale_orders/${context.sale_order}/save`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async CheckKey(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/checkkey`, data);
            }
            return await this.http.post(`/sale_orders/${context.sale_order}/checkkey`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.delete(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}`);
            }
            return await this.http.delete(`/sale_orders/${context.sale_order}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/sale_orders/getdraft`);
            }
            const res: any = await this.http.get(`/sale_orders/getdraft`);
            res.data.sale_order = context.sale_order;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.sale_order) {
                return await this.http.post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/createbatch`, data);
            }
            return await this.http.post(`/sale_orders/${context.sale_order}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.post(`/res_partners/${context.res_partner}/sale_orders`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            data.sale_order = null;
            const res: any = await this.http.post(`/sale_orders`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_orderServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/sale_orders/fetchdefault`, data);
            }
            return await this.http.get(`/sale_orders/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}