import { Product_pricelistServiceBase } from './product-pricelist-service-base';

/**
 * 价格表服务对象
 *
 * @export
 * @class Product_pricelistService
 * @extends {Product_pricelistServiceBase}
 */
export class Product_pricelistService extends Product_pricelistServiceBase { }
// 默认导出
export default Product_pricelistService;