import { Account_paymentServiceBase } from './account-payment-service-base';

/**
 * 付款服务对象
 *
 * @export
 * @class Account_paymentService
 * @extends {Account_paymentServiceBase}
 */
export class Account_paymentService extends Account_paymentServiceBase { }
// 默认导出
export default Account_paymentService;