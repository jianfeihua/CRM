import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobMainInfoPanelModel } from '@/app-core/ctrl-model/crm-lead/mob-main-info-panel-portlet-model';


/**
 * MobMainInfoPanel 部件服务对象
 *
 * @export
 * @class MobMainInfoPanelService
 * @extends {PortletServiceBase}
 */
export class MobMainInfoPanelService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobMainInfoPanelModel}
     * @memberof ControlServiceBase
     */
    protected model: MobMainInfoPanelModel = new MobMainInfoPanelModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobMainInfoPanelService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobMainInfoPanelService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobMainInfoPanelService;