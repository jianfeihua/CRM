import { ListServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MainInfoModel } from '@/app-core/ctrl-model/crm-lead/main-info-panel-model';


/**
 * MainInfo 部件服务对象
 *
 * @export
 * @class MainInfoService
 * @extends {ListServiceBase}
 */
export class MainInfoService extends ListServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MainInfoModel}
     * @memberof ControlServiceBase
     */
    protected model: MainInfoModel = new MainInfoModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainInfoService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MainInfoService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MainInfoService;