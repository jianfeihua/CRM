import { ListServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { ListLayoutModel } from '@/app-core/ctrl-model/crm-lead/list-layout-panel-model';


/**
 * ListLayout 部件服务对象
 *
 * @export
 * @class ListLayoutService
 * @extends {ListServiceBase}
 */
export class ListLayoutService extends ListServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {ListLayoutModel}
     * @memberof ControlServiceBase
     */
    protected model: ListLayoutModel = new ListLayoutModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListLayoutService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof ListLayoutService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default ListLayoutService;