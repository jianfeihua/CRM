import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { ModMainView9Model } from '@/app-core/ctrl-model/res-partner/mod-main-view9-portlet-model';


/**
 * ModMainView9 部件服务对象
 *
 * @export
 * @class ModMainView9Service
 * @extends {PortletServiceBase}
 */
export class ModMainView9Service extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {ModMainView9Model}
     * @memberof ControlServiceBase
     */
    protected model: ModMainView9Model = new ModMainView9Model();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ModMainView9Service
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof ModMainView9Service
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default ModMainView9Service;