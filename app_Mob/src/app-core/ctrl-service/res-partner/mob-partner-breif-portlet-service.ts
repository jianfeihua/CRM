import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobPartnerBreifModel } from '@/app-core/ctrl-model/res-partner/mob-partner-breif-portlet-model';


/**
 * MobPartnerBreif 部件服务对象
 *
 * @export
 * @class MobPartnerBreifService
 * @extends {PortletServiceBase}
 */
export class MobPartnerBreifService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobPartnerBreifModel}
     * @memberof ControlServiceBase
     */
    protected model: MobPartnerBreifModel = new MobPartnerBreifModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobPartnerBreifService
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobPartnerBreifService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobPartnerBreifService;