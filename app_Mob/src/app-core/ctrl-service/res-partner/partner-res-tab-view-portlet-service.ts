import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { PartnerResTabViewModel } from '@/app-core/ctrl-model/res-partner/partner-res-tab-view-portlet-model';


/**
 * PartnerResTabView 部件服务对象
 *
 * @export
 * @class PartnerResTabViewService
 * @extends {PortletServiceBase}
 */
export class PartnerResTabViewService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {PartnerResTabViewModel}
     * @memberof ControlServiceBase
     */
    protected model: PartnerResTabViewModel = new PartnerResTabViewModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PartnerResTabViewService
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof PartnerResTabViewService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default PartnerResTabViewService;