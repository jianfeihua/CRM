import { ListServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { ListModel } from '@/app-core/ctrl-model/mail-activity/list-panel-model';


/**
 * List 部件服务对象
 *
 * @export
 * @class ListService
 * @extends {ListServiceBase}
 */
export class ListService extends ListServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {ListModel}
     * @memberof ControlServiceBase
     */
    protected model: ListModel = new ListModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListService
     */
    protected appDEName: string = 'mail_activity';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof ListService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default ListService;