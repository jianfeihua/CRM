import { CalendarServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobActivityCalendarModel } from '@/app-core/ctrl-model/mail-activity/mob-activity-calendar-calendar-model';


/**
 * MobActivityCalendar 部件服务对象
 *
 * @export
 * @class MobActivityCalendarService
 * @extends {CalendarServiceBase}
 */
export class MobActivityCalendarService extends CalendarServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobActivityCalendarModel}
     * @memberof ControlServiceBase
     */
    protected model: MobActivityCalendarModel = new MobActivityCalendarModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobActivityCalendarService
     */
    protected appDEName: string = 'mail_activity';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobActivityCalendarService
     */
    protected appDeKey: string = 'id';

    /**
     * 事件配置集合
     *
     * @protected
     * @type {any[]}
     * @memberof MobActivityCalendar
     */
    protected eventsConfig: any = {
        'activity2': {
            itemName : '已结束活动',
            itemType : 'activity2',
            color : 'red',
            textColor : '',
        },
        'activity': {
            itemName : '未进行活动',
            itemType : 'activity',
            color : 'blue',
            textColor : '',
        },
    };

    /**
     * 查询数据
     *
     * @param {string} itemType
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<HttpResponse>}
     * @memberof MobActivityCalendarService
     */
    public async search(itemType: string, context: any = {}, data: any = {}, isLoading?: boolean): Promise<HttpResponse> {
        let item: any = {};
        try {

            this.model.itemType = 'activity2';
            const _activity2_data = this.handleRequestData('', context, data);
            await this.onBeforeAction('', context, _activity2_data, isLoading);
            const _activity2 = await this.loadDEDataSet('FetchDefault', context, _activity2_data, 'activity2');
            Object.assign(item, { activity2: _activity2 });

            this.model.itemType = 'activity';
            const _activity_data = this.handleRequestData('', context, data);
            await this.onBeforeAction('', context, _activity_data, isLoading);
            const _activity = await this.loadDEDataSet('FetchDefault', context, _activity_data, 'activity');
            Object.assign(item, { activity: _activity });
        } catch (response) {
            await this.onAfterAction('', context, response);
            return new HttpResponse(response.status);
        }
        await this.onAfterAction('', context);
        return new HttpResponse(200, item);
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<any>}
     * @memberof MobActivityCalendarService
     */
    public async update(itemType: string, context: any = {}, data: any = {}, isLoading?: boolean): Promise<any> {
        await this.onBeforeAction('', context, data, isLoading);
        data = this.handleRequestData('', context, data);
        let response: any;
        switch (itemType) {
            case "Activity2":
                response = await this.service.Update(context, data);
                break;
            default:
                response = new HttpResponse(500, null, { code: 101, message: '未配置更新实体行为' });
        }
        if (!response.isError()) {
            response = this.handleResponse("", response);
        }
        await this.onAfterAction('', context, response);
        return new HttpResponse(response.status, response.data, response.error);
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<any>}
     * @memberof MobActivityCalendarService
     */
    public async delete(itemType: string, context: any = {}, data: any = {}, isLoading?: boolean): Promise<any> {
        await this.onBeforeAction('', context, data, isLoading);
        data = this.handleRequestData('', context, data);
        let response: any;
        switch (itemType) {
            case "Activity2":
                response = await this.service.Remove(context, data);
                break;
            default:
                response = new HttpResponse(500, null, { code: 101, message: '未配置删除实体行为' });
        }
        if (!response.isError()) {
            response = this.handleResponse("", response);
        }
        await this.onAfterAction('', context, response);
        return new HttpResponse(response.status, response.data, response.error);
    }

}
// 默认导出
export default MobActivityCalendarService;