import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobBriefReportModel } from '@/app-core/ctrl-model/res-users/mob-brief-report-portlet-model';


/**
 * MobBriefReport 部件服务对象
 *
 * @export
 * @class MobBriefReportService
 * @extends {PortletServiceBase}
 */
export class MobBriefReportService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobBriefReportModel}
     * @memberof ControlServiceBase
     */
    protected model: MobBriefReportModel = new MobBriefReportModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobBriefReportService
     */
    protected appDEName: string = 'res_users';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobBriefReportService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobBriefReportService;