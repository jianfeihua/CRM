import { CodeListBase } from '@/ibiz-core/code-list/code-list-base';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 代码表--CRM_LEAD_STAGE
 *
 * @export
 * @class CodeList
 * @extends {CodeListBase}
 */
export class CodeList extends CodeListBase { 
}