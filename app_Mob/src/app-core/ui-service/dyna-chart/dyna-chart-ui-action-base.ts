import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 动态图表UI服务对象基类
 *
 * @export
 * @class DynaChartUIActionBase
 * @extends {UIActionBase}
 */
export class DynaChartUIActionBase extends UIActionBase {

}