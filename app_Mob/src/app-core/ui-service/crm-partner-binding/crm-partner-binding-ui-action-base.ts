import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 在CRM向导中处理业务伙伴的绑定或生成。UI服务对象基类
 *
 * @export
 * @class Crm_partner_bindingUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_partner_bindingUIActionBase extends UIActionBase {

}