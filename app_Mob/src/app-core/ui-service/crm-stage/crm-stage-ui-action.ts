import { Crm_stageUIActionBase } from './crm-stage-ui-action-base';

/**
 * CRM 阶段UI服务对象
 *
 * @export
 * @class Crm_stageUIAction
 * @extends {Crm_stageUIActionBase}
 */
export class Crm_stageUIAction extends Crm_stageUIActionBase { }
// 默认导出
export default Crm_stageUIAction;