import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 公司UI服务对象基类
 *
 * @export
 * @class Res_companyUIActionBase
 * @extends {UIActionBase}
 */
export class Res_companyUIActionBase extends UIActionBase {

}