import { Res_companyUIActionBase } from './res-company-ui-action-base';

/**
 * 公司UI服务对象
 *
 * @export
 * @class Res_companyUIAction
 * @extends {Res_companyUIActionBase}
 */
export class Res_companyUIAction extends Res_companyUIActionBase { }
// 默认导出
export default Res_companyUIAction;