import { Account_paymentUIActionBase } from './account-payment-ui-action-base';

/**
 * 付款UI服务对象
 *
 * @export
 * @class Account_paymentUIAction
 * @extends {Account_paymentUIActionBase}
 */
export class Account_paymentUIAction extends Account_paymentUIActionBase { }
// 默认导出
export default Account_paymentUIAction;