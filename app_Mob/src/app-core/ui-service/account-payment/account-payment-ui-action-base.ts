import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 付款UI服务对象基类
 *
 * @export
 * @class Account_paymentUIActionBase
 * @extends {UIActionBase}
 */
export class Account_paymentUIActionBase extends UIActionBase {

}