import { Crm_lost_reasonUIActionBase } from './crm-lost-reason-ui-action-base';

/**
 * 丢单原因UI服务对象
 *
 * @export
 * @class Crm_lost_reasonUIAction
 * @extends {Crm_lost_reasonUIActionBase}
 */
export class Crm_lost_reasonUIAction extends Crm_lost_reasonUIActionBase { }
// 默认导出
export default Crm_lost_reasonUIAction;