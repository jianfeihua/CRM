import { Uom_uomUIActionBase } from './uom-uom-ui-action-base';

/**
 * 产品计量单位UI服务对象
 *
 * @export
 * @class Uom_uomUIAction
 * @extends {Uom_uomUIActionBase}
 */
export class Uom_uomUIAction extends Uom_uomUIActionBase { }
// 默认导出
export default Uom_uomUIAction;