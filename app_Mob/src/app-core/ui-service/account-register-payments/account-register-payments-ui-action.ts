import { Account_register_paymentsUIActionBase } from './account-register-payments-ui-action-base';

/**
 * 登记付款UI服务对象
 *
 * @export
 * @class Account_register_paymentsUIAction
 * @extends {Account_register_paymentsUIActionBase}
 */
export class Account_register_paymentsUIAction extends Account_register_paymentsUIActionBase { }
// 默认导出
export default Account_register_paymentsUIAction;