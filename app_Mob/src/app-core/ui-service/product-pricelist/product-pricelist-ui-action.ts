import { Product_pricelistUIActionBase } from './product-pricelist-ui-action-base';

/**
 * 价格表UI服务对象
 *
 * @export
 * @class Product_pricelistUIAction
 * @extends {Product_pricelistUIActionBase}
 */
export class Product_pricelistUIAction extends Product_pricelistUIActionBase { }
// 默认导出
export default Product_pricelistUIAction;