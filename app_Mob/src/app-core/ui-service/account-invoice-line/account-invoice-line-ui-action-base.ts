import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 发票行UI服务对象基类
 *
 * @export
 * @class Account_invoice_lineUIActionBase
 * @extends {UIActionBase}
 */
export class Account_invoice_lineUIActionBase extends UIActionBase {

}