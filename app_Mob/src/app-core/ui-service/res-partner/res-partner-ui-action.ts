import { Res_partnerUIActionBase } from './res-partner-ui-action-base';

/**
 * 客户UI服务对象
 *
 * @export
 * @class Res_partnerUIAction
 * @extends {Res_partnerUIActionBase}
 */
export class Res_partnerUIAction extends Res_partnerUIActionBase { }
// 默认导出
export default Res_partnerUIAction;