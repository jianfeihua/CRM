import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 消息UI服务对象基类
 *
 * @export
 * @class Mail_messageUIActionBase
 * @extends {UIActionBase}
 */
export class Mail_messageUIActionBase extends UIActionBase {

}