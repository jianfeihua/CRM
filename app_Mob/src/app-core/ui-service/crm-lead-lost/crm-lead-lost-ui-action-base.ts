import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 获取丢失原因UI服务对象基类
 *
 * @export
 * @class Crm_lead_lostUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_lead_lostUIActionBase extends UIActionBase {

}