import { Crm_leadUIActionBase } from './crm-lead-ui-action-base';

/**
 * 线索/商机UI服务对象
 *
 * @export
 * @class Crm_leadUIAction
 * @extends {Crm_leadUIActionBase}
 */
export class Crm_leadUIAction extends Crm_leadUIActionBase { }
// 默认导出
export default Crm_leadUIAction;