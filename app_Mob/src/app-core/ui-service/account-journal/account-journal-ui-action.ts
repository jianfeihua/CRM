import { Account_journalUIActionBase } from './account-journal-ui-action-base';

/**
 * 日记账UI服务对象
 *
 * @export
 * @class Account_journalUIAction
 * @extends {Account_journalUIActionBase}
 */
export class Account_journalUIAction extends Account_journalUIActionBase { }
// 默认导出
export default Account_journalUIAction;