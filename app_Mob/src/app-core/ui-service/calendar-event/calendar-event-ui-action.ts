import { Calendar_eventUIActionBase } from './calendar-event-ui-action-base';

/**
 * 活动UI服务对象
 *
 * @export
 * @class Calendar_eventUIAction
 * @extends {Calendar_eventUIActionBase}
 */
export class Calendar_eventUIAction extends Calendar_eventUIActionBase { }
// 默认导出
export default Calendar_eventUIAction;