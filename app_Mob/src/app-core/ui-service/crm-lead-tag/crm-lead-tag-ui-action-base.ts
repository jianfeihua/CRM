import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 线索标签UI服务对象基类
 *
 * @export
 * @class Crm_lead_tagUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_lead_tagUIActionBase extends UIActionBase {

}