import { Product_productUIActionBase } from './product-product-ui-action-base';

/**
 * 产品UI服务对象
 *
 * @export
 * @class Product_productUIAction
 * @extends {Product_productUIActionBase}
 */
export class Product_productUIAction extends Product_productUIActionBase { }
// 默认导出
export default Product_productUIAction;