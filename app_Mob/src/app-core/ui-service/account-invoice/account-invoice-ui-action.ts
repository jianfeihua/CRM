import { Account_invoiceUIActionBase } from './account-invoice-ui-action-base';

/**
 * 发票UI服务对象
 *
 * @export
 * @class Account_invoiceUIAction
 * @extends {Account_invoiceUIActionBase}
 */
export class Account_invoiceUIAction extends Account_invoiceUIActionBase { }
// 默认导出
export default Account_invoiceUIAction;