iBizCRM前端应用为Odoo12中标准CRM业务模型的基础上借助iBizOdoo中台服务重新构建的具备现代化界面呈现与符合国内用户习惯的CRM标准系统，同时提供了原Odoo社区版中尚未提供的移动端界面呈现功能。 

iBizCRM系统群全面采取中台模式、SpringBoot+VUE前后台分离架构、MDD/MDA全方位建模技术，满足上万级用户的高性能需求，致力于提供高可用度、全业务覆盖的重度开源项目

iBiz致力于提升中国软件软件建设和应用的价值，从业务到技术，从用户到开发者，我们希望iBiz能帮助尽可能多的人：
* 如果你是企业CTO，不仅可以获取运行系统和完整源码，更可获取完整的业务模型，获得系统的实施建设能力和对软件资产的全面管控；
* 如果你是业务或者技术专家，你不仅可以获取全部的业务模型和完整源码，更可获取更强大的自动化实施能力，延伸强者的手臂；
* 如果你是一个初学者，我想从没有一个开源项目可以把最先进的业务和技术全面开放，完整的呈现在你的面前，任你所取。


# 项目总述
* iBizCRM希望给用户带来Odoo中页面呈现的技术革新，采用流行的前端应用框架解决企业在面对原有Odoo系统调整不易、技术老旧等问题，借助iBiz生产体系让界面设计实现标准化与模式化同时不丧失业务灵活性与复杂度。使得企业内原有Odoo系统适应潮流迸发出新的能量
* 有兴趣请帮点一下 **Star** 哦！
* **[iBiz开源社区](https://www.ibizlab.cn)**
* **[iBizOdoo在线演示](http://odoo.ibizlab.cn)**
* **[iBizOdoo移动端在线演示](http://odoo.ibizlab.cn/mob)**
* **[iBizOdoo解决方案](http://demo.ibizlab.cn/ibizsln_odoo)**

# 技术框架
**后台技术模板[iBiz4j Spring R7](http://demo.ibizlab.cn/ibizr7sfstdtempl/ibiz4jr7)**
* 核心框架：Spring Boot
* 持久层框架: Mybatis-plus
* 服务发现：Nacos
* 日志管理：Logback
* 项目管理框架: Maven

**前端技术模板[iBiz-Vue-R7-Plus](http://demo.ibizlab.cn/ibizr7pfstdtempl/ibizvuer7plus)**
* 前端MVVM框架：vue.js 2.6.10
* 路由：vue-router 3.1.3
* 状态管理：vue-router 3.1.3
* 国际化：vue-i18n 8.15.3
* 数据交互：axios 0.19.1
* UI框架：element-ui 2.13.0, view-design 4.1.0
* 工具库：qs, path-to-regexp, rxjs
* 图标库：font-awesome 4.7.0
* 引入组件： tinymce 4.8.5
* 代码风格检测：eslint

**移动端技术模板[iBiz-Vue-Mob-R7](http://demo.ibizlab.cn/ibizr7pfstdtempl/ibizvuemobr7)**
- 前端MVVM框架：`vue.js ^2.6.11 `
- 路由：`vue-router ^3.1.5` 
- 状态管理：`vuex ^3.1.2` 
- 国际化：`vue-i18n ^8.15.5` 
- 数据交互：`axios ^0.19.2` 
- UI框架：`@ionic/core ^5.0.5`, `vant ^2.5.4`
- 工具库：`qs ^6.9.1`, `path-to-regexp ^6.1.0`, `rxjs ^6.5.4`
- 图标库：`font-awesome 4.7.0`, `ionicons ^5.0.1` 
- 引入组件： `echarts ^4.6.0`
- 代码风格检测：`tslint`

# 开发环境
* JDK
* Maven
* Node.js
* Yarn
* Vue Cli


# 开源说明
* 本系统100%开源，遵守MIT协议


# 模型设计
* ER图设计
![ER图设计](https://images.gitee.com/uploads/images/2020/0520/130501_2b08f0ca_1804797.png "模型设计.png")
* 故事板
![故事板](https://images.gitee.com/uploads/images/2020/0520/130527_9b4649f2_1804797.png "故事板.png")
* 表单设计
![表单设计](https://images.gitee.com/uploads/images/2020/0520/130545_100f0dd8_1804797.png "表单设计.png")
* 图表设计
![图表设计](https://images.gitee.com/uploads/images/2020/0520/130820_68884f13_1804797.png "图表设计.png")


# 系统美图
* 工作台
![工作台](https://images.gitee.com/uploads/images/2020/0520/130848_5c5925fa_1804797.png "首页工作台.png")
* 工作台自定义
![工作台自定义](https://images.gitee.com/uploads/images/2020/0520/130915_655ff8b1_1804797.png "工作台自定义.png")
* 客户信息
![客户信息](https://images.gitee.com/uploads/images/2020/0520/131013_d8a90a49_1804797.png "客户信息表格.png")
* 移动端首页工作台
![移动端首页工作台](https://images.gitee.com/uploads/images/2020/0520/131349_500fdd65_1804797.png "移动端首页工作台.png")
* 移动端首页客户列表
![移动端首页客户列表](https://images.gitee.com/uploads/images/2020/0520/131415_492a8d12_1804797.png "移动端客户列表.png")
* 移动端首页商机线索列表
![* 移动端首页商机线索列表](https://images.gitee.com/uploads/images/2020/0520/131538_e1dda541_1804797.png "移动端商机线索列表.png")
* 移动端首页商机线索信息页
![移动端首页商机线索信息页](https://images.gitee.com/uploads/images/2020/0520/131513_3975903b_1804797.png "移动式商机线索信息页.png")


# 捐赠
开源不易，坚持更难！如果您觉得iBizCRM不错，可以捐赠请作者喝杯咖啡~，在此表示感谢^_^。

点击以下链接，将页面拉到最下方点击“捐赠”即可。

[前往捐赠](https://gitee.com/iBizOdoo/CRM)
