package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_resend_cancel;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_cancelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_resend_cancel] 服务对象接口
 */
public interface mail_resend_cancelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_cancels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_cancels/{id}")
    public mail_resend_cancelImpl update(@PathVariable("id") Integer id,@RequestBody mail_resend_cancelImpl mail_resend_cancel);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_cancels/createbatch")
    public mail_resend_cancelImpl createBatch(@RequestBody List<mail_resend_cancelImpl> mail_resend_cancels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_cancels/removebatch")
    public mail_resend_cancelImpl removeBatch(@RequestBody List<mail_resend_cancelImpl> mail_resend_cancels);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_cancels/updatebatch")
    public mail_resend_cancelImpl updateBatch(@RequestBody List<mail_resend_cancelImpl> mail_resend_cancels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_cancels/fetchdefault")
    public Page<mail_resend_cancelImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_cancels")
    public mail_resend_cancelImpl create(@RequestBody mail_resend_cancelImpl mail_resend_cancel);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_cancels/{id}")
    public mail_resend_cancelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_cancels/select")
    public Page<mail_resend_cancelImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_cancels/{id}/getdraft")
    public mail_resend_cancelImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_resend_cancelImpl mail_resend_cancel);



}
