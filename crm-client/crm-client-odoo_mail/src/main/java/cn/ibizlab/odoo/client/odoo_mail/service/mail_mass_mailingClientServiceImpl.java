package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailingClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailingImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailingClientServiceImpl implements Imail_mass_mailingClientService {

    mail_mass_mailingFeignClient mail_mass_mailingFeignClient;

    @Autowired
    public mail_mass_mailingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailingFeignClient = nameBuilder.target(mail_mass_mailingFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailingFeignClient = nameBuilder.target(mail_mass_mailingFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing createModel() {
		return new mail_mass_mailingImpl();
	}


    public void removeBatch(List<Imail_mass_mailing> mail_mass_mailings){
        if(mail_mass_mailings!=null){
            List<mail_mass_mailingImpl> list = new ArrayList<mail_mass_mailingImpl>();
            for(Imail_mass_mailing imail_mass_mailing :mail_mass_mailings){
                list.add((mail_mass_mailingImpl)imail_mass_mailing) ;
            }
            mail_mass_mailingFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imail_mass_mailing mail_mass_mailing){
        Imail_mass_mailing clientModel = mail_mass_mailingFeignClient.create((mail_mass_mailingImpl)mail_mass_mailing) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing, null);
    }


    public void remove(Imail_mass_mailing mail_mass_mailing){
        mail_mass_mailingFeignClient.remove(mail_mass_mailing.getId()) ;
    }


    public void updateBatch(List<Imail_mass_mailing> mail_mass_mailings){
        if(mail_mass_mailings!=null){
            List<mail_mass_mailingImpl> list = new ArrayList<mail_mass_mailingImpl>();
            for(Imail_mass_mailing imail_mass_mailing :mail_mass_mailings){
                list.add((mail_mass_mailingImpl)imail_mass_mailing) ;
            }
            mail_mass_mailingFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_mass_mailing> fetchDefault(SearchContext context){
        Page<mail_mass_mailingImpl> page = this.mail_mass_mailingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imail_mass_mailing mail_mass_mailing){
        Imail_mass_mailing clientModel = mail_mass_mailingFeignClient.update(mail_mass_mailing.getId(),(mail_mass_mailingImpl)mail_mass_mailing) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing, null);
    }


    public void createBatch(List<Imail_mass_mailing> mail_mass_mailings){
        if(mail_mass_mailings!=null){
            List<mail_mass_mailingImpl> list = new ArrayList<mail_mass_mailingImpl>();
            for(Imail_mass_mailing imail_mass_mailing :mail_mass_mailings){
                list.add((mail_mass_mailingImpl)imail_mass_mailing) ;
            }
            mail_mass_mailingFeignClient.createBatch(list) ;
        }
    }


    public void get(Imail_mass_mailing mail_mass_mailing){
        Imail_mass_mailing clientModel = mail_mass_mailingFeignClient.get(mail_mass_mailing.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing, null);
    }


    public Page<Imail_mass_mailing> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing mail_mass_mailing){
        Imail_mass_mailing clientModel = mail_mass_mailingFeignClient.getDraft(mail_mass_mailing.getId(),(mail_mass_mailingImpl)mail_mass_mailing) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing, null);
    }



}

