package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_compose_message;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_compose_messageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_compose_message] 服务对象接口
 */
public interface mail_compose_messageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_compose_messages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_compose_messages/{id}")
    public mail_compose_messageImpl update(@PathVariable("id") Integer id,@RequestBody mail_compose_messageImpl mail_compose_message);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_compose_messages/removebatch")
    public mail_compose_messageImpl removeBatch(@RequestBody List<mail_compose_messageImpl> mail_compose_messages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_compose_messages/fetchdefault")
    public Page<mail_compose_messageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_compose_messages/updatebatch")
    public mail_compose_messageImpl updateBatch(@RequestBody List<mail_compose_messageImpl> mail_compose_messages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_compose_messages")
    public mail_compose_messageImpl create(@RequestBody mail_compose_messageImpl mail_compose_message);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_compose_messages/createbatch")
    public mail_compose_messageImpl createBatch(@RequestBody List<mail_compose_messageImpl> mail_compose_messages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_compose_messages/{id}")
    public mail_compose_messageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_compose_messages/select")
    public Page<mail_compose_messageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_compose_messages/{id}/getdraft")
    public mail_compose_messageImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_compose_messageImpl mail_compose_message);



}
