package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_tag;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_tagClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_tagImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailing_tagFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing_tag] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailing_tagClientServiceImpl implements Imail_mass_mailing_tagClientService {

    mail_mass_mailing_tagFeignClient mail_mass_mailing_tagFeignClient;

    @Autowired
    public mail_mass_mailing_tagClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_tagFeignClient = nameBuilder.target(mail_mass_mailing_tagFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_tagFeignClient = nameBuilder.target(mail_mass_mailing_tagFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing_tag createModel() {
		return new mail_mass_mailing_tagImpl();
	}


    public void removeBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags){
        if(mail_mass_mailing_tags!=null){
            List<mail_mass_mailing_tagImpl> list = new ArrayList<mail_mass_mailing_tagImpl>();
            for(Imail_mass_mailing_tag imail_mass_mailing_tag :mail_mass_mailing_tags){
                list.add((mail_mass_mailing_tagImpl)imail_mass_mailing_tag) ;
            }
            mail_mass_mailing_tagFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imail_mass_mailing_tag mail_mass_mailing_tag){
        mail_mass_mailing_tagFeignClient.remove(mail_mass_mailing_tag.getId()) ;
    }


    public Page<Imail_mass_mailing_tag> fetchDefault(SearchContext context){
        Page<mail_mass_mailing_tagImpl> page = this.mail_mass_mailing_tagFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags){
        if(mail_mass_mailing_tags!=null){
            List<mail_mass_mailing_tagImpl> list = new ArrayList<mail_mass_mailing_tagImpl>();
            for(Imail_mass_mailing_tag imail_mass_mailing_tag :mail_mass_mailing_tags){
                list.add((mail_mass_mailing_tagImpl)imail_mass_mailing_tag) ;
            }
            mail_mass_mailing_tagFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_mass_mailing_tag mail_mass_mailing_tag){
        Imail_mass_mailing_tag clientModel = mail_mass_mailing_tagFeignClient.create((mail_mass_mailing_tagImpl)mail_mass_mailing_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_tag.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_tag, null);
    }


    public void update(Imail_mass_mailing_tag mail_mass_mailing_tag){
        Imail_mass_mailing_tag clientModel = mail_mass_mailing_tagFeignClient.update(mail_mass_mailing_tag.getId(),(mail_mass_mailing_tagImpl)mail_mass_mailing_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_tag.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_tag, null);
    }


    public void createBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags){
        if(mail_mass_mailing_tags!=null){
            List<mail_mass_mailing_tagImpl> list = new ArrayList<mail_mass_mailing_tagImpl>();
            for(Imail_mass_mailing_tag imail_mass_mailing_tag :mail_mass_mailing_tags){
                list.add((mail_mass_mailing_tagImpl)imail_mass_mailing_tag) ;
            }
            mail_mass_mailing_tagFeignClient.createBatch(list) ;
        }
    }


    public void get(Imail_mass_mailing_tag mail_mass_mailing_tag){
        Imail_mass_mailing_tag clientModel = mail_mass_mailing_tagFeignClient.get(mail_mass_mailing_tag.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_tag.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_tag, null);
    }


    public Page<Imail_mass_mailing_tag> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing_tag mail_mass_mailing_tag){
        Imail_mass_mailing_tag clientModel = mail_mass_mailing_tagFeignClient.getDraft(mail_mass_mailing_tag.getId(),(mail_mass_mailing_tagImpl)mail_mass_mailing_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_tag.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_tag, null);
    }



}

