package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mail_statistics;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mail_statisticsClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mail_statisticsImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mail_statisticsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mail_statistics] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mail_statisticsClientServiceImpl implements Imail_mail_statisticsClientService {

    mail_mail_statisticsFeignClient mail_mail_statisticsFeignClient;

    @Autowired
    public mail_mail_statisticsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mail_statisticsFeignClient = nameBuilder.target(mail_mail_statisticsFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mail_statisticsFeignClient = nameBuilder.target(mail_mail_statisticsFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mail_statistics createModel() {
		return new mail_mail_statisticsImpl();
	}


    public void remove(Imail_mail_statistics mail_mail_statistics){
        mail_mail_statisticsFeignClient.remove(mail_mail_statistics.getId()) ;
    }


    public Page<Imail_mail_statistics> fetchDefault(SearchContext context){
        Page<mail_mail_statisticsImpl> page = this.mail_mail_statisticsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Imail_mail_statistics> mail_mail_statistics){
        if(mail_mail_statistics!=null){
            List<mail_mail_statisticsImpl> list = new ArrayList<mail_mail_statisticsImpl>();
            for(Imail_mail_statistics imail_mail_statistics :mail_mail_statistics){
                list.add((mail_mail_statisticsImpl)imail_mail_statistics) ;
            }
            mail_mail_statisticsFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_mail_statistics mail_mail_statistics){
        Imail_mail_statistics clientModel = mail_mail_statisticsFeignClient.create((mail_mail_statisticsImpl)mail_mail_statistics) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail_statistics.getClass(), false);
        copier.copy(clientModel, mail_mail_statistics, null);
    }


    public void removeBatch(List<Imail_mail_statistics> mail_mail_statistics){
        if(mail_mail_statistics!=null){
            List<mail_mail_statisticsImpl> list = new ArrayList<mail_mail_statisticsImpl>();
            for(Imail_mail_statistics imail_mail_statistics :mail_mail_statistics){
                list.add((mail_mail_statisticsImpl)imail_mail_statistics) ;
            }
            mail_mail_statisticsFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imail_mail_statistics> mail_mail_statistics){
        if(mail_mail_statistics!=null){
            List<mail_mail_statisticsImpl> list = new ArrayList<mail_mail_statisticsImpl>();
            for(Imail_mail_statistics imail_mail_statistics :mail_mail_statistics){
                list.add((mail_mail_statisticsImpl)imail_mail_statistics) ;
            }
            mail_mail_statisticsFeignClient.createBatch(list) ;
        }
    }


    public void update(Imail_mail_statistics mail_mail_statistics){
        Imail_mail_statistics clientModel = mail_mail_statisticsFeignClient.update(mail_mail_statistics.getId(),(mail_mail_statisticsImpl)mail_mail_statistics) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail_statistics.getClass(), false);
        copier.copy(clientModel, mail_mail_statistics, null);
    }


    public void get(Imail_mail_statistics mail_mail_statistics){
        Imail_mail_statistics clientModel = mail_mail_statisticsFeignClient.get(mail_mail_statistics.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail_statistics.getClass(), false);
        copier.copy(clientModel, mail_mail_statistics, null);
    }


    public Page<Imail_mail_statistics> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mail_statistics mail_mail_statistics){
        Imail_mail_statistics clientModel = mail_mail_statisticsFeignClient.getDraft(mail_mail_statistics.getId(),(mail_mail_statisticsImpl)mail_mail_statistics) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail_statistics.getClass(), false);
        copier.copy(clientModel, mail_mail_statistics, null);
    }



}

