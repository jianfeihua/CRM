package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_mail_statistics;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mail_mail_statistics] 对象
 */
public class mail_mail_statisticsImpl implements Imail_mail_statistics,Serializable{

    /**
     * 被退回
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp bounced;

    @JsonIgnore
    public boolean bouncedDirtyFlag;
    
    /**
     * 点击率
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp clicked;

    @JsonIgnore
    public boolean clickedDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 收件人email地址
     */
    public String email;

    @JsonIgnore
    public boolean emailDirtyFlag;
    
    /**
     * 异常
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp exception;

    @JsonIgnore
    public boolean exceptionDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 忽略
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp ignored;

    @JsonIgnore
    public boolean ignoredDirtyFlag;
    
    /**
     * 点击链接
     */
    public String links_click_ids;

    @JsonIgnore
    public boolean links_click_idsDirtyFlag;
    
    /**
     * 邮件
     */
    public Integer mail_mail_id;

    @JsonIgnore
    public boolean mail_mail_idDirtyFlag;
    
    /**
     * 邮件ID（技术）
     */
    public Integer mail_mail_id_int;

    @JsonIgnore
    public boolean mail_mail_id_intDirtyFlag;
    
    /**
     * 群发邮件营销
     */
    public Integer mass_mailing_campaign_id;

    @JsonIgnore
    public boolean mass_mailing_campaign_idDirtyFlag;
    
    /**
     * 群发邮件营销
     */
    public String mass_mailing_campaign_id_text;

    @JsonIgnore
    public boolean mass_mailing_campaign_id_textDirtyFlag;
    
    /**
     * 群发邮件
     */
    public Integer mass_mailing_id;

    @JsonIgnore
    public boolean mass_mailing_idDirtyFlag;
    
    /**
     * 群发邮件
     */
    public String mass_mailing_id_text;

    @JsonIgnore
    public boolean mass_mailing_id_textDirtyFlag;
    
    /**
     * 消息ID
     */
    public String message_id;

    @JsonIgnore
    public boolean message_idDirtyFlag;
    
    /**
     * 文档模型
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * 已开启
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp opened;

    @JsonIgnore
    public boolean openedDirtyFlag;
    
    /**
     * 已回复
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp replied;

    @JsonIgnore
    public boolean repliedDirtyFlag;
    
    /**
     * 文档ID
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 安排
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp scheduled;

    @JsonIgnore
    public boolean scheduledDirtyFlag;
    
    /**
     * 已汇
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp sent;

    @JsonIgnore
    public boolean sentDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 状态更新
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp state_update;

    @JsonIgnore
    public boolean state_updateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [被退回]
     */
    @JsonProperty("bounced")
    public Timestamp getBounced(){
        return this.bounced ;
    }

    /**
     * 设置 [被退回]
     */
    @JsonProperty("bounced")
    public void setBounced(Timestamp  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

     /**
     * 获取 [被退回]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return this.bouncedDirtyFlag ;
    }   

    /**
     * 获取 [点击率]
     */
    @JsonProperty("clicked")
    public Timestamp getClicked(){
        return this.clicked ;
    }

    /**
     * 设置 [点击率]
     */
    @JsonProperty("clicked")
    public void setClicked(Timestamp  clicked){
        this.clicked = clicked ;
        this.clickedDirtyFlag = true ;
    }

     /**
     * 获取 [点击率]脏标记
     */
    @JsonIgnore
    public boolean getClickedDirtyFlag(){
        return this.clickedDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [收件人email地址]
     */
    @JsonProperty("email")
    public String getEmail(){
        return this.email ;
    }

    /**
     * 设置 [收件人email地址]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

     /**
     * 获取 [收件人email地址]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return this.emailDirtyFlag ;
    }   

    /**
     * 获取 [异常]
     */
    @JsonProperty("exception")
    public Timestamp getException(){
        return this.exception ;
    }

    /**
     * 设置 [异常]
     */
    @JsonProperty("exception")
    public void setException(Timestamp  exception){
        this.exception = exception ;
        this.exceptionDirtyFlag = true ;
    }

     /**
     * 获取 [异常]脏标记
     */
    @JsonIgnore
    public boolean getExceptionDirtyFlag(){
        return this.exceptionDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [忽略]
     */
    @JsonProperty("ignored")
    public Timestamp getIgnored(){
        return this.ignored ;
    }

    /**
     * 设置 [忽略]
     */
    @JsonProperty("ignored")
    public void setIgnored(Timestamp  ignored){
        this.ignored = ignored ;
        this.ignoredDirtyFlag = true ;
    }

     /**
     * 获取 [忽略]脏标记
     */
    @JsonIgnore
    public boolean getIgnoredDirtyFlag(){
        return this.ignoredDirtyFlag ;
    }   

    /**
     * 获取 [点击链接]
     */
    @JsonProperty("links_click_ids")
    public String getLinks_click_ids(){
        return this.links_click_ids ;
    }

    /**
     * 设置 [点击链接]
     */
    @JsonProperty("links_click_ids")
    public void setLinks_click_ids(String  links_click_ids){
        this.links_click_ids = links_click_ids ;
        this.links_click_idsDirtyFlag = true ;
    }

     /**
     * 获取 [点击链接]脏标记
     */
    @JsonIgnore
    public boolean getLinks_click_idsDirtyFlag(){
        return this.links_click_idsDirtyFlag ;
    }   

    /**
     * 获取 [邮件]
     */
    @JsonProperty("mail_mail_id")
    public Integer getMail_mail_id(){
        return this.mail_mail_id ;
    }

    /**
     * 设置 [邮件]
     */
    @JsonProperty("mail_mail_id")
    public void setMail_mail_id(Integer  mail_mail_id){
        this.mail_mail_id = mail_mail_id ;
        this.mail_mail_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件]脏标记
     */
    @JsonIgnore
    public boolean getMail_mail_idDirtyFlag(){
        return this.mail_mail_idDirtyFlag ;
    }   

    /**
     * 获取 [邮件ID（技术）]
     */
    @JsonProperty("mail_mail_id_int")
    public Integer getMail_mail_id_int(){
        return this.mail_mail_id_int ;
    }

    /**
     * 设置 [邮件ID（技术）]
     */
    @JsonProperty("mail_mail_id_int")
    public void setMail_mail_id_int(Integer  mail_mail_id_int){
        this.mail_mail_id_int = mail_mail_id_int ;
        this.mail_mail_id_intDirtyFlag = true ;
    }

     /**
     * 获取 [邮件ID（技术）]脏标记
     */
    @JsonIgnore
    public boolean getMail_mail_id_intDirtyFlag(){
        return this.mail_mail_id_intDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public Integer getMass_mailing_campaign_id(){
        return this.mass_mailing_campaign_id ;
    }

    /**
     * 设置 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public void setMass_mailing_campaign_id(Integer  mass_mailing_campaign_id){
        this.mass_mailing_campaign_id = mass_mailing_campaign_id ;
        this.mass_mailing_campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件营销]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_idDirtyFlag(){
        return this.mass_mailing_campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public String getMass_mailing_campaign_id_text(){
        return this.mass_mailing_campaign_id_text ;
    }

    /**
     * 设置 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public void setMass_mailing_campaign_id_text(String  mass_mailing_campaign_id_text){
        this.mass_mailing_campaign_id_text = mass_mailing_campaign_id_text ;
        this.mass_mailing_campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件营销]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_id_textDirtyFlag(){
        return this.mass_mailing_campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件]
     */
    @JsonProperty("mass_mailing_id")
    public Integer getMass_mailing_id(){
        return this.mass_mailing_id ;
    }

    /**
     * 设置 [群发邮件]
     */
    @JsonProperty("mass_mailing_id")
    public void setMass_mailing_id(Integer  mass_mailing_id){
        this.mass_mailing_id = mass_mailing_id ;
        this.mass_mailing_idDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_idDirtyFlag(){
        return this.mass_mailing_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件]
     */
    @JsonProperty("mass_mailing_id_text")
    public String getMass_mailing_id_text(){
        return this.mass_mailing_id_text ;
    }

    /**
     * 设置 [群发邮件]
     */
    @JsonProperty("mass_mailing_id_text")
    public void setMass_mailing_id_text(String  mass_mailing_id_text){
        this.mass_mailing_id_text = mass_mailing_id_text ;
        this.mass_mailing_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_id_textDirtyFlag(){
        return this.mass_mailing_id_textDirtyFlag ;
    }   

    /**
     * 获取 [消息ID]
     */
    @JsonProperty("message_id")
    public String getMessage_id(){
        return this.message_id ;
    }

    /**
     * 设置 [消息ID]
     */
    @JsonProperty("message_id")
    public void setMessage_id(String  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

     /**
     * 获取 [消息ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return this.message_idDirtyFlag ;
    }   

    /**
     * 获取 [文档模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [文档模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [文档模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [已开启]
     */
    @JsonProperty("opened")
    public Timestamp getOpened(){
        return this.opened ;
    }

    /**
     * 设置 [已开启]
     */
    @JsonProperty("opened")
    public void setOpened(Timestamp  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

     /**
     * 获取 [已开启]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return this.openedDirtyFlag ;
    }   

    /**
     * 获取 [已回复]
     */
    @JsonProperty("replied")
    public Timestamp getReplied(){
        return this.replied ;
    }

    /**
     * 设置 [已回复]
     */
    @JsonProperty("replied")
    public void setReplied(Timestamp  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

     /**
     * 获取 [已回复]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return this.repliedDirtyFlag ;
    }   

    /**
     * 获取 [文档ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [文档ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [安排]
     */
    @JsonProperty("scheduled")
    public Timestamp getScheduled(){
        return this.scheduled ;
    }

    /**
     * 设置 [安排]
     */
    @JsonProperty("scheduled")
    public void setScheduled(Timestamp  scheduled){
        this.scheduled = scheduled ;
        this.scheduledDirtyFlag = true ;
    }

     /**
     * 获取 [安排]脏标记
     */
    @JsonIgnore
    public boolean getScheduledDirtyFlag(){
        return this.scheduledDirtyFlag ;
    }   

    /**
     * 获取 [已汇]
     */
    @JsonProperty("sent")
    public Timestamp getSent(){
        return this.sent ;
    }

    /**
     * 设置 [已汇]
     */
    @JsonProperty("sent")
    public void setSent(Timestamp  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

     /**
     * 获取 [已汇]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return this.sentDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [状态更新]
     */
    @JsonProperty("state_update")
    public Timestamp getState_update(){
        return this.state_update ;
    }

    /**
     * 设置 [状态更新]
     */
    @JsonProperty("state_update")
    public void setState_update(Timestamp  state_update){
        this.state_update = state_update ;
        this.state_updateDirtyFlag = true ;
    }

     /**
     * 获取 [状态更新]脏标记
     */
    @JsonIgnore
    public boolean getState_updateDirtyFlag(){
        return this.state_updateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
