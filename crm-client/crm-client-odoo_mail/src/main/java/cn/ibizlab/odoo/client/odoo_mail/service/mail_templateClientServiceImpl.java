package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_template;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_templateClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_templateImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_templateClientServiceImpl implements Imail_templateClientService {

    mail_templateFeignClient mail_templateFeignClient;

    @Autowired
    public mail_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_templateFeignClient = nameBuilder.target(mail_templateFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_templateFeignClient = nameBuilder.target(mail_templateFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_template createModel() {
		return new mail_templateImpl();
	}


    public void updateBatch(List<Imail_template> mail_templates){
        if(mail_templates!=null){
            List<mail_templateImpl> list = new ArrayList<mail_templateImpl>();
            for(Imail_template imail_template :mail_templates){
                list.add((mail_templateImpl)imail_template) ;
            }
            mail_templateFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_template mail_template){
        Imail_template clientModel = mail_templateFeignClient.create((mail_templateImpl)mail_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_template.getClass(), false);
        copier.copy(clientModel, mail_template, null);
    }


    public void createBatch(List<Imail_template> mail_templates){
        if(mail_templates!=null){
            List<mail_templateImpl> list = new ArrayList<mail_templateImpl>();
            for(Imail_template imail_template :mail_templates){
                list.add((mail_templateImpl)imail_template) ;
            }
            mail_templateFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_template> fetchDefault(SearchContext context){
        Page<mail_templateImpl> page = this.mail_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imail_template mail_template){
        mail_templateFeignClient.remove(mail_template.getId()) ;
    }


    public void update(Imail_template mail_template){
        Imail_template clientModel = mail_templateFeignClient.update(mail_template.getId(),(mail_templateImpl)mail_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_template.getClass(), false);
        copier.copy(clientModel, mail_template, null);
    }


    public void get(Imail_template mail_template){
        Imail_template clientModel = mail_templateFeignClient.get(mail_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_template.getClass(), false);
        copier.copy(clientModel, mail_template, null);
    }


    public void removeBatch(List<Imail_template> mail_templates){
        if(mail_templates!=null){
            List<mail_templateImpl> list = new ArrayList<mail_templateImpl>();
            for(Imail_template imail_template :mail_templates){
                list.add((mail_templateImpl)imail_template) ;
            }
            mail_templateFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_template mail_template){
        Imail_template clientModel = mail_templateFeignClient.getDraft(mail_template.getId(),(mail_templateImpl)mail_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_template.getClass(), false);
        copier.copy(clientModel, mail_template, null);
    }



}

