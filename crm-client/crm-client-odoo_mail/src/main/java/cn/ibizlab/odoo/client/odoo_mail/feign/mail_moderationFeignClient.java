package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_moderation;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_moderationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_moderation] 服务对象接口
 */
public interface mail_moderationFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_moderations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_moderations/removebatch")
    public mail_moderationImpl removeBatch(@RequestBody List<mail_moderationImpl> mail_moderations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_moderations/fetchdefault")
    public Page<mail_moderationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_moderations/{id}")
    public mail_moderationImpl update(@PathVariable("id") Integer id,@RequestBody mail_moderationImpl mail_moderation);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_moderations/{id}")
    public mail_moderationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_moderations")
    public mail_moderationImpl create(@RequestBody mail_moderationImpl mail_moderation);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_moderations/createbatch")
    public mail_moderationImpl createBatch(@RequestBody List<mail_moderationImpl> mail_moderations);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_moderations/updatebatch")
    public mail_moderationImpl updateBatch(@RequestBody List<mail_moderationImpl> mail_moderations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_moderations/select")
    public Page<mail_moderationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_moderations/{id}/getdraft")
    public mail_moderationImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_moderationImpl mail_moderation);



}
