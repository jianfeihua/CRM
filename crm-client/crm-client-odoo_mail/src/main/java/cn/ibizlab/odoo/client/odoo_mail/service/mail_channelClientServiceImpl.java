package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_channel;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_channelClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_channelImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_channelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_channel] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_channelClientServiceImpl implements Imail_channelClientService {

    mail_channelFeignClient mail_channelFeignClient;

    @Autowired
    public mail_channelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_channelFeignClient = nameBuilder.target(mail_channelFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_channelFeignClient = nameBuilder.target(mail_channelFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_channel createModel() {
		return new mail_channelImpl();
	}


    public void updateBatch(List<Imail_channel> mail_channels){
        if(mail_channels!=null){
            List<mail_channelImpl> list = new ArrayList<mail_channelImpl>();
            for(Imail_channel imail_channel :mail_channels){
                list.add((mail_channelImpl)imail_channel) ;
            }
            mail_channelFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_channel mail_channel){
        Imail_channel clientModel = mail_channelFeignClient.create((mail_channelImpl)mail_channel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel.getClass(), false);
        copier.copy(clientModel, mail_channel, null);
    }


    public Page<Imail_channel> fetchDefault(SearchContext context){
        Page<mail_channelImpl> page = this.mail_channelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imail_channel mail_channel){
        Imail_channel clientModel = mail_channelFeignClient.get(mail_channel.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel.getClass(), false);
        copier.copy(clientModel, mail_channel, null);
    }


    public void removeBatch(List<Imail_channel> mail_channels){
        if(mail_channels!=null){
            List<mail_channelImpl> list = new ArrayList<mail_channelImpl>();
            for(Imail_channel imail_channel :mail_channels){
                list.add((mail_channelImpl)imail_channel) ;
            }
            mail_channelFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imail_channel> mail_channels){
        if(mail_channels!=null){
            List<mail_channelImpl> list = new ArrayList<mail_channelImpl>();
            for(Imail_channel imail_channel :mail_channels){
                list.add((mail_channelImpl)imail_channel) ;
            }
            mail_channelFeignClient.createBatch(list) ;
        }
    }


    public void update(Imail_channel mail_channel){
        Imail_channel clientModel = mail_channelFeignClient.update(mail_channel.getId(),(mail_channelImpl)mail_channel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel.getClass(), false);
        copier.copy(clientModel, mail_channel, null);
    }


    public void remove(Imail_channel mail_channel){
        mail_channelFeignClient.remove(mail_channel.getId()) ;
    }


    public Page<Imail_channel> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_channel mail_channel){
        Imail_channel clientModel = mail_channelFeignClient.getDraft(mail_channel.getId(),(mail_channelImpl)mail_channel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel.getClass(), false);
        copier.copy(clientModel, mail_channel, null);
    }



}

