package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_activity_type;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activity_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_activity_type] 服务对象接口
 */
public interface mail_activity_typeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_types/fetchdefault")
    public Page<mail_activity_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_activity_types/{id}")
    public mail_activity_typeImpl update(@PathVariable("id") Integer id,@RequestBody mail_activity_typeImpl mail_activity_type);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activity_types")
    public mail_activity_typeImpl create(@RequestBody mail_activity_typeImpl mail_activity_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_types/{id}")
    public mail_activity_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_activity_types/updatebatch")
    public mail_activity_typeImpl updateBatch(@RequestBody List<mail_activity_typeImpl> mail_activity_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_activity_types/removebatch")
    public mail_activity_typeImpl removeBatch(@RequestBody List<mail_activity_typeImpl> mail_activity_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_activity_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activity_types/createbatch")
    public mail_activity_typeImpl createBatch(@RequestBody List<mail_activity_typeImpl> mail_activity_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_types/select")
    public Page<mail_activity_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_types/{id}/getdraft")
    public mail_activity_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_activity_typeImpl mail_activity_type);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activity_types/{id}/checkkey")
    public mail_activity_typeImpl checkKey(@PathVariable("id") Integer id,@RequestBody mail_activity_typeImpl mail_activity_type);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activity_types/{id}/save")
    public mail_activity_typeImpl save(@PathVariable("id") Integer id,@RequestBody mail_activity_typeImpl mail_activity_type);



}
