package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_statistics_report;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mail_statistics_report] 对象
 */
public class mail_statistics_reportImpl implements Imail_statistics_report,Serializable{

    /**
     * 被退回
     */
    public Integer bounced;

    @JsonIgnore
    public boolean bouncedDirtyFlag;
    
    /**
     * 群发邮件营销
     */
    public String campaign;

    @JsonIgnore
    public boolean campaignDirtyFlag;
    
    /**
     * 点击率
     */
    public Integer clicked;

    @JsonIgnore
    public boolean clickedDirtyFlag;
    
    /**
     * 已送货
     */
    public Integer delivered;

    @JsonIgnore
    public boolean deliveredDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 从
     */
    public String email_from;

    @JsonIgnore
    public boolean email_fromDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 群发邮件
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 已开启
     */
    public Integer opened;

    @JsonIgnore
    public boolean openedDirtyFlag;
    
    /**
     * 已回复
     */
    public Integer replied;

    @JsonIgnore
    public boolean repliedDirtyFlag;
    
    /**
     * 计划日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp scheduled_date;

    @JsonIgnore
    public boolean scheduled_dateDirtyFlag;
    
    /**
     * 已汇
     */
    public Integer sent;

    @JsonIgnore
    public boolean sentDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [被退回]
     */
    @JsonProperty("bounced")
    public Integer getBounced(){
        return this.bounced ;
    }

    /**
     * 设置 [被退回]
     */
    @JsonProperty("bounced")
    public void setBounced(Integer  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

     /**
     * 获取 [被退回]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return this.bouncedDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件营销]
     */
    @JsonProperty("campaign")
    public String getCampaign(){
        return this.campaign ;
    }

    /**
     * 设置 [群发邮件营销]
     */
    @JsonProperty("campaign")
    public void setCampaign(String  campaign){
        this.campaign = campaign ;
        this.campaignDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaignDirtyFlag(){
        return this.campaignDirtyFlag ;
    }   

    /**
     * 获取 [点击率]
     */
    @JsonProperty("clicked")
    public Integer getClicked(){
        return this.clicked ;
    }

    /**
     * 设置 [点击率]
     */
    @JsonProperty("clicked")
    public void setClicked(Integer  clicked){
        this.clicked = clicked ;
        this.clickedDirtyFlag = true ;
    }

     /**
     * 获取 [点击率]脏标记
     */
    @JsonIgnore
    public boolean getClickedDirtyFlag(){
        return this.clickedDirtyFlag ;
    }   

    /**
     * 获取 [已送货]
     */
    @JsonProperty("delivered")
    public Integer getDelivered(){
        return this.delivered ;
    }

    /**
     * 设置 [已送货]
     */
    @JsonProperty("delivered")
    public void setDelivered(Integer  delivered){
        this.delivered = delivered ;
        this.deliveredDirtyFlag = true ;
    }

     /**
     * 获取 [已送货]脏标记
     */
    @JsonIgnore
    public boolean getDeliveredDirtyFlag(){
        return this.deliveredDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [群发邮件]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [已开启]
     */
    @JsonProperty("opened")
    public Integer getOpened(){
        return this.opened ;
    }

    /**
     * 设置 [已开启]
     */
    @JsonProperty("opened")
    public void setOpened(Integer  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

     /**
     * 获取 [已开启]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return this.openedDirtyFlag ;
    }   

    /**
     * 获取 [已回复]
     */
    @JsonProperty("replied")
    public Integer getReplied(){
        return this.replied ;
    }

    /**
     * 设置 [已回复]
     */
    @JsonProperty("replied")
    public void setReplied(Integer  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

     /**
     * 获取 [已回复]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return this.repliedDirtyFlag ;
    }   

    /**
     * 获取 [计划日期]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return this.scheduled_date ;
    }

    /**
     * 设置 [计划日期]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

     /**
     * 获取 [计划日期]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return this.scheduled_dateDirtyFlag ;
    }   

    /**
     * 获取 [已汇]
     */
    @JsonProperty("sent")
    public Integer getSent(){
        return this.sent ;
    }

    /**
     * 设置 [已汇]
     */
    @JsonProperty("sent")
    public void setSent(Integer  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

     /**
     * 获取 [已汇]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return this.sentDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
