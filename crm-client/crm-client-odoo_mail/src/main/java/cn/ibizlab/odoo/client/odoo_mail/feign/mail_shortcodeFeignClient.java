package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_shortcode;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_shortcodeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_shortcode] 服务对象接口
 */
public interface mail_shortcodeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_shortcodes/updatebatch")
    public mail_shortcodeImpl updateBatch(@RequestBody List<mail_shortcodeImpl> mail_shortcodes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_shortcodes/{id}")
    public mail_shortcodeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_shortcodes/fetchdefault")
    public Page<mail_shortcodeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_shortcodes/createbatch")
    public mail_shortcodeImpl createBatch(@RequestBody List<mail_shortcodeImpl> mail_shortcodes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_shortcodes")
    public mail_shortcodeImpl create(@RequestBody mail_shortcodeImpl mail_shortcode);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_shortcodes/removebatch")
    public mail_shortcodeImpl removeBatch(@RequestBody List<mail_shortcodeImpl> mail_shortcodes);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_shortcodes/{id}")
    public mail_shortcodeImpl update(@PathVariable("id") Integer id,@RequestBody mail_shortcodeImpl mail_shortcode);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_shortcodes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_shortcodes/select")
    public Page<mail_shortcodeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_shortcodes/{id}/getdraft")
    public mail_shortcodeImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_shortcodeImpl mail_shortcode);



}
