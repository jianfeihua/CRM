package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_activity_mixin;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_activity_mixinClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activity_mixinImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_activity_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_activity_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_activity_mixinClientServiceImpl implements Imail_activity_mixinClientService {

    mail_activity_mixinFeignClient mail_activity_mixinFeignClient;

    @Autowired
    public mail_activity_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_activity_mixinFeignClient = nameBuilder.target(mail_activity_mixinFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_activity_mixinFeignClient = nameBuilder.target(mail_activity_mixinFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_activity_mixin createModel() {
		return new mail_activity_mixinImpl();
	}


    public void create(Imail_activity_mixin mail_activity_mixin){
        Imail_activity_mixin clientModel = mail_activity_mixinFeignClient.create((mail_activity_mixinImpl)mail_activity_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_mixin.getClass(), false);
        copier.copy(clientModel, mail_activity_mixin, null);
    }


    public Page<Imail_activity_mixin> fetchDefault(SearchContext context){
        Page<mail_activity_mixinImpl> page = this.mail_activity_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imail_activity_mixin mail_activity_mixin){
        Imail_activity_mixin clientModel = mail_activity_mixinFeignClient.get(mail_activity_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_mixin.getClass(), false);
        copier.copy(clientModel, mail_activity_mixin, null);
    }


    public void createBatch(List<Imail_activity_mixin> mail_activity_mixins){
        if(mail_activity_mixins!=null){
            List<mail_activity_mixinImpl> list = new ArrayList<mail_activity_mixinImpl>();
            for(Imail_activity_mixin imail_activity_mixin :mail_activity_mixins){
                list.add((mail_activity_mixinImpl)imail_activity_mixin) ;
            }
            mail_activity_mixinFeignClient.createBatch(list) ;
        }
    }


    public void update(Imail_activity_mixin mail_activity_mixin){
        Imail_activity_mixin clientModel = mail_activity_mixinFeignClient.update(mail_activity_mixin.getId(),(mail_activity_mixinImpl)mail_activity_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_mixin.getClass(), false);
        copier.copy(clientModel, mail_activity_mixin, null);
    }


    public void updateBatch(List<Imail_activity_mixin> mail_activity_mixins){
        if(mail_activity_mixins!=null){
            List<mail_activity_mixinImpl> list = new ArrayList<mail_activity_mixinImpl>();
            for(Imail_activity_mixin imail_activity_mixin :mail_activity_mixins){
                list.add((mail_activity_mixinImpl)imail_activity_mixin) ;
            }
            mail_activity_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Imail_activity_mixin mail_activity_mixin){
        mail_activity_mixinFeignClient.remove(mail_activity_mixin.getId()) ;
    }


    public void removeBatch(List<Imail_activity_mixin> mail_activity_mixins){
        if(mail_activity_mixins!=null){
            List<mail_activity_mixinImpl> list = new ArrayList<mail_activity_mixinImpl>();
            for(Imail_activity_mixin imail_activity_mixin :mail_activity_mixins){
                list.add((mail_activity_mixinImpl)imail_activity_mixin) ;
            }
            mail_activity_mixinFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_activity_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_activity_mixin mail_activity_mixin){
        Imail_activity_mixin clientModel = mail_activity_mixinFeignClient.getDraft(mail_activity_mixin.getId(),(mail_activity_mixinImpl)mail_activity_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_mixin.getClass(), false);
        copier.copy(clientModel, mail_activity_mixin, null);
    }



}

