package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_activity;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activityImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_activity] 服务对象接口
 */
public interface mail_activityFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_activities/removebatch")
    public mail_activityImpl removeBatch(@RequestBody List<mail_activityImpl> mail_activities);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activities/{id}")
    public mail_activityImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_activities/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activities/createbatch")
    public mail_activityImpl createBatch(@RequestBody List<mail_activityImpl> mail_activities);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_activities/{id}")
    public mail_activityImpl update(@PathVariable("id") Integer id,@RequestBody mail_activityImpl mail_activity);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activities")
    public mail_activityImpl create(@RequestBody mail_activityImpl mail_activity);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_activities/updatebatch")
    public mail_activityImpl updateBatch(@RequestBody List<mail_activityImpl> mail_activities);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activities/fetchdefault")
    public Page<mail_activityImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activities/select")
    public Page<mail_activityImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activities/{id}/getdraft")
    public mail_activityImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_activityImpl mail_activity);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activities/{id}/checkkey")
    public mail_activityImpl checkKey(@PathVariable("id") Integer id,@RequestBody mail_activityImpl mail_activity);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activities/{id}/save")
    public mail_activityImpl save(@PathVariable("id") Integer id,@RequestBody mail_activityImpl mail_activity);



}
