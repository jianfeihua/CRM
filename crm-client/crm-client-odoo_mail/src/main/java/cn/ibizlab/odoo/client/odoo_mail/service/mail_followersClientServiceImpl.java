package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_followers;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_followersClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_followersImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_followersFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_followers] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_followersClientServiceImpl implements Imail_followersClientService {

    mail_followersFeignClient mail_followersFeignClient;

    @Autowired
    public mail_followersClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_followersFeignClient = nameBuilder.target(mail_followersFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_followersFeignClient = nameBuilder.target(mail_followersFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_followers createModel() {
		return new mail_followersImpl();
	}


    public void createBatch(List<Imail_followers> mail_followers){
        if(mail_followers!=null){
            List<mail_followersImpl> list = new ArrayList<mail_followersImpl>();
            for(Imail_followers imail_followers :mail_followers){
                list.add((mail_followersImpl)imail_followers) ;
            }
            mail_followersFeignClient.createBatch(list) ;
        }
    }


    public void update(Imail_followers mail_followers){
        Imail_followers clientModel = mail_followersFeignClient.update(mail_followers.getId(),(mail_followersImpl)mail_followers) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_followers.getClass(), false);
        copier.copy(clientModel, mail_followers, null);
    }


    public void get(Imail_followers mail_followers){
        Imail_followers clientModel = mail_followersFeignClient.get(mail_followers.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_followers.getClass(), false);
        copier.copy(clientModel, mail_followers, null);
    }


    public void updateBatch(List<Imail_followers> mail_followers){
        if(mail_followers!=null){
            List<mail_followersImpl> list = new ArrayList<mail_followersImpl>();
            for(Imail_followers imail_followers :mail_followers){
                list.add((mail_followersImpl)imail_followers) ;
            }
            mail_followersFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_followers> fetchDefault(SearchContext context){
        Page<mail_followersImpl> page = this.mail_followersFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imail_followers> mail_followers){
        if(mail_followers!=null){
            List<mail_followersImpl> list = new ArrayList<mail_followersImpl>();
            for(Imail_followers imail_followers :mail_followers){
                list.add((mail_followersImpl)imail_followers) ;
            }
            mail_followersFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imail_followers mail_followers){
        mail_followersFeignClient.remove(mail_followers.getId()) ;
    }


    public void create(Imail_followers mail_followers){
        Imail_followers clientModel = mail_followersFeignClient.create((mail_followersImpl)mail_followers) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_followers.getClass(), false);
        copier.copy(clientModel, mail_followers, null);
    }


    public Page<Imail_followers> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_followers mail_followers){
        Imail_followers clientModel = mail_followersFeignClient.getDraft(mail_followers.getId(),(mail_followersImpl)mail_followers) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_followers.getClass(), false);
        copier.copy(clientModel, mail_followers, null);
    }



}

