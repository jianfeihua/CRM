package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_resend_cancel;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_resend_cancelClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_cancelImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_resend_cancelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_resend_cancel] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_resend_cancelClientServiceImpl implements Imail_resend_cancelClientService {

    mail_resend_cancelFeignClient mail_resend_cancelFeignClient;

    @Autowired
    public mail_resend_cancelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_resend_cancelFeignClient = nameBuilder.target(mail_resend_cancelFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_resend_cancelFeignClient = nameBuilder.target(mail_resend_cancelFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_resend_cancel createModel() {
		return new mail_resend_cancelImpl();
	}


    public void remove(Imail_resend_cancel mail_resend_cancel){
        mail_resend_cancelFeignClient.remove(mail_resend_cancel.getId()) ;
    }


    public void update(Imail_resend_cancel mail_resend_cancel){
        Imail_resend_cancel clientModel = mail_resend_cancelFeignClient.update(mail_resend_cancel.getId(),(mail_resend_cancelImpl)mail_resend_cancel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_cancel.getClass(), false);
        copier.copy(clientModel, mail_resend_cancel, null);
    }


    public void createBatch(List<Imail_resend_cancel> mail_resend_cancels){
        if(mail_resend_cancels!=null){
            List<mail_resend_cancelImpl> list = new ArrayList<mail_resend_cancelImpl>();
            for(Imail_resend_cancel imail_resend_cancel :mail_resend_cancels){
                list.add((mail_resend_cancelImpl)imail_resend_cancel) ;
            }
            mail_resend_cancelFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_resend_cancel> mail_resend_cancels){
        if(mail_resend_cancels!=null){
            List<mail_resend_cancelImpl> list = new ArrayList<mail_resend_cancelImpl>();
            for(Imail_resend_cancel imail_resend_cancel :mail_resend_cancels){
                list.add((mail_resend_cancelImpl)imail_resend_cancel) ;
            }
            mail_resend_cancelFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_resend_cancel> mail_resend_cancels){
        if(mail_resend_cancels!=null){
            List<mail_resend_cancelImpl> list = new ArrayList<mail_resend_cancelImpl>();
            for(Imail_resend_cancel imail_resend_cancel :mail_resend_cancels){
                list.add((mail_resend_cancelImpl)imail_resend_cancel) ;
            }
            mail_resend_cancelFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_resend_cancel> fetchDefault(SearchContext context){
        Page<mail_resend_cancelImpl> page = this.mail_resend_cancelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Imail_resend_cancel mail_resend_cancel){
        Imail_resend_cancel clientModel = mail_resend_cancelFeignClient.create((mail_resend_cancelImpl)mail_resend_cancel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_cancel.getClass(), false);
        copier.copy(clientModel, mail_resend_cancel, null);
    }


    public void get(Imail_resend_cancel mail_resend_cancel){
        Imail_resend_cancel clientModel = mail_resend_cancelFeignClient.get(mail_resend_cancel.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_cancel.getClass(), false);
        copier.copy(clientModel, mail_resend_cancel, null);
    }


    public Page<Imail_resend_cancel> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_resend_cancel mail_resend_cancel){
        Imail_resend_cancel clientModel = mail_resend_cancelFeignClient.getDraft(mail_resend_cancel.getId(),(mail_resend_cancelImpl)mail_resend_cancel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_cancel.getClass(), false);
        copier.copy(clientModel, mail_resend_cancel, null);
    }



}

