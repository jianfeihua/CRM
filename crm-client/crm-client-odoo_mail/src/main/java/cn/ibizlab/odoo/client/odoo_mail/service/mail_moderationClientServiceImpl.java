package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_moderation;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_moderationClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_moderationImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_moderationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_moderation] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_moderationClientServiceImpl implements Imail_moderationClientService {

    mail_moderationFeignClient mail_moderationFeignClient;

    @Autowired
    public mail_moderationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_moderationFeignClient = nameBuilder.target(mail_moderationFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_moderationFeignClient = nameBuilder.target(mail_moderationFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_moderation createModel() {
		return new mail_moderationImpl();
	}


    public void remove(Imail_moderation mail_moderation){
        mail_moderationFeignClient.remove(mail_moderation.getId()) ;
    }


    public void removeBatch(List<Imail_moderation> mail_moderations){
        if(mail_moderations!=null){
            List<mail_moderationImpl> list = new ArrayList<mail_moderationImpl>();
            for(Imail_moderation imail_moderation :mail_moderations){
                list.add((mail_moderationImpl)imail_moderation) ;
            }
            mail_moderationFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_moderation> fetchDefault(SearchContext context){
        Page<mail_moderationImpl> page = this.mail_moderationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imail_moderation mail_moderation){
        Imail_moderation clientModel = mail_moderationFeignClient.update(mail_moderation.getId(),(mail_moderationImpl)mail_moderation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_moderation.getClass(), false);
        copier.copy(clientModel, mail_moderation, null);
    }


    public void get(Imail_moderation mail_moderation){
        Imail_moderation clientModel = mail_moderationFeignClient.get(mail_moderation.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_moderation.getClass(), false);
        copier.copy(clientModel, mail_moderation, null);
    }


    public void create(Imail_moderation mail_moderation){
        Imail_moderation clientModel = mail_moderationFeignClient.create((mail_moderationImpl)mail_moderation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_moderation.getClass(), false);
        copier.copy(clientModel, mail_moderation, null);
    }


    public void createBatch(List<Imail_moderation> mail_moderations){
        if(mail_moderations!=null){
            List<mail_moderationImpl> list = new ArrayList<mail_moderationImpl>();
            for(Imail_moderation imail_moderation :mail_moderations){
                list.add((mail_moderationImpl)imail_moderation) ;
            }
            mail_moderationFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_moderation> mail_moderations){
        if(mail_moderations!=null){
            List<mail_moderationImpl> list = new ArrayList<mail_moderationImpl>();
            for(Imail_moderation imail_moderation :mail_moderations){
                list.add((mail_moderationImpl)imail_moderation) ;
            }
            mail_moderationFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_moderation> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_moderation mail_moderation){
        Imail_moderation clientModel = mail_moderationFeignClient.getDraft(mail_moderation.getId(),(mail_moderationImpl)mail_moderation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_moderation.getClass(), false);
        copier.copy(clientModel, mail_moderation, null);
    }



}

