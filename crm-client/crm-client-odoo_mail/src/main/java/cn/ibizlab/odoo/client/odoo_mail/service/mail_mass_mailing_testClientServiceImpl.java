package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_test;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_testClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_testImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailing_testFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing_test] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailing_testClientServiceImpl implements Imail_mass_mailing_testClientService {

    mail_mass_mailing_testFeignClient mail_mass_mailing_testFeignClient;

    @Autowired
    public mail_mass_mailing_testClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_testFeignClient = nameBuilder.target(mail_mass_mailing_testFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_testFeignClient = nameBuilder.target(mail_mass_mailing_testFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing_test createModel() {
		return new mail_mass_mailing_testImpl();
	}


    public void update(Imail_mass_mailing_test mail_mass_mailing_test){
        Imail_mass_mailing_test clientModel = mail_mass_mailing_testFeignClient.update(mail_mass_mailing_test.getId(),(mail_mass_mailing_testImpl)mail_mass_mailing_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_test.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_test, null);
    }


    public void create(Imail_mass_mailing_test mail_mass_mailing_test){
        Imail_mass_mailing_test clientModel = mail_mass_mailing_testFeignClient.create((mail_mass_mailing_testImpl)mail_mass_mailing_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_test.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_test, null);
    }


    public void createBatch(List<Imail_mass_mailing_test> mail_mass_mailing_tests){
        if(mail_mass_mailing_tests!=null){
            List<mail_mass_mailing_testImpl> list = new ArrayList<mail_mass_mailing_testImpl>();
            for(Imail_mass_mailing_test imail_mass_mailing_test :mail_mass_mailing_tests){
                list.add((mail_mass_mailing_testImpl)imail_mass_mailing_test) ;
            }
            mail_mass_mailing_testFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_mass_mailing_test> fetchDefault(SearchContext context){
        Page<mail_mass_mailing_testImpl> page = this.mail_mass_mailing_testFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imail_mass_mailing_test> mail_mass_mailing_tests){
        if(mail_mass_mailing_tests!=null){
            List<mail_mass_mailing_testImpl> list = new ArrayList<mail_mass_mailing_testImpl>();
            for(Imail_mass_mailing_test imail_mass_mailing_test :mail_mass_mailing_tests){
                list.add((mail_mass_mailing_testImpl)imail_mass_mailing_test) ;
            }
            mail_mass_mailing_testFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_mass_mailing_test> mail_mass_mailing_tests){
        if(mail_mass_mailing_tests!=null){
            List<mail_mass_mailing_testImpl> list = new ArrayList<mail_mass_mailing_testImpl>();
            for(Imail_mass_mailing_test imail_mass_mailing_test :mail_mass_mailing_tests){
                list.add((mail_mass_mailing_testImpl)imail_mass_mailing_test) ;
            }
            mail_mass_mailing_testFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imail_mass_mailing_test mail_mass_mailing_test){
        Imail_mass_mailing_test clientModel = mail_mass_mailing_testFeignClient.get(mail_mass_mailing_test.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_test.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_test, null);
    }


    public void remove(Imail_mass_mailing_test mail_mass_mailing_test){
        mail_mass_mailing_testFeignClient.remove(mail_mass_mailing_test.getId()) ;
    }


    public Page<Imail_mass_mailing_test> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing_test mail_mass_mailing_test){
        Imail_mass_mailing_test clientModel = mail_mass_mailing_testFeignClient.getDraft(mail_mass_mailing_test.getId(),(mail_mass_mailing_testImpl)mail_mass_mailing_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_test.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_test, null);
    }



}

