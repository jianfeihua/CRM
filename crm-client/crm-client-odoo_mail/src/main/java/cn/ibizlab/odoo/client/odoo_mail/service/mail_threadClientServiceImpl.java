package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_thread;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_threadClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_threadImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_threadFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_thread] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_threadClientServiceImpl implements Imail_threadClientService {

    mail_threadFeignClient mail_threadFeignClient;

    @Autowired
    public mail_threadClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_threadFeignClient = nameBuilder.target(mail_threadFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_threadFeignClient = nameBuilder.target(mail_threadFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_thread createModel() {
		return new mail_threadImpl();
	}


    public void updateBatch(List<Imail_thread> mail_threads){
        if(mail_threads!=null){
            List<mail_threadImpl> list = new ArrayList<mail_threadImpl>();
            for(Imail_thread imail_thread :mail_threads){
                list.add((mail_threadImpl)imail_thread) ;
            }
            mail_threadFeignClient.updateBatch(list) ;
        }
    }


    public void update(Imail_thread mail_thread){
        Imail_thread clientModel = mail_threadFeignClient.update(mail_thread.getId(),(mail_threadImpl)mail_thread) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_thread.getClass(), false);
        copier.copy(clientModel, mail_thread, null);
    }


    public void get(Imail_thread mail_thread){
        Imail_thread clientModel = mail_threadFeignClient.get(mail_thread.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_thread.getClass(), false);
        copier.copy(clientModel, mail_thread, null);
    }


    public void remove(Imail_thread mail_thread){
        mail_threadFeignClient.remove(mail_thread.getId()) ;
    }


    public void createBatch(List<Imail_thread> mail_threads){
        if(mail_threads!=null){
            List<mail_threadImpl> list = new ArrayList<mail_threadImpl>();
            for(Imail_thread imail_thread :mail_threads){
                list.add((mail_threadImpl)imail_thread) ;
            }
            mail_threadFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_thread> mail_threads){
        if(mail_threads!=null){
            List<mail_threadImpl> list = new ArrayList<mail_threadImpl>();
            for(Imail_thread imail_thread :mail_threads){
                list.add((mail_threadImpl)imail_thread) ;
            }
            mail_threadFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_thread> fetchDefault(SearchContext context){
        Page<mail_threadImpl> page = this.mail_threadFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Imail_thread mail_thread){
        Imail_thread clientModel = mail_threadFeignClient.create((mail_threadImpl)mail_thread) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_thread.getClass(), false);
        copier.copy(clientModel, mail_thread, null);
    }


    public Page<Imail_thread> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_thread mail_thread){
        Imail_thread clientModel = mail_threadFeignClient.getDraft(mail_thread.getId(),(mail_threadImpl)mail_thread) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_thread.getClass(), false);
        copier.copy(clientModel, mail_thread, null);
    }



}

