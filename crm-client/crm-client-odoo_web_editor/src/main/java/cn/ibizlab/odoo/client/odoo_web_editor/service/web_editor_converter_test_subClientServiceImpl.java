package cn.ibizlab.odoo.client.odoo_web_editor.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test_sub;
import cn.ibizlab.odoo.client.odoo_web_editor.config.odoo_web_editorClientProperties;
import cn.ibizlab.odoo.core.client.service.Iweb_editor_converter_test_subClientService;
import cn.ibizlab.odoo.client.odoo_web_editor.model.web_editor_converter_test_subImpl;
import cn.ibizlab.odoo.client.odoo_web_editor.feign.web_editor_converter_test_subFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[web_editor_converter_test_sub] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class web_editor_converter_test_subClientServiceImpl implements Iweb_editor_converter_test_subClientService {

    web_editor_converter_test_subFeignClient web_editor_converter_test_subFeignClient;

    @Autowired
    public web_editor_converter_test_subClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_web_editorClientProperties odoo_web_editorClientProperties) {
        if (odoo_web_editorClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.web_editor_converter_test_subFeignClient = nameBuilder.target(web_editor_converter_test_subFeignClient.class,"http://"+odoo_web_editorClientProperties.getServiceId()+"/") ;
		}else if (odoo_web_editorClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.web_editor_converter_test_subFeignClient = nameBuilder.target(web_editor_converter_test_subFeignClient.class,odoo_web_editorClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iweb_editor_converter_test_sub createModel() {
		return new web_editor_converter_test_subImpl();
	}


    public void remove(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
        web_editor_converter_test_subFeignClient.remove(web_editor_converter_test_sub.getId()) ;
    }


    public void create(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
        Iweb_editor_converter_test_sub clientModel = web_editor_converter_test_subFeignClient.create((web_editor_converter_test_subImpl)web_editor_converter_test_sub) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test_sub.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test_sub, null);
    }


    public Page<Iweb_editor_converter_test_sub> fetchDefault(SearchContext context){
        Page<web_editor_converter_test_subImpl> page = this.web_editor_converter_test_subFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs){
        if(web_editor_converter_test_subs!=null){
            List<web_editor_converter_test_subImpl> list = new ArrayList<web_editor_converter_test_subImpl>();
            for(Iweb_editor_converter_test_sub iweb_editor_converter_test_sub :web_editor_converter_test_subs){
                list.add((web_editor_converter_test_subImpl)iweb_editor_converter_test_sub) ;
            }
            web_editor_converter_test_subFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs){
        if(web_editor_converter_test_subs!=null){
            List<web_editor_converter_test_subImpl> list = new ArrayList<web_editor_converter_test_subImpl>();
            for(Iweb_editor_converter_test_sub iweb_editor_converter_test_sub :web_editor_converter_test_subs){
                list.add((web_editor_converter_test_subImpl)iweb_editor_converter_test_sub) ;
            }
            web_editor_converter_test_subFeignClient.createBatch(list) ;
        }
    }


    public void update(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
        Iweb_editor_converter_test_sub clientModel = web_editor_converter_test_subFeignClient.update(web_editor_converter_test_sub.getId(),(web_editor_converter_test_subImpl)web_editor_converter_test_sub) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test_sub.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test_sub, null);
    }


    public void removeBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs){
        if(web_editor_converter_test_subs!=null){
            List<web_editor_converter_test_subImpl> list = new ArrayList<web_editor_converter_test_subImpl>();
            for(Iweb_editor_converter_test_sub iweb_editor_converter_test_sub :web_editor_converter_test_subs){
                list.add((web_editor_converter_test_subImpl)iweb_editor_converter_test_sub) ;
            }
            web_editor_converter_test_subFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
        Iweb_editor_converter_test_sub clientModel = web_editor_converter_test_subFeignClient.get(web_editor_converter_test_sub.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test_sub.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test_sub, null);
    }


    public Page<Iweb_editor_converter_test_sub> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
        Iweb_editor_converter_test_sub clientModel = web_editor_converter_test_subFeignClient.getDraft(web_editor_converter_test_sub.getId(),(web_editor_converter_test_subImpl)web_editor_converter_test_sub) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test_sub.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test_sub, null);
    }



}

