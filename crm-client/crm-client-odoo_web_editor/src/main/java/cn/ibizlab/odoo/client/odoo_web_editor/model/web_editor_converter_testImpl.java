package cn.ibizlab.odoo.client.odoo_web_editor.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[web_editor_converter_test] 对象
 */
public class web_editor_converter_testImpl implements Iweb_editor_converter_test,Serializable{

    /**
     * Binary
     */
    public byte[] binary;

    @JsonIgnore
    public boolean binaryDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * Datetime
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp datetime;

    @JsonIgnore
    public boolean datetimeDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * Html
     */
    public String html;

    @JsonIgnore
    public boolean htmlDirtyFlag;
    
    /**
     * Char
     */
    public String ibizchar;

    @JsonIgnore
    public boolean ibizcharDirtyFlag;
    
    /**
     * 浮点数
     */
    public Double ibizfloat;

    @JsonIgnore
    public boolean ibizfloatDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * Integer
     */
    public Integer integer;

    @JsonIgnore
    public boolean integerDirtyFlag;
    
    /**
     * Many2One
     */
    public Integer many2one;

    @JsonIgnore
    public boolean many2oneDirtyFlag;
    
    /**
     * Many2One
     */
    public String many2one_text;

    @JsonIgnore
    public boolean many2one_textDirtyFlag;
    
    /**
     * Numeric
     */
    public Double numeric;

    @JsonIgnore
    public boolean numericDirtyFlag;
    
    /**
     * Selection
     */
    public String selection;

    @JsonIgnore
    public boolean selectionDirtyFlag;
    
    /**
     * Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:
     */
    public String selection_str;

    @JsonIgnore
    public boolean selection_strDirtyFlag;
    
    /**
     * Text
     */
    public String text;

    @JsonIgnore
    public boolean textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [Binary]
     */
    @JsonProperty("binary")
    public byte[] getBinary(){
        return this.binary ;
    }

    /**
     * 设置 [Binary]
     */
    @JsonProperty("binary")
    public void setBinary(byte[]  binary){
        this.binary = binary ;
        this.binaryDirtyFlag = true ;
    }

     /**
     * 获取 [Binary]脏标记
     */
    @JsonIgnore
    public boolean getBinaryDirtyFlag(){
        return this.binaryDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [Datetime]
     */
    @JsonProperty("datetime")
    public Timestamp getDatetime(){
        return this.datetime ;
    }

    /**
     * 设置 [Datetime]
     */
    @JsonProperty("datetime")
    public void setDatetime(Timestamp  datetime){
        this.datetime = datetime ;
        this.datetimeDirtyFlag = true ;
    }

     /**
     * 获取 [Datetime]脏标记
     */
    @JsonIgnore
    public boolean getDatetimeDirtyFlag(){
        return this.datetimeDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [Html]
     */
    @JsonProperty("html")
    public String getHtml(){
        return this.html ;
    }

    /**
     * 设置 [Html]
     */
    @JsonProperty("html")
    public void setHtml(String  html){
        this.html = html ;
        this.htmlDirtyFlag = true ;
    }

     /**
     * 获取 [Html]脏标记
     */
    @JsonIgnore
    public boolean getHtmlDirtyFlag(){
        return this.htmlDirtyFlag ;
    }   

    /**
     * 获取 [Char]
     */
    @JsonProperty("ibizchar")
    public String getIbizchar(){
        return this.ibizchar ;
    }

    /**
     * 设置 [Char]
     */
    @JsonProperty("ibizchar")
    public void setIbizchar(String  ibizchar){
        this.ibizchar = ibizchar ;
        this.ibizcharDirtyFlag = true ;
    }

     /**
     * 获取 [Char]脏标记
     */
    @JsonIgnore
    public boolean getIbizcharDirtyFlag(){
        return this.ibizcharDirtyFlag ;
    }   

    /**
     * 获取 [浮点数]
     */
    @JsonProperty("ibizfloat")
    public Double getIbizfloat(){
        return this.ibizfloat ;
    }

    /**
     * 设置 [浮点数]
     */
    @JsonProperty("ibizfloat")
    public void setIbizfloat(Double  ibizfloat){
        this.ibizfloat = ibizfloat ;
        this.ibizfloatDirtyFlag = true ;
    }

     /**
     * 获取 [浮点数]脏标记
     */
    @JsonIgnore
    public boolean getIbizfloatDirtyFlag(){
        return this.ibizfloatDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [Integer]
     */
    @JsonProperty("integer")
    public Integer getInteger(){
        return this.integer ;
    }

    /**
     * 设置 [Integer]
     */
    @JsonProperty("integer")
    public void setInteger(Integer  integer){
        this.integer = integer ;
        this.integerDirtyFlag = true ;
    }

     /**
     * 获取 [Integer]脏标记
     */
    @JsonIgnore
    public boolean getIntegerDirtyFlag(){
        return this.integerDirtyFlag ;
    }   

    /**
     * 获取 [Many2One]
     */
    @JsonProperty("many2one")
    public Integer getMany2one(){
        return this.many2one ;
    }

    /**
     * 设置 [Many2One]
     */
    @JsonProperty("many2one")
    public void setMany2one(Integer  many2one){
        this.many2one = many2one ;
        this.many2oneDirtyFlag = true ;
    }

     /**
     * 获取 [Many2One]脏标记
     */
    @JsonIgnore
    public boolean getMany2oneDirtyFlag(){
        return this.many2oneDirtyFlag ;
    }   

    /**
     * 获取 [Many2One]
     */
    @JsonProperty("many2one_text")
    public String getMany2one_text(){
        return this.many2one_text ;
    }

    /**
     * 设置 [Many2One]
     */
    @JsonProperty("many2one_text")
    public void setMany2one_text(String  many2one_text){
        this.many2one_text = many2one_text ;
        this.many2one_textDirtyFlag = true ;
    }

     /**
     * 获取 [Many2One]脏标记
     */
    @JsonIgnore
    public boolean getMany2one_textDirtyFlag(){
        return this.many2one_textDirtyFlag ;
    }   

    /**
     * 获取 [Numeric]
     */
    @JsonProperty("numeric")
    public Double getNumeric(){
        return this.numeric ;
    }

    /**
     * 设置 [Numeric]
     */
    @JsonProperty("numeric")
    public void setNumeric(Double  numeric){
        this.numeric = numeric ;
        this.numericDirtyFlag = true ;
    }

     /**
     * 获取 [Numeric]脏标记
     */
    @JsonIgnore
    public boolean getNumericDirtyFlag(){
        return this.numericDirtyFlag ;
    }   

    /**
     * 获取 [Selection]
     */
    @JsonProperty("selection")
    public String getSelection(){
        return this.selection ;
    }

    /**
     * 设置 [Selection]
     */
    @JsonProperty("selection")
    public void setSelection(String  selection){
        this.selection = selection ;
        this.selectionDirtyFlag = true ;
    }

     /**
     * 获取 [Selection]脏标记
     */
    @JsonIgnore
    public boolean getSelectionDirtyFlag(){
        return this.selectionDirtyFlag ;
    }   

    /**
     * 获取 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]
     */
    @JsonProperty("selection_str")
    public String getSelection_str(){
        return this.selection_str ;
    }

    /**
     * 设置 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]
     */
    @JsonProperty("selection_str")
    public void setSelection_str(String  selection_str){
        this.selection_str = selection_str ;
        this.selection_strDirtyFlag = true ;
    }

     /**
     * 获取 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]脏标记
     */
    @JsonIgnore
    public boolean getSelection_strDirtyFlag(){
        return this.selection_strDirtyFlag ;
    }   

    /**
     * 获取 [Text]
     */
    @JsonProperty("text")
    public String getText(){
        return this.text ;
    }

    /**
     * 设置 [Text]
     */
    @JsonProperty("text")
    public void setText(String  text){
        this.text = text ;
        this.textDirtyFlag = true ;
    }

     /**
     * 获取 [Text]脏标记
     */
    @JsonIgnore
    public boolean getTextDirtyFlag(){
        return this.textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
