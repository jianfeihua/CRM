package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_char_stillreadonlyClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_stillreadonlyImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_char_stillreadonlyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_char_stillreadonly] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_char_stillreadonlyClientServiceImpl implements Ibase_import_tests_models_char_stillreadonlyClientService {

    base_import_tests_models_char_stillreadonlyFeignClient base_import_tests_models_char_stillreadonlyFeignClient;

    @Autowired
    public base_import_tests_models_char_stillreadonlyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_stillreadonlyFeignClient = nameBuilder.target(base_import_tests_models_char_stillreadonlyFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_stillreadonlyFeignClient = nameBuilder.target(base_import_tests_models_char_stillreadonlyFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_char_stillreadonly createModel() {
		return new base_import_tests_models_char_stillreadonlyImpl();
	}


    public void remove(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
        base_import_tests_models_char_stillreadonlyFeignClient.remove(base_import_tests_models_char_stillreadonly.getId()) ;
    }


    public void get(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
        Ibase_import_tests_models_char_stillreadonly clientModel = base_import_tests_models_char_stillreadonlyFeignClient.get(base_import_tests_models_char_stillreadonly.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_stillreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_stillreadonly, null);
    }


    public void create(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
        Ibase_import_tests_models_char_stillreadonly clientModel = base_import_tests_models_char_stillreadonlyFeignClient.create((base_import_tests_models_char_stillreadonlyImpl)base_import_tests_models_char_stillreadonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_stillreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_stillreadonly, null);
    }


    public void removeBatch(List<Ibase_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
        if(base_import_tests_models_char_stillreadonlies!=null){
            List<base_import_tests_models_char_stillreadonlyImpl> list = new ArrayList<base_import_tests_models_char_stillreadonlyImpl>();
            for(Ibase_import_tests_models_char_stillreadonly ibase_import_tests_models_char_stillreadonly :base_import_tests_models_char_stillreadonlies){
                list.add((base_import_tests_models_char_stillreadonlyImpl)ibase_import_tests_models_char_stillreadonly) ;
            }
            base_import_tests_models_char_stillreadonlyFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ibase_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
        if(base_import_tests_models_char_stillreadonlies!=null){
            List<base_import_tests_models_char_stillreadonlyImpl> list = new ArrayList<base_import_tests_models_char_stillreadonlyImpl>();
            for(Ibase_import_tests_models_char_stillreadonly ibase_import_tests_models_char_stillreadonly :base_import_tests_models_char_stillreadonlies){
                list.add((base_import_tests_models_char_stillreadonlyImpl)ibase_import_tests_models_char_stillreadonly) ;
            }
            base_import_tests_models_char_stillreadonlyFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_char_stillreadonly> fetchDefault(SearchContext context){
        Page<base_import_tests_models_char_stillreadonlyImpl> page = this.base_import_tests_models_char_stillreadonlyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ibase_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
        if(base_import_tests_models_char_stillreadonlies!=null){
            List<base_import_tests_models_char_stillreadonlyImpl> list = new ArrayList<base_import_tests_models_char_stillreadonlyImpl>();
            for(Ibase_import_tests_models_char_stillreadonly ibase_import_tests_models_char_stillreadonly :base_import_tests_models_char_stillreadonlies){
                list.add((base_import_tests_models_char_stillreadonlyImpl)ibase_import_tests_models_char_stillreadonly) ;
            }
            base_import_tests_models_char_stillreadonlyFeignClient.createBatch(list) ;
        }
    }


    public void update(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
        Ibase_import_tests_models_char_stillreadonly clientModel = base_import_tests_models_char_stillreadonlyFeignClient.update(base_import_tests_models_char_stillreadonly.getId(),(base_import_tests_models_char_stillreadonlyImpl)base_import_tests_models_char_stillreadonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_stillreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_stillreadonly, null);
    }


    public Page<Ibase_import_tests_models_char_stillreadonly> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
        Ibase_import_tests_models_char_stillreadonly clientModel = base_import_tests_models_char_stillreadonlyFeignClient.getDraft(base_import_tests_models_char_stillreadonly.getId(),(base_import_tests_models_char_stillreadonlyImpl)base_import_tests_models_char_stillreadonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_stillreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_stillreadonly, null);
    }



}

