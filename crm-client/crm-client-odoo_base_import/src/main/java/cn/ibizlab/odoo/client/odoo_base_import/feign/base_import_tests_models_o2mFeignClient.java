package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_o2m;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_o2mImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_o2m] 服务对象接口
 */
public interface base_import_tests_models_o2mFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2ms/{id}")
    public base_import_tests_models_o2mImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_o2mImpl base_import_tests_models_o2m);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2ms/{id}")
    public base_import_tests_models_o2mImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2ms/fetchdefault")
    public Page<base_import_tests_models_o2mImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2ms")
    public base_import_tests_models_o2mImpl create(@RequestBody base_import_tests_models_o2mImpl base_import_tests_models_o2m);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2ms/updatebatch")
    public base_import_tests_models_o2mImpl updateBatch(@RequestBody List<base_import_tests_models_o2mImpl> base_import_tests_models_o2ms);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2ms/createbatch")
    public base_import_tests_models_o2mImpl createBatch(@RequestBody List<base_import_tests_models_o2mImpl> base_import_tests_models_o2ms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2ms/removebatch")
    public base_import_tests_models_o2mImpl removeBatch(@RequestBody List<base_import_tests_models_o2mImpl> base_import_tests_models_o2ms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2ms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2ms/select")
    public Page<base_import_tests_models_o2mImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2ms/{id}/getdraft")
    public base_import_tests_models_o2mImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_o2mImpl base_import_tests_models_o2m);



}
