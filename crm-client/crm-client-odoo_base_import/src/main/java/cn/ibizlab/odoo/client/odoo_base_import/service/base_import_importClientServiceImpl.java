package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_import;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_importClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_importImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_importFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_import] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_importClientServiceImpl implements Ibase_import_importClientService {

    base_import_importFeignClient base_import_importFeignClient;

    @Autowired
    public base_import_importClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_importFeignClient = nameBuilder.target(base_import_importFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_importFeignClient = nameBuilder.target(base_import_importFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_import createModel() {
		return new base_import_importImpl();
	}


    public void remove(Ibase_import_import base_import_import){
        base_import_importFeignClient.remove(base_import_import.getId()) ;
    }


    public Page<Ibase_import_import> fetchDefault(SearchContext context){
        Page<base_import_importImpl> page = this.base_import_importFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ibase_import_import base_import_import){
        Ibase_import_import clientModel = base_import_importFeignClient.update(base_import_import.getId(),(base_import_importImpl)base_import_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_import.getClass(), false);
        copier.copy(clientModel, base_import_import, null);
    }


    public void create(Ibase_import_import base_import_import){
        Ibase_import_import clientModel = base_import_importFeignClient.create((base_import_importImpl)base_import_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_import.getClass(), false);
        copier.copy(clientModel, base_import_import, null);
    }


    public void get(Ibase_import_import base_import_import){
        Ibase_import_import clientModel = base_import_importFeignClient.get(base_import_import.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_import.getClass(), false);
        copier.copy(clientModel, base_import_import, null);
    }


    public void updateBatch(List<Ibase_import_import> base_import_imports){
        if(base_import_imports!=null){
            List<base_import_importImpl> list = new ArrayList<base_import_importImpl>();
            for(Ibase_import_import ibase_import_import :base_import_imports){
                list.add((base_import_importImpl)ibase_import_import) ;
            }
            base_import_importFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_import> base_import_imports){
        if(base_import_imports!=null){
            List<base_import_importImpl> list = new ArrayList<base_import_importImpl>();
            for(Ibase_import_import ibase_import_import :base_import_imports){
                list.add((base_import_importImpl)ibase_import_import) ;
            }
            base_import_importFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_import_import> base_import_imports){
        if(base_import_imports!=null){
            List<base_import_importImpl> list = new ArrayList<base_import_importImpl>();
            for(Ibase_import_import ibase_import_import :base_import_imports){
                list.add((base_import_importImpl)ibase_import_import) ;
            }
            base_import_importFeignClient.createBatch(list) ;
        }
    }


    public Page<Ibase_import_import> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_import base_import_import){
        Ibase_import_import clientModel = base_import_importFeignClient.getDraft(base_import_import.getId(),(base_import_importImpl)base_import_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_import.getClass(), false);
        copier.copy(clientModel, base_import_import, null);
    }



}

