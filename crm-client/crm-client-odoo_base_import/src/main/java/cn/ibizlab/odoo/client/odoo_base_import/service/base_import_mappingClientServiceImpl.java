package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_mapping;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_mappingClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_mappingImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_mappingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_mapping] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_mappingClientServiceImpl implements Ibase_import_mappingClientService {

    base_import_mappingFeignClient base_import_mappingFeignClient;

    @Autowired
    public base_import_mappingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_mappingFeignClient = nameBuilder.target(base_import_mappingFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_mappingFeignClient = nameBuilder.target(base_import_mappingFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_mapping createModel() {
		return new base_import_mappingImpl();
	}


    public void update(Ibase_import_mapping base_import_mapping){
        Ibase_import_mapping clientModel = base_import_mappingFeignClient.update(base_import_mapping.getId(),(base_import_mappingImpl)base_import_mapping) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_mapping.getClass(), false);
        copier.copy(clientModel, base_import_mapping, null);
    }


    public void createBatch(List<Ibase_import_mapping> base_import_mappings){
        if(base_import_mappings!=null){
            List<base_import_mappingImpl> list = new ArrayList<base_import_mappingImpl>();
            for(Ibase_import_mapping ibase_import_mapping :base_import_mappings){
                list.add((base_import_mappingImpl)ibase_import_mapping) ;
            }
            base_import_mappingFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_mapping> base_import_mappings){
        if(base_import_mappings!=null){
            List<base_import_mappingImpl> list = new ArrayList<base_import_mappingImpl>();
            for(Ibase_import_mapping ibase_import_mapping :base_import_mappings){
                list.add((base_import_mappingImpl)ibase_import_mapping) ;
            }
            base_import_mappingFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ibase_import_mapping base_import_mapping){
        base_import_mappingFeignClient.remove(base_import_mapping.getId()) ;
    }


    public void updateBatch(List<Ibase_import_mapping> base_import_mappings){
        if(base_import_mappings!=null){
            List<base_import_mappingImpl> list = new ArrayList<base_import_mappingImpl>();
            for(Ibase_import_mapping ibase_import_mapping :base_import_mappings){
                list.add((base_import_mappingImpl)ibase_import_mapping) ;
            }
            base_import_mappingFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ibase_import_mapping base_import_mapping){
        Ibase_import_mapping clientModel = base_import_mappingFeignClient.create((base_import_mappingImpl)base_import_mapping) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_mapping.getClass(), false);
        copier.copy(clientModel, base_import_mapping, null);
    }


    public void get(Ibase_import_mapping base_import_mapping){
        Ibase_import_mapping clientModel = base_import_mappingFeignClient.get(base_import_mapping.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_mapping.getClass(), false);
        copier.copy(clientModel, base_import_mapping, null);
    }


    public Page<Ibase_import_mapping> fetchDefault(SearchContext context){
        Page<base_import_mappingImpl> page = this.base_import_mappingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ibase_import_mapping> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_mapping base_import_mapping){
        Ibase_import_mapping clientModel = base_import_mappingFeignClient.getDraft(base_import_mapping.getId(),(base_import_mappingImpl)base_import_mapping) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_mapping.getClass(), false);
        copier.copy(clientModel, base_import_mapping, null);
    }



}

