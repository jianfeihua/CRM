package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_charClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_charImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_charFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_char] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_charClientServiceImpl implements Ibase_import_tests_models_charClientService {

    base_import_tests_models_charFeignClient base_import_tests_models_charFeignClient;

    @Autowired
    public base_import_tests_models_charClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_charFeignClient = nameBuilder.target(base_import_tests_models_charFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_charFeignClient = nameBuilder.target(base_import_tests_models_charFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_char createModel() {
		return new base_import_tests_models_charImpl();
	}


    public void create(Ibase_import_tests_models_char base_import_tests_models_char){
        Ibase_import_tests_models_char clientModel = base_import_tests_models_charFeignClient.create((base_import_tests_models_charImpl)base_import_tests_models_char) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char, null);
    }


    public void createBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars){
        if(base_import_tests_models_chars!=null){
            List<base_import_tests_models_charImpl> list = new ArrayList<base_import_tests_models_charImpl>();
            for(Ibase_import_tests_models_char ibase_import_tests_models_char :base_import_tests_models_chars){
                list.add((base_import_tests_models_charImpl)ibase_import_tests_models_char) ;
            }
            base_import_tests_models_charFeignClient.createBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_char> fetchDefault(SearchContext context){
        Page<base_import_tests_models_charImpl> page = this.base_import_tests_models_charFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ibase_import_tests_models_char base_import_tests_models_char){
        base_import_tests_models_charFeignClient.remove(base_import_tests_models_char.getId()) ;
    }


    public void get(Ibase_import_tests_models_char base_import_tests_models_char){
        Ibase_import_tests_models_char clientModel = base_import_tests_models_charFeignClient.get(base_import_tests_models_char.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char, null);
    }


    public void removeBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars){
        if(base_import_tests_models_chars!=null){
            List<base_import_tests_models_charImpl> list = new ArrayList<base_import_tests_models_charImpl>();
            for(Ibase_import_tests_models_char ibase_import_tests_models_char :base_import_tests_models_chars){
                list.add((base_import_tests_models_charImpl)ibase_import_tests_models_char) ;
            }
            base_import_tests_models_charFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ibase_import_tests_models_char base_import_tests_models_char){
        Ibase_import_tests_models_char clientModel = base_import_tests_models_charFeignClient.update(base_import_tests_models_char.getId(),(base_import_tests_models_charImpl)base_import_tests_models_char) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char, null);
    }


    public void updateBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars){
        if(base_import_tests_models_chars!=null){
            List<base_import_tests_models_charImpl> list = new ArrayList<base_import_tests_models_charImpl>();
            for(Ibase_import_tests_models_char ibase_import_tests_models_char :base_import_tests_models_chars){
                list.add((base_import_tests_models_charImpl)ibase_import_tests_models_char) ;
            }
            base_import_tests_models_charFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_char> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_char base_import_tests_models_char){
        Ibase_import_tests_models_char clientModel = base_import_tests_models_charFeignClient.getDraft(base_import_tests_models_char.getId(),(base_import_tests_models_charImpl)base_import_tests_models_char) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char, null);
    }



}

