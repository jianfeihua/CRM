package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_o2m;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_o2mClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_o2mImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_o2mFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_o2m] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_o2mClientServiceImpl implements Ibase_import_tests_models_o2mClientService {

    base_import_tests_models_o2mFeignClient base_import_tests_models_o2mFeignClient;

    @Autowired
    public base_import_tests_models_o2mClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_o2mFeignClient = nameBuilder.target(base_import_tests_models_o2mFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_o2mFeignClient = nameBuilder.target(base_import_tests_models_o2mFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_o2m createModel() {
		return new base_import_tests_models_o2mImpl();
	}


    public void update(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
        Ibase_import_tests_models_o2m clientModel = base_import_tests_models_o2mFeignClient.update(base_import_tests_models_o2m.getId(),(base_import_tests_models_o2mImpl)base_import_tests_models_o2m) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m, null);
    }


    public void get(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
        Ibase_import_tests_models_o2m clientModel = base_import_tests_models_o2mFeignClient.get(base_import_tests_models_o2m.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m, null);
    }


    public Page<Ibase_import_tests_models_o2m> fetchDefault(SearchContext context){
        Page<base_import_tests_models_o2mImpl> page = this.base_import_tests_models_o2mFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
        Ibase_import_tests_models_o2m clientModel = base_import_tests_models_o2mFeignClient.create((base_import_tests_models_o2mImpl)base_import_tests_models_o2m) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m, null);
    }


    public void updateBatch(List<Ibase_import_tests_models_o2m> base_import_tests_models_o2ms){
        if(base_import_tests_models_o2ms!=null){
            List<base_import_tests_models_o2mImpl> list = new ArrayList<base_import_tests_models_o2mImpl>();
            for(Ibase_import_tests_models_o2m ibase_import_tests_models_o2m :base_import_tests_models_o2ms){
                list.add((base_import_tests_models_o2mImpl)ibase_import_tests_models_o2m) ;
            }
            base_import_tests_models_o2mFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_import_tests_models_o2m> base_import_tests_models_o2ms){
        if(base_import_tests_models_o2ms!=null){
            List<base_import_tests_models_o2mImpl> list = new ArrayList<base_import_tests_models_o2mImpl>();
            for(Ibase_import_tests_models_o2m ibase_import_tests_models_o2m :base_import_tests_models_o2ms){
                list.add((base_import_tests_models_o2mImpl)ibase_import_tests_models_o2m) ;
            }
            base_import_tests_models_o2mFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_tests_models_o2m> base_import_tests_models_o2ms){
        if(base_import_tests_models_o2ms!=null){
            List<base_import_tests_models_o2mImpl> list = new ArrayList<base_import_tests_models_o2mImpl>();
            for(Ibase_import_tests_models_o2m ibase_import_tests_models_o2m :base_import_tests_models_o2ms){
                list.add((base_import_tests_models_o2mImpl)ibase_import_tests_models_o2m) ;
            }
            base_import_tests_models_o2mFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
        base_import_tests_models_o2mFeignClient.remove(base_import_tests_models_o2m.getId()) ;
    }


    public Page<Ibase_import_tests_models_o2m> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
        Ibase_import_tests_models_o2m clientModel = base_import_tests_models_o2mFeignClient.getDraft(base_import_tests_models_o2m.getId(),(base_import_tests_models_o2mImpl)base_import_tests_models_o2m) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m, null);
    }



}

