package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_states;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_char_statesClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_statesImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_char_statesFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_char_states] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_char_statesClientServiceImpl implements Ibase_import_tests_models_char_statesClientService {

    base_import_tests_models_char_statesFeignClient base_import_tests_models_char_statesFeignClient;

    @Autowired
    public base_import_tests_models_char_statesClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_statesFeignClient = nameBuilder.target(base_import_tests_models_char_statesFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_statesFeignClient = nameBuilder.target(base_import_tests_models_char_statesFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_char_states createModel() {
		return new base_import_tests_models_char_statesImpl();
	}


    public void updateBatch(List<Ibase_import_tests_models_char_states> base_import_tests_models_char_states){
        if(base_import_tests_models_char_states!=null){
            List<base_import_tests_models_char_statesImpl> list = new ArrayList<base_import_tests_models_char_statesImpl>();
            for(Ibase_import_tests_models_char_states ibase_import_tests_models_char_states :base_import_tests_models_char_states){
                list.add((base_import_tests_models_char_statesImpl)ibase_import_tests_models_char_states) ;
            }
            base_import_tests_models_char_statesFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_tests_models_char_states> base_import_tests_models_char_states){
        if(base_import_tests_models_char_states!=null){
            List<base_import_tests_models_char_statesImpl> list = new ArrayList<base_import_tests_models_char_statesImpl>();
            for(Ibase_import_tests_models_char_states ibase_import_tests_models_char_states :base_import_tests_models_char_states){
                list.add((base_import_tests_models_char_statesImpl)ibase_import_tests_models_char_states) ;
            }
            base_import_tests_models_char_statesFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_import_tests_models_char_states> base_import_tests_models_char_states){
        if(base_import_tests_models_char_states!=null){
            List<base_import_tests_models_char_statesImpl> list = new ArrayList<base_import_tests_models_char_statesImpl>();
            for(Ibase_import_tests_models_char_states ibase_import_tests_models_char_states :base_import_tests_models_char_states){
                list.add((base_import_tests_models_char_statesImpl)ibase_import_tests_models_char_states) ;
            }
            base_import_tests_models_char_statesFeignClient.createBatch(list) ;
        }
    }


    public void create(Ibase_import_tests_models_char_states base_import_tests_models_char_states){
        Ibase_import_tests_models_char_states clientModel = base_import_tests_models_char_statesFeignClient.create((base_import_tests_models_char_statesImpl)base_import_tests_models_char_states) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_states.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_states, null);
    }


    public void update(Ibase_import_tests_models_char_states base_import_tests_models_char_states){
        Ibase_import_tests_models_char_states clientModel = base_import_tests_models_char_statesFeignClient.update(base_import_tests_models_char_states.getId(),(base_import_tests_models_char_statesImpl)base_import_tests_models_char_states) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_states.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_states, null);
    }


    public void get(Ibase_import_tests_models_char_states base_import_tests_models_char_states){
        Ibase_import_tests_models_char_states clientModel = base_import_tests_models_char_statesFeignClient.get(base_import_tests_models_char_states.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_states.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_states, null);
    }


    public void remove(Ibase_import_tests_models_char_states base_import_tests_models_char_states){
        base_import_tests_models_char_statesFeignClient.remove(base_import_tests_models_char_states.getId()) ;
    }


    public Page<Ibase_import_tests_models_char_states> fetchDefault(SearchContext context){
        Page<base_import_tests_models_char_statesImpl> page = this.base_import_tests_models_char_statesFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ibase_import_tests_models_char_states> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_char_states base_import_tests_models_char_states){
        Ibase_import_tests_models_char_states clientModel = base_import_tests_models_char_statesFeignClient.getDraft(base_import_tests_models_char_states.getId(),(base_import_tests_models_char_statesImpl)base_import_tests_models_char_states) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_states.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_states, null);
    }



}

