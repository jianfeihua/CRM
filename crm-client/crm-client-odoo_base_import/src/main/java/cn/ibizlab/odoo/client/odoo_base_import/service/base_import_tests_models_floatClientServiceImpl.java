package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_float;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_floatClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_floatImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_floatFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_float] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_floatClientServiceImpl implements Ibase_import_tests_models_floatClientService {

    base_import_tests_models_floatFeignClient base_import_tests_models_floatFeignClient;

    @Autowired
    public base_import_tests_models_floatClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_floatFeignClient = nameBuilder.target(base_import_tests_models_floatFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_floatFeignClient = nameBuilder.target(base_import_tests_models_floatFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_float createModel() {
		return new base_import_tests_models_floatImpl();
	}


    public void create(Ibase_import_tests_models_float base_import_tests_models_float){
        Ibase_import_tests_models_float clientModel = base_import_tests_models_floatFeignClient.create((base_import_tests_models_floatImpl)base_import_tests_models_float) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_float.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_float, null);
    }


    public void remove(Ibase_import_tests_models_float base_import_tests_models_float){
        base_import_tests_models_floatFeignClient.remove(base_import_tests_models_float.getId()) ;
    }


    public void get(Ibase_import_tests_models_float base_import_tests_models_float){
        Ibase_import_tests_models_float clientModel = base_import_tests_models_floatFeignClient.get(base_import_tests_models_float.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_float.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_float, null);
    }


    public void createBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats){
        if(base_import_tests_models_floats!=null){
            List<base_import_tests_models_floatImpl> list = new ArrayList<base_import_tests_models_floatImpl>();
            for(Ibase_import_tests_models_float ibase_import_tests_models_float :base_import_tests_models_floats){
                list.add((base_import_tests_models_floatImpl)ibase_import_tests_models_float) ;
            }
            base_import_tests_models_floatFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats){
        if(base_import_tests_models_floats!=null){
            List<base_import_tests_models_floatImpl> list = new ArrayList<base_import_tests_models_floatImpl>();
            for(Ibase_import_tests_models_float ibase_import_tests_models_float :base_import_tests_models_floats){
                list.add((base_import_tests_models_floatImpl)ibase_import_tests_models_float) ;
            }
            base_import_tests_models_floatFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ibase_import_tests_models_float base_import_tests_models_float){
        Ibase_import_tests_models_float clientModel = base_import_tests_models_floatFeignClient.update(base_import_tests_models_float.getId(),(base_import_tests_models_floatImpl)base_import_tests_models_float) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_float.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_float, null);
    }


    public void updateBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats){
        if(base_import_tests_models_floats!=null){
            List<base_import_tests_models_floatImpl> list = new ArrayList<base_import_tests_models_floatImpl>();
            for(Ibase_import_tests_models_float ibase_import_tests_models_float :base_import_tests_models_floats){
                list.add((base_import_tests_models_floatImpl)ibase_import_tests_models_float) ;
            }
            base_import_tests_models_floatFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_float> fetchDefault(SearchContext context){
        Page<base_import_tests_models_floatImpl> page = this.base_import_tests_models_floatFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ibase_import_tests_models_float> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_float base_import_tests_models_float){
        Ibase_import_tests_models_float clientModel = base_import_tests_models_floatFeignClient.getDraft(base_import_tests_models_float.getId(),(base_import_tests_models_floatImpl)base_import_tests_models_float) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_float.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_float, null);
    }



}

