package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_o2m_child;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_o2m_childClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_o2m_childImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_o2m_childFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_o2m_child] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_o2m_childClientServiceImpl implements Ibase_import_tests_models_o2m_childClientService {

    base_import_tests_models_o2m_childFeignClient base_import_tests_models_o2m_childFeignClient;

    @Autowired
    public base_import_tests_models_o2m_childClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_o2m_childFeignClient = nameBuilder.target(base_import_tests_models_o2m_childFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_o2m_childFeignClient = nameBuilder.target(base_import_tests_models_o2m_childFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_o2m_child createModel() {
		return new base_import_tests_models_o2m_childImpl();
	}


    public void create(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
        Ibase_import_tests_models_o2m_child clientModel = base_import_tests_models_o2m_childFeignClient.create((base_import_tests_models_o2m_childImpl)base_import_tests_models_o2m_child) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m_child.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m_child, null);
    }


    public void remove(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
        base_import_tests_models_o2m_childFeignClient.remove(base_import_tests_models_o2m_child.getId()) ;
    }


    public void createBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
        if(base_import_tests_models_o2m_children!=null){
            List<base_import_tests_models_o2m_childImpl> list = new ArrayList<base_import_tests_models_o2m_childImpl>();
            for(Ibase_import_tests_models_o2m_child ibase_import_tests_models_o2m_child :base_import_tests_models_o2m_children){
                list.add((base_import_tests_models_o2m_childImpl)ibase_import_tests_models_o2m_child) ;
            }
            base_import_tests_models_o2m_childFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
        if(base_import_tests_models_o2m_children!=null){
            List<base_import_tests_models_o2m_childImpl> list = new ArrayList<base_import_tests_models_o2m_childImpl>();
            for(Ibase_import_tests_models_o2m_child ibase_import_tests_models_o2m_child :base_import_tests_models_o2m_children){
                list.add((base_import_tests_models_o2m_childImpl)ibase_import_tests_models_o2m_child) ;
            }
            base_import_tests_models_o2m_childFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
        if(base_import_tests_models_o2m_children!=null){
            List<base_import_tests_models_o2m_childImpl> list = new ArrayList<base_import_tests_models_o2m_childImpl>();
            for(Ibase_import_tests_models_o2m_child ibase_import_tests_models_o2m_child :base_import_tests_models_o2m_children){
                list.add((base_import_tests_models_o2m_childImpl)ibase_import_tests_models_o2m_child) ;
            }
            base_import_tests_models_o2m_childFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_o2m_child> fetchDefault(SearchContext context){
        Page<base_import_tests_models_o2m_childImpl> page = this.base_import_tests_models_o2m_childFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
        Ibase_import_tests_models_o2m_child clientModel = base_import_tests_models_o2m_childFeignClient.get(base_import_tests_models_o2m_child.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m_child.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m_child, null);
    }


    public void update(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
        Ibase_import_tests_models_o2m_child clientModel = base_import_tests_models_o2m_childFeignClient.update(base_import_tests_models_o2m_child.getId(),(base_import_tests_models_o2m_childImpl)base_import_tests_models_o2m_child) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m_child.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m_child, null);
    }


    public Page<Ibase_import_tests_models_o2m_child> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
        Ibase_import_tests_models_o2m_child clientModel = base_import_tests_models_o2m_childFeignClient.getDraft(base_import_tests_models_o2m_child.getId(),(base_import_tests_models_o2m_childImpl)base_import_tests_models_o2m_child) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_o2m_child.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_o2m_child, null);
    }



}

