package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_o2m_child;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_o2m_childImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_o2m_child] 服务对象接口
 */
public interface base_import_tests_models_o2m_childFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2m_children")
    public base_import_tests_models_o2m_childImpl create(@RequestBody base_import_tests_models_o2m_childImpl base_import_tests_models_o2m_child);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2m_children/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2m_children/createbatch")
    public base_import_tests_models_o2m_childImpl createBatch(@RequestBody List<base_import_tests_models_o2m_childImpl> base_import_tests_models_o2m_children);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2m_children/removebatch")
    public base_import_tests_models_o2m_childImpl removeBatch(@RequestBody List<base_import_tests_models_o2m_childImpl> base_import_tests_models_o2m_children);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2m_children/updatebatch")
    public base_import_tests_models_o2m_childImpl updateBatch(@RequestBody List<base_import_tests_models_o2m_childImpl> base_import_tests_models_o2m_children);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2m_children/fetchdefault")
    public Page<base_import_tests_models_o2m_childImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2m_children/{id}")
    public base_import_tests_models_o2m_childImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2m_children/{id}")
    public base_import_tests_models_o2m_childImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_o2m_childImpl base_import_tests_models_o2m_child);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2m_children/select")
    public Page<base_import_tests_models_o2m_childImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2m_children/{id}/getdraft")
    public base_import_tests_models_o2m_childImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_o2m_childImpl base_import_tests_models_o2m_child);



}
