package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_preview;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_previewClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_previewImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_previewFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_preview] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_previewClientServiceImpl implements Ibase_import_tests_models_previewClientService {

    base_import_tests_models_previewFeignClient base_import_tests_models_previewFeignClient;

    @Autowired
    public base_import_tests_models_previewClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_previewFeignClient = nameBuilder.target(base_import_tests_models_previewFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_previewFeignClient = nameBuilder.target(base_import_tests_models_previewFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_preview createModel() {
		return new base_import_tests_models_previewImpl();
	}


    public void createBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews){
        if(base_import_tests_models_previews!=null){
            List<base_import_tests_models_previewImpl> list = new ArrayList<base_import_tests_models_previewImpl>();
            for(Ibase_import_tests_models_preview ibase_import_tests_models_preview :base_import_tests_models_previews){
                list.add((base_import_tests_models_previewImpl)ibase_import_tests_models_preview) ;
            }
            base_import_tests_models_previewFeignClient.createBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_preview> fetchDefault(SearchContext context){
        Page<base_import_tests_models_previewImpl> page = this.base_import_tests_models_previewFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews){
        if(base_import_tests_models_previews!=null){
            List<base_import_tests_models_previewImpl> list = new ArrayList<base_import_tests_models_previewImpl>();
            for(Ibase_import_tests_models_preview ibase_import_tests_models_preview :base_import_tests_models_previews){
                list.add((base_import_tests_models_previewImpl)ibase_import_tests_models_preview) ;
            }
            base_import_tests_models_previewFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ibase_import_tests_models_preview base_import_tests_models_preview){
        Ibase_import_tests_models_preview clientModel = base_import_tests_models_previewFeignClient.create((base_import_tests_models_previewImpl)base_import_tests_models_preview) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_preview.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_preview, null);
    }


    public void update(Ibase_import_tests_models_preview base_import_tests_models_preview){
        Ibase_import_tests_models_preview clientModel = base_import_tests_models_previewFeignClient.update(base_import_tests_models_preview.getId(),(base_import_tests_models_previewImpl)base_import_tests_models_preview) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_preview.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_preview, null);
    }


    public void get(Ibase_import_tests_models_preview base_import_tests_models_preview){
        Ibase_import_tests_models_preview clientModel = base_import_tests_models_previewFeignClient.get(base_import_tests_models_preview.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_preview.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_preview, null);
    }


    public void removeBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews){
        if(base_import_tests_models_previews!=null){
            List<base_import_tests_models_previewImpl> list = new ArrayList<base_import_tests_models_previewImpl>();
            for(Ibase_import_tests_models_preview ibase_import_tests_models_preview :base_import_tests_models_previews){
                list.add((base_import_tests_models_previewImpl)ibase_import_tests_models_preview) ;
            }
            base_import_tests_models_previewFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ibase_import_tests_models_preview base_import_tests_models_preview){
        base_import_tests_models_previewFeignClient.remove(base_import_tests_models_preview.getId()) ;
    }


    public Page<Ibase_import_tests_models_preview> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_preview base_import_tests_models_preview){
        Ibase_import_tests_models_preview clientModel = base_import_tests_models_previewFeignClient.getDraft(base_import_tests_models_preview.getId(),(base_import_tests_models_previewImpl)base_import_tests_models_preview) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_preview.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_preview, null);
    }



}

