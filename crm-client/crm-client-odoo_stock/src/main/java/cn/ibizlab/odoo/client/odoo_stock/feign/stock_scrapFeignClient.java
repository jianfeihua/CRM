package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_scrap;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_scrapImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_scrap] 服务对象接口
 */
public interface stock_scrapFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scraps/fetchdefault")
    public Page<stock_scrapImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_scraps/updatebatch")
    public stock_scrapImpl updateBatch(@RequestBody List<stock_scrapImpl> stock_scraps);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_scraps/createbatch")
    public stock_scrapImpl createBatch(@RequestBody List<stock_scrapImpl> stock_scraps);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scraps/{id}")
    public stock_scrapImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_scraps/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_scraps")
    public stock_scrapImpl create(@RequestBody stock_scrapImpl stock_scrap);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_scraps/removebatch")
    public stock_scrapImpl removeBatch(@RequestBody List<stock_scrapImpl> stock_scraps);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_scraps/{id}")
    public stock_scrapImpl update(@PathVariable("id") Integer id,@RequestBody stock_scrapImpl stock_scrap);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scraps/select")
    public Page<stock_scrapImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scraps/{id}/getdraft")
    public stock_scrapImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_scrapImpl stock_scrap);



}
