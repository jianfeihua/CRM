package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_overprocessed_transfer;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_overprocessed_transferClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_overprocessed_transferImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_overprocessed_transferFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_overprocessed_transfer] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_overprocessed_transferClientServiceImpl implements Istock_overprocessed_transferClientService {

    stock_overprocessed_transferFeignClient stock_overprocessed_transferFeignClient;

    @Autowired
    public stock_overprocessed_transferClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_overprocessed_transferFeignClient = nameBuilder.target(stock_overprocessed_transferFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_overprocessed_transferFeignClient = nameBuilder.target(stock_overprocessed_transferFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_overprocessed_transfer createModel() {
		return new stock_overprocessed_transferImpl();
	}


    public void create(Istock_overprocessed_transfer stock_overprocessed_transfer){
        Istock_overprocessed_transfer clientModel = stock_overprocessed_transferFeignClient.create((stock_overprocessed_transferImpl)stock_overprocessed_transfer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_overprocessed_transfer.getClass(), false);
        copier.copy(clientModel, stock_overprocessed_transfer, null);
    }


    public void get(Istock_overprocessed_transfer stock_overprocessed_transfer){
        Istock_overprocessed_transfer clientModel = stock_overprocessed_transferFeignClient.get(stock_overprocessed_transfer.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_overprocessed_transfer.getClass(), false);
        copier.copy(clientModel, stock_overprocessed_transfer, null);
    }


    public void removeBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers){
        if(stock_overprocessed_transfers!=null){
            List<stock_overprocessed_transferImpl> list = new ArrayList<stock_overprocessed_transferImpl>();
            for(Istock_overprocessed_transfer istock_overprocessed_transfer :stock_overprocessed_transfers){
                list.add((stock_overprocessed_transferImpl)istock_overprocessed_transfer) ;
            }
            stock_overprocessed_transferFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_overprocessed_transfer stock_overprocessed_transfer){
        Istock_overprocessed_transfer clientModel = stock_overprocessed_transferFeignClient.update(stock_overprocessed_transfer.getId(),(stock_overprocessed_transferImpl)stock_overprocessed_transfer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_overprocessed_transfer.getClass(), false);
        copier.copy(clientModel, stock_overprocessed_transfer, null);
    }


    public void updateBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers){
        if(stock_overprocessed_transfers!=null){
            List<stock_overprocessed_transferImpl> list = new ArrayList<stock_overprocessed_transferImpl>();
            for(Istock_overprocessed_transfer istock_overprocessed_transfer :stock_overprocessed_transfers){
                list.add((stock_overprocessed_transferImpl)istock_overprocessed_transfer) ;
            }
            stock_overprocessed_transferFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Istock_overprocessed_transfer stock_overprocessed_transfer){
        stock_overprocessed_transferFeignClient.remove(stock_overprocessed_transfer.getId()) ;
    }


    public void createBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers){
        if(stock_overprocessed_transfers!=null){
            List<stock_overprocessed_transferImpl> list = new ArrayList<stock_overprocessed_transferImpl>();
            for(Istock_overprocessed_transfer istock_overprocessed_transfer :stock_overprocessed_transfers){
                list.add((stock_overprocessed_transferImpl)istock_overprocessed_transfer) ;
            }
            stock_overprocessed_transferFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_overprocessed_transfer> fetchDefault(SearchContext context){
        Page<stock_overprocessed_transferImpl> page = this.stock_overprocessed_transferFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Istock_overprocessed_transfer> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_overprocessed_transfer stock_overprocessed_transfer){
        Istock_overprocessed_transfer clientModel = stock_overprocessed_transferFeignClient.getDraft(stock_overprocessed_transfer.getId(),(stock_overprocessed_transferImpl)stock_overprocessed_transfer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_overprocessed_transfer.getClass(), false);
        copier.copy(clientModel, stock_overprocessed_transfer, null);
    }



}

