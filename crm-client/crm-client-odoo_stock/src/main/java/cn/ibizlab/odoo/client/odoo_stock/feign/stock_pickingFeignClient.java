package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_picking;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_pickingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_picking] 服务对象接口
 */
public interface stock_pickingFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_pickings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_pickings")
    public stock_pickingImpl create(@RequestBody stock_pickingImpl stock_picking);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_pickings/updatebatch")
    public stock_pickingImpl updateBatch(@RequestBody List<stock_pickingImpl> stock_pickings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_pickings/{id}")
    public stock_pickingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_pickings/removebatch")
    public stock_pickingImpl removeBatch(@RequestBody List<stock_pickingImpl> stock_pickings);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_pickings/{id}")
    public stock_pickingImpl update(@PathVariable("id") Integer id,@RequestBody stock_pickingImpl stock_picking);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_pickings/createbatch")
    public stock_pickingImpl createBatch(@RequestBody List<stock_pickingImpl> stock_pickings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_pickings/fetchdefault")
    public Page<stock_pickingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_pickings/select")
    public Page<stock_pickingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_pickings/{id}/getdraft")
    public stock_pickingImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_pickingImpl stock_picking);



}
