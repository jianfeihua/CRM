package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_backorder_confirmation;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_backorder_confirmationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_backorder_confirmationImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_backorder_confirmationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_backorder_confirmation] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_backorder_confirmationClientServiceImpl implements Istock_backorder_confirmationClientService {

    stock_backorder_confirmationFeignClient stock_backorder_confirmationFeignClient;

    @Autowired
    public stock_backorder_confirmationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_backorder_confirmationFeignClient = nameBuilder.target(stock_backorder_confirmationFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_backorder_confirmationFeignClient = nameBuilder.target(stock_backorder_confirmationFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_backorder_confirmation createModel() {
		return new stock_backorder_confirmationImpl();
	}


    public void create(Istock_backorder_confirmation stock_backorder_confirmation){
        Istock_backorder_confirmation clientModel = stock_backorder_confirmationFeignClient.create((stock_backorder_confirmationImpl)stock_backorder_confirmation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_backorder_confirmation.getClass(), false);
        copier.copy(clientModel, stock_backorder_confirmation, null);
    }


    public Page<Istock_backorder_confirmation> fetchDefault(SearchContext context){
        Page<stock_backorder_confirmationImpl> page = this.stock_backorder_confirmationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Istock_backorder_confirmation stock_backorder_confirmation){
        Istock_backorder_confirmation clientModel = stock_backorder_confirmationFeignClient.get(stock_backorder_confirmation.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_backorder_confirmation.getClass(), false);
        copier.copy(clientModel, stock_backorder_confirmation, null);
    }


    public void removeBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations){
        if(stock_backorder_confirmations!=null){
            List<stock_backorder_confirmationImpl> list = new ArrayList<stock_backorder_confirmationImpl>();
            for(Istock_backorder_confirmation istock_backorder_confirmation :stock_backorder_confirmations){
                list.add((stock_backorder_confirmationImpl)istock_backorder_confirmation) ;
            }
            stock_backorder_confirmationFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Istock_backorder_confirmation stock_backorder_confirmation){
        stock_backorder_confirmationFeignClient.remove(stock_backorder_confirmation.getId()) ;
    }


    public void updateBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations){
        if(stock_backorder_confirmations!=null){
            List<stock_backorder_confirmationImpl> list = new ArrayList<stock_backorder_confirmationImpl>();
            for(Istock_backorder_confirmation istock_backorder_confirmation :stock_backorder_confirmations){
                list.add((stock_backorder_confirmationImpl)istock_backorder_confirmation) ;
            }
            stock_backorder_confirmationFeignClient.updateBatch(list) ;
        }
    }


    public void update(Istock_backorder_confirmation stock_backorder_confirmation){
        Istock_backorder_confirmation clientModel = stock_backorder_confirmationFeignClient.update(stock_backorder_confirmation.getId(),(stock_backorder_confirmationImpl)stock_backorder_confirmation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_backorder_confirmation.getClass(), false);
        copier.copy(clientModel, stock_backorder_confirmation, null);
    }


    public void createBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations){
        if(stock_backorder_confirmations!=null){
            List<stock_backorder_confirmationImpl> list = new ArrayList<stock_backorder_confirmationImpl>();
            for(Istock_backorder_confirmation istock_backorder_confirmation :stock_backorder_confirmations){
                list.add((stock_backorder_confirmationImpl)istock_backorder_confirmation) ;
            }
            stock_backorder_confirmationFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_backorder_confirmation> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_backorder_confirmation stock_backorder_confirmation){
        Istock_backorder_confirmation clientModel = stock_backorder_confirmationFeignClient.getDraft(stock_backorder_confirmation.getId(),(stock_backorder_confirmationImpl)stock_backorder_confirmation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_backorder_confirmation.getClass(), false);
        copier.copy(clientModel, stock_backorder_confirmation, null);
    }



}

