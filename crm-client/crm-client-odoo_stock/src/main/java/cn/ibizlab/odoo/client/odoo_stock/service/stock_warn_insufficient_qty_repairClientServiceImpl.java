package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qty_repairClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_repairImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_warn_insufficient_qty_repairFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_warn_insufficient_qty_repair] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_warn_insufficient_qty_repairClientServiceImpl implements Istock_warn_insufficient_qty_repairClientService {

    stock_warn_insufficient_qty_repairFeignClient stock_warn_insufficient_qty_repairFeignClient;

    @Autowired
    public stock_warn_insufficient_qty_repairClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qty_repairFeignClient = nameBuilder.target(stock_warn_insufficient_qty_repairFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qty_repairFeignClient = nameBuilder.target(stock_warn_insufficient_qty_repairFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_warn_insufficient_qty_repair createModel() {
		return new stock_warn_insufficient_qty_repairImpl();
	}


    public void get(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
        Istock_warn_insufficient_qty_repair clientModel = stock_warn_insufficient_qty_repairFeignClient.get(stock_warn_insufficient_qty_repair.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_repair.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_repair, null);
    }


    public void create(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
        Istock_warn_insufficient_qty_repair clientModel = stock_warn_insufficient_qty_repairFeignClient.create((stock_warn_insufficient_qty_repairImpl)stock_warn_insufficient_qty_repair) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_repair.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_repair, null);
    }


    public void createBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
        if(stock_warn_insufficient_qty_repairs!=null){
            List<stock_warn_insufficient_qty_repairImpl> list = new ArrayList<stock_warn_insufficient_qty_repairImpl>();
            for(Istock_warn_insufficient_qty_repair istock_warn_insufficient_qty_repair :stock_warn_insufficient_qty_repairs){
                list.add((stock_warn_insufficient_qty_repairImpl)istock_warn_insufficient_qty_repair) ;
            }
            stock_warn_insufficient_qty_repairFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
        if(stock_warn_insufficient_qty_repairs!=null){
            List<stock_warn_insufficient_qty_repairImpl> list = new ArrayList<stock_warn_insufficient_qty_repairImpl>();
            for(Istock_warn_insufficient_qty_repair istock_warn_insufficient_qty_repair :stock_warn_insufficient_qty_repairs){
                list.add((stock_warn_insufficient_qty_repairImpl)istock_warn_insufficient_qty_repair) ;
            }
            stock_warn_insufficient_qty_repairFeignClient.removeBatch(list) ;
        }
    }


    public Page<Istock_warn_insufficient_qty_repair> fetchDefault(SearchContext context){
        Page<stock_warn_insufficient_qty_repairImpl> page = this.stock_warn_insufficient_qty_repairFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
        if(stock_warn_insufficient_qty_repairs!=null){
            List<stock_warn_insufficient_qty_repairImpl> list = new ArrayList<stock_warn_insufficient_qty_repairImpl>();
            for(Istock_warn_insufficient_qty_repair istock_warn_insufficient_qty_repair :stock_warn_insufficient_qty_repairs){
                list.add((stock_warn_insufficient_qty_repairImpl)istock_warn_insufficient_qty_repair) ;
            }
            stock_warn_insufficient_qty_repairFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
        stock_warn_insufficient_qty_repairFeignClient.remove(stock_warn_insufficient_qty_repair.getId()) ;
    }


    public void update(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
        Istock_warn_insufficient_qty_repair clientModel = stock_warn_insufficient_qty_repairFeignClient.update(stock_warn_insufficient_qty_repair.getId(),(stock_warn_insufficient_qty_repairImpl)stock_warn_insufficient_qty_repair) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_repair.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_repair, null);
    }


    public Page<Istock_warn_insufficient_qty_repair> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
        Istock_warn_insufficient_qty_repair clientModel = stock_warn_insufficient_qty_repairFeignClient.getDraft(stock_warn_insufficient_qty_repair.getId(),(stock_warn_insufficient_qty_repairImpl)stock_warn_insufficient_qty_repair) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_repair.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_repair, null);
    }



}

