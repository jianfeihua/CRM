package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_rule;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_ruleClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_ruleImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_ruleFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_rule] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_ruleClientServiceImpl implements Istock_ruleClientService {

    stock_ruleFeignClient stock_ruleFeignClient;

    @Autowired
    public stock_ruleClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_ruleFeignClient = nameBuilder.target(stock_ruleFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_ruleFeignClient = nameBuilder.target(stock_ruleFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_rule createModel() {
		return new stock_ruleImpl();
	}


    public void update(Istock_rule stock_rule){
        Istock_rule clientModel = stock_ruleFeignClient.update(stock_rule.getId(),(stock_ruleImpl)stock_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rule.getClass(), false);
        copier.copy(clientModel, stock_rule, null);
    }


    public void remove(Istock_rule stock_rule){
        stock_ruleFeignClient.remove(stock_rule.getId()) ;
    }


    public void removeBatch(List<Istock_rule> stock_rules){
        if(stock_rules!=null){
            List<stock_ruleImpl> list = new ArrayList<stock_ruleImpl>();
            for(Istock_rule istock_rule :stock_rules){
                list.add((stock_ruleImpl)istock_rule) ;
            }
            stock_ruleFeignClient.removeBatch(list) ;
        }
    }


    public Page<Istock_rule> fetchDefault(SearchContext context){
        Page<stock_ruleImpl> page = this.stock_ruleFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Istock_rule stock_rule){
        Istock_rule clientModel = stock_ruleFeignClient.create((stock_ruleImpl)stock_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rule.getClass(), false);
        copier.copy(clientModel, stock_rule, null);
    }


    public void updateBatch(List<Istock_rule> stock_rules){
        if(stock_rules!=null){
            List<stock_ruleImpl> list = new ArrayList<stock_ruleImpl>();
            for(Istock_rule istock_rule :stock_rules){
                list.add((stock_ruleImpl)istock_rule) ;
            }
            stock_ruleFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Istock_rule> stock_rules){
        if(stock_rules!=null){
            List<stock_ruleImpl> list = new ArrayList<stock_ruleImpl>();
            for(Istock_rule istock_rule :stock_rules){
                list.add((stock_ruleImpl)istock_rule) ;
            }
            stock_ruleFeignClient.createBatch(list) ;
        }
    }


    public void get(Istock_rule stock_rule){
        Istock_rule clientModel = stock_ruleFeignClient.get(stock_rule.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rule.getClass(), false);
        copier.copy(clientModel, stock_rule, null);
    }


    public Page<Istock_rule> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_rule stock_rule){
        Istock_rule clientModel = stock_ruleFeignClient.getDraft(stock_rule.getId(),(stock_ruleImpl)stock_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rule.getClass(), false);
        copier.copy(clientModel, stock_rule, null);
    }



}

