package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_quant;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quantImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_quant] 服务对象接口
 */
public interface stock_quantFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quants/{id}")
    public stock_quantImpl update(@PathVariable("id") Integer id,@RequestBody stock_quantImpl stock_quant);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quants/removebatch")
    public stock_quantImpl removeBatch(@RequestBody List<stock_quantImpl> stock_quants);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quants/{id}")
    public stock_quantImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quants/fetchdefault")
    public Page<stock_quantImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quants")
    public stock_quantImpl create(@RequestBody stock_quantImpl stock_quant);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quants/createbatch")
    public stock_quantImpl createBatch(@RequestBody List<stock_quantImpl> stock_quants);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quants/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quants/updatebatch")
    public stock_quantImpl updateBatch(@RequestBody List<stock_quantImpl> stock_quants);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quants/select")
    public Page<stock_quantImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quants/{id}/getdraft")
    public stock_quantImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_quantImpl stock_quant);



}
