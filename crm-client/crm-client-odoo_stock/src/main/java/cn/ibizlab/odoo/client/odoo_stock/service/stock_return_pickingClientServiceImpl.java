package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_return_pickingClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_return_pickingImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_return_pickingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_return_picking] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_return_pickingClientServiceImpl implements Istock_return_pickingClientService {

    stock_return_pickingFeignClient stock_return_pickingFeignClient;

    @Autowired
    public stock_return_pickingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_return_pickingFeignClient = nameBuilder.target(stock_return_pickingFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_return_pickingFeignClient = nameBuilder.target(stock_return_pickingFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_return_picking createModel() {
		return new stock_return_pickingImpl();
	}


    public void updateBatch(List<Istock_return_picking> stock_return_pickings){
        if(stock_return_pickings!=null){
            List<stock_return_pickingImpl> list = new ArrayList<stock_return_pickingImpl>();
            for(Istock_return_picking istock_return_picking :stock_return_pickings){
                list.add((stock_return_pickingImpl)istock_return_picking) ;
            }
            stock_return_pickingFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Istock_return_picking stock_return_picking){
        stock_return_pickingFeignClient.remove(stock_return_picking.getId()) ;
    }


    public Page<Istock_return_picking> fetchDefault(SearchContext context){
        Page<stock_return_pickingImpl> page = this.stock_return_pickingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Istock_return_picking> stock_return_pickings){
        if(stock_return_pickings!=null){
            List<stock_return_pickingImpl> list = new ArrayList<stock_return_pickingImpl>();
            for(Istock_return_picking istock_return_picking :stock_return_pickings){
                list.add((stock_return_pickingImpl)istock_return_picking) ;
            }
            stock_return_pickingFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Istock_return_picking> stock_return_pickings){
        if(stock_return_pickings!=null){
            List<stock_return_pickingImpl> list = new ArrayList<stock_return_pickingImpl>();
            for(Istock_return_picking istock_return_picking :stock_return_pickings){
                list.add((stock_return_pickingImpl)istock_return_picking) ;
            }
            stock_return_pickingFeignClient.createBatch(list) ;
        }
    }


    public void create(Istock_return_picking stock_return_picking){
        Istock_return_picking clientModel = stock_return_pickingFeignClient.create((stock_return_pickingImpl)stock_return_picking) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking.getClass(), false);
        copier.copy(clientModel, stock_return_picking, null);
    }


    public void update(Istock_return_picking stock_return_picking){
        Istock_return_picking clientModel = stock_return_pickingFeignClient.update(stock_return_picking.getId(),(stock_return_pickingImpl)stock_return_picking) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking.getClass(), false);
        copier.copy(clientModel, stock_return_picking, null);
    }


    public void get(Istock_return_picking stock_return_picking){
        Istock_return_picking clientModel = stock_return_pickingFeignClient.get(stock_return_picking.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking.getClass(), false);
        copier.copy(clientModel, stock_return_picking, null);
    }


    public Page<Istock_return_picking> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_return_picking stock_return_picking){
        Istock_return_picking clientModel = stock_return_pickingFeignClient.getDraft(stock_return_picking.getId(),(stock_return_pickingImpl)stock_return_picking) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking.getClass(), false);
        copier.copy(clientModel, stock_return_picking, null);
    }



}

