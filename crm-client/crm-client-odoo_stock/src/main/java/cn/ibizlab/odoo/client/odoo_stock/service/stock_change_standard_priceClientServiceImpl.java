package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_change_standard_price;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_change_standard_priceClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_change_standard_priceImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_change_standard_priceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_change_standard_price] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_change_standard_priceClientServiceImpl implements Istock_change_standard_priceClientService {

    stock_change_standard_priceFeignClient stock_change_standard_priceFeignClient;

    @Autowired
    public stock_change_standard_priceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_change_standard_priceFeignClient = nameBuilder.target(stock_change_standard_priceFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_change_standard_priceFeignClient = nameBuilder.target(stock_change_standard_priceFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_change_standard_price createModel() {
		return new stock_change_standard_priceImpl();
	}


    public void get(Istock_change_standard_price stock_change_standard_price){
        Istock_change_standard_price clientModel = stock_change_standard_priceFeignClient.get(stock_change_standard_price.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_standard_price.getClass(), false);
        copier.copy(clientModel, stock_change_standard_price, null);
    }


    public void create(Istock_change_standard_price stock_change_standard_price){
        Istock_change_standard_price clientModel = stock_change_standard_priceFeignClient.create((stock_change_standard_priceImpl)stock_change_standard_price) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_standard_price.getClass(), false);
        copier.copy(clientModel, stock_change_standard_price, null);
    }


    public void updateBatch(List<Istock_change_standard_price> stock_change_standard_prices){
        if(stock_change_standard_prices!=null){
            List<stock_change_standard_priceImpl> list = new ArrayList<stock_change_standard_priceImpl>();
            for(Istock_change_standard_price istock_change_standard_price :stock_change_standard_prices){
                list.add((stock_change_standard_priceImpl)istock_change_standard_price) ;
            }
            stock_change_standard_priceFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Istock_change_standard_price stock_change_standard_price){
        stock_change_standard_priceFeignClient.remove(stock_change_standard_price.getId()) ;
    }


    public Page<Istock_change_standard_price> fetchDefault(SearchContext context){
        Page<stock_change_standard_priceImpl> page = this.stock_change_standard_priceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Istock_change_standard_price stock_change_standard_price){
        Istock_change_standard_price clientModel = stock_change_standard_priceFeignClient.update(stock_change_standard_price.getId(),(stock_change_standard_priceImpl)stock_change_standard_price) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_standard_price.getClass(), false);
        copier.copy(clientModel, stock_change_standard_price, null);
    }


    public void removeBatch(List<Istock_change_standard_price> stock_change_standard_prices){
        if(stock_change_standard_prices!=null){
            List<stock_change_standard_priceImpl> list = new ArrayList<stock_change_standard_priceImpl>();
            for(Istock_change_standard_price istock_change_standard_price :stock_change_standard_prices){
                list.add((stock_change_standard_priceImpl)istock_change_standard_price) ;
            }
            stock_change_standard_priceFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Istock_change_standard_price> stock_change_standard_prices){
        if(stock_change_standard_prices!=null){
            List<stock_change_standard_priceImpl> list = new ArrayList<stock_change_standard_priceImpl>();
            for(Istock_change_standard_price istock_change_standard_price :stock_change_standard_prices){
                list.add((stock_change_standard_priceImpl)istock_change_standard_price) ;
            }
            stock_change_standard_priceFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_change_standard_price> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_change_standard_price stock_change_standard_price){
        Istock_change_standard_price clientModel = stock_change_standard_priceFeignClient.getDraft(stock_change_standard_price.getId(),(stock_change_standard_priceImpl)stock_change_standard_price) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_standard_price.getClass(), false);
        copier.copy(clientModel, stock_change_standard_price, null);
    }



}

