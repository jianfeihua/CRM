package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_scrap;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_scrapClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_scrapImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_scrapFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_scrap] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_scrapClientServiceImpl implements Istock_scrapClientService {

    stock_scrapFeignClient stock_scrapFeignClient;

    @Autowired
    public stock_scrapClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_scrapFeignClient = nameBuilder.target(stock_scrapFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_scrapFeignClient = nameBuilder.target(stock_scrapFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_scrap createModel() {
		return new stock_scrapImpl();
	}


    public Page<Istock_scrap> fetchDefault(SearchContext context){
        Page<stock_scrapImpl> page = this.stock_scrapFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_scrap> stock_scraps){
        if(stock_scraps!=null){
            List<stock_scrapImpl> list = new ArrayList<stock_scrapImpl>();
            for(Istock_scrap istock_scrap :stock_scraps){
                list.add((stock_scrapImpl)istock_scrap) ;
            }
            stock_scrapFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Istock_scrap> stock_scraps){
        if(stock_scraps!=null){
            List<stock_scrapImpl> list = new ArrayList<stock_scrapImpl>();
            for(Istock_scrap istock_scrap :stock_scraps){
                list.add((stock_scrapImpl)istock_scrap) ;
            }
            stock_scrapFeignClient.createBatch(list) ;
        }
    }


    public void get(Istock_scrap stock_scrap){
        Istock_scrap clientModel = stock_scrapFeignClient.get(stock_scrap.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scrap.getClass(), false);
        copier.copy(clientModel, stock_scrap, null);
    }


    public void remove(Istock_scrap stock_scrap){
        stock_scrapFeignClient.remove(stock_scrap.getId()) ;
    }


    public void create(Istock_scrap stock_scrap){
        Istock_scrap clientModel = stock_scrapFeignClient.create((stock_scrapImpl)stock_scrap) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scrap.getClass(), false);
        copier.copy(clientModel, stock_scrap, null);
    }


    public void removeBatch(List<Istock_scrap> stock_scraps){
        if(stock_scraps!=null){
            List<stock_scrapImpl> list = new ArrayList<stock_scrapImpl>();
            for(Istock_scrap istock_scrap :stock_scraps){
                list.add((stock_scrapImpl)istock_scrap) ;
            }
            stock_scrapFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_scrap stock_scrap){
        Istock_scrap clientModel = stock_scrapFeignClient.update(stock_scrap.getId(),(stock_scrapImpl)stock_scrap) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scrap.getClass(), false);
        copier.copy(clientModel, stock_scrap, null);
    }


    public Page<Istock_scrap> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_scrap stock_scrap){
        Istock_scrap clientModel = stock_scrapFeignClient.getDraft(stock_scrap.getId(),(stock_scrapImpl)stock_scrap) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scrap.getClass(), false);
        copier.copy(clientModel, stock_scrap, null);
    }



}

