package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_quantity_history;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_quantity_historyClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quantity_historyImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_quantity_historyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_quantity_history] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_quantity_historyClientServiceImpl implements Istock_quantity_historyClientService {

    stock_quantity_historyFeignClient stock_quantity_historyFeignClient;

    @Autowired
    public stock_quantity_historyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_quantity_historyFeignClient = nameBuilder.target(stock_quantity_historyFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_quantity_historyFeignClient = nameBuilder.target(stock_quantity_historyFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_quantity_history createModel() {
		return new stock_quantity_historyImpl();
	}


    public void update(Istock_quantity_history stock_quantity_history){
        Istock_quantity_history clientModel = stock_quantity_historyFeignClient.update(stock_quantity_history.getId(),(stock_quantity_historyImpl)stock_quantity_history) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quantity_history.getClass(), false);
        copier.copy(clientModel, stock_quantity_history, null);
    }


    public void create(Istock_quantity_history stock_quantity_history){
        Istock_quantity_history clientModel = stock_quantity_historyFeignClient.create((stock_quantity_historyImpl)stock_quantity_history) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quantity_history.getClass(), false);
        copier.copy(clientModel, stock_quantity_history, null);
    }


    public void updateBatch(List<Istock_quantity_history> stock_quantity_histories){
        if(stock_quantity_histories!=null){
            List<stock_quantity_historyImpl> list = new ArrayList<stock_quantity_historyImpl>();
            for(Istock_quantity_history istock_quantity_history :stock_quantity_histories){
                list.add((stock_quantity_historyImpl)istock_quantity_history) ;
            }
            stock_quantity_historyFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_quantity_history> stock_quantity_histories){
        if(stock_quantity_histories!=null){
            List<stock_quantity_historyImpl> list = new ArrayList<stock_quantity_historyImpl>();
            for(Istock_quantity_history istock_quantity_history :stock_quantity_histories){
                list.add((stock_quantity_historyImpl)istock_quantity_history) ;
            }
            stock_quantity_historyFeignClient.removeBatch(list) ;
        }
    }


    public Page<Istock_quantity_history> fetchDefault(SearchContext context){
        Page<stock_quantity_historyImpl> page = this.stock_quantity_historyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Istock_quantity_history stock_quantity_history){
        Istock_quantity_history clientModel = stock_quantity_historyFeignClient.get(stock_quantity_history.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quantity_history.getClass(), false);
        copier.copy(clientModel, stock_quantity_history, null);
    }


    public void createBatch(List<Istock_quantity_history> stock_quantity_histories){
        if(stock_quantity_histories!=null){
            List<stock_quantity_historyImpl> list = new ArrayList<stock_quantity_historyImpl>();
            for(Istock_quantity_history istock_quantity_history :stock_quantity_histories){
                list.add((stock_quantity_historyImpl)istock_quantity_history) ;
            }
            stock_quantity_historyFeignClient.createBatch(list) ;
        }
    }


    public void remove(Istock_quantity_history stock_quantity_history){
        stock_quantity_historyFeignClient.remove(stock_quantity_history.getId()) ;
    }


    public Page<Istock_quantity_history> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_quantity_history stock_quantity_history){
        Istock_quantity_history clientModel = stock_quantity_historyFeignClient.getDraft(stock_quantity_history.getId(),(stock_quantity_historyImpl)stock_quantity_history) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quantity_history.getClass(), false);
        copier.copy(clientModel, stock_quantity_history, null);
    }



}

