package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_move_line;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_move_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_move_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_move_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_move_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_move_lineClientServiceImpl implements Istock_move_lineClientService {

    stock_move_lineFeignClient stock_move_lineFeignClient;

    @Autowired
    public stock_move_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_move_lineFeignClient = nameBuilder.target(stock_move_lineFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_move_lineFeignClient = nameBuilder.target(stock_move_lineFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_move_line createModel() {
		return new stock_move_lineImpl();
	}


    public void removeBatch(List<Istock_move_line> stock_move_lines){
        if(stock_move_lines!=null){
            List<stock_move_lineImpl> list = new ArrayList<stock_move_lineImpl>();
            for(Istock_move_line istock_move_line :stock_move_lines){
                list.add((stock_move_lineImpl)istock_move_line) ;
            }
            stock_move_lineFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Istock_move_line stock_move_line){
        stock_move_lineFeignClient.remove(stock_move_line.getId()) ;
    }


    public void create(Istock_move_line stock_move_line){
        Istock_move_line clientModel = stock_move_lineFeignClient.create((stock_move_lineImpl)stock_move_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move_line.getClass(), false);
        copier.copy(clientModel, stock_move_line, null);
    }


    public void createBatch(List<Istock_move_line> stock_move_lines){
        if(stock_move_lines!=null){
            List<stock_move_lineImpl> list = new ArrayList<stock_move_lineImpl>();
            for(Istock_move_line istock_move_line :stock_move_lines){
                list.add((stock_move_lineImpl)istock_move_line) ;
            }
            stock_move_lineFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_move_line> fetchDefault(SearchContext context){
        Page<stock_move_lineImpl> page = this.stock_move_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_move_line> stock_move_lines){
        if(stock_move_lines!=null){
            List<stock_move_lineImpl> list = new ArrayList<stock_move_lineImpl>();
            for(Istock_move_line istock_move_line :stock_move_lines){
                list.add((stock_move_lineImpl)istock_move_line) ;
            }
            stock_move_lineFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_move_line stock_move_line){
        Istock_move_line clientModel = stock_move_lineFeignClient.get(stock_move_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move_line.getClass(), false);
        copier.copy(clientModel, stock_move_line, null);
    }


    public void update(Istock_move_line stock_move_line){
        Istock_move_line clientModel = stock_move_lineFeignClient.update(stock_move_line.getId(),(stock_move_lineImpl)stock_move_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move_line.getClass(), false);
        copier.copy(clientModel, stock_move_line, null);
    }


    public Page<Istock_move_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_move_line stock_move_line){
        Istock_move_line clientModel = stock_move_lineFeignClient.getDraft(stock_move_line.getId(),(stock_move_lineImpl)stock_move_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move_line.getClass(), false);
        copier.copy(clientModel, stock_move_line, null);
    }



}

