package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_rule;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_ruleImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_rule] 服务对象接口
 */
public interface stock_ruleFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules/{id}")
    public stock_ruleImpl update(@PathVariable("id") Integer id,@RequestBody stock_ruleImpl stock_rule);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules/removebatch")
    public stock_ruleImpl removeBatch(@RequestBody List<stock_ruleImpl> stock_rules);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules/fetchdefault")
    public Page<stock_ruleImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules")
    public stock_ruleImpl create(@RequestBody stock_ruleImpl stock_rule);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules/updatebatch")
    public stock_ruleImpl updateBatch(@RequestBody List<stock_ruleImpl> stock_rules);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules/createbatch")
    public stock_ruleImpl createBatch(@RequestBody List<stock_ruleImpl> stock_rules);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules/{id}")
    public stock_ruleImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules/select")
    public Page<stock_ruleImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules/{id}/getdraft")
    public stock_ruleImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_ruleImpl stock_rule);



}
