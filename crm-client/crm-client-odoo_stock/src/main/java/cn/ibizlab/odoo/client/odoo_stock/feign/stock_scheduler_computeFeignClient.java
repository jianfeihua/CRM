package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_scheduler_compute;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_scheduler_computeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_scheduler_compute] 服务对象接口
 */
public interface stock_scheduler_computeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scheduler_computes/fetchdefault")
    public Page<stock_scheduler_computeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_scheduler_computes/updatebatch")
    public stock_scheduler_computeImpl updateBatch(@RequestBody List<stock_scheduler_computeImpl> stock_scheduler_computes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_scheduler_computes/removebatch")
    public stock_scheduler_computeImpl removeBatch(@RequestBody List<stock_scheduler_computeImpl> stock_scheduler_computes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_scheduler_computes/createbatch")
    public stock_scheduler_computeImpl createBatch(@RequestBody List<stock_scheduler_computeImpl> stock_scheduler_computes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_scheduler_computes")
    public stock_scheduler_computeImpl create(@RequestBody stock_scheduler_computeImpl stock_scheduler_compute);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scheduler_computes/{id}")
    public stock_scheduler_computeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_scheduler_computes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_scheduler_computes/{id}")
    public stock_scheduler_computeImpl update(@PathVariable("id") Integer id,@RequestBody stock_scheduler_computeImpl stock_scheduler_compute);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scheduler_computes/select")
    public Page<stock_scheduler_computeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scheduler_computes/{id}/getdraft")
    public stock_scheduler_computeImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_scheduler_computeImpl stock_scheduler_compute);



}
