package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_move;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_moveClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_moveImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_moveFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_move] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_moveClientServiceImpl implements Istock_moveClientService {

    stock_moveFeignClient stock_moveFeignClient;

    @Autowired
    public stock_moveClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_moveFeignClient = nameBuilder.target(stock_moveFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_moveFeignClient = nameBuilder.target(stock_moveFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_move createModel() {
		return new stock_moveImpl();
	}


    public void createBatch(List<Istock_move> stock_moves){
        if(stock_moves!=null){
            List<stock_moveImpl> list = new ArrayList<stock_moveImpl>();
            for(Istock_move istock_move :stock_moves){
                list.add((stock_moveImpl)istock_move) ;
            }
            stock_moveFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Istock_move> stock_moves){
        if(stock_moves!=null){
            List<stock_moveImpl> list = new ArrayList<stock_moveImpl>();
            for(Istock_move istock_move :stock_moves){
                list.add((stock_moveImpl)istock_move) ;
            }
            stock_moveFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_move stock_move){
        Istock_move clientModel = stock_moveFeignClient.get(stock_move.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move.getClass(), false);
        copier.copy(clientModel, stock_move, null);
    }


    public void create(Istock_move stock_move){
        Istock_move clientModel = stock_moveFeignClient.create((stock_moveImpl)stock_move) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move.getClass(), false);
        copier.copy(clientModel, stock_move, null);
    }


    public void update(Istock_move stock_move){
        Istock_move clientModel = stock_moveFeignClient.update(stock_move.getId(),(stock_moveImpl)stock_move) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move.getClass(), false);
        copier.copy(clientModel, stock_move, null);
    }


    public void removeBatch(List<Istock_move> stock_moves){
        if(stock_moves!=null){
            List<stock_moveImpl> list = new ArrayList<stock_moveImpl>();
            for(Istock_move istock_move :stock_moves){
                list.add((stock_moveImpl)istock_move) ;
            }
            stock_moveFeignClient.removeBatch(list) ;
        }
    }


    public Page<Istock_move> fetchDefault(SearchContext context){
        Page<stock_moveImpl> page = this.stock_moveFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Istock_move stock_move){
        stock_moveFeignClient.remove(stock_move.getId()) ;
    }


    public Page<Istock_move> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_move stock_move){
        Istock_move clientModel = stock_moveFeignClient.getDraft(stock_move.getId(),(stock_moveImpl)stock_move) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_move.getClass(), false);
        copier.copy(clientModel, stock_move, null);
    }



}

