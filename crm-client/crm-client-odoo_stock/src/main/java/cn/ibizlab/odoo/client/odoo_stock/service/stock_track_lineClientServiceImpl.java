package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_track_line;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_track_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_track_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_track_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_track_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_track_lineClientServiceImpl implements Istock_track_lineClientService {

    stock_track_lineFeignClient stock_track_lineFeignClient;

    @Autowired
    public stock_track_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_track_lineFeignClient = nameBuilder.target(stock_track_lineFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_track_lineFeignClient = nameBuilder.target(stock_track_lineFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_track_line createModel() {
		return new stock_track_lineImpl();
	}


    public void updateBatch(List<Istock_track_line> stock_track_lines){
        if(stock_track_lines!=null){
            List<stock_track_lineImpl> list = new ArrayList<stock_track_lineImpl>();
            for(Istock_track_line istock_track_line :stock_track_lines){
                list.add((stock_track_lineImpl)istock_track_line) ;
            }
            stock_track_lineFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_track_line stock_track_line){
        Istock_track_line clientModel = stock_track_lineFeignClient.get(stock_track_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_line.getClass(), false);
        copier.copy(clientModel, stock_track_line, null);
    }


    public void create(Istock_track_line stock_track_line){
        Istock_track_line clientModel = stock_track_lineFeignClient.create((stock_track_lineImpl)stock_track_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_line.getClass(), false);
        copier.copy(clientModel, stock_track_line, null);
    }


    public void removeBatch(List<Istock_track_line> stock_track_lines){
        if(stock_track_lines!=null){
            List<stock_track_lineImpl> list = new ArrayList<stock_track_lineImpl>();
            for(Istock_track_line istock_track_line :stock_track_lines){
                list.add((stock_track_lineImpl)istock_track_line) ;
            }
            stock_track_lineFeignClient.removeBatch(list) ;
        }
    }


    public Page<Istock_track_line> fetchDefault(SearchContext context){
        Page<stock_track_lineImpl> page = this.stock_track_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Istock_track_line stock_track_line){
        Istock_track_line clientModel = stock_track_lineFeignClient.update(stock_track_line.getId(),(stock_track_lineImpl)stock_track_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_line.getClass(), false);
        copier.copy(clientModel, stock_track_line, null);
    }


    public void remove(Istock_track_line stock_track_line){
        stock_track_lineFeignClient.remove(stock_track_line.getId()) ;
    }


    public void createBatch(List<Istock_track_line> stock_track_lines){
        if(stock_track_lines!=null){
            List<stock_track_lineImpl> list = new ArrayList<stock_track_lineImpl>();
            for(Istock_track_line istock_track_line :stock_track_lines){
                list.add((stock_track_lineImpl)istock_track_line) ;
            }
            stock_track_lineFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_track_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_track_line stock_track_line){
        Istock_track_line clientModel = stock_track_lineFeignClient.getDraft(stock_track_line.getId(),(stock_track_lineImpl)stock_track_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_line.getClass(), false);
        copier.copy(clientModel, stock_track_line, null);
    }



}

