package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_location;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_locationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_locationImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_locationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_location] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_locationClientServiceImpl implements Istock_locationClientService {

    stock_locationFeignClient stock_locationFeignClient;

    @Autowired
    public stock_locationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_locationFeignClient = nameBuilder.target(stock_locationFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_locationFeignClient = nameBuilder.target(stock_locationFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_location createModel() {
		return new stock_locationImpl();
	}


    public void update(Istock_location stock_location){
        Istock_location clientModel = stock_locationFeignClient.update(stock_location.getId(),(stock_locationImpl)stock_location) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location.getClass(), false);
        copier.copy(clientModel, stock_location, null);
    }


    public void create(Istock_location stock_location){
        Istock_location clientModel = stock_locationFeignClient.create((stock_locationImpl)stock_location) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location.getClass(), false);
        copier.copy(clientModel, stock_location, null);
    }


    public void remove(Istock_location stock_location){
        stock_locationFeignClient.remove(stock_location.getId()) ;
    }


    public void removeBatch(List<Istock_location> stock_locations){
        if(stock_locations!=null){
            List<stock_locationImpl> list = new ArrayList<stock_locationImpl>();
            for(Istock_location istock_location :stock_locations){
                list.add((stock_locationImpl)istock_location) ;
            }
            stock_locationFeignClient.removeBatch(list) ;
        }
    }


    public void get(Istock_location stock_location){
        Istock_location clientModel = stock_locationFeignClient.get(stock_location.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location.getClass(), false);
        copier.copy(clientModel, stock_location, null);
    }


    public void createBatch(List<Istock_location> stock_locations){
        if(stock_locations!=null){
            List<stock_locationImpl> list = new ArrayList<stock_locationImpl>();
            for(Istock_location istock_location :stock_locations){
                list.add((stock_locationImpl)istock_location) ;
            }
            stock_locationFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_location> fetchDefault(SearchContext context){
        Page<stock_locationImpl> page = this.stock_locationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_location> stock_locations){
        if(stock_locations!=null){
            List<stock_locationImpl> list = new ArrayList<stock_locationImpl>();
            for(Istock_location istock_location :stock_locations){
                list.add((stock_locationImpl)istock_location) ;
            }
            stock_locationFeignClient.updateBatch(list) ;
        }
    }


    public Page<Istock_location> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_location stock_location){
        Istock_location clientModel = stock_locationFeignClient.getDraft(stock_location.getId(),(stock_locationImpl)stock_location) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location.getClass(), false);
        copier.copy(clientModel, stock_location, null);
    }



}

