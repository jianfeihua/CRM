package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_fixed_putaway_strat;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_fixed_putaway_stratClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_fixed_putaway_stratImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_fixed_putaway_stratFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_fixed_putaway_strat] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_fixed_putaway_stratClientServiceImpl implements Istock_fixed_putaway_stratClientService {

    stock_fixed_putaway_stratFeignClient stock_fixed_putaway_stratFeignClient;

    @Autowired
    public stock_fixed_putaway_stratClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_fixed_putaway_stratFeignClient = nameBuilder.target(stock_fixed_putaway_stratFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_fixed_putaway_stratFeignClient = nameBuilder.target(stock_fixed_putaway_stratFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_fixed_putaway_strat createModel() {
		return new stock_fixed_putaway_stratImpl();
	}


    public void updateBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats){
        if(stock_fixed_putaway_strats!=null){
            List<stock_fixed_putaway_stratImpl> list = new ArrayList<stock_fixed_putaway_stratImpl>();
            for(Istock_fixed_putaway_strat istock_fixed_putaway_strat :stock_fixed_putaway_strats){
                list.add((stock_fixed_putaway_stratImpl)istock_fixed_putaway_strat) ;
            }
            stock_fixed_putaway_stratFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats){
        if(stock_fixed_putaway_strats!=null){
            List<stock_fixed_putaway_stratImpl> list = new ArrayList<stock_fixed_putaway_stratImpl>();
            for(Istock_fixed_putaway_strat istock_fixed_putaway_strat :stock_fixed_putaway_strats){
                list.add((stock_fixed_putaway_stratImpl)istock_fixed_putaway_strat) ;
            }
            stock_fixed_putaway_stratFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
        Istock_fixed_putaway_strat clientModel = stock_fixed_putaway_stratFeignClient.update(stock_fixed_putaway_strat.getId(),(stock_fixed_putaway_stratImpl)stock_fixed_putaway_strat) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_fixed_putaway_strat.getClass(), false);
        copier.copy(clientModel, stock_fixed_putaway_strat, null);
    }


    public void create(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
        Istock_fixed_putaway_strat clientModel = stock_fixed_putaway_stratFeignClient.create((stock_fixed_putaway_stratImpl)stock_fixed_putaway_strat) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_fixed_putaway_strat.getClass(), false);
        copier.copy(clientModel, stock_fixed_putaway_strat, null);
    }


    public void get(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
        Istock_fixed_putaway_strat clientModel = stock_fixed_putaway_stratFeignClient.get(stock_fixed_putaway_strat.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_fixed_putaway_strat.getClass(), false);
        copier.copy(clientModel, stock_fixed_putaway_strat, null);
    }


    public void createBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats){
        if(stock_fixed_putaway_strats!=null){
            List<stock_fixed_putaway_stratImpl> list = new ArrayList<stock_fixed_putaway_stratImpl>();
            for(Istock_fixed_putaway_strat istock_fixed_putaway_strat :stock_fixed_putaway_strats){
                list.add((stock_fixed_putaway_stratImpl)istock_fixed_putaway_strat) ;
            }
            stock_fixed_putaway_stratFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_fixed_putaway_strat> fetchDefault(SearchContext context){
        Page<stock_fixed_putaway_stratImpl> page = this.stock_fixed_putaway_stratFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
        stock_fixed_putaway_stratFeignClient.remove(stock_fixed_putaway_strat.getId()) ;
    }


    public Page<Istock_fixed_putaway_strat> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
        Istock_fixed_putaway_strat clientModel = stock_fixed_putaway_stratFeignClient.getDraft(stock_fixed_putaway_strat.getId(),(stock_fixed_putaway_stratImpl)stock_fixed_putaway_strat) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_fixed_putaway_strat.getClass(), false);
        copier.copy(clientModel, stock_fixed_putaway_strat, null);
    }



}

