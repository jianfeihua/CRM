package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qtyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_warn_insufficient_qty] 服务对象接口
 */
public interface stock_warn_insufficient_qtyFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qties/removebatch")
    public stock_warn_insufficient_qtyImpl removeBatch(@RequestBody List<stock_warn_insufficient_qtyImpl> stock_warn_insufficient_qties);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qties/{id}")
    public stock_warn_insufficient_qtyImpl update(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qtyImpl stock_warn_insufficient_qty);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qties/updatebatch")
    public stock_warn_insufficient_qtyImpl updateBatch(@RequestBody List<stock_warn_insufficient_qtyImpl> stock_warn_insufficient_qties);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qties/fetchdefault")
    public Page<stock_warn_insufficient_qtyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qties/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qties/createbatch")
    public stock_warn_insufficient_qtyImpl createBatch(@RequestBody List<stock_warn_insufficient_qtyImpl> stock_warn_insufficient_qties);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qties/{id}")
    public stock_warn_insufficient_qtyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qties")
    public stock_warn_insufficient_qtyImpl create(@RequestBody stock_warn_insufficient_qtyImpl stock_warn_insufficient_qty);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qties/select")
    public Page<stock_warn_insufficient_qtyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qties/{id}/getdraft")
    public stock_warn_insufficient_qtyImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qtyImpl stock_warn_insufficient_qty);



}
