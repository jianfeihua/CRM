package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_quant;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_quantClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quantImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_quantFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_quant] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_quantClientServiceImpl implements Istock_quantClientService {

    stock_quantFeignClient stock_quantFeignClient;

    @Autowired
    public stock_quantClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_quantFeignClient = nameBuilder.target(stock_quantFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_quantFeignClient = nameBuilder.target(stock_quantFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_quant createModel() {
		return new stock_quantImpl();
	}


    public void update(Istock_quant stock_quant){
        Istock_quant clientModel = stock_quantFeignClient.update(stock_quant.getId(),(stock_quantImpl)stock_quant) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant.getClass(), false);
        copier.copy(clientModel, stock_quant, null);
    }


    public void removeBatch(List<Istock_quant> stock_quants){
        if(stock_quants!=null){
            List<stock_quantImpl> list = new ArrayList<stock_quantImpl>();
            for(Istock_quant istock_quant :stock_quants){
                list.add((stock_quantImpl)istock_quant) ;
            }
            stock_quantFeignClient.removeBatch(list) ;
        }
    }


    public void get(Istock_quant stock_quant){
        Istock_quant clientModel = stock_quantFeignClient.get(stock_quant.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant.getClass(), false);
        copier.copy(clientModel, stock_quant, null);
    }


    public Page<Istock_quant> fetchDefault(SearchContext context){
        Page<stock_quantImpl> page = this.stock_quantFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Istock_quant stock_quant){
        Istock_quant clientModel = stock_quantFeignClient.create((stock_quantImpl)stock_quant) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant.getClass(), false);
        copier.copy(clientModel, stock_quant, null);
    }


    public void createBatch(List<Istock_quant> stock_quants){
        if(stock_quants!=null){
            List<stock_quantImpl> list = new ArrayList<stock_quantImpl>();
            for(Istock_quant istock_quant :stock_quants){
                list.add((stock_quantImpl)istock_quant) ;
            }
            stock_quantFeignClient.createBatch(list) ;
        }
    }


    public void remove(Istock_quant stock_quant){
        stock_quantFeignClient.remove(stock_quant.getId()) ;
    }


    public void updateBatch(List<Istock_quant> stock_quants){
        if(stock_quants!=null){
            List<stock_quantImpl> list = new ArrayList<stock_quantImpl>();
            for(Istock_quant istock_quant :stock_quants){
                list.add((stock_quantImpl)istock_quant) ;
            }
            stock_quantFeignClient.updateBatch(list) ;
        }
    }


    public Page<Istock_quant> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_quant stock_quant){
        Istock_quant clientModel = stock_quantFeignClient.getDraft(stock_quant.getId(),(stock_quantImpl)stock_quant) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant.getClass(), false);
        copier.copy(clientModel, stock_quant, null);
    }



}

