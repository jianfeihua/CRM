package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qty_unbuildClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_unbuildImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_warn_insufficient_qty_unbuildFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_warn_insufficient_qty_unbuildClientServiceImpl implements Istock_warn_insufficient_qty_unbuildClientService {

    stock_warn_insufficient_qty_unbuildFeignClient stock_warn_insufficient_qty_unbuildFeignClient;

    @Autowired
    public stock_warn_insufficient_qty_unbuildClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qty_unbuildFeignClient = nameBuilder.target(stock_warn_insufficient_qty_unbuildFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qty_unbuildFeignClient = nameBuilder.target(stock_warn_insufficient_qty_unbuildFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_warn_insufficient_qty_unbuild createModel() {
		return new stock_warn_insufficient_qty_unbuildImpl();
	}


    public void remove(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
        stock_warn_insufficient_qty_unbuildFeignClient.remove(stock_warn_insufficient_qty_unbuild.getId()) ;
    }


    public void updateBatch(List<Istock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
        if(stock_warn_insufficient_qty_unbuilds!=null){
            List<stock_warn_insufficient_qty_unbuildImpl> list = new ArrayList<stock_warn_insufficient_qty_unbuildImpl>();
            for(Istock_warn_insufficient_qty_unbuild istock_warn_insufficient_qty_unbuild :stock_warn_insufficient_qty_unbuilds){
                list.add((stock_warn_insufficient_qty_unbuildImpl)istock_warn_insufficient_qty_unbuild) ;
            }
            stock_warn_insufficient_qty_unbuildFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
        Istock_warn_insufficient_qty_unbuild clientModel = stock_warn_insufficient_qty_unbuildFeignClient.get(stock_warn_insufficient_qty_unbuild.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_unbuild.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_unbuild, null);
    }


    public void removeBatch(List<Istock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
        if(stock_warn_insufficient_qty_unbuilds!=null){
            List<stock_warn_insufficient_qty_unbuildImpl> list = new ArrayList<stock_warn_insufficient_qty_unbuildImpl>();
            for(Istock_warn_insufficient_qty_unbuild istock_warn_insufficient_qty_unbuild :stock_warn_insufficient_qty_unbuilds){
                list.add((stock_warn_insufficient_qty_unbuildImpl)istock_warn_insufficient_qty_unbuild) ;
            }
            stock_warn_insufficient_qty_unbuildFeignClient.removeBatch(list) ;
        }
    }


    public void create(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
        Istock_warn_insufficient_qty_unbuild clientModel = stock_warn_insufficient_qty_unbuildFeignClient.create((stock_warn_insufficient_qty_unbuildImpl)stock_warn_insufficient_qty_unbuild) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_unbuild.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_unbuild, null);
    }


    public Page<Istock_warn_insufficient_qty_unbuild> fetchDefault(SearchContext context){
        Page<stock_warn_insufficient_qty_unbuildImpl> page = this.stock_warn_insufficient_qty_unbuildFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Istock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
        if(stock_warn_insufficient_qty_unbuilds!=null){
            List<stock_warn_insufficient_qty_unbuildImpl> list = new ArrayList<stock_warn_insufficient_qty_unbuildImpl>();
            for(Istock_warn_insufficient_qty_unbuild istock_warn_insufficient_qty_unbuild :stock_warn_insufficient_qty_unbuilds){
                list.add((stock_warn_insufficient_qty_unbuildImpl)istock_warn_insufficient_qty_unbuild) ;
            }
            stock_warn_insufficient_qty_unbuildFeignClient.createBatch(list) ;
        }
    }


    public void update(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
        Istock_warn_insufficient_qty_unbuild clientModel = stock_warn_insufficient_qty_unbuildFeignClient.update(stock_warn_insufficient_qty_unbuild.getId(),(stock_warn_insufficient_qty_unbuildImpl)stock_warn_insufficient_qty_unbuild) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_unbuild.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_unbuild, null);
    }


    public Page<Istock_warn_insufficient_qty_unbuild> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
        Istock_warn_insufficient_qty_unbuild clientModel = stock_warn_insufficient_qty_unbuildFeignClient.getDraft(stock_warn_insufficient_qty_unbuild.getId(),(stock_warn_insufficient_qty_unbuildImpl)stock_warn_insufficient_qty_unbuild) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_unbuild.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_unbuild, null);
    }



}

