package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qty_scrapClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_scrapImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_warn_insufficient_qty_scrapFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_warn_insufficient_qty_scrap] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_warn_insufficient_qty_scrapClientServiceImpl implements Istock_warn_insufficient_qty_scrapClientService {

    stock_warn_insufficient_qty_scrapFeignClient stock_warn_insufficient_qty_scrapFeignClient;

    @Autowired
    public stock_warn_insufficient_qty_scrapClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qty_scrapFeignClient = nameBuilder.target(stock_warn_insufficient_qty_scrapFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qty_scrapFeignClient = nameBuilder.target(stock_warn_insufficient_qty_scrapFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_warn_insufficient_qty_scrap createModel() {
		return new stock_warn_insufficient_qty_scrapImpl();
	}


    public Page<Istock_warn_insufficient_qty_scrap> fetchDefault(SearchContext context){
        Page<stock_warn_insufficient_qty_scrapImpl> page = this.stock_warn_insufficient_qty_scrapFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
        if(stock_warn_insufficient_qty_scraps!=null){
            List<stock_warn_insufficient_qty_scrapImpl> list = new ArrayList<stock_warn_insufficient_qty_scrapImpl>();
            for(Istock_warn_insufficient_qty_scrap istock_warn_insufficient_qty_scrap :stock_warn_insufficient_qty_scraps){
                list.add((stock_warn_insufficient_qty_scrapImpl)istock_warn_insufficient_qty_scrap) ;
            }
            stock_warn_insufficient_qty_scrapFeignClient.updateBatch(list) ;
        }
    }


    public void create(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
        Istock_warn_insufficient_qty_scrap clientModel = stock_warn_insufficient_qty_scrapFeignClient.create((stock_warn_insufficient_qty_scrapImpl)stock_warn_insufficient_qty_scrap) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_scrap.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_scrap, null);
    }


    public void createBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
        if(stock_warn_insufficient_qty_scraps!=null){
            List<stock_warn_insufficient_qty_scrapImpl> list = new ArrayList<stock_warn_insufficient_qty_scrapImpl>();
            for(Istock_warn_insufficient_qty_scrap istock_warn_insufficient_qty_scrap :stock_warn_insufficient_qty_scraps){
                list.add((stock_warn_insufficient_qty_scrapImpl)istock_warn_insufficient_qty_scrap) ;
            }
            stock_warn_insufficient_qty_scrapFeignClient.createBatch(list) ;
        }
    }


    public void remove(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
        stock_warn_insufficient_qty_scrapFeignClient.remove(stock_warn_insufficient_qty_scrap.getId()) ;
    }


    public void removeBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
        if(stock_warn_insufficient_qty_scraps!=null){
            List<stock_warn_insufficient_qty_scrapImpl> list = new ArrayList<stock_warn_insufficient_qty_scrapImpl>();
            for(Istock_warn_insufficient_qty_scrap istock_warn_insufficient_qty_scrap :stock_warn_insufficient_qty_scraps){
                list.add((stock_warn_insufficient_qty_scrapImpl)istock_warn_insufficient_qty_scrap) ;
            }
            stock_warn_insufficient_qty_scrapFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
        Istock_warn_insufficient_qty_scrap clientModel = stock_warn_insufficient_qty_scrapFeignClient.update(stock_warn_insufficient_qty_scrap.getId(),(stock_warn_insufficient_qty_scrapImpl)stock_warn_insufficient_qty_scrap) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_scrap.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_scrap, null);
    }


    public void get(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
        Istock_warn_insufficient_qty_scrap clientModel = stock_warn_insufficient_qty_scrapFeignClient.get(stock_warn_insufficient_qty_scrap.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_scrap.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_scrap, null);
    }


    public Page<Istock_warn_insufficient_qty_scrap> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
        Istock_warn_insufficient_qty_scrap clientModel = stock_warn_insufficient_qty_scrapFeignClient.getDraft(stock_warn_insufficient_qty_scrap.getId(),(stock_warn_insufficient_qty_scrapImpl)stock_warn_insufficient_qty_scrap) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty_scrap.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty_scrap, null);
    }



}

