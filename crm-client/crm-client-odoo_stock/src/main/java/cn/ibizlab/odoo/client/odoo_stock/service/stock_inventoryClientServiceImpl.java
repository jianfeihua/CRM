package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_inventory;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_inventoryClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_inventoryImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_inventoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_inventory] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_inventoryClientServiceImpl implements Istock_inventoryClientService {

    stock_inventoryFeignClient stock_inventoryFeignClient;

    @Autowired
    public stock_inventoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_inventoryFeignClient = nameBuilder.target(stock_inventoryFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_inventoryFeignClient = nameBuilder.target(stock_inventoryFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_inventory createModel() {
		return new stock_inventoryImpl();
	}


    public void update(Istock_inventory stock_inventory){
        Istock_inventory clientModel = stock_inventoryFeignClient.update(stock_inventory.getId(),(stock_inventoryImpl)stock_inventory) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory.getClass(), false);
        copier.copy(clientModel, stock_inventory, null);
    }


    public Page<Istock_inventory> fetchDefault(SearchContext context){
        Page<stock_inventoryImpl> page = this.stock_inventoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_inventory> stock_inventories){
        if(stock_inventories!=null){
            List<stock_inventoryImpl> list = new ArrayList<stock_inventoryImpl>();
            for(Istock_inventory istock_inventory :stock_inventories){
                list.add((stock_inventoryImpl)istock_inventory) ;
            }
            stock_inventoryFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Istock_inventory stock_inventory){
        stock_inventoryFeignClient.remove(stock_inventory.getId()) ;
    }


    public void create(Istock_inventory stock_inventory){
        Istock_inventory clientModel = stock_inventoryFeignClient.create((stock_inventoryImpl)stock_inventory) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory.getClass(), false);
        copier.copy(clientModel, stock_inventory, null);
    }


    public void removeBatch(List<Istock_inventory> stock_inventories){
        if(stock_inventories!=null){
            List<stock_inventoryImpl> list = new ArrayList<stock_inventoryImpl>();
            for(Istock_inventory istock_inventory :stock_inventories){
                list.add((stock_inventoryImpl)istock_inventory) ;
            }
            stock_inventoryFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Istock_inventory> stock_inventories){
        if(stock_inventories!=null){
            List<stock_inventoryImpl> list = new ArrayList<stock_inventoryImpl>();
            for(Istock_inventory istock_inventory :stock_inventories){
                list.add((stock_inventoryImpl)istock_inventory) ;
            }
            stock_inventoryFeignClient.createBatch(list) ;
        }
    }


    public void get(Istock_inventory stock_inventory){
        Istock_inventory clientModel = stock_inventoryFeignClient.get(stock_inventory.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory.getClass(), false);
        copier.copy(clientModel, stock_inventory, null);
    }


    public Page<Istock_inventory> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_inventory stock_inventory){
        Istock_inventory clientModel = stock_inventoryFeignClient.getDraft(stock_inventory.getId(),(stock_inventoryImpl)stock_inventory) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory.getClass(), false);
        copier.copy(clientModel, stock_inventory, null);
    }



}

