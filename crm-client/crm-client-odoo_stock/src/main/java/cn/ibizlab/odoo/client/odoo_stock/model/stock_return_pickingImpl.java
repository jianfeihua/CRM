package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[stock_return_picking] 对象
 */
public class stock_return_pickingImpl implements Istock_return_picking,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 退回位置
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 退回位置
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 链接的移动已存在
     */
    public String move_dest_exists;

    @JsonIgnore
    public boolean move_dest_existsDirtyFlag;
    
    /**
     * 原始位置
     */
    public Integer original_location_id;

    @JsonIgnore
    public boolean original_location_idDirtyFlag;
    
    /**
     * 原始位置
     */
    public String original_location_id_text;

    @JsonIgnore
    public boolean original_location_id_textDirtyFlag;
    
    /**
     * 上级位置
     */
    public Integer parent_location_id;

    @JsonIgnore
    public boolean parent_location_idDirtyFlag;
    
    /**
     * 上级位置
     */
    public String parent_location_id_text;

    @JsonIgnore
    public boolean parent_location_id_textDirtyFlag;
    
    /**
     * 分拣
     */
    public Integer picking_id;

    @JsonIgnore
    public boolean picking_idDirtyFlag;
    
    /**
     * 分拣
     */
    public String picking_id_text;

    @JsonIgnore
    public boolean picking_id_textDirtyFlag;
    
    /**
     * 移动
     */
    public String product_return_moves;

    @JsonIgnore
    public boolean product_return_movesDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [退回位置]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [退回位置]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [退回位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [退回位置]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [退回位置]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [退回位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [链接的移动已存在]
     */
    @JsonProperty("move_dest_exists")
    public String getMove_dest_exists(){
        return this.move_dest_exists ;
    }

    /**
     * 设置 [链接的移动已存在]
     */
    @JsonProperty("move_dest_exists")
    public void setMove_dest_exists(String  move_dest_exists){
        this.move_dest_exists = move_dest_exists ;
        this.move_dest_existsDirtyFlag = true ;
    }

     /**
     * 获取 [链接的移动已存在]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_existsDirtyFlag(){
        return this.move_dest_existsDirtyFlag ;
    }   

    /**
     * 获取 [原始位置]
     */
    @JsonProperty("original_location_id")
    public Integer getOriginal_location_id(){
        return this.original_location_id ;
    }

    /**
     * 设置 [原始位置]
     */
    @JsonProperty("original_location_id")
    public void setOriginal_location_id(Integer  original_location_id){
        this.original_location_id = original_location_id ;
        this.original_location_idDirtyFlag = true ;
    }

     /**
     * 获取 [原始位置]脏标记
     */
    @JsonIgnore
    public boolean getOriginal_location_idDirtyFlag(){
        return this.original_location_idDirtyFlag ;
    }   

    /**
     * 获取 [原始位置]
     */
    @JsonProperty("original_location_id_text")
    public String getOriginal_location_id_text(){
        return this.original_location_id_text ;
    }

    /**
     * 设置 [原始位置]
     */
    @JsonProperty("original_location_id_text")
    public void setOriginal_location_id_text(String  original_location_id_text){
        this.original_location_id_text = original_location_id_text ;
        this.original_location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [原始位置]脏标记
     */
    @JsonIgnore
    public boolean getOriginal_location_id_textDirtyFlag(){
        return this.original_location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [上级位置]
     */
    @JsonProperty("parent_location_id")
    public Integer getParent_location_id(){
        return this.parent_location_id ;
    }

    /**
     * 设置 [上级位置]
     */
    @JsonProperty("parent_location_id")
    public void setParent_location_id(Integer  parent_location_id){
        this.parent_location_id = parent_location_id ;
        this.parent_location_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级位置]脏标记
     */
    @JsonIgnore
    public boolean getParent_location_idDirtyFlag(){
        return this.parent_location_idDirtyFlag ;
    }   

    /**
     * 获取 [上级位置]
     */
    @JsonProperty("parent_location_id_text")
    public String getParent_location_id_text(){
        return this.parent_location_id_text ;
    }

    /**
     * 设置 [上级位置]
     */
    @JsonProperty("parent_location_id_text")
    public void setParent_location_id_text(String  parent_location_id_text){
        this.parent_location_id_text = parent_location_id_text ;
        this.parent_location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级位置]脏标记
     */
    @JsonIgnore
    public boolean getParent_location_id_textDirtyFlag(){
        return this.parent_location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分拣]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return this.picking_id ;
    }

    /**
     * 设置 [分拣]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

     /**
     * 获取 [分拣]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return this.picking_idDirtyFlag ;
    }   

    /**
     * 获取 [分拣]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return this.picking_id_text ;
    }

    /**
     * 设置 [分拣]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分拣]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return this.picking_id_textDirtyFlag ;
    }   

    /**
     * 获取 [移动]
     */
    @JsonProperty("product_return_moves")
    public String getProduct_return_moves(){
        return this.product_return_moves ;
    }

    /**
     * 设置 [移动]
     */
    @JsonProperty("product_return_moves")
    public void setProduct_return_moves(String  product_return_moves){
        this.product_return_moves = product_return_moves ;
        this.product_return_movesDirtyFlag = true ;
    }

     /**
     * 获取 [移动]脏标记
     */
    @JsonIgnore
    public boolean getProduct_return_movesDirtyFlag(){
        return this.product_return_movesDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
