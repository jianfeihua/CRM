package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_location_route;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_location_routeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_location_route] 服务对象接口
 */
public interface stock_location_routeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_location_routes/fetchdefault")
    public Page<stock_location_routeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_location_routes")
    public stock_location_routeImpl create(@RequestBody stock_location_routeImpl stock_location_route);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_location_routes/createbatch")
    public stock_location_routeImpl createBatch(@RequestBody List<stock_location_routeImpl> stock_location_routes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_location_routes/removebatch")
    public stock_location_routeImpl removeBatch(@RequestBody List<stock_location_routeImpl> stock_location_routes);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_location_routes/updatebatch")
    public stock_location_routeImpl updateBatch(@RequestBody List<stock_location_routeImpl> stock_location_routes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_location_routes/{id}")
    public stock_location_routeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_location_routes/{id}")
    public stock_location_routeImpl update(@PathVariable("id") Integer id,@RequestBody stock_location_routeImpl stock_location_route);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_location_routes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_location_routes/select")
    public Page<stock_location_routeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_location_routes/{id}/getdraft")
    public stock_location_routeImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_location_routeImpl stock_location_route);



}
