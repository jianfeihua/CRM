package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_picking;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[stock_picking] 对象
 */
public class stock_pickingImpl implements Istock_picking,Serializable{

    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 欠单
     */
    public Integer backorder_id;

    @JsonIgnore
    public boolean backorder_idDirtyFlag;
    
    /**
     * 欠单
     */
    public String backorder_ids;

    @JsonIgnore
    public boolean backorder_idsDirtyFlag;
    
    /**
     * 欠单
     */
    public String backorder_id_text;

    @JsonIgnore
    public boolean backorder_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 调拨日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_done;

    @JsonIgnore
    public boolean date_doneDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 补货组
     */
    public Integer group_id;

    @JsonIgnore
    public boolean group_idDirtyFlag;
    
    /**
     * 有包裹
     */
    public String has_packages;

    @JsonIgnore
    public boolean has_packagesDirtyFlag;
    
    /**
     * 有报废移动
     */
    public String has_scrap_move;

    @JsonIgnore
    public boolean has_scrap_moveDirtyFlag;
    
    /**
     * 有跟踪
     */
    public String has_tracking;

    @JsonIgnore
    public boolean has_trackingDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 立即调拨
     */
    public String immediate_transfer;

    @JsonIgnore
    public boolean immediate_transferDirtyFlag;
    
    /**
     * 是锁定
     */
    public String is_locked;

    @JsonIgnore
    public boolean is_lockedDirtyFlag;
    
    /**
     * 目的位置
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 目的位置
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 源位置
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 源位置
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 库存移动不在包裹里
     */
    public String move_ids_without_package;

    @JsonIgnore
    public boolean move_ids_without_packageDirtyFlag;
    
    /**
     * 库存移动
     */
    public String move_lines;

    @JsonIgnore
    public boolean move_linesDirtyFlag;
    
    /**
     * 有包裹作业
     */
    public String move_line_exist;

    @JsonIgnore
    public boolean move_line_existDirtyFlag;
    
    /**
     * 作业
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 无包裹作业
     */
    public String move_line_ids_without_package;

    @JsonIgnore
    public boolean move_line_ids_without_packageDirtyFlag;
    
    /**
     * 送货策略
     */
    public String move_type;

    @JsonIgnore
    public boolean move_typeDirtyFlag;
    
    /**
     * 编号
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 源文档
     */
    public String origin;

    @JsonIgnore
    public boolean originDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer owner_id;

    @JsonIgnore
    public boolean owner_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String owner_id_text;

    @JsonIgnore
    public boolean owner_id_textDirtyFlag;
    
    /**
     * 包裹层级
     */
    public String package_level_ids;

    @JsonIgnore
    public boolean package_level_idsDirtyFlag;
    
    /**
     * 包裹层级ids 详情
     */
    public String package_level_ids_details;

    @JsonIgnore
    public boolean package_level_ids_detailsDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 作业的类型
     */
    public String picking_type_code;

    @JsonIgnore
    public boolean picking_type_codeDirtyFlag;
    
    /**
     * 移动整个包裹
     */
    public String picking_type_entire_packs;

    @JsonIgnore
    public boolean picking_type_entire_packsDirtyFlag;
    
    /**
     * 作业类型
     */
    public Integer picking_type_id;

    @JsonIgnore
    public boolean picking_type_idDirtyFlag;
    
    /**
     * 作业类型
     */
    public String picking_type_id_text;

    @JsonIgnore
    public boolean picking_type_id_textDirtyFlag;
    
    /**
     * 已打印
     */
    public String printed;

    @JsonIgnore
    public boolean printedDirtyFlag;
    
    /**
     * 优先级
     */
    public String priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 采购订单
     */
    public Integer purchase_id;

    @JsonIgnore
    public boolean purchase_idDirtyFlag;
    
    /**
     * 销售订单
     */
    public Integer sale_id;

    @JsonIgnore
    public boolean sale_idDirtyFlag;
    
    /**
     * 销售订单
     */
    public String sale_id_text;

    @JsonIgnore
    public boolean sale_id_textDirtyFlag;
    
    /**
     * 预定交货日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp scheduled_date;

    @JsonIgnore
    public boolean scheduled_dateDirtyFlag;
    
    /**
     * 显示检查可用
     */
    public String show_check_availability;

    @JsonIgnore
    public boolean show_check_availabilityDirtyFlag;
    
    /**
     * 显示批次文本
     */
    public String show_lots_text;

    @JsonIgnore
    public boolean show_lots_textDirtyFlag;
    
    /**
     * 显示标记为代办
     */
    public String show_mark_as_todo;

    @JsonIgnore
    public boolean show_mark_as_todoDirtyFlag;
    
    /**
     * 显示作业
     */
    public String show_operations;

    @JsonIgnore
    public boolean show_operationsDirtyFlag;
    
    /**
     * 显示验证
     */
    public String show_validate;

    @JsonIgnore
    public boolean show_validateDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [欠单]
     */
    @JsonProperty("backorder_id")
    public Integer getBackorder_id(){
        return this.backorder_id ;
    }

    /**
     * 设置 [欠单]
     */
    @JsonProperty("backorder_id")
    public void setBackorder_id(Integer  backorder_id){
        this.backorder_id = backorder_id ;
        this.backorder_idDirtyFlag = true ;
    }

     /**
     * 获取 [欠单]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idDirtyFlag(){
        return this.backorder_idDirtyFlag ;
    }   

    /**
     * 获取 [欠单]
     */
    @JsonProperty("backorder_ids")
    public String getBackorder_ids(){
        return this.backorder_ids ;
    }

    /**
     * 设置 [欠单]
     */
    @JsonProperty("backorder_ids")
    public void setBackorder_ids(String  backorder_ids){
        this.backorder_ids = backorder_ids ;
        this.backorder_idsDirtyFlag = true ;
    }

     /**
     * 获取 [欠单]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idsDirtyFlag(){
        return this.backorder_idsDirtyFlag ;
    }   

    /**
     * 获取 [欠单]
     */
    @JsonProperty("backorder_id_text")
    public String getBackorder_id_text(){
        return this.backorder_id_text ;
    }

    /**
     * 设置 [欠单]
     */
    @JsonProperty("backorder_id_text")
    public void setBackorder_id_text(String  backorder_id_text){
        this.backorder_id_text = backorder_id_text ;
        this.backorder_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [欠单]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_id_textDirtyFlag(){
        return this.backorder_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [调拨日期]
     */
    @JsonProperty("date_done")
    public Timestamp getDate_done(){
        return this.date_done ;
    }

    /**
     * 设置 [调拨日期]
     */
    @JsonProperty("date_done")
    public void setDate_done(Timestamp  date_done){
        this.date_done = date_done ;
        this.date_doneDirtyFlag = true ;
    }

     /**
     * 获取 [调拨日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_doneDirtyFlag(){
        return this.date_doneDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [补货组]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return this.group_id ;
    }

    /**
     * 设置 [补货组]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

     /**
     * 获取 [补货组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return this.group_idDirtyFlag ;
    }   

    /**
     * 获取 [有包裹]
     */
    @JsonProperty("has_packages")
    public String getHas_packages(){
        return this.has_packages ;
    }

    /**
     * 设置 [有包裹]
     */
    @JsonProperty("has_packages")
    public void setHas_packages(String  has_packages){
        this.has_packages = has_packages ;
        this.has_packagesDirtyFlag = true ;
    }

     /**
     * 获取 [有包裹]脏标记
     */
    @JsonIgnore
    public boolean getHas_packagesDirtyFlag(){
        return this.has_packagesDirtyFlag ;
    }   

    /**
     * 获取 [有报废移动]
     */
    @JsonProperty("has_scrap_move")
    public String getHas_scrap_move(){
        return this.has_scrap_move ;
    }

    /**
     * 设置 [有报废移动]
     */
    @JsonProperty("has_scrap_move")
    public void setHas_scrap_move(String  has_scrap_move){
        this.has_scrap_move = has_scrap_move ;
        this.has_scrap_moveDirtyFlag = true ;
    }

     /**
     * 获取 [有报废移动]脏标记
     */
    @JsonIgnore
    public boolean getHas_scrap_moveDirtyFlag(){
        return this.has_scrap_moveDirtyFlag ;
    }   

    /**
     * 获取 [有跟踪]
     */
    @JsonProperty("has_tracking")
    public String getHas_tracking(){
        return this.has_tracking ;
    }

    /**
     * 设置 [有跟踪]
     */
    @JsonProperty("has_tracking")
    public void setHas_tracking(String  has_tracking){
        this.has_tracking = has_tracking ;
        this.has_trackingDirtyFlag = true ;
    }

     /**
     * 获取 [有跟踪]脏标记
     */
    @JsonIgnore
    public boolean getHas_trackingDirtyFlag(){
        return this.has_trackingDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [立即调拨]
     */
    @JsonProperty("immediate_transfer")
    public String getImmediate_transfer(){
        return this.immediate_transfer ;
    }

    /**
     * 设置 [立即调拨]
     */
    @JsonProperty("immediate_transfer")
    public void setImmediate_transfer(String  immediate_transfer){
        this.immediate_transfer = immediate_transfer ;
        this.immediate_transferDirtyFlag = true ;
    }

     /**
     * 获取 [立即调拨]脏标记
     */
    @JsonIgnore
    public boolean getImmediate_transferDirtyFlag(){
        return this.immediate_transferDirtyFlag ;
    }   

    /**
     * 获取 [是锁定]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return this.is_locked ;
    }

    /**
     * 设置 [是锁定]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

     /**
     * 获取 [是锁定]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return this.is_lockedDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [库存移动不在包裹里]
     */
    @JsonProperty("move_ids_without_package")
    public String getMove_ids_without_package(){
        return this.move_ids_without_package ;
    }

    /**
     * 设置 [库存移动不在包裹里]
     */
    @JsonProperty("move_ids_without_package")
    public void setMove_ids_without_package(String  move_ids_without_package){
        this.move_ids_without_package = move_ids_without_package ;
        this.move_ids_without_packageDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动不在包裹里]脏标记
     */
    @JsonIgnore
    public boolean getMove_ids_without_packageDirtyFlag(){
        return this.move_ids_without_packageDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("move_lines")
    public String getMove_lines(){
        return this.move_lines ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("move_lines")
    public void setMove_lines(String  move_lines){
        this.move_lines = move_lines ;
        this.move_linesDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_linesDirtyFlag(){
        return this.move_linesDirtyFlag ;
    }   

    /**
     * 获取 [有包裹作业]
     */
    @JsonProperty("move_line_exist")
    public String getMove_line_exist(){
        return this.move_line_exist ;
    }

    /**
     * 设置 [有包裹作业]
     */
    @JsonProperty("move_line_exist")
    public void setMove_line_exist(String  move_line_exist){
        this.move_line_exist = move_line_exist ;
        this.move_line_existDirtyFlag = true ;
    }

     /**
     * 获取 [有包裹作业]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_existDirtyFlag(){
        return this.move_line_existDirtyFlag ;
    }   

    /**
     * 获取 [作业]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [作业]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [作业]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [无包裹作业]
     */
    @JsonProperty("move_line_ids_without_package")
    public String getMove_line_ids_without_package(){
        return this.move_line_ids_without_package ;
    }

    /**
     * 设置 [无包裹作业]
     */
    @JsonProperty("move_line_ids_without_package")
    public void setMove_line_ids_without_package(String  move_line_ids_without_package){
        this.move_line_ids_without_package = move_line_ids_without_package ;
        this.move_line_ids_without_packageDirtyFlag = true ;
    }

     /**
     * 获取 [无包裹作业]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_ids_without_packageDirtyFlag(){
        return this.move_line_ids_without_packageDirtyFlag ;
    }   

    /**
     * 获取 [送货策略]
     */
    @JsonProperty("move_type")
    public String getMove_type(){
        return this.move_type ;
    }

    /**
     * 设置 [送货策略]
     */
    @JsonProperty("move_type")
    public void setMove_type(String  move_type){
        this.move_type = move_type ;
        this.move_typeDirtyFlag = true ;
    }

     /**
     * 获取 [送货策略]脏标记
     */
    @JsonIgnore
    public boolean getMove_typeDirtyFlag(){
        return this.move_typeDirtyFlag ;
    }   

    /**
     * 获取 [编号]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [编号]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [编号]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

     /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return this.owner_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return this.owner_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return this.owner_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return this.owner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包裹层级]
     */
    @JsonProperty("package_level_ids")
    public String getPackage_level_ids(){
        return this.package_level_ids ;
    }

    /**
     * 设置 [包裹层级]
     */
    @JsonProperty("package_level_ids")
    public void setPackage_level_ids(String  package_level_ids){
        this.package_level_ids = package_level_ids ;
        this.package_level_idsDirtyFlag = true ;
    }

     /**
     * 获取 [包裹层级]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idsDirtyFlag(){
        return this.package_level_idsDirtyFlag ;
    }   

    /**
     * 获取 [包裹层级ids 详情]
     */
    @JsonProperty("package_level_ids_details")
    public String getPackage_level_ids_details(){
        return this.package_level_ids_details ;
    }

    /**
     * 设置 [包裹层级ids 详情]
     */
    @JsonProperty("package_level_ids_details")
    public void setPackage_level_ids_details(String  package_level_ids_details){
        this.package_level_ids_details = package_level_ids_details ;
        this.package_level_ids_detailsDirtyFlag = true ;
    }

     /**
     * 获取 [包裹层级ids 详情]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_ids_detailsDirtyFlag(){
        return this.package_level_ids_detailsDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [作业的类型]
     */
    @JsonProperty("picking_type_code")
    public String getPicking_type_code(){
        return this.picking_type_code ;
    }

    /**
     * 设置 [作业的类型]
     */
    @JsonProperty("picking_type_code")
    public void setPicking_type_code(String  picking_type_code){
        this.picking_type_code = picking_type_code ;
        this.picking_type_codeDirtyFlag = true ;
    }

     /**
     * 获取 [作业的类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_codeDirtyFlag(){
        return this.picking_type_codeDirtyFlag ;
    }   

    /**
     * 获取 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return this.picking_type_entire_packs ;
    }

    /**
     * 设置 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

     /**
     * 获取 [移动整个包裹]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return this.picking_type_entire_packsDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return this.picking_type_id ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return this.picking_type_idDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return this.picking_type_id_text ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return this.picking_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已打印]
     */
    @JsonProperty("printed")
    public String getPrinted(){
        return this.printed ;
    }

    /**
     * 设置 [已打印]
     */
    @JsonProperty("printed")
    public void setPrinted(String  printed){
        this.printed = printed ;
        this.printedDirtyFlag = true ;
    }

     /**
     * 获取 [已打印]脏标记
     */
    @JsonIgnore
    public boolean getPrintedDirtyFlag(){
        return this.printedDirtyFlag ;
    }   

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [采购订单]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return this.purchase_id ;
    }

    /**
     * 设置 [采购订单]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return this.purchase_idDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_id")
    public Integer getSale_id(){
        return this.sale_id ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_id")
    public void setSale_id(Integer  sale_id){
        this.sale_id = sale_id ;
        this.sale_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_idDirtyFlag(){
        return this.sale_idDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_id_text")
    public String getSale_id_text(){
        return this.sale_id_text ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_id_text")
    public void setSale_id_text(String  sale_id_text){
        this.sale_id_text = sale_id_text ;
        this.sale_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_id_textDirtyFlag(){
        return this.sale_id_textDirtyFlag ;
    }   

    /**
     * 获取 [预定交货日期]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return this.scheduled_date ;
    }

    /**
     * 设置 [预定交货日期]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

     /**
     * 获取 [预定交货日期]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return this.scheduled_dateDirtyFlag ;
    }   

    /**
     * 获取 [显示检查可用]
     */
    @JsonProperty("show_check_availability")
    public String getShow_check_availability(){
        return this.show_check_availability ;
    }

    /**
     * 设置 [显示检查可用]
     */
    @JsonProperty("show_check_availability")
    public void setShow_check_availability(String  show_check_availability){
        this.show_check_availability = show_check_availability ;
        this.show_check_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [显示检查可用]脏标记
     */
    @JsonIgnore
    public boolean getShow_check_availabilityDirtyFlag(){
        return this.show_check_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [显示批次文本]
     */
    @JsonProperty("show_lots_text")
    public String getShow_lots_text(){
        return this.show_lots_text ;
    }

    /**
     * 设置 [显示批次文本]
     */
    @JsonProperty("show_lots_text")
    public void setShow_lots_text(String  show_lots_text){
        this.show_lots_text = show_lots_text ;
        this.show_lots_textDirtyFlag = true ;
    }

     /**
     * 获取 [显示批次文本]脏标记
     */
    @JsonIgnore
    public boolean getShow_lots_textDirtyFlag(){
        return this.show_lots_textDirtyFlag ;
    }   

    /**
     * 获取 [显示标记为代办]
     */
    @JsonProperty("show_mark_as_todo")
    public String getShow_mark_as_todo(){
        return this.show_mark_as_todo ;
    }

    /**
     * 设置 [显示标记为代办]
     */
    @JsonProperty("show_mark_as_todo")
    public void setShow_mark_as_todo(String  show_mark_as_todo){
        this.show_mark_as_todo = show_mark_as_todo ;
        this.show_mark_as_todoDirtyFlag = true ;
    }

     /**
     * 获取 [显示标记为代办]脏标记
     */
    @JsonIgnore
    public boolean getShow_mark_as_todoDirtyFlag(){
        return this.show_mark_as_todoDirtyFlag ;
    }   

    /**
     * 获取 [显示作业]
     */
    @JsonProperty("show_operations")
    public String getShow_operations(){
        return this.show_operations ;
    }

    /**
     * 设置 [显示作业]
     */
    @JsonProperty("show_operations")
    public void setShow_operations(String  show_operations){
        this.show_operations = show_operations ;
        this.show_operationsDirtyFlag = true ;
    }

     /**
     * 获取 [显示作业]脏标记
     */
    @JsonIgnore
    public boolean getShow_operationsDirtyFlag(){
        return this.show_operationsDirtyFlag ;
    }   

    /**
     * 获取 [显示验证]
     */
    @JsonProperty("show_validate")
    public String getShow_validate(){
        return this.show_validate ;
    }

    /**
     * 设置 [显示验证]
     */
    @JsonProperty("show_validate")
    public void setShow_validate(String  show_validate){
        this.show_validate = show_validate ;
        this.show_validateDirtyFlag = true ;
    }

     /**
     * 获取 [显示验证]脏标记
     */
    @JsonIgnore
    public boolean getShow_validateDirtyFlag(){
        return this.show_validateDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
