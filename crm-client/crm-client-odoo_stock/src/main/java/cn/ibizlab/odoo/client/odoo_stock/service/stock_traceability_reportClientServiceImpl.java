package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_traceability_report;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_traceability_reportClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_traceability_reportImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_traceability_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_traceability_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_traceability_reportClientServiceImpl implements Istock_traceability_reportClientService {

    stock_traceability_reportFeignClient stock_traceability_reportFeignClient;

    @Autowired
    public stock_traceability_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_traceability_reportFeignClient = nameBuilder.target(stock_traceability_reportFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_traceability_reportFeignClient = nameBuilder.target(stock_traceability_reportFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_traceability_report createModel() {
		return new stock_traceability_reportImpl();
	}


    public void create(Istock_traceability_report stock_traceability_report){
        Istock_traceability_report clientModel = stock_traceability_reportFeignClient.create((stock_traceability_reportImpl)stock_traceability_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_traceability_report.getClass(), false);
        copier.copy(clientModel, stock_traceability_report, null);
    }


    public void update(Istock_traceability_report stock_traceability_report){
        Istock_traceability_report clientModel = stock_traceability_reportFeignClient.update(stock_traceability_report.getId(),(stock_traceability_reportImpl)stock_traceability_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_traceability_report.getClass(), false);
        copier.copy(clientModel, stock_traceability_report, null);
    }


    public void removeBatch(List<Istock_traceability_report> stock_traceability_reports){
        if(stock_traceability_reports!=null){
            List<stock_traceability_reportImpl> list = new ArrayList<stock_traceability_reportImpl>();
            for(Istock_traceability_report istock_traceability_report :stock_traceability_reports){
                list.add((stock_traceability_reportImpl)istock_traceability_report) ;
            }
            stock_traceability_reportFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Istock_traceability_report> stock_traceability_reports){
        if(stock_traceability_reports!=null){
            List<stock_traceability_reportImpl> list = new ArrayList<stock_traceability_reportImpl>();
            for(Istock_traceability_report istock_traceability_report :stock_traceability_reports){
                list.add((stock_traceability_reportImpl)istock_traceability_report) ;
            }
            stock_traceability_reportFeignClient.createBatch(list) ;
        }
    }


    public void remove(Istock_traceability_report stock_traceability_report){
        stock_traceability_reportFeignClient.remove(stock_traceability_report.getId()) ;
    }


    public void updateBatch(List<Istock_traceability_report> stock_traceability_reports){
        if(stock_traceability_reports!=null){
            List<stock_traceability_reportImpl> list = new ArrayList<stock_traceability_reportImpl>();
            for(Istock_traceability_report istock_traceability_report :stock_traceability_reports){
                list.add((stock_traceability_reportImpl)istock_traceability_report) ;
            }
            stock_traceability_reportFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_traceability_report stock_traceability_report){
        Istock_traceability_report clientModel = stock_traceability_reportFeignClient.get(stock_traceability_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_traceability_report.getClass(), false);
        copier.copy(clientModel, stock_traceability_report, null);
    }


    public Page<Istock_traceability_report> fetchDefault(SearchContext context){
        Page<stock_traceability_reportImpl> page = this.stock_traceability_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Istock_traceability_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_traceability_report stock_traceability_report){
        Istock_traceability_report clientModel = stock_traceability_reportFeignClient.getDraft(stock_traceability_report.getId(),(stock_traceability_reportImpl)stock_traceability_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_traceability_report.getClass(), false);
        copier.copy(clientModel, stock_traceability_report, null);
    }



}

