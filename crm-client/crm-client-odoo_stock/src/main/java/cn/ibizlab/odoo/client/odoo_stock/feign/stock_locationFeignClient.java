package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_location;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_locationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_location] 服务对象接口
 */
public interface stock_locationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_locations/{id}")
    public stock_locationImpl update(@PathVariable("id") Integer id,@RequestBody stock_locationImpl stock_location);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_locations")
    public stock_locationImpl create(@RequestBody stock_locationImpl stock_location);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_locations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_locations/removebatch")
    public stock_locationImpl removeBatch(@RequestBody List<stock_locationImpl> stock_locations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_locations/{id}")
    public stock_locationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_locations/createbatch")
    public stock_locationImpl createBatch(@RequestBody List<stock_locationImpl> stock_locations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_locations/fetchdefault")
    public Page<stock_locationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_locations/updatebatch")
    public stock_locationImpl updateBatch(@RequestBody List<stock_locationImpl> stock_locations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_locations/select")
    public Page<stock_locationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_locations/{id}/getdraft")
    public stock_locationImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_locationImpl stock_location);



}
