package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_quant_package;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_quant_packageClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quant_packageImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_quant_packageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_quant_package] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_quant_packageClientServiceImpl implements Istock_quant_packageClientService {

    stock_quant_packageFeignClient stock_quant_packageFeignClient;

    @Autowired
    public stock_quant_packageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_quant_packageFeignClient = nameBuilder.target(stock_quant_packageFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_quant_packageFeignClient = nameBuilder.target(stock_quant_packageFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_quant_package createModel() {
		return new stock_quant_packageImpl();
	}


    public void removeBatch(List<Istock_quant_package> stock_quant_packages){
        if(stock_quant_packages!=null){
            List<stock_quant_packageImpl> list = new ArrayList<stock_quant_packageImpl>();
            for(Istock_quant_package istock_quant_package :stock_quant_packages){
                list.add((stock_quant_packageImpl)istock_quant_package) ;
            }
            stock_quant_packageFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Istock_quant_package> stock_quant_packages){
        if(stock_quant_packages!=null){
            List<stock_quant_packageImpl> list = new ArrayList<stock_quant_packageImpl>();
            for(Istock_quant_package istock_quant_package :stock_quant_packages){
                list.add((stock_quant_packageImpl)istock_quant_package) ;
            }
            stock_quant_packageFeignClient.updateBatch(list) ;
        }
    }


    public void update(Istock_quant_package stock_quant_package){
        Istock_quant_package clientModel = stock_quant_packageFeignClient.update(stock_quant_package.getId(),(stock_quant_packageImpl)stock_quant_package) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant_package.getClass(), false);
        copier.copy(clientModel, stock_quant_package, null);
    }


    public void get(Istock_quant_package stock_quant_package){
        Istock_quant_package clientModel = stock_quant_packageFeignClient.get(stock_quant_package.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant_package.getClass(), false);
        copier.copy(clientModel, stock_quant_package, null);
    }


    public Page<Istock_quant_package> fetchDefault(SearchContext context){
        Page<stock_quant_packageImpl> page = this.stock_quant_packageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Istock_quant_package stock_quant_package){
        Istock_quant_package clientModel = stock_quant_packageFeignClient.create((stock_quant_packageImpl)stock_quant_package) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant_package.getClass(), false);
        copier.copy(clientModel, stock_quant_package, null);
    }


    public void remove(Istock_quant_package stock_quant_package){
        stock_quant_packageFeignClient.remove(stock_quant_package.getId()) ;
    }


    public void createBatch(List<Istock_quant_package> stock_quant_packages){
        if(stock_quant_packages!=null){
            List<stock_quant_packageImpl> list = new ArrayList<stock_quant_packageImpl>();
            for(Istock_quant_package istock_quant_package :stock_quant_packages){
                list.add((stock_quant_packageImpl)istock_quant_package) ;
            }
            stock_quant_packageFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_quant_package> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_quant_package stock_quant_package){
        Istock_quant_package clientModel = stock_quant_packageFeignClient.getDraft(stock_quant_package.getId(),(stock_quant_packageImpl)stock_quant_package) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_quant_package.getClass(), false);
        copier.copy(clientModel, stock_quant_package, null);
    }



}

