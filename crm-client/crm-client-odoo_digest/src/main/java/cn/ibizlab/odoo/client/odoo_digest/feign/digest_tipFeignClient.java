package cn.ibizlab.odoo.client.odoo_digest.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Idigest_tip;
import cn.ibizlab.odoo.client.odoo_digest.model.digest_tipImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[digest_tip] 服务对象接口
 */
public interface digest_tipFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_tips/updatebatch")
    public digest_tipImpl updateBatch(@RequestBody List<digest_tipImpl> digest_tips);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_tips/fetchdefault")
    public Page<digest_tipImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_tips/{id}")
    public digest_tipImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_tips/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_tips/createbatch")
    public digest_tipImpl createBatch(@RequestBody List<digest_tipImpl> digest_tips);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_tips/{id}")
    public digest_tipImpl update(@PathVariable("id") Integer id,@RequestBody digest_tipImpl digest_tip);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_tips")
    public digest_tipImpl create(@RequestBody digest_tipImpl digest_tip);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_tips/removebatch")
    public digest_tipImpl removeBatch(@RequestBody List<digest_tipImpl> digest_tips);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_tips/select")
    public Page<digest_tipImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_tips/{id}/getdraft")
    public digest_tipImpl getDraft(@PathVariable("id") Integer id,@RequestBody digest_tipImpl digest_tip);



}
