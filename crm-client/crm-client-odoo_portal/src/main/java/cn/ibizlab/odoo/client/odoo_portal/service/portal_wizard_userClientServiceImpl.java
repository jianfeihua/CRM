package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard_user;
import cn.ibizlab.odoo.client.odoo_portal.config.odoo_portalClientProperties;
import cn.ibizlab.odoo.core.client.service.Iportal_wizard_userClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_wizard_userImpl;
import cn.ibizlab.odoo.client.odoo_portal.feign.portal_wizard_userFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[portal_wizard_user] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class portal_wizard_userClientServiceImpl implements Iportal_wizard_userClientService {

    portal_wizard_userFeignClient portal_wizard_userFeignClient;

    @Autowired
    public portal_wizard_userClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_portalClientProperties odoo_portalClientProperties) {
        if (odoo_portalClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_wizard_userFeignClient = nameBuilder.target(portal_wizard_userFeignClient.class,"http://"+odoo_portalClientProperties.getServiceId()+"/") ;
		}else if (odoo_portalClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_wizard_userFeignClient = nameBuilder.target(portal_wizard_userFeignClient.class,odoo_portalClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iportal_wizard_user createModel() {
		return new portal_wizard_userImpl();
	}


    public void get(Iportal_wizard_user portal_wizard_user){
        Iportal_wizard_user clientModel = portal_wizard_userFeignClient.get(portal_wizard_user.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard_user.getClass(), false);
        copier.copy(clientModel, portal_wizard_user, null);
    }


    public void create(Iportal_wizard_user portal_wizard_user){
        Iportal_wizard_user clientModel = portal_wizard_userFeignClient.create((portal_wizard_userImpl)portal_wizard_user) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard_user.getClass(), false);
        copier.copy(clientModel, portal_wizard_user, null);
    }


    public void createBatch(List<Iportal_wizard_user> portal_wizard_users){
        if(portal_wizard_users!=null){
            List<portal_wizard_userImpl> list = new ArrayList<portal_wizard_userImpl>();
            for(Iportal_wizard_user iportal_wizard_user :portal_wizard_users){
                list.add((portal_wizard_userImpl)iportal_wizard_user) ;
            }
            portal_wizard_userFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iportal_wizard_user> portal_wizard_users){
        if(portal_wizard_users!=null){
            List<portal_wizard_userImpl> list = new ArrayList<portal_wizard_userImpl>();
            for(Iportal_wizard_user iportal_wizard_user :portal_wizard_users){
                list.add((portal_wizard_userImpl)iportal_wizard_user) ;
            }
            portal_wizard_userFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iportal_wizard_user> portal_wizard_users){
        if(portal_wizard_users!=null){
            List<portal_wizard_userImpl> list = new ArrayList<portal_wizard_userImpl>();
            for(Iportal_wizard_user iportal_wizard_user :portal_wizard_users){
                list.add((portal_wizard_userImpl)iportal_wizard_user) ;
            }
            portal_wizard_userFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iportal_wizard_user> fetchDefault(SearchContext context){
        Page<portal_wizard_userImpl> page = this.portal_wizard_userFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iportal_wizard_user portal_wizard_user){
        portal_wizard_userFeignClient.remove(portal_wizard_user.getId()) ;
    }


    public void update(Iportal_wizard_user portal_wizard_user){
        Iportal_wizard_user clientModel = portal_wizard_userFeignClient.update(portal_wizard_user.getId(),(portal_wizard_userImpl)portal_wizard_user) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard_user.getClass(), false);
        copier.copy(clientModel, portal_wizard_user, null);
    }


    public Page<Iportal_wizard_user> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iportal_wizard_user portal_wizard_user){
        Iportal_wizard_user clientModel = portal_wizard_userFeignClient.getDraft(portal_wizard_user.getId(),(portal_wizard_userImpl)portal_wizard_user) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard_user.getClass(), false);
        copier.copy(clientModel, portal_wizard_user, null);
    }



}

