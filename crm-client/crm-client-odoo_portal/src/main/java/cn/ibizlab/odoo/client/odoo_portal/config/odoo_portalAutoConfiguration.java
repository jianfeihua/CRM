package cn.ibizlab.odoo.client.odoo_portal.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(odoo_portalClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(odoo_portalClientProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class odoo_portalAutoConfiguration {

}
