package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_partner_binding;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_partner_bindingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_partner_binding] 服务对象接口
 */
public interface crm_partner_bindingFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_partner_bindings/{id}")
    public crm_partner_bindingImpl update(@PathVariable("id") Integer id,@RequestBody crm_partner_bindingImpl crm_partner_binding);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_partner_bindings/updatebatch")
    public crm_partner_bindingImpl updateBatch(@RequestBody List<crm_partner_bindingImpl> crm_partner_bindings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_partner_bindings/removebatch")
    public crm_partner_bindingImpl removeBatch(@RequestBody List<crm_partner_bindingImpl> crm_partner_bindings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_partner_bindings/{id}")
    public crm_partner_bindingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_partner_bindings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_partner_bindings/fetchdefault")
    public Page<crm_partner_bindingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_partner_bindings/createbatch")
    public crm_partner_bindingImpl createBatch(@RequestBody List<crm_partner_bindingImpl> crm_partner_bindings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_partner_bindings")
    public crm_partner_bindingImpl create(@RequestBody crm_partner_bindingImpl crm_partner_binding);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_partner_bindings/select")
    public Page<crm_partner_bindingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_partner_bindings/{id}/getdraft")
    public crm_partner_bindingImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_partner_bindingImpl crm_partner_binding);



}
