package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_leadClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_leadImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_leadFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_lead] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_leadClientServiceImpl implements Icrm_leadClientService {

    crm_leadFeignClient crm_leadFeignClient;

    @Autowired
    public crm_leadClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_leadFeignClient = nameBuilder.target(crm_leadFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_leadFeignClient = nameBuilder.target(crm_leadFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_lead createModel() {
		return new crm_leadImpl();
	}


    public void update(Icrm_lead crm_lead){
        Icrm_lead clientModel = crm_leadFeignClient.update(crm_lead.getId(),(crm_leadImpl)crm_lead) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead.getClass(), false);
        copier.copy(clientModel, crm_lead, null);
    }


    public void updateBatch(List<Icrm_lead> crm_leads){
        if(crm_leads!=null){
            List<crm_leadImpl> list = new ArrayList<crm_leadImpl>();
            for(Icrm_lead icrm_lead :crm_leads){
                list.add((crm_leadImpl)icrm_lead) ;
            }
            crm_leadFeignClient.updateBatch(list) ;
        }
    }


    public Page<Icrm_lead> fetchDefault(SearchContext context){
        Page<crm_leadImpl> page = this.crm_leadFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Icrm_lead crm_lead){
        Icrm_lead clientModel = crm_leadFeignClient.create((crm_leadImpl)crm_lead) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead.getClass(), false);
        copier.copy(clientModel, crm_lead, null);
    }


    public void createBatch(List<Icrm_lead> crm_leads){
        if(crm_leads!=null){
            List<crm_leadImpl> list = new ArrayList<crm_leadImpl>();
            for(Icrm_lead icrm_lead :crm_leads){
                list.add((crm_leadImpl)icrm_lead) ;
            }
            crm_leadFeignClient.createBatch(list) ;
        }
    }


    public void get(Icrm_lead crm_lead){
        Icrm_lead clientModel = crm_leadFeignClient.get(crm_lead.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead.getClass(), false);
        copier.copy(clientModel, crm_lead, null);
    }


    public void removeBatch(List<Icrm_lead> crm_leads){
        if(crm_leads!=null){
            List<crm_leadImpl> list = new ArrayList<crm_leadImpl>();
            for(Icrm_lead icrm_lead :crm_leads){
                list.add((crm_leadImpl)icrm_lead) ;
            }
            crm_leadFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Icrm_lead crm_lead){
        crm_leadFeignClient.remove(crm_lead.getId()) ;
    }


    public Page<Icrm_lead> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_lead crm_lead){
        Icrm_lead clientModel = crm_leadFeignClient.getDraft(crm_lead.getId(),(crm_leadImpl)crm_lead) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead.getClass(), false);
        copier.copy(clientModel, crm_lead, null);
    }


    public void save(Icrm_lead crm_lead){
        Icrm_lead clientModel = crm_leadFeignClient.save(crm_lead.getId(),(crm_leadImpl)crm_lead) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead.getClass(), false);
        copier.copy(clientModel, crm_lead, null);
    }


    public void checkKey(Icrm_lead crm_lead){
        Icrm_lead clientModel = crm_leadFeignClient.checkKey(crm_lead.getId(),(crm_leadImpl)crm_lead) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead.getClass(), false);
        copier.copy(clientModel, crm_lead, null);
    }



}

