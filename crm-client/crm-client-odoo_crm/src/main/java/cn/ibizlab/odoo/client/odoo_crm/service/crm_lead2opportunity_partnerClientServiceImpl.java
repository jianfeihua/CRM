package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_lead2opportunity_partnerClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead2opportunity_partnerImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_lead2opportunity_partnerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_lead2opportunity_partner] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_lead2opportunity_partnerClientServiceImpl implements Icrm_lead2opportunity_partnerClientService {

    crm_lead2opportunity_partnerFeignClient crm_lead2opportunity_partnerFeignClient;

    @Autowired
    public crm_lead2opportunity_partnerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead2opportunity_partnerFeignClient = nameBuilder.target(crm_lead2opportunity_partnerFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead2opportunity_partnerFeignClient = nameBuilder.target(crm_lead2opportunity_partnerFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_lead2opportunity_partner createModel() {
		return new crm_lead2opportunity_partnerImpl();
	}


    public void createBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners){
        if(crm_lead2opportunity_partners!=null){
            List<crm_lead2opportunity_partnerImpl> list = new ArrayList<crm_lead2opportunity_partnerImpl>();
            for(Icrm_lead2opportunity_partner icrm_lead2opportunity_partner :crm_lead2opportunity_partners){
                list.add((crm_lead2opportunity_partnerImpl)icrm_lead2opportunity_partner) ;
            }
            crm_lead2opportunity_partnerFeignClient.createBatch(list) ;
        }
    }


    public void update(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
        Icrm_lead2opportunity_partner clientModel = crm_lead2opportunity_partnerFeignClient.update(crm_lead2opportunity_partner.getId(),(crm_lead2opportunity_partnerImpl)crm_lead2opportunity_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner, null);
    }


    public void removeBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners){
        if(crm_lead2opportunity_partners!=null){
            List<crm_lead2opportunity_partnerImpl> list = new ArrayList<crm_lead2opportunity_partnerImpl>();
            for(Icrm_lead2opportunity_partner icrm_lead2opportunity_partner :crm_lead2opportunity_partners){
                list.add((crm_lead2opportunity_partnerImpl)icrm_lead2opportunity_partner) ;
            }
            crm_lead2opportunity_partnerFeignClient.removeBatch(list) ;
        }
    }


    public void get(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
        Icrm_lead2opportunity_partner clientModel = crm_lead2opportunity_partnerFeignClient.get(crm_lead2opportunity_partner.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner, null);
    }


    public void create(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
        Icrm_lead2opportunity_partner clientModel = crm_lead2opportunity_partnerFeignClient.create((crm_lead2opportunity_partnerImpl)crm_lead2opportunity_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner, null);
    }


    public void updateBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners){
        if(crm_lead2opportunity_partners!=null){
            List<crm_lead2opportunity_partnerImpl> list = new ArrayList<crm_lead2opportunity_partnerImpl>();
            for(Icrm_lead2opportunity_partner icrm_lead2opportunity_partner :crm_lead2opportunity_partners){
                list.add((crm_lead2opportunity_partnerImpl)icrm_lead2opportunity_partner) ;
            }
            crm_lead2opportunity_partnerFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
        crm_lead2opportunity_partnerFeignClient.remove(crm_lead2opportunity_partner.getId()) ;
    }


    public Page<Icrm_lead2opportunity_partner> fetchDefault(SearchContext context){
        Page<crm_lead2opportunity_partnerImpl> page = this.crm_lead2opportunity_partnerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Icrm_lead2opportunity_partner> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
        Icrm_lead2opportunity_partner clientModel = crm_lead2opportunity_partnerFeignClient.getDraft(crm_lead2opportunity_partner.getId(),(crm_lead2opportunity_partnerImpl)crm_lead2opportunity_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner, null);
    }



}

