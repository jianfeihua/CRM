package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead2opportunity_partner_massImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_lead2opportunity_partner_mass] 服务对象接口
 */
public interface crm_lead2opportunity_partner_massFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead2opportunity_partner_masses/updatebatch")
    public crm_lead2opportunity_partner_massImpl updateBatch(@RequestBody List<crm_lead2opportunity_partner_massImpl> crm_lead2opportunity_partner_masses);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partner_masses/fetchdefault")
    public Page<crm_lead2opportunity_partner_massImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead2opportunity_partner_masses/{id}")
    public crm_lead2opportunity_partner_massImpl update(@PathVariable("id") Integer id,@RequestBody crm_lead2opportunity_partner_massImpl crm_lead2opportunity_partner_mass);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead2opportunity_partner_masses/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partner_masses/{id}")
    public crm_lead2opportunity_partner_massImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead2opportunity_partner_masses")
    public crm_lead2opportunity_partner_massImpl create(@RequestBody crm_lead2opportunity_partner_massImpl crm_lead2opportunity_partner_mass);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead2opportunity_partner_masses/removebatch")
    public crm_lead2opportunity_partner_massImpl removeBatch(@RequestBody List<crm_lead2opportunity_partner_massImpl> crm_lead2opportunity_partner_masses);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead2opportunity_partner_masses/createbatch")
    public crm_lead2opportunity_partner_massImpl createBatch(@RequestBody List<crm_lead2opportunity_partner_massImpl> crm_lead2opportunity_partner_masses);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partner_masses/select")
    public Page<crm_lead2opportunity_partner_massImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partner_masses/{id}/getdraft")
    public crm_lead2opportunity_partner_massImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_lead2opportunity_partner_massImpl crm_lead2opportunity_partner_mass);



}
