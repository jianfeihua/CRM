package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead2opportunity_partnerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_lead2opportunity_partner] 服务对象接口
 */
public interface crm_lead2opportunity_partnerFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead2opportunity_partners/createbatch")
    public crm_lead2opportunity_partnerImpl createBatch(@RequestBody List<crm_lead2opportunity_partnerImpl> crm_lead2opportunity_partners);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead2opportunity_partners/{id}")
    public crm_lead2opportunity_partnerImpl update(@PathVariable("id") Integer id,@RequestBody crm_lead2opportunity_partnerImpl crm_lead2opportunity_partner);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead2opportunity_partners/removebatch")
    public crm_lead2opportunity_partnerImpl removeBatch(@RequestBody List<crm_lead2opportunity_partnerImpl> crm_lead2opportunity_partners);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partners/{id}")
    public crm_lead2opportunity_partnerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead2opportunity_partners")
    public crm_lead2opportunity_partnerImpl create(@RequestBody crm_lead2opportunity_partnerImpl crm_lead2opportunity_partner);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead2opportunity_partners/updatebatch")
    public crm_lead2opportunity_partnerImpl updateBatch(@RequestBody List<crm_lead2opportunity_partnerImpl> crm_lead2opportunity_partners);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead2opportunity_partners/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partners/fetchdefault")
    public Page<crm_lead2opportunity_partnerImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partners/select")
    public Page<crm_lead2opportunity_partnerImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partners/{id}/getdraft")
    public crm_lead2opportunity_partnerImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_lead2opportunity_partnerImpl crm_lead2opportunity_partner);



}
