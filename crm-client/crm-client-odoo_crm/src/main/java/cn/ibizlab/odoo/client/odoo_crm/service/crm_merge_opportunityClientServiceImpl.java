package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_merge_opportunity;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_merge_opportunityClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_merge_opportunityImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_merge_opportunityFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_merge_opportunity] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_merge_opportunityClientServiceImpl implements Icrm_merge_opportunityClientService {

    crm_merge_opportunityFeignClient crm_merge_opportunityFeignClient;

    @Autowired
    public crm_merge_opportunityClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_merge_opportunityFeignClient = nameBuilder.target(crm_merge_opportunityFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_merge_opportunityFeignClient = nameBuilder.target(crm_merge_opportunityFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_merge_opportunity createModel() {
		return new crm_merge_opportunityImpl();
	}


    public void createBatch(List<Icrm_merge_opportunity> crm_merge_opportunities){
        if(crm_merge_opportunities!=null){
            List<crm_merge_opportunityImpl> list = new ArrayList<crm_merge_opportunityImpl>();
            for(Icrm_merge_opportunity icrm_merge_opportunity :crm_merge_opportunities){
                list.add((crm_merge_opportunityImpl)icrm_merge_opportunity) ;
            }
            crm_merge_opportunityFeignClient.createBatch(list) ;
        }
    }


    public void remove(Icrm_merge_opportunity crm_merge_opportunity){
        crm_merge_opportunityFeignClient.remove(crm_merge_opportunity.getId()) ;
    }


    public void updateBatch(List<Icrm_merge_opportunity> crm_merge_opportunities){
        if(crm_merge_opportunities!=null){
            List<crm_merge_opportunityImpl> list = new ArrayList<crm_merge_opportunityImpl>();
            for(Icrm_merge_opportunity icrm_merge_opportunity :crm_merge_opportunities){
                list.add((crm_merge_opportunityImpl)icrm_merge_opportunity) ;
            }
            crm_merge_opportunityFeignClient.updateBatch(list) ;
        }
    }


    public Page<Icrm_merge_opportunity> fetchDefault(SearchContext context){
        Page<crm_merge_opportunityImpl> page = this.crm_merge_opportunityFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Icrm_merge_opportunity crm_merge_opportunity){
        Icrm_merge_opportunity clientModel = crm_merge_opportunityFeignClient.update(crm_merge_opportunity.getId(),(crm_merge_opportunityImpl)crm_merge_opportunity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_merge_opportunity.getClass(), false);
        copier.copy(clientModel, crm_merge_opportunity, null);
    }


    public void removeBatch(List<Icrm_merge_opportunity> crm_merge_opportunities){
        if(crm_merge_opportunities!=null){
            List<crm_merge_opportunityImpl> list = new ArrayList<crm_merge_opportunityImpl>();
            for(Icrm_merge_opportunity icrm_merge_opportunity :crm_merge_opportunities){
                list.add((crm_merge_opportunityImpl)icrm_merge_opportunity) ;
            }
            crm_merge_opportunityFeignClient.removeBatch(list) ;
        }
    }


    public void create(Icrm_merge_opportunity crm_merge_opportunity){
        Icrm_merge_opportunity clientModel = crm_merge_opportunityFeignClient.create((crm_merge_opportunityImpl)crm_merge_opportunity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_merge_opportunity.getClass(), false);
        copier.copy(clientModel, crm_merge_opportunity, null);
    }


    public void get(Icrm_merge_opportunity crm_merge_opportunity){
        Icrm_merge_opportunity clientModel = crm_merge_opportunityFeignClient.get(crm_merge_opportunity.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_merge_opportunity.getClass(), false);
        copier.copy(clientModel, crm_merge_opportunity, null);
    }


    public Page<Icrm_merge_opportunity> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_merge_opportunity crm_merge_opportunity){
        Icrm_merge_opportunity clientModel = crm_merge_opportunityFeignClient.getDraft(crm_merge_opportunity.getId(),(crm_merge_opportunityImpl)crm_merge_opportunity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_merge_opportunity.getClass(), false);
        copier.copy(clientModel, crm_merge_opportunity, null);
    }



}

