package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_termImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_payment_term] 服务对象接口
 */
public interface account_payment_termFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_terms")
    public account_payment_termImpl create(@RequestBody account_payment_termImpl account_payment_term);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_terms/createbatch")
    public account_payment_termImpl createBatch(@RequestBody List<account_payment_termImpl> account_payment_terms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_terms/updatebatch")
    public account_payment_termImpl updateBatch(@RequestBody List<account_payment_termImpl> account_payment_terms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_terms/fetchdefault")
    public Page<account_payment_termImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_terms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_terms/removebatch")
    public account_payment_termImpl removeBatch(@RequestBody List<account_payment_termImpl> account_payment_terms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_terms/{id}")
    public account_payment_termImpl update(@PathVariable("id") Integer id,@RequestBody account_payment_termImpl account_payment_term);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_terms/{id}")
    public account_payment_termImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_terms/select")
    public Page<account_payment_termImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_terms/{id}/getdraft")
    public account_payment_termImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_payment_termImpl account_payment_term);



}
