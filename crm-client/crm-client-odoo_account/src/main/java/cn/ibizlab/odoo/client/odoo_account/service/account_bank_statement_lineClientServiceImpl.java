package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_line;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_bank_statement_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_bank_statement_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_bank_statement_lineClientServiceImpl implements Iaccount_bank_statement_lineClientService {

    account_bank_statement_lineFeignClient account_bank_statement_lineFeignClient;

    @Autowired
    public account_bank_statement_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_lineFeignClient = nameBuilder.target(account_bank_statement_lineFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_lineFeignClient = nameBuilder.target(account_bank_statement_lineFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_bank_statement_line createModel() {
		return new account_bank_statement_lineImpl();
	}


    public void remove(Iaccount_bank_statement_line account_bank_statement_line){
        account_bank_statement_lineFeignClient.remove(account_bank_statement_line.getId()) ;
    }


    public void updateBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines){
        if(account_bank_statement_lines!=null){
            List<account_bank_statement_lineImpl> list = new ArrayList<account_bank_statement_lineImpl>();
            for(Iaccount_bank_statement_line iaccount_bank_statement_line :account_bank_statement_lines){
                list.add((account_bank_statement_lineImpl)iaccount_bank_statement_line) ;
            }
            account_bank_statement_lineFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_bank_statement_line> fetchDefault(SearchContext context){
        Page<account_bank_statement_lineImpl> page = this.account_bank_statement_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines){
        if(account_bank_statement_lines!=null){
            List<account_bank_statement_lineImpl> list = new ArrayList<account_bank_statement_lineImpl>();
            for(Iaccount_bank_statement_line iaccount_bank_statement_line :account_bank_statement_lines){
                list.add((account_bank_statement_lineImpl)iaccount_bank_statement_line) ;
            }
            account_bank_statement_lineFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_bank_statement_line account_bank_statement_line){
        Iaccount_bank_statement_line clientModel = account_bank_statement_lineFeignClient.get(account_bank_statement_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_line.getClass(), false);
        copier.copy(clientModel, account_bank_statement_line, null);
    }


    public void removeBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines){
        if(account_bank_statement_lines!=null){
            List<account_bank_statement_lineImpl> list = new ArrayList<account_bank_statement_lineImpl>();
            for(Iaccount_bank_statement_line iaccount_bank_statement_line :account_bank_statement_lines){
                list.add((account_bank_statement_lineImpl)iaccount_bank_statement_line) ;
            }
            account_bank_statement_lineFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iaccount_bank_statement_line account_bank_statement_line){
        Iaccount_bank_statement_line clientModel = account_bank_statement_lineFeignClient.update(account_bank_statement_line.getId(),(account_bank_statement_lineImpl)account_bank_statement_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_line.getClass(), false);
        copier.copy(clientModel, account_bank_statement_line, null);
    }


    public void create(Iaccount_bank_statement_line account_bank_statement_line){
        Iaccount_bank_statement_line clientModel = account_bank_statement_lineFeignClient.create((account_bank_statement_lineImpl)account_bank_statement_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_line.getClass(), false);
        copier.copy(clientModel, account_bank_statement_line, null);
    }


    public Page<Iaccount_bank_statement_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_bank_statement_line account_bank_statement_line){
        Iaccount_bank_statement_line clientModel = account_bank_statement_lineFeignClient.getDraft(account_bank_statement_line.getId(),(account_bank_statement_lineImpl)account_bank_statement_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_line.getClass(), false);
        copier.copy(clientModel, account_bank_statement_line, null);
    }



}

