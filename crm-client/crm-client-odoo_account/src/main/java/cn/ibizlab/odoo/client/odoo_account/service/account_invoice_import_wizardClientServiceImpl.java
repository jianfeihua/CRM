package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_import_wizard;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_import_wizardClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_import_wizardImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoice_import_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice_import_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoice_import_wizardClientServiceImpl implements Iaccount_invoice_import_wizardClientService {

    account_invoice_import_wizardFeignClient account_invoice_import_wizardFeignClient;

    @Autowired
    public account_invoice_import_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_import_wizardFeignClient = nameBuilder.target(account_invoice_import_wizardFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_import_wizardFeignClient = nameBuilder.target(account_invoice_import_wizardFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice_import_wizard createModel() {
		return new account_invoice_import_wizardImpl();
	}


    public void updateBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards){
        if(account_invoice_import_wizards!=null){
            List<account_invoice_import_wizardImpl> list = new ArrayList<account_invoice_import_wizardImpl>();
            for(Iaccount_invoice_import_wizard iaccount_invoice_import_wizard :account_invoice_import_wizards){
                list.add((account_invoice_import_wizardImpl)iaccount_invoice_import_wizard) ;
            }
            account_invoice_import_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_invoice_import_wizard account_invoice_import_wizard){
        account_invoice_import_wizardFeignClient.remove(account_invoice_import_wizard.getId()) ;
    }


    public void create(Iaccount_invoice_import_wizard account_invoice_import_wizard){
        Iaccount_invoice_import_wizard clientModel = account_invoice_import_wizardFeignClient.create((account_invoice_import_wizardImpl)account_invoice_import_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_import_wizard.getClass(), false);
        copier.copy(clientModel, account_invoice_import_wizard, null);
    }


    public void removeBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards){
        if(account_invoice_import_wizards!=null){
            List<account_invoice_import_wizardImpl> list = new ArrayList<account_invoice_import_wizardImpl>();
            for(Iaccount_invoice_import_wizard iaccount_invoice_import_wizard :account_invoice_import_wizards){
                list.add((account_invoice_import_wizardImpl)iaccount_invoice_import_wizard) ;
            }
            account_invoice_import_wizardFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards){
        if(account_invoice_import_wizards!=null){
            List<account_invoice_import_wizardImpl> list = new ArrayList<account_invoice_import_wizardImpl>();
            for(Iaccount_invoice_import_wizard iaccount_invoice_import_wizard :account_invoice_import_wizards){
                list.add((account_invoice_import_wizardImpl)iaccount_invoice_import_wizard) ;
            }
            account_invoice_import_wizardFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_invoice_import_wizard account_invoice_import_wizard){
        Iaccount_invoice_import_wizard clientModel = account_invoice_import_wizardFeignClient.get(account_invoice_import_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_import_wizard.getClass(), false);
        copier.copy(clientModel, account_invoice_import_wizard, null);
    }


    public void update(Iaccount_invoice_import_wizard account_invoice_import_wizard){
        Iaccount_invoice_import_wizard clientModel = account_invoice_import_wizardFeignClient.update(account_invoice_import_wizard.getId(),(account_invoice_import_wizardImpl)account_invoice_import_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_import_wizard.getClass(), false);
        copier.copy(clientModel, account_invoice_import_wizard, null);
    }


    public Page<Iaccount_invoice_import_wizard> fetchDefault(SearchContext context){
        Page<account_invoice_import_wizardImpl> page = this.account_invoice_import_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_invoice_import_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_invoice_import_wizard account_invoice_import_wizard){
        Iaccount_invoice_import_wizard clientModel = account_invoice_import_wizardFeignClient.getDraft(account_invoice_import_wizard.getId(),(account_invoice_import_wizardImpl)account_invoice_import_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_import_wizard.getClass(), false);
        copier.copy(clientModel, account_invoice_import_wizard, null);
    }



}

