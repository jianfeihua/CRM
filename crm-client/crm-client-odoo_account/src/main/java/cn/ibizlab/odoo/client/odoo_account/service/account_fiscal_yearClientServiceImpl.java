package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_year;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_yearClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_yearImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_fiscal_yearFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_fiscal_year] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_fiscal_yearClientServiceImpl implements Iaccount_fiscal_yearClientService {

    account_fiscal_yearFeignClient account_fiscal_yearFeignClient;

    @Autowired
    public account_fiscal_yearClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_yearFeignClient = nameBuilder.target(account_fiscal_yearFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_yearFeignClient = nameBuilder.target(account_fiscal_yearFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_fiscal_year createModel() {
		return new account_fiscal_yearImpl();
	}


    public void removeBatch(List<Iaccount_fiscal_year> account_fiscal_years){
        if(account_fiscal_years!=null){
            List<account_fiscal_yearImpl> list = new ArrayList<account_fiscal_yearImpl>();
            for(Iaccount_fiscal_year iaccount_fiscal_year :account_fiscal_years){
                list.add((account_fiscal_yearImpl)iaccount_fiscal_year) ;
            }
            account_fiscal_yearFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_fiscal_year> fetchDefault(SearchContext context){
        Page<account_fiscal_yearImpl> page = this.account_fiscal_yearFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_fiscal_year> account_fiscal_years){
        if(account_fiscal_years!=null){
            List<account_fiscal_yearImpl> list = new ArrayList<account_fiscal_yearImpl>();
            for(Iaccount_fiscal_year iaccount_fiscal_year :account_fiscal_years){
                list.add((account_fiscal_yearImpl)iaccount_fiscal_year) ;
            }
            account_fiscal_yearFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iaccount_fiscal_year account_fiscal_year){
        Iaccount_fiscal_year clientModel = account_fiscal_yearFeignClient.get(account_fiscal_year.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_year.getClass(), false);
        copier.copy(clientModel, account_fiscal_year, null);
    }


    public void remove(Iaccount_fiscal_year account_fiscal_year){
        account_fiscal_yearFeignClient.remove(account_fiscal_year.getId()) ;
    }


    public void createBatch(List<Iaccount_fiscal_year> account_fiscal_years){
        if(account_fiscal_years!=null){
            List<account_fiscal_yearImpl> list = new ArrayList<account_fiscal_yearImpl>();
            for(Iaccount_fiscal_year iaccount_fiscal_year :account_fiscal_years){
                list.add((account_fiscal_yearImpl)iaccount_fiscal_year) ;
            }
            account_fiscal_yearFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_fiscal_year account_fiscal_year){
        Iaccount_fiscal_year clientModel = account_fiscal_yearFeignClient.update(account_fiscal_year.getId(),(account_fiscal_yearImpl)account_fiscal_year) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_year.getClass(), false);
        copier.copy(clientModel, account_fiscal_year, null);
    }


    public void create(Iaccount_fiscal_year account_fiscal_year){
        Iaccount_fiscal_year clientModel = account_fiscal_yearFeignClient.create((account_fiscal_yearImpl)account_fiscal_year) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_year.getClass(), false);
        copier.copy(clientModel, account_fiscal_year, null);
    }


    public Page<Iaccount_fiscal_year> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_fiscal_year account_fiscal_year){
        Iaccount_fiscal_year clientModel = account_fiscal_yearFeignClient.getDraft(account_fiscal_year.getId(),(account_fiscal_yearImpl)account_fiscal_year) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_year.getClass(), false);
        copier.copy(clientModel, account_fiscal_year, null);
    }



}

