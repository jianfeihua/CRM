package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_unreconcile;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_unreconcileClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_unreconcileImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_unreconcileFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_unreconcile] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_unreconcileClientServiceImpl implements Iaccount_unreconcileClientService {

    account_unreconcileFeignClient account_unreconcileFeignClient;

    @Autowired
    public account_unreconcileClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_unreconcileFeignClient = nameBuilder.target(account_unreconcileFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_unreconcileFeignClient = nameBuilder.target(account_unreconcileFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_unreconcile createModel() {
		return new account_unreconcileImpl();
	}


    public Page<Iaccount_unreconcile> fetchDefault(SearchContext context){
        Page<account_unreconcileImpl> page = this.account_unreconcileFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iaccount_unreconcile account_unreconcile){
        Iaccount_unreconcile clientModel = account_unreconcileFeignClient.get(account_unreconcile.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_unreconcile.getClass(), false);
        copier.copy(clientModel, account_unreconcile, null);
    }


    public void createBatch(List<Iaccount_unreconcile> account_unreconciles){
        if(account_unreconciles!=null){
            List<account_unreconcileImpl> list = new ArrayList<account_unreconcileImpl>();
            for(Iaccount_unreconcile iaccount_unreconcile :account_unreconciles){
                list.add((account_unreconcileImpl)iaccount_unreconcile) ;
            }
            account_unreconcileFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_unreconcile> account_unreconciles){
        if(account_unreconciles!=null){
            List<account_unreconcileImpl> list = new ArrayList<account_unreconcileImpl>();
            for(Iaccount_unreconcile iaccount_unreconcile :account_unreconciles){
                list.add((account_unreconcileImpl)iaccount_unreconcile) ;
            }
            account_unreconcileFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iaccount_unreconcile account_unreconcile){
        Iaccount_unreconcile clientModel = account_unreconcileFeignClient.create((account_unreconcileImpl)account_unreconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_unreconcile.getClass(), false);
        copier.copy(clientModel, account_unreconcile, null);
    }


    public void updateBatch(List<Iaccount_unreconcile> account_unreconciles){
        if(account_unreconciles!=null){
            List<account_unreconcileImpl> list = new ArrayList<account_unreconcileImpl>();
            for(Iaccount_unreconcile iaccount_unreconcile :account_unreconciles){
                list.add((account_unreconcileImpl)iaccount_unreconcile) ;
            }
            account_unreconcileFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iaccount_unreconcile account_unreconcile){
        Iaccount_unreconcile clientModel = account_unreconcileFeignClient.update(account_unreconcile.getId(),(account_unreconcileImpl)account_unreconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_unreconcile.getClass(), false);
        copier.copy(clientModel, account_unreconcile, null);
    }


    public void remove(Iaccount_unreconcile account_unreconcile){
        account_unreconcileFeignClient.remove(account_unreconcile.getId()) ;
    }


    public Page<Iaccount_unreconcile> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_unreconcile account_unreconcile){
        Iaccount_unreconcile clientModel = account_unreconcileFeignClient.getDraft(account_unreconcile.getId(),(account_unreconcileImpl)account_unreconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_unreconcile.getClass(), false);
        copier.copy(clientModel, account_unreconcile, null);
    }



}

