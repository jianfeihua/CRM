package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_tag;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_account_tagClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_tagImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_account_tagFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_account_tag] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_account_tagClientServiceImpl implements Iaccount_account_tagClientService {

    account_account_tagFeignClient account_account_tagFeignClient;

    @Autowired
    public account_account_tagClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_account_tagFeignClient = nameBuilder.target(account_account_tagFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_account_tagFeignClient = nameBuilder.target(account_account_tagFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_account_tag createModel() {
		return new account_account_tagImpl();
	}


    public void removeBatch(List<Iaccount_account_tag> account_account_tags){
        if(account_account_tags!=null){
            List<account_account_tagImpl> list = new ArrayList<account_account_tagImpl>();
            for(Iaccount_account_tag iaccount_account_tag :account_account_tags){
                list.add((account_account_tagImpl)iaccount_account_tag) ;
            }
            account_account_tagFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iaccount_account_tag account_account_tag){
        Iaccount_account_tag clientModel = account_account_tagFeignClient.create((account_account_tagImpl)account_account_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_tag.getClass(), false);
        copier.copy(clientModel, account_account_tag, null);
    }


    public void updateBatch(List<Iaccount_account_tag> account_account_tags){
        if(account_account_tags!=null){
            List<account_account_tagImpl> list = new ArrayList<account_account_tagImpl>();
            for(Iaccount_account_tag iaccount_account_tag :account_account_tags){
                list.add((account_account_tagImpl)iaccount_account_tag) ;
            }
            account_account_tagFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_account_tag> account_account_tags){
        if(account_account_tags!=null){
            List<account_account_tagImpl> list = new ArrayList<account_account_tagImpl>();
            for(Iaccount_account_tag iaccount_account_tag :account_account_tags){
                list.add((account_account_tagImpl)iaccount_account_tag) ;
            }
            account_account_tagFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_account_tag> fetchDefault(SearchContext context){
        Page<account_account_tagImpl> page = this.account_account_tagFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_account_tag account_account_tag){
        Iaccount_account_tag clientModel = account_account_tagFeignClient.update(account_account_tag.getId(),(account_account_tagImpl)account_account_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_tag.getClass(), false);
        copier.copy(clientModel, account_account_tag, null);
    }


    public void remove(Iaccount_account_tag account_account_tag){
        account_account_tagFeignClient.remove(account_account_tag.getId()) ;
    }


    public void get(Iaccount_account_tag account_account_tag){
        Iaccount_account_tag clientModel = account_account_tagFeignClient.get(account_account_tag.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_tag.getClass(), false);
        copier.copy(clientModel, account_account_tag, null);
    }


    public Page<Iaccount_account_tag> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_account_tag account_account_tag){
        Iaccount_account_tag clientModel = account_account_tagFeignClient.getDraft(account_account_tag.getId(),(account_account_tagImpl)account_account_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_tag.getClass(), false);
        copier.copy(clientModel, account_account_tag, null);
    }



}

