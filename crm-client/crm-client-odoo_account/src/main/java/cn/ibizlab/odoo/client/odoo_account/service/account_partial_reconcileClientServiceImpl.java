package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_partial_reconcile;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_partial_reconcileClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_partial_reconcileImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_partial_reconcileFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_partial_reconcile] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_partial_reconcileClientServiceImpl implements Iaccount_partial_reconcileClientService {

    account_partial_reconcileFeignClient account_partial_reconcileFeignClient;

    @Autowired
    public account_partial_reconcileClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_partial_reconcileFeignClient = nameBuilder.target(account_partial_reconcileFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_partial_reconcileFeignClient = nameBuilder.target(account_partial_reconcileFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_partial_reconcile createModel() {
		return new account_partial_reconcileImpl();
	}


    public void get(Iaccount_partial_reconcile account_partial_reconcile){
        Iaccount_partial_reconcile clientModel = account_partial_reconcileFeignClient.get(account_partial_reconcile.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_partial_reconcile.getClass(), false);
        copier.copy(clientModel, account_partial_reconcile, null);
    }


    public void remove(Iaccount_partial_reconcile account_partial_reconcile){
        account_partial_reconcileFeignClient.remove(account_partial_reconcile.getId()) ;
    }


    public void update(Iaccount_partial_reconcile account_partial_reconcile){
        Iaccount_partial_reconcile clientModel = account_partial_reconcileFeignClient.update(account_partial_reconcile.getId(),(account_partial_reconcileImpl)account_partial_reconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_partial_reconcile.getClass(), false);
        copier.copy(clientModel, account_partial_reconcile, null);
    }


    public void updateBatch(List<Iaccount_partial_reconcile> account_partial_reconciles){
        if(account_partial_reconciles!=null){
            List<account_partial_reconcileImpl> list = new ArrayList<account_partial_reconcileImpl>();
            for(Iaccount_partial_reconcile iaccount_partial_reconcile :account_partial_reconciles){
                list.add((account_partial_reconcileImpl)iaccount_partial_reconcile) ;
            }
            account_partial_reconcileFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iaccount_partial_reconcile account_partial_reconcile){
        Iaccount_partial_reconcile clientModel = account_partial_reconcileFeignClient.create((account_partial_reconcileImpl)account_partial_reconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_partial_reconcile.getClass(), false);
        copier.copy(clientModel, account_partial_reconcile, null);
    }


    public Page<Iaccount_partial_reconcile> fetchDefault(SearchContext context){
        Page<account_partial_reconcileImpl> page = this.account_partial_reconcileFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iaccount_partial_reconcile> account_partial_reconciles){
        if(account_partial_reconciles!=null){
            List<account_partial_reconcileImpl> list = new ArrayList<account_partial_reconcileImpl>();
            for(Iaccount_partial_reconcile iaccount_partial_reconcile :account_partial_reconciles){
                list.add((account_partial_reconcileImpl)iaccount_partial_reconcile) ;
            }
            account_partial_reconcileFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_partial_reconcile> account_partial_reconciles){
        if(account_partial_reconciles!=null){
            List<account_partial_reconcileImpl> list = new ArrayList<account_partial_reconcileImpl>();
            for(Iaccount_partial_reconcile iaccount_partial_reconcile :account_partial_reconciles){
                list.add((account_partial_reconcileImpl)iaccount_partial_reconcile) ;
            }
            account_partial_reconcileFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_partial_reconcile> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_partial_reconcile account_partial_reconcile){
        Iaccount_partial_reconcile clientModel = account_partial_reconcileFeignClient.getDraft(account_partial_reconcile.getId(),(account_partial_reconcileImpl)account_partial_reconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_partial_reconcile.getClass(), false);
        copier.copy(clientModel, account_partial_reconcile, null);
    }



}

