package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_financial_year_op;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_financial_year_opClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_financial_year_opImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_financial_year_opFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_financial_year_op] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_financial_year_opClientServiceImpl implements Iaccount_financial_year_opClientService {

    account_financial_year_opFeignClient account_financial_year_opFeignClient;

    @Autowired
    public account_financial_year_opClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_financial_year_opFeignClient = nameBuilder.target(account_financial_year_opFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_financial_year_opFeignClient = nameBuilder.target(account_financial_year_opFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_financial_year_op createModel() {
		return new account_financial_year_opImpl();
	}


    public void remove(Iaccount_financial_year_op account_financial_year_op){
        account_financial_year_opFeignClient.remove(account_financial_year_op.getId()) ;
    }


    public void removeBatch(List<Iaccount_financial_year_op> account_financial_year_ops){
        if(account_financial_year_ops!=null){
            List<account_financial_year_opImpl> list = new ArrayList<account_financial_year_opImpl>();
            for(Iaccount_financial_year_op iaccount_financial_year_op :account_financial_year_ops){
                list.add((account_financial_year_opImpl)iaccount_financial_year_op) ;
            }
            account_financial_year_opFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_financial_year_op> account_financial_year_ops){
        if(account_financial_year_ops!=null){
            List<account_financial_year_opImpl> list = new ArrayList<account_financial_year_opImpl>();
            for(Iaccount_financial_year_op iaccount_financial_year_op :account_financial_year_ops){
                list.add((account_financial_year_opImpl)iaccount_financial_year_op) ;
            }
            account_financial_year_opFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_financial_year_op> account_financial_year_ops){
        if(account_financial_year_ops!=null){
            List<account_financial_year_opImpl> list = new ArrayList<account_financial_year_opImpl>();
            for(Iaccount_financial_year_op iaccount_financial_year_op :account_financial_year_ops){
                list.add((account_financial_year_opImpl)iaccount_financial_year_op) ;
            }
            account_financial_year_opFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_financial_year_op> fetchDefault(SearchContext context){
        Page<account_financial_year_opImpl> page = this.account_financial_year_opFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iaccount_financial_year_op account_financial_year_op){
        Iaccount_financial_year_op clientModel = account_financial_year_opFeignClient.get(account_financial_year_op.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_financial_year_op.getClass(), false);
        copier.copy(clientModel, account_financial_year_op, null);
    }


    public void update(Iaccount_financial_year_op account_financial_year_op){
        Iaccount_financial_year_op clientModel = account_financial_year_opFeignClient.update(account_financial_year_op.getId(),(account_financial_year_opImpl)account_financial_year_op) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_financial_year_op.getClass(), false);
        copier.copy(clientModel, account_financial_year_op, null);
    }


    public void create(Iaccount_financial_year_op account_financial_year_op){
        Iaccount_financial_year_op clientModel = account_financial_year_opFeignClient.create((account_financial_year_opImpl)account_financial_year_op) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_financial_year_op.getClass(), false);
        copier.copy(clientModel, account_financial_year_op, null);
    }


    public Page<Iaccount_financial_year_op> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_financial_year_op account_financial_year_op){
        Iaccount_financial_year_op clientModel = account_financial_year_opFeignClient.getDraft(account_financial_year_op.getId(),(account_financial_year_opImpl)account_financial_year_op) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_financial_year_op.getClass(), false);
        copier.copy(clientModel, account_financial_year_op, null);
    }



}

