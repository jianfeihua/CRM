package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_method;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_methodImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_payment_method] 服务对象接口
 */
public interface account_payment_methodFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_methods/{id}")
    public account_payment_methodImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_methods/updatebatch")
    public account_payment_methodImpl updateBatch(@RequestBody List<account_payment_methodImpl> account_payment_methods);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_methods/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_methods")
    public account_payment_methodImpl create(@RequestBody account_payment_methodImpl account_payment_method);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_methods/removebatch")
    public account_payment_methodImpl removeBatch(@RequestBody List<account_payment_methodImpl> account_payment_methods);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_methods/fetchdefault")
    public Page<account_payment_methodImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_methods/{id}")
    public account_payment_methodImpl update(@PathVariable("id") Integer id,@RequestBody account_payment_methodImpl account_payment_method);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_methods/createbatch")
    public account_payment_methodImpl createBatch(@RequestBody List<account_payment_methodImpl> account_payment_methods);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_methods/select")
    public Page<account_payment_methodImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_methods/{id}/getdraft")
    public account_payment_methodImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_payment_methodImpl account_payment_method);



}
