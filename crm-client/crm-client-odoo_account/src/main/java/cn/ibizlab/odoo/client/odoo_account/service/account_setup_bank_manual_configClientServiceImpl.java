package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_setup_bank_manual_config;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_setup_bank_manual_configClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_setup_bank_manual_configImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_setup_bank_manual_configFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_setup_bank_manual_config] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_setup_bank_manual_configClientServiceImpl implements Iaccount_setup_bank_manual_configClientService {

    account_setup_bank_manual_configFeignClient account_setup_bank_manual_configFeignClient;

    @Autowired
    public account_setup_bank_manual_configClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_setup_bank_manual_configFeignClient = nameBuilder.target(account_setup_bank_manual_configFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_setup_bank_manual_configFeignClient = nameBuilder.target(account_setup_bank_manual_configFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_setup_bank_manual_config createModel() {
		return new account_setup_bank_manual_configImpl();
	}


    public void update(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
        Iaccount_setup_bank_manual_config clientModel = account_setup_bank_manual_configFeignClient.update(account_setup_bank_manual_config.getId(),(account_setup_bank_manual_configImpl)account_setup_bank_manual_config) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_setup_bank_manual_config.getClass(), false);
        copier.copy(clientModel, account_setup_bank_manual_config, null);
    }


    public void create(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
        Iaccount_setup_bank_manual_config clientModel = account_setup_bank_manual_configFeignClient.create((account_setup_bank_manual_configImpl)account_setup_bank_manual_config) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_setup_bank_manual_config.getClass(), false);
        copier.copy(clientModel, account_setup_bank_manual_config, null);
    }


    public void get(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
        Iaccount_setup_bank_manual_config clientModel = account_setup_bank_manual_configFeignClient.get(account_setup_bank_manual_config.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_setup_bank_manual_config.getClass(), false);
        copier.copy(clientModel, account_setup_bank_manual_config, null);
    }


    public void updateBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs){
        if(account_setup_bank_manual_configs!=null){
            List<account_setup_bank_manual_configImpl> list = new ArrayList<account_setup_bank_manual_configImpl>();
            for(Iaccount_setup_bank_manual_config iaccount_setup_bank_manual_config :account_setup_bank_manual_configs){
                list.add((account_setup_bank_manual_configImpl)iaccount_setup_bank_manual_config) ;
            }
            account_setup_bank_manual_configFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
        account_setup_bank_manual_configFeignClient.remove(account_setup_bank_manual_config.getId()) ;
    }


    public void removeBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs){
        if(account_setup_bank_manual_configs!=null){
            List<account_setup_bank_manual_configImpl> list = new ArrayList<account_setup_bank_manual_configImpl>();
            for(Iaccount_setup_bank_manual_config iaccount_setup_bank_manual_config :account_setup_bank_manual_configs){
                list.add((account_setup_bank_manual_configImpl)iaccount_setup_bank_manual_config) ;
            }
            account_setup_bank_manual_configFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs){
        if(account_setup_bank_manual_configs!=null){
            List<account_setup_bank_manual_configImpl> list = new ArrayList<account_setup_bank_manual_configImpl>();
            for(Iaccount_setup_bank_manual_config iaccount_setup_bank_manual_config :account_setup_bank_manual_configs){
                list.add((account_setup_bank_manual_configImpl)iaccount_setup_bank_manual_config) ;
            }
            account_setup_bank_manual_configFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_setup_bank_manual_config> fetchDefault(SearchContext context){
        Page<account_setup_bank_manual_configImpl> page = this.account_setup_bank_manual_configFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_setup_bank_manual_config> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
        Iaccount_setup_bank_manual_config clientModel = account_setup_bank_manual_configFeignClient.getDraft(account_setup_bank_manual_config.getId(),(account_setup_bank_manual_configImpl)account_setup_bank_manual_config) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_setup_bank_manual_config.getClass(), false);
        copier.copy(clientModel, account_setup_bank_manual_config, null);
    }



}

