package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_positionClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_positionImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_fiscal_positionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_fiscal_position] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_fiscal_positionClientServiceImpl implements Iaccount_fiscal_positionClientService {

    account_fiscal_positionFeignClient account_fiscal_positionFeignClient;

    @Autowired
    public account_fiscal_positionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_positionFeignClient = nameBuilder.target(account_fiscal_positionFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_positionFeignClient = nameBuilder.target(account_fiscal_positionFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_fiscal_position createModel() {
		return new account_fiscal_positionImpl();
	}


    public void update(Iaccount_fiscal_position account_fiscal_position){
        Iaccount_fiscal_position clientModel = account_fiscal_positionFeignClient.update(account_fiscal_position.getId(),(account_fiscal_positionImpl)account_fiscal_position) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position.getClass(), false);
        copier.copy(clientModel, account_fiscal_position, null);
    }


    public void removeBatch(List<Iaccount_fiscal_position> account_fiscal_positions){
        if(account_fiscal_positions!=null){
            List<account_fiscal_positionImpl> list = new ArrayList<account_fiscal_positionImpl>();
            for(Iaccount_fiscal_position iaccount_fiscal_position :account_fiscal_positions){
                list.add((account_fiscal_positionImpl)iaccount_fiscal_position) ;
            }
            account_fiscal_positionFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iaccount_fiscal_position account_fiscal_position){
        account_fiscal_positionFeignClient.remove(account_fiscal_position.getId()) ;
    }


    public void get(Iaccount_fiscal_position account_fiscal_position){
        Iaccount_fiscal_position clientModel = account_fiscal_positionFeignClient.get(account_fiscal_position.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position.getClass(), false);
        copier.copy(clientModel, account_fiscal_position, null);
    }


    public Page<Iaccount_fiscal_position> fetchDefault(SearchContext context){
        Page<account_fiscal_positionImpl> page = this.account_fiscal_positionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_fiscal_position> account_fiscal_positions){
        if(account_fiscal_positions!=null){
            List<account_fiscal_positionImpl> list = new ArrayList<account_fiscal_positionImpl>();
            for(Iaccount_fiscal_position iaccount_fiscal_position :account_fiscal_positions){
                list.add((account_fiscal_positionImpl)iaccount_fiscal_position) ;
            }
            account_fiscal_positionFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iaccount_fiscal_position account_fiscal_position){
        Iaccount_fiscal_position clientModel = account_fiscal_positionFeignClient.create((account_fiscal_positionImpl)account_fiscal_position) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position.getClass(), false);
        copier.copy(clientModel, account_fiscal_position, null);
    }


    public void createBatch(List<Iaccount_fiscal_position> account_fiscal_positions){
        if(account_fiscal_positions!=null){
            List<account_fiscal_positionImpl> list = new ArrayList<account_fiscal_positionImpl>();
            for(Iaccount_fiscal_position iaccount_fiscal_position :account_fiscal_positions){
                list.add((account_fiscal_positionImpl)iaccount_fiscal_position) ;
            }
            account_fiscal_positionFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_fiscal_position> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_fiscal_position account_fiscal_position){
        Iaccount_fiscal_position clientModel = account_fiscal_positionFeignClient.getDraft(account_fiscal_position.getId(),(account_fiscal_positionImpl)account_fiscal_position) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position.getClass(), false);
        copier.copy(clientModel, account_fiscal_position, null);
    }



}

