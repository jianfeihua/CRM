package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_reversal;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_move_reversalClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_move_reversalImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_move_reversalFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_move_reversal] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_move_reversalClientServiceImpl implements Iaccount_move_reversalClientService {

    account_move_reversalFeignClient account_move_reversalFeignClient;

    @Autowired
    public account_move_reversalClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_move_reversalFeignClient = nameBuilder.target(account_move_reversalFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_move_reversalFeignClient = nameBuilder.target(account_move_reversalFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_move_reversal createModel() {
		return new account_move_reversalImpl();
	}


    public void createBatch(List<Iaccount_move_reversal> account_move_reversals){
        if(account_move_reversals!=null){
            List<account_move_reversalImpl> list = new ArrayList<account_move_reversalImpl>();
            for(Iaccount_move_reversal iaccount_move_reversal :account_move_reversals){
                list.add((account_move_reversalImpl)iaccount_move_reversal) ;
            }
            account_move_reversalFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_move_reversal account_move_reversal){
        Iaccount_move_reversal clientModel = account_move_reversalFeignClient.update(account_move_reversal.getId(),(account_move_reversalImpl)account_move_reversal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_reversal.getClass(), false);
        copier.copy(clientModel, account_move_reversal, null);
    }


    public void removeBatch(List<Iaccount_move_reversal> account_move_reversals){
        if(account_move_reversals!=null){
            List<account_move_reversalImpl> list = new ArrayList<account_move_reversalImpl>();
            for(Iaccount_move_reversal iaccount_move_reversal :account_move_reversals){
                list.add((account_move_reversalImpl)iaccount_move_reversal) ;
            }
            account_move_reversalFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iaccount_move_reversal account_move_reversal){
        Iaccount_move_reversal clientModel = account_move_reversalFeignClient.create((account_move_reversalImpl)account_move_reversal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_reversal.getClass(), false);
        copier.copy(clientModel, account_move_reversal, null);
    }


    public void updateBatch(List<Iaccount_move_reversal> account_move_reversals){
        if(account_move_reversals!=null){
            List<account_move_reversalImpl> list = new ArrayList<account_move_reversalImpl>();
            for(Iaccount_move_reversal iaccount_move_reversal :account_move_reversals){
                list.add((account_move_reversalImpl)iaccount_move_reversal) ;
            }
            account_move_reversalFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iaccount_move_reversal account_move_reversal){
        Iaccount_move_reversal clientModel = account_move_reversalFeignClient.get(account_move_reversal.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_reversal.getClass(), false);
        copier.copy(clientModel, account_move_reversal, null);
    }


    public void remove(Iaccount_move_reversal account_move_reversal){
        account_move_reversalFeignClient.remove(account_move_reversal.getId()) ;
    }


    public Page<Iaccount_move_reversal> fetchDefault(SearchContext context){
        Page<account_move_reversalImpl> page = this.account_move_reversalFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_move_reversal> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_move_reversal account_move_reversal){
        Iaccount_move_reversal clientModel = account_move_reversalFeignClient.getDraft(account_move_reversal.getId(),(account_move_reversalImpl)account_move_reversal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_reversal.getClass(), false);
        copier.copy(clientModel, account_move_reversal, null);
    }



}

