package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_line;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_bank_statement_line] 对象
 */
public class account_bank_statement_lineImpl implements Iaccount_bank_statement_line,Serializable{

    /**
     * 对方科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 对方科目
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 银行账户号码
     */
    public String account_number;

    @JsonIgnore
    public boolean account_numberDirtyFlag;
    
    /**
     * 金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 货币金额
     */
    public Double amount_currency;

    @JsonIgnore
    public boolean amount_currencyDirtyFlag;
    
    /**
     * 银行账户
     */
    public Integer bank_account_id;

    @JsonIgnore
    public boolean bank_account_idDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 现金分类账
     */
    public Integer journal_currency_id;

    @JsonIgnore
    public boolean journal_currency_idDirtyFlag;
    
    /**
     * 日记账项目
     */
    public String journal_entry_ids;

    @JsonIgnore
    public boolean journal_entry_idsDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 日记账分录名称
     */
    public String move_name;

    @JsonIgnore
    public boolean move_nameDirtyFlag;
    
    /**
     * 标签
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 合作伙伴名称
     */
    public String partner_name;

    @JsonIgnore
    public boolean partner_nameDirtyFlag;
    
    /**
     * POS报表
     */
    public Integer pos_statement_id;

    @JsonIgnore
    public boolean pos_statement_idDirtyFlag;
    
    /**
     * 参考
     */
    public String ref;

    @JsonIgnore
    public boolean refDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 报告
     */
    public Integer statement_id;

    @JsonIgnore
    public boolean statement_idDirtyFlag;
    
    /**
     * 报告
     */
    public String statement_id_text;

    @JsonIgnore
    public boolean statement_id_textDirtyFlag;
    
    /**
     * 导入ID
     */
    public String unique_import_id;

    @JsonIgnore
    public boolean unique_import_idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [对方科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [对方科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [对方科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [对方科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [对方科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [对方科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [银行账户号码]
     */
    @JsonProperty("account_number")
    public String getAccount_number(){
        return this.account_number ;
    }

    /**
     * 设置 [银行账户号码]
     */
    @JsonProperty("account_number")
    public void setAccount_number(String  account_number){
        this.account_number = account_number ;
        this.account_numberDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户号码]脏标记
     */
    @JsonIgnore
    public boolean getAccount_numberDirtyFlag(){
        return this.account_numberDirtyFlag ;
    }   

    /**
     * 获取 [金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [货币金额]
     */
    @JsonProperty("amount_currency")
    public Double getAmount_currency(){
        return this.amount_currency ;
    }

    /**
     * 设置 [货币金额]
     */
    @JsonProperty("amount_currency")
    public void setAmount_currency(Double  amount_currency){
        this.amount_currency = amount_currency ;
        this.amount_currencyDirtyFlag = true ;
    }

     /**
     * 获取 [货币金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_currencyDirtyFlag(){
        return this.amount_currencyDirtyFlag ;
    }   

    /**
     * 获取 [银行账户]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return this.bank_account_id ;
    }

    /**
     * 设置 [银行账户]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return this.bank_account_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [现金分类账]
     */
    @JsonProperty("journal_currency_id")
    public Integer getJournal_currency_id(){
        return this.journal_currency_id ;
    }

    /**
     * 设置 [现金分类账]
     */
    @JsonProperty("journal_currency_id")
    public void setJournal_currency_id(Integer  journal_currency_id){
        this.journal_currency_id = journal_currency_id ;
        this.journal_currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [现金分类账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_currency_idDirtyFlag(){
        return this.journal_currency_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账项目]
     */
    @JsonProperty("journal_entry_ids")
    public String getJournal_entry_ids(){
        return this.journal_entry_ids ;
    }

    /**
     * 设置 [日记账项目]
     */
    @JsonProperty("journal_entry_ids")
    public void setJournal_entry_ids(String  journal_entry_ids){
        this.journal_entry_ids = journal_entry_ids ;
        this.journal_entry_idsDirtyFlag = true ;
    }

     /**
     * 获取 [日记账项目]脏标记
     */
    @JsonIgnore
    public boolean getJournal_entry_idsDirtyFlag(){
        return this.journal_entry_idsDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public String getMove_name(){
        return this.move_name ;
    }

    /**
     * 设置 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public void setMove_name(String  move_name){
        this.move_name = move_name ;
        this.move_nameDirtyFlag = true ;
    }

     /**
     * 获取 [日记账分录名称]脏标记
     */
    @JsonIgnore
    public boolean getMove_nameDirtyFlag(){
        return this.move_nameDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [合作伙伴名称]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return this.partner_name ;
    }

    /**
     * 设置 [合作伙伴名称]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

     /**
     * 获取 [合作伙伴名称]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return this.partner_nameDirtyFlag ;
    }   

    /**
     * 获取 [POS报表]
     */
    @JsonProperty("pos_statement_id")
    public Integer getPos_statement_id(){
        return this.pos_statement_id ;
    }

    /**
     * 设置 [POS报表]
     */
    @JsonProperty("pos_statement_id")
    public void setPos_statement_id(Integer  pos_statement_id){
        this.pos_statement_id = pos_statement_id ;
        this.pos_statement_idDirtyFlag = true ;
    }

     /**
     * 获取 [POS报表]脏标记
     */
    @JsonIgnore
    public boolean getPos_statement_idDirtyFlag(){
        return this.pos_statement_idDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("ref")
    public String getRef(){
        return this.ref ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return this.refDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [报告]
     */
    @JsonProperty("statement_id")
    public Integer getStatement_id(){
        return this.statement_id ;
    }

    /**
     * 设置 [报告]
     */
    @JsonProperty("statement_id")
    public void setStatement_id(Integer  statement_id){
        this.statement_id = statement_id ;
        this.statement_idDirtyFlag = true ;
    }

     /**
     * 获取 [报告]脏标记
     */
    @JsonIgnore
    public boolean getStatement_idDirtyFlag(){
        return this.statement_idDirtyFlag ;
    }   

    /**
     * 获取 [报告]
     */
    @JsonProperty("statement_id_text")
    public String getStatement_id_text(){
        return this.statement_id_text ;
    }

    /**
     * 设置 [报告]
     */
    @JsonProperty("statement_id_text")
    public void setStatement_id_text(String  statement_id_text){
        this.statement_id_text = statement_id_text ;
        this.statement_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [报告]脏标记
     */
    @JsonIgnore
    public boolean getStatement_id_textDirtyFlag(){
        return this.statement_id_textDirtyFlag ;
    }   

    /**
     * 获取 [导入ID]
     */
    @JsonProperty("unique_import_id")
    public String getUnique_import_id(){
        return this.unique_import_id ;
    }

    /**
     * 设置 [导入ID]
     */
    @JsonProperty("unique_import_id")
    public void setUnique_import_id(String  unique_import_id){
        this.unique_import_id = unique_import_id ;
        this.unique_import_idDirtyFlag = true ;
    }

     /**
     * 获取 [导入ID]脏标记
     */
    @JsonIgnore
    public boolean getUnique_import_idDirtyFlag(){
        return this.unique_import_idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
