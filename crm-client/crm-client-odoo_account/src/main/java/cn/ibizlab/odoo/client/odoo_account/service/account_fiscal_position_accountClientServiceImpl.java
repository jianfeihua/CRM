package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_account;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_position_accountClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_accountImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_fiscal_position_accountFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_fiscal_position_account] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_fiscal_position_accountClientServiceImpl implements Iaccount_fiscal_position_accountClientService {

    account_fiscal_position_accountFeignClient account_fiscal_position_accountFeignClient;

    @Autowired
    public account_fiscal_position_accountClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_position_accountFeignClient = nameBuilder.target(account_fiscal_position_accountFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_position_accountFeignClient = nameBuilder.target(account_fiscal_position_accountFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_fiscal_position_account createModel() {
		return new account_fiscal_position_accountImpl();
	}


    public void updateBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts){
        if(account_fiscal_position_accounts!=null){
            List<account_fiscal_position_accountImpl> list = new ArrayList<account_fiscal_position_accountImpl>();
            for(Iaccount_fiscal_position_account iaccount_fiscal_position_account :account_fiscal_position_accounts){
                list.add((account_fiscal_position_accountImpl)iaccount_fiscal_position_account) ;
            }
            account_fiscal_position_accountFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_fiscal_position_account> fetchDefault(SearchContext context){
        Page<account_fiscal_position_accountImpl> page = this.account_fiscal_position_accountFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_fiscal_position_account account_fiscal_position_account){
        Iaccount_fiscal_position_account clientModel = account_fiscal_position_accountFeignClient.create((account_fiscal_position_accountImpl)account_fiscal_position_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_account.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_account, null);
    }


    public void createBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts){
        if(account_fiscal_position_accounts!=null){
            List<account_fiscal_position_accountImpl> list = new ArrayList<account_fiscal_position_accountImpl>();
            for(Iaccount_fiscal_position_account iaccount_fiscal_position_account :account_fiscal_position_accounts){
                list.add((account_fiscal_position_accountImpl)iaccount_fiscal_position_account) ;
            }
            account_fiscal_position_accountFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_fiscal_position_account account_fiscal_position_account){
        Iaccount_fiscal_position_account clientModel = account_fiscal_position_accountFeignClient.update(account_fiscal_position_account.getId(),(account_fiscal_position_accountImpl)account_fiscal_position_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_account.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_account, null);
    }


    public void get(Iaccount_fiscal_position_account account_fiscal_position_account){
        Iaccount_fiscal_position_account clientModel = account_fiscal_position_accountFeignClient.get(account_fiscal_position_account.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_account.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_account, null);
    }


    public void remove(Iaccount_fiscal_position_account account_fiscal_position_account){
        account_fiscal_position_accountFeignClient.remove(account_fiscal_position_account.getId()) ;
    }


    public void removeBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts){
        if(account_fiscal_position_accounts!=null){
            List<account_fiscal_position_accountImpl> list = new ArrayList<account_fiscal_position_accountImpl>();
            for(Iaccount_fiscal_position_account iaccount_fiscal_position_account :account_fiscal_position_accounts){
                list.add((account_fiscal_position_accountImpl)iaccount_fiscal_position_account) ;
            }
            account_fiscal_position_accountFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_fiscal_position_account> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_fiscal_position_account account_fiscal_position_account){
        Iaccount_fiscal_position_account clientModel = account_fiscal_position_accountFeignClient.getDraft(account_fiscal_position_account.getId(),(account_fiscal_position_accountImpl)account_fiscal_position_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_account.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_account, null);
    }



}

