package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_importClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_importImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_bank_statement_importFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_bank_statement_import] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_bank_statement_importClientServiceImpl implements Iaccount_bank_statement_importClientService {

    account_bank_statement_importFeignClient account_bank_statement_importFeignClient;

    @Autowired
    public account_bank_statement_importClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_importFeignClient = nameBuilder.target(account_bank_statement_importFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_importFeignClient = nameBuilder.target(account_bank_statement_importFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_bank_statement_import createModel() {
		return new account_bank_statement_importImpl();
	}


    public void update(Iaccount_bank_statement_import account_bank_statement_import){
        Iaccount_bank_statement_import clientModel = account_bank_statement_importFeignClient.update(account_bank_statement_import.getId(),(account_bank_statement_importImpl)account_bank_statement_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import, null);
    }


    public void get(Iaccount_bank_statement_import account_bank_statement_import){
        Iaccount_bank_statement_import clientModel = account_bank_statement_importFeignClient.get(account_bank_statement_import.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import, null);
    }


    public void create(Iaccount_bank_statement_import account_bank_statement_import){
        Iaccount_bank_statement_import clientModel = account_bank_statement_importFeignClient.create((account_bank_statement_importImpl)account_bank_statement_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import, null);
    }


    public void createBatch(List<Iaccount_bank_statement_import> account_bank_statement_imports){
        if(account_bank_statement_imports!=null){
            List<account_bank_statement_importImpl> list = new ArrayList<account_bank_statement_importImpl>();
            for(Iaccount_bank_statement_import iaccount_bank_statement_import :account_bank_statement_imports){
                list.add((account_bank_statement_importImpl)iaccount_bank_statement_import) ;
            }
            account_bank_statement_importFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_bank_statement_import> account_bank_statement_imports){
        if(account_bank_statement_imports!=null){
            List<account_bank_statement_importImpl> list = new ArrayList<account_bank_statement_importImpl>();
            for(Iaccount_bank_statement_import iaccount_bank_statement_import :account_bank_statement_imports){
                list.add((account_bank_statement_importImpl)iaccount_bank_statement_import) ;
            }
            account_bank_statement_importFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_bank_statement_import account_bank_statement_import){
        account_bank_statement_importFeignClient.remove(account_bank_statement_import.getId()) ;
    }


    public void removeBatch(List<Iaccount_bank_statement_import> account_bank_statement_imports){
        if(account_bank_statement_imports!=null){
            List<account_bank_statement_importImpl> list = new ArrayList<account_bank_statement_importImpl>();
            for(Iaccount_bank_statement_import iaccount_bank_statement_import :account_bank_statement_imports){
                list.add((account_bank_statement_importImpl)iaccount_bank_statement_import) ;
            }
            account_bank_statement_importFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_bank_statement_import> fetchDefault(SearchContext context){
        Page<account_bank_statement_importImpl> page = this.account_bank_statement_importFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_bank_statement_import> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_bank_statement_import account_bank_statement_import){
        Iaccount_bank_statement_import clientModel = account_bank_statement_importFeignClient.getDraft(account_bank_statement_import.getId(),(account_bank_statement_importImpl)account_bank_statement_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import, null);
    }



}

