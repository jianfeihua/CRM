package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoiceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice] 服务对象接口
 */
public interface account_invoiceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoices/{id}")
    public account_invoiceImpl update(@PathVariable("id") Integer id,@RequestBody account_invoiceImpl account_invoice);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoices/fetchdefault")
    public Page<account_invoiceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoices/removebatch")
    public account_invoiceImpl removeBatch(@RequestBody List<account_invoiceImpl> account_invoices);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoices/createbatch")
    public account_invoiceImpl createBatch(@RequestBody List<account_invoiceImpl> account_invoices);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoices")
    public account_invoiceImpl create(@RequestBody account_invoiceImpl account_invoice);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoices/updatebatch")
    public account_invoiceImpl updateBatch(@RequestBody List<account_invoiceImpl> account_invoices);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoices/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoices/{id}")
    public account_invoiceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoices/select")
    public Page<account_invoiceImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoices/{id}/checkkey")
    public account_invoiceImpl checkKey(@PathVariable("id") Integer id,@RequestBody account_invoiceImpl account_invoice);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoices/{id}/getdraft")
    public account_invoiceImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_invoiceImpl account_invoice);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoices/{id}/save")
    public account_invoiceImpl save(@PathVariable("id") Integer id,@RequestBody account_invoiceImpl account_invoice);



}
