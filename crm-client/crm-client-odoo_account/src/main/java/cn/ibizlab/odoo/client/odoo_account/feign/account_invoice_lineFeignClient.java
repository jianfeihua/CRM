package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_line;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice_line] 服务对象接口
 */
public interface account_invoice_lineFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_lines/{id}")
    public account_invoice_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_lines")
    public account_invoice_lineImpl create(@RequestBody account_invoice_lineImpl account_invoice_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_lines/fetchdefault")
    public Page<account_invoice_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_lines/createbatch")
    public account_invoice_lineImpl createBatch(@RequestBody List<account_invoice_lineImpl> account_invoice_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_lines/{id}")
    public account_invoice_lineImpl update(@PathVariable("id") Integer id,@RequestBody account_invoice_lineImpl account_invoice_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_lines/updatebatch")
    public account_invoice_lineImpl updateBatch(@RequestBody List<account_invoice_lineImpl> account_invoice_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_lines/removebatch")
    public account_invoice_lineImpl removeBatch(@RequestBody List<account_invoice_lineImpl> account_invoice_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_lines/select")
    public Page<account_invoice_lineImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_lines/{id}/checkkey")
    public account_invoice_lineImpl checkKey(@PathVariable("id") Integer id,@RequestBody account_invoice_lineImpl account_invoice_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_lines/{id}/getdraft")
    public account_invoice_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_invoice_lineImpl account_invoice_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_lines/{id}/save")
    public account_invoice_lineImpl save(@PathVariable("id") Integer id,@RequestBody account_invoice_lineImpl account_invoice_line);



}
