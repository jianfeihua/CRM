package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_positionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_fiscal_position] 服务对象接口
 */
public interface account_fiscal_positionFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_positions/{id}")
    public account_fiscal_positionImpl update(@PathVariable("id") Integer id,@RequestBody account_fiscal_positionImpl account_fiscal_position);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_positions/removebatch")
    public account_fiscal_positionImpl removeBatch(@RequestBody List<account_fiscal_positionImpl> account_fiscal_positions);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_positions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_positions/{id}")
    public account_fiscal_positionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_positions/fetchdefault")
    public Page<account_fiscal_positionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_positions/updatebatch")
    public account_fiscal_positionImpl updateBatch(@RequestBody List<account_fiscal_positionImpl> account_fiscal_positions);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_positions")
    public account_fiscal_positionImpl create(@RequestBody account_fiscal_positionImpl account_fiscal_position);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_positions/createbatch")
    public account_fiscal_positionImpl createBatch(@RequestBody List<account_fiscal_positionImpl> account_fiscal_positions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_positions/select")
    public Page<account_fiscal_positionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_positions/{id}/getdraft")
    public account_fiscal_positionImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_fiscal_positionImpl account_fiscal_position);



}
