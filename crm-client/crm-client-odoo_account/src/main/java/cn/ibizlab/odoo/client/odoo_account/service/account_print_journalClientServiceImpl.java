package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_print_journal;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_print_journalClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_print_journalImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_print_journalFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_print_journal] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_print_journalClientServiceImpl implements Iaccount_print_journalClientService {

    account_print_journalFeignClient account_print_journalFeignClient;

    @Autowired
    public account_print_journalClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_print_journalFeignClient = nameBuilder.target(account_print_journalFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_print_journalFeignClient = nameBuilder.target(account_print_journalFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_print_journal createModel() {
		return new account_print_journalImpl();
	}


    public void create(Iaccount_print_journal account_print_journal){
        Iaccount_print_journal clientModel = account_print_journalFeignClient.create((account_print_journalImpl)account_print_journal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_print_journal.getClass(), false);
        copier.copy(clientModel, account_print_journal, null);
    }


    public void createBatch(List<Iaccount_print_journal> account_print_journals){
        if(account_print_journals!=null){
            List<account_print_journalImpl> list = new ArrayList<account_print_journalImpl>();
            for(Iaccount_print_journal iaccount_print_journal :account_print_journals){
                list.add((account_print_journalImpl)iaccount_print_journal) ;
            }
            account_print_journalFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_print_journal account_print_journal){
        Iaccount_print_journal clientModel = account_print_journalFeignClient.update(account_print_journal.getId(),(account_print_journalImpl)account_print_journal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_print_journal.getClass(), false);
        copier.copy(clientModel, account_print_journal, null);
    }


    public void remove(Iaccount_print_journal account_print_journal){
        account_print_journalFeignClient.remove(account_print_journal.getId()) ;
    }


    public Page<Iaccount_print_journal> fetchDefault(SearchContext context){
        Page<account_print_journalImpl> page = this.account_print_journalFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iaccount_print_journal> account_print_journals){
        if(account_print_journals!=null){
            List<account_print_journalImpl> list = new ArrayList<account_print_journalImpl>();
            for(Iaccount_print_journal iaccount_print_journal :account_print_journals){
                list.add((account_print_journalImpl)iaccount_print_journal) ;
            }
            account_print_journalFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iaccount_print_journal account_print_journal){
        Iaccount_print_journal clientModel = account_print_journalFeignClient.get(account_print_journal.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_print_journal.getClass(), false);
        copier.copy(clientModel, account_print_journal, null);
    }


    public void updateBatch(List<Iaccount_print_journal> account_print_journals){
        if(account_print_journals!=null){
            List<account_print_journalImpl> list = new ArrayList<account_print_journalImpl>();
            for(Iaccount_print_journal iaccount_print_journal :account_print_journals){
                list.add((account_print_journalImpl)iaccount_print_journal) ;
            }
            account_print_journalFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_print_journal> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_print_journal account_print_journal){
        Iaccount_print_journal clientModel = account_print_journalFeignClient.getDraft(account_print_journal.getId(),(account_print_journalImpl)account_print_journal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_print_journal.getClass(), false);
        copier.copy(clientModel, account_print_journal, null);
    }



}

