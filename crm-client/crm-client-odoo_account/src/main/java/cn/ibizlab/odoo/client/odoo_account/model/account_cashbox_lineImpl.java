package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_cashbox_line;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_cashbox_line] 对象
 */
public class account_cashbox_lineImpl implements Iaccount_cashbox_line,Serializable{

    /**
     * 钱箱
     */
    public Integer cashbox_id;

    @JsonIgnore
    public boolean cashbox_idDirtyFlag;
    
    /**
     * 硬币／账单　价值
     */
    public Double coin_value;

    @JsonIgnore
    public boolean coin_valueDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行
     */
    public Integer default_pos_id;

    @JsonIgnore
    public boolean default_pos_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 货币和账单编号
     */
    public Integer number;

    @JsonIgnore
    public boolean numberDirtyFlag;
    
    /**
     * 小计
     */
    public Double subtotal;

    @JsonIgnore
    public boolean subtotalDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [钱箱]
     */
    @JsonProperty("cashbox_id")
    public Integer getCashbox_id(){
        return this.cashbox_id ;
    }

    /**
     * 设置 [钱箱]
     */
    @JsonProperty("cashbox_id")
    public void setCashbox_id(Integer  cashbox_id){
        this.cashbox_id = cashbox_id ;
        this.cashbox_idDirtyFlag = true ;
    }

     /**
     * 获取 [钱箱]脏标记
     */
    @JsonIgnore
    public boolean getCashbox_idDirtyFlag(){
        return this.cashbox_idDirtyFlag ;
    }   

    /**
     * 获取 [硬币／账单　价值]
     */
    @JsonProperty("coin_value")
    public Double getCoin_value(){
        return this.coin_value ;
    }

    /**
     * 设置 [硬币／账单　价值]
     */
    @JsonProperty("coin_value")
    public void setCoin_value(Double  coin_value){
        this.coin_value = coin_value ;
        this.coin_valueDirtyFlag = true ;
    }

     /**
     * 获取 [硬币／账单　价值]脏标记
     */
    @JsonIgnore
    public boolean getCoin_valueDirtyFlag(){
        return this.coin_valueDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]
     */
    @JsonProperty("default_pos_id")
    public Integer getDefault_pos_id(){
        return this.default_pos_id ;
    }

    /**
     * 设置 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]
     */
    @JsonProperty("default_pos_id")
    public void setDefault_pos_id(Integer  default_pos_id){
        this.default_pos_id = default_pos_id ;
        this.default_pos_idDirtyFlag = true ;
    }

     /**
     * 获取 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]脏标记
     */
    @JsonIgnore
    public boolean getDefault_pos_idDirtyFlag(){
        return this.default_pos_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [货币和账单编号]
     */
    @JsonProperty("number")
    public Integer getNumber(){
        return this.number ;
    }

    /**
     * 设置 [货币和账单编号]
     */
    @JsonProperty("number")
    public void setNumber(Integer  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

     /**
     * 获取 [货币和账单编号]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return this.numberDirtyFlag ;
    }   

    /**
     * 获取 [小计]
     */
    @JsonProperty("subtotal")
    public Double getSubtotal(){
        return this.subtotal ;
    }

    /**
     * 设置 [小计]
     */
    @JsonProperty("subtotal")
    public void setSubtotal(Double  subtotal){
        this.subtotal = subtotal ;
        this.subtotalDirtyFlag = true ;
    }

     /**
     * 获取 [小计]脏标记
     */
    @JsonIgnore
    public boolean getSubtotalDirtyFlag(){
        return this.subtotalDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
