package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_line;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_analytic_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_analytic_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_analytic_lineClientServiceImpl implements Iaccount_analytic_lineClientService {

    account_analytic_lineFeignClient account_analytic_lineFeignClient;

    @Autowired
    public account_analytic_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_lineFeignClient = nameBuilder.target(account_analytic_lineFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_lineFeignClient = nameBuilder.target(account_analytic_lineFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_analytic_line createModel() {
		return new account_analytic_lineImpl();
	}


    public void get(Iaccount_analytic_line account_analytic_line){
        Iaccount_analytic_line clientModel = account_analytic_lineFeignClient.get(account_analytic_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_line.getClass(), false);
        copier.copy(clientModel, account_analytic_line, null);
    }


    public void create(Iaccount_analytic_line account_analytic_line){
        Iaccount_analytic_line clientModel = account_analytic_lineFeignClient.create((account_analytic_lineImpl)account_analytic_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_line.getClass(), false);
        copier.copy(clientModel, account_analytic_line, null);
    }


    public void removeBatch(List<Iaccount_analytic_line> account_analytic_lines){
        if(account_analytic_lines!=null){
            List<account_analytic_lineImpl> list = new ArrayList<account_analytic_lineImpl>();
            for(Iaccount_analytic_line iaccount_analytic_line :account_analytic_lines){
                list.add((account_analytic_lineImpl)iaccount_analytic_line) ;
            }
            account_analytic_lineFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_analytic_line> account_analytic_lines){
        if(account_analytic_lines!=null){
            List<account_analytic_lineImpl> list = new ArrayList<account_analytic_lineImpl>();
            for(Iaccount_analytic_line iaccount_analytic_line :account_analytic_lines){
                list.add((account_analytic_lineImpl)iaccount_analytic_line) ;
            }
            account_analytic_lineFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iaccount_analytic_line account_analytic_line){
        account_analytic_lineFeignClient.remove(account_analytic_line.getId()) ;
    }


    public void updateBatch(List<Iaccount_analytic_line> account_analytic_lines){
        if(account_analytic_lines!=null){
            List<account_analytic_lineImpl> list = new ArrayList<account_analytic_lineImpl>();
            for(Iaccount_analytic_line iaccount_analytic_line :account_analytic_lines){
                list.add((account_analytic_lineImpl)iaccount_analytic_line) ;
            }
            account_analytic_lineFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iaccount_analytic_line account_analytic_line){
        Iaccount_analytic_line clientModel = account_analytic_lineFeignClient.update(account_analytic_line.getId(),(account_analytic_lineImpl)account_analytic_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_line.getClass(), false);
        copier.copy(clientModel, account_analytic_line, null);
    }


    public Page<Iaccount_analytic_line> fetchDefault(SearchContext context){
        Page<account_analytic_lineImpl> page = this.account_analytic_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_analytic_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_analytic_line account_analytic_line){
        Iaccount_analytic_line clientModel = account_analytic_lineFeignClient.getDraft(account_analytic_line.getId(),(account_analytic_lineImpl)account_analytic_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_line.getClass(), false);
        copier.copy(clientModel, account_analytic_line, null);
    }



}

