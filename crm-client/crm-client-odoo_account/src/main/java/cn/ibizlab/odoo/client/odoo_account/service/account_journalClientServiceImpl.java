package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_journal;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_journalClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_journalImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_journalFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_journal] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_journalClientServiceImpl implements Iaccount_journalClientService {

    account_journalFeignClient account_journalFeignClient;

    @Autowired
    public account_journalClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_journalFeignClient = nameBuilder.target(account_journalFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_journalFeignClient = nameBuilder.target(account_journalFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_journal createModel() {
		return new account_journalImpl();
	}


    public void updateBatch(List<Iaccount_journal> account_journals){
        if(account_journals!=null){
            List<account_journalImpl> list = new ArrayList<account_journalImpl>();
            for(Iaccount_journal iaccount_journal :account_journals){
                list.add((account_journalImpl)iaccount_journal) ;
            }
            account_journalFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iaccount_journal account_journal){
        Iaccount_journal clientModel = account_journalFeignClient.get(account_journal.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_journal.getClass(), false);
        copier.copy(clientModel, account_journal, null);
    }


    public void create(Iaccount_journal account_journal){
        Iaccount_journal clientModel = account_journalFeignClient.create((account_journalImpl)account_journal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_journal.getClass(), false);
        copier.copy(clientModel, account_journal, null);
    }


    public void createBatch(List<Iaccount_journal> account_journals){
        if(account_journals!=null){
            List<account_journalImpl> list = new ArrayList<account_journalImpl>();
            for(Iaccount_journal iaccount_journal :account_journals){
                list.add((account_journalImpl)iaccount_journal) ;
            }
            account_journalFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_journal> fetchDefault(SearchContext context){
        Page<account_journalImpl> page = this.account_journalFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_journal account_journal){
        Iaccount_journal clientModel = account_journalFeignClient.update(account_journal.getId(),(account_journalImpl)account_journal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_journal.getClass(), false);
        copier.copy(clientModel, account_journal, null);
    }


    public void remove(Iaccount_journal account_journal){
        account_journalFeignClient.remove(account_journal.getId()) ;
    }


    public void removeBatch(List<Iaccount_journal> account_journals){
        if(account_journals!=null){
            List<account_journalImpl> list = new ArrayList<account_journalImpl>();
            for(Iaccount_journal iaccount_journal :account_journals){
                list.add((account_journalImpl)iaccount_journal) ;
            }
            account_journalFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_journal> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_journal account_journal){
        Iaccount_journal clientModel = account_journalFeignClient.getDraft(account_journal.getId(),(account_journalImpl)account_journal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_journal.getClass(), false);
        copier.copy(clientModel, account_journal, null);
    }



}

