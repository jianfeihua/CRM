package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_import_wizard;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_import_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice_import_wizard] 服务对象接口
 */
public interface account_invoice_import_wizardFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_import_wizards/updatebatch")
    public account_invoice_import_wizardImpl updateBatch(@RequestBody List<account_invoice_import_wizardImpl> account_invoice_import_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_import_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_import_wizards")
    public account_invoice_import_wizardImpl create(@RequestBody account_invoice_import_wizardImpl account_invoice_import_wizard);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_import_wizards/removebatch")
    public account_invoice_import_wizardImpl removeBatch(@RequestBody List<account_invoice_import_wizardImpl> account_invoice_import_wizards);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_import_wizards/createbatch")
    public account_invoice_import_wizardImpl createBatch(@RequestBody List<account_invoice_import_wizardImpl> account_invoice_import_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_import_wizards/{id}")
    public account_invoice_import_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_import_wizards/{id}")
    public account_invoice_import_wizardImpl update(@PathVariable("id") Integer id,@RequestBody account_invoice_import_wizardImpl account_invoice_import_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_import_wizards/fetchdefault")
    public Page<account_invoice_import_wizardImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_import_wizards/select")
    public Page<account_invoice_import_wizardImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_import_wizards/{id}/getdraft")
    public account_invoice_import_wizardImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_invoice_import_wizardImpl account_invoice_import_wizard);



}
