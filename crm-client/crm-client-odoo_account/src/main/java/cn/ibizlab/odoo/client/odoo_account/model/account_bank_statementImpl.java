package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_bank_statement] 对象
 */
public class account_bank_statementImpl implements Iaccount_bank_statement,Serializable{

    /**
     * 会计日期
     */
    public Timestamp accounting_date;

    @JsonIgnore
    public boolean accounting_dateDirtyFlag;
    
    /**
     * 默认借方科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 所有的明细行都已核销
     */
    public String all_lines_reconciled;

    @JsonIgnore
    public boolean all_lines_reconciledDirtyFlag;
    
    /**
     * 计算余额
     */
    public Double balance_end;

    @JsonIgnore
    public boolean balance_endDirtyFlag;
    
    /**
     * 结束余额
     */
    public Double balance_end_real;

    @JsonIgnore
    public boolean balance_end_realDirtyFlag;
    
    /**
     * 起始余额
     */
    public Double balance_start;

    @JsonIgnore
    public boolean balance_startDirtyFlag;
    
    /**
     * 关闭钱箱
     */
    public Integer cashbox_end_id;

    @JsonIgnore
    public boolean cashbox_end_idDirtyFlag;
    
    /**
     * 开始钱箱
     */
    public Integer cashbox_start_id;

    @JsonIgnore
    public boolean cashbox_start_idDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 关闭在
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_done;

    @JsonIgnore
    public boolean date_doneDirtyFlag;
    
    /**
     * 差异
     */
    public Double difference;

    @JsonIgnore
    public boolean differenceDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 是0
     */
    public String is_difference_zero;

    @JsonIgnore
    public boolean is_difference_zeroDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 类型
     */
    public String journal_type;

    @JsonIgnore
    public boolean journal_typeDirtyFlag;
    
    /**
     * 对账单明细行
     */
    public String line_ids;

    @JsonIgnore
    public boolean line_idsDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 需要一个动作消息的编码
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作编号
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 凭证明细数
     */
    public Integer move_line_count;

    @JsonIgnore
    public boolean move_line_countDirtyFlag;
    
    /**
     * 分录明细行
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 参考
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 会话
     */
    public Integer pos_session_id;

    @JsonIgnore
    public boolean pos_session_idDirtyFlag;
    
    /**
     * 外部引用
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 交易小计
     */
    public Double total_entry_encoding;

    @JsonIgnore
    public boolean total_entry_encodingDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [会计日期]
     */
    @JsonProperty("accounting_date")
    public Timestamp getAccounting_date(){
        return this.accounting_date ;
    }

    /**
     * 设置 [会计日期]
     */
    @JsonProperty("accounting_date")
    public void setAccounting_date(Timestamp  accounting_date){
        this.accounting_date = accounting_date ;
        this.accounting_dateDirtyFlag = true ;
    }

     /**
     * 获取 [会计日期]脏标记
     */
    @JsonIgnore
    public boolean getAccounting_dateDirtyFlag(){
        return this.accounting_dateDirtyFlag ;
    }   

    /**
     * 获取 [默认借方科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [默认借方科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认借方科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [所有的明细行都已核销]
     */
    @JsonProperty("all_lines_reconciled")
    public String getAll_lines_reconciled(){
        return this.all_lines_reconciled ;
    }

    /**
     * 设置 [所有的明细行都已核销]
     */
    @JsonProperty("all_lines_reconciled")
    public void setAll_lines_reconciled(String  all_lines_reconciled){
        this.all_lines_reconciled = all_lines_reconciled ;
        this.all_lines_reconciledDirtyFlag = true ;
    }

     /**
     * 获取 [所有的明细行都已核销]脏标记
     */
    @JsonIgnore
    public boolean getAll_lines_reconciledDirtyFlag(){
        return this.all_lines_reconciledDirtyFlag ;
    }   

    /**
     * 获取 [计算余额]
     */
    @JsonProperty("balance_end")
    public Double getBalance_end(){
        return this.balance_end ;
    }

    /**
     * 设置 [计算余额]
     */
    @JsonProperty("balance_end")
    public void setBalance_end(Double  balance_end){
        this.balance_end = balance_end ;
        this.balance_endDirtyFlag = true ;
    }

     /**
     * 获取 [计算余额]脏标记
     */
    @JsonIgnore
    public boolean getBalance_endDirtyFlag(){
        return this.balance_endDirtyFlag ;
    }   

    /**
     * 获取 [结束余额]
     */
    @JsonProperty("balance_end_real")
    public Double getBalance_end_real(){
        return this.balance_end_real ;
    }

    /**
     * 设置 [结束余额]
     */
    @JsonProperty("balance_end_real")
    public void setBalance_end_real(Double  balance_end_real){
        this.balance_end_real = balance_end_real ;
        this.balance_end_realDirtyFlag = true ;
    }

     /**
     * 获取 [结束余额]脏标记
     */
    @JsonIgnore
    public boolean getBalance_end_realDirtyFlag(){
        return this.balance_end_realDirtyFlag ;
    }   

    /**
     * 获取 [起始余额]
     */
    @JsonProperty("balance_start")
    public Double getBalance_start(){
        return this.balance_start ;
    }

    /**
     * 设置 [起始余额]
     */
    @JsonProperty("balance_start")
    public void setBalance_start(Double  balance_start){
        this.balance_start = balance_start ;
        this.balance_startDirtyFlag = true ;
    }

     /**
     * 获取 [起始余额]脏标记
     */
    @JsonIgnore
    public boolean getBalance_startDirtyFlag(){
        return this.balance_startDirtyFlag ;
    }   

    /**
     * 获取 [关闭钱箱]
     */
    @JsonProperty("cashbox_end_id")
    public Integer getCashbox_end_id(){
        return this.cashbox_end_id ;
    }

    /**
     * 设置 [关闭钱箱]
     */
    @JsonProperty("cashbox_end_id")
    public void setCashbox_end_id(Integer  cashbox_end_id){
        this.cashbox_end_id = cashbox_end_id ;
        this.cashbox_end_idDirtyFlag = true ;
    }

     /**
     * 获取 [关闭钱箱]脏标记
     */
    @JsonIgnore
    public boolean getCashbox_end_idDirtyFlag(){
        return this.cashbox_end_idDirtyFlag ;
    }   

    /**
     * 获取 [开始钱箱]
     */
    @JsonProperty("cashbox_start_id")
    public Integer getCashbox_start_id(){
        return this.cashbox_start_id ;
    }

    /**
     * 设置 [开始钱箱]
     */
    @JsonProperty("cashbox_start_id")
    public void setCashbox_start_id(Integer  cashbox_start_id){
        this.cashbox_start_id = cashbox_start_id ;
        this.cashbox_start_idDirtyFlag = true ;
    }

     /**
     * 获取 [开始钱箱]脏标记
     */
    @JsonIgnore
    public boolean getCashbox_start_idDirtyFlag(){
        return this.cashbox_start_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [关闭在]
     */
    @JsonProperty("date_done")
    public Timestamp getDate_done(){
        return this.date_done ;
    }

    /**
     * 设置 [关闭在]
     */
    @JsonProperty("date_done")
    public void setDate_done(Timestamp  date_done){
        this.date_done = date_done ;
        this.date_doneDirtyFlag = true ;
    }

     /**
     * 获取 [关闭在]脏标记
     */
    @JsonIgnore
    public boolean getDate_doneDirtyFlag(){
        return this.date_doneDirtyFlag ;
    }   

    /**
     * 获取 [差异]
     */
    @JsonProperty("difference")
    public Double getDifference(){
        return this.difference ;
    }

    /**
     * 设置 [差异]
     */
    @JsonProperty("difference")
    public void setDifference(Double  difference){
        this.difference = difference ;
        this.differenceDirtyFlag = true ;
    }

     /**
     * 获取 [差异]脏标记
     */
    @JsonIgnore
    public boolean getDifferenceDirtyFlag(){
        return this.differenceDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [是0]
     */
    @JsonProperty("is_difference_zero")
    public String getIs_difference_zero(){
        return this.is_difference_zero ;
    }

    /**
     * 设置 [是0]
     */
    @JsonProperty("is_difference_zero")
    public void setIs_difference_zero(String  is_difference_zero){
        this.is_difference_zero = is_difference_zero ;
        this.is_difference_zeroDirtyFlag = true ;
    }

     /**
     * 获取 [是0]脏标记
     */
    @JsonIgnore
    public boolean getIs_difference_zeroDirtyFlag(){
        return this.is_difference_zeroDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("journal_type")
    public String getJournal_type(){
        return this.journal_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("journal_type")
    public void setJournal_type(String  journal_type){
        this.journal_type = journal_type ;
        this.journal_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getJournal_typeDirtyFlag(){
        return this.journal_typeDirtyFlag ;
    }   

    /**
     * 获取 [对账单明细行]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return this.line_ids ;
    }

    /**
     * 设置 [对账单明细行]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [对账单明细行]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return this.line_idsDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作编号]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [凭证明细数]
     */
    @JsonProperty("move_line_count")
    public Integer getMove_line_count(){
        return this.move_line_count ;
    }

    /**
     * 设置 [凭证明细数]
     */
    @JsonProperty("move_line_count")
    public void setMove_line_count(Integer  move_line_count){
        this.move_line_count = move_line_count ;
        this.move_line_countDirtyFlag = true ;
    }

     /**
     * 获取 [凭证明细数]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_countDirtyFlag(){
        return this.move_line_countDirtyFlag ;
    }   

    /**
     * 获取 [分录明细行]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [分录明细行]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分录明细行]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [会话]
     */
    @JsonProperty("pos_session_id")
    public Integer getPos_session_id(){
        return this.pos_session_id ;
    }

    /**
     * 设置 [会话]
     */
    @JsonProperty("pos_session_id")
    public void setPos_session_id(Integer  pos_session_id){
        this.pos_session_id = pos_session_id ;
        this.pos_session_idDirtyFlag = true ;
    }

     /**
     * 获取 [会话]脏标记
     */
    @JsonIgnore
    public boolean getPos_session_idDirtyFlag(){
        return this.pos_session_idDirtyFlag ;
    }   

    /**
     * 获取 [外部引用]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [外部引用]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [外部引用]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [交易小计]
     */
    @JsonProperty("total_entry_encoding")
    public Double getTotal_entry_encoding(){
        return this.total_entry_encoding ;
    }

    /**
     * 设置 [交易小计]
     */
    @JsonProperty("total_entry_encoding")
    public void setTotal_entry_encoding(Double  total_entry_encoding){
        this.total_entry_encoding = total_entry_encoding ;
        this.total_entry_encodingDirtyFlag = true ;
    }

     /**
     * 获取 [交易小计]脏标记
     */
    @JsonIgnore
    public boolean getTotal_entry_encodingDirtyFlag(){
        return this.total_entry_encodingDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
