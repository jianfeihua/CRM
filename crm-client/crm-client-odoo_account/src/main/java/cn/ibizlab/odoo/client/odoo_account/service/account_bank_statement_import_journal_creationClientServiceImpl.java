package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_import_journal_creationClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_import_journal_creationImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_bank_statement_import_journal_creationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_bank_statement_import_journal_creation] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_bank_statement_import_journal_creationClientServiceImpl implements Iaccount_bank_statement_import_journal_creationClientService {

    account_bank_statement_import_journal_creationFeignClient account_bank_statement_import_journal_creationFeignClient;

    @Autowired
    public account_bank_statement_import_journal_creationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_import_journal_creationFeignClient = nameBuilder.target(account_bank_statement_import_journal_creationFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_import_journal_creationFeignClient = nameBuilder.target(account_bank_statement_import_journal_creationFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_bank_statement_import_journal_creation createModel() {
		return new account_bank_statement_import_journal_creationImpl();
	}


    public void create(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
        Iaccount_bank_statement_import_journal_creation clientModel = account_bank_statement_import_journal_creationFeignClient.create((account_bank_statement_import_journal_creationImpl)account_bank_statement_import_journal_creation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import_journal_creation.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import_journal_creation, null);
    }


    public void get(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
        Iaccount_bank_statement_import_journal_creation clientModel = account_bank_statement_import_journal_creationFeignClient.get(account_bank_statement_import_journal_creation.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import_journal_creation.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import_journal_creation, null);
    }


    public Page<Iaccount_bank_statement_import_journal_creation> fetchDefault(SearchContext context){
        Page<account_bank_statement_import_journal_creationImpl> page = this.account_bank_statement_import_journal_creationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
        if(account_bank_statement_import_journal_creations!=null){
            List<account_bank_statement_import_journal_creationImpl> list = new ArrayList<account_bank_statement_import_journal_creationImpl>();
            for(Iaccount_bank_statement_import_journal_creation iaccount_bank_statement_import_journal_creation :account_bank_statement_import_journal_creations){
                list.add((account_bank_statement_import_journal_creationImpl)iaccount_bank_statement_import_journal_creation) ;
            }
            account_bank_statement_import_journal_creationFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
        if(account_bank_statement_import_journal_creations!=null){
            List<account_bank_statement_import_journal_creationImpl> list = new ArrayList<account_bank_statement_import_journal_creationImpl>();
            for(Iaccount_bank_statement_import_journal_creation iaccount_bank_statement_import_journal_creation :account_bank_statement_import_journal_creations){
                list.add((account_bank_statement_import_journal_creationImpl)iaccount_bank_statement_import_journal_creation) ;
            }
            account_bank_statement_import_journal_creationFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
        if(account_bank_statement_import_journal_creations!=null){
            List<account_bank_statement_import_journal_creationImpl> list = new ArrayList<account_bank_statement_import_journal_creationImpl>();
            for(Iaccount_bank_statement_import_journal_creation iaccount_bank_statement_import_journal_creation :account_bank_statement_import_journal_creations){
                list.add((account_bank_statement_import_journal_creationImpl)iaccount_bank_statement_import_journal_creation) ;
            }
            account_bank_statement_import_journal_creationFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
        account_bank_statement_import_journal_creationFeignClient.remove(account_bank_statement_import_journal_creation.getId()) ;
    }


    public void update(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
        Iaccount_bank_statement_import_journal_creation clientModel = account_bank_statement_import_journal_creationFeignClient.update(account_bank_statement_import_journal_creation.getId(),(account_bank_statement_import_journal_creationImpl)account_bank_statement_import_journal_creation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import_journal_creation.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import_journal_creation, null);
    }


    public Page<Iaccount_bank_statement_import_journal_creation> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
        Iaccount_bank_statement_import_journal_creation clientModel = account_bank_statement_import_journal_creationFeignClient.getDraft(account_bank_statement_import_journal_creation.getId(),(account_bank_statement_import_journal_creationImpl)account_bank_statement_import_journal_creation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_import_journal_creation.getClass(), false);
        copier.copy(clientModel, account_bank_statement_import_journal_creation, null);
    }



}

