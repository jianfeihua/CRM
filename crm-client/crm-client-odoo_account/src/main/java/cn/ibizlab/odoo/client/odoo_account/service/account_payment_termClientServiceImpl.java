package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_payment_termClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_termImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_payment_termFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_payment_term] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_payment_termClientServiceImpl implements Iaccount_payment_termClientService {

    account_payment_termFeignClient account_payment_termFeignClient;

    @Autowired
    public account_payment_termClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_payment_termFeignClient = nameBuilder.target(account_payment_termFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_payment_termFeignClient = nameBuilder.target(account_payment_termFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_payment_term createModel() {
		return new account_payment_termImpl();
	}


    public void create(Iaccount_payment_term account_payment_term){
        Iaccount_payment_term clientModel = account_payment_termFeignClient.create((account_payment_termImpl)account_payment_term) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term.getClass(), false);
        copier.copy(clientModel, account_payment_term, null);
    }


    public void createBatch(List<Iaccount_payment_term> account_payment_terms){
        if(account_payment_terms!=null){
            List<account_payment_termImpl> list = new ArrayList<account_payment_termImpl>();
            for(Iaccount_payment_term iaccount_payment_term :account_payment_terms){
                list.add((account_payment_termImpl)iaccount_payment_term) ;
            }
            account_payment_termFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_payment_term> account_payment_terms){
        if(account_payment_terms!=null){
            List<account_payment_termImpl> list = new ArrayList<account_payment_termImpl>();
            for(Iaccount_payment_term iaccount_payment_term :account_payment_terms){
                list.add((account_payment_termImpl)iaccount_payment_term) ;
            }
            account_payment_termFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_payment_term> fetchDefault(SearchContext context){
        Page<account_payment_termImpl> page = this.account_payment_termFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iaccount_payment_term account_payment_term){
        account_payment_termFeignClient.remove(account_payment_term.getId()) ;
    }


    public void removeBatch(List<Iaccount_payment_term> account_payment_terms){
        if(account_payment_terms!=null){
            List<account_payment_termImpl> list = new ArrayList<account_payment_termImpl>();
            for(Iaccount_payment_term iaccount_payment_term :account_payment_terms){
                list.add((account_payment_termImpl)iaccount_payment_term) ;
            }
            account_payment_termFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iaccount_payment_term account_payment_term){
        Iaccount_payment_term clientModel = account_payment_termFeignClient.update(account_payment_term.getId(),(account_payment_termImpl)account_payment_term) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term.getClass(), false);
        copier.copy(clientModel, account_payment_term, null);
    }


    public void get(Iaccount_payment_term account_payment_term){
        Iaccount_payment_term clientModel = account_payment_termFeignClient.get(account_payment_term.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term.getClass(), false);
        copier.copy(clientModel, account_payment_term, null);
    }


    public Page<Iaccount_payment_term> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_payment_term account_payment_term){
        Iaccount_payment_term clientModel = account_payment_termFeignClient.getDraft(account_payment_term.getId(),(account_payment_termImpl)account_payment_term) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term.getClass(), false);
        copier.copy(clientModel, account_payment_term, null);
    }



}

