package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_report;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_reportClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_reportImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoice_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoice_reportClientServiceImpl implements Iaccount_invoice_reportClientService {

    account_invoice_reportFeignClient account_invoice_reportFeignClient;

    @Autowired
    public account_invoice_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_reportFeignClient = nameBuilder.target(account_invoice_reportFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_reportFeignClient = nameBuilder.target(account_invoice_reportFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice_report createModel() {
		return new account_invoice_reportImpl();
	}


    public void createBatch(List<Iaccount_invoice_report> account_invoice_reports){
        if(account_invoice_reports!=null){
            List<account_invoice_reportImpl> list = new ArrayList<account_invoice_reportImpl>();
            for(Iaccount_invoice_report iaccount_invoice_report :account_invoice_reports){
                list.add((account_invoice_reportImpl)iaccount_invoice_report) ;
            }
            account_invoice_reportFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_invoice_report> account_invoice_reports){
        if(account_invoice_reports!=null){
            List<account_invoice_reportImpl> list = new ArrayList<account_invoice_reportImpl>();
            for(Iaccount_invoice_report iaccount_invoice_report :account_invoice_reports){
                list.add((account_invoice_reportImpl)iaccount_invoice_report) ;
            }
            account_invoice_reportFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_invoice_report> account_invoice_reports){
        if(account_invoice_reports!=null){
            List<account_invoice_reportImpl> list = new ArrayList<account_invoice_reportImpl>();
            for(Iaccount_invoice_report iaccount_invoice_report :account_invoice_reports){
                list.add((account_invoice_reportImpl)iaccount_invoice_report) ;
            }
            account_invoice_reportFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_invoice_report account_invoice_report){
        account_invoice_reportFeignClient.remove(account_invoice_report.getId()) ;
    }


    public Page<Iaccount_invoice_report> fetchDefault(SearchContext context){
        Page<account_invoice_reportImpl> page = this.account_invoice_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iaccount_invoice_report account_invoice_report){
        Iaccount_invoice_report clientModel = account_invoice_reportFeignClient.get(account_invoice_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_report.getClass(), false);
        copier.copy(clientModel, account_invoice_report, null);
    }


    public void create(Iaccount_invoice_report account_invoice_report){
        Iaccount_invoice_report clientModel = account_invoice_reportFeignClient.create((account_invoice_reportImpl)account_invoice_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_report.getClass(), false);
        copier.copy(clientModel, account_invoice_report, null);
    }


    public void update(Iaccount_invoice_report account_invoice_report){
        Iaccount_invoice_report clientModel = account_invoice_reportFeignClient.update(account_invoice_report.getId(),(account_invoice_reportImpl)account_invoice_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_report.getClass(), false);
        copier.copy(clientModel, account_invoice_report, null);
    }


    public Page<Iaccount_invoice_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_invoice_report account_invoice_report){
        Iaccount_invoice_report clientModel = account_invoice_reportFeignClient.getDraft(account_invoice_report.getId(),(account_invoice_reportImpl)account_invoice_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_report.getClass(), false);
        copier.copy(clientModel, account_invoice_report, null);
    }



}

