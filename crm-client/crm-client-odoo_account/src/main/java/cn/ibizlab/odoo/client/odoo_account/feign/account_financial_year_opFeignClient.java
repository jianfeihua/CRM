package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_financial_year_op;
import cn.ibizlab.odoo.client.odoo_account.model.account_financial_year_opImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_financial_year_op] 服务对象接口
 */
public interface account_financial_year_opFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_financial_year_ops/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_financial_year_ops/removebatch")
    public account_financial_year_opImpl removeBatch(@RequestBody List<account_financial_year_opImpl> account_financial_year_ops);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_financial_year_ops/updatebatch")
    public account_financial_year_opImpl updateBatch(@RequestBody List<account_financial_year_opImpl> account_financial_year_ops);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_financial_year_ops/createbatch")
    public account_financial_year_opImpl createBatch(@RequestBody List<account_financial_year_opImpl> account_financial_year_ops);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_financial_year_ops/fetchdefault")
    public Page<account_financial_year_opImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_financial_year_ops/{id}")
    public account_financial_year_opImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_financial_year_ops/{id}")
    public account_financial_year_opImpl update(@PathVariable("id") Integer id,@RequestBody account_financial_year_opImpl account_financial_year_op);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_financial_year_ops")
    public account_financial_year_opImpl create(@RequestBody account_financial_year_opImpl account_financial_year_op);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_financial_year_ops/select")
    public Page<account_financial_year_opImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_financial_year_ops/{id}/getdraft")
    public account_financial_year_opImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_financial_year_opImpl account_financial_year_op);



}
