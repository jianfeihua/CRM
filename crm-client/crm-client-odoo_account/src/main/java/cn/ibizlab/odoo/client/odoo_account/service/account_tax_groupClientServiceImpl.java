package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax_group;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_tax_groupClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_tax_groupImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_tax_groupFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_tax_group] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_tax_groupClientServiceImpl implements Iaccount_tax_groupClientService {

    account_tax_groupFeignClient account_tax_groupFeignClient;

    @Autowired
    public account_tax_groupClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_tax_groupFeignClient = nameBuilder.target(account_tax_groupFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_tax_groupFeignClient = nameBuilder.target(account_tax_groupFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_tax_group createModel() {
		return new account_tax_groupImpl();
	}


    public void createBatch(List<Iaccount_tax_group> account_tax_groups){
        if(account_tax_groups!=null){
            List<account_tax_groupImpl> list = new ArrayList<account_tax_groupImpl>();
            for(Iaccount_tax_group iaccount_tax_group :account_tax_groups){
                list.add((account_tax_groupImpl)iaccount_tax_group) ;
            }
            account_tax_groupFeignClient.createBatch(list) ;
        }
    }


    public void create(Iaccount_tax_group account_tax_group){
        Iaccount_tax_group clientModel = account_tax_groupFeignClient.create((account_tax_groupImpl)account_tax_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_group.getClass(), false);
        copier.copy(clientModel, account_tax_group, null);
    }


    public Page<Iaccount_tax_group> fetchDefault(SearchContext context){
        Page<account_tax_groupImpl> page = this.account_tax_groupFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iaccount_tax_group account_tax_group){
        Iaccount_tax_group clientModel = account_tax_groupFeignClient.get(account_tax_group.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_group.getClass(), false);
        copier.copy(clientModel, account_tax_group, null);
    }


    public void updateBatch(List<Iaccount_tax_group> account_tax_groups){
        if(account_tax_groups!=null){
            List<account_tax_groupImpl> list = new ArrayList<account_tax_groupImpl>();
            for(Iaccount_tax_group iaccount_tax_group :account_tax_groups){
                list.add((account_tax_groupImpl)iaccount_tax_group) ;
            }
            account_tax_groupFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_tax_group account_tax_group){
        account_tax_groupFeignClient.remove(account_tax_group.getId()) ;
    }


    public void update(Iaccount_tax_group account_tax_group){
        Iaccount_tax_group clientModel = account_tax_groupFeignClient.update(account_tax_group.getId(),(account_tax_groupImpl)account_tax_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_group.getClass(), false);
        copier.copy(clientModel, account_tax_group, null);
    }


    public void removeBatch(List<Iaccount_tax_group> account_tax_groups){
        if(account_tax_groups!=null){
            List<account_tax_groupImpl> list = new ArrayList<account_tax_groupImpl>();
            for(Iaccount_tax_group iaccount_tax_group :account_tax_groups){
                list.add((account_tax_groupImpl)iaccount_tax_group) ;
            }
            account_tax_groupFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_tax_group> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_tax_group account_tax_group){
        Iaccount_tax_group clientModel = account_tax_groupFeignClient.getDraft(account_tax_group.getId(),(account_tax_groupImpl)account_tax_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_group.getClass(), false);
        copier.copy(clientModel, account_tax_group, null);
    }



}

