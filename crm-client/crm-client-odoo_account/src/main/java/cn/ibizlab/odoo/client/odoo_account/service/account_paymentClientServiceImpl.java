package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_paymentClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_paymentImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_paymentFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_payment] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_paymentClientServiceImpl implements Iaccount_paymentClientService {

    account_paymentFeignClient account_paymentFeignClient;

    @Autowired
    public account_paymentClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_paymentFeignClient = nameBuilder.target(account_paymentFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_paymentFeignClient = nameBuilder.target(account_paymentFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_payment createModel() {
		return new account_paymentImpl();
	}


    public void updateBatch(List<Iaccount_payment> account_payments){
        if(account_payments!=null){
            List<account_paymentImpl> list = new ArrayList<account_paymentImpl>();
            for(Iaccount_payment iaccount_payment :account_payments){
                list.add((account_paymentImpl)iaccount_payment) ;
            }
            account_paymentFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iaccount_payment account_payment){
        Iaccount_payment clientModel = account_paymentFeignClient.create((account_paymentImpl)account_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment.getClass(), false);
        copier.copy(clientModel, account_payment, null);
    }


    public void update(Iaccount_payment account_payment){
        Iaccount_payment clientModel = account_paymentFeignClient.update(account_payment.getId(),(account_paymentImpl)account_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment.getClass(), false);
        copier.copy(clientModel, account_payment, null);
    }


    public void remove(Iaccount_payment account_payment){
        account_paymentFeignClient.remove(account_payment.getId()) ;
    }


    public void createBatch(List<Iaccount_payment> account_payments){
        if(account_payments!=null){
            List<account_paymentImpl> list = new ArrayList<account_paymentImpl>();
            for(Iaccount_payment iaccount_payment :account_payments){
                list.add((account_paymentImpl)iaccount_payment) ;
            }
            account_paymentFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_payment> account_payments){
        if(account_payments!=null){
            List<account_paymentImpl> list = new ArrayList<account_paymentImpl>();
            for(Iaccount_payment iaccount_payment :account_payments){
                list.add((account_paymentImpl)iaccount_payment) ;
            }
            account_paymentFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_payment> fetchDefault(SearchContext context){
        Page<account_paymentImpl> page = this.account_paymentFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iaccount_payment account_payment){
        Iaccount_payment clientModel = account_paymentFeignClient.get(account_payment.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment.getClass(), false);
        copier.copy(clientModel, account_payment, null);
    }


    public Page<Iaccount_payment> select(SearchContext context){
        return null ;
    }


    public void checkKey(Iaccount_payment account_payment){
        Iaccount_payment clientModel = account_paymentFeignClient.checkKey(account_payment.getId(),(account_paymentImpl)account_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment.getClass(), false);
        copier.copy(clientModel, account_payment, null);
    }


    public void getDraft(Iaccount_payment account_payment){
        Iaccount_payment clientModel = account_paymentFeignClient.getDraft(account_payment.getId(),(account_paymentImpl)account_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment.getClass(), false);
        copier.copy(clientModel, account_payment, null);
    }


    public void save(Iaccount_payment account_payment){
        Iaccount_payment clientModel = account_paymentFeignClient.save(account_payment.getId(),(account_paymentImpl)account_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment.getClass(), false);
        copier.copy(clientModel, account_payment, null);
    }



}

