package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_refund;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_refundClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_refundImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoice_refundFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice_refund] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoice_refundClientServiceImpl implements Iaccount_invoice_refundClientService {

    account_invoice_refundFeignClient account_invoice_refundFeignClient;

    @Autowired
    public account_invoice_refundClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_refundFeignClient = nameBuilder.target(account_invoice_refundFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_refundFeignClient = nameBuilder.target(account_invoice_refundFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice_refund createModel() {
		return new account_invoice_refundImpl();
	}


    public void updateBatch(List<Iaccount_invoice_refund> account_invoice_refunds){
        if(account_invoice_refunds!=null){
            List<account_invoice_refundImpl> list = new ArrayList<account_invoice_refundImpl>();
            for(Iaccount_invoice_refund iaccount_invoice_refund :account_invoice_refunds){
                list.add((account_invoice_refundImpl)iaccount_invoice_refund) ;
            }
            account_invoice_refundFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iaccount_invoice_refund account_invoice_refund){
        Iaccount_invoice_refund clientModel = account_invoice_refundFeignClient.get(account_invoice_refund.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_refund.getClass(), false);
        copier.copy(clientModel, account_invoice_refund, null);
    }


    public void removeBatch(List<Iaccount_invoice_refund> account_invoice_refunds){
        if(account_invoice_refunds!=null){
            List<account_invoice_refundImpl> list = new ArrayList<account_invoice_refundImpl>();
            for(Iaccount_invoice_refund iaccount_invoice_refund :account_invoice_refunds){
                list.add((account_invoice_refundImpl)iaccount_invoice_refund) ;
            }
            account_invoice_refundFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iaccount_invoice_refund account_invoice_refund){
        account_invoice_refundFeignClient.remove(account_invoice_refund.getId()) ;
    }


    public Page<Iaccount_invoice_refund> fetchDefault(SearchContext context){
        Page<account_invoice_refundImpl> page = this.account_invoice_refundFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_invoice_refund account_invoice_refund){
        Iaccount_invoice_refund clientModel = account_invoice_refundFeignClient.update(account_invoice_refund.getId(),(account_invoice_refundImpl)account_invoice_refund) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_refund.getClass(), false);
        copier.copy(clientModel, account_invoice_refund, null);
    }


    public void create(Iaccount_invoice_refund account_invoice_refund){
        Iaccount_invoice_refund clientModel = account_invoice_refundFeignClient.create((account_invoice_refundImpl)account_invoice_refund) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_refund.getClass(), false);
        copier.copy(clientModel, account_invoice_refund, null);
    }


    public void createBatch(List<Iaccount_invoice_refund> account_invoice_refunds){
        if(account_invoice_refunds!=null){
            List<account_invoice_refundImpl> list = new ArrayList<account_invoice_refundImpl>();
            for(Iaccount_invoice_refund iaccount_invoice_refund :account_invoice_refunds){
                list.add((account_invoice_refundImpl)iaccount_invoice_refund) ;
            }
            account_invoice_refundFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_invoice_refund> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_invoice_refund account_invoice_refund){
        Iaccount_invoice_refund clientModel = account_invoice_refundFeignClient.getDraft(account_invoice_refund.getId(),(account_invoice_refundImpl)account_invoice_refund) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_refund.getClass(), false);
        copier.copy(clientModel, account_invoice_refund, null);
    }



}

