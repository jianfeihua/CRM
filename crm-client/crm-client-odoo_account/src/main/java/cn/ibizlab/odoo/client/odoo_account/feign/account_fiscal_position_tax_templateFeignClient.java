package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_tax_template;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_tax_templateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_fiscal_position_tax_template] 服务对象接口
 */
public interface account_fiscal_position_tax_templateFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_tax_templates")
    public account_fiscal_position_tax_templateImpl create(@RequestBody account_fiscal_position_tax_templateImpl account_fiscal_position_tax_template);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_tax_templates/{id}")
    public account_fiscal_position_tax_templateImpl update(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_tax_templateImpl account_fiscal_position_tax_template);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_tax_templates/updatebatch")
    public account_fiscal_position_tax_templateImpl updateBatch(@RequestBody List<account_fiscal_position_tax_templateImpl> account_fiscal_position_tax_templates);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_tax_templates/removebatch")
    public account_fiscal_position_tax_templateImpl removeBatch(@RequestBody List<account_fiscal_position_tax_templateImpl> account_fiscal_position_tax_templates);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_tax_templates/createbatch")
    public account_fiscal_position_tax_templateImpl createBatch(@RequestBody List<account_fiscal_position_tax_templateImpl> account_fiscal_position_tax_templates);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_tax_templates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_tax_templates/{id}")
    public account_fiscal_position_tax_templateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_tax_templates/fetchdefault")
    public Page<account_fiscal_position_tax_templateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_tax_templates/select")
    public Page<account_fiscal_position_tax_templateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_tax_templates/{id}/getdraft")
    public account_fiscal_position_tax_templateImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_tax_templateImpl account_fiscal_position_tax_template);



}
