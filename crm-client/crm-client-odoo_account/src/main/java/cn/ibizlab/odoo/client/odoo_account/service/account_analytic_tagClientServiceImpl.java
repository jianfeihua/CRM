package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_tag;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_tagClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_tagImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_analytic_tagFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_analytic_tag] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_analytic_tagClientServiceImpl implements Iaccount_analytic_tagClientService {

    account_analytic_tagFeignClient account_analytic_tagFeignClient;

    @Autowired
    public account_analytic_tagClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_tagFeignClient = nameBuilder.target(account_analytic_tagFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_tagFeignClient = nameBuilder.target(account_analytic_tagFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_analytic_tag createModel() {
		return new account_analytic_tagImpl();
	}


    public void createBatch(List<Iaccount_analytic_tag> account_analytic_tags){
        if(account_analytic_tags!=null){
            List<account_analytic_tagImpl> list = new ArrayList<account_analytic_tagImpl>();
            for(Iaccount_analytic_tag iaccount_analytic_tag :account_analytic_tags){
                list.add((account_analytic_tagImpl)iaccount_analytic_tag) ;
            }
            account_analytic_tagFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_analytic_tag> fetchDefault(SearchContext context){
        Page<account_analytic_tagImpl> page = this.account_analytic_tagFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_analytic_tag> account_analytic_tags){
        if(account_analytic_tags!=null){
            List<account_analytic_tagImpl> list = new ArrayList<account_analytic_tagImpl>();
            for(Iaccount_analytic_tag iaccount_analytic_tag :account_analytic_tags){
                list.add((account_analytic_tagImpl)iaccount_analytic_tag) ;
            }
            account_analytic_tagFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_analytic_tag account_analytic_tag){
        account_analytic_tagFeignClient.remove(account_analytic_tag.getId()) ;
    }


    public void update(Iaccount_analytic_tag account_analytic_tag){
        Iaccount_analytic_tag clientModel = account_analytic_tagFeignClient.update(account_analytic_tag.getId(),(account_analytic_tagImpl)account_analytic_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_tag.getClass(), false);
        copier.copy(clientModel, account_analytic_tag, null);
    }


    public void get(Iaccount_analytic_tag account_analytic_tag){
        Iaccount_analytic_tag clientModel = account_analytic_tagFeignClient.get(account_analytic_tag.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_tag.getClass(), false);
        copier.copy(clientModel, account_analytic_tag, null);
    }


    public void create(Iaccount_analytic_tag account_analytic_tag){
        Iaccount_analytic_tag clientModel = account_analytic_tagFeignClient.create((account_analytic_tagImpl)account_analytic_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_tag.getClass(), false);
        copier.copy(clientModel, account_analytic_tag, null);
    }


    public void removeBatch(List<Iaccount_analytic_tag> account_analytic_tags){
        if(account_analytic_tags!=null){
            List<account_analytic_tagImpl> list = new ArrayList<account_analytic_tagImpl>();
            for(Iaccount_analytic_tag iaccount_analytic_tag :account_analytic_tags){
                list.add((account_analytic_tagImpl)iaccount_analytic_tag) ;
            }
            account_analytic_tagFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_analytic_tag> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_analytic_tag account_analytic_tag){
        Iaccount_analytic_tag clientModel = account_analytic_tagFeignClient.getDraft(account_analytic_tag.getId(),(account_analytic_tagImpl)account_analytic_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_tag.getClass(), false);
        copier.copy(clientModel, account_analytic_tag, null);
    }



}

