package cn.ibizlab.odoo.client.r7rt_dyna.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.IDynaDashboard;
import cn.ibizlab.odoo.client.r7rt_dyna.model.DynaDashboardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[DynaDashboard] 服务对象接口
 */
public interface DynaDashboardFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynadashboards/select")
    public Page<DynaDashboardImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynadashboards/{dynadashboardid}/save")
    public DynaDashboardImpl save(@PathVariable("dynadashboardid") String dynadashboardid,@RequestBody DynaDashboardImpl dynadashboard);


    @RequestMapping(method = RequestMethod.PUT, value = "/r7rt_dyna/dynadashboards/{dynadashboardid}")
    public DynaDashboardImpl update(@PathVariable("dynadashboardid") String dynadashboardid,@RequestBody DynaDashboardImpl dynadashboard);


    @RequestMapping(method = RequestMethod.GET, value = "/r7rt_dyna/dynadashboards/{dynadashboardid}")
    public DynaDashboardImpl get(@PathVariable("dynadashboardid") String dynadashboardid);


    @RequestMapping(method = RequestMethod.DELETE, value = "/r7rt_dyna/dynadashboards/{dynadashboardid}")
    public Boolean remove(@PathVariable("dynadashboardid") String dynadashboardid);


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynadashboards/{dynadashboardid}/checkkey")
    public DynaDashboardImpl checkKey(@PathVariable("dynadashboardid") String dynadashboardid,@RequestBody DynaDashboardImpl dynadashboard);


    @RequestMapping(method = RequestMethod.GET, value = "/r7rt_dyna/dynadashboards/{dynadashboardid}/getdraft")
    public DynaDashboardImpl getDraft(@PathVariable("dynadashboardid") String dynadashboardid,@RequestBody DynaDashboardImpl dynadashboard);


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynadashboards")
    public DynaDashboardImpl create(@RequestBody DynaDashboardImpl dynadashboard);


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynadashboards/fetchdefault")
    public Page<DynaDashboardImpl> fetchDefault(SearchContext context);



}
