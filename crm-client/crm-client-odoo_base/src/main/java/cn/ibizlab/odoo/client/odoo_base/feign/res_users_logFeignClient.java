package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_users_log;
import cn.ibizlab.odoo.client.odoo_base.model.res_users_logImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_users_log] 服务对象接口
 */
public interface res_users_logFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users_logs/removebatch")
    public res_users_logImpl removeBatch(@RequestBody List<res_users_logImpl> res_users_logs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users_logs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users_logs")
    public res_users_logImpl create(@RequestBody res_users_logImpl res_users_log);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users_logs/fetchdefault")
    public Page<res_users_logImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users_logs/{id}")
    public res_users_logImpl update(@PathVariable("id") Integer id,@RequestBody res_users_logImpl res_users_log);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users_logs/{id}")
    public res_users_logImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users_logs/updatebatch")
    public res_users_logImpl updateBatch(@RequestBody List<res_users_logImpl> res_users_logs);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users_logs/createbatch")
    public res_users_logImpl createBatch(@RequestBody List<res_users_logImpl> res_users_logs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users_logs/select")
    public Page<res_users_logImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users_logs/{id}/getdraft")
    public res_users_logImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_users_logImpl res_users_log);



}
