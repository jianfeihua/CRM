package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_users;
import cn.ibizlab.odoo.client.odoo_base.model.res_usersImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_users] 服务对象接口
 */
public interface res_usersFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users/{id}")
    public res_usersImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users/removebatch")
    public res_usersImpl removeBatch(@RequestBody List<res_usersImpl> res_users);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users")
    public res_usersImpl create(@RequestBody res_usersImpl res_users);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users/fetchdefault")
    public Page<res_usersImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users/updatebatch")
    public res_usersImpl updateBatch(@RequestBody List<res_usersImpl> res_users);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users/createbatch")
    public res_usersImpl createBatch(@RequestBody List<res_usersImpl> res_users);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users/{id}")
    public res_usersImpl update(@PathVariable("id") Integer id,@RequestBody res_usersImpl res_users);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users/select")
    public Page<res_usersImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users/{id}/save")
    public res_usersImpl save(@PathVariable("id") Integer id,@RequestBody res_usersImpl res_users);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users/{id}/checkkey")
    public res_usersImpl checkKey(@PathVariable("id") Integer id,@RequestBody res_usersImpl res_users);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users/{id}/getdraft")
    public res_usersImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_usersImpl res_users);



}
