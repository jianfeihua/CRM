package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_title;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_partner_titleClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_titleImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_partner_titleFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_partner_title] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_partner_titleClientServiceImpl implements Ires_partner_titleClientService {

    res_partner_titleFeignClient res_partner_titleFeignClient;

    @Autowired
    public res_partner_titleClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_titleFeignClient = nameBuilder.target(res_partner_titleFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_titleFeignClient = nameBuilder.target(res_partner_titleFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_partner_title createModel() {
		return new res_partner_titleImpl();
	}


    public void remove(Ires_partner_title res_partner_title){
        res_partner_titleFeignClient.remove(res_partner_title.getId()) ;
    }


    public void updateBatch(List<Ires_partner_title> res_partner_titles){
        if(res_partner_titles!=null){
            List<res_partner_titleImpl> list = new ArrayList<res_partner_titleImpl>();
            for(Ires_partner_title ires_partner_title :res_partner_titles){
                list.add((res_partner_titleImpl)ires_partner_title) ;
            }
            res_partner_titleFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ires_partner_title res_partner_title){
        Ires_partner_title clientModel = res_partner_titleFeignClient.update(res_partner_title.getId(),(res_partner_titleImpl)res_partner_title) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_title.getClass(), false);
        copier.copy(clientModel, res_partner_title, null);
    }


    public Page<Ires_partner_title> fetchDefault(SearchContext context){
        Page<res_partner_titleImpl> page = this.res_partner_titleFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ires_partner_title res_partner_title){
        Ires_partner_title clientModel = res_partner_titleFeignClient.create((res_partner_titleImpl)res_partner_title) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_title.getClass(), false);
        copier.copy(clientModel, res_partner_title, null);
    }


    public void get(Ires_partner_title res_partner_title){
        Ires_partner_title clientModel = res_partner_titleFeignClient.get(res_partner_title.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_title.getClass(), false);
        copier.copy(clientModel, res_partner_title, null);
    }


    public void createBatch(List<Ires_partner_title> res_partner_titles){
        if(res_partner_titles!=null){
            List<res_partner_titleImpl> list = new ArrayList<res_partner_titleImpl>();
            for(Ires_partner_title ires_partner_title :res_partner_titles){
                list.add((res_partner_titleImpl)ires_partner_title) ;
            }
            res_partner_titleFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ires_partner_title> res_partner_titles){
        if(res_partner_titles!=null){
            List<res_partner_titleImpl> list = new ArrayList<res_partner_titleImpl>();
            for(Ires_partner_title ires_partner_title :res_partner_titles){
                list.add((res_partner_titleImpl)ires_partner_title) ;
            }
            res_partner_titleFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ires_partner_title> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_partner_title res_partner_title){
        Ires_partner_title clientModel = res_partner_titleFeignClient.getDraft(res_partner_title.getId(),(res_partner_titleImpl)res_partner_title) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_title.getClass(), false);
        copier.copy(clientModel, res_partner_title, null);
    }



}

