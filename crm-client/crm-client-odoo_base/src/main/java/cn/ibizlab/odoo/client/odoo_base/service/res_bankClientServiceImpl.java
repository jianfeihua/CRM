package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_bank;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_bankClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_bankImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_bankFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_bank] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_bankClientServiceImpl implements Ires_bankClientService {

    res_bankFeignClient res_bankFeignClient;

    @Autowired
    public res_bankClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_bankFeignClient = nameBuilder.target(res_bankFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_bankFeignClient = nameBuilder.target(res_bankFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_bank createModel() {
		return new res_bankImpl();
	}


    public void remove(Ires_bank res_bank){
        res_bankFeignClient.remove(res_bank.getId()) ;
    }


    public Page<Ires_bank> fetchDefault(SearchContext context){
        Page<res_bankImpl> page = this.res_bankFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ires_bank> res_banks){
        if(res_banks!=null){
            List<res_bankImpl> list = new ArrayList<res_bankImpl>();
            for(Ires_bank ires_bank :res_banks){
                list.add((res_bankImpl)ires_bank) ;
            }
            res_bankFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ires_bank> res_banks){
        if(res_banks!=null){
            List<res_bankImpl> list = new ArrayList<res_bankImpl>();
            for(Ires_bank ires_bank :res_banks){
                list.add((res_bankImpl)ires_bank) ;
            }
            res_bankFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ires_bank res_bank){
        Ires_bank clientModel = res_bankFeignClient.update(res_bank.getId(),(res_bankImpl)res_bank) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_bank.getClass(), false);
        copier.copy(clientModel, res_bank, null);
    }


    public void updateBatch(List<Ires_bank> res_banks){
        if(res_banks!=null){
            List<res_bankImpl> list = new ArrayList<res_bankImpl>();
            for(Ires_bank ires_bank :res_banks){
                list.add((res_bankImpl)ires_bank) ;
            }
            res_bankFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ires_bank res_bank){
        Ires_bank clientModel = res_bankFeignClient.get(res_bank.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_bank.getClass(), false);
        copier.copy(clientModel, res_bank, null);
    }


    public void create(Ires_bank res_bank){
        Ires_bank clientModel = res_bankFeignClient.create((res_bankImpl)res_bank) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_bank.getClass(), false);
        copier.copy(clientModel, res_bank, null);
    }


    public Page<Ires_bank> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_bank res_bank){
        Ires_bank clientModel = res_bankFeignClient.getDraft(res_bank.getId(),(res_bankImpl)res_bank) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_bank.getClass(), false);
        copier.copy(clientModel, res_bank, null);
    }



}

