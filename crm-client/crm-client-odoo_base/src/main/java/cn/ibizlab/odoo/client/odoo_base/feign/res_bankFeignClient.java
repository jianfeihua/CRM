package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_bank;
import cn.ibizlab.odoo.client.odoo_base.model.res_bankImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_bank] 服务对象接口
 */
public interface res_bankFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_banks/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_banks/fetchdefault")
    public Page<res_bankImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_banks/createbatch")
    public res_bankImpl createBatch(@RequestBody List<res_bankImpl> res_banks);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_banks/removebatch")
    public res_bankImpl removeBatch(@RequestBody List<res_bankImpl> res_banks);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_banks/{id}")
    public res_bankImpl update(@PathVariable("id") Integer id,@RequestBody res_bankImpl res_bank);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_banks/updatebatch")
    public res_bankImpl updateBatch(@RequestBody List<res_bankImpl> res_banks);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_banks/{id}")
    public res_bankImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_banks")
    public res_bankImpl create(@RequestBody res_bankImpl res_bank);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_banks/select")
    public Page<res_bankImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_banks/{id}/getdraft")
    public res_bankImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_bankImpl res_bank);



}
