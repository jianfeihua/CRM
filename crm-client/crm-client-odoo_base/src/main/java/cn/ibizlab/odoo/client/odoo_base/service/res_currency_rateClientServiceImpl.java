package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_currency_rate;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_currency_rateClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_currency_rateImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_currency_rateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_currency_rate] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_currency_rateClientServiceImpl implements Ires_currency_rateClientService {

    res_currency_rateFeignClient res_currency_rateFeignClient;

    @Autowired
    public res_currency_rateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_currency_rateFeignClient = nameBuilder.target(res_currency_rateFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_currency_rateFeignClient = nameBuilder.target(res_currency_rateFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_currency_rate createModel() {
		return new res_currency_rateImpl();
	}


    public void get(Ires_currency_rate res_currency_rate){
        Ires_currency_rate clientModel = res_currency_rateFeignClient.get(res_currency_rate.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency_rate.getClass(), false);
        copier.copy(clientModel, res_currency_rate, null);
    }


    public void createBatch(List<Ires_currency_rate> res_currency_rates){
        if(res_currency_rates!=null){
            List<res_currency_rateImpl> list = new ArrayList<res_currency_rateImpl>();
            for(Ires_currency_rate ires_currency_rate :res_currency_rates){
                list.add((res_currency_rateImpl)ires_currency_rate) ;
            }
            res_currency_rateFeignClient.createBatch(list) ;
        }
    }


    public void create(Ires_currency_rate res_currency_rate){
        Ires_currency_rate clientModel = res_currency_rateFeignClient.create((res_currency_rateImpl)res_currency_rate) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency_rate.getClass(), false);
        copier.copy(clientModel, res_currency_rate, null);
    }


    public void update(Ires_currency_rate res_currency_rate){
        Ires_currency_rate clientModel = res_currency_rateFeignClient.update(res_currency_rate.getId(),(res_currency_rateImpl)res_currency_rate) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency_rate.getClass(), false);
        copier.copy(clientModel, res_currency_rate, null);
    }


    public void removeBatch(List<Ires_currency_rate> res_currency_rates){
        if(res_currency_rates!=null){
            List<res_currency_rateImpl> list = new ArrayList<res_currency_rateImpl>();
            for(Ires_currency_rate ires_currency_rate :res_currency_rates){
                list.add((res_currency_rateImpl)ires_currency_rate) ;
            }
            res_currency_rateFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ires_currency_rate res_currency_rate){
        res_currency_rateFeignClient.remove(res_currency_rate.getId()) ;
    }


    public void updateBatch(List<Ires_currency_rate> res_currency_rates){
        if(res_currency_rates!=null){
            List<res_currency_rateImpl> list = new ArrayList<res_currency_rateImpl>();
            for(Ires_currency_rate ires_currency_rate :res_currency_rates){
                list.add((res_currency_rateImpl)ires_currency_rate) ;
            }
            res_currency_rateFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ires_currency_rate> fetchDefault(SearchContext context){
        Page<res_currency_rateImpl> page = this.res_currency_rateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ires_currency_rate> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_currency_rate res_currency_rate){
        Ires_currency_rate clientModel = res_currency_rateFeignClient.getDraft(res_currency_rate.getId(),(res_currency_rateImpl)res_currency_rate) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency_rate.getClass(), false);
        copier.copy(clientModel, res_currency_rate, null);
    }



}

