package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_partner_merge_automatic_wizardClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_partner_merge_automatic_wizardImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_partner_merge_automatic_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_partner_merge_automatic_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_partner_merge_automatic_wizardClientServiceImpl implements Ibase_partner_merge_automatic_wizardClientService {

    base_partner_merge_automatic_wizardFeignClient base_partner_merge_automatic_wizardFeignClient;

    @Autowired
    public base_partner_merge_automatic_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_partner_merge_automatic_wizardFeignClient = nameBuilder.target(base_partner_merge_automatic_wizardFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_partner_merge_automatic_wizardFeignClient = nameBuilder.target(base_partner_merge_automatic_wizardFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_partner_merge_automatic_wizard createModel() {
		return new base_partner_merge_automatic_wizardImpl();
	}


    public void remove(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
        base_partner_merge_automatic_wizardFeignClient.remove(base_partner_merge_automatic_wizard.getId()) ;
    }


    public Page<Ibase_partner_merge_automatic_wizard> fetchDefault(SearchContext context){
        Page<base_partner_merge_automatic_wizardImpl> page = this.base_partner_merge_automatic_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
        Ibase_partner_merge_automatic_wizard clientModel = base_partner_merge_automatic_wizardFeignClient.update(base_partner_merge_automatic_wizard.getId(),(base_partner_merge_automatic_wizardImpl)base_partner_merge_automatic_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_automatic_wizard.getClass(), false);
        copier.copy(clientModel, base_partner_merge_automatic_wizard, null);
    }


    public void createBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
        if(base_partner_merge_automatic_wizards!=null){
            List<base_partner_merge_automatic_wizardImpl> list = new ArrayList<base_partner_merge_automatic_wizardImpl>();
            for(Ibase_partner_merge_automatic_wizard ibase_partner_merge_automatic_wizard :base_partner_merge_automatic_wizards){
                list.add((base_partner_merge_automatic_wizardImpl)ibase_partner_merge_automatic_wizard) ;
            }
            base_partner_merge_automatic_wizardFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
        if(base_partner_merge_automatic_wizards!=null){
            List<base_partner_merge_automatic_wizardImpl> list = new ArrayList<base_partner_merge_automatic_wizardImpl>();
            for(Ibase_partner_merge_automatic_wizard ibase_partner_merge_automatic_wizard :base_partner_merge_automatic_wizards){
                list.add((base_partner_merge_automatic_wizardImpl)ibase_partner_merge_automatic_wizard) ;
            }
            base_partner_merge_automatic_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
        Ibase_partner_merge_automatic_wizard clientModel = base_partner_merge_automatic_wizardFeignClient.get(base_partner_merge_automatic_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_automatic_wizard.getClass(), false);
        copier.copy(clientModel, base_partner_merge_automatic_wizard, null);
    }


    public void create(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
        Ibase_partner_merge_automatic_wizard clientModel = base_partner_merge_automatic_wizardFeignClient.create((base_partner_merge_automatic_wizardImpl)base_partner_merge_automatic_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_automatic_wizard.getClass(), false);
        copier.copy(clientModel, base_partner_merge_automatic_wizard, null);
    }


    public void removeBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
        if(base_partner_merge_automatic_wizards!=null){
            List<base_partner_merge_automatic_wizardImpl> list = new ArrayList<base_partner_merge_automatic_wizardImpl>();
            for(Ibase_partner_merge_automatic_wizard ibase_partner_merge_automatic_wizard :base_partner_merge_automatic_wizards){
                list.add((base_partner_merge_automatic_wizardImpl)ibase_partner_merge_automatic_wizard) ;
            }
            base_partner_merge_automatic_wizardFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ibase_partner_merge_automatic_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
        Ibase_partner_merge_automatic_wizard clientModel = base_partner_merge_automatic_wizardFeignClient.getDraft(base_partner_merge_automatic_wizard.getId(),(base_partner_merge_automatic_wizardImpl)base_partner_merge_automatic_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_automatic_wizard.getClass(), false);
        copier.copy(clientModel, base_partner_merge_automatic_wizard, null);
    }



}

