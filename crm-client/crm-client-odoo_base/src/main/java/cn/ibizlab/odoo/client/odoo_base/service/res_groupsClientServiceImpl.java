package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_groups;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_groupsClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_groupsImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_groupsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_groups] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_groupsClientServiceImpl implements Ires_groupsClientService {

    res_groupsFeignClient res_groupsFeignClient;

    @Autowired
    public res_groupsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_groupsFeignClient = nameBuilder.target(res_groupsFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_groupsFeignClient = nameBuilder.target(res_groupsFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_groups createModel() {
		return new res_groupsImpl();
	}


    public void removeBatch(List<Ires_groups> res_groups){
        if(res_groups!=null){
            List<res_groupsImpl> list = new ArrayList<res_groupsImpl>();
            for(Ires_groups ires_groups :res_groups){
                list.add((res_groupsImpl)ires_groups) ;
            }
            res_groupsFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ires_groups res_groups){
        res_groupsFeignClient.remove(res_groups.getId()) ;
    }


    public void create(Ires_groups res_groups){
        Ires_groups clientModel = res_groupsFeignClient.create((res_groupsImpl)res_groups) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_groups.getClass(), false);
        copier.copy(clientModel, res_groups, null);
    }


    public void update(Ires_groups res_groups){
        Ires_groups clientModel = res_groupsFeignClient.update(res_groups.getId(),(res_groupsImpl)res_groups) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_groups.getClass(), false);
        copier.copy(clientModel, res_groups, null);
    }


    public void createBatch(List<Ires_groups> res_groups){
        if(res_groups!=null){
            List<res_groupsImpl> list = new ArrayList<res_groupsImpl>();
            for(Ires_groups ires_groups :res_groups){
                list.add((res_groupsImpl)ires_groups) ;
            }
            res_groupsFeignClient.createBatch(list) ;
        }
    }


    public void get(Ires_groups res_groups){
        Ires_groups clientModel = res_groupsFeignClient.get(res_groups.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_groups.getClass(), false);
        copier.copy(clientModel, res_groups, null);
    }


    public void updateBatch(List<Ires_groups> res_groups){
        if(res_groups!=null){
            List<res_groupsImpl> list = new ArrayList<res_groupsImpl>();
            for(Ires_groups ires_groups :res_groups){
                list.add((res_groupsImpl)ires_groups) ;
            }
            res_groupsFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ires_groups> fetchDefault(SearchContext context){
        Page<res_groupsImpl> page = this.res_groupsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ires_groups> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_groups res_groups){
        Ires_groups clientModel = res_groupsFeignClient.getDraft(res_groups.getId(),(res_groupsImpl)res_groups) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_groups.getClass(), false);
        copier.copy(clientModel, res_groups, null);
    }



}

