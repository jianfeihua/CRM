package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_automatic_wizard;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[base_partner_merge_automatic_wizard] 对象
 */
public class base_partner_merge_automatic_wizardImpl implements Ibase_partner_merge_automatic_wizard,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 当前行
     */
    public Integer current_line_id;

    @JsonIgnore
    public boolean current_line_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 目的地之联系人
     */
    public Integer dst_partner_id;

    @JsonIgnore
    public boolean dst_partner_idDirtyFlag;
    
    /**
     * 目的地之联系人
     */
    public String dst_partner_id_text;

    @JsonIgnore
    public boolean dst_partner_id_textDirtyFlag;
    
    /**
     * 与系统用户相关的联系人
     */
    public String exclude_contact;

    @JsonIgnore
    public boolean exclude_contactDirtyFlag;
    
    /**
     * 与日记账相关的联系人
     */
    public String exclude_journal_item;

    @JsonIgnore
    public boolean exclude_journal_itemDirtyFlag;
    
    /**
     * EMail
     */
    public String group_by_email;

    @JsonIgnore
    public boolean group_by_emailDirtyFlag;
    
    /**
     * 是公司
     */
    public String group_by_is_company;

    @JsonIgnore
    public boolean group_by_is_companyDirtyFlag;
    
    /**
     * 名称
     */
    public String group_by_name;

    @JsonIgnore
    public boolean group_by_nameDirtyFlag;
    
    /**
     * 上级公司
     */
    public String group_by_parent_id;

    @JsonIgnore
    public boolean group_by_parent_idDirtyFlag;
    
    /**
     * 增值税
     */
    public String group_by_vat;

    @JsonIgnore
    public boolean group_by_vatDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 明细行
     */
    public String line_ids;

    @JsonIgnore
    public boolean line_idsDirtyFlag;
    
    /**
     * 联系人组的最多联系人数量
     */
    public Integer maximum_group;

    @JsonIgnore
    public boolean maximum_groupDirtyFlag;
    
    /**
     * 联系人组
     */
    public Integer number_group;

    @JsonIgnore
    public boolean number_groupDirtyFlag;
    
    /**
     * 联系人
     */
    public String partner_ids;

    @JsonIgnore
    public boolean partner_idsDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [当前行]
     */
    @JsonProperty("current_line_id")
    public Integer getCurrent_line_id(){
        return this.current_line_id ;
    }

    /**
     * 设置 [当前行]
     */
    @JsonProperty("current_line_id")
    public void setCurrent_line_id(Integer  current_line_id){
        this.current_line_id = current_line_id ;
        this.current_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [当前行]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_line_idDirtyFlag(){
        return this.current_line_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id")
    public Integer getDst_partner_id(){
        return this.dst_partner_id ;
    }

    /**
     * 设置 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id")
    public void setDst_partner_id(Integer  dst_partner_id){
        this.dst_partner_id = dst_partner_id ;
        this.dst_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的地之联系人]脏标记
     */
    @JsonIgnore
    public boolean getDst_partner_idDirtyFlag(){
        return this.dst_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id_text")
    public String getDst_partner_id_text(){
        return this.dst_partner_id_text ;
    }

    /**
     * 设置 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id_text")
    public void setDst_partner_id_text(String  dst_partner_id_text){
        this.dst_partner_id_text = dst_partner_id_text ;
        this.dst_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的地之联系人]脏标记
     */
    @JsonIgnore
    public boolean getDst_partner_id_textDirtyFlag(){
        return this.dst_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [与系统用户相关的联系人]
     */
    @JsonProperty("exclude_contact")
    public String getExclude_contact(){
        return this.exclude_contact ;
    }

    /**
     * 设置 [与系统用户相关的联系人]
     */
    @JsonProperty("exclude_contact")
    public void setExclude_contact(String  exclude_contact){
        this.exclude_contact = exclude_contact ;
        this.exclude_contactDirtyFlag = true ;
    }

     /**
     * 获取 [与系统用户相关的联系人]脏标记
     */
    @JsonIgnore
    public boolean getExclude_contactDirtyFlag(){
        return this.exclude_contactDirtyFlag ;
    }   

    /**
     * 获取 [与日记账相关的联系人]
     */
    @JsonProperty("exclude_journal_item")
    public String getExclude_journal_item(){
        return this.exclude_journal_item ;
    }

    /**
     * 设置 [与日记账相关的联系人]
     */
    @JsonProperty("exclude_journal_item")
    public void setExclude_journal_item(String  exclude_journal_item){
        this.exclude_journal_item = exclude_journal_item ;
        this.exclude_journal_itemDirtyFlag = true ;
    }

     /**
     * 获取 [与日记账相关的联系人]脏标记
     */
    @JsonIgnore
    public boolean getExclude_journal_itemDirtyFlag(){
        return this.exclude_journal_itemDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("group_by_email")
    public String getGroup_by_email(){
        return this.group_by_email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("group_by_email")
    public void setGroup_by_email(String  group_by_email){
        this.group_by_email = group_by_email ;
        this.group_by_emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_emailDirtyFlag(){
        return this.group_by_emailDirtyFlag ;
    }   

    /**
     * 获取 [是公司]
     */
    @JsonProperty("group_by_is_company")
    public String getGroup_by_is_company(){
        return this.group_by_is_company ;
    }

    /**
     * 设置 [是公司]
     */
    @JsonProperty("group_by_is_company")
    public void setGroup_by_is_company(String  group_by_is_company){
        this.group_by_is_company = group_by_is_company ;
        this.group_by_is_companyDirtyFlag = true ;
    }

     /**
     * 获取 [是公司]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_is_companyDirtyFlag(){
        return this.group_by_is_companyDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("group_by_name")
    public String getGroup_by_name(){
        return this.group_by_name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("group_by_name")
    public void setGroup_by_name(String  group_by_name){
        this.group_by_name = group_by_name ;
        this.group_by_nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_nameDirtyFlag(){
        return this.group_by_nameDirtyFlag ;
    }   

    /**
     * 获取 [上级公司]
     */
    @JsonProperty("group_by_parent_id")
    public String getGroup_by_parent_id(){
        return this.group_by_parent_id ;
    }

    /**
     * 设置 [上级公司]
     */
    @JsonProperty("group_by_parent_id")
    public void setGroup_by_parent_id(String  group_by_parent_id){
        this.group_by_parent_id = group_by_parent_id ;
        this.group_by_parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级公司]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_parent_idDirtyFlag(){
        return this.group_by_parent_idDirtyFlag ;
    }   

    /**
     * 获取 [增值税]
     */
    @JsonProperty("group_by_vat")
    public String getGroup_by_vat(){
        return this.group_by_vat ;
    }

    /**
     * 设置 [增值税]
     */
    @JsonProperty("group_by_vat")
    public void setGroup_by_vat(String  group_by_vat){
        this.group_by_vat = group_by_vat ;
        this.group_by_vatDirtyFlag = true ;
    }

     /**
     * 获取 [增值税]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_vatDirtyFlag(){
        return this.group_by_vatDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [明细行]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return this.line_ids ;
    }

    /**
     * 设置 [明细行]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [明细行]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return this.line_idsDirtyFlag ;
    }   

    /**
     * 获取 [联系人组的最多联系人数量]
     */
    @JsonProperty("maximum_group")
    public Integer getMaximum_group(){
        return this.maximum_group ;
    }

    /**
     * 设置 [联系人组的最多联系人数量]
     */
    @JsonProperty("maximum_group")
    public void setMaximum_group(Integer  maximum_group){
        this.maximum_group = maximum_group ;
        this.maximum_groupDirtyFlag = true ;
    }

     /**
     * 获取 [联系人组的最多联系人数量]脏标记
     */
    @JsonIgnore
    public boolean getMaximum_groupDirtyFlag(){
        return this.maximum_groupDirtyFlag ;
    }   

    /**
     * 获取 [联系人组]
     */
    @JsonProperty("number_group")
    public Integer getNumber_group(){
        return this.number_group ;
    }

    /**
     * 设置 [联系人组]
     */
    @JsonProperty("number_group")
    public void setNumber_group(Integer  number_group){
        this.number_group = number_group ;
        this.number_groupDirtyFlag = true ;
    }

     /**
     * 获取 [联系人组]脏标记
     */
    @JsonIgnore
    public boolean getNumber_groupDirtyFlag(){
        return this.number_groupDirtyFlag ;
    }   

    /**
     * 获取 [联系人]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [联系人]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [联系人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
