package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_country_state;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_country_stateClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_country_stateImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_country_stateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_country_state] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_country_stateClientServiceImpl implements Ires_country_stateClientService {

    res_country_stateFeignClient res_country_stateFeignClient;

    @Autowired
    public res_country_stateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_country_stateFeignClient = nameBuilder.target(res_country_stateFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_country_stateFeignClient = nameBuilder.target(res_country_stateFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_country_state createModel() {
		return new res_country_stateImpl();
	}


    public void update(Ires_country_state res_country_state){
        Ires_country_state clientModel = res_country_stateFeignClient.update(res_country_state.getId(),(res_country_stateImpl)res_country_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_state.getClass(), false);
        copier.copy(clientModel, res_country_state, null);
    }


    public Page<Ires_country_state> fetchDefault(SearchContext context){
        Page<res_country_stateImpl> page = this.res_country_stateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ires_country_state> res_country_states){
        if(res_country_states!=null){
            List<res_country_stateImpl> list = new ArrayList<res_country_stateImpl>();
            for(Ires_country_state ires_country_state :res_country_states){
                list.add((res_country_stateImpl)ires_country_state) ;
            }
            res_country_stateFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ires_country_state> res_country_states){
        if(res_country_states!=null){
            List<res_country_stateImpl> list = new ArrayList<res_country_stateImpl>();
            for(Ires_country_state ires_country_state :res_country_states){
                list.add((res_country_stateImpl)ires_country_state) ;
            }
            res_country_stateFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ires_country_state res_country_state){
        res_country_stateFeignClient.remove(res_country_state.getId()) ;
    }


    public void get(Ires_country_state res_country_state){
        Ires_country_state clientModel = res_country_stateFeignClient.get(res_country_state.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_state.getClass(), false);
        copier.copy(clientModel, res_country_state, null);
    }


    public void create(Ires_country_state res_country_state){
        Ires_country_state clientModel = res_country_stateFeignClient.create((res_country_stateImpl)res_country_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_state.getClass(), false);
        copier.copy(clientModel, res_country_state, null);
    }


    public void updateBatch(List<Ires_country_state> res_country_states){
        if(res_country_states!=null){
            List<res_country_stateImpl> list = new ArrayList<res_country_stateImpl>();
            for(Ires_country_state ires_country_state :res_country_states){
                list.add((res_country_stateImpl)ires_country_state) ;
            }
            res_country_stateFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ires_country_state> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_country_state res_country_state){
        Ires_country_state clientModel = res_country_stateFeignClient.getDraft(res_country_state.getId(),(res_country_stateImpl)res_country_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_state.getClass(), false);
        copier.copy(clientModel, res_country_state, null);
    }



}

