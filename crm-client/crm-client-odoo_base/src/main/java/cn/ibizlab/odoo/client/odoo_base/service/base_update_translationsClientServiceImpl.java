package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_update_translations;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_update_translationsClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_update_translationsImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_update_translationsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_update_translations] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_update_translationsClientServiceImpl implements Ibase_update_translationsClientService {

    base_update_translationsFeignClient base_update_translationsFeignClient;

    @Autowired
    public base_update_translationsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_update_translationsFeignClient = nameBuilder.target(base_update_translationsFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_update_translationsFeignClient = nameBuilder.target(base_update_translationsFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_update_translations createModel() {
		return new base_update_translationsImpl();
	}


    public void remove(Ibase_update_translations base_update_translations){
        base_update_translationsFeignClient.remove(base_update_translations.getId()) ;
    }


    public void update(Ibase_update_translations base_update_translations){
        Ibase_update_translations clientModel = base_update_translationsFeignClient.update(base_update_translations.getId(),(base_update_translationsImpl)base_update_translations) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_update_translations.getClass(), false);
        copier.copy(clientModel, base_update_translations, null);
    }


    public void create(Ibase_update_translations base_update_translations){
        Ibase_update_translations clientModel = base_update_translationsFeignClient.create((base_update_translationsImpl)base_update_translations) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_update_translations.getClass(), false);
        copier.copy(clientModel, base_update_translations, null);
    }


    public void updateBatch(List<Ibase_update_translations> base_update_translations){
        if(base_update_translations!=null){
            List<base_update_translationsImpl> list = new ArrayList<base_update_translationsImpl>();
            for(Ibase_update_translations ibase_update_translations :base_update_translations){
                list.add((base_update_translationsImpl)ibase_update_translations) ;
            }
            base_update_translationsFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_update_translations> fetchDefault(SearchContext context){
        Page<base_update_translationsImpl> page = this.base_update_translationsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ibase_update_translations> base_update_translations){
        if(base_update_translations!=null){
            List<base_update_translationsImpl> list = new ArrayList<base_update_translationsImpl>();
            for(Ibase_update_translations ibase_update_translations :base_update_translations){
                list.add((base_update_translationsImpl)ibase_update_translations) ;
            }
            base_update_translationsFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_update_translations> base_update_translations){
        if(base_update_translations!=null){
            List<base_update_translationsImpl> list = new ArrayList<base_update_translationsImpl>();
            for(Ibase_update_translations ibase_update_translations :base_update_translations){
                list.add((base_update_translationsImpl)ibase_update_translations) ;
            }
            base_update_translationsFeignClient.removeBatch(list) ;
        }
    }


    public void get(Ibase_update_translations base_update_translations){
        Ibase_update_translations clientModel = base_update_translationsFeignClient.get(base_update_translations.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_update_translations.getClass(), false);
        copier.copy(clientModel, base_update_translations, null);
    }


    public Page<Ibase_update_translations> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_update_translations base_update_translations){
        Ibase_update_translations clientModel = base_update_translationsFeignClient.getDraft(base_update_translations.getId(),(base_update_translationsImpl)base_update_translations) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_update_translations.getClass(), false);
        copier.copy(clientModel, base_update_translations, null);
    }



}

