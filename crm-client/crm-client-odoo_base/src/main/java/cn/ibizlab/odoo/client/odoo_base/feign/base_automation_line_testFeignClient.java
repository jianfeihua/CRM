package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_automation_line_test;
import cn.ibizlab.odoo.client.odoo_base.model.base_automation_line_testImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_automation_line_test] 服务对象接口
 */
public interface base_automation_line_testFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automation_line_tests/removebatch")
    public base_automation_line_testImpl removeBatch(@RequestBody List<base_automation_line_testImpl> base_automation_line_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_line_tests/fetchdefault")
    public Page<base_automation_line_testImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automation_line_tests/updatebatch")
    public base_automation_line_testImpl updateBatch(@RequestBody List<base_automation_line_testImpl> base_automation_line_tests);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automation_line_tests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automation_line_tests/createbatch")
    public base_automation_line_testImpl createBatch(@RequestBody List<base_automation_line_testImpl> base_automation_line_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_line_tests/{id}")
    public base_automation_line_testImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automation_line_tests")
    public base_automation_line_testImpl create(@RequestBody base_automation_line_testImpl base_automation_line_test);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automation_line_tests/{id}")
    public base_automation_line_testImpl update(@PathVariable("id") Integer id,@RequestBody base_automation_line_testImpl base_automation_line_test);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_line_tests/select")
    public Page<base_automation_line_testImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_line_tests/{id}/getdraft")
    public base_automation_line_testImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_automation_line_testImpl base_automation_line_test);



}
