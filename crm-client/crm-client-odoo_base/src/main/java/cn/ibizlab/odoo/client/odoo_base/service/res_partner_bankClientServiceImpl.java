package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_bank;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_partner_bankClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_bankImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_partner_bankFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_partner_bank] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_partner_bankClientServiceImpl implements Ires_partner_bankClientService {

    res_partner_bankFeignClient res_partner_bankFeignClient;

    @Autowired
    public res_partner_bankClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_bankFeignClient = nameBuilder.target(res_partner_bankFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_bankFeignClient = nameBuilder.target(res_partner_bankFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_partner_bank createModel() {
		return new res_partner_bankImpl();
	}


    public void update(Ires_partner_bank res_partner_bank){
        Ires_partner_bank clientModel = res_partner_bankFeignClient.update(res_partner_bank.getId(),(res_partner_bankImpl)res_partner_bank) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_bank.getClass(), false);
        copier.copy(clientModel, res_partner_bank, null);
    }


    public void removeBatch(List<Ires_partner_bank> res_partner_banks){
        if(res_partner_banks!=null){
            List<res_partner_bankImpl> list = new ArrayList<res_partner_bankImpl>();
            for(Ires_partner_bank ires_partner_bank :res_partner_banks){
                list.add((res_partner_bankImpl)ires_partner_bank) ;
            }
            res_partner_bankFeignClient.removeBatch(list) ;
        }
    }


    public void get(Ires_partner_bank res_partner_bank){
        Ires_partner_bank clientModel = res_partner_bankFeignClient.get(res_partner_bank.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_bank.getClass(), false);
        copier.copy(clientModel, res_partner_bank, null);
    }


    public void updateBatch(List<Ires_partner_bank> res_partner_banks){
        if(res_partner_banks!=null){
            List<res_partner_bankImpl> list = new ArrayList<res_partner_bankImpl>();
            for(Ires_partner_bank ires_partner_bank :res_partner_banks){
                list.add((res_partner_bankImpl)ires_partner_bank) ;
            }
            res_partner_bankFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ires_partner_bank> fetchDefault(SearchContext context){
        Page<res_partner_bankImpl> page = this.res_partner_bankFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ires_partner_bank res_partner_bank){
        res_partner_bankFeignClient.remove(res_partner_bank.getId()) ;
    }


    public void createBatch(List<Ires_partner_bank> res_partner_banks){
        if(res_partner_banks!=null){
            List<res_partner_bankImpl> list = new ArrayList<res_partner_bankImpl>();
            for(Ires_partner_bank ires_partner_bank :res_partner_banks){
                list.add((res_partner_bankImpl)ires_partner_bank) ;
            }
            res_partner_bankFeignClient.createBatch(list) ;
        }
    }


    public void create(Ires_partner_bank res_partner_bank){
        Ires_partner_bank clientModel = res_partner_bankFeignClient.create((res_partner_bankImpl)res_partner_bank) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_bank.getClass(), false);
        copier.copy(clientModel, res_partner_bank, null);
    }


    public Page<Ires_partner_bank> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_partner_bank res_partner_bank){
        Ires_partner_bank clientModel = res_partner_bankFeignClient.getDraft(res_partner_bank.getId(),(res_partner_bankImpl)res_partner_bank) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_bank.getClass(), false);
        copier.copy(clientModel, res_partner_bank, null);
    }



}

