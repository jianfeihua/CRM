package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_automation;
import cn.ibizlab.odoo.client.odoo_base.model.base_automationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_automation] 服务对象接口
 */
public interface base_automationFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automations/removebatch")
    public base_automationImpl removeBatch(@RequestBody List<base_automationImpl> base_automations);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automations/updatebatch")
    public base_automationImpl updateBatch(@RequestBody List<base_automationImpl> base_automations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automations/createbatch")
    public base_automationImpl createBatch(@RequestBody List<base_automationImpl> base_automations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automations")
    public base_automationImpl create(@RequestBody base_automationImpl base_automation);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automations/{id}")
    public base_automationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automations/{id}")
    public base_automationImpl update(@PathVariable("id") Integer id,@RequestBody base_automationImpl base_automation);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automations/fetchdefault")
    public Page<base_automationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automations/select")
    public Page<base_automationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automations/{id}/getdraft")
    public base_automationImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_automationImpl base_automation);



}
