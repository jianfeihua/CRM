package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_config_installer;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_config_installerClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_config_installerImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_config_installerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_config_installer] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_config_installerClientServiceImpl implements Ires_config_installerClientService {

    res_config_installerFeignClient res_config_installerFeignClient;

    @Autowired
    public res_config_installerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_config_installerFeignClient = nameBuilder.target(res_config_installerFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_config_installerFeignClient = nameBuilder.target(res_config_installerFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_config_installer createModel() {
		return new res_config_installerImpl();
	}


    public void updateBatch(List<Ires_config_installer> res_config_installers){
        if(res_config_installers!=null){
            List<res_config_installerImpl> list = new ArrayList<res_config_installerImpl>();
            for(Ires_config_installer ires_config_installer :res_config_installers){
                list.add((res_config_installerImpl)ires_config_installer) ;
            }
            res_config_installerFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ires_config_installer res_config_installer){
        res_config_installerFeignClient.remove(res_config_installer.getId()) ;
    }


    public void create(Ires_config_installer res_config_installer){
        Ires_config_installer clientModel = res_config_installerFeignClient.create((res_config_installerImpl)res_config_installer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_installer.getClass(), false);
        copier.copy(clientModel, res_config_installer, null);
    }


    public Page<Ires_config_installer> fetchDefault(SearchContext context){
        Page<res_config_installerImpl> page = this.res_config_installerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ires_config_installer> res_config_installers){
        if(res_config_installers!=null){
            List<res_config_installerImpl> list = new ArrayList<res_config_installerImpl>();
            for(Ires_config_installer ires_config_installer :res_config_installers){
                list.add((res_config_installerImpl)ires_config_installer) ;
            }
            res_config_installerFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ires_config_installer res_config_installer){
        Ires_config_installer clientModel = res_config_installerFeignClient.update(res_config_installer.getId(),(res_config_installerImpl)res_config_installer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_installer.getClass(), false);
        copier.copy(clientModel, res_config_installer, null);
    }


    public void get(Ires_config_installer res_config_installer){
        Ires_config_installer clientModel = res_config_installerFeignClient.get(res_config_installer.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_installer.getClass(), false);
        copier.copy(clientModel, res_config_installer, null);
    }


    public void createBatch(List<Ires_config_installer> res_config_installers){
        if(res_config_installers!=null){
            List<res_config_installerImpl> list = new ArrayList<res_config_installerImpl>();
            for(Ires_config_installer ires_config_installer :res_config_installers){
                list.add((res_config_installerImpl)ires_config_installer) ;
            }
            res_config_installerFeignClient.createBatch(list) ;
        }
    }


    public Page<Ires_config_installer> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_config_installer res_config_installer){
        Ires_config_installer clientModel = res_config_installerFeignClient.getDraft(res_config_installer.getId(),(res_config_installerImpl)res_config_installer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_installer.getClass(), false);
        copier.copy(clientModel, res_config_installer, null);
    }



}

