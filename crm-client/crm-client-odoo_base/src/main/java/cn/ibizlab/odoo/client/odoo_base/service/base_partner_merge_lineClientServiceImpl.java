package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_line;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_partner_merge_lineClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_partner_merge_lineImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_partner_merge_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_partner_merge_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_partner_merge_lineClientServiceImpl implements Ibase_partner_merge_lineClientService {

    base_partner_merge_lineFeignClient base_partner_merge_lineFeignClient;

    @Autowired
    public base_partner_merge_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_partner_merge_lineFeignClient = nameBuilder.target(base_partner_merge_lineFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_partner_merge_lineFeignClient = nameBuilder.target(base_partner_merge_lineFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_partner_merge_line createModel() {
		return new base_partner_merge_lineImpl();
	}


    public void create(Ibase_partner_merge_line base_partner_merge_line){
        Ibase_partner_merge_line clientModel = base_partner_merge_lineFeignClient.create((base_partner_merge_lineImpl)base_partner_merge_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_line.getClass(), false);
        copier.copy(clientModel, base_partner_merge_line, null);
    }


    public void update(Ibase_partner_merge_line base_partner_merge_line){
        Ibase_partner_merge_line clientModel = base_partner_merge_lineFeignClient.update(base_partner_merge_line.getId(),(base_partner_merge_lineImpl)base_partner_merge_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_line.getClass(), false);
        copier.copy(clientModel, base_partner_merge_line, null);
    }


    public void updateBatch(List<Ibase_partner_merge_line> base_partner_merge_lines){
        if(base_partner_merge_lines!=null){
            List<base_partner_merge_lineImpl> list = new ArrayList<base_partner_merge_lineImpl>();
            for(Ibase_partner_merge_line ibase_partner_merge_line :base_partner_merge_lines){
                list.add((base_partner_merge_lineImpl)ibase_partner_merge_line) ;
            }
            base_partner_merge_lineFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ibase_partner_merge_line base_partner_merge_line){
        base_partner_merge_lineFeignClient.remove(base_partner_merge_line.getId()) ;
    }


    public void createBatch(List<Ibase_partner_merge_line> base_partner_merge_lines){
        if(base_partner_merge_lines!=null){
            List<base_partner_merge_lineImpl> list = new ArrayList<base_partner_merge_lineImpl>();
            for(Ibase_partner_merge_line ibase_partner_merge_line :base_partner_merge_lines){
                list.add((base_partner_merge_lineImpl)ibase_partner_merge_line) ;
            }
            base_partner_merge_lineFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_partner_merge_line> base_partner_merge_lines){
        if(base_partner_merge_lines!=null){
            List<base_partner_merge_lineImpl> list = new ArrayList<base_partner_merge_lineImpl>();
            for(Ibase_partner_merge_line ibase_partner_merge_line :base_partner_merge_lines){
                list.add((base_partner_merge_lineImpl)ibase_partner_merge_line) ;
            }
            base_partner_merge_lineFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ibase_partner_merge_line> fetchDefault(SearchContext context){
        Page<base_partner_merge_lineImpl> page = this.base_partner_merge_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ibase_partner_merge_line base_partner_merge_line){
        Ibase_partner_merge_line clientModel = base_partner_merge_lineFeignClient.get(base_partner_merge_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_line.getClass(), false);
        copier.copy(clientModel, base_partner_merge_line, null);
    }


    public Page<Ibase_partner_merge_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_partner_merge_line base_partner_merge_line){
        Ibase_partner_merge_line clientModel = base_partner_merge_lineFeignClient.getDraft(base_partner_merge_line.getId(),(base_partner_merge_lineImpl)base_partner_merge_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_partner_merge_line.getClass(), false);
        copier.copy(clientModel, base_partner_merge_line, null);
    }



}

