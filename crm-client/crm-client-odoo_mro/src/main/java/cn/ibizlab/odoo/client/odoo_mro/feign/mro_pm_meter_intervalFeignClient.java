package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_interval;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_intervalImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_meter_interval] 服务对象接口
 */
public interface mro_pm_meter_intervalFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_intervals/{id}")
    public mro_pm_meter_intervalImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_meter_intervalImpl mro_pm_meter_interval);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_intervals/fetchdefault")
    public Page<mro_pm_meter_intervalImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_intervals/removebatch")
    public mro_pm_meter_intervalImpl removeBatch(@RequestBody List<mro_pm_meter_intervalImpl> mro_pm_meter_intervals);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_intervals/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_intervals/createbatch")
    public mro_pm_meter_intervalImpl createBatch(@RequestBody List<mro_pm_meter_intervalImpl> mro_pm_meter_intervals);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_intervals")
    public mro_pm_meter_intervalImpl create(@RequestBody mro_pm_meter_intervalImpl mro_pm_meter_interval);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_intervals/{id}")
    public mro_pm_meter_intervalImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_intervals/updatebatch")
    public mro_pm_meter_intervalImpl updateBatch(@RequestBody List<mro_pm_meter_intervalImpl> mro_pm_meter_intervals);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_intervals/select")
    public Page<mro_pm_meter_intervalImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_intervals/{id}/getdraft")
    public mro_pm_meter_intervalImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_meter_intervalImpl mro_pm_meter_interval);



}
