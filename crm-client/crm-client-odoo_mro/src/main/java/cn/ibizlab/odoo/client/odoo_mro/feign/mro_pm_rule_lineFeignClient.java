package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_rule_line;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_rule_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_rule_line] 服务对象接口
 */
public interface mro_pm_rule_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rule_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rule_lines/updatebatch")
    public mro_pm_rule_lineImpl updateBatch(@RequestBody List<mro_pm_rule_lineImpl> mro_pm_rule_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rule_lines/removebatch")
    public mro_pm_rule_lineImpl removeBatch(@RequestBody List<mro_pm_rule_lineImpl> mro_pm_rule_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rule_lines")
    public mro_pm_rule_lineImpl create(@RequestBody mro_pm_rule_lineImpl mro_pm_rule_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rule_lines/createbatch")
    public mro_pm_rule_lineImpl createBatch(@RequestBody List<mro_pm_rule_lineImpl> mro_pm_rule_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rule_lines/fetchdefault")
    public Page<mro_pm_rule_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rule_lines/{id}")
    public mro_pm_rule_lineImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_rule_lineImpl mro_pm_rule_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rule_lines/{id}")
    public mro_pm_rule_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rule_lines/select")
    public Page<mro_pm_rule_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rule_lines/{id}/getdraft")
    public mro_pm_rule_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_rule_lineImpl mro_pm_rule_line);



}
