package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_ratio;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_pm_meter_ratioClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_ratioImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_pm_meter_ratioFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_pm_meter_ratio] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_pm_meter_ratioClientServiceImpl implements Imro_pm_meter_ratioClientService {

    mro_pm_meter_ratioFeignClient mro_pm_meter_ratioFeignClient;

    @Autowired
    public mro_pm_meter_ratioClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_meter_ratioFeignClient = nameBuilder.target(mro_pm_meter_ratioFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_meter_ratioFeignClient = nameBuilder.target(mro_pm_meter_ratioFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_pm_meter_ratio createModel() {
		return new mro_pm_meter_ratioImpl();
	}


    public void get(Imro_pm_meter_ratio mro_pm_meter_ratio){
        Imro_pm_meter_ratio clientModel = mro_pm_meter_ratioFeignClient.get(mro_pm_meter_ratio.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_ratio.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_ratio, null);
    }


    public void create(Imro_pm_meter_ratio mro_pm_meter_ratio){
        Imro_pm_meter_ratio clientModel = mro_pm_meter_ratioFeignClient.create((mro_pm_meter_ratioImpl)mro_pm_meter_ratio) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_ratio.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_ratio, null);
    }


    public void update(Imro_pm_meter_ratio mro_pm_meter_ratio){
        Imro_pm_meter_ratio clientModel = mro_pm_meter_ratioFeignClient.update(mro_pm_meter_ratio.getId(),(mro_pm_meter_ratioImpl)mro_pm_meter_ratio) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_ratio.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_ratio, null);
    }


    public void removeBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios){
        if(mro_pm_meter_ratios!=null){
            List<mro_pm_meter_ratioImpl> list = new ArrayList<mro_pm_meter_ratioImpl>();
            for(Imro_pm_meter_ratio imro_pm_meter_ratio :mro_pm_meter_ratios){
                list.add((mro_pm_meter_ratioImpl)imro_pm_meter_ratio) ;
            }
            mro_pm_meter_ratioFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imro_pm_meter_ratio mro_pm_meter_ratio){
        mro_pm_meter_ratioFeignClient.remove(mro_pm_meter_ratio.getId()) ;
    }


    public void updateBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios){
        if(mro_pm_meter_ratios!=null){
            List<mro_pm_meter_ratioImpl> list = new ArrayList<mro_pm_meter_ratioImpl>();
            for(Imro_pm_meter_ratio imro_pm_meter_ratio :mro_pm_meter_ratios){
                list.add((mro_pm_meter_ratioImpl)imro_pm_meter_ratio) ;
            }
            mro_pm_meter_ratioFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios){
        if(mro_pm_meter_ratios!=null){
            List<mro_pm_meter_ratioImpl> list = new ArrayList<mro_pm_meter_ratioImpl>();
            for(Imro_pm_meter_ratio imro_pm_meter_ratio :mro_pm_meter_ratios){
                list.add((mro_pm_meter_ratioImpl)imro_pm_meter_ratio) ;
            }
            mro_pm_meter_ratioFeignClient.createBatch(list) ;
        }
    }


    public Page<Imro_pm_meter_ratio> fetchDefault(SearchContext context){
        Page<mro_pm_meter_ratioImpl> page = this.mro_pm_meter_ratioFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Imro_pm_meter_ratio> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_pm_meter_ratio mro_pm_meter_ratio){
        Imro_pm_meter_ratio clientModel = mro_pm_meter_ratioFeignClient.getDraft(mro_pm_meter_ratio.getId(),(mro_pm_meter_ratioImpl)mro_pm_meter_ratio) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_ratio.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_ratio, null);
    }



}

