package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meterImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_meter] 服务对象接口
 */
public interface mro_pm_meterFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meters/{id}")
    public mro_pm_meterImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_meterImpl mro_pm_meter);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meters/removebatch")
    public mro_pm_meterImpl removeBatch(@RequestBody List<mro_pm_meterImpl> mro_pm_meters);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meters")
    public mro_pm_meterImpl create(@RequestBody mro_pm_meterImpl mro_pm_meter);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meters/{id}")
    public mro_pm_meterImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meters/updatebatch")
    public mro_pm_meterImpl updateBatch(@RequestBody List<mro_pm_meterImpl> mro_pm_meters);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meters/fetchdefault")
    public Page<mro_pm_meterImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meters/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meters/createbatch")
    public mro_pm_meterImpl createBatch(@RequestBody List<mro_pm_meterImpl> mro_pm_meters);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meters/select")
    public Page<mro_pm_meterImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meters/{id}/getdraft")
    public mro_pm_meterImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_meterImpl mro_pm_meter);



}
