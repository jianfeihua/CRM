package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_request_reject;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_request_rejectClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_request_rejectImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_request_rejectFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_request_reject] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_request_rejectClientServiceImpl implements Imro_request_rejectClientService {

    mro_request_rejectFeignClient mro_request_rejectFeignClient;

    @Autowired
    public mro_request_rejectClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_request_rejectFeignClient = nameBuilder.target(mro_request_rejectFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_request_rejectFeignClient = nameBuilder.target(mro_request_rejectFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_request_reject createModel() {
		return new mro_request_rejectImpl();
	}


    public void update(Imro_request_reject mro_request_reject){
        Imro_request_reject clientModel = mro_request_rejectFeignClient.update(mro_request_reject.getId(),(mro_request_rejectImpl)mro_request_reject) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request_reject.getClass(), false);
        copier.copy(clientModel, mro_request_reject, null);
    }


    public void updateBatch(List<Imro_request_reject> mro_request_rejects){
        if(mro_request_rejects!=null){
            List<mro_request_rejectImpl> list = new ArrayList<mro_request_rejectImpl>();
            for(Imro_request_reject imro_request_reject :mro_request_rejects){
                list.add((mro_request_rejectImpl)imro_request_reject) ;
            }
            mro_request_rejectFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imro_request_reject mro_request_reject){
        Imro_request_reject clientModel = mro_request_rejectFeignClient.create((mro_request_rejectImpl)mro_request_reject) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request_reject.getClass(), false);
        copier.copy(clientModel, mro_request_reject, null);
    }


    public Page<Imro_request_reject> fetchDefault(SearchContext context){
        Page<mro_request_rejectImpl> page = this.mro_request_rejectFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imro_request_reject mro_request_reject){
        mro_request_rejectFeignClient.remove(mro_request_reject.getId()) ;
    }


    public void removeBatch(List<Imro_request_reject> mro_request_rejects){
        if(mro_request_rejects!=null){
            List<mro_request_rejectImpl> list = new ArrayList<mro_request_rejectImpl>();
            for(Imro_request_reject imro_request_reject :mro_request_rejects){
                list.add((mro_request_rejectImpl)imro_request_reject) ;
            }
            mro_request_rejectFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imro_request_reject> mro_request_rejects){
        if(mro_request_rejects!=null){
            List<mro_request_rejectImpl> list = new ArrayList<mro_request_rejectImpl>();
            for(Imro_request_reject imro_request_reject :mro_request_rejects){
                list.add((mro_request_rejectImpl)imro_request_reject) ;
            }
            mro_request_rejectFeignClient.createBatch(list) ;
        }
    }


    public void get(Imro_request_reject mro_request_reject){
        Imro_request_reject clientModel = mro_request_rejectFeignClient.get(mro_request_reject.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request_reject.getClass(), false);
        copier.copy(clientModel, mro_request_reject, null);
    }


    public Page<Imro_request_reject> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_request_reject mro_request_reject){
        Imro_request_reject clientModel = mro_request_rejectFeignClient.getDraft(mro_request_reject.getId(),(mro_request_rejectImpl)mro_request_reject) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request_reject.getClass(), false);
        copier.copy(clientModel, mro_request_reject, null);
    }



}

