package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_request;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_requestClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_requestImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_requestFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_request] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_requestClientServiceImpl implements Imro_requestClientService {

    mro_requestFeignClient mro_requestFeignClient;

    @Autowired
    public mro_requestClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_requestFeignClient = nameBuilder.target(mro_requestFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_requestFeignClient = nameBuilder.target(mro_requestFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_request createModel() {
		return new mro_requestImpl();
	}


    public void remove(Imro_request mro_request){
        mro_requestFeignClient.remove(mro_request.getId()) ;
    }


    public void create(Imro_request mro_request){
        Imro_request clientModel = mro_requestFeignClient.create((mro_requestImpl)mro_request) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request.getClass(), false);
        copier.copy(clientModel, mro_request, null);
    }


    public void removeBatch(List<Imro_request> mro_requests){
        if(mro_requests!=null){
            List<mro_requestImpl> list = new ArrayList<mro_requestImpl>();
            for(Imro_request imro_request :mro_requests){
                list.add((mro_requestImpl)imro_request) ;
            }
            mro_requestFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imro_request mro_request){
        Imro_request clientModel = mro_requestFeignClient.update(mro_request.getId(),(mro_requestImpl)mro_request) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request.getClass(), false);
        copier.copy(clientModel, mro_request, null);
    }


    public void createBatch(List<Imro_request> mro_requests){
        if(mro_requests!=null){
            List<mro_requestImpl> list = new ArrayList<mro_requestImpl>();
            for(Imro_request imro_request :mro_requests){
                list.add((mro_requestImpl)imro_request) ;
            }
            mro_requestFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imro_request> mro_requests){
        if(mro_requests!=null){
            List<mro_requestImpl> list = new ArrayList<mro_requestImpl>();
            for(Imro_request imro_request :mro_requests){
                list.add((mro_requestImpl)imro_request) ;
            }
            mro_requestFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imro_request> fetchDefault(SearchContext context){
        Page<mro_requestImpl> page = this.mro_requestFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imro_request mro_request){
        Imro_request clientModel = mro_requestFeignClient.get(mro_request.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request.getClass(), false);
        copier.copy(clientModel, mro_request, null);
    }


    public Page<Imro_request> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_request mro_request){
        Imro_request clientModel = mro_requestFeignClient.getDraft(mro_request.getId(),(mro_requestImpl)mro_request) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_request.getClass(), false);
        copier.copy(clientModel, mro_request, null);
    }



}

