package cn.ibizlab.odoo.client.odoo_mro.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mro_pm_meter] 对象
 */
public class mro_pm_meterImpl implements Imro_pm_meter,Serializable{

    /**
     * Asset
     */
    public Integer asset_id;

    @JsonIgnore
    public boolean asset_idDirtyFlag;
    
    /**
     * Asset
     */
    public String asset_id_text;

    @JsonIgnore
    public boolean asset_id_textDirtyFlag;
    
    /**
     * Averaging time (days)
     */
    public Double av_time;

    @JsonIgnore
    public boolean av_timeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * Meters
     */
    public String meter_line_ids;

    @JsonIgnore
    public boolean meter_line_idsDirtyFlag;
    
    /**
     * 单位
     */
    public Integer meter_uom;

    @JsonIgnore
    public boolean meter_uomDirtyFlag;
    
    /**
     * Min Utilization (per day)
     */
    public Double min_utilization;

    @JsonIgnore
    public boolean min_utilizationDirtyFlag;
    
    /**
     * Meter
     */
    public Integer name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * Meter
     */
    public String name_text;

    @JsonIgnore
    public boolean name_textDirtyFlag;
    
    /**
     * New value
     */
    public Double new_value;

    @JsonIgnore
    public boolean new_valueDirtyFlag;
    
    /**
     * Source Meter
     */
    public Integer parent_meter_id;

    @JsonIgnore
    public boolean parent_meter_idDirtyFlag;
    
    /**
     * Source Meter
     */
    public String parent_meter_id_text;

    @JsonIgnore
    public boolean parent_meter_id_textDirtyFlag;
    
    /**
     * Ratio to Source
     */
    public Integer parent_ratio_id;

    @JsonIgnore
    public boolean parent_ratio_idDirtyFlag;
    
    /**
     * Ratio to Source
     */
    public String parent_ratio_id_text;

    @JsonIgnore
    public boolean parent_ratio_id_textDirtyFlag;
    
    /**
     * Reading Type
     */
    public String reading_type;

    @JsonIgnore
    public boolean reading_typeDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * Total Value
     */
    public Double total_value;

    @JsonIgnore
    public boolean total_valueDirtyFlag;
    
    /**
     * Utilization (per day)
     */
    public Double utilization;

    @JsonIgnore
    public boolean utilizationDirtyFlag;
    
    /**
     * 值
     */
    public Double value;

    @JsonIgnore
    public boolean valueDirtyFlag;
    
    /**
     * View Line
     */
    public String view_line_ids;

    @JsonIgnore
    public boolean view_line_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [Asset]
     */
    @JsonProperty("asset_id")
    public Integer getAsset_id(){
        return this.asset_id ;
    }

    /**
     * 设置 [Asset]
     */
    @JsonProperty("asset_id")
    public void setAsset_id(Integer  asset_id){
        this.asset_id = asset_id ;
        this.asset_idDirtyFlag = true ;
    }

     /**
     * 获取 [Asset]脏标记
     */
    @JsonIgnore
    public boolean getAsset_idDirtyFlag(){
        return this.asset_idDirtyFlag ;
    }   

    /**
     * 获取 [Asset]
     */
    @JsonProperty("asset_id_text")
    public String getAsset_id_text(){
        return this.asset_id_text ;
    }

    /**
     * 设置 [Asset]
     */
    @JsonProperty("asset_id_text")
    public void setAsset_id_text(String  asset_id_text){
        this.asset_id_text = asset_id_text ;
        this.asset_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Asset]脏标记
     */
    @JsonIgnore
    public boolean getAsset_id_textDirtyFlag(){
        return this.asset_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Averaging time (days)]
     */
    @JsonProperty("av_time")
    public Double getAv_time(){
        return this.av_time ;
    }

    /**
     * 设置 [Averaging time (days)]
     */
    @JsonProperty("av_time")
    public void setAv_time(Double  av_time){
        this.av_time = av_time ;
        this.av_timeDirtyFlag = true ;
    }

     /**
     * 获取 [Averaging time (days)]脏标记
     */
    @JsonIgnore
    public boolean getAv_timeDirtyFlag(){
        return this.av_timeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [Meters]
     */
    @JsonProperty("meter_line_ids")
    public String getMeter_line_ids(){
        return this.meter_line_ids ;
    }

    /**
     * 设置 [Meters]
     */
    @JsonProperty("meter_line_ids")
    public void setMeter_line_ids(String  meter_line_ids){
        this.meter_line_ids = meter_line_ids ;
        this.meter_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [Meters]脏标记
     */
    @JsonIgnore
    public boolean getMeter_line_idsDirtyFlag(){
        return this.meter_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("meter_uom")
    public Integer getMeter_uom(){
        return this.meter_uom ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("meter_uom")
    public void setMeter_uom(Integer  meter_uom){
        this.meter_uom = meter_uom ;
        this.meter_uomDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getMeter_uomDirtyFlag(){
        return this.meter_uomDirtyFlag ;
    }   

    /**
     * 获取 [Min Utilization (per day)]
     */
    @JsonProperty("min_utilization")
    public Double getMin_utilization(){
        return this.min_utilization ;
    }

    /**
     * 设置 [Min Utilization (per day)]
     */
    @JsonProperty("min_utilization")
    public void setMin_utilization(Double  min_utilization){
        this.min_utilization = min_utilization ;
        this.min_utilizationDirtyFlag = true ;
    }

     /**
     * 获取 [Min Utilization (per day)]脏标记
     */
    @JsonIgnore
    public boolean getMin_utilizationDirtyFlag(){
        return this.min_utilizationDirtyFlag ;
    }   

    /**
     * 获取 [Meter]
     */
    @JsonProperty("name")
    public Integer getName(){
        return this.name ;
    }

    /**
     * 设置 [Meter]
     */
    @JsonProperty("name")
    public void setName(Integer  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [Meter]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [Meter]
     */
    @JsonProperty("name_text")
    public String getName_text(){
        return this.name_text ;
    }

    /**
     * 设置 [Meter]
     */
    @JsonProperty("name_text")
    public void setName_text(String  name_text){
        this.name_text = name_text ;
        this.name_textDirtyFlag = true ;
    }

     /**
     * 获取 [Meter]脏标记
     */
    @JsonIgnore
    public boolean getName_textDirtyFlag(){
        return this.name_textDirtyFlag ;
    }   

    /**
     * 获取 [New value]
     */
    @JsonProperty("new_value")
    public Double getNew_value(){
        return this.new_value ;
    }

    /**
     * 设置 [New value]
     */
    @JsonProperty("new_value")
    public void setNew_value(Double  new_value){
        this.new_value = new_value ;
        this.new_valueDirtyFlag = true ;
    }

     /**
     * 获取 [New value]脏标记
     */
    @JsonIgnore
    public boolean getNew_valueDirtyFlag(){
        return this.new_valueDirtyFlag ;
    }   

    /**
     * 获取 [Source Meter]
     */
    @JsonProperty("parent_meter_id")
    public Integer getParent_meter_id(){
        return this.parent_meter_id ;
    }

    /**
     * 设置 [Source Meter]
     */
    @JsonProperty("parent_meter_id")
    public void setParent_meter_id(Integer  parent_meter_id){
        this.parent_meter_id = parent_meter_id ;
        this.parent_meter_idDirtyFlag = true ;
    }

     /**
     * 获取 [Source Meter]脏标记
     */
    @JsonIgnore
    public boolean getParent_meter_idDirtyFlag(){
        return this.parent_meter_idDirtyFlag ;
    }   

    /**
     * 获取 [Source Meter]
     */
    @JsonProperty("parent_meter_id_text")
    public String getParent_meter_id_text(){
        return this.parent_meter_id_text ;
    }

    /**
     * 设置 [Source Meter]
     */
    @JsonProperty("parent_meter_id_text")
    public void setParent_meter_id_text(String  parent_meter_id_text){
        this.parent_meter_id_text = parent_meter_id_text ;
        this.parent_meter_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Source Meter]脏标记
     */
    @JsonIgnore
    public boolean getParent_meter_id_textDirtyFlag(){
        return this.parent_meter_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Ratio to Source]
     */
    @JsonProperty("parent_ratio_id")
    public Integer getParent_ratio_id(){
        return this.parent_ratio_id ;
    }

    /**
     * 设置 [Ratio to Source]
     */
    @JsonProperty("parent_ratio_id")
    public void setParent_ratio_id(Integer  parent_ratio_id){
        this.parent_ratio_id = parent_ratio_id ;
        this.parent_ratio_idDirtyFlag = true ;
    }

     /**
     * 获取 [Ratio to Source]脏标记
     */
    @JsonIgnore
    public boolean getParent_ratio_idDirtyFlag(){
        return this.parent_ratio_idDirtyFlag ;
    }   

    /**
     * 获取 [Ratio to Source]
     */
    @JsonProperty("parent_ratio_id_text")
    public String getParent_ratio_id_text(){
        return this.parent_ratio_id_text ;
    }

    /**
     * 设置 [Ratio to Source]
     */
    @JsonProperty("parent_ratio_id_text")
    public void setParent_ratio_id_text(String  parent_ratio_id_text){
        this.parent_ratio_id_text = parent_ratio_id_text ;
        this.parent_ratio_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Ratio to Source]脏标记
     */
    @JsonIgnore
    public boolean getParent_ratio_id_textDirtyFlag(){
        return this.parent_ratio_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Reading Type]
     */
    @JsonProperty("reading_type")
    public String getReading_type(){
        return this.reading_type ;
    }

    /**
     * 设置 [Reading Type]
     */
    @JsonProperty("reading_type")
    public void setReading_type(String  reading_type){
        this.reading_type = reading_type ;
        this.reading_typeDirtyFlag = true ;
    }

     /**
     * 获取 [Reading Type]脏标记
     */
    @JsonIgnore
    public boolean getReading_typeDirtyFlag(){
        return this.reading_typeDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [Total Value]
     */
    @JsonProperty("total_value")
    public Double getTotal_value(){
        return this.total_value ;
    }

    /**
     * 设置 [Total Value]
     */
    @JsonProperty("total_value")
    public void setTotal_value(Double  total_value){
        this.total_value = total_value ;
        this.total_valueDirtyFlag = true ;
    }

     /**
     * 获取 [Total Value]脏标记
     */
    @JsonIgnore
    public boolean getTotal_valueDirtyFlag(){
        return this.total_valueDirtyFlag ;
    }   

    /**
     * 获取 [Utilization (per day)]
     */
    @JsonProperty("utilization")
    public Double getUtilization(){
        return this.utilization ;
    }

    /**
     * 设置 [Utilization (per day)]
     */
    @JsonProperty("utilization")
    public void setUtilization(Double  utilization){
        this.utilization = utilization ;
        this.utilizationDirtyFlag = true ;
    }

     /**
     * 获取 [Utilization (per day)]脏标记
     */
    @JsonIgnore
    public boolean getUtilizationDirtyFlag(){
        return this.utilizationDirtyFlag ;
    }   

    /**
     * 获取 [值]
     */
    @JsonProperty("value")
    public Double getValue(){
        return this.value ;
    }

    /**
     * 设置 [值]
     */
    @JsonProperty("value")
    public void setValue(Double  value){
        this.value = value ;
        this.valueDirtyFlag = true ;
    }

     /**
     * 获取 [值]脏标记
     */
    @JsonIgnore
    public boolean getValueDirtyFlag(){
        return this.valueDirtyFlag ;
    }   

    /**
     * 获取 [View Line]
     */
    @JsonProperty("view_line_ids")
    public String getView_line_ids(){
        return this.view_line_ids ;
    }

    /**
     * 设置 [View Line]
     */
    @JsonProperty("view_line_ids")
    public void setView_line_ids(String  view_line_ids){
        this.view_line_ids = view_line_ids ;
        this.view_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [View Line]脏标记
     */
    @JsonIgnore
    public boolean getView_line_idsDirtyFlag(){
        return this.view_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
