package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_definition;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_goal_definitionClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goal_definitionImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_goal_definitionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_goal_definitionClientServiceImpl implements Igamification_goal_definitionClientService {

    gamification_goal_definitionFeignClient gamification_goal_definitionFeignClient;

    @Autowired
    public gamification_goal_definitionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_goal_definitionFeignClient = nameBuilder.target(gamification_goal_definitionFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_goal_definitionFeignClient = nameBuilder.target(gamification_goal_definitionFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_goal_definition createModel() {
		return new gamification_goal_definitionImpl();
	}


    public void updateBatch(List<Igamification_goal_definition> gamification_goal_definitions){
        if(gamification_goal_definitions!=null){
            List<gamification_goal_definitionImpl> list = new ArrayList<gamification_goal_definitionImpl>();
            for(Igamification_goal_definition igamification_goal_definition :gamification_goal_definitions){
                list.add((gamification_goal_definitionImpl)igamification_goal_definition) ;
            }
            gamification_goal_definitionFeignClient.updateBatch(list) ;
        }
    }


    public Page<Igamification_goal_definition> fetchDefault(SearchContext context){
        Page<gamification_goal_definitionImpl> page = this.gamification_goal_definitionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Igamification_goal_definition gamification_goal_definition){
        Igamification_goal_definition clientModel = gamification_goal_definitionFeignClient.update(gamification_goal_definition.getId(),(gamification_goal_definitionImpl)gamification_goal_definition) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_definition.getClass(), false);
        copier.copy(clientModel, gamification_goal_definition, null);
    }


    public void get(Igamification_goal_definition gamification_goal_definition){
        Igamification_goal_definition clientModel = gamification_goal_definitionFeignClient.get(gamification_goal_definition.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_definition.getClass(), false);
        copier.copy(clientModel, gamification_goal_definition, null);
    }


    public void removeBatch(List<Igamification_goal_definition> gamification_goal_definitions){
        if(gamification_goal_definitions!=null){
            List<gamification_goal_definitionImpl> list = new ArrayList<gamification_goal_definitionImpl>();
            for(Igamification_goal_definition igamification_goal_definition :gamification_goal_definitions){
                list.add((gamification_goal_definitionImpl)igamification_goal_definition) ;
            }
            gamification_goal_definitionFeignClient.removeBatch(list) ;
        }
    }


    public void create(Igamification_goal_definition gamification_goal_definition){
        Igamification_goal_definition clientModel = gamification_goal_definitionFeignClient.create((gamification_goal_definitionImpl)gamification_goal_definition) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_definition.getClass(), false);
        copier.copy(clientModel, gamification_goal_definition, null);
    }


    public void remove(Igamification_goal_definition gamification_goal_definition){
        gamification_goal_definitionFeignClient.remove(gamification_goal_definition.getId()) ;
    }


    public void createBatch(List<Igamification_goal_definition> gamification_goal_definitions){
        if(gamification_goal_definitions!=null){
            List<gamification_goal_definitionImpl> list = new ArrayList<gamification_goal_definitionImpl>();
            for(Igamification_goal_definition igamification_goal_definition :gamification_goal_definitions){
                list.add((gamification_goal_definitionImpl)igamification_goal_definition) ;
            }
            gamification_goal_definitionFeignClient.createBatch(list) ;
        }
    }


    public Page<Igamification_goal_definition> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_goal_definition gamification_goal_definition){
        Igamification_goal_definition clientModel = gamification_goal_definitionFeignClient.getDraft(gamification_goal_definition.getId(),(gamification_goal_definitionImpl)gamification_goal_definition) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_definition.getClass(), false);
        copier.copy(clientModel, gamification_goal_definition, null);
    }



}

