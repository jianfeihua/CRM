package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_definition;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goal_definitionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
public interface gamification_goal_definitionFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goal_definitions/updatebatch")
    public gamification_goal_definitionImpl updateBatch(@RequestBody List<gamification_goal_definitionImpl> gamification_goal_definitions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_definitions/fetchdefault")
    public Page<gamification_goal_definitionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goal_definitions/{id}")
    public gamification_goal_definitionImpl update(@PathVariable("id") Integer id,@RequestBody gamification_goal_definitionImpl gamification_goal_definition);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_definitions/{id}")
    public gamification_goal_definitionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goal_definitions/removebatch")
    public gamification_goal_definitionImpl removeBatch(@RequestBody List<gamification_goal_definitionImpl> gamification_goal_definitions);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goal_definitions")
    public gamification_goal_definitionImpl create(@RequestBody gamification_goal_definitionImpl gamification_goal_definition);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goal_definitions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goal_definitions/createbatch")
    public gamification_goal_definitionImpl createBatch(@RequestBody List<gamification_goal_definitionImpl> gamification_goal_definitions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_definitions/select")
    public Page<gamification_goal_definitionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_definitions/{id}/getdraft")
    public gamification_goal_definitionImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_goal_definitionImpl gamification_goal_definition);



}
