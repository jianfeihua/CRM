package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_badge;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_badgeClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badgeImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_badgeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_badge] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_badgeClientServiceImpl implements Igamification_badgeClientService {

    gamification_badgeFeignClient gamification_badgeFeignClient;

    @Autowired
    public gamification_badgeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_badgeFeignClient = nameBuilder.target(gamification_badgeFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_badgeFeignClient = nameBuilder.target(gamification_badgeFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_badge createModel() {
		return new gamification_badgeImpl();
	}


    public void remove(Igamification_badge gamification_badge){
        gamification_badgeFeignClient.remove(gamification_badge.getId()) ;
    }


    public void get(Igamification_badge gamification_badge){
        Igamification_badge clientModel = gamification_badgeFeignClient.get(gamification_badge.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge.getClass(), false);
        copier.copy(clientModel, gamification_badge, null);
    }


    public void removeBatch(List<Igamification_badge> gamification_badges){
        if(gamification_badges!=null){
            List<gamification_badgeImpl> list = new ArrayList<gamification_badgeImpl>();
            for(Igamification_badge igamification_badge :gamification_badges){
                list.add((gamification_badgeImpl)igamification_badge) ;
            }
            gamification_badgeFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Igamification_badge> gamification_badges){
        if(gamification_badges!=null){
            List<gamification_badgeImpl> list = new ArrayList<gamification_badgeImpl>();
            for(Igamification_badge igamification_badge :gamification_badges){
                list.add((gamification_badgeImpl)igamification_badge) ;
            }
            gamification_badgeFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Igamification_badge> gamification_badges){
        if(gamification_badges!=null){
            List<gamification_badgeImpl> list = new ArrayList<gamification_badgeImpl>();
            for(Igamification_badge igamification_badge :gamification_badges){
                list.add((gamification_badgeImpl)igamification_badge) ;
            }
            gamification_badgeFeignClient.updateBatch(list) ;
        }
    }


    public void update(Igamification_badge gamification_badge){
        Igamification_badge clientModel = gamification_badgeFeignClient.update(gamification_badge.getId(),(gamification_badgeImpl)gamification_badge) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge.getClass(), false);
        copier.copy(clientModel, gamification_badge, null);
    }


    public Page<Igamification_badge> fetchDefault(SearchContext context){
        Page<gamification_badgeImpl> page = this.gamification_badgeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Igamification_badge gamification_badge){
        Igamification_badge clientModel = gamification_badgeFeignClient.create((gamification_badgeImpl)gamification_badge) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge.getClass(), false);
        copier.copy(clientModel, gamification_badge, null);
    }


    public Page<Igamification_badge> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_badge gamification_badge){
        Igamification_badge clientModel = gamification_badgeFeignClient.getDraft(gamification_badge.getId(),(gamification_badgeImpl)gamification_badge) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge.getClass(), false);
        copier.copy(clientModel, gamification_badge, null);
    }



}

