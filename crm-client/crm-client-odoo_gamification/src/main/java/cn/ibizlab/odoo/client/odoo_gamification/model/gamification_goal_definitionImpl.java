package cn.ibizlab.odoo.client.odoo_gamification.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_definition;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[gamification_goal_definition] 对象
 */
public class gamification_goal_definitionImpl implements Igamification_goal_definition,Serializable{

    /**
     * 动作
     */
    public Integer action_id;

    @JsonIgnore
    public boolean action_idDirtyFlag;
    
    /**
     * 批量用户的特有字段
     */
    public Integer batch_distinctive_field;

    @JsonIgnore
    public boolean batch_distinctive_fieldDirtyFlag;
    
    /**
     * 批量模式
     */
    public String batch_mode;

    @JsonIgnore
    public boolean batch_modeDirtyFlag;
    
    /**
     * 批处理模式的求值表达式
     */
    public String batch_user_expression;

    @JsonIgnore
    public boolean batch_user_expressionDirtyFlag;
    
    /**
     * 计算模式
     */
    public String computation_mode;

    @JsonIgnore
    public boolean computation_modeDirtyFlag;
    
    /**
     * Python 代码
     */
    public String compute_code;

    @JsonIgnore
    public boolean compute_codeDirtyFlag;
    
    /**
     * 目标绩效
     */
    public String condition;

    @JsonIgnore
    public boolean conditionDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 目标说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示为
     */
    public String display_mode;

    @JsonIgnore
    public boolean display_modeDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 筛选域
     */
    public String domain;

    @JsonIgnore
    public boolean domainDirtyFlag;
    
    /**
     * 日期字段
     */
    public Integer field_date_id;

    @JsonIgnore
    public boolean field_date_idDirtyFlag;
    
    /**
     * 字段总计
     */
    public Integer field_id;

    @JsonIgnore
    public boolean field_idDirtyFlag;
    
    /**
     * 完整的后缀
     */
    public String full_suffix;

    @JsonIgnore
    public boolean full_suffixDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 模型
     */
    public Integer model_id;

    @JsonIgnore
    public boolean model_idDirtyFlag;
    
    /**
     * 金钱值
     */
    public String monetary;

    @JsonIgnore
    public boolean monetaryDirtyFlag;
    
    /**
     * 目标定义
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 用户ID字段
     */
    public String res_id_field;

    @JsonIgnore
    public boolean res_id_fieldDirtyFlag;
    
    /**
     * 后缀
     */
    public String suffix;

    @JsonIgnore
    public boolean suffixDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [动作]
     */
    @JsonProperty("action_id")
    public Integer getAction_id(){
        return this.action_id ;
    }

    /**
     * 设置 [动作]
     */
    @JsonProperty("action_id")
    public void setAction_id(Integer  action_id){
        this.action_id = action_id ;
        this.action_idDirtyFlag = true ;
    }

     /**
     * 获取 [动作]脏标记
     */
    @JsonIgnore
    public boolean getAction_idDirtyFlag(){
        return this.action_idDirtyFlag ;
    }   

    /**
     * 获取 [批量用户的特有字段]
     */
    @JsonProperty("batch_distinctive_field")
    public Integer getBatch_distinctive_field(){
        return this.batch_distinctive_field ;
    }

    /**
     * 设置 [批量用户的特有字段]
     */
    @JsonProperty("batch_distinctive_field")
    public void setBatch_distinctive_field(Integer  batch_distinctive_field){
        this.batch_distinctive_field = batch_distinctive_field ;
        this.batch_distinctive_fieldDirtyFlag = true ;
    }

     /**
     * 获取 [批量用户的特有字段]脏标记
     */
    @JsonIgnore
    public boolean getBatch_distinctive_fieldDirtyFlag(){
        return this.batch_distinctive_fieldDirtyFlag ;
    }   

    /**
     * 获取 [批量模式]
     */
    @JsonProperty("batch_mode")
    public String getBatch_mode(){
        return this.batch_mode ;
    }

    /**
     * 设置 [批量模式]
     */
    @JsonProperty("batch_mode")
    public void setBatch_mode(String  batch_mode){
        this.batch_mode = batch_mode ;
        this.batch_modeDirtyFlag = true ;
    }

     /**
     * 获取 [批量模式]脏标记
     */
    @JsonIgnore
    public boolean getBatch_modeDirtyFlag(){
        return this.batch_modeDirtyFlag ;
    }   

    /**
     * 获取 [批处理模式的求值表达式]
     */
    @JsonProperty("batch_user_expression")
    public String getBatch_user_expression(){
        return this.batch_user_expression ;
    }

    /**
     * 设置 [批处理模式的求值表达式]
     */
    @JsonProperty("batch_user_expression")
    public void setBatch_user_expression(String  batch_user_expression){
        this.batch_user_expression = batch_user_expression ;
        this.batch_user_expressionDirtyFlag = true ;
    }

     /**
     * 获取 [批处理模式的求值表达式]脏标记
     */
    @JsonIgnore
    public boolean getBatch_user_expressionDirtyFlag(){
        return this.batch_user_expressionDirtyFlag ;
    }   

    /**
     * 获取 [计算模式]
     */
    @JsonProperty("computation_mode")
    public String getComputation_mode(){
        return this.computation_mode ;
    }

    /**
     * 设置 [计算模式]
     */
    @JsonProperty("computation_mode")
    public void setComputation_mode(String  computation_mode){
        this.computation_mode = computation_mode ;
        this.computation_modeDirtyFlag = true ;
    }

     /**
     * 获取 [计算模式]脏标记
     */
    @JsonIgnore
    public boolean getComputation_modeDirtyFlag(){
        return this.computation_modeDirtyFlag ;
    }   

    /**
     * 获取 [Python 代码]
     */
    @JsonProperty("compute_code")
    public String getCompute_code(){
        return this.compute_code ;
    }

    /**
     * 设置 [Python 代码]
     */
    @JsonProperty("compute_code")
    public void setCompute_code(String  compute_code){
        this.compute_code = compute_code ;
        this.compute_codeDirtyFlag = true ;
    }

     /**
     * 获取 [Python 代码]脏标记
     */
    @JsonIgnore
    public boolean getCompute_codeDirtyFlag(){
        return this.compute_codeDirtyFlag ;
    }   

    /**
     * 获取 [目标绩效]
     */
    @JsonProperty("condition")
    public String getCondition(){
        return this.condition ;
    }

    /**
     * 设置 [目标绩效]
     */
    @JsonProperty("condition")
    public void setCondition(String  condition){
        this.condition = condition ;
        this.conditionDirtyFlag = true ;
    }

     /**
     * 获取 [目标绩效]脏标记
     */
    @JsonIgnore
    public boolean getConditionDirtyFlag(){
        return this.conditionDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [目标说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [目标说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [目标说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示为]
     */
    @JsonProperty("display_mode")
    public String getDisplay_mode(){
        return this.display_mode ;
    }

    /**
     * 设置 [显示为]
     */
    @JsonProperty("display_mode")
    public void setDisplay_mode(String  display_mode){
        this.display_mode = display_mode ;
        this.display_modeDirtyFlag = true ;
    }

     /**
     * 获取 [显示为]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_modeDirtyFlag(){
        return this.display_modeDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [筛选域]
     */
    @JsonProperty("domain")
    public String getDomain(){
        return this.domain ;
    }

    /**
     * 设置 [筛选域]
     */
    @JsonProperty("domain")
    public void setDomain(String  domain){
        this.domain = domain ;
        this.domainDirtyFlag = true ;
    }

     /**
     * 获取 [筛选域]脏标记
     */
    @JsonIgnore
    public boolean getDomainDirtyFlag(){
        return this.domainDirtyFlag ;
    }   

    /**
     * 获取 [日期字段]
     */
    @JsonProperty("field_date_id")
    public Integer getField_date_id(){
        return this.field_date_id ;
    }

    /**
     * 设置 [日期字段]
     */
    @JsonProperty("field_date_id")
    public void setField_date_id(Integer  field_date_id){
        this.field_date_id = field_date_id ;
        this.field_date_idDirtyFlag = true ;
    }

     /**
     * 获取 [日期字段]脏标记
     */
    @JsonIgnore
    public boolean getField_date_idDirtyFlag(){
        return this.field_date_idDirtyFlag ;
    }   

    /**
     * 获取 [字段总计]
     */
    @JsonProperty("field_id")
    public Integer getField_id(){
        return this.field_id ;
    }

    /**
     * 设置 [字段总计]
     */
    @JsonProperty("field_id")
    public void setField_id(Integer  field_id){
        this.field_id = field_id ;
        this.field_idDirtyFlag = true ;
    }

     /**
     * 获取 [字段总计]脏标记
     */
    @JsonIgnore
    public boolean getField_idDirtyFlag(){
        return this.field_idDirtyFlag ;
    }   

    /**
     * 获取 [完整的后缀]
     */
    @JsonProperty("full_suffix")
    public String getFull_suffix(){
        return this.full_suffix ;
    }

    /**
     * 设置 [完整的后缀]
     */
    @JsonProperty("full_suffix")
    public void setFull_suffix(String  full_suffix){
        this.full_suffix = full_suffix ;
        this.full_suffixDirtyFlag = true ;
    }

     /**
     * 获取 [完整的后缀]脏标记
     */
    @JsonIgnore
    public boolean getFull_suffixDirtyFlag(){
        return this.full_suffixDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return this.model_id ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return this.model_idDirtyFlag ;
    }   

    /**
     * 获取 [金钱值]
     */
    @JsonProperty("monetary")
    public String getMonetary(){
        return this.monetary ;
    }

    /**
     * 设置 [金钱值]
     */
    @JsonProperty("monetary")
    public void setMonetary(String  monetary){
        this.monetary = monetary ;
        this.monetaryDirtyFlag = true ;
    }

     /**
     * 获取 [金钱值]脏标记
     */
    @JsonIgnore
    public boolean getMonetaryDirtyFlag(){
        return this.monetaryDirtyFlag ;
    }   

    /**
     * 获取 [目标定义]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [目标定义]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [目标定义]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [用户ID字段]
     */
    @JsonProperty("res_id_field")
    public String getRes_id_field(){
        return this.res_id_field ;
    }

    /**
     * 设置 [用户ID字段]
     */
    @JsonProperty("res_id_field")
    public void setRes_id_field(String  res_id_field){
        this.res_id_field = res_id_field ;
        this.res_id_fieldDirtyFlag = true ;
    }

     /**
     * 获取 [用户ID字段]脏标记
     */
    @JsonIgnore
    public boolean getRes_id_fieldDirtyFlag(){
        return this.res_id_fieldDirtyFlag ;
    }   

    /**
     * 获取 [后缀]
     */
    @JsonProperty("suffix")
    public String getSuffix(){
        return this.suffix ;
    }

    /**
     * 设置 [后缀]
     */
    @JsonProperty("suffix")
    public void setSuffix(String  suffix){
        this.suffix = suffix ;
        this.suffixDirtyFlag = true ;
    }

     /**
     * 获取 [后缀]脏标记
     */
    @JsonIgnore
    public boolean getSuffixDirtyFlag(){
        return this.suffixDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
