package cn.ibizlab.odoo.client.odoo_asset.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iasset_category;
import cn.ibizlab.odoo.client.odoo_asset.config.odoo_assetClientProperties;
import cn.ibizlab.odoo.core.client.service.Iasset_categoryClientService;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_categoryImpl;
import cn.ibizlab.odoo.client.odoo_asset.feign.asset_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[asset_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class asset_categoryClientServiceImpl implements Iasset_categoryClientService {

    asset_categoryFeignClient asset_categoryFeignClient;

    @Autowired
    public asset_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_assetClientProperties odoo_assetClientProperties) {
        if (odoo_assetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.asset_categoryFeignClient = nameBuilder.target(asset_categoryFeignClient.class,"http://"+odoo_assetClientProperties.getServiceId()+"/") ;
		}else if (odoo_assetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.asset_categoryFeignClient = nameBuilder.target(asset_categoryFeignClient.class,odoo_assetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iasset_category createModel() {
		return new asset_categoryImpl();
	}


    public void create(Iasset_category asset_category){
        Iasset_category clientModel = asset_categoryFeignClient.create((asset_categoryImpl)asset_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_category.getClass(), false);
        copier.copy(clientModel, asset_category, null);
    }


    public void get(Iasset_category asset_category){
        Iasset_category clientModel = asset_categoryFeignClient.get(asset_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_category.getClass(), false);
        copier.copy(clientModel, asset_category, null);
    }


    public Page<Iasset_category> fetchDefault(SearchContext context){
        Page<asset_categoryImpl> page = this.asset_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iasset_category asset_category){
        asset_categoryFeignClient.remove(asset_category.getId()) ;
    }


    public void removeBatch(List<Iasset_category> asset_categories){
        if(asset_categories!=null){
            List<asset_categoryImpl> list = new ArrayList<asset_categoryImpl>();
            for(Iasset_category iasset_category :asset_categories){
                list.add((asset_categoryImpl)iasset_category) ;
            }
            asset_categoryFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iasset_category> asset_categories){
        if(asset_categories!=null){
            List<asset_categoryImpl> list = new ArrayList<asset_categoryImpl>();
            for(Iasset_category iasset_category :asset_categories){
                list.add((asset_categoryImpl)iasset_category) ;
            }
            asset_categoryFeignClient.createBatch(list) ;
        }
    }


    public void update(Iasset_category asset_category){
        Iasset_category clientModel = asset_categoryFeignClient.update(asset_category.getId(),(asset_categoryImpl)asset_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_category.getClass(), false);
        copier.copy(clientModel, asset_category, null);
    }


    public void updateBatch(List<Iasset_category> asset_categories){
        if(asset_categories!=null){
            List<asset_categoryImpl> list = new ArrayList<asset_categoryImpl>();
            for(Iasset_category iasset_category :asset_categories){
                list.add((asset_categoryImpl)iasset_category) ;
            }
            asset_categoryFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iasset_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iasset_category asset_category){
        Iasset_category clientModel = asset_categoryFeignClient.getDraft(asset_category.getId(),(asset_categoryImpl)asset_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_category.getClass(), false);
        copier.copy(clientModel, asset_category, null);
    }



}

