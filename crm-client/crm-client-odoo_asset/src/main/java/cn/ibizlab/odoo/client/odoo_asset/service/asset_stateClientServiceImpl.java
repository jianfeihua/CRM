package cn.ibizlab.odoo.client.odoo_asset.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iasset_state;
import cn.ibizlab.odoo.client.odoo_asset.config.odoo_assetClientProperties;
import cn.ibizlab.odoo.core.client.service.Iasset_stateClientService;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_stateImpl;
import cn.ibizlab.odoo.client.odoo_asset.feign.asset_stateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[asset_state] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class asset_stateClientServiceImpl implements Iasset_stateClientService {

    asset_stateFeignClient asset_stateFeignClient;

    @Autowired
    public asset_stateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_assetClientProperties odoo_assetClientProperties) {
        if (odoo_assetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.asset_stateFeignClient = nameBuilder.target(asset_stateFeignClient.class,"http://"+odoo_assetClientProperties.getServiceId()+"/") ;
		}else if (odoo_assetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.asset_stateFeignClient = nameBuilder.target(asset_stateFeignClient.class,odoo_assetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iasset_state createModel() {
		return new asset_stateImpl();
	}


    public void updateBatch(List<Iasset_state> asset_states){
        if(asset_states!=null){
            List<asset_stateImpl> list = new ArrayList<asset_stateImpl>();
            for(Iasset_state iasset_state :asset_states){
                list.add((asset_stateImpl)iasset_state) ;
            }
            asset_stateFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iasset_state asset_state){
        Iasset_state clientModel = asset_stateFeignClient.get(asset_state.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_state.getClass(), false);
        copier.copy(clientModel, asset_state, null);
    }


    public void create(Iasset_state asset_state){
        Iasset_state clientModel = asset_stateFeignClient.create((asset_stateImpl)asset_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_state.getClass(), false);
        copier.copy(clientModel, asset_state, null);
    }


    public Page<Iasset_state> fetchDefault(SearchContext context){
        Page<asset_stateImpl> page = this.asset_stateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iasset_state> asset_states){
        if(asset_states!=null){
            List<asset_stateImpl> list = new ArrayList<asset_stateImpl>();
            for(Iasset_state iasset_state :asset_states){
                list.add((asset_stateImpl)iasset_state) ;
            }
            asset_stateFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iasset_state asset_state){
        asset_stateFeignClient.remove(asset_state.getId()) ;
    }


    public void createBatch(List<Iasset_state> asset_states){
        if(asset_states!=null){
            List<asset_stateImpl> list = new ArrayList<asset_stateImpl>();
            for(Iasset_state iasset_state :asset_states){
                list.add((asset_stateImpl)iasset_state) ;
            }
            asset_stateFeignClient.createBatch(list) ;
        }
    }


    public void update(Iasset_state asset_state){
        Iasset_state clientModel = asset_stateFeignClient.update(asset_state.getId(),(asset_stateImpl)asset_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_state.getClass(), false);
        copier.copy(clientModel, asset_state, null);
    }


    public Page<Iasset_state> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iasset_state asset_state){
        Iasset_state clientModel = asset_stateFeignClient.getDraft(asset_state.getId(),(asset_stateImpl)asset_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_state.getClass(), false);
        copier.copy(clientModel, asset_state, null);
    }



}

