package cn.ibizlab.odoo.client.odoo_note.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Inote_stage;
import cn.ibizlab.odoo.client.odoo_note.model.note_stageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[note_stage] 服务对象接口
 */
public interface note_stageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_stages/removebatch")
    public note_stageImpl removeBatch(@RequestBody List<note_stageImpl> note_stages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_stages/{id}")
    public note_stageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_stages/{id}")
    public note_stageImpl update(@PathVariable("id") Integer id,@RequestBody note_stageImpl note_stage);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_stages")
    public note_stageImpl create(@RequestBody note_stageImpl note_stage);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_stages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_stages/fetchdefault")
    public Page<note_stageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_stages/updatebatch")
    public note_stageImpl updateBatch(@RequestBody List<note_stageImpl> note_stages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_stages/createbatch")
    public note_stageImpl createBatch(@RequestBody List<note_stageImpl> note_stages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_stages/select")
    public Page<note_stageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_stages/{id}/getdraft")
    public note_stageImpl getDraft(@PathVariable("id") Integer id,@RequestBody note_stageImpl note_stage);



}
