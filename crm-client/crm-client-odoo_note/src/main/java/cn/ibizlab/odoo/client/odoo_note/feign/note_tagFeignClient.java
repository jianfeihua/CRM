package cn.ibizlab.odoo.client.odoo_note.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Inote_tag;
import cn.ibizlab.odoo.client.odoo_note.model.note_tagImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[note_tag] 服务对象接口
 */
public interface note_tagFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_tags/removebatch")
    public note_tagImpl removeBatch(@RequestBody List<note_tagImpl> note_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_tags/{id}")
    public note_tagImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_tags")
    public note_tagImpl create(@RequestBody note_tagImpl note_tag);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_tags/fetchdefault")
    public Page<note_tagImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_tags/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_tags/createbatch")
    public note_tagImpl createBatch(@RequestBody List<note_tagImpl> note_tags);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_tags/updatebatch")
    public note_tagImpl updateBatch(@RequestBody List<note_tagImpl> note_tags);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_tags/{id}")
    public note_tagImpl update(@PathVariable("id") Integer id,@RequestBody note_tagImpl note_tag);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_tags/select")
    public Page<note_tagImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_tags/{id}/getdraft")
    public note_tagImpl getDraft(@PathVariable("id") Integer id,@RequestBody note_tagImpl note_tag);



}
