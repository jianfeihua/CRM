package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_stage;
import cn.ibizlab.odoo.client.odoo_maintenance.config.odoo_maintenanceClientProperties;
import cn.ibizlab.odoo.core.client.service.Imaintenance_stageClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_stageImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.feign.maintenance_stageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[maintenance_stage] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class maintenance_stageClientServiceImpl implements Imaintenance_stageClientService {

    maintenance_stageFeignClient maintenance_stageFeignClient;

    @Autowired
    public maintenance_stageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_maintenanceClientProperties odoo_maintenanceClientProperties) {
        if (odoo_maintenanceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_stageFeignClient = nameBuilder.target(maintenance_stageFeignClient.class,"http://"+odoo_maintenanceClientProperties.getServiceId()+"/") ;
		}else if (odoo_maintenanceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_stageFeignClient = nameBuilder.target(maintenance_stageFeignClient.class,odoo_maintenanceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imaintenance_stage createModel() {
		return new maintenance_stageImpl();
	}


    public Page<Imaintenance_stage> fetchDefault(SearchContext context){
        Page<maintenance_stageImpl> page = this.maintenance_stageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imaintenance_stage maintenance_stage){
        Imaintenance_stage clientModel = maintenance_stageFeignClient.get(maintenance_stage.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_stage.getClass(), false);
        copier.copy(clientModel, maintenance_stage, null);
    }


    public void create(Imaintenance_stage maintenance_stage){
        Imaintenance_stage clientModel = maintenance_stageFeignClient.create((maintenance_stageImpl)maintenance_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_stage.getClass(), false);
        copier.copy(clientModel, maintenance_stage, null);
    }


    public void remove(Imaintenance_stage maintenance_stage){
        maintenance_stageFeignClient.remove(maintenance_stage.getId()) ;
    }


    public void update(Imaintenance_stage maintenance_stage){
        Imaintenance_stage clientModel = maintenance_stageFeignClient.update(maintenance_stage.getId(),(maintenance_stageImpl)maintenance_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_stage.getClass(), false);
        copier.copy(clientModel, maintenance_stage, null);
    }


    public void removeBatch(List<Imaintenance_stage> maintenance_stages){
        if(maintenance_stages!=null){
            List<maintenance_stageImpl> list = new ArrayList<maintenance_stageImpl>();
            for(Imaintenance_stage imaintenance_stage :maintenance_stages){
                list.add((maintenance_stageImpl)imaintenance_stage) ;
            }
            maintenance_stageFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Imaintenance_stage> maintenance_stages){
        if(maintenance_stages!=null){
            List<maintenance_stageImpl> list = new ArrayList<maintenance_stageImpl>();
            for(Imaintenance_stage imaintenance_stage :maintenance_stages){
                list.add((maintenance_stageImpl)imaintenance_stage) ;
            }
            maintenance_stageFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Imaintenance_stage> maintenance_stages){
        if(maintenance_stages!=null){
            List<maintenance_stageImpl> list = new ArrayList<maintenance_stageImpl>();
            for(Imaintenance_stage imaintenance_stage :maintenance_stages){
                list.add((maintenance_stageImpl)imaintenance_stage) ;
            }
            maintenance_stageFeignClient.createBatch(list) ;
        }
    }


    public Page<Imaintenance_stage> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imaintenance_stage maintenance_stage){
        Imaintenance_stage clientModel = maintenance_stageFeignClient.getDraft(maintenance_stage.getId(),(maintenance_stageImpl)maintenance_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_stage.getClass(), false);
        copier.copy(clientModel, maintenance_stage, null);
    }



}

