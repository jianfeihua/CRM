package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment_category;
import cn.ibizlab.odoo.client.odoo_maintenance.config.odoo_maintenanceClientProperties;
import cn.ibizlab.odoo.core.client.service.Imaintenance_equipment_categoryClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_equipment_categoryImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.feign.maintenance_equipment_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[maintenance_equipment_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class maintenance_equipment_categoryClientServiceImpl implements Imaintenance_equipment_categoryClientService {

    maintenance_equipment_categoryFeignClient maintenance_equipment_categoryFeignClient;

    @Autowired
    public maintenance_equipment_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_maintenanceClientProperties odoo_maintenanceClientProperties) {
        if (odoo_maintenanceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_equipment_categoryFeignClient = nameBuilder.target(maintenance_equipment_categoryFeignClient.class,"http://"+odoo_maintenanceClientProperties.getServiceId()+"/") ;
		}else if (odoo_maintenanceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_equipment_categoryFeignClient = nameBuilder.target(maintenance_equipment_categoryFeignClient.class,odoo_maintenanceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imaintenance_equipment_category createModel() {
		return new maintenance_equipment_categoryImpl();
	}


    public void createBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories){
        if(maintenance_equipment_categories!=null){
            List<maintenance_equipment_categoryImpl> list = new ArrayList<maintenance_equipment_categoryImpl>();
            for(Imaintenance_equipment_category imaintenance_equipment_category :maintenance_equipment_categories){
                list.add((maintenance_equipment_categoryImpl)imaintenance_equipment_category) ;
            }
            maintenance_equipment_categoryFeignClient.createBatch(list) ;
        }
    }


    public void create(Imaintenance_equipment_category maintenance_equipment_category){
        Imaintenance_equipment_category clientModel = maintenance_equipment_categoryFeignClient.create((maintenance_equipment_categoryImpl)maintenance_equipment_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment_category.getClass(), false);
        copier.copy(clientModel, maintenance_equipment_category, null);
    }


    public void update(Imaintenance_equipment_category maintenance_equipment_category){
        Imaintenance_equipment_category clientModel = maintenance_equipment_categoryFeignClient.update(maintenance_equipment_category.getId(),(maintenance_equipment_categoryImpl)maintenance_equipment_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment_category.getClass(), false);
        copier.copy(clientModel, maintenance_equipment_category, null);
    }


    public void get(Imaintenance_equipment_category maintenance_equipment_category){
        Imaintenance_equipment_category clientModel = maintenance_equipment_categoryFeignClient.get(maintenance_equipment_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment_category.getClass(), false);
        copier.copy(clientModel, maintenance_equipment_category, null);
    }


    public void updateBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories){
        if(maintenance_equipment_categories!=null){
            List<maintenance_equipment_categoryImpl> list = new ArrayList<maintenance_equipment_categoryImpl>();
            for(Imaintenance_equipment_category imaintenance_equipment_category :maintenance_equipment_categories){
                list.add((maintenance_equipment_categoryImpl)imaintenance_equipment_category) ;
            }
            maintenance_equipment_categoryFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories){
        if(maintenance_equipment_categories!=null){
            List<maintenance_equipment_categoryImpl> list = new ArrayList<maintenance_equipment_categoryImpl>();
            for(Imaintenance_equipment_category imaintenance_equipment_category :maintenance_equipment_categories){
                list.add((maintenance_equipment_categoryImpl)imaintenance_equipment_category) ;
            }
            maintenance_equipment_categoryFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imaintenance_equipment_category> fetchDefault(SearchContext context){
        Page<maintenance_equipment_categoryImpl> page = this.maintenance_equipment_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imaintenance_equipment_category maintenance_equipment_category){
        maintenance_equipment_categoryFeignClient.remove(maintenance_equipment_category.getId()) ;
    }


    public Page<Imaintenance_equipment_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imaintenance_equipment_category maintenance_equipment_category){
        Imaintenance_equipment_category clientModel = maintenance_equipment_categoryFeignClient.getDraft(maintenance_equipment_category.getId(),(maintenance_equipment_categoryImpl)maintenance_equipment_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment_category.getClass(), false);
        copier.copy(clientModel, maintenance_equipment_category, null);
    }



}

