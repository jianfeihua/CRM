package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_document;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_documentClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_documentImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_documentFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_document] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_documentClientServiceImpl implements Imrp_documentClientService {

    mrp_documentFeignClient mrp_documentFeignClient;

    @Autowired
    public mrp_documentClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_documentFeignClient = nameBuilder.target(mrp_documentFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_documentFeignClient = nameBuilder.target(mrp_documentFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_document createModel() {
		return new mrp_documentImpl();
	}


    public Page<Imrp_document> fetchDefault(SearchContext context){
        Page<mrp_documentImpl> page = this.mrp_documentFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imrp_document> mrp_documents){
        if(mrp_documents!=null){
            List<mrp_documentImpl> list = new ArrayList<mrp_documentImpl>();
            for(Imrp_document imrp_document :mrp_documents){
                list.add((mrp_documentImpl)imrp_document) ;
            }
            mrp_documentFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imrp_document mrp_document){
        Imrp_document clientModel = mrp_documentFeignClient.create((mrp_documentImpl)mrp_document) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_document.getClass(), false);
        copier.copy(clientModel, mrp_document, null);
    }


    public void updateBatch(List<Imrp_document> mrp_documents){
        if(mrp_documents!=null){
            List<mrp_documentImpl> list = new ArrayList<mrp_documentImpl>();
            for(Imrp_document imrp_document :mrp_documents){
                list.add((mrp_documentImpl)imrp_document) ;
            }
            mrp_documentFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Imrp_document> mrp_documents){
        if(mrp_documents!=null){
            List<mrp_documentImpl> list = new ArrayList<mrp_documentImpl>();
            for(Imrp_document imrp_document :mrp_documents){
                list.add((mrp_documentImpl)imrp_document) ;
            }
            mrp_documentFeignClient.createBatch(list) ;
        }
    }


    public void get(Imrp_document mrp_document){
        Imrp_document clientModel = mrp_documentFeignClient.get(mrp_document.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_document.getClass(), false);
        copier.copy(clientModel, mrp_document, null);
    }


    public void remove(Imrp_document mrp_document){
        mrp_documentFeignClient.remove(mrp_document.getId()) ;
    }


    public void update(Imrp_document mrp_document){
        Imrp_document clientModel = mrp_documentFeignClient.update(mrp_document.getId(),(mrp_documentImpl)mrp_document) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_document.getClass(), false);
        copier.copy(clientModel, mrp_document, null);
    }


    public Page<Imrp_document> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_document mrp_document){
        Imrp_document clientModel = mrp_documentFeignClient.getDraft(mrp_document.getId(),(mrp_documentImpl)mrp_document) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_document.getClass(), false);
        copier.copy(clientModel, mrp_document, null);
    }



}

