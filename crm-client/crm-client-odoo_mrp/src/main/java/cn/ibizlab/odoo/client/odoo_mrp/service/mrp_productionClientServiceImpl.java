package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_production;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_productionClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_productionImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_productionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_production] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_productionClientServiceImpl implements Imrp_productionClientService {

    mrp_productionFeignClient mrp_productionFeignClient;

    @Autowired
    public mrp_productionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_productionFeignClient = nameBuilder.target(mrp_productionFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_productionFeignClient = nameBuilder.target(mrp_productionFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_production createModel() {
		return new mrp_productionImpl();
	}


    public void removeBatch(List<Imrp_production> mrp_productions){
        if(mrp_productions!=null){
            List<mrp_productionImpl> list = new ArrayList<mrp_productionImpl>();
            for(Imrp_production imrp_production :mrp_productions){
                list.add((mrp_productionImpl)imrp_production) ;
            }
            mrp_productionFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imrp_production> mrp_productions){
        if(mrp_productions!=null){
            List<mrp_productionImpl> list = new ArrayList<mrp_productionImpl>();
            for(Imrp_production imrp_production :mrp_productions){
                list.add((mrp_productionImpl)imrp_production) ;
            }
            mrp_productionFeignClient.createBatch(list) ;
        }
    }


    public void create(Imrp_production mrp_production){
        Imrp_production clientModel = mrp_productionFeignClient.create((mrp_productionImpl)mrp_production) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_production.getClass(), false);
        copier.copy(clientModel, mrp_production, null);
    }


    public Page<Imrp_production> fetchDefault(SearchContext context){
        Page<mrp_productionImpl> page = this.mrp_productionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Imrp_production> mrp_productions){
        if(mrp_productions!=null){
            List<mrp_productionImpl> list = new ArrayList<mrp_productionImpl>();
            for(Imrp_production imrp_production :mrp_productions){
                list.add((mrp_productionImpl)imrp_production) ;
            }
            mrp_productionFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imrp_production mrp_production){
        Imrp_production clientModel = mrp_productionFeignClient.get(mrp_production.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_production.getClass(), false);
        copier.copy(clientModel, mrp_production, null);
    }


    public void remove(Imrp_production mrp_production){
        mrp_productionFeignClient.remove(mrp_production.getId()) ;
    }


    public void update(Imrp_production mrp_production){
        Imrp_production clientModel = mrp_productionFeignClient.update(mrp_production.getId(),(mrp_productionImpl)mrp_production) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_production.getClass(), false);
        copier.copy(clientModel, mrp_production, null);
    }


    public Page<Imrp_production> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_production mrp_production){
        Imrp_production clientModel = mrp_productionFeignClient.getDraft(mrp_production.getId(),(mrp_productionImpl)mrp_production) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_production.getClass(), false);
        copier.copy(clientModel, mrp_production, null);
    }



}

