package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_bom;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_bomClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_bomImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_bomFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_bom] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_bomClientServiceImpl implements Imrp_bomClientService {

    mrp_bomFeignClient mrp_bomFeignClient;

    @Autowired
    public mrp_bomClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_bomFeignClient = nameBuilder.target(mrp_bomFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_bomFeignClient = nameBuilder.target(mrp_bomFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_bom createModel() {
		return new mrp_bomImpl();
	}


    public void create(Imrp_bom mrp_bom){
        Imrp_bom clientModel = mrp_bomFeignClient.create((mrp_bomImpl)mrp_bom) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom.getClass(), false);
        copier.copy(clientModel, mrp_bom, null);
    }


    public Page<Imrp_bom> fetchDefault(SearchContext context){
        Page<mrp_bomImpl> page = this.mrp_bomFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imrp_bom mrp_bom){
        mrp_bomFeignClient.remove(mrp_bom.getId()) ;
    }


    public void update(Imrp_bom mrp_bom){
        Imrp_bom clientModel = mrp_bomFeignClient.update(mrp_bom.getId(),(mrp_bomImpl)mrp_bom) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom.getClass(), false);
        copier.copy(clientModel, mrp_bom, null);
    }


    public void get(Imrp_bom mrp_bom){
        Imrp_bom clientModel = mrp_bomFeignClient.get(mrp_bom.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom.getClass(), false);
        copier.copy(clientModel, mrp_bom, null);
    }


    public void createBatch(List<Imrp_bom> mrp_boms){
        if(mrp_boms!=null){
            List<mrp_bomImpl> list = new ArrayList<mrp_bomImpl>();
            for(Imrp_bom imrp_bom :mrp_boms){
                list.add((mrp_bomImpl)imrp_bom) ;
            }
            mrp_bomFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imrp_bom> mrp_boms){
        if(mrp_boms!=null){
            List<mrp_bomImpl> list = new ArrayList<mrp_bomImpl>();
            for(Imrp_bom imrp_bom :mrp_boms){
                list.add((mrp_bomImpl)imrp_bom) ;
            }
            mrp_bomFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imrp_bom> mrp_boms){
        if(mrp_boms!=null){
            List<mrp_bomImpl> list = new ArrayList<mrp_bomImpl>();
            for(Imrp_bom imrp_bom :mrp_boms){
                list.add((mrp_bomImpl)imrp_bom) ;
            }
            mrp_bomFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imrp_bom> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_bom mrp_bom){
        Imrp_bom clientModel = mrp_bomFeignClient.getDraft(mrp_bom.getId(),(mrp_bomImpl)mrp_bom) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom.getClass(), false);
        copier.copy(clientModel, mrp_bom, null);
    }



}

