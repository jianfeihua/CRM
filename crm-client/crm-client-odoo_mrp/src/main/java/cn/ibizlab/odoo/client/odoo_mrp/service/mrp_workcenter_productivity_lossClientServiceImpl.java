package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenter_productivity_lossClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivity_lossImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_workcenter_productivity_lossFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_workcenter_productivity_lossClientServiceImpl implements Imrp_workcenter_productivity_lossClientService {

    mrp_workcenter_productivity_lossFeignClient mrp_workcenter_productivity_lossFeignClient;

    @Autowired
    public mrp_workcenter_productivity_lossClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenter_productivity_lossFeignClient = nameBuilder.target(mrp_workcenter_productivity_lossFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenter_productivity_lossFeignClient = nameBuilder.target(mrp_workcenter_productivity_lossFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_workcenter_productivity_loss createModel() {
		return new mrp_workcenter_productivity_lossImpl();
	}


    public void update(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
        Imrp_workcenter_productivity_loss clientModel = mrp_workcenter_productivity_lossFeignClient.update(mrp_workcenter_productivity_loss.getId(),(mrp_workcenter_productivity_lossImpl)mrp_workcenter_productivity_loss) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss, null);
    }


    public void create(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
        Imrp_workcenter_productivity_loss clientModel = mrp_workcenter_productivity_lossFeignClient.create((mrp_workcenter_productivity_lossImpl)mrp_workcenter_productivity_loss) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss, null);
    }


    public void remove(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
        mrp_workcenter_productivity_lossFeignClient.remove(mrp_workcenter_productivity_loss.getId()) ;
    }


    public void removeBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
        if(mrp_workcenter_productivity_losses!=null){
            List<mrp_workcenter_productivity_lossImpl> list = new ArrayList<mrp_workcenter_productivity_lossImpl>();
            for(Imrp_workcenter_productivity_loss imrp_workcenter_productivity_loss :mrp_workcenter_productivity_losses){
                list.add((mrp_workcenter_productivity_lossImpl)imrp_workcenter_productivity_loss) ;
            }
            mrp_workcenter_productivity_lossFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imrp_workcenter_productivity_loss> fetchDefault(SearchContext context){
        Page<mrp_workcenter_productivity_lossImpl> page = this.mrp_workcenter_productivity_lossFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
        if(mrp_workcenter_productivity_losses!=null){
            List<mrp_workcenter_productivity_lossImpl> list = new ArrayList<mrp_workcenter_productivity_lossImpl>();
            for(Imrp_workcenter_productivity_loss imrp_workcenter_productivity_loss :mrp_workcenter_productivity_losses){
                list.add((mrp_workcenter_productivity_lossImpl)imrp_workcenter_productivity_loss) ;
            }
            mrp_workcenter_productivity_lossFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
        if(mrp_workcenter_productivity_losses!=null){
            List<mrp_workcenter_productivity_lossImpl> list = new ArrayList<mrp_workcenter_productivity_lossImpl>();
            for(Imrp_workcenter_productivity_loss imrp_workcenter_productivity_loss :mrp_workcenter_productivity_losses){
                list.add((mrp_workcenter_productivity_lossImpl)imrp_workcenter_productivity_loss) ;
            }
            mrp_workcenter_productivity_lossFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
        Imrp_workcenter_productivity_loss clientModel = mrp_workcenter_productivity_lossFeignClient.get(mrp_workcenter_productivity_loss.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss, null);
    }


    public Page<Imrp_workcenter_productivity_loss> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
        Imrp_workcenter_productivity_loss clientModel = mrp_workcenter_productivity_lossFeignClient.getDraft(mrp_workcenter_productivity_loss.getId(),(mrp_workcenter_productivity_lossImpl)mrp_workcenter_productivity_loss) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss, null);
    }



}

