package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivityImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_workcenter_productivity] 服务对象接口
 */
public interface mrp_workcenter_productivityFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivities/updatebatch")
    public mrp_workcenter_productivityImpl updateBatch(@RequestBody List<mrp_workcenter_productivityImpl> mrp_workcenter_productivities);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivities/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivities/{id}")
    public mrp_workcenter_productivityImpl update(@PathVariable("id") Integer id,@RequestBody mrp_workcenter_productivityImpl mrp_workcenter_productivity);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivities/fetchdefault")
    public Page<mrp_workcenter_productivityImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivities")
    public mrp_workcenter_productivityImpl create(@RequestBody mrp_workcenter_productivityImpl mrp_workcenter_productivity);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivities/createbatch")
    public mrp_workcenter_productivityImpl createBatch(@RequestBody List<mrp_workcenter_productivityImpl> mrp_workcenter_productivities);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivities/removebatch")
    public mrp_workcenter_productivityImpl removeBatch(@RequestBody List<mrp_workcenter_productivityImpl> mrp_workcenter_productivities);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivities/{id}")
    public mrp_workcenter_productivityImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivities/select")
    public Page<mrp_workcenter_productivityImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivities/{id}/getdraft")
    public mrp_workcenter_productivityImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_workcenter_productivityImpl mrp_workcenter_productivity);



}
