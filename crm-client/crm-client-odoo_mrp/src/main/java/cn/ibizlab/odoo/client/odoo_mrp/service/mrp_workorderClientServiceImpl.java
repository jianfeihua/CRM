package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workorder;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_workorderClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workorderImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_workorderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_workorder] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_workorderClientServiceImpl implements Imrp_workorderClientService {

    mrp_workorderFeignClient mrp_workorderFeignClient;

    @Autowired
    public mrp_workorderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workorderFeignClient = nameBuilder.target(mrp_workorderFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workorderFeignClient = nameBuilder.target(mrp_workorderFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_workorder createModel() {
		return new mrp_workorderImpl();
	}


    public void updateBatch(List<Imrp_workorder> mrp_workorders){
        if(mrp_workorders!=null){
            List<mrp_workorderImpl> list = new ArrayList<mrp_workorderImpl>();
            for(Imrp_workorder imrp_workorder :mrp_workorders){
                list.add((mrp_workorderImpl)imrp_workorder) ;
            }
            mrp_workorderFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imrp_workorder> mrp_workorders){
        if(mrp_workorders!=null){
            List<mrp_workorderImpl> list = new ArrayList<mrp_workorderImpl>();
            for(Imrp_workorder imrp_workorder :mrp_workorders){
                list.add((mrp_workorderImpl)imrp_workorder) ;
            }
            mrp_workorderFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imrp_workorder> fetchDefault(SearchContext context){
        Page<mrp_workorderImpl> page = this.mrp_workorderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Imrp_workorder mrp_workorder){
        Imrp_workorder clientModel = mrp_workorderFeignClient.create((mrp_workorderImpl)mrp_workorder) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workorder.getClass(), false);
        copier.copy(clientModel, mrp_workorder, null);
    }


    public void createBatch(List<Imrp_workorder> mrp_workorders){
        if(mrp_workorders!=null){
            List<mrp_workorderImpl> list = new ArrayList<mrp_workorderImpl>();
            for(Imrp_workorder imrp_workorder :mrp_workorders){
                list.add((mrp_workorderImpl)imrp_workorder) ;
            }
            mrp_workorderFeignClient.createBatch(list) ;
        }
    }


    public void remove(Imrp_workorder mrp_workorder){
        mrp_workorderFeignClient.remove(mrp_workorder.getId()) ;
    }


    public void update(Imrp_workorder mrp_workorder){
        Imrp_workorder clientModel = mrp_workorderFeignClient.update(mrp_workorder.getId(),(mrp_workorderImpl)mrp_workorder) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workorder.getClass(), false);
        copier.copy(clientModel, mrp_workorder, null);
    }


    public void get(Imrp_workorder mrp_workorder){
        Imrp_workorder clientModel = mrp_workorderFeignClient.get(mrp_workorder.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workorder.getClass(), false);
        copier.copy(clientModel, mrp_workorder, null);
    }


    public Page<Imrp_workorder> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_workorder mrp_workorder){
        Imrp_workorder clientModel = mrp_workorderFeignClient.getDraft(mrp_workorder.getId(),(mrp_workorderImpl)mrp_workorder) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workorder.getClass(), false);
        copier.copy(clientModel, mrp_workorder, null);
    }



}

