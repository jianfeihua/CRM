package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenter_productivity_loss_typeClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivity_loss_typeImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_workcenter_productivity_loss_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_workcenter_productivity_loss_typeClientServiceImpl implements Imrp_workcenter_productivity_loss_typeClientService {

    mrp_workcenter_productivity_loss_typeFeignClient mrp_workcenter_productivity_loss_typeFeignClient;

    @Autowired
    public mrp_workcenter_productivity_loss_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenter_productivity_loss_typeFeignClient = nameBuilder.target(mrp_workcenter_productivity_loss_typeFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenter_productivity_loss_typeFeignClient = nameBuilder.target(mrp_workcenter_productivity_loss_typeFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_workcenter_productivity_loss_type createModel() {
		return new mrp_workcenter_productivity_loss_typeImpl();
	}


    public void createBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
        if(mrp_workcenter_productivity_loss_types!=null){
            List<mrp_workcenter_productivity_loss_typeImpl> list = new ArrayList<mrp_workcenter_productivity_loss_typeImpl>();
            for(Imrp_workcenter_productivity_loss_type imrp_workcenter_productivity_loss_type :mrp_workcenter_productivity_loss_types){
                list.add((mrp_workcenter_productivity_loss_typeImpl)imrp_workcenter_productivity_loss_type) ;
            }
            mrp_workcenter_productivity_loss_typeFeignClient.createBatch(list) ;
        }
    }


    public void create(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
        Imrp_workcenter_productivity_loss_type clientModel = mrp_workcenter_productivity_loss_typeFeignClient.create((mrp_workcenter_productivity_loss_typeImpl)mrp_workcenter_productivity_loss_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss_type.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss_type, null);
    }


    public void removeBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
        if(mrp_workcenter_productivity_loss_types!=null){
            List<mrp_workcenter_productivity_loss_typeImpl> list = new ArrayList<mrp_workcenter_productivity_loss_typeImpl>();
            for(Imrp_workcenter_productivity_loss_type imrp_workcenter_productivity_loss_type :mrp_workcenter_productivity_loss_types){
                list.add((mrp_workcenter_productivity_loss_typeImpl)imrp_workcenter_productivity_loss_type) ;
            }
            mrp_workcenter_productivity_loss_typeFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
        if(mrp_workcenter_productivity_loss_types!=null){
            List<mrp_workcenter_productivity_loss_typeImpl> list = new ArrayList<mrp_workcenter_productivity_loss_typeImpl>();
            for(Imrp_workcenter_productivity_loss_type imrp_workcenter_productivity_loss_type :mrp_workcenter_productivity_loss_types){
                list.add((mrp_workcenter_productivity_loss_typeImpl)imrp_workcenter_productivity_loss_type) ;
            }
            mrp_workcenter_productivity_loss_typeFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
        Imrp_workcenter_productivity_loss_type clientModel = mrp_workcenter_productivity_loss_typeFeignClient.get(mrp_workcenter_productivity_loss_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss_type.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss_type, null);
    }


    public void update(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
        Imrp_workcenter_productivity_loss_type clientModel = mrp_workcenter_productivity_loss_typeFeignClient.update(mrp_workcenter_productivity_loss_type.getId(),(mrp_workcenter_productivity_loss_typeImpl)mrp_workcenter_productivity_loss_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss_type.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss_type, null);
    }


    public Page<Imrp_workcenter_productivity_loss_type> fetchDefault(SearchContext context){
        Page<mrp_workcenter_productivity_loss_typeImpl> page = this.mrp_workcenter_productivity_loss_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
        mrp_workcenter_productivity_loss_typeFeignClient.remove(mrp_workcenter_productivity_loss_type.getId()) ;
    }


    public Page<Imrp_workcenter_productivity_loss_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
        Imrp_workcenter_productivity_loss_type clientModel = mrp_workcenter_productivity_loss_typeFeignClient.getDraft(mrp_workcenter_productivity_loss_type.getId(),(mrp_workcenter_productivity_loss_typeImpl)mrp_workcenter_productivity_loss_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity_loss_type.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity_loss_type, null);
    }



}

