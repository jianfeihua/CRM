package cn.ibizlab.odoo.client.odoo_rating.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Irating_rating;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[rating_rating] 对象
 */
public class rating_ratingImpl implements Irating_rating,Serializable{

    /**
     * 安全令牌
     */
    public String access_token;

    @JsonIgnore
    public boolean access_tokenDirtyFlag;
    
    /**
     * 已填写的评级
     */
    public String consumed;

    @JsonIgnore
    public boolean consumedDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 备注
     */
    public String feedback;

    @JsonIgnore
    public boolean feedbackDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 链接信息
     */
    public Integer message_id;

    @JsonIgnore
    public boolean message_idDirtyFlag;
    
    /**
     * 父级文档
     */
    public Integer parent_res_id;

    @JsonIgnore
    public boolean parent_res_idDirtyFlag;
    
    /**
     * 父级文档模型
     */
    public String parent_res_model;

    @JsonIgnore
    public boolean parent_res_modelDirtyFlag;
    
    /**
     * 父级相关文档模型
     */
    public Integer parent_res_model_id;

    @JsonIgnore
    public boolean parent_res_model_idDirtyFlag;
    
    /**
     * 父级文档名称
     */
    public String parent_res_name;

    @JsonIgnore
    public boolean parent_res_nameDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 评级人员
     */
    public Integer rated_partner_id;

    @JsonIgnore
    public boolean rated_partner_idDirtyFlag;
    
    /**
     * 评级人员
     */
    public String rated_partner_id_text;

    @JsonIgnore
    public boolean rated_partner_id_textDirtyFlag;
    
    /**
     * 评级数值
     */
    public Double rating;

    @JsonIgnore
    public boolean ratingDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] rating_image;

    @JsonIgnore
    public boolean rating_imageDirtyFlag;
    
    /**
     * 评级
     */
    public String rating_text;

    @JsonIgnore
    public boolean rating_textDirtyFlag;
    
    /**
     * 文档
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 文档模型
     */
    public String res_model;

    @JsonIgnore
    public boolean res_modelDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    public Integer res_model_id;

    @JsonIgnore
    public boolean res_model_idDirtyFlag;
    
    /**
     * 资源名称
     */
    public String res_name;

    @JsonIgnore
    public boolean res_nameDirtyFlag;
    
    /**
     * 已发布
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }   

    /**
     * 获取 [已填写的评级]
     */
    @JsonProperty("consumed")
    public String getConsumed(){
        return this.consumed ;
    }

    /**
     * 设置 [已填写的评级]
     */
    @JsonProperty("consumed")
    public void setConsumed(String  consumed){
        this.consumed = consumed ;
        this.consumedDirtyFlag = true ;
    }

     /**
     * 获取 [已填写的评级]脏标记
     */
    @JsonIgnore
    public boolean getConsumedDirtyFlag(){
        return this.consumedDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("feedback")
    public String getFeedback(){
        return this.feedback ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("feedback")
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.feedbackDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getFeedbackDirtyFlag(){
        return this.feedbackDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [链接信息]
     */
    @JsonProperty("message_id")
    public Integer getMessage_id(){
        return this.message_id ;
    }

    /**
     * 设置 [链接信息]
     */
    @JsonProperty("message_id")
    public void setMessage_id(Integer  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

     /**
     * 获取 [链接信息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return this.message_idDirtyFlag ;
    }   

    /**
     * 获取 [父级文档]
     */
    @JsonProperty("parent_res_id")
    public Integer getParent_res_id(){
        return this.parent_res_id ;
    }

    /**
     * 设置 [父级文档]
     */
    @JsonProperty("parent_res_id")
    public void setParent_res_id(Integer  parent_res_id){
        this.parent_res_id = parent_res_id ;
        this.parent_res_idDirtyFlag = true ;
    }

     /**
     * 获取 [父级文档]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_idDirtyFlag(){
        return this.parent_res_idDirtyFlag ;
    }   

    /**
     * 获取 [父级文档模型]
     */
    @JsonProperty("parent_res_model")
    public String getParent_res_model(){
        return this.parent_res_model ;
    }

    /**
     * 设置 [父级文档模型]
     */
    @JsonProperty("parent_res_model")
    public void setParent_res_model(String  parent_res_model){
        this.parent_res_model = parent_res_model ;
        this.parent_res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [父级文档模型]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_modelDirtyFlag(){
        return this.parent_res_modelDirtyFlag ;
    }   

    /**
     * 获取 [父级相关文档模型]
     */
    @JsonProperty("parent_res_model_id")
    public Integer getParent_res_model_id(){
        return this.parent_res_model_id ;
    }

    /**
     * 设置 [父级相关文档模型]
     */
    @JsonProperty("parent_res_model_id")
    public void setParent_res_model_id(Integer  parent_res_model_id){
        this.parent_res_model_id = parent_res_model_id ;
        this.parent_res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [父级相关文档模型]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_model_idDirtyFlag(){
        return this.parent_res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [父级文档名称]
     */
    @JsonProperty("parent_res_name")
    public String getParent_res_name(){
        return this.parent_res_name ;
    }

    /**
     * 设置 [父级文档名称]
     */
    @JsonProperty("parent_res_name")
    public void setParent_res_name(String  parent_res_name){
        this.parent_res_name = parent_res_name ;
        this.parent_res_nameDirtyFlag = true ;
    }

     /**
     * 获取 [父级文档名称]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_nameDirtyFlag(){
        return this.parent_res_nameDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [评级人员]
     */
    @JsonProperty("rated_partner_id")
    public Integer getRated_partner_id(){
        return this.rated_partner_id ;
    }

    /**
     * 设置 [评级人员]
     */
    @JsonProperty("rated_partner_id")
    public void setRated_partner_id(Integer  rated_partner_id){
        this.rated_partner_id = rated_partner_id ;
        this.rated_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [评级人员]脏标记
     */
    @JsonIgnore
    public boolean getRated_partner_idDirtyFlag(){
        return this.rated_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [评级人员]
     */
    @JsonProperty("rated_partner_id_text")
    public String getRated_partner_id_text(){
        return this.rated_partner_id_text ;
    }

    /**
     * 设置 [评级人员]
     */
    @JsonProperty("rated_partner_id_text")
    public void setRated_partner_id_text(String  rated_partner_id_text){
        this.rated_partner_id_text = rated_partner_id_text ;
        this.rated_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [评级人员]脏标记
     */
    @JsonIgnore
    public boolean getRated_partner_id_textDirtyFlag(){
        return this.rated_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [评级数值]
     */
    @JsonProperty("rating")
    public Double getRating(){
        return this.rating ;
    }

    /**
     * 设置 [评级数值]
     */
    @JsonProperty("rating")
    public void setRating(Double  rating){
        this.rating = rating ;
        this.ratingDirtyFlag = true ;
    }

     /**
     * 获取 [评级数值]脏标记
     */
    @JsonIgnore
    public boolean getRatingDirtyFlag(){
        return this.ratingDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("rating_image")
    public byte[] getRating_image(){
        return this.rating_image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("rating_image")
    public void setRating_image(byte[]  rating_image){
        this.rating_image = rating_image ;
        this.rating_imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getRating_imageDirtyFlag(){
        return this.rating_imageDirtyFlag ;
    }   

    /**
     * 获取 [评级]
     */
    @JsonProperty("rating_text")
    public String getRating_text(){
        return this.rating_text ;
    }

    /**
     * 设置 [评级]
     */
    @JsonProperty("rating_text")
    public void setRating_text(String  rating_text){
        this.rating_text = rating_text ;
        this.rating_textDirtyFlag = true ;
    }

     /**
     * 获取 [评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_textDirtyFlag(){
        return this.rating_textDirtyFlag ;
    }   

    /**
     * 获取 [文档]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [文档]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [文档模型]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [文档模型]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }   

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [资源名称]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return this.res_name ;
    }

    /**
     * 设置 [资源名称]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

     /**
     * 获取 [资源名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return this.res_nameDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
