package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_event;
import cn.ibizlab.odoo.client.odoo_calendar.config.odoo_calendarClientProperties;
import cn.ibizlab.odoo.core.client.service.Icalendar_eventClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_eventImpl;
import cn.ibizlab.odoo.client.odoo_calendar.feign.calendar_eventFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[calendar_event] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class calendar_eventClientServiceImpl implements Icalendar_eventClientService {

    calendar_eventFeignClient calendar_eventFeignClient;

    @Autowired
    public calendar_eventClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_calendarClientProperties odoo_calendarClientProperties) {
        if (odoo_calendarClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_eventFeignClient = nameBuilder.target(calendar_eventFeignClient.class,"http://"+odoo_calendarClientProperties.getServiceId()+"/") ;
		}else if (odoo_calendarClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_eventFeignClient = nameBuilder.target(calendar_eventFeignClient.class,odoo_calendarClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icalendar_event createModel() {
		return new calendar_eventImpl();
	}


    public void get(Icalendar_event calendar_event){
        Icalendar_event clientModel = calendar_eventFeignClient.get(calendar_event.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event.getClass(), false);
        copier.copy(clientModel, calendar_event, null);
    }


    public Page<Icalendar_event> fetchDefault(SearchContext context){
        Page<calendar_eventImpl> page = this.calendar_eventFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Icalendar_event calendar_event){
        Icalendar_event clientModel = calendar_eventFeignClient.update(calendar_event.getId(),(calendar_eventImpl)calendar_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event.getClass(), false);
        copier.copy(clientModel, calendar_event, null);
    }


    public void createBatch(List<Icalendar_event> calendar_events){
        if(calendar_events!=null){
            List<calendar_eventImpl> list = new ArrayList<calendar_eventImpl>();
            for(Icalendar_event icalendar_event :calendar_events){
                list.add((calendar_eventImpl)icalendar_event) ;
            }
            calendar_eventFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Icalendar_event> calendar_events){
        if(calendar_events!=null){
            List<calendar_eventImpl> list = new ArrayList<calendar_eventImpl>();
            for(Icalendar_event icalendar_event :calendar_events){
                list.add((calendar_eventImpl)icalendar_event) ;
            }
            calendar_eventFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Icalendar_event> calendar_events){
        if(calendar_events!=null){
            List<calendar_eventImpl> list = new ArrayList<calendar_eventImpl>();
            for(Icalendar_event icalendar_event :calendar_events){
                list.add((calendar_eventImpl)icalendar_event) ;
            }
            calendar_eventFeignClient.removeBatch(list) ;
        }
    }


    public void create(Icalendar_event calendar_event){
        Icalendar_event clientModel = calendar_eventFeignClient.create((calendar_eventImpl)calendar_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event.getClass(), false);
        copier.copy(clientModel, calendar_event, null);
    }


    public void remove(Icalendar_event calendar_event){
        calendar_eventFeignClient.remove(calendar_event.getId()) ;
    }


    public Page<Icalendar_event> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icalendar_event calendar_event){
        Icalendar_event clientModel = calendar_eventFeignClient.getDraft(calendar_event.getId(),(calendar_eventImpl)calendar_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event.getClass(), false);
        copier.copy(clientModel, calendar_event, null);
    }


    public void save(Icalendar_event calendar_event){
        Icalendar_event clientModel = calendar_eventFeignClient.save(calendar_event.getId(),(calendar_eventImpl)calendar_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event.getClass(), false);
        copier.copy(clientModel, calendar_event, null);
    }


    public void checkKey(Icalendar_event calendar_event){
        Icalendar_event clientModel = calendar_eventFeignClient.checkKey(calendar_event.getId(),(calendar_eventImpl)calendar_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event.getClass(), false);
        copier.copy(clientModel, calendar_event, null);
    }



}

