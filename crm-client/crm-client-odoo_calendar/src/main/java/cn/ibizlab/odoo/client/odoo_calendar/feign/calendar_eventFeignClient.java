package cn.ibizlab.odoo.client.odoo_calendar.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icalendar_event;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_eventImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[calendar_event] 服务对象接口
 */
public interface calendar_eventFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_events/{id}")
    public calendar_eventImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_events/fetchdefault")
    public Page<calendar_eventImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_events/{id}")
    public calendar_eventImpl update(@PathVariable("id") Integer id,@RequestBody calendar_eventImpl calendar_event);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_events/createbatch")
    public calendar_eventImpl createBatch(@RequestBody List<calendar_eventImpl> calendar_events);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_events/updatebatch")
    public calendar_eventImpl updateBatch(@RequestBody List<calendar_eventImpl> calendar_events);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_events/removebatch")
    public calendar_eventImpl removeBatch(@RequestBody List<calendar_eventImpl> calendar_events);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_events")
    public calendar_eventImpl create(@RequestBody calendar_eventImpl calendar_event);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_events/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_events/select")
    public Page<calendar_eventImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_events/{id}/getdraft")
    public calendar_eventImpl getDraft(@PathVariable("id") Integer id,@RequestBody calendar_eventImpl calendar_event);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_events/{id}/save")
    public calendar_eventImpl save(@PathVariable("id") Integer id,@RequestBody calendar_eventImpl calendar_event);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_events/{id}/checkkey")
    public calendar_eventImpl checkKey(@PathVariable("id") Integer id,@RequestBody calendar_eventImpl calendar_event);



}
