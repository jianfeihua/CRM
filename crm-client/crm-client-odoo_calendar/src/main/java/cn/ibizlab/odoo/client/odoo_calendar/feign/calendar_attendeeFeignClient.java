package cn.ibizlab.odoo.client.odoo_calendar.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icalendar_attendee;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_attendeeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[calendar_attendee] 服务对象接口
 */
public interface calendar_attendeeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_attendees/{id}")
    public calendar_attendeeImpl update(@PathVariable("id") Integer id,@RequestBody calendar_attendeeImpl calendar_attendee);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_attendees/createbatch")
    public calendar_attendeeImpl createBatch(@RequestBody List<calendar_attendeeImpl> calendar_attendees);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_attendees")
    public calendar_attendeeImpl create(@RequestBody calendar_attendeeImpl calendar_attendee);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_attendees/removebatch")
    public calendar_attendeeImpl removeBatch(@RequestBody List<calendar_attendeeImpl> calendar_attendees);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_attendees/{id}")
    public calendar_attendeeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_attendees/fetchdefault")
    public Page<calendar_attendeeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_attendees/updatebatch")
    public calendar_attendeeImpl updateBatch(@RequestBody List<calendar_attendeeImpl> calendar_attendees);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_attendees/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_attendees/select")
    public Page<calendar_attendeeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_attendees/{id}/getdraft")
    public calendar_attendeeImpl getDraft(@PathVariable("id") Integer id,@RequestBody calendar_attendeeImpl calendar_attendee);



}
