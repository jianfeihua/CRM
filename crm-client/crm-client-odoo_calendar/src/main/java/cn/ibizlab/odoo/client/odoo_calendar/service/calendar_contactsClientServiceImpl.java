package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_contacts;
import cn.ibizlab.odoo.client.odoo_calendar.config.odoo_calendarClientProperties;
import cn.ibizlab.odoo.core.client.service.Icalendar_contactsClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_contactsImpl;
import cn.ibizlab.odoo.client.odoo_calendar.feign.calendar_contactsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[calendar_contacts] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class calendar_contactsClientServiceImpl implements Icalendar_contactsClientService {

    calendar_contactsFeignClient calendar_contactsFeignClient;

    @Autowired
    public calendar_contactsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_calendarClientProperties odoo_calendarClientProperties) {
        if (odoo_calendarClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_contactsFeignClient = nameBuilder.target(calendar_contactsFeignClient.class,"http://"+odoo_calendarClientProperties.getServiceId()+"/") ;
		}else if (odoo_calendarClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_contactsFeignClient = nameBuilder.target(calendar_contactsFeignClient.class,odoo_calendarClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icalendar_contacts createModel() {
		return new calendar_contactsImpl();
	}


    public void update(Icalendar_contacts calendar_contacts){
        Icalendar_contacts clientModel = calendar_contactsFeignClient.update(calendar_contacts.getId(),(calendar_contactsImpl)calendar_contacts) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_contacts.getClass(), false);
        copier.copy(clientModel, calendar_contacts, null);
    }


    public void create(Icalendar_contacts calendar_contacts){
        Icalendar_contacts clientModel = calendar_contactsFeignClient.create((calendar_contactsImpl)calendar_contacts) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_contacts.getClass(), false);
        copier.copy(clientModel, calendar_contacts, null);
    }


    public Page<Icalendar_contacts> fetchDefault(SearchContext context){
        Page<calendar_contactsImpl> page = this.calendar_contactsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Icalendar_contacts calendar_contacts){
        calendar_contactsFeignClient.remove(calendar_contacts.getId()) ;
    }


    public void get(Icalendar_contacts calendar_contacts){
        Icalendar_contacts clientModel = calendar_contactsFeignClient.get(calendar_contacts.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_contacts.getClass(), false);
        copier.copy(clientModel, calendar_contacts, null);
    }


    public void createBatch(List<Icalendar_contacts> calendar_contacts){
        if(calendar_contacts!=null){
            List<calendar_contactsImpl> list = new ArrayList<calendar_contactsImpl>();
            for(Icalendar_contacts icalendar_contacts :calendar_contacts){
                list.add((calendar_contactsImpl)icalendar_contacts) ;
            }
            calendar_contactsFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Icalendar_contacts> calendar_contacts){
        if(calendar_contacts!=null){
            List<calendar_contactsImpl> list = new ArrayList<calendar_contactsImpl>();
            for(Icalendar_contacts icalendar_contacts :calendar_contacts){
                list.add((calendar_contactsImpl)icalendar_contacts) ;
            }
            calendar_contactsFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Icalendar_contacts> calendar_contacts){
        if(calendar_contacts!=null){
            List<calendar_contactsImpl> list = new ArrayList<calendar_contactsImpl>();
            for(Icalendar_contacts icalendar_contacts :calendar_contacts){
                list.add((calendar_contactsImpl)icalendar_contacts) ;
            }
            calendar_contactsFeignClient.removeBatch(list) ;
        }
    }


    public Page<Icalendar_contacts> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icalendar_contacts calendar_contacts){
        Icalendar_contacts clientModel = calendar_contactsFeignClient.getDraft(calendar_contacts.getId(),(calendar_contactsImpl)calendar_contacts) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_contacts.getClass(), false);
        copier.copy(clientModel, calendar_contacts, null);
    }



}

