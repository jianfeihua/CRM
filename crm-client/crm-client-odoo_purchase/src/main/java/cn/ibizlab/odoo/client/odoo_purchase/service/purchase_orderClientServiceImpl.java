package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order;
import cn.ibizlab.odoo.client.odoo_purchase.config.odoo_purchaseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipurchase_orderClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_orderImpl;
import cn.ibizlab.odoo.client.odoo_purchase.feign.purchase_orderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[purchase_order] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class purchase_orderClientServiceImpl implements Ipurchase_orderClientService {

    purchase_orderFeignClient purchase_orderFeignClient;

    @Autowired
    public purchase_orderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_purchaseClientProperties odoo_purchaseClientProperties) {
        if (odoo_purchaseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_orderFeignClient = nameBuilder.target(purchase_orderFeignClient.class,"http://"+odoo_purchaseClientProperties.getServiceId()+"/") ;
		}else if (odoo_purchaseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_orderFeignClient = nameBuilder.target(purchase_orderFeignClient.class,odoo_purchaseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipurchase_order createModel() {
		return new purchase_orderImpl();
	}


    public void remove(Ipurchase_order purchase_order){
        purchase_orderFeignClient.remove(purchase_order.getId()) ;
    }


    public Page<Ipurchase_order> fetchDefault(SearchContext context){
        Page<purchase_orderImpl> page = this.purchase_orderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ipurchase_order purchase_order){
        Ipurchase_order clientModel = purchase_orderFeignClient.update(purchase_order.getId(),(purchase_orderImpl)purchase_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order.getClass(), false);
        copier.copy(clientModel, purchase_order, null);
    }


    public void removeBatch(List<Ipurchase_order> purchase_orders){
        if(purchase_orders!=null){
            List<purchase_orderImpl> list = new ArrayList<purchase_orderImpl>();
            for(Ipurchase_order ipurchase_order :purchase_orders){
                list.add((purchase_orderImpl)ipurchase_order) ;
            }
            purchase_orderFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ipurchase_order> purchase_orders){
        if(purchase_orders!=null){
            List<purchase_orderImpl> list = new ArrayList<purchase_orderImpl>();
            for(Ipurchase_order ipurchase_order :purchase_orders){
                list.add((purchase_orderImpl)ipurchase_order) ;
            }
            purchase_orderFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ipurchase_order> purchase_orders){
        if(purchase_orders!=null){
            List<purchase_orderImpl> list = new ArrayList<purchase_orderImpl>();
            for(Ipurchase_order ipurchase_order :purchase_orders){
                list.add((purchase_orderImpl)ipurchase_order) ;
            }
            purchase_orderFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ipurchase_order purchase_order){
        Ipurchase_order clientModel = purchase_orderFeignClient.get(purchase_order.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order.getClass(), false);
        copier.copy(clientModel, purchase_order, null);
    }


    public void create(Ipurchase_order purchase_order){
        Ipurchase_order clientModel = purchase_orderFeignClient.create((purchase_orderImpl)purchase_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order.getClass(), false);
        copier.copy(clientModel, purchase_order, null);
    }


    public Page<Ipurchase_order> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipurchase_order purchase_order){
        Ipurchase_order clientModel = purchase_orderFeignClient.getDraft(purchase_order.getId(),(purchase_orderImpl)purchase_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order.getClass(), false);
        copier.copy(clientModel, purchase_order, null);
    }



}

