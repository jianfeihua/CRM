package cn.ibizlab.odoo.client.odoo_purchase.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ipurchase_bill_union;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[purchase_bill_union] 对象
 */
public class purchase_bill_unionImpl implements Ipurchase_bill_union,Serializable{

    /**
     * 金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 参考
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 供应商
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 采购订单
     */
    public Integer purchase_order_id;

    @JsonIgnore
    public boolean purchase_order_idDirtyFlag;
    
    /**
     * 采购订单
     */
    public String purchase_order_id_text;

    @JsonIgnore
    public boolean purchase_order_id_textDirtyFlag;
    
    /**
     * 来源
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 供应商账单
     */
    public Integer vendor_bill_id;

    @JsonIgnore
    public boolean vendor_bill_idDirtyFlag;
    
    /**
     * 供应商账单
     */
    public String vendor_bill_id_text;

    @JsonIgnore
    public boolean vendor_bill_id_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [采购订单]
     */
    @JsonProperty("purchase_order_id")
    public Integer getPurchase_order_id(){
        return this.purchase_order_id ;
    }

    /**
     * 设置 [采购订单]
     */
    @JsonProperty("purchase_order_id")
    public void setPurchase_order_id(Integer  purchase_order_id){
        this.purchase_order_id = purchase_order_id ;
        this.purchase_order_idDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_idDirtyFlag(){
        return this.purchase_order_idDirtyFlag ;
    }   

    /**
     * 获取 [采购订单]
     */
    @JsonProperty("purchase_order_id_text")
    public String getPurchase_order_id_text(){
        return this.purchase_order_id_text ;
    }

    /**
     * 设置 [采购订单]
     */
    @JsonProperty("purchase_order_id_text")
    public void setPurchase_order_id_text(String  purchase_order_id_text){
        this.purchase_order_id_text = purchase_order_id_text ;
        this.purchase_order_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_id_textDirtyFlag(){
        return this.purchase_order_id_textDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [供应商账单]
     */
    @JsonProperty("vendor_bill_id")
    public Integer getVendor_bill_id(){
        return this.vendor_bill_id ;
    }

    /**
     * 设置 [供应商账单]
     */
    @JsonProperty("vendor_bill_id")
    public void setVendor_bill_id(Integer  vendor_bill_id){
        this.vendor_bill_id = vendor_bill_id ;
        this.vendor_bill_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_idDirtyFlag(){
        return this.vendor_bill_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商账单]
     */
    @JsonProperty("vendor_bill_id_text")
    public String getVendor_bill_id_text(){
        return this.vendor_bill_id_text ;
    }

    /**
     * 设置 [供应商账单]
     */
    @JsonProperty("vendor_bill_id_text")
    public void setVendor_bill_id_text(String  vendor_bill_id_text){
        this.vendor_bill_id_text = vendor_bill_id_text ;
        this.vendor_bill_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_id_textDirtyFlag(){
        return this.vendor_bill_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
