package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_contract;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_contractImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_contract] 服务对象接口
 */
public interface hr_contractFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contracts/fetchdefault")
    public Page<hr_contractImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_contracts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contracts/{id}")
    public hr_contractImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_contracts/createbatch")
    public hr_contractImpl createBatch(@RequestBody List<hr_contractImpl> hr_contracts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_contracts/updatebatch")
    public hr_contractImpl updateBatch(@RequestBody List<hr_contractImpl> hr_contracts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_contracts")
    public hr_contractImpl create(@RequestBody hr_contractImpl hr_contract);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_contracts/{id}")
    public hr_contractImpl update(@PathVariable("id") Integer id,@RequestBody hr_contractImpl hr_contract);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_contracts/removebatch")
    public hr_contractImpl removeBatch(@RequestBody List<hr_contractImpl> hr_contracts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contracts/select")
    public Page<hr_contractImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contracts/{id}/getdraft")
    public hr_contractImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_contractImpl hr_contract);



}
