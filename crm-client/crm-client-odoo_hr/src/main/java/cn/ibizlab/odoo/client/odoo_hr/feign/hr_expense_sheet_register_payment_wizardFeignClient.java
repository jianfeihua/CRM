package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expense_sheet_register_payment_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
public interface hr_expense_sheet_register_payment_wizardFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/updatebatch")
    public hr_expense_sheet_register_payment_wizardImpl updateBatch(@RequestBody List<hr_expense_sheet_register_payment_wizardImpl> hr_expense_sheet_register_payment_wizards);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/{id}")
    public hr_expense_sheet_register_payment_wizardImpl update(@PathVariable("id") Integer id,@RequestBody hr_expense_sheet_register_payment_wizardImpl hr_expense_sheet_register_payment_wizard);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/removebatch")
    public hr_expense_sheet_register_payment_wizardImpl removeBatch(@RequestBody List<hr_expense_sheet_register_payment_wizardImpl> hr_expense_sheet_register_payment_wizards);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards")
    public hr_expense_sheet_register_payment_wizardImpl create(@RequestBody hr_expense_sheet_register_payment_wizardImpl hr_expense_sheet_register_payment_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/fetchdefault")
    public Page<hr_expense_sheet_register_payment_wizardImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/createbatch")
    public hr_expense_sheet_register_payment_wizardImpl createBatch(@RequestBody List<hr_expense_sheet_register_payment_wizardImpl> hr_expense_sheet_register_payment_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/{id}")
    public hr_expense_sheet_register_payment_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/select")
    public Page<hr_expense_sheet_register_payment_wizardImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheet_register_payment_wizards/{id}/getdraft")
    public hr_expense_sheet_register_payment_wizardImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_expense_sheet_register_payment_wizardImpl hr_expense_sheet_register_payment_wizard);



}
