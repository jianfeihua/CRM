package cn.ibizlab.odoo.client.odoo_hr.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ihr_employee;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[hr_employee] 对象
 */
public class hr_employeeImpl implements Ihr_employee,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 附加说明
     */
    public String additional_note;

    @JsonIgnore
    public boolean additional_noteDirtyFlag;
    
    /**
     * 家庭住址
     */
    public Integer address_home_id;

    @JsonIgnore
    public boolean address_home_idDirtyFlag;
    
    /**
     * 家庭住址
     */
    public String address_home_id_text;

    @JsonIgnore
    public boolean address_home_id_textDirtyFlag;
    
    /**
     * 工作地址
     */
    public Integer address_id;

    @JsonIgnore
    public boolean address_idDirtyFlag;
    
    /**
     * 工作地址
     */
    public String address_id_text;

    @JsonIgnore
    public boolean address_id_textDirtyFlag;
    
    /**
     * 出勤
     */
    public String attendance_ids;

    @JsonIgnore
    public boolean attendance_idsDirtyFlag;
    
    /**
     * 出勤状态
     */
    public String attendance_state;

    @JsonIgnore
    public boolean attendance_stateDirtyFlag;
    
    /**
     * 员工徽章
     */
    public String badge_ids;

    @JsonIgnore
    public boolean badge_idsDirtyFlag;
    
    /**
     * 银行账户号码
     */
    public Integer bank_account_id;

    @JsonIgnore
    public boolean bank_account_idDirtyFlag;
    
    /**
     * 工牌 ID
     */
    public String barcode;

    @JsonIgnore
    public boolean barcodeDirtyFlag;
    
    /**
     * 出生日期
     */
    public Timestamp birthday;

    @JsonIgnore
    public boolean birthdayDirtyFlag;
    
    /**
     * 标签
     */
    public String category_ids;

    @JsonIgnore
    public boolean category_idsDirtyFlag;
    
    /**
     * 证书等级
     */
    public String certificate;

    @JsonIgnore
    public boolean certificateDirtyFlag;
    
    /**
     * 子女数目
     */
    public Integer children;

    @JsonIgnore
    public boolean childrenDirtyFlag;
    
    /**
     * 下属
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * 教练
     */
    public Integer coach_id;

    @JsonIgnore
    public boolean coach_idDirtyFlag;
    
    /**
     * 教练
     */
    public String coach_id_text;

    @JsonIgnore
    public boolean coach_id_textDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 合同统计
     */
    public Integer contracts_count;

    @JsonIgnore
    public boolean contracts_countDirtyFlag;
    
    /**
     * 当前合同
     */
    public Integer contract_id;

    @JsonIgnore
    public boolean contract_idDirtyFlag;
    
    /**
     * 员工合同
     */
    public String contract_ids;

    @JsonIgnore
    public boolean contract_idsDirtyFlag;
    
    /**
     * 国籍(国家)
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国籍(国家)
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 国籍
     */
    public Integer country_of_birth;

    @JsonIgnore
    public boolean country_of_birthDirtyFlag;
    
    /**
     * 国籍
     */
    public String country_of_birth_text;

    @JsonIgnore
    public boolean country_of_birth_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 当前休假类型
     */
    public Integer current_leave_id;

    @JsonIgnore
    public boolean current_leave_idDirtyFlag;
    
    /**
     * 当前休假状态
     */
    public String current_leave_state;

    @JsonIgnore
    public boolean current_leave_stateDirtyFlag;
    
    /**
     * 部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 直接徽章
     */
    public String direct_badge_ids;

    @JsonIgnore
    public boolean direct_badge_idsDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 紧急联系人
     */
    public String emergency_contact;

    @JsonIgnore
    public boolean emergency_contactDirtyFlag;
    
    /**
     * 紧急电话
     */
    public String emergency_phone;

    @JsonIgnore
    public boolean emergency_phoneDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer expense_manager_id;

    @JsonIgnore
    public boolean expense_manager_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String expense_manager_id_text;

    @JsonIgnore
    public boolean expense_manager_id_textDirtyFlag;
    
    /**
     * 性别
     */
    public String gender;

    @JsonIgnore
    public boolean genderDirtyFlag;
    
    /**
     * 员工人力资源目标
     */
    public String goal_ids;

    @JsonIgnore
    public boolean goal_idsDirtyFlag;
    
    /**
     * 员工文档
     */
    public String google_drive_link;

    @JsonIgnore
    public boolean google_drive_linkDirtyFlag;
    
    /**
     * 拥有徽章
     */
    public String has_badges;

    @JsonIgnore
    public boolean has_badgesDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 身份证号
     */
    public String identification_id;

    @JsonIgnore
    public boolean identification_idDirtyFlag;
    
    /**
     * 照片
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 中等尺寸照片
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 小尺寸照片
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * 今日缺勤
     */
    public String is_absent_totay;

    @JsonIgnore
    public boolean is_absent_totayDirtyFlag;
    
    /**
     * 已与公司地址相关联的员工住址
     */
    public String is_address_home_a_company;

    @JsonIgnore
    public boolean is_address_home_a_companyDirtyFlag;
    
    /**
     * 工作岗位
     */
    public Integer job_id;

    @JsonIgnore
    public boolean job_idDirtyFlag;
    
    /**
     * 工作岗位
     */
    public String job_id_text;

    @JsonIgnore
    public boolean job_id_textDirtyFlag;
    
    /**
     * 工作头衔
     */
    public String job_title;

    @JsonIgnore
    public boolean job_titleDirtyFlag;
    
    /**
     * 上班距离
     */
    public Integer km_home_work;

    @JsonIgnore
    public boolean km_home_workDirtyFlag;
    
    /**
     * 上次出勤
     */
    public Integer last_attendance_id;

    @JsonIgnore
    public boolean last_attendance_idDirtyFlag;
    
    /**
     * 休假天数
     */
    public Double leaves_count;

    @JsonIgnore
    public boolean leaves_countDirtyFlag;
    
    /**
     * 起始日期
     */
    public Timestamp leave_date_from;

    @JsonIgnore
    public boolean leave_date_fromDirtyFlag;
    
    /**
     * 至日期
     */
    public Timestamp leave_date_to;

    @JsonIgnore
    public boolean leave_date_toDirtyFlag;
    
    /**
     * 是管理者
     */
    public String manager;

    @JsonIgnore
    public boolean managerDirtyFlag;
    
    /**
     * 手动出勤
     */
    public String manual_attendance;

    @JsonIgnore
    public boolean manual_attendanceDirtyFlag;
    
    /**
     * 婚姻状况
     */
    public String marital;

    @JsonIgnore
    public boolean maritalDirtyFlag;
    
    /**
     * 体检日期
     */
    public Timestamp medic_exam;

    @JsonIgnore
    public boolean medic_examDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 信息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 办公手机
     */
    public String mobile_phone;

    @JsonIgnore
    public boolean mobile_phoneDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 新近雇用的员工
     */
    public String newly_hired_employee;

    @JsonIgnore
    public boolean newly_hired_employeeDirtyFlag;
    
    /**
     * 备注
     */
    public String notes;

    @JsonIgnore
    public boolean notesDirtyFlag;
    
    /**
     * 经理
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 经理
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 护照号
     */
    public String passport_id;

    @JsonIgnore
    public boolean passport_idDirtyFlag;
    
    /**
     * 工作许可编号
     */
    public String permit_no;

    @JsonIgnore
    public boolean permit_noDirtyFlag;
    
    /**
     * PIN
     */
    public String pin;

    @JsonIgnore
    public boolean pinDirtyFlag;
    
    /**
     * 出生地
     */
    public String place_of_birth;

    @JsonIgnore
    public boolean place_of_birthDirtyFlag;
    
    /**
     * 剩余的法定休假
     */
    public Double remaining_leaves;

    @JsonIgnore
    public boolean remaining_leavesDirtyFlag;
    
    /**
     * 工作时间
     */
    public Integer resource_calendar_id;

    @JsonIgnore
    public boolean resource_calendar_idDirtyFlag;
    
    /**
     * 工作时间
     */
    public String resource_calendar_id_text;

    @JsonIgnore
    public boolean resource_calendar_id_textDirtyFlag;
    
    /**
     * 资源
     */
    public Integer resource_id;

    @JsonIgnore
    public boolean resource_idDirtyFlag;
    
    /**
     * 能查看剩余的休假
     */
    public String show_leaves;

    @JsonIgnore
    public boolean show_leavesDirtyFlag;
    
    /**
     * 社会保险号SIN
     */
    public String sinid;

    @JsonIgnore
    public boolean sinidDirtyFlag;
    
    /**
     * 配偶生日
     */
    public Timestamp spouse_birthdate;

    @JsonIgnore
    public boolean spouse_birthdateDirtyFlag;
    
    /**
     * 配偶全名
     */
    public String spouse_complete_name;

    @JsonIgnore
    public boolean spouse_complete_nameDirtyFlag;
    
    /**
     * 社会保障号SSN
     */
    public String ssnid;

    @JsonIgnore
    public boolean ssnidDirtyFlag;
    
    /**
     * 研究领域
     */
    public String study_field;

    @JsonIgnore
    public boolean study_fieldDirtyFlag;
    
    /**
     * 毕业院校
     */
    public String study_school;

    @JsonIgnore
    public boolean study_schoolDirtyFlag;
    
    /**
     * 时区
     */
    public String tz;

    @JsonIgnore
    public boolean tzDirtyFlag;
    
    /**
     * 用户
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 公司汽车
     */
    public String vehicle;

    @JsonIgnore
    public boolean vehicleDirtyFlag;
    
    /**
     * 签证到期日期
     */
    public Timestamp visa_expire;

    @JsonIgnore
    public boolean visa_expireDirtyFlag;
    
    /**
     * 签证号
     */
    public String visa_no;

    @JsonIgnore
    public boolean visa_noDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 工作EMail
     */
    public String work_email;

    @JsonIgnore
    public boolean work_emailDirtyFlag;
    
    /**
     * 工作地点
     */
    public String work_location;

    @JsonIgnore
    public boolean work_locationDirtyFlag;
    
    /**
     * 办公电话
     */
    public String work_phone;

    @JsonIgnore
    public boolean work_phoneDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [附加说明]
     */
    @JsonProperty("additional_note")
    public String getAdditional_note(){
        return this.additional_note ;
    }

    /**
     * 设置 [附加说明]
     */
    @JsonProperty("additional_note")
    public void setAdditional_note(String  additional_note){
        this.additional_note = additional_note ;
        this.additional_noteDirtyFlag = true ;
    }

     /**
     * 获取 [附加说明]脏标记
     */
    @JsonIgnore
    public boolean getAdditional_noteDirtyFlag(){
        return this.additional_noteDirtyFlag ;
    }   

    /**
     * 获取 [家庭住址]
     */
    @JsonProperty("address_home_id")
    public Integer getAddress_home_id(){
        return this.address_home_id ;
    }

    /**
     * 设置 [家庭住址]
     */
    @JsonProperty("address_home_id")
    public void setAddress_home_id(Integer  address_home_id){
        this.address_home_id = address_home_id ;
        this.address_home_idDirtyFlag = true ;
    }

     /**
     * 获取 [家庭住址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_home_idDirtyFlag(){
        return this.address_home_idDirtyFlag ;
    }   

    /**
     * 获取 [家庭住址]
     */
    @JsonProperty("address_home_id_text")
    public String getAddress_home_id_text(){
        return this.address_home_id_text ;
    }

    /**
     * 设置 [家庭住址]
     */
    @JsonProperty("address_home_id_text")
    public void setAddress_home_id_text(String  address_home_id_text){
        this.address_home_id_text = address_home_id_text ;
        this.address_home_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [家庭住址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_home_id_textDirtyFlag(){
        return this.address_home_id_textDirtyFlag ;
    }   

    /**
     * 获取 [工作地址]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return this.address_id ;
    }

    /**
     * 设置 [工作地址]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作地址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return this.address_idDirtyFlag ;
    }   

    /**
     * 获取 [工作地址]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return this.address_id_text ;
    }

    /**
     * 设置 [工作地址]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作地址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return this.address_id_textDirtyFlag ;
    }   

    /**
     * 获取 [出勤]
     */
    @JsonProperty("attendance_ids")
    public String getAttendance_ids(){
        return this.attendance_ids ;
    }

    /**
     * 设置 [出勤]
     */
    @JsonProperty("attendance_ids")
    public void setAttendance_ids(String  attendance_ids){
        this.attendance_ids = attendance_ids ;
        this.attendance_idsDirtyFlag = true ;
    }

     /**
     * 获取 [出勤]脏标记
     */
    @JsonIgnore
    public boolean getAttendance_idsDirtyFlag(){
        return this.attendance_idsDirtyFlag ;
    }   

    /**
     * 获取 [出勤状态]
     */
    @JsonProperty("attendance_state")
    public String getAttendance_state(){
        return this.attendance_state ;
    }

    /**
     * 设置 [出勤状态]
     */
    @JsonProperty("attendance_state")
    public void setAttendance_state(String  attendance_state){
        this.attendance_state = attendance_state ;
        this.attendance_stateDirtyFlag = true ;
    }

     /**
     * 获取 [出勤状态]脏标记
     */
    @JsonIgnore
    public boolean getAttendance_stateDirtyFlag(){
        return this.attendance_stateDirtyFlag ;
    }   

    /**
     * 获取 [员工徽章]
     */
    @JsonProperty("badge_ids")
    public String getBadge_ids(){
        return this.badge_ids ;
    }

    /**
     * 设置 [员工徽章]
     */
    @JsonProperty("badge_ids")
    public void setBadge_ids(String  badge_ids){
        this.badge_ids = badge_ids ;
        this.badge_idsDirtyFlag = true ;
    }

     /**
     * 获取 [员工徽章]脏标记
     */
    @JsonIgnore
    public boolean getBadge_idsDirtyFlag(){
        return this.badge_idsDirtyFlag ;
    }   

    /**
     * 获取 [银行账户号码]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return this.bank_account_id ;
    }

    /**
     * 设置 [银行账户号码]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户号码]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return this.bank_account_idDirtyFlag ;
    }   

    /**
     * 获取 [工牌 ID]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return this.barcode ;
    }

    /**
     * 设置 [工牌 ID]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

     /**
     * 获取 [工牌 ID]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return this.barcodeDirtyFlag ;
    }   

    /**
     * 获取 [出生日期]
     */
    @JsonProperty("birthday")
    public Timestamp getBirthday(){
        return this.birthday ;
    }

    /**
     * 设置 [出生日期]
     */
    @JsonProperty("birthday")
    public void setBirthday(Timestamp  birthday){
        this.birthday = birthday ;
        this.birthdayDirtyFlag = true ;
    }

     /**
     * 获取 [出生日期]脏标记
     */
    @JsonIgnore
    public boolean getBirthdayDirtyFlag(){
        return this.birthdayDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("category_ids")
    public String getCategory_ids(){
        return this.category_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("category_ids")
    public void setCategory_ids(String  category_ids){
        this.category_ids = category_ids ;
        this.category_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idsDirtyFlag(){
        return this.category_idsDirtyFlag ;
    }   

    /**
     * 获取 [证书等级]
     */
    @JsonProperty("certificate")
    public String getCertificate(){
        return this.certificate ;
    }

    /**
     * 设置 [证书等级]
     */
    @JsonProperty("certificate")
    public void setCertificate(String  certificate){
        this.certificate = certificate ;
        this.certificateDirtyFlag = true ;
    }

     /**
     * 获取 [证书等级]脏标记
     */
    @JsonIgnore
    public boolean getCertificateDirtyFlag(){
        return this.certificateDirtyFlag ;
    }   

    /**
     * 获取 [子女数目]
     */
    @JsonProperty("children")
    public Integer getChildren(){
        return this.children ;
    }

    /**
     * 设置 [子女数目]
     */
    @JsonProperty("children")
    public void setChildren(Integer  children){
        this.children = children ;
        this.childrenDirtyFlag = true ;
    }

     /**
     * 获取 [子女数目]脏标记
     */
    @JsonIgnore
    public boolean getChildrenDirtyFlag(){
        return this.childrenDirtyFlag ;
    }   

    /**
     * 获取 [下属]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [下属]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [下属]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [教练]
     */
    @JsonProperty("coach_id")
    public Integer getCoach_id(){
        return this.coach_id ;
    }

    /**
     * 设置 [教练]
     */
    @JsonProperty("coach_id")
    public void setCoach_id(Integer  coach_id){
        this.coach_id = coach_id ;
        this.coach_idDirtyFlag = true ;
    }

     /**
     * 获取 [教练]脏标记
     */
    @JsonIgnore
    public boolean getCoach_idDirtyFlag(){
        return this.coach_idDirtyFlag ;
    }   

    /**
     * 获取 [教练]
     */
    @JsonProperty("coach_id_text")
    public String getCoach_id_text(){
        return this.coach_id_text ;
    }

    /**
     * 设置 [教练]
     */
    @JsonProperty("coach_id_text")
    public void setCoach_id_text(String  coach_id_text){
        this.coach_id_text = coach_id_text ;
        this.coach_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [教练]脏标记
     */
    @JsonIgnore
    public boolean getCoach_id_textDirtyFlag(){
        return this.coach_id_textDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [合同统计]
     */
    @JsonProperty("contracts_count")
    public Integer getContracts_count(){
        return this.contracts_count ;
    }

    /**
     * 设置 [合同统计]
     */
    @JsonProperty("contracts_count")
    public void setContracts_count(Integer  contracts_count){
        this.contracts_count = contracts_count ;
        this.contracts_countDirtyFlag = true ;
    }

     /**
     * 获取 [合同统计]脏标记
     */
    @JsonIgnore
    public boolean getContracts_countDirtyFlag(){
        return this.contracts_countDirtyFlag ;
    }   

    /**
     * 获取 [当前合同]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return this.contract_id ;
    }

    /**
     * 设置 [当前合同]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

     /**
     * 获取 [当前合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return this.contract_idDirtyFlag ;
    }   

    /**
     * 获取 [员工合同]
     */
    @JsonProperty("contract_ids")
    public String getContract_ids(){
        return this.contract_ids ;
    }

    /**
     * 设置 [员工合同]
     */
    @JsonProperty("contract_ids")
    public void setContract_ids(String  contract_ids){
        this.contract_ids = contract_ids ;
        this.contract_idsDirtyFlag = true ;
    }

     /**
     * 获取 [员工合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idsDirtyFlag(){
        return this.contract_idsDirtyFlag ;
    }   

    /**
     * 获取 [国籍(国家)]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国籍(国家)]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国籍(国家)]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国籍(国家)]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国籍(国家)]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国籍(国家)]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国籍]
     */
    @JsonProperty("country_of_birth")
    public Integer getCountry_of_birth(){
        return this.country_of_birth ;
    }

    /**
     * 设置 [国籍]
     */
    @JsonProperty("country_of_birth")
    public void setCountry_of_birth(Integer  country_of_birth){
        this.country_of_birth = country_of_birth ;
        this.country_of_birthDirtyFlag = true ;
    }

     /**
     * 获取 [国籍]脏标记
     */
    @JsonIgnore
    public boolean getCountry_of_birthDirtyFlag(){
        return this.country_of_birthDirtyFlag ;
    }   

    /**
     * 获取 [国籍]
     */
    @JsonProperty("country_of_birth_text")
    public String getCountry_of_birth_text(){
        return this.country_of_birth_text ;
    }

    /**
     * 设置 [国籍]
     */
    @JsonProperty("country_of_birth_text")
    public void setCountry_of_birth_text(String  country_of_birth_text){
        this.country_of_birth_text = country_of_birth_text ;
        this.country_of_birth_textDirtyFlag = true ;
    }

     /**
     * 获取 [国籍]脏标记
     */
    @JsonIgnore
    public boolean getCountry_of_birth_textDirtyFlag(){
        return this.country_of_birth_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [当前休假类型]
     */
    @JsonProperty("current_leave_id")
    public Integer getCurrent_leave_id(){
        return this.current_leave_id ;
    }

    /**
     * 设置 [当前休假类型]
     */
    @JsonProperty("current_leave_id")
    public void setCurrent_leave_id(Integer  current_leave_id){
        this.current_leave_id = current_leave_id ;
        this.current_leave_idDirtyFlag = true ;
    }

     /**
     * 获取 [当前休假类型]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_leave_idDirtyFlag(){
        return this.current_leave_idDirtyFlag ;
    }   

    /**
     * 获取 [当前休假状态]
     */
    @JsonProperty("current_leave_state")
    public String getCurrent_leave_state(){
        return this.current_leave_state ;
    }

    /**
     * 设置 [当前休假状态]
     */
    @JsonProperty("current_leave_state")
    public void setCurrent_leave_state(String  current_leave_state){
        this.current_leave_state = current_leave_state ;
        this.current_leave_stateDirtyFlag = true ;
    }

     /**
     * 获取 [当前休假状态]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_leave_stateDirtyFlag(){
        return this.current_leave_stateDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [直接徽章]
     */
    @JsonProperty("direct_badge_ids")
    public String getDirect_badge_ids(){
        return this.direct_badge_ids ;
    }

    /**
     * 设置 [直接徽章]
     */
    @JsonProperty("direct_badge_ids")
    public void setDirect_badge_ids(String  direct_badge_ids){
        this.direct_badge_ids = direct_badge_ids ;
        this.direct_badge_idsDirtyFlag = true ;
    }

     /**
     * 获取 [直接徽章]脏标记
     */
    @JsonIgnore
    public boolean getDirect_badge_idsDirtyFlag(){
        return this.direct_badge_idsDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [紧急联系人]
     */
    @JsonProperty("emergency_contact")
    public String getEmergency_contact(){
        return this.emergency_contact ;
    }

    /**
     * 设置 [紧急联系人]
     */
    @JsonProperty("emergency_contact")
    public void setEmergency_contact(String  emergency_contact){
        this.emergency_contact = emergency_contact ;
        this.emergency_contactDirtyFlag = true ;
    }

     /**
     * 获取 [紧急联系人]脏标记
     */
    @JsonIgnore
    public boolean getEmergency_contactDirtyFlag(){
        return this.emergency_contactDirtyFlag ;
    }   

    /**
     * 获取 [紧急电话]
     */
    @JsonProperty("emergency_phone")
    public String getEmergency_phone(){
        return this.emergency_phone ;
    }

    /**
     * 设置 [紧急电话]
     */
    @JsonProperty("emergency_phone")
    public void setEmergency_phone(String  emergency_phone){
        this.emergency_phone = emergency_phone ;
        this.emergency_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [紧急电话]脏标记
     */
    @JsonIgnore
    public boolean getEmergency_phoneDirtyFlag(){
        return this.emergency_phoneDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("expense_manager_id")
    public Integer getExpense_manager_id(){
        return this.expense_manager_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("expense_manager_id")
    public void setExpense_manager_id(Integer  expense_manager_id){
        this.expense_manager_id = expense_manager_id ;
        this.expense_manager_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getExpense_manager_idDirtyFlag(){
        return this.expense_manager_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("expense_manager_id_text")
    public String getExpense_manager_id_text(){
        return this.expense_manager_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("expense_manager_id_text")
    public void setExpense_manager_id_text(String  expense_manager_id_text){
        this.expense_manager_id_text = expense_manager_id_text ;
        this.expense_manager_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getExpense_manager_id_textDirtyFlag(){
        return this.expense_manager_id_textDirtyFlag ;
    }   

    /**
     * 获取 [性别]
     */
    @JsonProperty("gender")
    public String getGender(){
        return this.gender ;
    }

    /**
     * 设置 [性别]
     */
    @JsonProperty("gender")
    public void setGender(String  gender){
        this.gender = gender ;
        this.genderDirtyFlag = true ;
    }

     /**
     * 获取 [性别]脏标记
     */
    @JsonIgnore
    public boolean getGenderDirtyFlag(){
        return this.genderDirtyFlag ;
    }   

    /**
     * 获取 [员工人力资源目标]
     */
    @JsonProperty("goal_ids")
    public String getGoal_ids(){
        return this.goal_ids ;
    }

    /**
     * 设置 [员工人力资源目标]
     */
    @JsonProperty("goal_ids")
    public void setGoal_ids(String  goal_ids){
        this.goal_ids = goal_ids ;
        this.goal_idsDirtyFlag = true ;
    }

     /**
     * 获取 [员工人力资源目标]脏标记
     */
    @JsonIgnore
    public boolean getGoal_idsDirtyFlag(){
        return this.goal_idsDirtyFlag ;
    }   

    /**
     * 获取 [员工文档]
     */
    @JsonProperty("google_drive_link")
    public String getGoogle_drive_link(){
        return this.google_drive_link ;
    }

    /**
     * 设置 [员工文档]
     */
    @JsonProperty("google_drive_link")
    public void setGoogle_drive_link(String  google_drive_link){
        this.google_drive_link = google_drive_link ;
        this.google_drive_linkDirtyFlag = true ;
    }

     /**
     * 获取 [员工文档]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_drive_linkDirtyFlag(){
        return this.google_drive_linkDirtyFlag ;
    }   

    /**
     * 获取 [拥有徽章]
     */
    @JsonProperty("has_badges")
    public String getHas_badges(){
        return this.has_badges ;
    }

    /**
     * 设置 [拥有徽章]
     */
    @JsonProperty("has_badges")
    public void setHas_badges(String  has_badges){
        this.has_badges = has_badges ;
        this.has_badgesDirtyFlag = true ;
    }

     /**
     * 获取 [拥有徽章]脏标记
     */
    @JsonIgnore
    public boolean getHas_badgesDirtyFlag(){
        return this.has_badgesDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [身份证号]
     */
    @JsonProperty("identification_id")
    public String getIdentification_id(){
        return this.identification_id ;
    }

    /**
     * 设置 [身份证号]
     */
    @JsonProperty("identification_id")
    public void setIdentification_id(String  identification_id){
        this.identification_id = identification_id ;
        this.identification_idDirtyFlag = true ;
    }

     /**
     * 获取 [身份证号]脏标记
     */
    @JsonIgnore
    public boolean getIdentification_idDirtyFlag(){
        return this.identification_idDirtyFlag ;
    }   

    /**
     * 获取 [照片]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [照片]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [照片]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [中等尺寸照片]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸照片]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [中等尺寸照片]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [小尺寸照片]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸照片]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [小尺寸照片]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [今日缺勤]
     */
    @JsonProperty("is_absent_totay")
    public String getIs_absent_totay(){
        return this.is_absent_totay ;
    }

    /**
     * 设置 [今日缺勤]
     */
    @JsonProperty("is_absent_totay")
    public void setIs_absent_totay(String  is_absent_totay){
        this.is_absent_totay = is_absent_totay ;
        this.is_absent_totayDirtyFlag = true ;
    }

     /**
     * 获取 [今日缺勤]脏标记
     */
    @JsonIgnore
    public boolean getIs_absent_totayDirtyFlag(){
        return this.is_absent_totayDirtyFlag ;
    }   

    /**
     * 获取 [已与公司地址相关联的员工住址]
     */
    @JsonProperty("is_address_home_a_company")
    public String getIs_address_home_a_company(){
        return this.is_address_home_a_company ;
    }

    /**
     * 设置 [已与公司地址相关联的员工住址]
     */
    @JsonProperty("is_address_home_a_company")
    public void setIs_address_home_a_company(String  is_address_home_a_company){
        this.is_address_home_a_company = is_address_home_a_company ;
        this.is_address_home_a_companyDirtyFlag = true ;
    }

     /**
     * 获取 [已与公司地址相关联的员工住址]脏标记
     */
    @JsonIgnore
    public boolean getIs_address_home_a_companyDirtyFlag(){
        return this.is_address_home_a_companyDirtyFlag ;
    }   

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("job_id")
    public Integer getJob_id(){
        return this.job_id ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("job_id")
    public void setJob_id(Integer  job_id){
        this.job_id = job_id ;
        this.job_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getJob_idDirtyFlag(){
        return this.job_idDirtyFlag ;
    }   

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("job_id_text")
    public String getJob_id_text(){
        return this.job_id_text ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("job_id_text")
    public void setJob_id_text(String  job_id_text){
        this.job_id_text = job_id_text ;
        this.job_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getJob_id_textDirtyFlag(){
        return this.job_id_textDirtyFlag ;
    }   

    /**
     * 获取 [工作头衔]
     */
    @JsonProperty("job_title")
    public String getJob_title(){
        return this.job_title ;
    }

    /**
     * 设置 [工作头衔]
     */
    @JsonProperty("job_title")
    public void setJob_title(String  job_title){
        this.job_title = job_title ;
        this.job_titleDirtyFlag = true ;
    }

     /**
     * 获取 [工作头衔]脏标记
     */
    @JsonIgnore
    public boolean getJob_titleDirtyFlag(){
        return this.job_titleDirtyFlag ;
    }   

    /**
     * 获取 [上班距离]
     */
    @JsonProperty("km_home_work")
    public Integer getKm_home_work(){
        return this.km_home_work ;
    }

    /**
     * 设置 [上班距离]
     */
    @JsonProperty("km_home_work")
    public void setKm_home_work(Integer  km_home_work){
        this.km_home_work = km_home_work ;
        this.km_home_workDirtyFlag = true ;
    }

     /**
     * 获取 [上班距离]脏标记
     */
    @JsonIgnore
    public boolean getKm_home_workDirtyFlag(){
        return this.km_home_workDirtyFlag ;
    }   

    /**
     * 获取 [上次出勤]
     */
    @JsonProperty("last_attendance_id")
    public Integer getLast_attendance_id(){
        return this.last_attendance_id ;
    }

    /**
     * 设置 [上次出勤]
     */
    @JsonProperty("last_attendance_id")
    public void setLast_attendance_id(Integer  last_attendance_id){
        this.last_attendance_id = last_attendance_id ;
        this.last_attendance_idDirtyFlag = true ;
    }

     /**
     * 获取 [上次出勤]脏标记
     */
    @JsonIgnore
    public boolean getLast_attendance_idDirtyFlag(){
        return this.last_attendance_idDirtyFlag ;
    }   

    /**
     * 获取 [休假天数]
     */
    @JsonProperty("leaves_count")
    public Double getLeaves_count(){
        return this.leaves_count ;
    }

    /**
     * 设置 [休假天数]
     */
    @JsonProperty("leaves_count")
    public void setLeaves_count(Double  leaves_count){
        this.leaves_count = leaves_count ;
        this.leaves_countDirtyFlag = true ;
    }

     /**
     * 获取 [休假天数]脏标记
     */
    @JsonIgnore
    public boolean getLeaves_countDirtyFlag(){
        return this.leaves_countDirtyFlag ;
    }   

    /**
     * 获取 [起始日期]
     */
    @JsonProperty("leave_date_from")
    public Timestamp getLeave_date_from(){
        return this.leave_date_from ;
    }

    /**
     * 设置 [起始日期]
     */
    @JsonProperty("leave_date_from")
    public void setLeave_date_from(Timestamp  leave_date_from){
        this.leave_date_from = leave_date_from ;
        this.leave_date_fromDirtyFlag = true ;
    }

     /**
     * 获取 [起始日期]脏标记
     */
    @JsonIgnore
    public boolean getLeave_date_fromDirtyFlag(){
        return this.leave_date_fromDirtyFlag ;
    }   

    /**
     * 获取 [至日期]
     */
    @JsonProperty("leave_date_to")
    public Timestamp getLeave_date_to(){
        return this.leave_date_to ;
    }

    /**
     * 设置 [至日期]
     */
    @JsonProperty("leave_date_to")
    public void setLeave_date_to(Timestamp  leave_date_to){
        this.leave_date_to = leave_date_to ;
        this.leave_date_toDirtyFlag = true ;
    }

     /**
     * 获取 [至日期]脏标记
     */
    @JsonIgnore
    public boolean getLeave_date_toDirtyFlag(){
        return this.leave_date_toDirtyFlag ;
    }   

    /**
     * 获取 [是管理者]
     */
    @JsonProperty("manager")
    public String getManager(){
        return this.manager ;
    }

    /**
     * 设置 [是管理者]
     */
    @JsonProperty("manager")
    public void setManager(String  manager){
        this.manager = manager ;
        this.managerDirtyFlag = true ;
    }

     /**
     * 获取 [是管理者]脏标记
     */
    @JsonIgnore
    public boolean getManagerDirtyFlag(){
        return this.managerDirtyFlag ;
    }   

    /**
     * 获取 [手动出勤]
     */
    @JsonProperty("manual_attendance")
    public String getManual_attendance(){
        return this.manual_attendance ;
    }

    /**
     * 设置 [手动出勤]
     */
    @JsonProperty("manual_attendance")
    public void setManual_attendance(String  manual_attendance){
        this.manual_attendance = manual_attendance ;
        this.manual_attendanceDirtyFlag = true ;
    }

     /**
     * 获取 [手动出勤]脏标记
     */
    @JsonIgnore
    public boolean getManual_attendanceDirtyFlag(){
        return this.manual_attendanceDirtyFlag ;
    }   

    /**
     * 获取 [婚姻状况]
     */
    @JsonProperty("marital")
    public String getMarital(){
        return this.marital ;
    }

    /**
     * 设置 [婚姻状况]
     */
    @JsonProperty("marital")
    public void setMarital(String  marital){
        this.marital = marital ;
        this.maritalDirtyFlag = true ;
    }

     /**
     * 获取 [婚姻状况]脏标记
     */
    @JsonIgnore
    public boolean getMaritalDirtyFlag(){
        return this.maritalDirtyFlag ;
    }   

    /**
     * 获取 [体检日期]
     */
    @JsonProperty("medic_exam")
    public Timestamp getMedic_exam(){
        return this.medic_exam ;
    }

    /**
     * 设置 [体检日期]
     */
    @JsonProperty("medic_exam")
    public void setMedic_exam(Timestamp  medic_exam){
        this.medic_exam = medic_exam ;
        this.medic_examDirtyFlag = true ;
    }

     /**
     * 获取 [体检日期]脏标记
     */
    @JsonIgnore
    public boolean getMedic_examDirtyFlag(){
        return this.medic_examDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [信息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [信息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [信息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [办公手机]
     */
    @JsonProperty("mobile_phone")
    public String getMobile_phone(){
        return this.mobile_phone ;
    }

    /**
     * 设置 [办公手机]
     */
    @JsonProperty("mobile_phone")
    public void setMobile_phone(String  mobile_phone){
        this.mobile_phone = mobile_phone ;
        this.mobile_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [办公手机]脏标记
     */
    @JsonIgnore
    public boolean getMobile_phoneDirtyFlag(){
        return this.mobile_phoneDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [新近雇用的员工]
     */
    @JsonProperty("newly_hired_employee")
    public String getNewly_hired_employee(){
        return this.newly_hired_employee ;
    }

    /**
     * 设置 [新近雇用的员工]
     */
    @JsonProperty("newly_hired_employee")
    public void setNewly_hired_employee(String  newly_hired_employee){
        this.newly_hired_employee = newly_hired_employee ;
        this.newly_hired_employeeDirtyFlag = true ;
    }

     /**
     * 获取 [新近雇用的员工]脏标记
     */
    @JsonIgnore
    public boolean getNewly_hired_employeeDirtyFlag(){
        return this.newly_hired_employeeDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return this.notes ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return this.notesDirtyFlag ;
    }   

    /**
     * 获取 [经理]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [经理]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [经理]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [经理]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [经理]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [经理]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [护照号]
     */
    @JsonProperty("passport_id")
    public String getPassport_id(){
        return this.passport_id ;
    }

    /**
     * 设置 [护照号]
     */
    @JsonProperty("passport_id")
    public void setPassport_id(String  passport_id){
        this.passport_id = passport_id ;
        this.passport_idDirtyFlag = true ;
    }

     /**
     * 获取 [护照号]脏标记
     */
    @JsonIgnore
    public boolean getPassport_idDirtyFlag(){
        return this.passport_idDirtyFlag ;
    }   

    /**
     * 获取 [工作许可编号]
     */
    @JsonProperty("permit_no")
    public String getPermit_no(){
        return this.permit_no ;
    }

    /**
     * 设置 [工作许可编号]
     */
    @JsonProperty("permit_no")
    public void setPermit_no(String  permit_no){
        this.permit_no = permit_no ;
        this.permit_noDirtyFlag = true ;
    }

     /**
     * 获取 [工作许可编号]脏标记
     */
    @JsonIgnore
    public boolean getPermit_noDirtyFlag(){
        return this.permit_noDirtyFlag ;
    }   

    /**
     * 获取 [PIN]
     */
    @JsonProperty("pin")
    public String getPin(){
        return this.pin ;
    }

    /**
     * 设置 [PIN]
     */
    @JsonProperty("pin")
    public void setPin(String  pin){
        this.pin = pin ;
        this.pinDirtyFlag = true ;
    }

     /**
     * 获取 [PIN]脏标记
     */
    @JsonIgnore
    public boolean getPinDirtyFlag(){
        return this.pinDirtyFlag ;
    }   

    /**
     * 获取 [出生地]
     */
    @JsonProperty("place_of_birth")
    public String getPlace_of_birth(){
        return this.place_of_birth ;
    }

    /**
     * 设置 [出生地]
     */
    @JsonProperty("place_of_birth")
    public void setPlace_of_birth(String  place_of_birth){
        this.place_of_birth = place_of_birth ;
        this.place_of_birthDirtyFlag = true ;
    }

     /**
     * 获取 [出生地]脏标记
     */
    @JsonIgnore
    public boolean getPlace_of_birthDirtyFlag(){
        return this.place_of_birthDirtyFlag ;
    }   

    /**
     * 获取 [剩余的法定休假]
     */
    @JsonProperty("remaining_leaves")
    public Double getRemaining_leaves(){
        return this.remaining_leaves ;
    }

    /**
     * 设置 [剩余的法定休假]
     */
    @JsonProperty("remaining_leaves")
    public void setRemaining_leaves(Double  remaining_leaves){
        this.remaining_leaves = remaining_leaves ;
        this.remaining_leavesDirtyFlag = true ;
    }

     /**
     * 获取 [剩余的法定休假]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_leavesDirtyFlag(){
        return this.remaining_leavesDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return this.resource_calendar_id ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return this.resource_calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return this.resource_calendar_id_text ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return this.resource_calendar_id_textDirtyFlag ;
    }   

    /**
     * 获取 [资源]
     */
    @JsonProperty("resource_id")
    public Integer getResource_id(){
        return this.resource_id ;
    }

    /**
     * 设置 [资源]
     */
    @JsonProperty("resource_id")
    public void setResource_id(Integer  resource_id){
        this.resource_id = resource_id ;
        this.resource_idDirtyFlag = true ;
    }

     /**
     * 获取 [资源]脏标记
     */
    @JsonIgnore
    public boolean getResource_idDirtyFlag(){
        return this.resource_idDirtyFlag ;
    }   

    /**
     * 获取 [能查看剩余的休假]
     */
    @JsonProperty("show_leaves")
    public String getShow_leaves(){
        return this.show_leaves ;
    }

    /**
     * 设置 [能查看剩余的休假]
     */
    @JsonProperty("show_leaves")
    public void setShow_leaves(String  show_leaves){
        this.show_leaves = show_leaves ;
        this.show_leavesDirtyFlag = true ;
    }

     /**
     * 获取 [能查看剩余的休假]脏标记
     */
    @JsonIgnore
    public boolean getShow_leavesDirtyFlag(){
        return this.show_leavesDirtyFlag ;
    }   

    /**
     * 获取 [社会保险号SIN]
     */
    @JsonProperty("sinid")
    public String getSinid(){
        return this.sinid ;
    }

    /**
     * 设置 [社会保险号SIN]
     */
    @JsonProperty("sinid")
    public void setSinid(String  sinid){
        this.sinid = sinid ;
        this.sinidDirtyFlag = true ;
    }

     /**
     * 获取 [社会保险号SIN]脏标记
     */
    @JsonIgnore
    public boolean getSinidDirtyFlag(){
        return this.sinidDirtyFlag ;
    }   

    /**
     * 获取 [配偶生日]
     */
    @JsonProperty("spouse_birthdate")
    public Timestamp getSpouse_birthdate(){
        return this.spouse_birthdate ;
    }

    /**
     * 设置 [配偶生日]
     */
    @JsonProperty("spouse_birthdate")
    public void setSpouse_birthdate(Timestamp  spouse_birthdate){
        this.spouse_birthdate = spouse_birthdate ;
        this.spouse_birthdateDirtyFlag = true ;
    }

     /**
     * 获取 [配偶生日]脏标记
     */
    @JsonIgnore
    public boolean getSpouse_birthdateDirtyFlag(){
        return this.spouse_birthdateDirtyFlag ;
    }   

    /**
     * 获取 [配偶全名]
     */
    @JsonProperty("spouse_complete_name")
    public String getSpouse_complete_name(){
        return this.spouse_complete_name ;
    }

    /**
     * 设置 [配偶全名]
     */
    @JsonProperty("spouse_complete_name")
    public void setSpouse_complete_name(String  spouse_complete_name){
        this.spouse_complete_name = spouse_complete_name ;
        this.spouse_complete_nameDirtyFlag = true ;
    }

     /**
     * 获取 [配偶全名]脏标记
     */
    @JsonIgnore
    public boolean getSpouse_complete_nameDirtyFlag(){
        return this.spouse_complete_nameDirtyFlag ;
    }   

    /**
     * 获取 [社会保障号SSN]
     */
    @JsonProperty("ssnid")
    public String getSsnid(){
        return this.ssnid ;
    }

    /**
     * 设置 [社会保障号SSN]
     */
    @JsonProperty("ssnid")
    public void setSsnid(String  ssnid){
        this.ssnid = ssnid ;
        this.ssnidDirtyFlag = true ;
    }

     /**
     * 获取 [社会保障号SSN]脏标记
     */
    @JsonIgnore
    public boolean getSsnidDirtyFlag(){
        return this.ssnidDirtyFlag ;
    }   

    /**
     * 获取 [研究领域]
     */
    @JsonProperty("study_field")
    public String getStudy_field(){
        return this.study_field ;
    }

    /**
     * 设置 [研究领域]
     */
    @JsonProperty("study_field")
    public void setStudy_field(String  study_field){
        this.study_field = study_field ;
        this.study_fieldDirtyFlag = true ;
    }

     /**
     * 获取 [研究领域]脏标记
     */
    @JsonIgnore
    public boolean getStudy_fieldDirtyFlag(){
        return this.study_fieldDirtyFlag ;
    }   

    /**
     * 获取 [毕业院校]
     */
    @JsonProperty("study_school")
    public String getStudy_school(){
        return this.study_school ;
    }

    /**
     * 设置 [毕业院校]
     */
    @JsonProperty("study_school")
    public void setStudy_school(String  study_school){
        this.study_school = study_school ;
        this.study_schoolDirtyFlag = true ;
    }

     /**
     * 获取 [毕业院校]脏标记
     */
    @JsonIgnore
    public boolean getStudy_schoolDirtyFlag(){
        return this.study_schoolDirtyFlag ;
    }   

    /**
     * 获取 [时区]
     */
    @JsonProperty("tz")
    public String getTz(){
        return this.tz ;
    }

    /**
     * 设置 [时区]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

     /**
     * 获取 [时区]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return this.tzDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司汽车]
     */
    @JsonProperty("vehicle")
    public String getVehicle(){
        return this.vehicle ;
    }

    /**
     * 设置 [公司汽车]
     */
    @JsonProperty("vehicle")
    public void setVehicle(String  vehicle){
        this.vehicle = vehicle ;
        this.vehicleDirtyFlag = true ;
    }

     /**
     * 获取 [公司汽车]脏标记
     */
    @JsonIgnore
    public boolean getVehicleDirtyFlag(){
        return this.vehicleDirtyFlag ;
    }   

    /**
     * 获取 [签证到期日期]
     */
    @JsonProperty("visa_expire")
    public Timestamp getVisa_expire(){
        return this.visa_expire ;
    }

    /**
     * 设置 [签证到期日期]
     */
    @JsonProperty("visa_expire")
    public void setVisa_expire(Timestamp  visa_expire){
        this.visa_expire = visa_expire ;
        this.visa_expireDirtyFlag = true ;
    }

     /**
     * 获取 [签证到期日期]脏标记
     */
    @JsonIgnore
    public boolean getVisa_expireDirtyFlag(){
        return this.visa_expireDirtyFlag ;
    }   

    /**
     * 获取 [签证号]
     */
    @JsonProperty("visa_no")
    public String getVisa_no(){
        return this.visa_no ;
    }

    /**
     * 设置 [签证号]
     */
    @JsonProperty("visa_no")
    public void setVisa_no(String  visa_no){
        this.visa_no = visa_no ;
        this.visa_noDirtyFlag = true ;
    }

     /**
     * 获取 [签证号]脏标记
     */
    @JsonIgnore
    public boolean getVisa_noDirtyFlag(){
        return this.visa_noDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [工作EMail]
     */
    @JsonProperty("work_email")
    public String getWork_email(){
        return this.work_email ;
    }

    /**
     * 设置 [工作EMail]
     */
    @JsonProperty("work_email")
    public void setWork_email(String  work_email){
        this.work_email = work_email ;
        this.work_emailDirtyFlag = true ;
    }

     /**
     * 获取 [工作EMail]脏标记
     */
    @JsonIgnore
    public boolean getWork_emailDirtyFlag(){
        return this.work_emailDirtyFlag ;
    }   

    /**
     * 获取 [工作地点]
     */
    @JsonProperty("work_location")
    public String getWork_location(){
        return this.work_location ;
    }

    /**
     * 设置 [工作地点]
     */
    @JsonProperty("work_location")
    public void setWork_location(String  work_location){
        this.work_location = work_location ;
        this.work_locationDirtyFlag = true ;
    }

     /**
     * 获取 [工作地点]脏标记
     */
    @JsonIgnore
    public boolean getWork_locationDirtyFlag(){
        return this.work_locationDirtyFlag ;
    }   

    /**
     * 获取 [办公电话]
     */
    @JsonProperty("work_phone")
    public String getWork_phone(){
        return this.work_phone ;
    }

    /**
     * 设置 [办公电话]
     */
    @JsonProperty("work_phone")
    public void setWork_phone(String  work_phone){
        this.work_phone = work_phone ;
        this.work_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [办公电话]脏标记
     */
    @JsonIgnore
    public boolean getWork_phoneDirtyFlag(){
        return this.work_phoneDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
