package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_type;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_leave_typeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_typeImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_leave_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_leave_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_leave_typeClientServiceImpl implements Ihr_leave_typeClientService {

    hr_leave_typeFeignClient hr_leave_typeFeignClient;

    @Autowired
    public hr_leave_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leave_typeFeignClient = nameBuilder.target(hr_leave_typeFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leave_typeFeignClient = nameBuilder.target(hr_leave_typeFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_leave_type createModel() {
		return new hr_leave_typeImpl();
	}


    public void updateBatch(List<Ihr_leave_type> hr_leave_types){
        if(hr_leave_types!=null){
            List<hr_leave_typeImpl> list = new ArrayList<hr_leave_typeImpl>();
            for(Ihr_leave_type ihr_leave_type :hr_leave_types){
                list.add((hr_leave_typeImpl)ihr_leave_type) ;
            }
            hr_leave_typeFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ihr_leave_type hr_leave_type){
        hr_leave_typeFeignClient.remove(hr_leave_type.getId()) ;
    }


    public void create(Ihr_leave_type hr_leave_type){
        Ihr_leave_type clientModel = hr_leave_typeFeignClient.create((hr_leave_typeImpl)hr_leave_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_type.getClass(), false);
        copier.copy(clientModel, hr_leave_type, null);
    }


    public Page<Ihr_leave_type> fetchDefault(SearchContext context){
        Page<hr_leave_typeImpl> page = this.hr_leave_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ihr_leave_type hr_leave_type){
        Ihr_leave_type clientModel = hr_leave_typeFeignClient.get(hr_leave_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_type.getClass(), false);
        copier.copy(clientModel, hr_leave_type, null);
    }


    public void update(Ihr_leave_type hr_leave_type){
        Ihr_leave_type clientModel = hr_leave_typeFeignClient.update(hr_leave_type.getId(),(hr_leave_typeImpl)hr_leave_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_type.getClass(), false);
        copier.copy(clientModel, hr_leave_type, null);
    }


    public void createBatch(List<Ihr_leave_type> hr_leave_types){
        if(hr_leave_types!=null){
            List<hr_leave_typeImpl> list = new ArrayList<hr_leave_typeImpl>();
            for(Ihr_leave_type ihr_leave_type :hr_leave_types){
                list.add((hr_leave_typeImpl)ihr_leave_type) ;
            }
            hr_leave_typeFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ihr_leave_type> hr_leave_types){
        if(hr_leave_types!=null){
            List<hr_leave_typeImpl> list = new ArrayList<hr_leave_typeImpl>();
            for(Ihr_leave_type ihr_leave_type :hr_leave_types){
                list.add((hr_leave_typeImpl)ihr_leave_type) ;
            }
            hr_leave_typeFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ihr_leave_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_leave_type hr_leave_type){
        Ihr_leave_type clientModel = hr_leave_typeFeignClient.getDraft(hr_leave_type.getId(),(hr_leave_typeImpl)hr_leave_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_type.getClass(), false);
        copier.copy(clientModel, hr_leave_type, null);
    }



}

