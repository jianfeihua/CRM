package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_report;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_leave_report] 服务对象接口
 */
public interface hr_leave_reportFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_reports/{id}")
    public hr_leave_reportImpl update(@PathVariable("id") Integer id,@RequestBody hr_leave_reportImpl hr_leave_report);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_reports/removebatch")
    public hr_leave_reportImpl removeBatch(@RequestBody List<hr_leave_reportImpl> hr_leave_reports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_reports/createbatch")
    public hr_leave_reportImpl createBatch(@RequestBody List<hr_leave_reportImpl> hr_leave_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_reports/fetchdefault")
    public Page<hr_leave_reportImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_reports")
    public hr_leave_reportImpl create(@RequestBody hr_leave_reportImpl hr_leave_report);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_reports/{id}")
    public hr_leave_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_reports/updatebatch")
    public hr_leave_reportImpl updateBatch(@RequestBody List<hr_leave_reportImpl> hr_leave_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_reports/select")
    public Page<hr_leave_reportImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_reports/{id}/getdraft")
    public hr_leave_reportImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_leave_reportImpl hr_leave_report);



}
