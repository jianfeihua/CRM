package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_allocation;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_leave_allocationClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_allocationImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_leave_allocationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_leave_allocation] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_leave_allocationClientServiceImpl implements Ihr_leave_allocationClientService {

    hr_leave_allocationFeignClient hr_leave_allocationFeignClient;

    @Autowired
    public hr_leave_allocationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leave_allocationFeignClient = nameBuilder.target(hr_leave_allocationFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leave_allocationFeignClient = nameBuilder.target(hr_leave_allocationFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_leave_allocation createModel() {
		return new hr_leave_allocationImpl();
	}


    public void removeBatch(List<Ihr_leave_allocation> hr_leave_allocations){
        if(hr_leave_allocations!=null){
            List<hr_leave_allocationImpl> list = new ArrayList<hr_leave_allocationImpl>();
            for(Ihr_leave_allocation ihr_leave_allocation :hr_leave_allocations){
                list.add((hr_leave_allocationImpl)ihr_leave_allocation) ;
            }
            hr_leave_allocationFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ihr_leave_allocation hr_leave_allocation){
        hr_leave_allocationFeignClient.remove(hr_leave_allocation.getId()) ;
    }


    public void get(Ihr_leave_allocation hr_leave_allocation){
        Ihr_leave_allocation clientModel = hr_leave_allocationFeignClient.get(hr_leave_allocation.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_allocation.getClass(), false);
        copier.copy(clientModel, hr_leave_allocation, null);
    }


    public Page<Ihr_leave_allocation> fetchDefault(SearchContext context){
        Page<hr_leave_allocationImpl> page = this.hr_leave_allocationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ihr_leave_allocation> hr_leave_allocations){
        if(hr_leave_allocations!=null){
            List<hr_leave_allocationImpl> list = new ArrayList<hr_leave_allocationImpl>();
            for(Ihr_leave_allocation ihr_leave_allocation :hr_leave_allocations){
                list.add((hr_leave_allocationImpl)ihr_leave_allocation) ;
            }
            hr_leave_allocationFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ihr_leave_allocation hr_leave_allocation){
        Ihr_leave_allocation clientModel = hr_leave_allocationFeignClient.update(hr_leave_allocation.getId(),(hr_leave_allocationImpl)hr_leave_allocation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_allocation.getClass(), false);
        copier.copy(clientModel, hr_leave_allocation, null);
    }


    public void create(Ihr_leave_allocation hr_leave_allocation){
        Ihr_leave_allocation clientModel = hr_leave_allocationFeignClient.create((hr_leave_allocationImpl)hr_leave_allocation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_allocation.getClass(), false);
        copier.copy(clientModel, hr_leave_allocation, null);
    }


    public void createBatch(List<Ihr_leave_allocation> hr_leave_allocations){
        if(hr_leave_allocations!=null){
            List<hr_leave_allocationImpl> list = new ArrayList<hr_leave_allocationImpl>();
            for(Ihr_leave_allocation ihr_leave_allocation :hr_leave_allocations){
                list.add((hr_leave_allocationImpl)ihr_leave_allocation) ;
            }
            hr_leave_allocationFeignClient.createBatch(list) ;
        }
    }


    public Page<Ihr_leave_allocation> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_leave_allocation hr_leave_allocation){
        Ihr_leave_allocation clientModel = hr_leave_allocationFeignClient.getDraft(hr_leave_allocation.getId(),(hr_leave_allocationImpl)hr_leave_allocation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_allocation.getClass(), false);
        copier.copy(clientModel, hr_leave_allocation, null);
    }



}

