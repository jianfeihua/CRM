package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_employee;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_employeeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_employeeImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_employeeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_employee] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_employeeClientServiceImpl implements Ihr_employeeClientService {

    hr_employeeFeignClient hr_employeeFeignClient;

    @Autowired
    public hr_employeeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_employeeFeignClient = nameBuilder.target(hr_employeeFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_employeeFeignClient = nameBuilder.target(hr_employeeFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_employee createModel() {
		return new hr_employeeImpl();
	}


    public Page<Ihr_employee> fetchDefault(SearchContext context){
        Page<hr_employeeImpl> page = this.hr_employeeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ihr_employee> hr_employees){
        if(hr_employees!=null){
            List<hr_employeeImpl> list = new ArrayList<hr_employeeImpl>();
            for(Ihr_employee ihr_employee :hr_employees){
                list.add((hr_employeeImpl)ihr_employee) ;
            }
            hr_employeeFeignClient.createBatch(list) ;
        }
    }


    public void update(Ihr_employee hr_employee){
        Ihr_employee clientModel = hr_employeeFeignClient.update(hr_employee.getId(),(hr_employeeImpl)hr_employee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee.getClass(), false);
        copier.copy(clientModel, hr_employee, null);
    }


    public void updateBatch(List<Ihr_employee> hr_employees){
        if(hr_employees!=null){
            List<hr_employeeImpl> list = new ArrayList<hr_employeeImpl>();
            for(Ihr_employee ihr_employee :hr_employees){
                list.add((hr_employeeImpl)ihr_employee) ;
            }
            hr_employeeFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ihr_employee> hr_employees){
        if(hr_employees!=null){
            List<hr_employeeImpl> list = new ArrayList<hr_employeeImpl>();
            for(Ihr_employee ihr_employee :hr_employees){
                list.add((hr_employeeImpl)ihr_employee) ;
            }
            hr_employeeFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ihr_employee hr_employee){
        hr_employeeFeignClient.remove(hr_employee.getId()) ;
    }


    public void create(Ihr_employee hr_employee){
        Ihr_employee clientModel = hr_employeeFeignClient.create((hr_employeeImpl)hr_employee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee.getClass(), false);
        copier.copy(clientModel, hr_employee, null);
    }


    public void get(Ihr_employee hr_employee){
        Ihr_employee clientModel = hr_employeeFeignClient.get(hr_employee.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee.getClass(), false);
        copier.copy(clientModel, hr_employee, null);
    }


    public Page<Ihr_employee> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_employee hr_employee){
        Ihr_employee clientModel = hr_employeeFeignClient.getDraft(hr_employee.getId(),(hr_employeeImpl)hr_employee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee.getClass(), false);
        copier.copy(clientModel, hr_employee, null);
    }



}

