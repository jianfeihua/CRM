package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_putaway;
import cn.ibizlab.odoo.client.odoo_product.model.product_putawayImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_putaway] 服务对象接口
 */
public interface product_putawayFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_putaways/createbatch")
    public product_putawayImpl createBatch(@RequestBody List<product_putawayImpl> product_putaways);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_putaways")
    public product_putawayImpl create(@RequestBody product_putawayImpl product_putaway);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_putaways/fetchdefault")
    public Page<product_putawayImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_putaways/{id}")
    public product_putawayImpl update(@PathVariable("id") Integer id,@RequestBody product_putawayImpl product_putaway);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_putaways/removebatch")
    public product_putawayImpl removeBatch(@RequestBody List<product_putawayImpl> product_putaways);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_putaways/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_putaways/updatebatch")
    public product_putawayImpl updateBatch(@RequestBody List<product_putawayImpl> product_putaways);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_putaways/{id}")
    public product_putawayImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_putaways/select")
    public Page<product_putawayImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_putaways/{id}/getdraft")
    public product_putawayImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_putawayImpl product_putaway);



}
