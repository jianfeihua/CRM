package cn.ibizlab.odoo.client.odoo_product.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproduct_supplierinfo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[product_supplierinfo] 对象
 */
public class product_supplierinfoImpl implements Iproduct_supplierinfo,Serializable{

    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp date_end;

    @JsonIgnore
    public boolean date_endDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp date_start;

    @JsonIgnore
    public boolean date_startDirtyFlag;
    
    /**
     * 交货提前时间
     */
    public Integer delay;

    @JsonIgnore
    public boolean delayDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最少数量
     */
    public Double min_qty;

    @JsonIgnore
    public boolean min_qtyDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 供应商
     */
    public String name_text;

    @JsonIgnore
    public boolean name_textDirtyFlag;
    
    /**
     * 价格
     */
    public Double price;

    @JsonIgnore
    public boolean priceDirtyFlag;
    
    /**
     * 供应商产品代码
     */
    public String product_code;

    @JsonIgnore
    public boolean product_codeDirtyFlag;
    
    /**
     * 产品变体
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品变体
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 供应商产品名称
     */
    public String product_name;

    @JsonIgnore
    public boolean product_nameDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 产品模板
     */
    public String product_tmpl_id_text;

    @JsonIgnore
    public boolean product_tmpl_id_textDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 变体计数
     */
    public Integer product_variant_count;

    @JsonIgnore
    public boolean product_variant_countDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return this.date_end ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return this.date_endDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return this.date_start ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return this.date_startDirtyFlag ;
    }   

    /**
     * 获取 [交货提前时间]
     */
    @JsonProperty("delay")
    public Integer getDelay(){
        return this.delay ;
    }

    /**
     * 设置 [交货提前时间]
     */
    @JsonProperty("delay")
    public void setDelay(Integer  delay){
        this.delay = delay ;
        this.delayDirtyFlag = true ;
    }

     /**
     * 获取 [交货提前时间]脏标记
     */
    @JsonIgnore
    public boolean getDelayDirtyFlag(){
        return this.delayDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最少数量]
     */
    @JsonProperty("min_qty")
    public Double getMin_qty(){
        return this.min_qty ;
    }

    /**
     * 设置 [最少数量]
     */
    @JsonProperty("min_qty")
    public void setMin_qty(Double  min_qty){
        this.min_qty = min_qty ;
        this.min_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [最少数量]脏标记
     */
    @JsonIgnore
    public boolean getMin_qtyDirtyFlag(){
        return this.min_qtyDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("name")
    public Integer getName(){
        return this.name ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("name")
    public void setName(Integer  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("name_text")
    public String getName_text(){
        return this.name_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("name_text")
    public void setName_text(String  name_text){
        this.name_text = name_text ;
        this.name_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getName_textDirtyFlag(){
        return this.name_textDirtyFlag ;
    }   

    /**
     * 获取 [价格]
     */
    @JsonProperty("price")
    public Double getPrice(){
        return this.price ;
    }

    /**
     * 设置 [价格]
     */
    @JsonProperty("price")
    public void setPrice(Double  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

     /**
     * 获取 [价格]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return this.priceDirtyFlag ;
    }   

    /**
     * 获取 [供应商产品代码]
     */
    @JsonProperty("product_code")
    public String getProduct_code(){
        return this.product_code ;
    }

    /**
     * 设置 [供应商产品代码]
     */
    @JsonProperty("product_code")
    public void setProduct_code(String  product_code){
        this.product_code = product_code ;
        this.product_codeDirtyFlag = true ;
    }

     /**
     * 获取 [供应商产品代码]脏标记
     */
    @JsonIgnore
    public boolean getProduct_codeDirtyFlag(){
        return this.product_codeDirtyFlag ;
    }   

    /**
     * 获取 [产品变体]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品变体]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品变体]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品变体]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品变体]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品变体]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [供应商产品名称]
     */
    @JsonProperty("product_name")
    public String getProduct_name(){
        return this.product_name ;
    }

    /**
     * 设置 [供应商产品名称]
     */
    @JsonProperty("product_name")
    public void setProduct_name(String  product_name){
        this.product_name = product_name ;
        this.product_nameDirtyFlag = true ;
    }

     /**
     * 获取 [供应商产品名称]脏标记
     */
    @JsonIgnore
    public boolean getProduct_nameDirtyFlag(){
        return this.product_nameDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return this.product_tmpl_id_text ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return this.product_tmpl_id_textDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [变体计数]
     */
    @JsonProperty("product_variant_count")
    public Integer getProduct_variant_count(){
        return this.product_variant_count ;
    }

    /**
     * 设置 [变体计数]
     */
    @JsonProperty("product_variant_count")
    public void setProduct_variant_count(Integer  product_variant_count){
        this.product_variant_count = product_variant_count ;
        this.product_variant_countDirtyFlag = true ;
    }

     /**
     * 获取 [变体计数]脏标记
     */
    @JsonIgnore
    public boolean getProduct_variant_countDirtyFlag(){
        return this.product_variant_countDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
