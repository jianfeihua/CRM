package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_category;
import cn.ibizlab.odoo.client.odoo_product.model.product_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_category] 服务对象接口
 */
public interface product_categoryFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_categories/{id}")
    public product_categoryImpl update(@PathVariable("id") Integer id,@RequestBody product_categoryImpl product_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_categories/removebatch")
    public product_categoryImpl removeBatch(@RequestBody List<product_categoryImpl> product_categories);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_categories")
    public product_categoryImpl create(@RequestBody product_categoryImpl product_category);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_categories/createbatch")
    public product_categoryImpl createBatch(@RequestBody List<product_categoryImpl> product_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_categories/{id}")
    public product_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_categories/fetchdefault")
    public Page<product_categoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_categories/updatebatch")
    public product_categoryImpl updateBatch(@RequestBody List<product_categoryImpl> product_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_categories/select")
    public Page<product_categoryImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_categories/{id}/save")
    public product_categoryImpl save(@PathVariable("id") Integer id,@RequestBody product_categoryImpl product_category);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_categories/{id}/getdraft")
    public product_categoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_categoryImpl product_category);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_categories/{id}/checkkey")
    public product_categoryImpl checkKey(@PathVariable("id") Integer id,@RequestBody product_categoryImpl product_category);



}
