package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_putaway;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_putawayClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_putawayImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_putawayFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_putaway] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_putawayClientServiceImpl implements Iproduct_putawayClientService {

    product_putawayFeignClient product_putawayFeignClient;

    @Autowired
    public product_putawayClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_putawayFeignClient = nameBuilder.target(product_putawayFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_putawayFeignClient = nameBuilder.target(product_putawayFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_putaway createModel() {
		return new product_putawayImpl();
	}


    public void createBatch(List<Iproduct_putaway> product_putaways){
        if(product_putaways!=null){
            List<product_putawayImpl> list = new ArrayList<product_putawayImpl>();
            for(Iproduct_putaway iproduct_putaway :product_putaways){
                list.add((product_putawayImpl)iproduct_putaway) ;
            }
            product_putawayFeignClient.createBatch(list) ;
        }
    }


    public void create(Iproduct_putaway product_putaway){
        Iproduct_putaway clientModel = product_putawayFeignClient.create((product_putawayImpl)product_putaway) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_putaway.getClass(), false);
        copier.copy(clientModel, product_putaway, null);
    }


    public Page<Iproduct_putaway> fetchDefault(SearchContext context){
        Page<product_putawayImpl> page = this.product_putawayFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iproduct_putaway product_putaway){
        Iproduct_putaway clientModel = product_putawayFeignClient.update(product_putaway.getId(),(product_putawayImpl)product_putaway) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_putaway.getClass(), false);
        copier.copy(clientModel, product_putaway, null);
    }


    public void removeBatch(List<Iproduct_putaway> product_putaways){
        if(product_putaways!=null){
            List<product_putawayImpl> list = new ArrayList<product_putawayImpl>();
            for(Iproduct_putaway iproduct_putaway :product_putaways){
                list.add((product_putawayImpl)iproduct_putaway) ;
            }
            product_putawayFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iproduct_putaway product_putaway){
        product_putawayFeignClient.remove(product_putaway.getId()) ;
    }


    public void updateBatch(List<Iproduct_putaway> product_putaways){
        if(product_putaways!=null){
            List<product_putawayImpl> list = new ArrayList<product_putawayImpl>();
            for(Iproduct_putaway iproduct_putaway :product_putaways){
                list.add((product_putawayImpl)iproduct_putaway) ;
            }
            product_putawayFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iproduct_putaway product_putaway){
        Iproduct_putaway clientModel = product_putawayFeignClient.get(product_putaway.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_putaway.getClass(), false);
        copier.copy(clientModel, product_putaway, null);
    }


    public Page<Iproduct_putaway> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_putaway product_putaway){
        Iproduct_putaway clientModel = product_putawayFeignClient.getDraft(product_putaway.getId(),(product_putawayImpl)product_putaway) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_putaway.getClass(), false);
        copier.copy(clientModel, product_putaway, null);
    }



}

