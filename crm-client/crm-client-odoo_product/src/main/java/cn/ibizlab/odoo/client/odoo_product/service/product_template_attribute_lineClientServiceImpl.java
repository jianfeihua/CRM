package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_line;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_template_attribute_lineClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_lineImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_template_attribute_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_template_attribute_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_template_attribute_lineClientServiceImpl implements Iproduct_template_attribute_lineClientService {

    product_template_attribute_lineFeignClient product_template_attribute_lineFeignClient;

    @Autowired
    public product_template_attribute_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_template_attribute_lineFeignClient = nameBuilder.target(product_template_attribute_lineFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_template_attribute_lineFeignClient = nameBuilder.target(product_template_attribute_lineFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_template_attribute_line createModel() {
		return new product_template_attribute_lineImpl();
	}


    public void removeBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines){
        if(product_template_attribute_lines!=null){
            List<product_template_attribute_lineImpl> list = new ArrayList<product_template_attribute_lineImpl>();
            for(Iproduct_template_attribute_line iproduct_template_attribute_line :product_template_attribute_lines){
                list.add((product_template_attribute_lineImpl)iproduct_template_attribute_line) ;
            }
            product_template_attribute_lineFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iproduct_template_attribute_line> fetchDefault(SearchContext context){
        Page<product_template_attribute_lineImpl> page = this.product_template_attribute_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines){
        if(product_template_attribute_lines!=null){
            List<product_template_attribute_lineImpl> list = new ArrayList<product_template_attribute_lineImpl>();
            for(Iproduct_template_attribute_line iproduct_template_attribute_line :product_template_attribute_lines){
                list.add((product_template_attribute_lineImpl)iproduct_template_attribute_line) ;
            }
            product_template_attribute_lineFeignClient.createBatch(list) ;
        }
    }


    public void update(Iproduct_template_attribute_line product_template_attribute_line){
        Iproduct_template_attribute_line clientModel = product_template_attribute_lineFeignClient.update(product_template_attribute_line.getId(),(product_template_attribute_lineImpl)product_template_attribute_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_line.getClass(), false);
        copier.copy(clientModel, product_template_attribute_line, null);
    }


    public void remove(Iproduct_template_attribute_line product_template_attribute_line){
        product_template_attribute_lineFeignClient.remove(product_template_attribute_line.getId()) ;
    }


    public void create(Iproduct_template_attribute_line product_template_attribute_line){
        Iproduct_template_attribute_line clientModel = product_template_attribute_lineFeignClient.create((product_template_attribute_lineImpl)product_template_attribute_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_line.getClass(), false);
        copier.copy(clientModel, product_template_attribute_line, null);
    }


    public void updateBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines){
        if(product_template_attribute_lines!=null){
            List<product_template_attribute_lineImpl> list = new ArrayList<product_template_attribute_lineImpl>();
            for(Iproduct_template_attribute_line iproduct_template_attribute_line :product_template_attribute_lines){
                list.add((product_template_attribute_lineImpl)iproduct_template_attribute_line) ;
            }
            product_template_attribute_lineFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iproduct_template_attribute_line product_template_attribute_line){
        Iproduct_template_attribute_line clientModel = product_template_attribute_lineFeignClient.get(product_template_attribute_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_line.getClass(), false);
        copier.copy(clientModel, product_template_attribute_line, null);
    }


    public Page<Iproduct_template_attribute_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_template_attribute_line product_template_attribute_line){
        Iproduct_template_attribute_line clientModel = product_template_attribute_lineFeignClient.getDraft(product_template_attribute_line.getId(),(product_template_attribute_lineImpl)product_template_attribute_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_line.getClass(), false);
        copier.copy(clientModel, product_template_attribute_line, null);
    }



}

