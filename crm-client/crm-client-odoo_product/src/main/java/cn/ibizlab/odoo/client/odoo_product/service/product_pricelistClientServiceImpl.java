package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_pricelistClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_pricelistImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_pricelistFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_pricelist] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_pricelistClientServiceImpl implements Iproduct_pricelistClientService {

    product_pricelistFeignClient product_pricelistFeignClient;

    @Autowired
    public product_pricelistClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_pricelistFeignClient = nameBuilder.target(product_pricelistFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_pricelistFeignClient = nameBuilder.target(product_pricelistFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_pricelist createModel() {
		return new product_pricelistImpl();
	}


    public void remove(Iproduct_pricelist product_pricelist){
        product_pricelistFeignClient.remove(product_pricelist.getId()) ;
    }


    public void get(Iproduct_pricelist product_pricelist){
        Iproduct_pricelist clientModel = product_pricelistFeignClient.get(product_pricelist.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist.getClass(), false);
        copier.copy(clientModel, product_pricelist, null);
    }


    public Page<Iproduct_pricelist> fetchDefault(SearchContext context){
        Page<product_pricelistImpl> page = this.product_pricelistFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iproduct_pricelist> product_pricelists){
        if(product_pricelists!=null){
            List<product_pricelistImpl> list = new ArrayList<product_pricelistImpl>();
            for(Iproduct_pricelist iproduct_pricelist :product_pricelists){
                list.add((product_pricelistImpl)iproduct_pricelist) ;
            }
            product_pricelistFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iproduct_pricelist> product_pricelists){
        if(product_pricelists!=null){
            List<product_pricelistImpl> list = new ArrayList<product_pricelistImpl>();
            for(Iproduct_pricelist iproduct_pricelist :product_pricelists){
                list.add((product_pricelistImpl)iproduct_pricelist) ;
            }
            product_pricelistFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iproduct_pricelist> product_pricelists){
        if(product_pricelists!=null){
            List<product_pricelistImpl> list = new ArrayList<product_pricelistImpl>();
            for(Iproduct_pricelist iproduct_pricelist :product_pricelists){
                list.add((product_pricelistImpl)iproduct_pricelist) ;
            }
            product_pricelistFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iproduct_pricelist product_pricelist){
        Iproduct_pricelist clientModel = product_pricelistFeignClient.create((product_pricelistImpl)product_pricelist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist.getClass(), false);
        copier.copy(clientModel, product_pricelist, null);
    }


    public void update(Iproduct_pricelist product_pricelist){
        Iproduct_pricelist clientModel = product_pricelistFeignClient.update(product_pricelist.getId(),(product_pricelistImpl)product_pricelist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist.getClass(), false);
        copier.copy(clientModel, product_pricelist, null);
    }


    public Page<Iproduct_pricelist> select(SearchContext context){
        return null ;
    }


    public void save(Iproduct_pricelist product_pricelist){
        Iproduct_pricelist clientModel = product_pricelistFeignClient.save(product_pricelist.getId(),(product_pricelistImpl)product_pricelist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist.getClass(), false);
        copier.copy(clientModel, product_pricelist, null);
    }


    public void getDraft(Iproduct_pricelist product_pricelist){
        Iproduct_pricelist clientModel = product_pricelistFeignClient.getDraft(product_pricelist.getId(),(product_pricelistImpl)product_pricelist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist.getClass(), false);
        copier.copy(clientModel, product_pricelist, null);
    }


    public void checkKey(Iproduct_pricelist product_pricelist){
        Iproduct_pricelist clientModel = product_pricelistFeignClient.checkKey(product_pricelist.getId(),(product_pricelistImpl)product_pricelist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist.getClass(), false);
        copier.copy(clientModel, product_pricelist, null);
    }



}

