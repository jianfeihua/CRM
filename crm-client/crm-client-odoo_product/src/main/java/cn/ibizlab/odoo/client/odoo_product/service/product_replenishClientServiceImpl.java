package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_replenish;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_replenishClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_replenishImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_replenishFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_replenish] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_replenishClientServiceImpl implements Iproduct_replenishClientService {

    product_replenishFeignClient product_replenishFeignClient;

    @Autowired
    public product_replenishClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_replenishFeignClient = nameBuilder.target(product_replenishFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_replenishFeignClient = nameBuilder.target(product_replenishFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_replenish createModel() {
		return new product_replenishImpl();
	}


    public void removeBatch(List<Iproduct_replenish> product_replenishes){
        if(product_replenishes!=null){
            List<product_replenishImpl> list = new ArrayList<product_replenishImpl>();
            for(Iproduct_replenish iproduct_replenish :product_replenishes){
                list.add((product_replenishImpl)iproduct_replenish) ;
            }
            product_replenishFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iproduct_replenish> product_replenishes){
        if(product_replenishes!=null){
            List<product_replenishImpl> list = new ArrayList<product_replenishImpl>();
            for(Iproduct_replenish iproduct_replenish :product_replenishes){
                list.add((product_replenishImpl)iproduct_replenish) ;
            }
            product_replenishFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iproduct_replenish> product_replenishes){
        if(product_replenishes!=null){
            List<product_replenishImpl> list = new ArrayList<product_replenishImpl>();
            for(Iproduct_replenish iproduct_replenish :product_replenishes){
                list.add((product_replenishImpl)iproduct_replenish) ;
            }
            product_replenishFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproduct_replenish> fetchDefault(SearchContext context){
        Page<product_replenishImpl> page = this.product_replenishFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iproduct_replenish product_replenish){
        product_replenishFeignClient.remove(product_replenish.getId()) ;
    }


    public void update(Iproduct_replenish product_replenish){
        Iproduct_replenish clientModel = product_replenishFeignClient.update(product_replenish.getId(),(product_replenishImpl)product_replenish) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_replenish.getClass(), false);
        copier.copy(clientModel, product_replenish, null);
    }


    public void get(Iproduct_replenish product_replenish){
        Iproduct_replenish clientModel = product_replenishFeignClient.get(product_replenish.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_replenish.getClass(), false);
        copier.copy(clientModel, product_replenish, null);
    }


    public void create(Iproduct_replenish product_replenish){
        Iproduct_replenish clientModel = product_replenishFeignClient.create((product_replenishImpl)product_replenish) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_replenish.getClass(), false);
        copier.copy(clientModel, product_replenish, null);
    }


    public Page<Iproduct_replenish> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_replenish product_replenish){
        Iproduct_replenish clientModel = product_replenishFeignClient.getDraft(product_replenish.getId(),(product_replenishImpl)product_replenish) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_replenish.getClass(), false);
        copier.copy(clientModel, product_replenish, null);
    }



}

