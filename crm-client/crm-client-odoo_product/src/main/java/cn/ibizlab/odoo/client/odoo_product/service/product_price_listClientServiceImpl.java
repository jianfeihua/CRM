package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_list;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_price_listClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_price_listImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_price_listFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_price_list] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_price_listClientServiceImpl implements Iproduct_price_listClientService {

    product_price_listFeignClient product_price_listFeignClient;

    @Autowired
    public product_price_listClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_price_listFeignClient = nameBuilder.target(product_price_listFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_price_listFeignClient = nameBuilder.target(product_price_listFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_price_list createModel() {
		return new product_price_listImpl();
	}


    public void createBatch(List<Iproduct_price_list> product_price_lists){
        if(product_price_lists!=null){
            List<product_price_listImpl> list = new ArrayList<product_price_listImpl>();
            for(Iproduct_price_list iproduct_price_list :product_price_lists){
                list.add((product_price_listImpl)iproduct_price_list) ;
            }
            product_price_listFeignClient.createBatch(list) ;
        }
    }


    public void create(Iproduct_price_list product_price_list){
        Iproduct_price_list clientModel = product_price_listFeignClient.create((product_price_listImpl)product_price_list) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_list.getClass(), false);
        copier.copy(clientModel, product_price_list, null);
    }


    public void updateBatch(List<Iproduct_price_list> product_price_lists){
        if(product_price_lists!=null){
            List<product_price_listImpl> list = new ArrayList<product_price_listImpl>();
            for(Iproduct_price_list iproduct_price_list :product_price_lists){
                list.add((product_price_listImpl)iproduct_price_list) ;
            }
            product_price_listFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iproduct_price_list product_price_list){
        Iproduct_price_list clientModel = product_price_listFeignClient.get(product_price_list.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_list.getClass(), false);
        copier.copy(clientModel, product_price_list, null);
    }


    public void update(Iproduct_price_list product_price_list){
        Iproduct_price_list clientModel = product_price_listFeignClient.update(product_price_list.getId(),(product_price_listImpl)product_price_list) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_list.getClass(), false);
        copier.copy(clientModel, product_price_list, null);
    }


    public Page<Iproduct_price_list> fetchDefault(SearchContext context){
        Page<product_price_listImpl> page = this.product_price_listFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iproduct_price_list product_price_list){
        product_price_listFeignClient.remove(product_price_list.getId()) ;
    }


    public void removeBatch(List<Iproduct_price_list> product_price_lists){
        if(product_price_lists!=null){
            List<product_price_listImpl> list = new ArrayList<product_price_listImpl>();
            for(Iproduct_price_list iproduct_price_list :product_price_lists){
                list.add((product_price_listImpl)iproduct_price_list) ;
            }
            product_price_listFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iproduct_price_list> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_price_list product_price_list){
        Iproduct_price_list clientModel = product_price_listFeignClient.getDraft(product_price_list.getId(),(product_price_listImpl)product_price_list) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_list.getClass(), false);
        copier.copy(clientModel, product_price_list, null);
    }



}

