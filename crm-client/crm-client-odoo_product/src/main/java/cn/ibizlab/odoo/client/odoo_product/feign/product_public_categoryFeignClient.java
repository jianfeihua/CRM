package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_public_category;
import cn.ibizlab.odoo.client.odoo_product.model.product_public_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_public_category] 服务对象接口
 */
public interface product_public_categoryFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_public_categories/{id}")
    public product_public_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_public_categories")
    public product_public_categoryImpl create(@RequestBody product_public_categoryImpl product_public_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_public_categories/removebatch")
    public product_public_categoryImpl removeBatch(@RequestBody List<product_public_categoryImpl> product_public_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_public_categories/fetchdefault")
    public Page<product_public_categoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_public_categories/createbatch")
    public product_public_categoryImpl createBatch(@RequestBody List<product_public_categoryImpl> product_public_categories);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_public_categories/{id}")
    public product_public_categoryImpl update(@PathVariable("id") Integer id,@RequestBody product_public_categoryImpl product_public_category);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_public_categories/updatebatch")
    public product_public_categoryImpl updateBatch(@RequestBody List<product_public_categoryImpl> product_public_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_public_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_public_categories/select")
    public Page<product_public_categoryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_public_categories/{id}/getdraft")
    public product_public_categoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_public_categoryImpl product_public_category);



}
