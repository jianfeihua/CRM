package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_packaging;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_packagingClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_packagingImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_packagingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_packaging] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_packagingClientServiceImpl implements Iproduct_packagingClientService {

    product_packagingFeignClient product_packagingFeignClient;

    @Autowired
    public product_packagingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_packagingFeignClient = nameBuilder.target(product_packagingFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_packagingFeignClient = nameBuilder.target(product_packagingFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_packaging createModel() {
		return new product_packagingImpl();
	}


    public void updateBatch(List<Iproduct_packaging> product_packagings){
        if(product_packagings!=null){
            List<product_packagingImpl> list = new ArrayList<product_packagingImpl>();
            for(Iproduct_packaging iproduct_packaging :product_packagings){
                list.add((product_packagingImpl)iproduct_packaging) ;
            }
            product_packagingFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iproduct_packaging> product_packagings){
        if(product_packagings!=null){
            List<product_packagingImpl> list = new ArrayList<product_packagingImpl>();
            for(Iproduct_packaging iproduct_packaging :product_packagings){
                list.add((product_packagingImpl)iproduct_packaging) ;
            }
            product_packagingFeignClient.createBatch(list) ;
        }
    }


    public void get(Iproduct_packaging product_packaging){
        Iproduct_packaging clientModel = product_packagingFeignClient.get(product_packaging.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_packaging.getClass(), false);
        copier.copy(clientModel, product_packaging, null);
    }


    public void removeBatch(List<Iproduct_packaging> product_packagings){
        if(product_packagings!=null){
            List<product_packagingImpl> list = new ArrayList<product_packagingImpl>();
            for(Iproduct_packaging iproduct_packaging :product_packagings){
                list.add((product_packagingImpl)iproduct_packaging) ;
            }
            product_packagingFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iproduct_packaging product_packaging){
        Iproduct_packaging clientModel = product_packagingFeignClient.create((product_packagingImpl)product_packaging) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_packaging.getClass(), false);
        copier.copy(clientModel, product_packaging, null);
    }


    public void remove(Iproduct_packaging product_packaging){
        product_packagingFeignClient.remove(product_packaging.getId()) ;
    }


    public void update(Iproduct_packaging product_packaging){
        Iproduct_packaging clientModel = product_packagingFeignClient.update(product_packaging.getId(),(product_packagingImpl)product_packaging) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_packaging.getClass(), false);
        copier.copy(clientModel, product_packaging, null);
    }


    public Page<Iproduct_packaging> fetchDefault(SearchContext context){
        Page<product_packagingImpl> page = this.product_packagingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iproduct_packaging> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_packaging product_packaging){
        Iproduct_packaging clientModel = product_packagingFeignClient.getDraft(product_packaging.getId(),(product_packagingImpl)product_packaging) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_packaging.getClass(), false);
        copier.copy(clientModel, product_packaging, null);
    }



}

