package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_image;
import cn.ibizlab.odoo.client.odoo_product.model.product_imageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_image] 服务对象接口
 */
public interface product_imageFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_images")
    public product_imageImpl create(@RequestBody product_imageImpl product_image);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_images/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_images/{id}")
    public product_imageImpl update(@PathVariable("id") Integer id,@RequestBody product_imageImpl product_image);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_images/updatebatch")
    public product_imageImpl updateBatch(@RequestBody List<product_imageImpl> product_images);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_images/fetchdefault")
    public Page<product_imageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_images/removebatch")
    public product_imageImpl removeBatch(@RequestBody List<product_imageImpl> product_images);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_images/{id}")
    public product_imageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_images/createbatch")
    public product_imageImpl createBatch(@RequestBody List<product_imageImpl> product_images);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_images/select")
    public Page<product_imageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_images/{id}/getdraft")
    public product_imageImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_imageImpl product_image);



}
