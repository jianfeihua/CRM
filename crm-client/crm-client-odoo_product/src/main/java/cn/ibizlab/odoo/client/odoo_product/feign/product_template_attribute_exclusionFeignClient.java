package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_exclusion;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_exclusionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_template_attribute_exclusion] 服务对象接口
 */
public interface product_template_attribute_exclusionFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_exclusions/updatebatch")
    public product_template_attribute_exclusionImpl updateBatch(@RequestBody List<product_template_attribute_exclusionImpl> product_template_attribute_exclusions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_exclusions/fetchdefault")
    public Page<product_template_attribute_exclusionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_exclusions/{id}")
    public product_template_attribute_exclusionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_exclusions")
    public product_template_attribute_exclusionImpl create(@RequestBody product_template_attribute_exclusionImpl product_template_attribute_exclusion);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_exclusions/{id}")
    public product_template_attribute_exclusionImpl update(@PathVariable("id") Integer id,@RequestBody product_template_attribute_exclusionImpl product_template_attribute_exclusion);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_exclusions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_exclusions/removebatch")
    public product_template_attribute_exclusionImpl removeBatch(@RequestBody List<product_template_attribute_exclusionImpl> product_template_attribute_exclusions);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_exclusions/createbatch")
    public product_template_attribute_exclusionImpl createBatch(@RequestBody List<product_template_attribute_exclusionImpl> product_template_attribute_exclusions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_exclusions/select")
    public Page<product_template_attribute_exclusionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_exclusions/{id}/getdraft")
    public product_template_attribute_exclusionImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_template_attribute_exclusionImpl product_template_attribute_exclusion);



}
