package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_template;
import cn.ibizlab.odoo.client.odoo_product.model.product_templateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_template] 服务对象接口
 */
public interface product_templateFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_templates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_templates/fetchdefault")
    public Page<product_templateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_templates")
    public product_templateImpl create(@RequestBody product_templateImpl product_template);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_templates/{id}")
    public product_templateImpl update(@PathVariable("id") Integer id,@RequestBody product_templateImpl product_template);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_templates/removebatch")
    public product_templateImpl removeBatch(@RequestBody List<product_templateImpl> product_templates);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_templates/updatebatch")
    public product_templateImpl updateBatch(@RequestBody List<product_templateImpl> product_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_templates/{id}")
    public product_templateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_templates/createbatch")
    public product_templateImpl createBatch(@RequestBody List<product_templateImpl> product_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_templates/select")
    public Page<product_templateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_templates/{id}/getdraft")
    public product_templateImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_templateImpl product_template);



}
