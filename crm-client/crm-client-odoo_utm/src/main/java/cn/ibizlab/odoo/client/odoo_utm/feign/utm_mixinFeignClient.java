package cn.ibizlab.odoo.client.odoo_utm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iutm_mixin;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[utm_mixin] 服务对象接口
 */
public interface utm_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_mixins/createbatch")
    public utm_mixinImpl createBatch(@RequestBody List<utm_mixinImpl> utm_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_mixins/removebatch")
    public utm_mixinImpl removeBatch(@RequestBody List<utm_mixinImpl> utm_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_mixins/updatebatch")
    public utm_mixinImpl updateBatch(@RequestBody List<utm_mixinImpl> utm_mixins);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_mixins")
    public utm_mixinImpl create(@RequestBody utm_mixinImpl utm_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_mixins/{id}")
    public utm_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_mixins/fetchdefault")
    public Page<utm_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_mixins/{id}")
    public utm_mixinImpl update(@PathVariable("id") Integer id,@RequestBody utm_mixinImpl utm_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_mixins/select")
    public Page<utm_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_mixins/{id}/getdraft")
    public utm_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody utm_mixinImpl utm_mixin);



}
