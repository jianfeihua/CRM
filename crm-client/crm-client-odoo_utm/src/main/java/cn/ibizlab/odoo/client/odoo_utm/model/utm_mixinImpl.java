package cn.ibizlab.odoo.client.odoo_utm.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iutm_mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[utm_mixin] 对象
 */
public class utm_mixinImpl implements Iutm_mixin,Serializable{

    /**
     * 营销
     */
    public Integer campaign_id;

    @JsonIgnore
    public boolean campaign_idDirtyFlag;
    
    /**
     * 营销
     */
    public String campaign_id_text;

    @JsonIgnore
    public boolean campaign_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 媒体
     */
    public Integer medium_id;

    @JsonIgnore
    public boolean medium_idDirtyFlag;
    
    /**
     * 媒体
     */
    public String medium_id_text;

    @JsonIgnore
    public boolean medium_id_textDirtyFlag;
    
    /**
     * 来源
     */
    public Integer source_id;

    @JsonIgnore
    public boolean source_idDirtyFlag;
    
    /**
     * 来源
     */
    public String source_id_text;

    @JsonIgnore
    public boolean source_id_textDirtyFlag;
    
    /**
     * 修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }   

    /**
     * 获取 [修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
