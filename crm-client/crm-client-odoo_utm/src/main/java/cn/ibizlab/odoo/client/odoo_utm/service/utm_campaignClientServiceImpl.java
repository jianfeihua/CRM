package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_campaign;
import cn.ibizlab.odoo.client.odoo_utm.config.odoo_utmClientProperties;
import cn.ibizlab.odoo.core.client.service.Iutm_campaignClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_campaignImpl;
import cn.ibizlab.odoo.client.odoo_utm.feign.utm_campaignFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[utm_campaign] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class utm_campaignClientServiceImpl implements Iutm_campaignClientService {

    utm_campaignFeignClient utm_campaignFeignClient;

    @Autowired
    public utm_campaignClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_utmClientProperties odoo_utmClientProperties) {
        if (odoo_utmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_campaignFeignClient = nameBuilder.target(utm_campaignFeignClient.class,"http://"+odoo_utmClientProperties.getServiceId()+"/") ;
		}else if (odoo_utmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_campaignFeignClient = nameBuilder.target(utm_campaignFeignClient.class,odoo_utmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iutm_campaign createModel() {
		return new utm_campaignImpl();
	}


    public void remove(Iutm_campaign utm_campaign){
        utm_campaignFeignClient.remove(utm_campaign.getId()) ;
    }


    public void get(Iutm_campaign utm_campaign){
        Iutm_campaign clientModel = utm_campaignFeignClient.get(utm_campaign.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_campaign.getClass(), false);
        copier.copy(clientModel, utm_campaign, null);
    }


    public void update(Iutm_campaign utm_campaign){
        Iutm_campaign clientModel = utm_campaignFeignClient.update(utm_campaign.getId(),(utm_campaignImpl)utm_campaign) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_campaign.getClass(), false);
        copier.copy(clientModel, utm_campaign, null);
    }


    public void createBatch(List<Iutm_campaign> utm_campaigns){
        if(utm_campaigns!=null){
            List<utm_campaignImpl> list = new ArrayList<utm_campaignImpl>();
            for(Iutm_campaign iutm_campaign :utm_campaigns){
                list.add((utm_campaignImpl)iutm_campaign) ;
            }
            utm_campaignFeignClient.createBatch(list) ;
        }
    }


    public void create(Iutm_campaign utm_campaign){
        Iutm_campaign clientModel = utm_campaignFeignClient.create((utm_campaignImpl)utm_campaign) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_campaign.getClass(), false);
        copier.copy(clientModel, utm_campaign, null);
    }


    public void removeBatch(List<Iutm_campaign> utm_campaigns){
        if(utm_campaigns!=null){
            List<utm_campaignImpl> list = new ArrayList<utm_campaignImpl>();
            for(Iutm_campaign iutm_campaign :utm_campaigns){
                list.add((utm_campaignImpl)iutm_campaign) ;
            }
            utm_campaignFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iutm_campaign> utm_campaigns){
        if(utm_campaigns!=null){
            List<utm_campaignImpl> list = new ArrayList<utm_campaignImpl>();
            for(Iutm_campaign iutm_campaign :utm_campaigns){
                list.add((utm_campaignImpl)iutm_campaign) ;
            }
            utm_campaignFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iutm_campaign> fetchDefault(SearchContext context){
        Page<utm_campaignImpl> page = this.utm_campaignFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iutm_campaign> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iutm_campaign utm_campaign){
        Iutm_campaign clientModel = utm_campaignFeignClient.getDraft(utm_campaign.getId(),(utm_campaignImpl)utm_campaign) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_campaign.getClass(), false);
        copier.copy(clientModel, utm_campaign, null);
    }



}

