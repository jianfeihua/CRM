package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_source;
import cn.ibizlab.odoo.client.odoo_utm.config.odoo_utmClientProperties;
import cn.ibizlab.odoo.core.client.service.Iutm_sourceClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_sourceImpl;
import cn.ibizlab.odoo.client.odoo_utm.feign.utm_sourceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[utm_source] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class utm_sourceClientServiceImpl implements Iutm_sourceClientService {

    utm_sourceFeignClient utm_sourceFeignClient;

    @Autowired
    public utm_sourceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_utmClientProperties odoo_utmClientProperties) {
        if (odoo_utmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_sourceFeignClient = nameBuilder.target(utm_sourceFeignClient.class,"http://"+odoo_utmClientProperties.getServiceId()+"/") ;
		}else if (odoo_utmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_sourceFeignClient = nameBuilder.target(utm_sourceFeignClient.class,odoo_utmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iutm_source createModel() {
		return new utm_sourceImpl();
	}


    public void createBatch(List<Iutm_source> utm_sources){
        if(utm_sources!=null){
            List<utm_sourceImpl> list = new ArrayList<utm_sourceImpl>();
            for(Iutm_source iutm_source :utm_sources){
                list.add((utm_sourceImpl)iutm_source) ;
            }
            utm_sourceFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iutm_source> utm_sources){
        if(utm_sources!=null){
            List<utm_sourceImpl> list = new ArrayList<utm_sourceImpl>();
            for(Iutm_source iutm_source :utm_sources){
                list.add((utm_sourceImpl)iutm_source) ;
            }
            utm_sourceFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iutm_source utm_source){
        Iutm_source clientModel = utm_sourceFeignClient.create((utm_sourceImpl)utm_source) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_source.getClass(), false);
        copier.copy(clientModel, utm_source, null);
    }


    public void get(Iutm_source utm_source){
        Iutm_source clientModel = utm_sourceFeignClient.get(utm_source.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_source.getClass(), false);
        copier.copy(clientModel, utm_source, null);
    }


    public void removeBatch(List<Iutm_source> utm_sources){
        if(utm_sources!=null){
            List<utm_sourceImpl> list = new ArrayList<utm_sourceImpl>();
            for(Iutm_source iutm_source :utm_sources){
                list.add((utm_sourceImpl)iutm_source) ;
            }
            utm_sourceFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iutm_source utm_source){
        utm_sourceFeignClient.remove(utm_source.getId()) ;
    }


    public Page<Iutm_source> fetchDefault(SearchContext context){
        Page<utm_sourceImpl> page = this.utm_sourceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iutm_source utm_source){
        Iutm_source clientModel = utm_sourceFeignClient.update(utm_source.getId(),(utm_sourceImpl)utm_source) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_source.getClass(), false);
        copier.copy(clientModel, utm_source, null);
    }


    public Page<Iutm_source> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iutm_source utm_source){
        Iutm_source clientModel = utm_sourceFeignClient.getDraft(utm_source.getId(),(utm_sourceImpl)utm_source) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_source.getClass(), false);
        copier.copy(clientModel, utm_source, null);
    }



}

