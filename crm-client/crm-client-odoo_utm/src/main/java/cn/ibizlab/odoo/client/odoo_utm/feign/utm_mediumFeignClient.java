package cn.ibizlab.odoo.client.odoo_utm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iutm_medium;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_mediumImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[utm_medium] 服务对象接口
 */
public interface utm_mediumFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_media/removebatch")
    public utm_mediumImpl removeBatch(@RequestBody List<utm_mediumImpl> utm_media);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_media/createbatch")
    public utm_mediumImpl createBatch(@RequestBody List<utm_mediumImpl> utm_media);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_media")
    public utm_mediumImpl create(@RequestBody utm_mediumImpl utm_medium);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_media/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_media/updatebatch")
    public utm_mediumImpl updateBatch(@RequestBody List<utm_mediumImpl> utm_media);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_media/{id}")
    public utm_mediumImpl update(@PathVariable("id") Integer id,@RequestBody utm_mediumImpl utm_medium);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_media/{id}")
    public utm_mediumImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_media/fetchdefault")
    public Page<utm_mediumImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_media/select")
    public Page<utm_mediumImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_media/{id}/getdraft")
    public utm_mediumImpl getDraft(@PathVariable("id") Integer id,@RequestBody utm_mediumImpl utm_medium);



}
