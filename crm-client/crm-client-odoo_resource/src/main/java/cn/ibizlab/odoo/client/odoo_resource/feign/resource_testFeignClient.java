package cn.ibizlab.odoo.client.odoo_resource.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iresource_test;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_testImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[resource_test] 服务对象接口
 */
public interface resource_testFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_tests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_tests/createbatch")
    public resource_testImpl createBatch(@RequestBody List<resource_testImpl> resource_tests);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_tests")
    public resource_testImpl create(@RequestBody resource_testImpl resource_test);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_tests/fetchdefault")
    public Page<resource_testImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_tests/{id}")
    public resource_testImpl update(@PathVariable("id") Integer id,@RequestBody resource_testImpl resource_test);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_tests/{id}")
    public resource_testImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_tests/removebatch")
    public resource_testImpl removeBatch(@RequestBody List<resource_testImpl> resource_tests);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_tests/updatebatch")
    public resource_testImpl updateBatch(@RequestBody List<resource_testImpl> resource_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_tests/select")
    public Page<resource_testImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_tests/{id}/getdraft")
    public resource_testImpl getDraft(@PathVariable("id") Integer id,@RequestBody resource_testImpl resource_test);



}
