package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_attendance;
import cn.ibizlab.odoo.client.odoo_resource.config.odoo_resourceClientProperties;
import cn.ibizlab.odoo.core.client.service.Iresource_calendar_attendanceClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendar_attendanceImpl;
import cn.ibizlab.odoo.client.odoo_resource.feign.resource_calendar_attendanceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[resource_calendar_attendance] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class resource_calendar_attendanceClientServiceImpl implements Iresource_calendar_attendanceClientService {

    resource_calendar_attendanceFeignClient resource_calendar_attendanceFeignClient;

    @Autowired
    public resource_calendar_attendanceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_resourceClientProperties odoo_resourceClientProperties) {
        if (odoo_resourceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_calendar_attendanceFeignClient = nameBuilder.target(resource_calendar_attendanceFeignClient.class,"http://"+odoo_resourceClientProperties.getServiceId()+"/") ;
		}else if (odoo_resourceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_calendar_attendanceFeignClient = nameBuilder.target(resource_calendar_attendanceFeignClient.class,odoo_resourceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iresource_calendar_attendance createModel() {
		return new resource_calendar_attendanceImpl();
	}


    public void removeBatch(List<Iresource_calendar_attendance> resource_calendar_attendances){
        if(resource_calendar_attendances!=null){
            List<resource_calendar_attendanceImpl> list = new ArrayList<resource_calendar_attendanceImpl>();
            for(Iresource_calendar_attendance iresource_calendar_attendance :resource_calendar_attendances){
                list.add((resource_calendar_attendanceImpl)iresource_calendar_attendance) ;
            }
            resource_calendar_attendanceFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iresource_calendar_attendance resource_calendar_attendance){
        Iresource_calendar_attendance clientModel = resource_calendar_attendanceFeignClient.update(resource_calendar_attendance.getId(),(resource_calendar_attendanceImpl)resource_calendar_attendance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_attendance.getClass(), false);
        copier.copy(clientModel, resource_calendar_attendance, null);
    }


    public void get(Iresource_calendar_attendance resource_calendar_attendance){
        Iresource_calendar_attendance clientModel = resource_calendar_attendanceFeignClient.get(resource_calendar_attendance.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_attendance.getClass(), false);
        copier.copy(clientModel, resource_calendar_attendance, null);
    }


    public void remove(Iresource_calendar_attendance resource_calendar_attendance){
        resource_calendar_attendanceFeignClient.remove(resource_calendar_attendance.getId()) ;
    }


    public void updateBatch(List<Iresource_calendar_attendance> resource_calendar_attendances){
        if(resource_calendar_attendances!=null){
            List<resource_calendar_attendanceImpl> list = new ArrayList<resource_calendar_attendanceImpl>();
            for(Iresource_calendar_attendance iresource_calendar_attendance :resource_calendar_attendances){
                list.add((resource_calendar_attendanceImpl)iresource_calendar_attendance) ;
            }
            resource_calendar_attendanceFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iresource_calendar_attendance resource_calendar_attendance){
        Iresource_calendar_attendance clientModel = resource_calendar_attendanceFeignClient.create((resource_calendar_attendanceImpl)resource_calendar_attendance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_attendance.getClass(), false);
        copier.copy(clientModel, resource_calendar_attendance, null);
    }


    public Page<Iresource_calendar_attendance> fetchDefault(SearchContext context){
        Page<resource_calendar_attendanceImpl> page = this.resource_calendar_attendanceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iresource_calendar_attendance> resource_calendar_attendances){
        if(resource_calendar_attendances!=null){
            List<resource_calendar_attendanceImpl> list = new ArrayList<resource_calendar_attendanceImpl>();
            for(Iresource_calendar_attendance iresource_calendar_attendance :resource_calendar_attendances){
                list.add((resource_calendar_attendanceImpl)iresource_calendar_attendance) ;
            }
            resource_calendar_attendanceFeignClient.createBatch(list) ;
        }
    }


    public Page<Iresource_calendar_attendance> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iresource_calendar_attendance resource_calendar_attendance){
        Iresource_calendar_attendance clientModel = resource_calendar_attendanceFeignClient.getDraft(resource_calendar_attendance.getId(),(resource_calendar_attendanceImpl)resource_calendar_attendance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_attendance.getClass(), false);
        copier.copy(clientModel, resource_calendar_attendance, null);
    }



}

