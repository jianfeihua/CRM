package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_leaves;
import cn.ibizlab.odoo.client.odoo_resource.config.odoo_resourceClientProperties;
import cn.ibizlab.odoo.core.client.service.Iresource_calendar_leavesClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendar_leavesImpl;
import cn.ibizlab.odoo.client.odoo_resource.feign.resource_calendar_leavesFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[resource_calendar_leaves] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class resource_calendar_leavesClientServiceImpl implements Iresource_calendar_leavesClientService {

    resource_calendar_leavesFeignClient resource_calendar_leavesFeignClient;

    @Autowired
    public resource_calendar_leavesClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_resourceClientProperties odoo_resourceClientProperties) {
        if (odoo_resourceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_calendar_leavesFeignClient = nameBuilder.target(resource_calendar_leavesFeignClient.class,"http://"+odoo_resourceClientProperties.getServiceId()+"/") ;
		}else if (odoo_resourceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_calendar_leavesFeignClient = nameBuilder.target(resource_calendar_leavesFeignClient.class,odoo_resourceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iresource_calendar_leaves createModel() {
		return new resource_calendar_leavesImpl();
	}


    public void updateBatch(List<Iresource_calendar_leaves> resource_calendar_leaves){
        if(resource_calendar_leaves!=null){
            List<resource_calendar_leavesImpl> list = new ArrayList<resource_calendar_leavesImpl>();
            for(Iresource_calendar_leaves iresource_calendar_leaves :resource_calendar_leaves){
                list.add((resource_calendar_leavesImpl)iresource_calendar_leaves) ;
            }
            resource_calendar_leavesFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iresource_calendar_leaves resource_calendar_leaves){
        Iresource_calendar_leaves clientModel = resource_calendar_leavesFeignClient.update(resource_calendar_leaves.getId(),(resource_calendar_leavesImpl)resource_calendar_leaves) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_leaves.getClass(), false);
        copier.copy(clientModel, resource_calendar_leaves, null);
    }


    public void remove(Iresource_calendar_leaves resource_calendar_leaves){
        resource_calendar_leavesFeignClient.remove(resource_calendar_leaves.getId()) ;
    }


    public void get(Iresource_calendar_leaves resource_calendar_leaves){
        Iresource_calendar_leaves clientModel = resource_calendar_leavesFeignClient.get(resource_calendar_leaves.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_leaves.getClass(), false);
        copier.copy(clientModel, resource_calendar_leaves, null);
    }


    public Page<Iresource_calendar_leaves> fetchDefault(SearchContext context){
        Page<resource_calendar_leavesImpl> page = this.resource_calendar_leavesFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iresource_calendar_leaves resource_calendar_leaves){
        Iresource_calendar_leaves clientModel = resource_calendar_leavesFeignClient.create((resource_calendar_leavesImpl)resource_calendar_leaves) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_leaves.getClass(), false);
        copier.copy(clientModel, resource_calendar_leaves, null);
    }


    public void createBatch(List<Iresource_calendar_leaves> resource_calendar_leaves){
        if(resource_calendar_leaves!=null){
            List<resource_calendar_leavesImpl> list = new ArrayList<resource_calendar_leavesImpl>();
            for(Iresource_calendar_leaves iresource_calendar_leaves :resource_calendar_leaves){
                list.add((resource_calendar_leavesImpl)iresource_calendar_leaves) ;
            }
            resource_calendar_leavesFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iresource_calendar_leaves> resource_calendar_leaves){
        if(resource_calendar_leaves!=null){
            List<resource_calendar_leavesImpl> list = new ArrayList<resource_calendar_leavesImpl>();
            for(Iresource_calendar_leaves iresource_calendar_leaves :resource_calendar_leaves){
                list.add((resource_calendar_leavesImpl)iresource_calendar_leaves) ;
            }
            resource_calendar_leavesFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iresource_calendar_leaves> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iresource_calendar_leaves resource_calendar_leaves){
        Iresource_calendar_leaves clientModel = resource_calendar_leavesFeignClient.getDraft(resource_calendar_leaves.getId(),(resource_calendar_leavesImpl)resource_calendar_leaves) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar_leaves.getClass(), false);
        copier.copy(clientModel, resource_calendar_leaves, null);
    }



}

