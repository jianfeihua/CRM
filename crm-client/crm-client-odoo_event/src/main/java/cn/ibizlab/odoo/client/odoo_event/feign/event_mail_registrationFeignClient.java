package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_mail_registration;
import cn.ibizlab.odoo.client.odoo_event.model.event_mail_registrationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_mail_registration] 服务对象接口
 */
public interface event_mail_registrationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mail_registrations/{id}")
    public event_mail_registrationImpl update(@PathVariable("id") Integer id,@RequestBody event_mail_registrationImpl event_mail_registration);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mail_registrations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mail_registrations/createbatch")
    public event_mail_registrationImpl createBatch(@RequestBody List<event_mail_registrationImpl> event_mail_registrations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mail_registrations/removebatch")
    public event_mail_registrationImpl removeBatch(@RequestBody List<event_mail_registrationImpl> event_mail_registrations);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mail_registrations/updatebatch")
    public event_mail_registrationImpl updateBatch(@RequestBody List<event_mail_registrationImpl> event_mail_registrations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mail_registrations")
    public event_mail_registrationImpl create(@RequestBody event_mail_registrationImpl event_mail_registration);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mail_registrations/fetchdefault")
    public Page<event_mail_registrationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mail_registrations/{id}")
    public event_mail_registrationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mail_registrations/select")
    public Page<event_mail_registrationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mail_registrations/{id}/getdraft")
    public event_mail_registrationImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_mail_registrationImpl event_mail_registration);



}
