package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_event_ticket;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_event_ticketClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_event_ticketImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_event_ticketFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_event_ticket] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_event_ticketClientServiceImpl implements Ievent_event_ticketClientService {

    event_event_ticketFeignClient event_event_ticketFeignClient;

    @Autowired
    public event_event_ticketClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_event_ticketFeignClient = nameBuilder.target(event_event_ticketFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_event_ticketFeignClient = nameBuilder.target(event_event_ticketFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_event_ticket createModel() {
		return new event_event_ticketImpl();
	}


    public void create(Ievent_event_ticket event_event_ticket){
        Ievent_event_ticket clientModel = event_event_ticketFeignClient.create((event_event_ticketImpl)event_event_ticket) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event_ticket.getClass(), false);
        copier.copy(clientModel, event_event_ticket, null);
    }


    public void get(Ievent_event_ticket event_event_ticket){
        Ievent_event_ticket clientModel = event_event_ticketFeignClient.get(event_event_ticket.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event_ticket.getClass(), false);
        copier.copy(clientModel, event_event_ticket, null);
    }


    public void update(Ievent_event_ticket event_event_ticket){
        Ievent_event_ticket clientModel = event_event_ticketFeignClient.update(event_event_ticket.getId(),(event_event_ticketImpl)event_event_ticket) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event_ticket.getClass(), false);
        copier.copy(clientModel, event_event_ticket, null);
    }


    public void removeBatch(List<Ievent_event_ticket> event_event_tickets){
        if(event_event_tickets!=null){
            List<event_event_ticketImpl> list = new ArrayList<event_event_ticketImpl>();
            for(Ievent_event_ticket ievent_event_ticket :event_event_tickets){
                list.add((event_event_ticketImpl)ievent_event_ticket) ;
            }
            event_event_ticketFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ievent_event_ticket> fetchDefault(SearchContext context){
        Page<event_event_ticketImpl> page = this.event_event_ticketFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ievent_event_ticket event_event_ticket){
        event_event_ticketFeignClient.remove(event_event_ticket.getId()) ;
    }


    public void createBatch(List<Ievent_event_ticket> event_event_tickets){
        if(event_event_tickets!=null){
            List<event_event_ticketImpl> list = new ArrayList<event_event_ticketImpl>();
            for(Ievent_event_ticket ievent_event_ticket :event_event_tickets){
                list.add((event_event_ticketImpl)ievent_event_ticket) ;
            }
            event_event_ticketFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ievent_event_ticket> event_event_tickets){
        if(event_event_tickets!=null){
            List<event_event_ticketImpl> list = new ArrayList<event_event_ticketImpl>();
            for(Ievent_event_ticket ievent_event_ticket :event_event_tickets){
                list.add((event_event_ticketImpl)ievent_event_ticket) ;
            }
            event_event_ticketFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ievent_event_ticket> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_event_ticket event_event_ticket){
        Ievent_event_ticket clientModel = event_event_ticketFeignClient.getDraft(event_event_ticket.getId(),(event_event_ticketImpl)event_event_ticket) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event_ticket.getClass(), false);
        copier.copy(clientModel, event_event_ticket, null);
    }



}

