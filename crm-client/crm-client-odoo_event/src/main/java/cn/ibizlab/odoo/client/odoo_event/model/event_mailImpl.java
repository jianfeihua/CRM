package cn.ibizlab.odoo.client.odoo_event.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ievent_mail;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[event_mail] 对象
 */
public class event_mailImpl implements Ievent_mail,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 已汇
     */
    public String done;

    @JsonIgnore
    public boolean doneDirtyFlag;
    
    /**
     * 活动
     */
    public Integer event_id;

    @JsonIgnore
    public boolean event_idDirtyFlag;
    
    /**
     * 活动
     */
    public String event_id_text;

    @JsonIgnore
    public boolean event_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 间隔
     */
    public Integer interval_nbr;

    @JsonIgnore
    public boolean interval_nbrDirtyFlag;
    
    /**
     * 触发器
     */
    public String interval_type;

    @JsonIgnore
    public boolean interval_typeDirtyFlag;
    
    /**
     * 单位
     */
    public String interval_unit;

    @JsonIgnore
    public boolean interval_unitDirtyFlag;
    
    /**
     * 邮箱注册
     */
    public String mail_registration_ids;

    @JsonIgnore
    public boolean mail_registration_idsDirtyFlag;
    
    /**
     * 在事件上发送EMail
     */
    public String mail_sent;

    @JsonIgnore
    public boolean mail_sentDirtyFlag;
    
    /**
     * 计划发出的邮件
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp scheduled_date;

    @JsonIgnore
    public boolean scheduled_dateDirtyFlag;
    
    /**
     * 现实顺序
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * EMail模板
     */
    public Integer template_id;

    @JsonIgnore
    public boolean template_idDirtyFlag;
    
    /**
     * EMail模板
     */
    public String template_id_text;

    @JsonIgnore
    public boolean template_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [已汇]
     */
    @JsonProperty("done")
    public String getDone(){
        return this.done ;
    }

    /**
     * 设置 [已汇]
     */
    @JsonProperty("done")
    public void setDone(String  done){
        this.done = done ;
        this.doneDirtyFlag = true ;
    }

     /**
     * 获取 [已汇]脏标记
     */
    @JsonIgnore
    public boolean getDoneDirtyFlag(){
        return this.doneDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_id")
    public Integer getEvent_id(){
        return this.event_id ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_id")
    public void setEvent_id(Integer  event_id){
        this.event_id = event_id ;
        this.event_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_idDirtyFlag(){
        return this.event_idDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_id_text")
    public String getEvent_id_text(){
        return this.event_id_text ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_id_text")
    public void setEvent_id_text(String  event_id_text){
        this.event_id_text = event_id_text ;
        this.event_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_id_textDirtyFlag(){
        return this.event_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [间隔]
     */
    @JsonProperty("interval_nbr")
    public Integer getInterval_nbr(){
        return this.interval_nbr ;
    }

    /**
     * 设置 [间隔]
     */
    @JsonProperty("interval_nbr")
    public void setInterval_nbr(Integer  interval_nbr){
        this.interval_nbr = interval_nbr ;
        this.interval_nbrDirtyFlag = true ;
    }

     /**
     * 获取 [间隔]脏标记
     */
    @JsonIgnore
    public boolean getInterval_nbrDirtyFlag(){
        return this.interval_nbrDirtyFlag ;
    }   

    /**
     * 获取 [触发器]
     */
    @JsonProperty("interval_type")
    public String getInterval_type(){
        return this.interval_type ;
    }

    /**
     * 设置 [触发器]
     */
    @JsonProperty("interval_type")
    public void setInterval_type(String  interval_type){
        this.interval_type = interval_type ;
        this.interval_typeDirtyFlag = true ;
    }

     /**
     * 获取 [触发器]脏标记
     */
    @JsonIgnore
    public boolean getInterval_typeDirtyFlag(){
        return this.interval_typeDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("interval_unit")
    public String getInterval_unit(){
        return this.interval_unit ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("interval_unit")
    public void setInterval_unit(String  interval_unit){
        this.interval_unit = interval_unit ;
        this.interval_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getInterval_unitDirtyFlag(){
        return this.interval_unitDirtyFlag ;
    }   

    /**
     * 获取 [邮箱注册]
     */
    @JsonProperty("mail_registration_ids")
    public String getMail_registration_ids(){
        return this.mail_registration_ids ;
    }

    /**
     * 设置 [邮箱注册]
     */
    @JsonProperty("mail_registration_ids")
    public void setMail_registration_ids(String  mail_registration_ids){
        this.mail_registration_ids = mail_registration_ids ;
        this.mail_registration_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮箱注册]脏标记
     */
    @JsonIgnore
    public boolean getMail_registration_idsDirtyFlag(){
        return this.mail_registration_idsDirtyFlag ;
    }   

    /**
     * 获取 [在事件上发送EMail]
     */
    @JsonProperty("mail_sent")
    public String getMail_sent(){
        return this.mail_sent ;
    }

    /**
     * 设置 [在事件上发送EMail]
     */
    @JsonProperty("mail_sent")
    public void setMail_sent(String  mail_sent){
        this.mail_sent = mail_sent ;
        this.mail_sentDirtyFlag = true ;
    }

     /**
     * 获取 [在事件上发送EMail]脏标记
     */
    @JsonIgnore
    public boolean getMail_sentDirtyFlag(){
        return this.mail_sentDirtyFlag ;
    }   

    /**
     * 获取 [计划发出的邮件]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return this.scheduled_date ;
    }

    /**
     * 设置 [计划发出的邮件]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

     /**
     * 获取 [计划发出的邮件]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return this.scheduled_dateDirtyFlag ;
    }   

    /**
     * 获取 [现实顺序]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [现实顺序]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [现实顺序]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return this.template_id ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return this.template_idDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return this.template_id_text ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return this.template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
