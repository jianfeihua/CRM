package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_event;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_eventClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_eventImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_eventFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_event] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_eventClientServiceImpl implements Ievent_eventClientService {

    event_eventFeignClient event_eventFeignClient;

    @Autowired
    public event_eventClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_eventFeignClient = nameBuilder.target(event_eventFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_eventFeignClient = nameBuilder.target(event_eventFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_event createModel() {
		return new event_eventImpl();
	}


    public void create(Ievent_event event_event){
        Ievent_event clientModel = event_eventFeignClient.create((event_eventImpl)event_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event.getClass(), false);
        copier.copy(clientModel, event_event, null);
    }


    public void get(Ievent_event event_event){
        Ievent_event clientModel = event_eventFeignClient.get(event_event.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event.getClass(), false);
        copier.copy(clientModel, event_event, null);
    }


    public Page<Ievent_event> fetchDefault(SearchContext context){
        Page<event_eventImpl> page = this.event_eventFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ievent_event> event_events){
        if(event_events!=null){
            List<event_eventImpl> list = new ArrayList<event_eventImpl>();
            for(Ievent_event ievent_event :event_events){
                list.add((event_eventImpl)ievent_event) ;
            }
            event_eventFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ievent_event> event_events){
        if(event_events!=null){
            List<event_eventImpl> list = new ArrayList<event_eventImpl>();
            for(Ievent_event ievent_event :event_events){
                list.add((event_eventImpl)ievent_event) ;
            }
            event_eventFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ievent_event event_event){
        event_eventFeignClient.remove(event_event.getId()) ;
    }


    public void update(Ievent_event event_event){
        Ievent_event clientModel = event_eventFeignClient.update(event_event.getId(),(event_eventImpl)event_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event.getClass(), false);
        copier.copy(clientModel, event_event, null);
    }


    public void removeBatch(List<Ievent_event> event_events){
        if(event_events!=null){
            List<event_eventImpl> list = new ArrayList<event_eventImpl>();
            for(Ievent_event ievent_event :event_events){
                list.add((event_eventImpl)ievent_event) ;
            }
            event_eventFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ievent_event> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_event event_event){
        Ievent_event clientModel = event_eventFeignClient.getDraft(event_event.getId(),(event_eventImpl)event_event) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_event.getClass(), false);
        copier.copy(clientModel, event_event, null);
    }



}

