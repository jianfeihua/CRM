package cn.ibizlab.odoo.client.odoo_event.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ievent_event_ticket;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[event_event_ticket] 对象
 */
public class event_event_ticketImpl implements Ievent_event_ticket,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 销售结束
     */
    public Timestamp deadline;

    @JsonIgnore
    public boolean deadlineDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 活动
     */
    public Integer event_id;

    @JsonIgnore
    public boolean event_idDirtyFlag;
    
    /**
     * 活动
     */
    public String event_id_text;

    @JsonIgnore
    public boolean event_id_textDirtyFlag;
    
    /**
     * 活动类别
     */
    public Integer event_type_id;

    @JsonIgnore
    public boolean event_type_idDirtyFlag;
    
    /**
     * 活动类别
     */
    public String event_type_id_text;

    @JsonIgnore
    public boolean event_type_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 已结束
     */
    public String is_expired;

    @JsonIgnore
    public boolean is_expiredDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 价格
     */
    public Double price;

    @JsonIgnore
    public boolean priceDirtyFlag;
    
    /**
     * 降价
     */
    public Double price_reduce;

    @JsonIgnore
    public boolean price_reduceDirtyFlag;
    
    /**
     * 减税后价格
     */
    public Double price_reduce_taxinc;

    @JsonIgnore
    public boolean price_reduce_taxincDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 登记
     */
    public String registration_ids;

    @JsonIgnore
    public boolean registration_idsDirtyFlag;
    
    /**
     * 可用席位
     */
    public String seats_availability;

    @JsonIgnore
    public boolean seats_availabilityDirtyFlag;
    
    /**
     * 可用席位
     */
    public Integer seats_available;

    @JsonIgnore
    public boolean seats_availableDirtyFlag;
    
    /**
     * 最多可用席位
     */
    public Integer seats_max;

    @JsonIgnore
    public boolean seats_maxDirtyFlag;
    
    /**
     * 预订席位
     */
    public Integer seats_reserved;

    @JsonIgnore
    public boolean seats_reservedDirtyFlag;
    
    /**
     * 未确认的席位预订
     */
    public Integer seats_unconfirmed;

    @JsonIgnore
    public boolean seats_unconfirmedDirtyFlag;
    
    /**
     * 座椅
     */
    public Integer seats_used;

    @JsonIgnore
    public boolean seats_usedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [销售结束]
     */
    @JsonProperty("deadline")
    public Timestamp getDeadline(){
        return this.deadline ;
    }

    /**
     * 设置 [销售结束]
     */
    @JsonProperty("deadline")
    public void setDeadline(Timestamp  deadline){
        this.deadline = deadline ;
        this.deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [销售结束]脏标记
     */
    @JsonIgnore
    public boolean getDeadlineDirtyFlag(){
        return this.deadlineDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_id")
    public Integer getEvent_id(){
        return this.event_id ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_id")
    public void setEvent_id(Integer  event_id){
        this.event_id = event_id ;
        this.event_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_idDirtyFlag(){
        return this.event_idDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_id_text")
    public String getEvent_id_text(){
        return this.event_id_text ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_id_text")
    public void setEvent_id_text(String  event_id_text){
        this.event_id_text = event_id_text ;
        this.event_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_id_textDirtyFlag(){
        return this.event_id_textDirtyFlag ;
    }   

    /**
     * 获取 [活动类别]
     */
    @JsonProperty("event_type_id")
    public Integer getEvent_type_id(){
        return this.event_type_id ;
    }

    /**
     * 设置 [活动类别]
     */
    @JsonProperty("event_type_id")
    public void setEvent_type_id(Integer  event_type_id){
        this.event_type_id = event_type_id ;
        this.event_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动类别]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_idDirtyFlag(){
        return this.event_type_idDirtyFlag ;
    }   

    /**
     * 获取 [活动类别]
     */
    @JsonProperty("event_type_id_text")
    public String getEvent_type_id_text(){
        return this.event_type_id_text ;
    }

    /**
     * 设置 [活动类别]
     */
    @JsonProperty("event_type_id_text")
    public void setEvent_type_id_text(String  event_type_id_text){
        this.event_type_id_text = event_type_id_text ;
        this.event_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动类别]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_id_textDirtyFlag(){
        return this.event_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [已结束]
     */
    @JsonProperty("is_expired")
    public String getIs_expired(){
        return this.is_expired ;
    }

    /**
     * 设置 [已结束]
     */
    @JsonProperty("is_expired")
    public void setIs_expired(String  is_expired){
        this.is_expired = is_expired ;
        this.is_expiredDirtyFlag = true ;
    }

     /**
     * 获取 [已结束]脏标记
     */
    @JsonIgnore
    public boolean getIs_expiredDirtyFlag(){
        return this.is_expiredDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [价格]
     */
    @JsonProperty("price")
    public Double getPrice(){
        return this.price ;
    }

    /**
     * 设置 [价格]
     */
    @JsonProperty("price")
    public void setPrice(Double  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

     /**
     * 获取 [价格]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return this.priceDirtyFlag ;
    }   

    /**
     * 获取 [降价]
     */
    @JsonProperty("price_reduce")
    public Double getPrice_reduce(){
        return this.price_reduce ;
    }

    /**
     * 设置 [降价]
     */
    @JsonProperty("price_reduce")
    public void setPrice_reduce(Double  price_reduce){
        this.price_reduce = price_reduce ;
        this.price_reduceDirtyFlag = true ;
    }

     /**
     * 获取 [降价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduceDirtyFlag(){
        return this.price_reduceDirtyFlag ;
    }   

    /**
     * 获取 [减税后价格]
     */
    @JsonProperty("price_reduce_taxinc")
    public Double getPrice_reduce_taxinc(){
        return this.price_reduce_taxinc ;
    }

    /**
     * 设置 [减税后价格]
     */
    @JsonProperty("price_reduce_taxinc")
    public void setPrice_reduce_taxinc(Double  price_reduce_taxinc){
        this.price_reduce_taxinc = price_reduce_taxinc ;
        this.price_reduce_taxincDirtyFlag = true ;
    }

     /**
     * 获取 [减税后价格]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduce_taxincDirtyFlag(){
        return this.price_reduce_taxincDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [登记]
     */
    @JsonProperty("registration_ids")
    public String getRegistration_ids(){
        return this.registration_ids ;
    }

    /**
     * 设置 [登记]
     */
    @JsonProperty("registration_ids")
    public void setRegistration_ids(String  registration_ids){
        this.registration_ids = registration_ids ;
        this.registration_idsDirtyFlag = true ;
    }

     /**
     * 获取 [登记]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_idsDirtyFlag(){
        return this.registration_idsDirtyFlag ;
    }   

    /**
     * 获取 [可用席位]
     */
    @JsonProperty("seats_availability")
    public String getSeats_availability(){
        return this.seats_availability ;
    }

    /**
     * 设置 [可用席位]
     */
    @JsonProperty("seats_availability")
    public void setSeats_availability(String  seats_availability){
        this.seats_availability = seats_availability ;
        this.seats_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [可用席位]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availabilityDirtyFlag(){
        return this.seats_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [可用席位]
     */
    @JsonProperty("seats_available")
    public Integer getSeats_available(){
        return this.seats_available ;
    }

    /**
     * 设置 [可用席位]
     */
    @JsonProperty("seats_available")
    public void setSeats_available(Integer  seats_available){
        this.seats_available = seats_available ;
        this.seats_availableDirtyFlag = true ;
    }

     /**
     * 获取 [可用席位]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availableDirtyFlag(){
        return this.seats_availableDirtyFlag ;
    }   

    /**
     * 获取 [最多可用席位]
     */
    @JsonProperty("seats_max")
    public Integer getSeats_max(){
        return this.seats_max ;
    }

    /**
     * 设置 [最多可用席位]
     */
    @JsonProperty("seats_max")
    public void setSeats_max(Integer  seats_max){
        this.seats_max = seats_max ;
        this.seats_maxDirtyFlag = true ;
    }

     /**
     * 获取 [最多可用席位]脏标记
     */
    @JsonIgnore
    public boolean getSeats_maxDirtyFlag(){
        return this.seats_maxDirtyFlag ;
    }   

    /**
     * 获取 [预订席位]
     */
    @JsonProperty("seats_reserved")
    public Integer getSeats_reserved(){
        return this.seats_reserved ;
    }

    /**
     * 设置 [预订席位]
     */
    @JsonProperty("seats_reserved")
    public void setSeats_reserved(Integer  seats_reserved){
        this.seats_reserved = seats_reserved ;
        this.seats_reservedDirtyFlag = true ;
    }

     /**
     * 获取 [预订席位]脏标记
     */
    @JsonIgnore
    public boolean getSeats_reservedDirtyFlag(){
        return this.seats_reservedDirtyFlag ;
    }   

    /**
     * 获取 [未确认的席位预订]
     */
    @JsonProperty("seats_unconfirmed")
    public Integer getSeats_unconfirmed(){
        return this.seats_unconfirmed ;
    }

    /**
     * 设置 [未确认的席位预订]
     */
    @JsonProperty("seats_unconfirmed")
    public void setSeats_unconfirmed(Integer  seats_unconfirmed){
        this.seats_unconfirmed = seats_unconfirmed ;
        this.seats_unconfirmedDirtyFlag = true ;
    }

     /**
     * 获取 [未确认的席位预订]脏标记
     */
    @JsonIgnore
    public boolean getSeats_unconfirmedDirtyFlag(){
        return this.seats_unconfirmedDirtyFlag ;
    }   

    /**
     * 获取 [座椅]
     */
    @JsonProperty("seats_used")
    public Integer getSeats_used(){
        return this.seats_used ;
    }

    /**
     * 设置 [座椅]
     */
    @JsonProperty("seats_used")
    public void setSeats_used(Integer  seats_used){
        this.seats_used = seats_used ;
        this.seats_usedDirtyFlag = true ;
    }

     /**
     * 获取 [座椅]脏标记
     */
    @JsonIgnore
    public boolean getSeats_usedDirtyFlag(){
        return this.seats_usedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
