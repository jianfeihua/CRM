package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_type_mail;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_type_mailClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_type_mailImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_type_mailFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_type_mail] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_type_mailClientServiceImpl implements Ievent_type_mailClientService {

    event_type_mailFeignClient event_type_mailFeignClient;

    @Autowired
    public event_type_mailClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_type_mailFeignClient = nameBuilder.target(event_type_mailFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_type_mailFeignClient = nameBuilder.target(event_type_mailFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_type_mail createModel() {
		return new event_type_mailImpl();
	}


    public void createBatch(List<Ievent_type_mail> event_type_mails){
        if(event_type_mails!=null){
            List<event_type_mailImpl> list = new ArrayList<event_type_mailImpl>();
            for(Ievent_type_mail ievent_type_mail :event_type_mails){
                list.add((event_type_mailImpl)ievent_type_mail) ;
            }
            event_type_mailFeignClient.createBatch(list) ;
        }
    }


    public void update(Ievent_type_mail event_type_mail){
        Ievent_type_mail clientModel = event_type_mailFeignClient.update(event_type_mail.getId(),(event_type_mailImpl)event_type_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type_mail.getClass(), false);
        copier.copy(clientModel, event_type_mail, null);
    }


    public void remove(Ievent_type_mail event_type_mail){
        event_type_mailFeignClient.remove(event_type_mail.getId()) ;
    }


    public void updateBatch(List<Ievent_type_mail> event_type_mails){
        if(event_type_mails!=null){
            List<event_type_mailImpl> list = new ArrayList<event_type_mailImpl>();
            for(Ievent_type_mail ievent_type_mail :event_type_mails){
                list.add((event_type_mailImpl)ievent_type_mail) ;
            }
            event_type_mailFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ievent_type_mail> event_type_mails){
        if(event_type_mails!=null){
            List<event_type_mailImpl> list = new ArrayList<event_type_mailImpl>();
            for(Ievent_type_mail ievent_type_mail :event_type_mails){
                list.add((event_type_mailImpl)ievent_type_mail) ;
            }
            event_type_mailFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ievent_type_mail> fetchDefault(SearchContext context){
        Page<event_type_mailImpl> page = this.event_type_mailFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ievent_type_mail event_type_mail){
        Ievent_type_mail clientModel = event_type_mailFeignClient.get(event_type_mail.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type_mail.getClass(), false);
        copier.copy(clientModel, event_type_mail, null);
    }


    public void create(Ievent_type_mail event_type_mail){
        Ievent_type_mail clientModel = event_type_mailFeignClient.create((event_type_mailImpl)event_type_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type_mail.getClass(), false);
        copier.copy(clientModel, event_type_mail, null);
    }


    public Page<Ievent_type_mail> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_type_mail event_type_mail){
        Ievent_type_mail clientModel = event_type_mailFeignClient.getDraft(event_type_mail.getId(),(event_type_mailImpl)event_type_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type_mail.getClass(), false);
        copier.copy(clientModel, event_type_mail, null);
    }



}

