package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_task_type;
import cn.ibizlab.odoo.client.odoo_project.config.odoo_projectClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproject_task_typeClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_task_typeImpl;
import cn.ibizlab.odoo.client.odoo_project.feign.project_task_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[project_task_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class project_task_typeClientServiceImpl implements Iproject_task_typeClientService {

    project_task_typeFeignClient project_task_typeFeignClient;

    @Autowired
    public project_task_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_projectClientProperties odoo_projectClientProperties) {
        if (odoo_projectClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_task_typeFeignClient = nameBuilder.target(project_task_typeFeignClient.class,"http://"+odoo_projectClientProperties.getServiceId()+"/") ;
		}else if (odoo_projectClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_task_typeFeignClient = nameBuilder.target(project_task_typeFeignClient.class,odoo_projectClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproject_task_type createModel() {
		return new project_task_typeImpl();
	}


    public void createBatch(List<Iproject_task_type> project_task_types){
        if(project_task_types!=null){
            List<project_task_typeImpl> list = new ArrayList<project_task_typeImpl>();
            for(Iproject_task_type iproject_task_type :project_task_types){
                list.add((project_task_typeImpl)iproject_task_type) ;
            }
            project_task_typeFeignClient.createBatch(list) ;
        }
    }


    public void get(Iproject_task_type project_task_type){
        Iproject_task_type clientModel = project_task_typeFeignClient.get(project_task_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task_type.getClass(), false);
        copier.copy(clientModel, project_task_type, null);
    }


    public void update(Iproject_task_type project_task_type){
        Iproject_task_type clientModel = project_task_typeFeignClient.update(project_task_type.getId(),(project_task_typeImpl)project_task_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task_type.getClass(), false);
        copier.copy(clientModel, project_task_type, null);
    }


    public Page<Iproject_task_type> fetchDefault(SearchContext context){
        Page<project_task_typeImpl> page = this.project_task_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iproject_task_type> project_task_types){
        if(project_task_types!=null){
            List<project_task_typeImpl> list = new ArrayList<project_task_typeImpl>();
            for(Iproject_task_type iproject_task_type :project_task_types){
                list.add((project_task_typeImpl)iproject_task_type) ;
            }
            project_task_typeFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iproject_task_type project_task_type){
        Iproject_task_type clientModel = project_task_typeFeignClient.create((project_task_typeImpl)project_task_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task_type.getClass(), false);
        copier.copy(clientModel, project_task_type, null);
    }


    public void remove(Iproject_task_type project_task_type){
        project_task_typeFeignClient.remove(project_task_type.getId()) ;
    }


    public void updateBatch(List<Iproject_task_type> project_task_types){
        if(project_task_types!=null){
            List<project_task_typeImpl> list = new ArrayList<project_task_typeImpl>();
            for(Iproject_task_type iproject_task_type :project_task_types){
                list.add((project_task_typeImpl)iproject_task_type) ;
            }
            project_task_typeFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproject_task_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproject_task_type project_task_type){
        Iproject_task_type clientModel = project_task_typeFeignClient.getDraft(project_task_type.getId(),(project_task_typeImpl)project_task_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task_type.getClass(), false);
        copier.copy(clientModel, project_task_type, null);
    }



}

