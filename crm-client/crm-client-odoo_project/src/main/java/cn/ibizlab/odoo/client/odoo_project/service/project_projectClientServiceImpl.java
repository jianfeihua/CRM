package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_project;
import cn.ibizlab.odoo.client.odoo_project.config.odoo_projectClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproject_projectClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_projectImpl;
import cn.ibizlab.odoo.client.odoo_project.feign.project_projectFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[project_project] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class project_projectClientServiceImpl implements Iproject_projectClientService {

    project_projectFeignClient project_projectFeignClient;

    @Autowired
    public project_projectClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_projectClientProperties odoo_projectClientProperties) {
        if (odoo_projectClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_projectFeignClient = nameBuilder.target(project_projectFeignClient.class,"http://"+odoo_projectClientProperties.getServiceId()+"/") ;
		}else if (odoo_projectClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_projectFeignClient = nameBuilder.target(project_projectFeignClient.class,odoo_projectClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproject_project createModel() {
		return new project_projectImpl();
	}


    public Page<Iproject_project> fetchDefault(SearchContext context){
        Page<project_projectImpl> page = this.project_projectFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iproject_project> project_projects){
        if(project_projects!=null){
            List<project_projectImpl> list = new ArrayList<project_projectImpl>();
            for(Iproject_project iproject_project :project_projects){
                list.add((project_projectImpl)iproject_project) ;
            }
            project_projectFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iproject_project> project_projects){
        if(project_projects!=null){
            List<project_projectImpl> list = new ArrayList<project_projectImpl>();
            for(Iproject_project iproject_project :project_projects){
                list.add((project_projectImpl)iproject_project) ;
            }
            project_projectFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iproject_project> project_projects){
        if(project_projects!=null){
            List<project_projectImpl> list = new ArrayList<project_projectImpl>();
            for(Iproject_project iproject_project :project_projects){
                list.add((project_projectImpl)iproject_project) ;
            }
            project_projectFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iproject_project project_project){
        project_projectFeignClient.remove(project_project.getId()) ;
    }


    public void create(Iproject_project project_project){
        Iproject_project clientModel = project_projectFeignClient.create((project_projectImpl)project_project) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_project.getClass(), false);
        copier.copy(clientModel, project_project, null);
    }


    public void get(Iproject_project project_project){
        Iproject_project clientModel = project_projectFeignClient.get(project_project.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_project.getClass(), false);
        copier.copy(clientModel, project_project, null);
    }


    public void update(Iproject_project project_project){
        Iproject_project clientModel = project_projectFeignClient.update(project_project.getId(),(project_projectImpl)project_project) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_project.getClass(), false);
        copier.copy(clientModel, project_project, null);
    }


    public Page<Iproject_project> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproject_project project_project){
        Iproject_project clientModel = project_projectFeignClient.getDraft(project_project.getId(),(project_projectImpl)project_project) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_project.getClass(), false);
        copier.copy(clientModel, project_project, null);
    }



}

