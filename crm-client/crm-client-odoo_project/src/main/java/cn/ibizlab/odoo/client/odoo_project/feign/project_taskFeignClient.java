package cn.ibizlab.odoo.client.odoo_project.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproject_task;
import cn.ibizlab.odoo.client.odoo_project.model.project_taskImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[project_task] 服务对象接口
 */
public interface project_taskFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tasks/updatebatch")
    public project_taskImpl updateBatch(@RequestBody List<project_taskImpl> project_tasks);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tasks/createbatch")
    public project_taskImpl createBatch(@RequestBody List<project_taskImpl> project_tasks);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tasks")
    public project_taskImpl create(@RequestBody project_taskImpl project_task);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tasks/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tasks/{id}")
    public project_taskImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tasks/fetchdefault")
    public Page<project_taskImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tasks/{id}")
    public project_taskImpl update(@PathVariable("id") Integer id,@RequestBody project_taskImpl project_task);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tasks/removebatch")
    public project_taskImpl removeBatch(@RequestBody List<project_taskImpl> project_tasks);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tasks/select")
    public Page<project_taskImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tasks/{id}/getdraft")
    public project_taskImpl getDraft(@PathVariable("id") Integer id,@RequestBody project_taskImpl project_task);



}
