package cn.ibizlab.odoo.client.odoo_survey.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Isurvey_question;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[survey_question] 对象
 */
public class survey_questionImpl implements Isurvey_question,Serializable{

    /**
     * 栏位数
     */
    public String column_nb;

    @JsonIgnore
    public boolean column_nbDirtyFlag;
    
    /**
     * 显示评论字段
     */
    public String comments_allowed;

    @JsonIgnore
    public boolean comments_allowedDirtyFlag;
    
    /**
     * 评论消息
     */
    public String comments_message;

    @JsonIgnore
    public boolean comments_messageDirtyFlag;
    
    /**
     * 评论字段是答案选项
     */
    public String comment_count_as_answer;

    @JsonIgnore
    public boolean comment_count_as_answerDirtyFlag;
    
    /**
     * 错误消息
     */
    public String constr_error_msg;

    @JsonIgnore
    public boolean constr_error_msgDirtyFlag;
    
    /**
     * 必答问题
     */
    public String constr_mandatory;

    @JsonIgnore
    public boolean constr_mandatoryDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示模式
     */
    public String display_mode;

    @JsonIgnore
    public boolean display_modeDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 答案类型
     */
    public String labels_ids;

    @JsonIgnore
    public boolean labels_idsDirtyFlag;
    
    /**
     * 表格行数
     */
    public String labels_ids_2;

    @JsonIgnore
    public boolean labels_ids_2DirtyFlag;
    
    /**
     * 表格类型
     */
    public String matrix_subtype;

    @JsonIgnore
    public boolean matrix_subtypeDirtyFlag;
    
    /**
     * 调查页面
     */
    public Integer page_id;

    @JsonIgnore
    public boolean page_idDirtyFlag;
    
    /**
     * 问题名称
     */
    public String question;

    @JsonIgnore
    public boolean questionDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 问卷
     */
    public Integer survey_id;

    @JsonIgnore
    public boolean survey_idDirtyFlag;
    
    /**
     * 问题类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 答案
     */
    public String user_input_line_ids;

    @JsonIgnore
    public boolean user_input_line_idsDirtyFlag;
    
    /**
     * 输入必须是EMail
     */
    public String validation_email;

    @JsonIgnore
    public boolean validation_emailDirtyFlag;
    
    /**
     * 信息：验证错误
     */
    public String validation_error_msg;

    @JsonIgnore
    public boolean validation_error_msgDirtyFlag;
    
    /**
     * 最大文本长度
     */
    public Integer validation_length_max;

    @JsonIgnore
    public boolean validation_length_maxDirtyFlag;
    
    /**
     * 最小文本长度
     */
    public Integer validation_length_min;

    @JsonIgnore
    public boolean validation_length_minDirtyFlag;
    
    /**
     * 最大日期
     */
    public Timestamp validation_max_date;

    @JsonIgnore
    public boolean validation_max_dateDirtyFlag;
    
    /**
     * 最大值
     */
    public Double validation_max_float_value;

    @JsonIgnore
    public boolean validation_max_float_valueDirtyFlag;
    
    /**
     * 最小日期
     */
    public Timestamp validation_min_date;

    @JsonIgnore
    public boolean validation_min_dateDirtyFlag;
    
    /**
     * 最小值
     */
    public Double validation_min_float_value;

    @JsonIgnore
    public boolean validation_min_float_valueDirtyFlag;
    
    /**
     * 验证文本
     */
    public String validation_required;

    @JsonIgnore
    public boolean validation_requiredDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [栏位数]
     */
    @JsonProperty("column_nb")
    public String getColumn_nb(){
        return this.column_nb ;
    }

    /**
     * 设置 [栏位数]
     */
    @JsonProperty("column_nb")
    public void setColumn_nb(String  column_nb){
        this.column_nb = column_nb ;
        this.column_nbDirtyFlag = true ;
    }

     /**
     * 获取 [栏位数]脏标记
     */
    @JsonIgnore
    public boolean getColumn_nbDirtyFlag(){
        return this.column_nbDirtyFlag ;
    }   

    /**
     * 获取 [显示评论字段]
     */
    @JsonProperty("comments_allowed")
    public String getComments_allowed(){
        return this.comments_allowed ;
    }

    /**
     * 设置 [显示评论字段]
     */
    @JsonProperty("comments_allowed")
    public void setComments_allowed(String  comments_allowed){
        this.comments_allowed = comments_allowed ;
        this.comments_allowedDirtyFlag = true ;
    }

     /**
     * 获取 [显示评论字段]脏标记
     */
    @JsonIgnore
    public boolean getComments_allowedDirtyFlag(){
        return this.comments_allowedDirtyFlag ;
    }   

    /**
     * 获取 [评论消息]
     */
    @JsonProperty("comments_message")
    public String getComments_message(){
        return this.comments_message ;
    }

    /**
     * 设置 [评论消息]
     */
    @JsonProperty("comments_message")
    public void setComments_message(String  comments_message){
        this.comments_message = comments_message ;
        this.comments_messageDirtyFlag = true ;
    }

     /**
     * 获取 [评论消息]脏标记
     */
    @JsonIgnore
    public boolean getComments_messageDirtyFlag(){
        return this.comments_messageDirtyFlag ;
    }   

    /**
     * 获取 [评论字段是答案选项]
     */
    @JsonProperty("comment_count_as_answer")
    public String getComment_count_as_answer(){
        return this.comment_count_as_answer ;
    }

    /**
     * 设置 [评论字段是答案选项]
     */
    @JsonProperty("comment_count_as_answer")
    public void setComment_count_as_answer(String  comment_count_as_answer){
        this.comment_count_as_answer = comment_count_as_answer ;
        this.comment_count_as_answerDirtyFlag = true ;
    }

     /**
     * 获取 [评论字段是答案选项]脏标记
     */
    @JsonIgnore
    public boolean getComment_count_as_answerDirtyFlag(){
        return this.comment_count_as_answerDirtyFlag ;
    }   

    /**
     * 获取 [错误消息]
     */
    @JsonProperty("constr_error_msg")
    public String getConstr_error_msg(){
        return this.constr_error_msg ;
    }

    /**
     * 设置 [错误消息]
     */
    @JsonProperty("constr_error_msg")
    public void setConstr_error_msg(String  constr_error_msg){
        this.constr_error_msg = constr_error_msg ;
        this.constr_error_msgDirtyFlag = true ;
    }

     /**
     * 获取 [错误消息]脏标记
     */
    @JsonIgnore
    public boolean getConstr_error_msgDirtyFlag(){
        return this.constr_error_msgDirtyFlag ;
    }   

    /**
     * 获取 [必答问题]
     */
    @JsonProperty("constr_mandatory")
    public String getConstr_mandatory(){
        return this.constr_mandatory ;
    }

    /**
     * 设置 [必答问题]
     */
    @JsonProperty("constr_mandatory")
    public void setConstr_mandatory(String  constr_mandatory){
        this.constr_mandatory = constr_mandatory ;
        this.constr_mandatoryDirtyFlag = true ;
    }

     /**
     * 获取 [必答问题]脏标记
     */
    @JsonIgnore
    public boolean getConstr_mandatoryDirtyFlag(){
        return this.constr_mandatoryDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示模式]
     */
    @JsonProperty("display_mode")
    public String getDisplay_mode(){
        return this.display_mode ;
    }

    /**
     * 设置 [显示模式]
     */
    @JsonProperty("display_mode")
    public void setDisplay_mode(String  display_mode){
        this.display_mode = display_mode ;
        this.display_modeDirtyFlag = true ;
    }

     /**
     * 获取 [显示模式]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_modeDirtyFlag(){
        return this.display_modeDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [答案类型]
     */
    @JsonProperty("labels_ids")
    public String getLabels_ids(){
        return this.labels_ids ;
    }

    /**
     * 设置 [答案类型]
     */
    @JsonProperty("labels_ids")
    public void setLabels_ids(String  labels_ids){
        this.labels_ids = labels_ids ;
        this.labels_idsDirtyFlag = true ;
    }

     /**
     * 获取 [答案类型]脏标记
     */
    @JsonIgnore
    public boolean getLabels_idsDirtyFlag(){
        return this.labels_idsDirtyFlag ;
    }   

    /**
     * 获取 [表格行数]
     */
    @JsonProperty("labels_ids_2")
    public String getLabels_ids_2(){
        return this.labels_ids_2 ;
    }

    /**
     * 设置 [表格行数]
     */
    @JsonProperty("labels_ids_2")
    public void setLabels_ids_2(String  labels_ids_2){
        this.labels_ids_2 = labels_ids_2 ;
        this.labels_ids_2DirtyFlag = true ;
    }

     /**
     * 获取 [表格行数]脏标记
     */
    @JsonIgnore
    public boolean getLabels_ids_2DirtyFlag(){
        return this.labels_ids_2DirtyFlag ;
    }   

    /**
     * 获取 [表格类型]
     */
    @JsonProperty("matrix_subtype")
    public String getMatrix_subtype(){
        return this.matrix_subtype ;
    }

    /**
     * 设置 [表格类型]
     */
    @JsonProperty("matrix_subtype")
    public void setMatrix_subtype(String  matrix_subtype){
        this.matrix_subtype = matrix_subtype ;
        this.matrix_subtypeDirtyFlag = true ;
    }

     /**
     * 获取 [表格类型]脏标记
     */
    @JsonIgnore
    public boolean getMatrix_subtypeDirtyFlag(){
        return this.matrix_subtypeDirtyFlag ;
    }   

    /**
     * 获取 [调查页面]
     */
    @JsonProperty("page_id")
    public Integer getPage_id(){
        return this.page_id ;
    }

    /**
     * 设置 [调查页面]
     */
    @JsonProperty("page_id")
    public void setPage_id(Integer  page_id){
        this.page_id = page_id ;
        this.page_idDirtyFlag = true ;
    }

     /**
     * 获取 [调查页面]脏标记
     */
    @JsonIgnore
    public boolean getPage_idDirtyFlag(){
        return this.page_idDirtyFlag ;
    }   

    /**
     * 获取 [问题名称]
     */
    @JsonProperty("question")
    public String getQuestion(){
        return this.question ;
    }

    /**
     * 设置 [问题名称]
     */
    @JsonProperty("question")
    public void setQuestion(String  question){
        this.question = question ;
        this.questionDirtyFlag = true ;
    }

     /**
     * 获取 [问题名称]脏标记
     */
    @JsonIgnore
    public boolean getQuestionDirtyFlag(){
        return this.questionDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [问卷]
     */
    @JsonProperty("survey_id")
    public Integer getSurvey_id(){
        return this.survey_id ;
    }

    /**
     * 设置 [问卷]
     */
    @JsonProperty("survey_id")
    public void setSurvey_id(Integer  survey_id){
        this.survey_id = survey_id ;
        this.survey_idDirtyFlag = true ;
    }

     /**
     * 获取 [问卷]脏标记
     */
    @JsonIgnore
    public boolean getSurvey_idDirtyFlag(){
        return this.survey_idDirtyFlag ;
    }   

    /**
     * 获取 [问题类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [问题类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [问题类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [答案]
     */
    @JsonProperty("user_input_line_ids")
    public String getUser_input_line_ids(){
        return this.user_input_line_ids ;
    }

    /**
     * 设置 [答案]
     */
    @JsonProperty("user_input_line_ids")
    public void setUser_input_line_ids(String  user_input_line_ids){
        this.user_input_line_ids = user_input_line_ids ;
        this.user_input_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [答案]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_line_idsDirtyFlag(){
        return this.user_input_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [输入必须是EMail]
     */
    @JsonProperty("validation_email")
    public String getValidation_email(){
        return this.validation_email ;
    }

    /**
     * 设置 [输入必须是EMail]
     */
    @JsonProperty("validation_email")
    public void setValidation_email(String  validation_email){
        this.validation_email = validation_email ;
        this.validation_emailDirtyFlag = true ;
    }

     /**
     * 获取 [输入必须是EMail]脏标记
     */
    @JsonIgnore
    public boolean getValidation_emailDirtyFlag(){
        return this.validation_emailDirtyFlag ;
    }   

    /**
     * 获取 [信息：验证错误]
     */
    @JsonProperty("validation_error_msg")
    public String getValidation_error_msg(){
        return this.validation_error_msg ;
    }

    /**
     * 设置 [信息：验证错误]
     */
    @JsonProperty("validation_error_msg")
    public void setValidation_error_msg(String  validation_error_msg){
        this.validation_error_msg = validation_error_msg ;
        this.validation_error_msgDirtyFlag = true ;
    }

     /**
     * 获取 [信息：验证错误]脏标记
     */
    @JsonIgnore
    public boolean getValidation_error_msgDirtyFlag(){
        return this.validation_error_msgDirtyFlag ;
    }   

    /**
     * 获取 [最大文本长度]
     */
    @JsonProperty("validation_length_max")
    public Integer getValidation_length_max(){
        return this.validation_length_max ;
    }

    /**
     * 设置 [最大文本长度]
     */
    @JsonProperty("validation_length_max")
    public void setValidation_length_max(Integer  validation_length_max){
        this.validation_length_max = validation_length_max ;
        this.validation_length_maxDirtyFlag = true ;
    }

     /**
     * 获取 [最大文本长度]脏标记
     */
    @JsonIgnore
    public boolean getValidation_length_maxDirtyFlag(){
        return this.validation_length_maxDirtyFlag ;
    }   

    /**
     * 获取 [最小文本长度]
     */
    @JsonProperty("validation_length_min")
    public Integer getValidation_length_min(){
        return this.validation_length_min ;
    }

    /**
     * 设置 [最小文本长度]
     */
    @JsonProperty("validation_length_min")
    public void setValidation_length_min(Integer  validation_length_min){
        this.validation_length_min = validation_length_min ;
        this.validation_length_minDirtyFlag = true ;
    }

     /**
     * 获取 [最小文本长度]脏标记
     */
    @JsonIgnore
    public boolean getValidation_length_minDirtyFlag(){
        return this.validation_length_minDirtyFlag ;
    }   

    /**
     * 获取 [最大日期]
     */
    @JsonProperty("validation_max_date")
    public Timestamp getValidation_max_date(){
        return this.validation_max_date ;
    }

    /**
     * 设置 [最大日期]
     */
    @JsonProperty("validation_max_date")
    public void setValidation_max_date(Timestamp  validation_max_date){
        this.validation_max_date = validation_max_date ;
        this.validation_max_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最大日期]脏标记
     */
    @JsonIgnore
    public boolean getValidation_max_dateDirtyFlag(){
        return this.validation_max_dateDirtyFlag ;
    }   

    /**
     * 获取 [最大值]
     */
    @JsonProperty("validation_max_float_value")
    public Double getValidation_max_float_value(){
        return this.validation_max_float_value ;
    }

    /**
     * 设置 [最大值]
     */
    @JsonProperty("validation_max_float_value")
    public void setValidation_max_float_value(Double  validation_max_float_value){
        this.validation_max_float_value = validation_max_float_value ;
        this.validation_max_float_valueDirtyFlag = true ;
    }

     /**
     * 获取 [最大值]脏标记
     */
    @JsonIgnore
    public boolean getValidation_max_float_valueDirtyFlag(){
        return this.validation_max_float_valueDirtyFlag ;
    }   

    /**
     * 获取 [最小日期]
     */
    @JsonProperty("validation_min_date")
    public Timestamp getValidation_min_date(){
        return this.validation_min_date ;
    }

    /**
     * 设置 [最小日期]
     */
    @JsonProperty("validation_min_date")
    public void setValidation_min_date(Timestamp  validation_min_date){
        this.validation_min_date = validation_min_date ;
        this.validation_min_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最小日期]脏标记
     */
    @JsonIgnore
    public boolean getValidation_min_dateDirtyFlag(){
        return this.validation_min_dateDirtyFlag ;
    }   

    /**
     * 获取 [最小值]
     */
    @JsonProperty("validation_min_float_value")
    public Double getValidation_min_float_value(){
        return this.validation_min_float_value ;
    }

    /**
     * 设置 [最小值]
     */
    @JsonProperty("validation_min_float_value")
    public void setValidation_min_float_value(Double  validation_min_float_value){
        this.validation_min_float_value = validation_min_float_value ;
        this.validation_min_float_valueDirtyFlag = true ;
    }

     /**
     * 获取 [最小值]脏标记
     */
    @JsonIgnore
    public boolean getValidation_min_float_valueDirtyFlag(){
        return this.validation_min_float_valueDirtyFlag ;
    }   

    /**
     * 获取 [验证文本]
     */
    @JsonProperty("validation_required")
    public String getValidation_required(){
        return this.validation_required ;
    }

    /**
     * 设置 [验证文本]
     */
    @JsonProperty("validation_required")
    public void setValidation_required(String  validation_required){
        this.validation_required = validation_required ;
        this.validation_requiredDirtyFlag = true ;
    }

     /**
     * 获取 [验证文本]脏标记
     */
    @JsonIgnore
    public boolean getValidation_requiredDirtyFlag(){
        return this.validation_requiredDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
