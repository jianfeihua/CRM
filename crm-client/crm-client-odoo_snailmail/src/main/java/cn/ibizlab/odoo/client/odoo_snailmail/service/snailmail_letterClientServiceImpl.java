package cn.ibizlab.odoo.client.odoo_snailmail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isnailmail_letter;
import cn.ibizlab.odoo.client.odoo_snailmail.config.odoo_snailmailClientProperties;
import cn.ibizlab.odoo.core.client.service.Isnailmail_letterClientService;
import cn.ibizlab.odoo.client.odoo_snailmail.model.snailmail_letterImpl;
import cn.ibizlab.odoo.client.odoo_snailmail.feign.snailmail_letterFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[snailmail_letter] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class snailmail_letterClientServiceImpl implements Isnailmail_letterClientService {

    snailmail_letterFeignClient snailmail_letterFeignClient;

    @Autowired
    public snailmail_letterClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_snailmailClientProperties odoo_snailmailClientProperties) {
        if (odoo_snailmailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.snailmail_letterFeignClient = nameBuilder.target(snailmail_letterFeignClient.class,"http://"+odoo_snailmailClientProperties.getServiceId()+"/") ;
		}else if (odoo_snailmailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.snailmail_letterFeignClient = nameBuilder.target(snailmail_letterFeignClient.class,odoo_snailmailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isnailmail_letter createModel() {
		return new snailmail_letterImpl();
	}


    public void create(Isnailmail_letter snailmail_letter){
        Isnailmail_letter clientModel = snailmail_letterFeignClient.create((snailmail_letterImpl)snailmail_letter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), snailmail_letter.getClass(), false);
        copier.copy(clientModel, snailmail_letter, null);
    }


    public void get(Isnailmail_letter snailmail_letter){
        Isnailmail_letter clientModel = snailmail_letterFeignClient.get(snailmail_letter.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), snailmail_letter.getClass(), false);
        copier.copy(clientModel, snailmail_letter, null);
    }


    public void update(Isnailmail_letter snailmail_letter){
        Isnailmail_letter clientModel = snailmail_letterFeignClient.update(snailmail_letter.getId(),(snailmail_letterImpl)snailmail_letter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), snailmail_letter.getClass(), false);
        copier.copy(clientModel, snailmail_letter, null);
    }


    public void remove(Isnailmail_letter snailmail_letter){
        snailmail_letterFeignClient.remove(snailmail_letter.getId()) ;
    }


    public Page<Isnailmail_letter> fetchDefault(SearchContext context){
        Page<snailmail_letterImpl> page = this.snailmail_letterFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Isnailmail_letter> snailmail_letters){
        if(snailmail_letters!=null){
            List<snailmail_letterImpl> list = new ArrayList<snailmail_letterImpl>();
            for(Isnailmail_letter isnailmail_letter :snailmail_letters){
                list.add((snailmail_letterImpl)isnailmail_letter) ;
            }
            snailmail_letterFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Isnailmail_letter> snailmail_letters){
        if(snailmail_letters!=null){
            List<snailmail_letterImpl> list = new ArrayList<snailmail_letterImpl>();
            for(Isnailmail_letter isnailmail_letter :snailmail_letters){
                list.add((snailmail_letterImpl)isnailmail_letter) ;
            }
            snailmail_letterFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Isnailmail_letter> snailmail_letters){
        if(snailmail_letters!=null){
            List<snailmail_letterImpl> list = new ArrayList<snailmail_letterImpl>();
            for(Isnailmail_letter isnailmail_letter :snailmail_letters){
                list.add((snailmail_letterImpl)isnailmail_letter) ;
            }
            snailmail_letterFeignClient.updateBatch(list) ;
        }
    }


    public Page<Isnailmail_letter> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isnailmail_letter snailmail_letter){
        Isnailmail_letter clientModel = snailmail_letterFeignClient.getDraft(snailmail_letter.getId(),(snailmail_letterImpl)snailmail_letter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), snailmail_letter.getClass(), false);
        copier.copy(clientModel, snailmail_letter, null);
    }



}

