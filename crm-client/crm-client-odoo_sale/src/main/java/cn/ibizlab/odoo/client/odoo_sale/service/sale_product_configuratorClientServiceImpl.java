package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_product_configurator;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_product_configuratorClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_product_configuratorImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_product_configuratorFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_product_configurator] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_product_configuratorClientServiceImpl implements Isale_product_configuratorClientService {

    sale_product_configuratorFeignClient sale_product_configuratorFeignClient;

    @Autowired
    public sale_product_configuratorClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_product_configuratorFeignClient = nameBuilder.target(sale_product_configuratorFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_product_configuratorFeignClient = nameBuilder.target(sale_product_configuratorFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_product_configurator createModel() {
		return new sale_product_configuratorImpl();
	}


    public void create(Isale_product_configurator sale_product_configurator){
        Isale_product_configurator clientModel = sale_product_configuratorFeignClient.create((sale_product_configuratorImpl)sale_product_configurator) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_product_configurator.getClass(), false);
        copier.copy(clientModel, sale_product_configurator, null);
    }


    public void createBatch(List<Isale_product_configurator> sale_product_configurators){
        if(sale_product_configurators!=null){
            List<sale_product_configuratorImpl> list = new ArrayList<sale_product_configuratorImpl>();
            for(Isale_product_configurator isale_product_configurator :sale_product_configurators){
                list.add((sale_product_configuratorImpl)isale_product_configurator) ;
            }
            sale_product_configuratorFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Isale_product_configurator> sale_product_configurators){
        if(sale_product_configurators!=null){
            List<sale_product_configuratorImpl> list = new ArrayList<sale_product_configuratorImpl>();
            for(Isale_product_configurator isale_product_configurator :sale_product_configurators){
                list.add((sale_product_configuratorImpl)isale_product_configurator) ;
            }
            sale_product_configuratorFeignClient.removeBatch(list) ;
        }
    }


    public void get(Isale_product_configurator sale_product_configurator){
        Isale_product_configurator clientModel = sale_product_configuratorFeignClient.get(sale_product_configurator.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_product_configurator.getClass(), false);
        copier.copy(clientModel, sale_product_configurator, null);
    }


    public void update(Isale_product_configurator sale_product_configurator){
        Isale_product_configurator clientModel = sale_product_configuratorFeignClient.update(sale_product_configurator.getId(),(sale_product_configuratorImpl)sale_product_configurator) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_product_configurator.getClass(), false);
        copier.copy(clientModel, sale_product_configurator, null);
    }


    public void updateBatch(List<Isale_product_configurator> sale_product_configurators){
        if(sale_product_configurators!=null){
            List<sale_product_configuratorImpl> list = new ArrayList<sale_product_configuratorImpl>();
            for(Isale_product_configurator isale_product_configurator :sale_product_configurators){
                list.add((sale_product_configuratorImpl)isale_product_configurator) ;
            }
            sale_product_configuratorFeignClient.updateBatch(list) ;
        }
    }


    public Page<Isale_product_configurator> fetchDefault(SearchContext context){
        Page<sale_product_configuratorImpl> page = this.sale_product_configuratorFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Isale_product_configurator sale_product_configurator){
        sale_product_configuratorFeignClient.remove(sale_product_configurator.getId()) ;
    }


    public Page<Isale_product_configurator> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isale_product_configurator sale_product_configurator){
        Isale_product_configurator clientModel = sale_product_configuratorFeignClient.getDraft(sale_product_configurator.getId(),(sale_product_configuratorImpl)sale_product_configurator) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_product_configurator.getClass(), false);
        copier.copy(clientModel, sale_product_configurator, null);
    }



}

