package cn.ibizlab.odoo.client.odoo_sale.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isale_order;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_orderImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sale_order] 服务对象接口
 */
public interface sale_orderFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_orders/updatebatch")
    public sale_orderImpl updateBatch(@RequestBody List<sale_orderImpl> sale_orders);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_orders/{id}")
    public sale_orderImpl update(@PathVariable("id") Integer id,@RequestBody sale_orderImpl sale_order);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_orders/fetchdefault")
    public Page<sale_orderImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_orders/removebatch")
    public sale_orderImpl removeBatch(@RequestBody List<sale_orderImpl> sale_orders);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_orders")
    public sale_orderImpl create(@RequestBody sale_orderImpl sale_order);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_orders/createbatch")
    public sale_orderImpl createBatch(@RequestBody List<sale_orderImpl> sale_orders);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_orders/{id}")
    public sale_orderImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_orders/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_orders/select")
    public Page<sale_orderImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_orders/{id}/save")
    public sale_orderImpl save(@PathVariable("id") Integer id,@RequestBody sale_orderImpl sale_order);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_orders/{id}/checkkey")
    public sale_orderImpl checkKey(@PathVariable("id") Integer id,@RequestBody sale_orderImpl sale_order);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_orders/{id}/getdraft")
    public sale_orderImpl getDraft(@PathVariable("id") Integer id,@RequestBody sale_orderImpl sale_order);



}
