package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order_line;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_order_lineClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_lineImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_order_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_order_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_order_lineClientServiceImpl implements Isale_order_lineClientService {

    sale_order_lineFeignClient sale_order_lineFeignClient;

    @Autowired
    public sale_order_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_order_lineFeignClient = nameBuilder.target(sale_order_lineFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_order_lineFeignClient = nameBuilder.target(sale_order_lineFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_order_line createModel() {
		return new sale_order_lineImpl();
	}


    public void removeBatch(List<Isale_order_line> sale_order_lines){
        if(sale_order_lines!=null){
            List<sale_order_lineImpl> list = new ArrayList<sale_order_lineImpl>();
            for(Isale_order_line isale_order_line :sale_order_lines){
                list.add((sale_order_lineImpl)isale_order_line) ;
            }
            sale_order_lineFeignClient.removeBatch(list) ;
        }
    }


    public void update(Isale_order_line sale_order_line){
        Isale_order_line clientModel = sale_order_lineFeignClient.update(sale_order_line.getId(),(sale_order_lineImpl)sale_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_line.getClass(), false);
        copier.copy(clientModel, sale_order_line, null);
    }


    public void createBatch(List<Isale_order_line> sale_order_lines){
        if(sale_order_lines!=null){
            List<sale_order_lineImpl> list = new ArrayList<sale_order_lineImpl>();
            for(Isale_order_line isale_order_line :sale_order_lines){
                list.add((sale_order_lineImpl)isale_order_line) ;
            }
            sale_order_lineFeignClient.createBatch(list) ;
        }
    }


    public void remove(Isale_order_line sale_order_line){
        sale_order_lineFeignClient.remove(sale_order_line.getId()) ;
    }


    public void get(Isale_order_line sale_order_line){
        Isale_order_line clientModel = sale_order_lineFeignClient.get(sale_order_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_line.getClass(), false);
        copier.copy(clientModel, sale_order_line, null);
    }


    public void create(Isale_order_line sale_order_line){
        Isale_order_line clientModel = sale_order_lineFeignClient.create((sale_order_lineImpl)sale_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_line.getClass(), false);
        copier.copy(clientModel, sale_order_line, null);
    }


    public void updateBatch(List<Isale_order_line> sale_order_lines){
        if(sale_order_lines!=null){
            List<sale_order_lineImpl> list = new ArrayList<sale_order_lineImpl>();
            for(Isale_order_line isale_order_line :sale_order_lines){
                list.add((sale_order_lineImpl)isale_order_line) ;
            }
            sale_order_lineFeignClient.updateBatch(list) ;
        }
    }


    public Page<Isale_order_line> fetchDefault(SearchContext context){
        Page<sale_order_lineImpl> page = this.sale_order_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Isale_order_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isale_order_line sale_order_line){
        Isale_order_line clientModel = sale_order_lineFeignClient.getDraft(sale_order_line.getId(),(sale_order_lineImpl)sale_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_line.getClass(), false);
        copier.copy(clientModel, sale_order_line, null);
    }


    public void checkKey(Isale_order_line sale_order_line){
        Isale_order_line clientModel = sale_order_lineFeignClient.checkKey(sale_order_line.getId(),(sale_order_lineImpl)sale_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_line.getClass(), false);
        copier.copy(clientModel, sale_order_line, null);
    }


    public void save(Isale_order_line sale_order_line){
        Isale_order_line clientModel = sale_order_lineFeignClient.save(sale_order_line.getId(),(sale_order_lineImpl)sale_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_line.getClass(), false);
        copier.copy(clientModel, sale_order_line, null);
    }



}

