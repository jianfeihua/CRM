package cn.ibizlab.odoo.client.odoo_sale.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Isale_advance_payment_inv;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[sale_advance_payment_inv] 对象
 */
public class sale_advance_payment_invImpl implements Isale_advance_payment_inv,Serializable{

    /**
     * 你要开什么发票？
     */
    public String advance_payment_method;

    @JsonIgnore
    public boolean advance_payment_methodDirtyFlag;
    
    /**
     * 预付定金总额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 订购数量
     */
    public Integer count;

    @JsonIgnore
    public boolean countDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 收入科目
     */
    public Integer deposit_account_id;

    @JsonIgnore
    public boolean deposit_account_idDirtyFlag;
    
    /**
     * 收入科目
     */
    public String deposit_account_id_text;

    @JsonIgnore
    public boolean deposit_account_id_textDirtyFlag;
    
    /**
     * 销项税
     */
    public String deposit_taxes_id;

    @JsonIgnore
    public boolean deposit_taxes_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 预付定金产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 预付定金产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [你要开什么发票？]
     */
    @JsonProperty("advance_payment_method")
    public String getAdvance_payment_method(){
        return this.advance_payment_method ;
    }

    /**
     * 设置 [你要开什么发票？]
     */
    @JsonProperty("advance_payment_method")
    public void setAdvance_payment_method(String  advance_payment_method){
        this.advance_payment_method = advance_payment_method ;
        this.advance_payment_methodDirtyFlag = true ;
    }

     /**
     * 获取 [你要开什么发票？]脏标记
     */
    @JsonIgnore
    public boolean getAdvance_payment_methodDirtyFlag(){
        return this.advance_payment_methodDirtyFlag ;
    }   

    /**
     * 获取 [预付定金总额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [预付定金总额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [预付定金总额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [订购数量]
     */
    @JsonProperty("count")
    public Integer getCount(){
        return this.count ;
    }

    /**
     * 设置 [订购数量]
     */
    @JsonProperty("count")
    public void setCount(Integer  count){
        this.count = count ;
        this.countDirtyFlag = true ;
    }

     /**
     * 获取 [订购数量]脏标记
     */
    @JsonIgnore
    public boolean getCountDirtyFlag(){
        return this.countDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [收入科目]
     */
    @JsonProperty("deposit_account_id")
    public Integer getDeposit_account_id(){
        return this.deposit_account_id ;
    }

    /**
     * 设置 [收入科目]
     */
    @JsonProperty("deposit_account_id")
    public void setDeposit_account_id(Integer  deposit_account_id){
        this.deposit_account_id = deposit_account_id ;
        this.deposit_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [收入科目]脏标记
     */
    @JsonIgnore
    public boolean getDeposit_account_idDirtyFlag(){
        return this.deposit_account_idDirtyFlag ;
    }   

    /**
     * 获取 [收入科目]
     */
    @JsonProperty("deposit_account_id_text")
    public String getDeposit_account_id_text(){
        return this.deposit_account_id_text ;
    }

    /**
     * 设置 [收入科目]
     */
    @JsonProperty("deposit_account_id_text")
    public void setDeposit_account_id_text(String  deposit_account_id_text){
        this.deposit_account_id_text = deposit_account_id_text ;
        this.deposit_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收入科目]脏标记
     */
    @JsonIgnore
    public boolean getDeposit_account_id_textDirtyFlag(){
        return this.deposit_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [销项税]
     */
    @JsonProperty("deposit_taxes_id")
    public String getDeposit_taxes_id(){
        return this.deposit_taxes_id ;
    }

    /**
     * 设置 [销项税]
     */
    @JsonProperty("deposit_taxes_id")
    public void setDeposit_taxes_id(String  deposit_taxes_id){
        this.deposit_taxes_id = deposit_taxes_id ;
        this.deposit_taxes_idDirtyFlag = true ;
    }

     /**
     * 获取 [销项税]脏标记
     */
    @JsonIgnore
    public boolean getDeposit_taxes_idDirtyFlag(){
        return this.deposit_taxes_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [预付定金产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [预付定金产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [预付定金产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [预付定金产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [预付定金产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [预付定金产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
