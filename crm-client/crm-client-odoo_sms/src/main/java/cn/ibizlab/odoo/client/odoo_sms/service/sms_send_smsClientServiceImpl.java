package cn.ibizlab.odoo.client.odoo_sms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isms_send_sms;
import cn.ibizlab.odoo.client.odoo_sms.config.odoo_smsClientProperties;
import cn.ibizlab.odoo.core.client.service.Isms_send_smsClientService;
import cn.ibizlab.odoo.client.odoo_sms.model.sms_send_smsImpl;
import cn.ibizlab.odoo.client.odoo_sms.feign.sms_send_smsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sms_send_sms] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sms_send_smsClientServiceImpl implements Isms_send_smsClientService {

    sms_send_smsFeignClient sms_send_smsFeignClient;

    @Autowired
    public sms_send_smsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_smsClientProperties odoo_smsClientProperties) {
        if (odoo_smsClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sms_send_smsFeignClient = nameBuilder.target(sms_send_smsFeignClient.class,"http://"+odoo_smsClientProperties.getServiceId()+"/") ;
		}else if (odoo_smsClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sms_send_smsFeignClient = nameBuilder.target(sms_send_smsFeignClient.class,odoo_smsClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isms_send_sms createModel() {
		return new sms_send_smsImpl();
	}


    public void remove(Isms_send_sms sms_send_sms){
        sms_send_smsFeignClient.remove(sms_send_sms.getId()) ;
    }


    public void updateBatch(List<Isms_send_sms> sms_send_sms){
        if(sms_send_sms!=null){
            List<sms_send_smsImpl> list = new ArrayList<sms_send_smsImpl>();
            for(Isms_send_sms isms_send_sms :sms_send_sms){
                list.add((sms_send_smsImpl)isms_send_sms) ;
            }
            sms_send_smsFeignClient.updateBatch(list) ;
        }
    }


    public void get(Isms_send_sms sms_send_sms){
        Isms_send_sms clientModel = sms_send_smsFeignClient.get(sms_send_sms.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_send_sms.getClass(), false);
        copier.copy(clientModel, sms_send_sms, null);
    }


    public void create(Isms_send_sms sms_send_sms){
        Isms_send_sms clientModel = sms_send_smsFeignClient.create((sms_send_smsImpl)sms_send_sms) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_send_sms.getClass(), false);
        copier.copy(clientModel, sms_send_sms, null);
    }


    public void removeBatch(List<Isms_send_sms> sms_send_sms){
        if(sms_send_sms!=null){
            List<sms_send_smsImpl> list = new ArrayList<sms_send_smsImpl>();
            for(Isms_send_sms isms_send_sms :sms_send_sms){
                list.add((sms_send_smsImpl)isms_send_sms) ;
            }
            sms_send_smsFeignClient.removeBatch(list) ;
        }
    }


    public void update(Isms_send_sms sms_send_sms){
        Isms_send_sms clientModel = sms_send_smsFeignClient.update(sms_send_sms.getId(),(sms_send_smsImpl)sms_send_sms) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_send_sms.getClass(), false);
        copier.copy(clientModel, sms_send_sms, null);
    }


    public void createBatch(List<Isms_send_sms> sms_send_sms){
        if(sms_send_sms!=null){
            List<sms_send_smsImpl> list = new ArrayList<sms_send_smsImpl>();
            for(Isms_send_sms isms_send_sms :sms_send_sms){
                list.add((sms_send_smsImpl)isms_send_sms) ;
            }
            sms_send_smsFeignClient.createBatch(list) ;
        }
    }


    public Page<Isms_send_sms> fetchDefault(SearchContext context){
        Page<sms_send_smsImpl> page = this.sms_send_smsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Isms_send_sms> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isms_send_sms sms_send_sms){
        Isms_send_sms clientModel = sms_send_smsFeignClient.getDraft(sms_send_sms.getId(),(sms_send_smsImpl)sms_send_sms) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_send_sms.getClass(), false);
        copier.copy(clientModel, sms_send_sms, null);
    }



}

