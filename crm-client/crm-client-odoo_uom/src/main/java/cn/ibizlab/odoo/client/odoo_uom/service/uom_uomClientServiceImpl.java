package cn.ibizlab.odoo.client.odoo_uom.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iuom_uom;
import cn.ibizlab.odoo.client.odoo_uom.config.odoo_uomClientProperties;
import cn.ibizlab.odoo.core.client.service.Iuom_uomClientService;
import cn.ibizlab.odoo.client.odoo_uom.model.uom_uomImpl;
import cn.ibizlab.odoo.client.odoo_uom.feign.uom_uomFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[uom_uom] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class uom_uomClientServiceImpl implements Iuom_uomClientService {

    uom_uomFeignClient uom_uomFeignClient;

    @Autowired
    public uom_uomClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_uomClientProperties odoo_uomClientProperties) {
        if (odoo_uomClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.uom_uomFeignClient = nameBuilder.target(uom_uomFeignClient.class,"http://"+odoo_uomClientProperties.getServiceId()+"/") ;
		}else if (odoo_uomClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.uom_uomFeignClient = nameBuilder.target(uom_uomFeignClient.class,odoo_uomClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iuom_uom createModel() {
		return new uom_uomImpl();
	}


    public void createBatch(List<Iuom_uom> uom_uoms){
        if(uom_uoms!=null){
            List<uom_uomImpl> list = new ArrayList<uom_uomImpl>();
            for(Iuom_uom iuom_uom :uom_uoms){
                list.add((uom_uomImpl)iuom_uom) ;
            }
            uom_uomFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iuom_uom uom_uom){
        uom_uomFeignClient.remove(uom_uom.getId()) ;
    }


    public void updateBatch(List<Iuom_uom> uom_uoms){
        if(uom_uoms!=null){
            List<uom_uomImpl> list = new ArrayList<uom_uomImpl>();
            for(Iuom_uom iuom_uom :uom_uoms){
                list.add((uom_uomImpl)iuom_uom) ;
            }
            uom_uomFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iuom_uom uom_uom){
        Iuom_uom clientModel = uom_uomFeignClient.create((uom_uomImpl)uom_uom) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_uom.getClass(), false);
        copier.copy(clientModel, uom_uom, null);
    }


    public Page<Iuom_uom> fetchDefault(SearchContext context){
        Page<uom_uomImpl> page = this.uom_uomFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iuom_uom uom_uom){
        Iuom_uom clientModel = uom_uomFeignClient.get(uom_uom.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_uom.getClass(), false);
        copier.copy(clientModel, uom_uom, null);
    }


    public void removeBatch(List<Iuom_uom> uom_uoms){
        if(uom_uoms!=null){
            List<uom_uomImpl> list = new ArrayList<uom_uomImpl>();
            for(Iuom_uom iuom_uom :uom_uoms){
                list.add((uom_uomImpl)iuom_uom) ;
            }
            uom_uomFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iuom_uom uom_uom){
        Iuom_uom clientModel = uom_uomFeignClient.update(uom_uom.getId(),(uom_uomImpl)uom_uom) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_uom.getClass(), false);
        copier.copy(clientModel, uom_uom, null);
    }


    public Page<Iuom_uom> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iuom_uom uom_uom){
        Iuom_uom clientModel = uom_uomFeignClient.getDraft(uom_uom.getId(),(uom_uomImpl)uom_uom) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_uom.getClass(), false);
        copier.copy(clientModel, uom_uom, null);
    }



}

