package cn.ibizlab.odoo.client.odoo_uom.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iuom_category;
import cn.ibizlab.odoo.client.odoo_uom.config.odoo_uomClientProperties;
import cn.ibizlab.odoo.core.client.service.Iuom_categoryClientService;
import cn.ibizlab.odoo.client.odoo_uom.model.uom_categoryImpl;
import cn.ibizlab.odoo.client.odoo_uom.feign.uom_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[uom_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class uom_categoryClientServiceImpl implements Iuom_categoryClientService {

    uom_categoryFeignClient uom_categoryFeignClient;

    @Autowired
    public uom_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_uomClientProperties odoo_uomClientProperties) {
        if (odoo_uomClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.uom_categoryFeignClient = nameBuilder.target(uom_categoryFeignClient.class,"http://"+odoo_uomClientProperties.getServiceId()+"/") ;
		}else if (odoo_uomClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.uom_categoryFeignClient = nameBuilder.target(uom_categoryFeignClient.class,odoo_uomClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iuom_category createModel() {
		return new uom_categoryImpl();
	}


    public void createBatch(List<Iuom_category> uom_categories){
        if(uom_categories!=null){
            List<uom_categoryImpl> list = new ArrayList<uom_categoryImpl>();
            for(Iuom_category iuom_category :uom_categories){
                list.add((uom_categoryImpl)iuom_category) ;
            }
            uom_categoryFeignClient.createBatch(list) ;
        }
    }


    public void get(Iuom_category uom_category){
        Iuom_category clientModel = uom_categoryFeignClient.get(uom_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_category.getClass(), false);
        copier.copy(clientModel, uom_category, null);
    }


    public void updateBatch(List<Iuom_category> uom_categories){
        if(uom_categories!=null){
            List<uom_categoryImpl> list = new ArrayList<uom_categoryImpl>();
            for(Iuom_category iuom_category :uom_categories){
                list.add((uom_categoryImpl)iuom_category) ;
            }
            uom_categoryFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iuom_category uom_category){
        uom_categoryFeignClient.remove(uom_category.getId()) ;
    }


    public void create(Iuom_category uom_category){
        Iuom_category clientModel = uom_categoryFeignClient.create((uom_categoryImpl)uom_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_category.getClass(), false);
        copier.copy(clientModel, uom_category, null);
    }


    public Page<Iuom_category> fetchDefault(SearchContext context){
        Page<uom_categoryImpl> page = this.uom_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iuom_category> uom_categories){
        if(uom_categories!=null){
            List<uom_categoryImpl> list = new ArrayList<uom_categoryImpl>();
            for(Iuom_category iuom_category :uom_categories){
                list.add((uom_categoryImpl)iuom_category) ;
            }
            uom_categoryFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iuom_category uom_category){
        Iuom_category clientModel = uom_categoryFeignClient.update(uom_category.getId(),(uom_categoryImpl)uom_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_category.getClass(), false);
        copier.copy(clientModel, uom_category, null);
    }


    public Page<Iuom_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iuom_category uom_category){
        Iuom_category clientModel = uom_categoryFeignClient.getDraft(uom_category.getId(),(uom_categoryImpl)uom_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), uom_category.getClass(), false);
        copier.copy(clientModel, uom_category, null);
    }



}

