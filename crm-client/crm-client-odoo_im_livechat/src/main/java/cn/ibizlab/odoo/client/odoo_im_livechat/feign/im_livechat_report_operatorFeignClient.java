package cn.ibizlab.odoo.client.odoo_im_livechat.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_operator;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_report_operatorImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[im_livechat_report_operator] 服务对象接口
 */
public interface im_livechat_report_operatorFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_operators/fetchdefault")
    public Page<im_livechat_report_operatorImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_operators/createbatch")
    public im_livechat_report_operatorImpl createBatch(@RequestBody List<im_livechat_report_operatorImpl> im_livechat_report_operators);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_operators/updatebatch")
    public im_livechat_report_operatorImpl updateBatch(@RequestBody List<im_livechat_report_operatorImpl> im_livechat_report_operators);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_operators/removebatch")
    public im_livechat_report_operatorImpl removeBatch(@RequestBody List<im_livechat_report_operatorImpl> im_livechat_report_operators);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_operators/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_operators")
    public im_livechat_report_operatorImpl create(@RequestBody im_livechat_report_operatorImpl im_livechat_report_operator);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_operators/{id}")
    public im_livechat_report_operatorImpl update(@PathVariable("id") Integer id,@RequestBody im_livechat_report_operatorImpl im_livechat_report_operator);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_operators/{id}")
    public im_livechat_report_operatorImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_operators/select")
    public Page<im_livechat_report_operatorImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_operators/{id}/getdraft")
    public im_livechat_report_operatorImpl getDraft(@PathVariable("id") Integer id,@RequestBody im_livechat_report_operatorImpl im_livechat_report_operator);



}
