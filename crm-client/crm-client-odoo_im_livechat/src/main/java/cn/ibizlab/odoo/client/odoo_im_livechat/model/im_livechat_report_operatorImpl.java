package cn.ibizlab.odoo.client.odoo_im_livechat.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_operator;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[im_livechat_report_operator] 对象
 */
public class im_livechat_report_operatorImpl implements Iim_livechat_report_operator,Serializable{

    /**
     * 对话
     */
    public Integer channel_id;

    @JsonIgnore
    public boolean channel_idDirtyFlag;
    
    /**
     * 对话
     */
    public String channel_id_text;

    @JsonIgnore
    public boolean channel_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 平均时间
     */
    public Double duration;

    @JsonIgnore
    public boolean durationDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 渠道
     */
    public Integer livechat_channel_id;

    @JsonIgnore
    public boolean livechat_channel_idDirtyFlag;
    
    /**
     * 渠道
     */
    public String livechat_channel_id_text;

    @JsonIgnore
    public boolean livechat_channel_id_textDirtyFlag;
    
    /**
     * # 会话
     */
    public Integer nbr_channel;

    @JsonIgnore
    public boolean nbr_channelDirtyFlag;
    
    /**
     * 运算符
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 运算符
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 会话的开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp start_date;

    @JsonIgnore
    public boolean start_dateDirtyFlag;
    
    /**
     * 该回答了
     */
    public Double time_to_answer;

    @JsonIgnore
    public boolean time_to_answerDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [对话]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return this.channel_id ;
    }

    /**
     * 设置 [对话]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [对话]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return this.channel_idDirtyFlag ;
    }   

    /**
     * 获取 [对话]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return this.channel_id_text ;
    }

    /**
     * 设置 [对话]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [对话]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return this.channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [平均时间]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return this.duration ;
    }

    /**
     * 设置 [平均时间]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

     /**
     * 获取 [平均时间]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return this.durationDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("livechat_channel_id")
    public Integer getLivechat_channel_id(){
        return this.livechat_channel_id ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("livechat_channel_id")
    public void setLivechat_channel_id(Integer  livechat_channel_id){
        this.livechat_channel_id = livechat_channel_id ;
        this.livechat_channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_idDirtyFlag(){
        return this.livechat_channel_idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("livechat_channel_id_text")
    public String getLivechat_channel_id_text(){
        return this.livechat_channel_id_text ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("livechat_channel_id_text")
    public void setLivechat_channel_id_text(String  livechat_channel_id_text){
        this.livechat_channel_id_text = livechat_channel_id_text ;
        this.livechat_channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_id_textDirtyFlag(){
        return this.livechat_channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [# 会话]
     */
    @JsonProperty("nbr_channel")
    public Integer getNbr_channel(){
        return this.nbr_channel ;
    }

    /**
     * 设置 [# 会话]
     */
    @JsonProperty("nbr_channel")
    public void setNbr_channel(Integer  nbr_channel){
        this.nbr_channel = nbr_channel ;
        this.nbr_channelDirtyFlag = true ;
    }

     /**
     * 获取 [# 会话]脏标记
     */
    @JsonIgnore
    public boolean getNbr_channelDirtyFlag(){
        return this.nbr_channelDirtyFlag ;
    }   

    /**
     * 获取 [运算符]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [运算符]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [运算符]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [运算符]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [运算符]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [运算符]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [会话的开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [会话的开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [会话的开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }   

    /**
     * 获取 [该回答了]
     */
    @JsonProperty("time_to_answer")
    public Double getTime_to_answer(){
        return this.time_to_answer ;
    }

    /**
     * 设置 [该回答了]
     */
    @JsonProperty("time_to_answer")
    public void setTime_to_answer(Double  time_to_answer){
        this.time_to_answer = time_to_answer ;
        this.time_to_answerDirtyFlag = true ;
    }

     /**
     * 获取 [该回答了]脏标记
     */
    @JsonIgnore
    public boolean getTime_to_answerDirtyFlag(){
        return this.time_to_answerDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
