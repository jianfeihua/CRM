package cn.ibizlab.odoo.client.odoo_repair.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Irepair_order;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_orderImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[repair_order] 服务对象接口
 */
public interface repair_orderFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_orders/createbatch")
    public repair_orderImpl createBatch(@RequestBody List<repair_orderImpl> repair_orders);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_orders/{id}")
    public repair_orderImpl update(@PathVariable("id") Integer id,@RequestBody repair_orderImpl repair_order);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_orders/removebatch")
    public repair_orderImpl removeBatch(@RequestBody List<repair_orderImpl> repair_orders);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_orders/updatebatch")
    public repair_orderImpl updateBatch(@RequestBody List<repair_orderImpl> repair_orders);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_orders")
    public repair_orderImpl create(@RequestBody repair_orderImpl repair_order);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_orders/fetchdefault")
    public Page<repair_orderImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_orders/{id}")
    public repair_orderImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_orders/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_orders/select")
    public Page<repair_orderImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_orders/{id}/getdraft")
    public repair_orderImpl getDraft(@PathVariable("id") Integer id,@RequestBody repair_orderImpl repair_order);



}
