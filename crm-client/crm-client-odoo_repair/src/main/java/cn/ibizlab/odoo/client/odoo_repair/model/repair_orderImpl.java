package cn.ibizlab.odoo.client.odoo_repair.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Irepair_order;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[repair_order] 对象
 */
public class repair_orderImpl implements Irepair_order,Serializable{

    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 收货地址
     */
    public Integer address_id;

    @JsonIgnore
    public boolean address_idDirtyFlag;
    
    /**
     * 收货地址
     */
    public String address_id_text;

    @JsonIgnore
    public boolean address_id_textDirtyFlag;
    
    /**
     * 税
     */
    public Double amount_tax;

    @JsonIgnore
    public boolean amount_taxDirtyFlag;
    
    /**
     * 合计
     */
    public Double amount_total;

    @JsonIgnore
    public boolean amount_totalDirtyFlag;
    
    /**
     * 未税金额
     */
    public Double amount_untaxed;

    @JsonIgnore
    public boolean amount_untaxedDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建者
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建者
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 默认地址
     */
    public Integer default_address_id;

    @JsonIgnore
    public boolean default_address_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 作业
     */
    public String fees_lines;

    @JsonIgnore
    public boolean fees_linesDirtyFlag;
    
    /**
     * 质保到期
     */
    public Timestamp guarantee_limit;

    @JsonIgnore
    public boolean guarantee_limitDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 内部备注
     */
    public String internal_notes;

    @JsonIgnore
    public boolean internal_notesDirtyFlag;
    
    /**
     * 已开票
     */
    public String invoiced;

    @JsonIgnore
    public boolean invoicedDirtyFlag;
    
    /**
     * 发票
     */
    public Integer invoice_id;

    @JsonIgnore
    public boolean invoice_idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_id_text;

    @JsonIgnore
    public boolean invoice_id_textDirtyFlag;
    
    /**
     * 开票方式
     */
    public String invoice_method;

    @JsonIgnore
    public boolean invoice_methodDirtyFlag;
    
    /**
     * 地点
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 地点
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 批次/序列号
     */
    public Integer lot_id;

    @JsonIgnore
    public boolean lot_idDirtyFlag;
    
    /**
     * 批次/序列号
     */
    public String lot_id_text;

    @JsonIgnore
    public boolean lot_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 移动
     */
    public Integer move_id;

    @JsonIgnore
    public boolean move_idDirtyFlag;
    
    /**
     * 移动
     */
    public String move_id_text;

    @JsonIgnore
    public boolean move_id_textDirtyFlag;
    
    /**
     * 维修参照
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 零件
     */
    public String operations;

    @JsonIgnore
    public boolean operationsDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 开票地址
     */
    public Integer partner_invoice_id;

    @JsonIgnore
    public boolean partner_invoice_idDirtyFlag;
    
    /**
     * 开票地址
     */
    public String partner_invoice_id_text;

    @JsonIgnore
    public boolean partner_invoice_id_textDirtyFlag;
    
    /**
     * 价格表
     */
    public Integer pricelist_id;

    @JsonIgnore
    public boolean pricelist_idDirtyFlag;
    
    /**
     * 价格表
     */
    public String pricelist_id_text;

    @JsonIgnore
    public boolean pricelist_id_textDirtyFlag;
    
    /**
     * 待维修产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 待维修产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 产品量度单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 产品量度单位
     */
    public String product_uom_text;

    @JsonIgnore
    public boolean product_uom_textDirtyFlag;
    
    /**
     * 报价单说明
     */
    public String quotation_notes;

    @JsonIgnore
    public boolean quotation_notesDirtyFlag;
    
    /**
     * 已维修
     */
    public String repaired;

    @JsonIgnore
    public boolean repairedDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 追踪
     */
    public String tracking;

    @JsonIgnore
    public boolean trackingDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [收货地址]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return this.address_id ;
    }

    /**
     * 设置 [收货地址]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

     /**
     * 获取 [收货地址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return this.address_idDirtyFlag ;
    }   

    /**
     * 获取 [收货地址]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return this.address_id_text ;
    }

    /**
     * 设置 [收货地址]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收货地址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return this.address_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return this.amount_tax ;
    }

    /**
     * 设置 [税]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

     /**
     * 获取 [税]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return this.amount_taxDirtyFlag ;
    }   

    /**
     * 获取 [合计]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return this.amount_total ;
    }

    /**
     * 设置 [合计]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

     /**
     * 获取 [合计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return this.amount_totalDirtyFlag ;
    }   

    /**
     * 获取 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return this.amount_untaxed ;
    }

    /**
     * 设置 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

     /**
     * 获取 [未税金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return this.amount_untaxedDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建者]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建者]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建者]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建者]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [默认地址]
     */
    @JsonProperty("default_address_id")
    public Integer getDefault_address_id(){
        return this.default_address_id ;
    }

    /**
     * 设置 [默认地址]
     */
    @JsonProperty("default_address_id")
    public void setDefault_address_id(Integer  default_address_id){
        this.default_address_id = default_address_id ;
        this.default_address_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认地址]脏标记
     */
    @JsonIgnore
    public boolean getDefault_address_idDirtyFlag(){
        return this.default_address_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [作业]
     */
    @JsonProperty("fees_lines")
    public String getFees_lines(){
        return this.fees_lines ;
    }

    /**
     * 设置 [作业]
     */
    @JsonProperty("fees_lines")
    public void setFees_lines(String  fees_lines){
        this.fees_lines = fees_lines ;
        this.fees_linesDirtyFlag = true ;
    }

     /**
     * 获取 [作业]脏标记
     */
    @JsonIgnore
    public boolean getFees_linesDirtyFlag(){
        return this.fees_linesDirtyFlag ;
    }   

    /**
     * 获取 [质保到期]
     */
    @JsonProperty("guarantee_limit")
    public Timestamp getGuarantee_limit(){
        return this.guarantee_limit ;
    }

    /**
     * 设置 [质保到期]
     */
    @JsonProperty("guarantee_limit")
    public void setGuarantee_limit(Timestamp  guarantee_limit){
        this.guarantee_limit = guarantee_limit ;
        this.guarantee_limitDirtyFlag = true ;
    }

     /**
     * 获取 [质保到期]脏标记
     */
    @JsonIgnore
    public boolean getGuarantee_limitDirtyFlag(){
        return this.guarantee_limitDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [内部备注]
     */
    @JsonProperty("internal_notes")
    public String getInternal_notes(){
        return this.internal_notes ;
    }

    /**
     * 设置 [内部备注]
     */
    @JsonProperty("internal_notes")
    public void setInternal_notes(String  internal_notes){
        this.internal_notes = internal_notes ;
        this.internal_notesDirtyFlag = true ;
    }

     /**
     * 获取 [内部备注]脏标记
     */
    @JsonIgnore
    public boolean getInternal_notesDirtyFlag(){
        return this.internal_notesDirtyFlag ;
    }   

    /**
     * 获取 [已开票]
     */
    @JsonProperty("invoiced")
    public String getInvoiced(){
        return this.invoiced ;
    }

    /**
     * 设置 [已开票]
     */
    @JsonProperty("invoiced")
    public void setInvoiced(String  invoiced){
        this.invoiced = invoiced ;
        this.invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [已开票]脏标记
     */
    @JsonIgnore
    public boolean getInvoicedDirtyFlag(){
        return this.invoicedDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return this.invoice_id ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return this.invoice_idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return this.invoice_id_text ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return this.invoice_id_textDirtyFlag ;
    }   

    /**
     * 获取 [开票方式]
     */
    @JsonProperty("invoice_method")
    public String getInvoice_method(){
        return this.invoice_method ;
    }

    /**
     * 设置 [开票方式]
     */
    @JsonProperty("invoice_method")
    public void setInvoice_method(String  invoice_method){
        this.invoice_method = invoice_method ;
        this.invoice_methodDirtyFlag = true ;
    }

     /**
     * 获取 [开票方式]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_methodDirtyFlag(){
        return this.invoice_methodDirtyFlag ;
    }   

    /**
     * 获取 [地点]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [地点]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return this.lot_id ;
    }

    /**
     * 设置 [批次/序列号]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return this.lot_idDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return this.lot_id_text ;
    }

    /**
     * 设置 [批次/序列号]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return this.lot_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [移动]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return this.move_id ;
    }

    /**
     * 设置 [移动]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

     /**
     * 获取 [移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return this.move_idDirtyFlag ;
    }   

    /**
     * 获取 [移动]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return this.move_id_text ;
    }

    /**
     * 设置 [移动]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return this.move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [维修参照]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [维修参照]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [维修参照]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [零件]
     */
    @JsonProperty("operations")
    public String getOperations(){
        return this.operations ;
    }

    /**
     * 设置 [零件]
     */
    @JsonProperty("operations")
    public void setOperations(String  operations){
        this.operations = operations ;
        this.operationsDirtyFlag = true ;
    }

     /**
     * 获取 [零件]脏标记
     */
    @JsonIgnore
    public boolean getOperationsDirtyFlag(){
        return this.operationsDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [开票地址]
     */
    @JsonProperty("partner_invoice_id")
    public Integer getPartner_invoice_id(){
        return this.partner_invoice_id ;
    }

    /**
     * 设置 [开票地址]
     */
    @JsonProperty("partner_invoice_id")
    public void setPartner_invoice_id(Integer  partner_invoice_id){
        this.partner_invoice_id = partner_invoice_id ;
        this.partner_invoice_idDirtyFlag = true ;
    }

     /**
     * 获取 [开票地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_idDirtyFlag(){
        return this.partner_invoice_idDirtyFlag ;
    }   

    /**
     * 获取 [开票地址]
     */
    @JsonProperty("partner_invoice_id_text")
    public String getPartner_invoice_id_text(){
        return this.partner_invoice_id_text ;
    }

    /**
     * 设置 [开票地址]
     */
    @JsonProperty("partner_invoice_id_text")
    public void setPartner_invoice_id_text(String  partner_invoice_id_text){
        this.partner_invoice_id_text = partner_invoice_id_text ;
        this.partner_invoice_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [开票地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_id_textDirtyFlag(){
        return this.partner_invoice_id_textDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return this.pricelist_id ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return this.pricelist_idDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return this.pricelist_id_text ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return this.pricelist_id_textDirtyFlag ;
    }   

    /**
     * 获取 [待维修产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [待维修产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [待维修产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [待维修产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [待维修产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [待维修产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [产品量度单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [产品量度单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [产品量度单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [产品量度单位]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return this.product_uom_text ;
    }

    /**
     * 设置 [产品量度单位]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品量度单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return this.product_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [报价单说明]
     */
    @JsonProperty("quotation_notes")
    public String getQuotation_notes(){
        return this.quotation_notes ;
    }

    /**
     * 设置 [报价单说明]
     */
    @JsonProperty("quotation_notes")
    public void setQuotation_notes(String  quotation_notes){
        this.quotation_notes = quotation_notes ;
        this.quotation_notesDirtyFlag = true ;
    }

     /**
     * 获取 [报价单说明]脏标记
     */
    @JsonIgnore
    public boolean getQuotation_notesDirtyFlag(){
        return this.quotation_notesDirtyFlag ;
    }   

    /**
     * 获取 [已维修]
     */
    @JsonProperty("repaired")
    public String getRepaired(){
        return this.repaired ;
    }

    /**
     * 设置 [已维修]
     */
    @JsonProperty("repaired")
    public void setRepaired(String  repaired){
        this.repaired = repaired ;
        this.repairedDirtyFlag = true ;
    }

     /**
     * 获取 [已维修]脏标记
     */
    @JsonIgnore
    public boolean getRepairedDirtyFlag(){
        return this.repairedDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [追踪]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return this.tracking ;
    }

    /**
     * 设置 [追踪]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

     /**
     * 获取 [追踪]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return this.trackingDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改时间]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改时间]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改时间]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
