package cn.ibizlab.odoo.client.odoo_repair.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Irepair_line;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[repair_line] 服务对象接口
 */
public interface repair_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_lines/updatebatch")
    public repair_lineImpl updateBatch(@RequestBody List<repair_lineImpl> repair_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_lines/createbatch")
    public repair_lineImpl createBatch(@RequestBody List<repair_lineImpl> repair_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_lines/fetchdefault")
    public Page<repair_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_lines/{id}")
    public repair_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_lines/{id}")
    public repair_lineImpl update(@PathVariable("id") Integer id,@RequestBody repair_lineImpl repair_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_lines/removebatch")
    public repair_lineImpl removeBatch(@RequestBody List<repair_lineImpl> repair_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_lines")
    public repair_lineImpl create(@RequestBody repair_lineImpl repair_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_lines/select")
    public Page<repair_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_lines/{id}/getdraft")
    public repair_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody repair_lineImpl repair_line);



}
