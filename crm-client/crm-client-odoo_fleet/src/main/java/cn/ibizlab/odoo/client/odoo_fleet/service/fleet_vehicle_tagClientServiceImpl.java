package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_tag;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_tagClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_tagImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_tagFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_tag] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_tagClientServiceImpl implements Ifleet_vehicle_tagClientService {

    fleet_vehicle_tagFeignClient fleet_vehicle_tagFeignClient;

    @Autowired
    public fleet_vehicle_tagClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_tagFeignClient = nameBuilder.target(fleet_vehicle_tagFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_tagFeignClient = nameBuilder.target(fleet_vehicle_tagFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_tag createModel() {
		return new fleet_vehicle_tagImpl();
	}


    public void createBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags){
        if(fleet_vehicle_tags!=null){
            List<fleet_vehicle_tagImpl> list = new ArrayList<fleet_vehicle_tagImpl>();
            for(Ifleet_vehicle_tag ifleet_vehicle_tag :fleet_vehicle_tags){
                list.add((fleet_vehicle_tagImpl)ifleet_vehicle_tag) ;
            }
            fleet_vehicle_tagFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags){
        if(fleet_vehicle_tags!=null){
            List<fleet_vehicle_tagImpl> list = new ArrayList<fleet_vehicle_tagImpl>();
            for(Ifleet_vehicle_tag ifleet_vehicle_tag :fleet_vehicle_tags){
                list.add((fleet_vehicle_tagImpl)ifleet_vehicle_tag) ;
            }
            fleet_vehicle_tagFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ifleet_vehicle_tag fleet_vehicle_tag){
        fleet_vehicle_tagFeignClient.remove(fleet_vehicle_tag.getId()) ;
    }


    public Page<Ifleet_vehicle_tag> fetchDefault(SearchContext context){
        Page<fleet_vehicle_tagImpl> page = this.fleet_vehicle_tagFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ifleet_vehicle_tag fleet_vehicle_tag){
        Ifleet_vehicle_tag clientModel = fleet_vehicle_tagFeignClient.create((fleet_vehicle_tagImpl)fleet_vehicle_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_tag.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_tag, null);
    }


    public void updateBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags){
        if(fleet_vehicle_tags!=null){
            List<fleet_vehicle_tagImpl> list = new ArrayList<fleet_vehicle_tagImpl>();
            for(Ifleet_vehicle_tag ifleet_vehicle_tag :fleet_vehicle_tags){
                list.add((fleet_vehicle_tagImpl)ifleet_vehicle_tag) ;
            }
            fleet_vehicle_tagFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ifleet_vehicle_tag fleet_vehicle_tag){
        Ifleet_vehicle_tag clientModel = fleet_vehicle_tagFeignClient.update(fleet_vehicle_tag.getId(),(fleet_vehicle_tagImpl)fleet_vehicle_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_tag.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_tag, null);
    }


    public void get(Ifleet_vehicle_tag fleet_vehicle_tag){
        Ifleet_vehicle_tag clientModel = fleet_vehicle_tagFeignClient.get(fleet_vehicle_tag.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_tag.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_tag, null);
    }


    public Page<Ifleet_vehicle_tag> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_tag fleet_vehicle_tag){
        Ifleet_vehicle_tag clientModel = fleet_vehicle_tagFeignClient.getDraft(fleet_vehicle_tag.getId(),(fleet_vehicle_tagImpl)fleet_vehicle_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_tag.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_tag, null);
    }



}

