package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_odometer;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_odometerClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_odometerImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_odometerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_odometer] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_odometerClientServiceImpl implements Ifleet_vehicle_odometerClientService {

    fleet_vehicle_odometerFeignClient fleet_vehicle_odometerFeignClient;

    @Autowired
    public fleet_vehicle_odometerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_odometerFeignClient = nameBuilder.target(fleet_vehicle_odometerFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_odometerFeignClient = nameBuilder.target(fleet_vehicle_odometerFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_odometer createModel() {
		return new fleet_vehicle_odometerImpl();
	}


    public void removeBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers){
        if(fleet_vehicle_odometers!=null){
            List<fleet_vehicle_odometerImpl> list = new ArrayList<fleet_vehicle_odometerImpl>();
            for(Ifleet_vehicle_odometer ifleet_vehicle_odometer :fleet_vehicle_odometers){
                list.add((fleet_vehicle_odometerImpl)ifleet_vehicle_odometer) ;
            }
            fleet_vehicle_odometerFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers){
        if(fleet_vehicle_odometers!=null){
            List<fleet_vehicle_odometerImpl> list = new ArrayList<fleet_vehicle_odometerImpl>();
            for(Ifleet_vehicle_odometer ifleet_vehicle_odometer :fleet_vehicle_odometers){
                list.add((fleet_vehicle_odometerImpl)ifleet_vehicle_odometer) ;
            }
            fleet_vehicle_odometerFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ifleet_vehicle_odometer fleet_vehicle_odometer){
        fleet_vehicle_odometerFeignClient.remove(fleet_vehicle_odometer.getId()) ;
    }


    public void updateBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers){
        if(fleet_vehicle_odometers!=null){
            List<fleet_vehicle_odometerImpl> list = new ArrayList<fleet_vehicle_odometerImpl>();
            for(Ifleet_vehicle_odometer ifleet_vehicle_odometer :fleet_vehicle_odometers){
                list.add((fleet_vehicle_odometerImpl)ifleet_vehicle_odometer) ;
            }
            fleet_vehicle_odometerFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ifleet_vehicle_odometer fleet_vehicle_odometer){
        Ifleet_vehicle_odometer clientModel = fleet_vehicle_odometerFeignClient.create((fleet_vehicle_odometerImpl)fleet_vehicle_odometer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_odometer.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_odometer, null);
    }


    public void get(Ifleet_vehicle_odometer fleet_vehicle_odometer){
        Ifleet_vehicle_odometer clientModel = fleet_vehicle_odometerFeignClient.get(fleet_vehicle_odometer.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_odometer.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_odometer, null);
    }


    public void update(Ifleet_vehicle_odometer fleet_vehicle_odometer){
        Ifleet_vehicle_odometer clientModel = fleet_vehicle_odometerFeignClient.update(fleet_vehicle_odometer.getId(),(fleet_vehicle_odometerImpl)fleet_vehicle_odometer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_odometer.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_odometer, null);
    }


    public Page<Ifleet_vehicle_odometer> fetchDefault(SearchContext context){
        Page<fleet_vehicle_odometerImpl> page = this.fleet_vehicle_odometerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ifleet_vehicle_odometer> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_odometer fleet_vehicle_odometer){
        Ifleet_vehicle_odometer clientModel = fleet_vehicle_odometerFeignClient.getDraft(fleet_vehicle_odometer.getId(),(fleet_vehicle_odometerImpl)fleet_vehicle_odometer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_odometer.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_odometer, null);
    }



}

