package cn.ibizlab.odoo.client.odoo_fleet.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[fleet_vehicle] 对象
 */
public class fleet_vehicleImpl implements Ifleet_vehicle,Serializable{

    /**
     * 注册日期
     */
    public Timestamp acquisition_date;

    @JsonIgnore
    public boolean acquisition_dateDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 品牌
     */
    public Integer brand_id;

    @JsonIgnore
    public boolean brand_idDirtyFlag;
    
    /**
     * 品牌
     */
    public String brand_id_text;

    @JsonIgnore
    public boolean brand_id_textDirtyFlag;
    
    /**
     * 目录值（包括增值税）
     */
    public Double car_value;

    @JsonIgnore
    public boolean car_valueDirtyFlag;
    
    /**
     * 二氧化碳排放量
     */
    public Double co2;

    @JsonIgnore
    public boolean co2DirtyFlag;
    
    /**
     * 颜色
     */
    public String color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 合同统计
     */
    public Integer contract_count;

    @JsonIgnore
    public boolean contract_countDirtyFlag;
    
    /**
     * 有合同待续签
     */
    public String contract_renewal_due_soon;

    @JsonIgnore
    public boolean contract_renewal_due_soonDirtyFlag;
    
    /**
     * 需马上续签合同的名称
     */
    public String contract_renewal_name;

    @JsonIgnore
    public boolean contract_renewal_nameDirtyFlag;
    
    /**
     * 有逾期合同
     */
    public String contract_renewal_overdue;

    @JsonIgnore
    public boolean contract_renewal_overdueDirtyFlag;
    
    /**
     * 截止或者逾期减一的合同总计
     */
    public String contract_renewal_total;

    @JsonIgnore
    public boolean contract_renewal_totalDirtyFlag;
    
    /**
     * 费用
     */
    public Integer cost_count;

    @JsonIgnore
    public boolean cost_countDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 车门数量
     */
    public Integer doors;

    @JsonIgnore
    public boolean doorsDirtyFlag;
    
    /**
     * 驾驶员
     */
    public Integer driver_id;

    @JsonIgnore
    public boolean driver_idDirtyFlag;
    
    /**
     * 驾驶员
     */
    public String driver_id_text;

    @JsonIgnore
    public boolean driver_id_textDirtyFlag;
    
    /**
     * 首次合同日期
     */
    public Timestamp first_contract_date;

    @JsonIgnore
    public boolean first_contract_dateDirtyFlag;
    
    /**
     * 加油记录统计
     */
    public Integer fuel_logs_count;

    @JsonIgnore
    public boolean fuel_logs_countDirtyFlag;
    
    /**
     * 燃油类型
     */
    public String fuel_type;

    @JsonIgnore
    public boolean fuel_typeDirtyFlag;
    
    /**
     * 马力
     */
    public Integer horsepower;

    @JsonIgnore
    public boolean horsepowerDirtyFlag;
    
    /**
     * 车船税
     */
    public Double horsepower_tax;

    @JsonIgnore
    public boolean horsepower_taxDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 徽标
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 车辆标志图片(普通大小)
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 车辆标志图片(小图片)
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * 车辆牌照
     */
    public String license_plate;

    @JsonIgnore
    public boolean license_plateDirtyFlag;
    
    /**
     * 地点
     */
    public String location;

    @JsonIgnore
    public boolean locationDirtyFlag;
    
    /**
     * 合同
     */
    public String log_contracts;

    @JsonIgnore
    public boolean log_contractsDirtyFlag;
    
    /**
     * 指派记录
     */
    public String log_drivers;

    @JsonIgnore
    public boolean log_driversDirtyFlag;
    
    /**
     * 燃油记录
     */
    public String log_fuel;

    @JsonIgnore
    public boolean log_fuelDirtyFlag;
    
    /**
     * 服务记录
     */
    public String log_services;

    @JsonIgnore
    public boolean log_servicesDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要激活
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 型号
     */
    public Integer model_id;

    @JsonIgnore
    public boolean model_idDirtyFlag;
    
    /**
     * 型号
     */
    public String model_id_text;

    @JsonIgnore
    public boolean model_id_textDirtyFlag;
    
    /**
     * 型号年份
     */
    public String model_year;

    @JsonIgnore
    public boolean model_yearDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 最新里程表
     */
    public Double odometer;

    @JsonIgnore
    public boolean odometerDirtyFlag;
    
    /**
     * 里程表
     */
    public Integer odometer_count;

    @JsonIgnore
    public boolean odometer_countDirtyFlag;
    
    /**
     * 里程表单位
     */
    public String odometer_unit;

    @JsonIgnore
    public boolean odometer_unitDirtyFlag;
    
    /**
     * 动力
     */
    public Integer power;

    @JsonIgnore
    public boolean powerDirtyFlag;
    
    /**
     * 残余价值
     */
    public Double residual_value;

    @JsonIgnore
    public boolean residual_valueDirtyFlag;
    
    /**
     * 座位数
     */
    public Integer seats;

    @JsonIgnore
    public boolean seatsDirtyFlag;
    
    /**
     * 服务
     */
    public Integer service_count;

    @JsonIgnore
    public boolean service_countDirtyFlag;
    
    /**
     * 状态
     */
    public Integer state_id;

    @JsonIgnore
    public boolean state_idDirtyFlag;
    
    /**
     * 状态
     */
    public String state_id_text;

    @JsonIgnore
    public boolean state_id_textDirtyFlag;
    
    /**
     * 标签
     */
    public String tag_ids;

    @JsonIgnore
    public boolean tag_idsDirtyFlag;
    
    /**
     * 变速器
     */
    public String transmission;

    @JsonIgnore
    public boolean transmissionDirtyFlag;
    
    /**
     * 车架号
     */
    public String vin_sn;

    @JsonIgnore
    public boolean vin_snDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [注册日期]
     */
    @JsonProperty("acquisition_date")
    public Timestamp getAcquisition_date(){
        return this.acquisition_date ;
    }

    /**
     * 设置 [注册日期]
     */
    @JsonProperty("acquisition_date")
    public void setAcquisition_date(Timestamp  acquisition_date){
        this.acquisition_date = acquisition_date ;
        this.acquisition_dateDirtyFlag = true ;
    }

     /**
     * 获取 [注册日期]脏标记
     */
    @JsonIgnore
    public boolean getAcquisition_dateDirtyFlag(){
        return this.acquisition_dateDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [品牌]
     */
    @JsonProperty("brand_id")
    public Integer getBrand_id(){
        return this.brand_id ;
    }

    /**
     * 设置 [品牌]
     */
    @JsonProperty("brand_id")
    public void setBrand_id(Integer  brand_id){
        this.brand_id = brand_id ;
        this.brand_idDirtyFlag = true ;
    }

     /**
     * 获取 [品牌]脏标记
     */
    @JsonIgnore
    public boolean getBrand_idDirtyFlag(){
        return this.brand_idDirtyFlag ;
    }   

    /**
     * 获取 [品牌]
     */
    @JsonProperty("brand_id_text")
    public String getBrand_id_text(){
        return this.brand_id_text ;
    }

    /**
     * 设置 [品牌]
     */
    @JsonProperty("brand_id_text")
    public void setBrand_id_text(String  brand_id_text){
        this.brand_id_text = brand_id_text ;
        this.brand_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [品牌]脏标记
     */
    @JsonIgnore
    public boolean getBrand_id_textDirtyFlag(){
        return this.brand_id_textDirtyFlag ;
    }   

    /**
     * 获取 [目录值（包括增值税）]
     */
    @JsonProperty("car_value")
    public Double getCar_value(){
        return this.car_value ;
    }

    /**
     * 设置 [目录值（包括增值税）]
     */
    @JsonProperty("car_value")
    public void setCar_value(Double  car_value){
        this.car_value = car_value ;
        this.car_valueDirtyFlag = true ;
    }

     /**
     * 获取 [目录值（包括增值税）]脏标记
     */
    @JsonIgnore
    public boolean getCar_valueDirtyFlag(){
        return this.car_valueDirtyFlag ;
    }   

    /**
     * 获取 [二氧化碳排放量]
     */
    @JsonProperty("co2")
    public Double getCo2(){
        return this.co2 ;
    }

    /**
     * 设置 [二氧化碳排放量]
     */
    @JsonProperty("co2")
    public void setCo2(Double  co2){
        this.co2 = co2 ;
        this.co2DirtyFlag = true ;
    }

     /**
     * 获取 [二氧化碳排放量]脏标记
     */
    @JsonIgnore
    public boolean getCo2DirtyFlag(){
        return this.co2DirtyFlag ;
    }   

    /**
     * 获取 [颜色]
     */
    @JsonProperty("color")
    public String getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色]
     */
    @JsonProperty("color")
    public void setColor(String  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [合同统计]
     */
    @JsonProperty("contract_count")
    public Integer getContract_count(){
        return this.contract_count ;
    }

    /**
     * 设置 [合同统计]
     */
    @JsonProperty("contract_count")
    public void setContract_count(Integer  contract_count){
        this.contract_count = contract_count ;
        this.contract_countDirtyFlag = true ;
    }

     /**
     * 获取 [合同统计]脏标记
     */
    @JsonIgnore
    public boolean getContract_countDirtyFlag(){
        return this.contract_countDirtyFlag ;
    }   

    /**
     * 获取 [有合同待续签]
     */
    @JsonProperty("contract_renewal_due_soon")
    public String getContract_renewal_due_soon(){
        return this.contract_renewal_due_soon ;
    }

    /**
     * 设置 [有合同待续签]
     */
    @JsonProperty("contract_renewal_due_soon")
    public void setContract_renewal_due_soon(String  contract_renewal_due_soon){
        this.contract_renewal_due_soon = contract_renewal_due_soon ;
        this.contract_renewal_due_soonDirtyFlag = true ;
    }

     /**
     * 获取 [有合同待续签]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_due_soonDirtyFlag(){
        return this.contract_renewal_due_soonDirtyFlag ;
    }   

    /**
     * 获取 [需马上续签合同的名称]
     */
    @JsonProperty("contract_renewal_name")
    public String getContract_renewal_name(){
        return this.contract_renewal_name ;
    }

    /**
     * 设置 [需马上续签合同的名称]
     */
    @JsonProperty("contract_renewal_name")
    public void setContract_renewal_name(String  contract_renewal_name){
        this.contract_renewal_name = contract_renewal_name ;
        this.contract_renewal_nameDirtyFlag = true ;
    }

     /**
     * 获取 [需马上续签合同的名称]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_nameDirtyFlag(){
        return this.contract_renewal_nameDirtyFlag ;
    }   

    /**
     * 获取 [有逾期合同]
     */
    @JsonProperty("contract_renewal_overdue")
    public String getContract_renewal_overdue(){
        return this.contract_renewal_overdue ;
    }

    /**
     * 设置 [有逾期合同]
     */
    @JsonProperty("contract_renewal_overdue")
    public void setContract_renewal_overdue(String  contract_renewal_overdue){
        this.contract_renewal_overdue = contract_renewal_overdue ;
        this.contract_renewal_overdueDirtyFlag = true ;
    }

     /**
     * 获取 [有逾期合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_overdueDirtyFlag(){
        return this.contract_renewal_overdueDirtyFlag ;
    }   

    /**
     * 获取 [截止或者逾期减一的合同总计]
     */
    @JsonProperty("contract_renewal_total")
    public String getContract_renewal_total(){
        return this.contract_renewal_total ;
    }

    /**
     * 设置 [截止或者逾期减一的合同总计]
     */
    @JsonProperty("contract_renewal_total")
    public void setContract_renewal_total(String  contract_renewal_total){
        this.contract_renewal_total = contract_renewal_total ;
        this.contract_renewal_totalDirtyFlag = true ;
    }

     /**
     * 获取 [截止或者逾期减一的合同总计]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_totalDirtyFlag(){
        return this.contract_renewal_totalDirtyFlag ;
    }   

    /**
     * 获取 [费用]
     */
    @JsonProperty("cost_count")
    public Integer getCost_count(){
        return this.cost_count ;
    }

    /**
     * 设置 [费用]
     */
    @JsonProperty("cost_count")
    public void setCost_count(Integer  cost_count){
        this.cost_count = cost_count ;
        this.cost_countDirtyFlag = true ;
    }

     /**
     * 获取 [费用]脏标记
     */
    @JsonIgnore
    public boolean getCost_countDirtyFlag(){
        return this.cost_countDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [车门数量]
     */
    @JsonProperty("doors")
    public Integer getDoors(){
        return this.doors ;
    }

    /**
     * 设置 [车门数量]
     */
    @JsonProperty("doors")
    public void setDoors(Integer  doors){
        this.doors = doors ;
        this.doorsDirtyFlag = true ;
    }

     /**
     * 获取 [车门数量]脏标记
     */
    @JsonIgnore
    public boolean getDoorsDirtyFlag(){
        return this.doorsDirtyFlag ;
    }   

    /**
     * 获取 [驾驶员]
     */
    @JsonProperty("driver_id")
    public Integer getDriver_id(){
        return this.driver_id ;
    }

    /**
     * 设置 [驾驶员]
     */
    @JsonProperty("driver_id")
    public void setDriver_id(Integer  driver_id){
        this.driver_id = driver_id ;
        this.driver_idDirtyFlag = true ;
    }

     /**
     * 获取 [驾驶员]脏标记
     */
    @JsonIgnore
    public boolean getDriver_idDirtyFlag(){
        return this.driver_idDirtyFlag ;
    }   

    /**
     * 获取 [驾驶员]
     */
    @JsonProperty("driver_id_text")
    public String getDriver_id_text(){
        return this.driver_id_text ;
    }

    /**
     * 设置 [驾驶员]
     */
    @JsonProperty("driver_id_text")
    public void setDriver_id_text(String  driver_id_text){
        this.driver_id_text = driver_id_text ;
        this.driver_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [驾驶员]脏标记
     */
    @JsonIgnore
    public boolean getDriver_id_textDirtyFlag(){
        return this.driver_id_textDirtyFlag ;
    }   

    /**
     * 获取 [首次合同日期]
     */
    @JsonProperty("first_contract_date")
    public Timestamp getFirst_contract_date(){
        return this.first_contract_date ;
    }

    /**
     * 设置 [首次合同日期]
     */
    @JsonProperty("first_contract_date")
    public void setFirst_contract_date(Timestamp  first_contract_date){
        this.first_contract_date = first_contract_date ;
        this.first_contract_dateDirtyFlag = true ;
    }

     /**
     * 获取 [首次合同日期]脏标记
     */
    @JsonIgnore
    public boolean getFirst_contract_dateDirtyFlag(){
        return this.first_contract_dateDirtyFlag ;
    }   

    /**
     * 获取 [加油记录统计]
     */
    @JsonProperty("fuel_logs_count")
    public Integer getFuel_logs_count(){
        return this.fuel_logs_count ;
    }

    /**
     * 设置 [加油记录统计]
     */
    @JsonProperty("fuel_logs_count")
    public void setFuel_logs_count(Integer  fuel_logs_count){
        this.fuel_logs_count = fuel_logs_count ;
        this.fuel_logs_countDirtyFlag = true ;
    }

     /**
     * 获取 [加油记录统计]脏标记
     */
    @JsonIgnore
    public boolean getFuel_logs_countDirtyFlag(){
        return this.fuel_logs_countDirtyFlag ;
    }   

    /**
     * 获取 [燃油类型]
     */
    @JsonProperty("fuel_type")
    public String getFuel_type(){
        return this.fuel_type ;
    }

    /**
     * 设置 [燃油类型]
     */
    @JsonProperty("fuel_type")
    public void setFuel_type(String  fuel_type){
        this.fuel_type = fuel_type ;
        this.fuel_typeDirtyFlag = true ;
    }

     /**
     * 获取 [燃油类型]脏标记
     */
    @JsonIgnore
    public boolean getFuel_typeDirtyFlag(){
        return this.fuel_typeDirtyFlag ;
    }   

    /**
     * 获取 [马力]
     */
    @JsonProperty("horsepower")
    public Integer getHorsepower(){
        return this.horsepower ;
    }

    /**
     * 设置 [马力]
     */
    @JsonProperty("horsepower")
    public void setHorsepower(Integer  horsepower){
        this.horsepower = horsepower ;
        this.horsepowerDirtyFlag = true ;
    }

     /**
     * 获取 [马力]脏标记
     */
    @JsonIgnore
    public boolean getHorsepowerDirtyFlag(){
        return this.horsepowerDirtyFlag ;
    }   

    /**
     * 获取 [车船税]
     */
    @JsonProperty("horsepower_tax")
    public Double getHorsepower_tax(){
        return this.horsepower_tax ;
    }

    /**
     * 设置 [车船税]
     */
    @JsonProperty("horsepower_tax")
    public void setHorsepower_tax(Double  horsepower_tax){
        this.horsepower_tax = horsepower_tax ;
        this.horsepower_taxDirtyFlag = true ;
    }

     /**
     * 获取 [车船税]脏标记
     */
    @JsonIgnore
    public boolean getHorsepower_taxDirtyFlag(){
        return this.horsepower_taxDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [徽标]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [徽标]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [徽标]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [车辆标志图片(普通大小)]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [车辆标志图片(普通大小)]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [车辆标志图片(普通大小)]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [车辆标志图片(小图片)]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [车辆标志图片(小图片)]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [车辆标志图片(小图片)]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [车辆牌照]
     */
    @JsonProperty("license_plate")
    public String getLicense_plate(){
        return this.license_plate ;
    }

    /**
     * 设置 [车辆牌照]
     */
    @JsonProperty("license_plate")
    public void setLicense_plate(String  license_plate){
        this.license_plate = license_plate ;
        this.license_plateDirtyFlag = true ;
    }

     /**
     * 获取 [车辆牌照]脏标记
     */
    @JsonIgnore
    public boolean getLicense_plateDirtyFlag(){
        return this.license_plateDirtyFlag ;
    }   

    /**
     * 获取 [地点]
     */
    @JsonProperty("location")
    public String getLocation(){
        return this.location ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("location")
    public void setLocation(String  location){
        this.location = location ;
        this.locationDirtyFlag = true ;
    }

     /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getLocationDirtyFlag(){
        return this.locationDirtyFlag ;
    }   

    /**
     * 获取 [合同]
     */
    @JsonProperty("log_contracts")
    public String getLog_contracts(){
        return this.log_contracts ;
    }

    /**
     * 设置 [合同]
     */
    @JsonProperty("log_contracts")
    public void setLog_contracts(String  log_contracts){
        this.log_contracts = log_contracts ;
        this.log_contractsDirtyFlag = true ;
    }

     /**
     * 获取 [合同]脏标记
     */
    @JsonIgnore
    public boolean getLog_contractsDirtyFlag(){
        return this.log_contractsDirtyFlag ;
    }   

    /**
     * 获取 [指派记录]
     */
    @JsonProperty("log_drivers")
    public String getLog_drivers(){
        return this.log_drivers ;
    }

    /**
     * 设置 [指派记录]
     */
    @JsonProperty("log_drivers")
    public void setLog_drivers(String  log_drivers){
        this.log_drivers = log_drivers ;
        this.log_driversDirtyFlag = true ;
    }

     /**
     * 获取 [指派记录]脏标记
     */
    @JsonIgnore
    public boolean getLog_driversDirtyFlag(){
        return this.log_driversDirtyFlag ;
    }   

    /**
     * 获取 [燃油记录]
     */
    @JsonProperty("log_fuel")
    public String getLog_fuel(){
        return this.log_fuel ;
    }

    /**
     * 设置 [燃油记录]
     */
    @JsonProperty("log_fuel")
    public void setLog_fuel(String  log_fuel){
        this.log_fuel = log_fuel ;
        this.log_fuelDirtyFlag = true ;
    }

     /**
     * 获取 [燃油记录]脏标记
     */
    @JsonIgnore
    public boolean getLog_fuelDirtyFlag(){
        return this.log_fuelDirtyFlag ;
    }   

    /**
     * 获取 [服务记录]
     */
    @JsonProperty("log_services")
    public String getLog_services(){
        return this.log_services ;
    }

    /**
     * 设置 [服务记录]
     */
    @JsonProperty("log_services")
    public void setLog_services(String  log_services){
        this.log_services = log_services ;
        this.log_servicesDirtyFlag = true ;
    }

     /**
     * 获取 [服务记录]脏标记
     */
    @JsonIgnore
    public boolean getLog_servicesDirtyFlag(){
        return this.log_servicesDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要激活]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要激活]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要激活]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [型号]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return this.model_id ;
    }

    /**
     * 设置 [型号]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

     /**
     * 获取 [型号]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return this.model_idDirtyFlag ;
    }   

    /**
     * 获取 [型号]
     */
    @JsonProperty("model_id_text")
    public String getModel_id_text(){
        return this.model_id_text ;
    }

    /**
     * 设置 [型号]
     */
    @JsonProperty("model_id_text")
    public void setModel_id_text(String  model_id_text){
        this.model_id_text = model_id_text ;
        this.model_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [型号]脏标记
     */
    @JsonIgnore
    public boolean getModel_id_textDirtyFlag(){
        return this.model_id_textDirtyFlag ;
    }   

    /**
     * 获取 [型号年份]
     */
    @JsonProperty("model_year")
    public String getModel_year(){
        return this.model_year ;
    }

    /**
     * 设置 [型号年份]
     */
    @JsonProperty("model_year")
    public void setModel_year(String  model_year){
        this.model_year = model_year ;
        this.model_yearDirtyFlag = true ;
    }

     /**
     * 获取 [型号年份]脏标记
     */
    @JsonIgnore
    public boolean getModel_yearDirtyFlag(){
        return this.model_yearDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [最新里程表]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return this.odometer ;
    }

    /**
     * 设置 [最新里程表]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

     /**
     * 获取 [最新里程表]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return this.odometerDirtyFlag ;
    }   

    /**
     * 获取 [里程表]
     */
    @JsonProperty("odometer_count")
    public Integer getOdometer_count(){
        return this.odometer_count ;
    }

    /**
     * 设置 [里程表]
     */
    @JsonProperty("odometer_count")
    public void setOdometer_count(Integer  odometer_count){
        this.odometer_count = odometer_count ;
        this.odometer_countDirtyFlag = true ;
    }

     /**
     * 获取 [里程表]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_countDirtyFlag(){
        return this.odometer_countDirtyFlag ;
    }   

    /**
     * 获取 [里程表单位]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return this.odometer_unit ;
    }

    /**
     * 设置 [里程表单位]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

     /**
     * 获取 [里程表单位]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return this.odometer_unitDirtyFlag ;
    }   

    /**
     * 获取 [动力]
     */
    @JsonProperty("power")
    public Integer getPower(){
        return this.power ;
    }

    /**
     * 设置 [动力]
     */
    @JsonProperty("power")
    public void setPower(Integer  power){
        this.power = power ;
        this.powerDirtyFlag = true ;
    }

     /**
     * 获取 [动力]脏标记
     */
    @JsonIgnore
    public boolean getPowerDirtyFlag(){
        return this.powerDirtyFlag ;
    }   

    /**
     * 获取 [残余价值]
     */
    @JsonProperty("residual_value")
    public Double getResidual_value(){
        return this.residual_value ;
    }

    /**
     * 设置 [残余价值]
     */
    @JsonProperty("residual_value")
    public void setResidual_value(Double  residual_value){
        this.residual_value = residual_value ;
        this.residual_valueDirtyFlag = true ;
    }

     /**
     * 获取 [残余价值]脏标记
     */
    @JsonIgnore
    public boolean getResidual_valueDirtyFlag(){
        return this.residual_valueDirtyFlag ;
    }   

    /**
     * 获取 [座位数]
     */
    @JsonProperty("seats")
    public Integer getSeats(){
        return this.seats ;
    }

    /**
     * 设置 [座位数]
     */
    @JsonProperty("seats")
    public void setSeats(Integer  seats){
        this.seats = seats ;
        this.seatsDirtyFlag = true ;
    }

     /**
     * 获取 [座位数]脏标记
     */
    @JsonIgnore
    public boolean getSeatsDirtyFlag(){
        return this.seatsDirtyFlag ;
    }   

    /**
     * 获取 [服务]
     */
    @JsonProperty("service_count")
    public Integer getService_count(){
        return this.service_count ;
    }

    /**
     * 设置 [服务]
     */
    @JsonProperty("service_count")
    public void setService_count(Integer  service_count){
        this.service_count = service_count ;
        this.service_countDirtyFlag = true ;
    }

     /**
     * 获取 [服务]脏标记
     */
    @JsonIgnore
    public boolean getService_countDirtyFlag(){
        return this.service_countDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return this.state_id ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return this.state_idDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state_id_text")
    public String getState_id_text(){
        return this.state_id_text ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state_id_text")
    public void setState_id_text(String  state_id_text){
        this.state_id_text = state_id_text ;
        this.state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getState_id_textDirtyFlag(){
        return this.state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [变速器]
     */
    @JsonProperty("transmission")
    public String getTransmission(){
        return this.transmission ;
    }

    /**
     * 设置 [变速器]
     */
    @JsonProperty("transmission")
    public void setTransmission(String  transmission){
        this.transmission = transmission ;
        this.transmissionDirtyFlag = true ;
    }

     /**
     * 获取 [变速器]脏标记
     */
    @JsonIgnore
    public boolean getTransmissionDirtyFlag(){
        return this.transmissionDirtyFlag ;
    }   

    /**
     * 获取 [车架号]
     */
    @JsonProperty("vin_sn")
    public String getVin_sn(){
        return this.vin_sn ;
    }

    /**
     * 设置 [车架号]
     */
    @JsonProperty("vin_sn")
    public void setVin_sn(String  vin_sn){
        this.vin_sn = vin_sn ;
        this.vin_snDirtyFlag = true ;
    }

     /**
     * 获取 [车架号]脏标记
     */
    @JsonIgnore
    public boolean getVin_snDirtyFlag(){
        return this.vin_snDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
