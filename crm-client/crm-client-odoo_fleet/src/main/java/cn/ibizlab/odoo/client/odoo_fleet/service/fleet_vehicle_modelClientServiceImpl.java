package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_modelClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_modelImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_modelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_model] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_modelClientServiceImpl implements Ifleet_vehicle_modelClientService {

    fleet_vehicle_modelFeignClient fleet_vehicle_modelFeignClient;

    @Autowired
    public fleet_vehicle_modelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_modelFeignClient = nameBuilder.target(fleet_vehicle_modelFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_modelFeignClient = nameBuilder.target(fleet_vehicle_modelFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_model createModel() {
		return new fleet_vehicle_modelImpl();
	}


    public void remove(Ifleet_vehicle_model fleet_vehicle_model){
        fleet_vehicle_modelFeignClient.remove(fleet_vehicle_model.getId()) ;
    }


    public void create(Ifleet_vehicle_model fleet_vehicle_model){
        Ifleet_vehicle_model clientModel = fleet_vehicle_modelFeignClient.create((fleet_vehicle_modelImpl)fleet_vehicle_model) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model, null);
    }


    public Page<Ifleet_vehicle_model> fetchDefault(SearchContext context){
        Page<fleet_vehicle_modelImpl> page = this.fleet_vehicle_modelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ifleet_vehicle_model fleet_vehicle_model){
        Ifleet_vehicle_model clientModel = fleet_vehicle_modelFeignClient.update(fleet_vehicle_model.getId(),(fleet_vehicle_modelImpl)fleet_vehicle_model) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model, null);
    }


    public void get(Ifleet_vehicle_model fleet_vehicle_model){
        Ifleet_vehicle_model clientModel = fleet_vehicle_modelFeignClient.get(fleet_vehicle_model.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model, null);
    }


    public void updateBatch(List<Ifleet_vehicle_model> fleet_vehicle_models){
        if(fleet_vehicle_models!=null){
            List<fleet_vehicle_modelImpl> list = new ArrayList<fleet_vehicle_modelImpl>();
            for(Ifleet_vehicle_model ifleet_vehicle_model :fleet_vehicle_models){
                list.add((fleet_vehicle_modelImpl)ifleet_vehicle_model) ;
            }
            fleet_vehicle_modelFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ifleet_vehicle_model> fleet_vehicle_models){
        if(fleet_vehicle_models!=null){
            List<fleet_vehicle_modelImpl> list = new ArrayList<fleet_vehicle_modelImpl>();
            for(Ifleet_vehicle_model ifleet_vehicle_model :fleet_vehicle_models){
                list.add((fleet_vehicle_modelImpl)ifleet_vehicle_model) ;
            }
            fleet_vehicle_modelFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ifleet_vehicle_model> fleet_vehicle_models){
        if(fleet_vehicle_models!=null){
            List<fleet_vehicle_modelImpl> list = new ArrayList<fleet_vehicle_modelImpl>();
            for(Ifleet_vehicle_model ifleet_vehicle_model :fleet_vehicle_models){
                list.add((fleet_vehicle_modelImpl)ifleet_vehicle_model) ;
            }
            fleet_vehicle_modelFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ifleet_vehicle_model> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_model fleet_vehicle_model){
        Ifleet_vehicle_model clientModel = fleet_vehicle_modelFeignClient.getDraft(fleet_vehicle_model.getId(),(fleet_vehicle_modelImpl)fleet_vehicle_model) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model, null);
    }



}

