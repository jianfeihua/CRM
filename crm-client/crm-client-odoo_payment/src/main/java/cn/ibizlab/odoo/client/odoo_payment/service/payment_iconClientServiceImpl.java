package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_icon;
import cn.ibizlab.odoo.client.odoo_payment.config.odoo_paymentClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipayment_iconClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_iconImpl;
import cn.ibizlab.odoo.client.odoo_payment.feign.payment_iconFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[payment_icon] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class payment_iconClientServiceImpl implements Ipayment_iconClientService {

    payment_iconFeignClient payment_iconFeignClient;

    @Autowired
    public payment_iconClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_paymentClientProperties odoo_paymentClientProperties) {
        if (odoo_paymentClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_iconFeignClient = nameBuilder.target(payment_iconFeignClient.class,"http://"+odoo_paymentClientProperties.getServiceId()+"/") ;
		}else if (odoo_paymentClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_iconFeignClient = nameBuilder.target(payment_iconFeignClient.class,odoo_paymentClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipayment_icon createModel() {
		return new payment_iconImpl();
	}


    public void removeBatch(List<Ipayment_icon> payment_icons){
        if(payment_icons!=null){
            List<payment_iconImpl> list = new ArrayList<payment_iconImpl>();
            for(Ipayment_icon ipayment_icon :payment_icons){
                list.add((payment_iconImpl)ipayment_icon) ;
            }
            payment_iconFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ipayment_icon> fetchDefault(SearchContext context){
        Page<payment_iconImpl> page = this.payment_iconFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ipayment_icon payment_icon){
        Ipayment_icon clientModel = payment_iconFeignClient.create((payment_iconImpl)payment_icon) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_icon.getClass(), false);
        copier.copy(clientModel, payment_icon, null);
    }


    public void updateBatch(List<Ipayment_icon> payment_icons){
        if(payment_icons!=null){
            List<payment_iconImpl> list = new ArrayList<payment_iconImpl>();
            for(Ipayment_icon ipayment_icon :payment_icons){
                list.add((payment_iconImpl)ipayment_icon) ;
            }
            payment_iconFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ipayment_icon payment_icon){
        payment_iconFeignClient.remove(payment_icon.getId()) ;
    }


    public void createBatch(List<Ipayment_icon> payment_icons){
        if(payment_icons!=null){
            List<payment_iconImpl> list = new ArrayList<payment_iconImpl>();
            for(Ipayment_icon ipayment_icon :payment_icons){
                list.add((payment_iconImpl)ipayment_icon) ;
            }
            payment_iconFeignClient.createBatch(list) ;
        }
    }


    public void get(Ipayment_icon payment_icon){
        Ipayment_icon clientModel = payment_iconFeignClient.get(payment_icon.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_icon.getClass(), false);
        copier.copy(clientModel, payment_icon, null);
    }


    public void update(Ipayment_icon payment_icon){
        Ipayment_icon clientModel = payment_iconFeignClient.update(payment_icon.getId(),(payment_iconImpl)payment_icon) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_icon.getClass(), false);
        copier.copy(clientModel, payment_icon, null);
    }


    public Page<Ipayment_icon> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipayment_icon payment_icon){
        Ipayment_icon clientModel = payment_iconFeignClient.getDraft(payment_icon.getId(),(payment_iconImpl)payment_icon) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_icon.getClass(), false);
        copier.copy(clientModel, payment_icon, null);
    }



}

