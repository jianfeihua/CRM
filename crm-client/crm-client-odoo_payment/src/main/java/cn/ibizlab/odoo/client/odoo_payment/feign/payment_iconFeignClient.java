package cn.ibizlab.odoo.client.odoo_payment.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipayment_icon;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_iconImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[payment_icon] 服务对象接口
 */
public interface payment_iconFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_icons/removebatch")
    public payment_iconImpl removeBatch(@RequestBody List<payment_iconImpl> payment_icons);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_icons/fetchdefault")
    public Page<payment_iconImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_icons")
    public payment_iconImpl create(@RequestBody payment_iconImpl payment_icon);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_icons/updatebatch")
    public payment_iconImpl updateBatch(@RequestBody List<payment_iconImpl> payment_icons);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_icons/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_icons/createbatch")
    public payment_iconImpl createBatch(@RequestBody List<payment_iconImpl> payment_icons);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_icons/{id}")
    public payment_iconImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_icons/{id}")
    public payment_iconImpl update(@PathVariable("id") Integer id,@RequestBody payment_iconImpl payment_icon);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_icons/select")
    public Page<payment_iconImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_icons/{id}/getdraft")
    public payment_iconImpl getDraft(@PathVariable("id") Integer id,@RequestBody payment_iconImpl payment_icon);



}
