package cn.ibizlab.odoo.client.odoo_payment.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ipayment_transaction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[payment_transaction] 对象
 */
public class payment_transactionImpl implements Ipayment_transaction,Serializable{

    /**
     * 收单方
     */
    public Integer acquirer_id;

    @JsonIgnore
    public boolean acquirer_idDirtyFlag;
    
    /**
     * 收单方
     */
    public String acquirer_id_text;

    @JsonIgnore
    public boolean acquirer_id_textDirtyFlag;
    
    /**
     * 收单方参考
     */
    public String acquirer_reference;

    @JsonIgnore
    public boolean acquirer_referenceDirtyFlag;
    
    /**
     * 金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 回调哈希函数
     */
    public String callback_hash;

    @JsonIgnore
    public boolean callback_hashDirtyFlag;
    
    /**
     * 回调方法
     */
    public String callback_method;

    @JsonIgnore
    public boolean callback_methodDirtyFlag;
    
    /**
     * 回调文档模型
     */
    public Integer callback_model_id;

    @JsonIgnore
    public boolean callback_model_idDirtyFlag;
    
    /**
     * 回调文档 ID
     */
    public Integer callback_res_id;

    @JsonIgnore
    public boolean callback_res_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 验证日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 费用
     */
    public Double fees;

    @JsonIgnore
    public boolean feesDirtyFlag;
    
    /**
     * 3D Secure HTML
     */
    public String html_3ds;

    @JsonIgnore
    public boolean html_3dsDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_ids;

    @JsonIgnore
    public boolean invoice_idsDirtyFlag;
    
    /**
     * # 发票
     */
    public Integer invoice_ids_nbr;

    @JsonIgnore
    public boolean invoice_ids_nbrDirtyFlag;
    
    /**
     * 付款是否已过账处理
     */
    public String is_processed;

    @JsonIgnore
    public boolean is_processedDirtyFlag;
    
    /**
     * 地址
     */
    public String partner_address;

    @JsonIgnore
    public boolean partner_addressDirtyFlag;
    
    /**
     * 城市
     */
    public String partner_city;

    @JsonIgnore
    public boolean partner_cityDirtyFlag;
    
    /**
     * 国家
     */
    public Integer partner_country_id;

    @JsonIgnore
    public boolean partner_country_idDirtyFlag;
    
    /**
     * 国家
     */
    public String partner_country_id_text;

    @JsonIgnore
    public boolean partner_country_id_textDirtyFlag;
    
    /**
     * EMail
     */
    public String partner_email;

    @JsonIgnore
    public boolean partner_emailDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 语言
     */
    public String partner_lang;

    @JsonIgnore
    public boolean partner_langDirtyFlag;
    
    /**
     * 合作伙伴名称
     */
    public String partner_name;

    @JsonIgnore
    public boolean partner_nameDirtyFlag;
    
    /**
     * 电话
     */
    public String partner_phone;

    @JsonIgnore
    public boolean partner_phoneDirtyFlag;
    
    /**
     * 邮政编码
     */
    public String partner_zip;

    @JsonIgnore
    public boolean partner_zipDirtyFlag;
    
    /**
     * 付款
     */
    public Integer payment_id;

    @JsonIgnore
    public boolean payment_idDirtyFlag;
    
    /**
     * 付款
     */
    public String payment_id_text;

    @JsonIgnore
    public boolean payment_id_textDirtyFlag;
    
    /**
     * 付款令牌
     */
    public Integer payment_token_id;

    @JsonIgnore
    public boolean payment_token_idDirtyFlag;
    
    /**
     * 付款令牌
     */
    public String payment_token_id_text;

    @JsonIgnore
    public boolean payment_token_id_textDirtyFlag;
    
    /**
     * 服务商
     */
    public String provider;

    @JsonIgnore
    public boolean providerDirtyFlag;
    
    /**
     * 参考
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 付款后返回网址
     */
    public String return_url;

    @JsonIgnore
    public boolean return_urlDirtyFlag;
    
    /**
     * 销售订单
     */
    public String sale_order_ids;

    @JsonIgnore
    public boolean sale_order_idsDirtyFlag;
    
    /**
     * # 销售订单
     */
    public Integer sale_order_ids_nbr;

    @JsonIgnore
    public boolean sale_order_ids_nbrDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 消息
     */
    public String state_message;

    @JsonIgnore
    public boolean state_messageDirtyFlag;
    
    /**
     * 类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [收单方]
     */
    @JsonProperty("acquirer_id")
    public Integer getAcquirer_id(){
        return this.acquirer_id ;
    }

    /**
     * 设置 [收单方]
     */
    @JsonProperty("acquirer_id")
    public void setAcquirer_id(Integer  acquirer_id){
        this.acquirer_id = acquirer_id ;
        this.acquirer_idDirtyFlag = true ;
    }

     /**
     * 获取 [收单方]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_idDirtyFlag(){
        return this.acquirer_idDirtyFlag ;
    }   

    /**
     * 获取 [收单方]
     */
    @JsonProperty("acquirer_id_text")
    public String getAcquirer_id_text(){
        return this.acquirer_id_text ;
    }

    /**
     * 设置 [收单方]
     */
    @JsonProperty("acquirer_id_text")
    public void setAcquirer_id_text(String  acquirer_id_text){
        this.acquirer_id_text = acquirer_id_text ;
        this.acquirer_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收单方]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_id_textDirtyFlag(){
        return this.acquirer_id_textDirtyFlag ;
    }   

    /**
     * 获取 [收单方参考]
     */
    @JsonProperty("acquirer_reference")
    public String getAcquirer_reference(){
        return this.acquirer_reference ;
    }

    /**
     * 设置 [收单方参考]
     */
    @JsonProperty("acquirer_reference")
    public void setAcquirer_reference(String  acquirer_reference){
        this.acquirer_reference = acquirer_reference ;
        this.acquirer_referenceDirtyFlag = true ;
    }

     /**
     * 获取 [收单方参考]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_referenceDirtyFlag(){
        return this.acquirer_referenceDirtyFlag ;
    }   

    /**
     * 获取 [金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [回调哈希函数]
     */
    @JsonProperty("callback_hash")
    public String getCallback_hash(){
        return this.callback_hash ;
    }

    /**
     * 设置 [回调哈希函数]
     */
    @JsonProperty("callback_hash")
    public void setCallback_hash(String  callback_hash){
        this.callback_hash = callback_hash ;
        this.callback_hashDirtyFlag = true ;
    }

     /**
     * 获取 [回调哈希函数]脏标记
     */
    @JsonIgnore
    public boolean getCallback_hashDirtyFlag(){
        return this.callback_hashDirtyFlag ;
    }   

    /**
     * 获取 [回调方法]
     */
    @JsonProperty("callback_method")
    public String getCallback_method(){
        return this.callback_method ;
    }

    /**
     * 设置 [回调方法]
     */
    @JsonProperty("callback_method")
    public void setCallback_method(String  callback_method){
        this.callback_method = callback_method ;
        this.callback_methodDirtyFlag = true ;
    }

     /**
     * 获取 [回调方法]脏标记
     */
    @JsonIgnore
    public boolean getCallback_methodDirtyFlag(){
        return this.callback_methodDirtyFlag ;
    }   

    /**
     * 获取 [回调文档模型]
     */
    @JsonProperty("callback_model_id")
    public Integer getCallback_model_id(){
        return this.callback_model_id ;
    }

    /**
     * 设置 [回调文档模型]
     */
    @JsonProperty("callback_model_id")
    public void setCallback_model_id(Integer  callback_model_id){
        this.callback_model_id = callback_model_id ;
        this.callback_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [回调文档模型]脏标记
     */
    @JsonIgnore
    public boolean getCallback_model_idDirtyFlag(){
        return this.callback_model_idDirtyFlag ;
    }   

    /**
     * 获取 [回调文档 ID]
     */
    @JsonProperty("callback_res_id")
    public Integer getCallback_res_id(){
        return this.callback_res_id ;
    }

    /**
     * 设置 [回调文档 ID]
     */
    @JsonProperty("callback_res_id")
    public void setCallback_res_id(Integer  callback_res_id){
        this.callback_res_id = callback_res_id ;
        this.callback_res_idDirtyFlag = true ;
    }

     /**
     * 获取 [回调文档 ID]脏标记
     */
    @JsonIgnore
    public boolean getCallback_res_idDirtyFlag(){
        return this.callback_res_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [验证日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [验证日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [验证日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [费用]
     */
    @JsonProperty("fees")
    public Double getFees(){
        return this.fees ;
    }

    /**
     * 设置 [费用]
     */
    @JsonProperty("fees")
    public void setFees(Double  fees){
        this.fees = fees ;
        this.feesDirtyFlag = true ;
    }

     /**
     * 获取 [费用]脏标记
     */
    @JsonIgnore
    public boolean getFeesDirtyFlag(){
        return this.feesDirtyFlag ;
    }   

    /**
     * 获取 [3D Secure HTML]
     */
    @JsonProperty("html_3ds")
    public String getHtml_3ds(){
        return this.html_3ds ;
    }

    /**
     * 设置 [3D Secure HTML]
     */
    @JsonProperty("html_3ds")
    public void setHtml_3ds(String  html_3ds){
        this.html_3ds = html_3ds ;
        this.html_3dsDirtyFlag = true ;
    }

     /**
     * 获取 [3D Secure HTML]脏标记
     */
    @JsonIgnore
    public boolean getHtml_3dsDirtyFlag(){
        return this.html_3dsDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [# 发票]
     */
    @JsonProperty("invoice_ids_nbr")
    public Integer getInvoice_ids_nbr(){
        return this.invoice_ids_nbr ;
    }

    /**
     * 设置 [# 发票]
     */
    @JsonProperty("invoice_ids_nbr")
    public void setInvoice_ids_nbr(Integer  invoice_ids_nbr){
        this.invoice_ids_nbr = invoice_ids_nbr ;
        this.invoice_ids_nbrDirtyFlag = true ;
    }

     /**
     * 获取 [# 发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_ids_nbrDirtyFlag(){
        return this.invoice_ids_nbrDirtyFlag ;
    }   

    /**
     * 获取 [付款是否已过账处理]
     */
    @JsonProperty("is_processed")
    public String getIs_processed(){
        return this.is_processed ;
    }

    /**
     * 设置 [付款是否已过账处理]
     */
    @JsonProperty("is_processed")
    public void setIs_processed(String  is_processed){
        this.is_processed = is_processed ;
        this.is_processedDirtyFlag = true ;
    }

     /**
     * 获取 [付款是否已过账处理]脏标记
     */
    @JsonIgnore
    public boolean getIs_processedDirtyFlag(){
        return this.is_processedDirtyFlag ;
    }   

    /**
     * 获取 [地址]
     */
    @JsonProperty("partner_address")
    public String getPartner_address(){
        return this.partner_address ;
    }

    /**
     * 设置 [地址]
     */
    @JsonProperty("partner_address")
    public void setPartner_address(String  partner_address){
        this.partner_address = partner_address ;
        this.partner_addressDirtyFlag = true ;
    }

     /**
     * 获取 [地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_addressDirtyFlag(){
        return this.partner_addressDirtyFlag ;
    }   

    /**
     * 获取 [城市]
     */
    @JsonProperty("partner_city")
    public String getPartner_city(){
        return this.partner_city ;
    }

    /**
     * 设置 [城市]
     */
    @JsonProperty("partner_city")
    public void setPartner_city(String  partner_city){
        this.partner_city = partner_city ;
        this.partner_cityDirtyFlag = true ;
    }

     /**
     * 获取 [城市]脏标记
     */
    @JsonIgnore
    public boolean getPartner_cityDirtyFlag(){
        return this.partner_cityDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("partner_country_id")
    public Integer getPartner_country_id(){
        return this.partner_country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("partner_country_id")
    public void setPartner_country_id(Integer  partner_country_id){
        this.partner_country_id = partner_country_id ;
        this.partner_country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getPartner_country_idDirtyFlag(){
        return this.partner_country_idDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("partner_country_id_text")
    public String getPartner_country_id_text(){
        return this.partner_country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("partner_country_id_text")
    public void setPartner_country_id_text(String  partner_country_id_text){
        this.partner_country_id_text = partner_country_id_text ;
        this.partner_country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getPartner_country_id_textDirtyFlag(){
        return this.partner_country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("partner_email")
    public String getPartner_email(){
        return this.partner_email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("partner_email")
    public void setPartner_email(String  partner_email){
        this.partner_email = partner_email ;
        this.partner_emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getPartner_emailDirtyFlag(){
        return this.partner_emailDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [语言]
     */
    @JsonProperty("partner_lang")
    public String getPartner_lang(){
        return this.partner_lang ;
    }

    /**
     * 设置 [语言]
     */
    @JsonProperty("partner_lang")
    public void setPartner_lang(String  partner_lang){
        this.partner_lang = partner_lang ;
        this.partner_langDirtyFlag = true ;
    }

     /**
     * 获取 [语言]脏标记
     */
    @JsonIgnore
    public boolean getPartner_langDirtyFlag(){
        return this.partner_langDirtyFlag ;
    }   

    /**
     * 获取 [合作伙伴名称]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return this.partner_name ;
    }

    /**
     * 设置 [合作伙伴名称]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

     /**
     * 获取 [合作伙伴名称]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return this.partner_nameDirtyFlag ;
    }   

    /**
     * 获取 [电话]
     */
    @JsonProperty("partner_phone")
    public String getPartner_phone(){
        return this.partner_phone ;
    }

    /**
     * 设置 [电话]
     */
    @JsonProperty("partner_phone")
    public void setPartner_phone(String  partner_phone){
        this.partner_phone = partner_phone ;
        this.partner_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [电话]脏标记
     */
    @JsonIgnore
    public boolean getPartner_phoneDirtyFlag(){
        return this.partner_phoneDirtyFlag ;
    }   

    /**
     * 获取 [邮政编码]
     */
    @JsonProperty("partner_zip")
    public String getPartner_zip(){
        return this.partner_zip ;
    }

    /**
     * 设置 [邮政编码]
     */
    @JsonProperty("partner_zip")
    public void setPartner_zip(String  partner_zip){
        this.partner_zip = partner_zip ;
        this.partner_zipDirtyFlag = true ;
    }

     /**
     * 获取 [邮政编码]脏标记
     */
    @JsonIgnore
    public boolean getPartner_zipDirtyFlag(){
        return this.partner_zipDirtyFlag ;
    }   

    /**
     * 获取 [付款]
     */
    @JsonProperty("payment_id")
    public Integer getPayment_id(){
        return this.payment_id ;
    }

    /**
     * 设置 [付款]
     */
    @JsonProperty("payment_id")
    public void setPayment_id(Integer  payment_id){
        this.payment_id = payment_id ;
        this.payment_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idDirtyFlag(){
        return this.payment_idDirtyFlag ;
    }   

    /**
     * 获取 [付款]
     */
    @JsonProperty("payment_id_text")
    public String getPayment_id_text(){
        return this.payment_id_text ;
    }

    /**
     * 设置 [付款]
     */
    @JsonProperty("payment_id_text")
    public void setPayment_id_text(String  payment_id_text){
        this.payment_id_text = payment_id_text ;
        this.payment_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_id_textDirtyFlag(){
        return this.payment_id_textDirtyFlag ;
    }   

    /**
     * 获取 [付款令牌]
     */
    @JsonProperty("payment_token_id")
    public Integer getPayment_token_id(){
        return this.payment_token_id ;
    }

    /**
     * 设置 [付款令牌]
     */
    @JsonProperty("payment_token_id")
    public void setPayment_token_id(Integer  payment_token_id){
        this.payment_token_id = payment_token_id ;
        this.payment_token_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款令牌]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idDirtyFlag(){
        return this.payment_token_idDirtyFlag ;
    }   

    /**
     * 获取 [付款令牌]
     */
    @JsonProperty("payment_token_id_text")
    public String getPayment_token_id_text(){
        return this.payment_token_id_text ;
    }

    /**
     * 设置 [付款令牌]
     */
    @JsonProperty("payment_token_id_text")
    public void setPayment_token_id_text(String  payment_token_id_text){
        this.payment_token_id_text = payment_token_id_text ;
        this.payment_token_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款令牌]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_id_textDirtyFlag(){
        return this.payment_token_id_textDirtyFlag ;
    }   

    /**
     * 获取 [服务商]
     */
    @JsonProperty("provider")
    public String getProvider(){
        return this.provider ;
    }

    /**
     * 设置 [服务商]
     */
    @JsonProperty("provider")
    public void setProvider(String  provider){
        this.provider = provider ;
        this.providerDirtyFlag = true ;
    }

     /**
     * 获取 [服务商]脏标记
     */
    @JsonIgnore
    public boolean getProviderDirtyFlag(){
        return this.providerDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [付款后返回网址]
     */
    @JsonProperty("return_url")
    public String getReturn_url(){
        return this.return_url ;
    }

    /**
     * 设置 [付款后返回网址]
     */
    @JsonProperty("return_url")
    public void setReturn_url(String  return_url){
        this.return_url = return_url ;
        this.return_urlDirtyFlag = true ;
    }

     /**
     * 获取 [付款后返回网址]脏标记
     */
    @JsonIgnore
    public boolean getReturn_urlDirtyFlag(){
        return this.return_urlDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_order_ids")
    public String getSale_order_ids(){
        return this.sale_order_ids ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_order_ids")
    public void setSale_order_ids(String  sale_order_ids){
        this.sale_order_ids = sale_order_ids ;
        this.sale_order_idsDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idsDirtyFlag(){
        return this.sale_order_idsDirtyFlag ;
    }   

    /**
     * 获取 [# 销售订单]
     */
    @JsonProperty("sale_order_ids_nbr")
    public Integer getSale_order_ids_nbr(){
        return this.sale_order_ids_nbr ;
    }

    /**
     * 设置 [# 销售订单]
     */
    @JsonProperty("sale_order_ids_nbr")
    public void setSale_order_ids_nbr(Integer  sale_order_ids_nbr){
        this.sale_order_ids_nbr = sale_order_ids_nbr ;
        this.sale_order_ids_nbrDirtyFlag = true ;
    }

     /**
     * 获取 [# 销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_ids_nbrDirtyFlag(){
        return this.sale_order_ids_nbrDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("state_message")
    public String getState_message(){
        return this.state_message ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("state_message")
    public void setState_message(String  state_message){
        this.state_message = state_message ;
        this.state_messageDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getState_messageDirtyFlag(){
        return this.state_messageDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
