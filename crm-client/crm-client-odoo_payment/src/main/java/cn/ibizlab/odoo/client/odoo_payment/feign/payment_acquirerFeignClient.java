package cn.ibizlab.odoo.client.odoo_payment.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_acquirerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[payment_acquirer] 服务对象接口
 */
public interface payment_acquirerFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_acquirers/{id}")
    public payment_acquirerImpl update(@PathVariable("id") Integer id,@RequestBody payment_acquirerImpl payment_acquirer);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_acquirers")
    public payment_acquirerImpl create(@RequestBody payment_acquirerImpl payment_acquirer);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_acquirers/createbatch")
    public payment_acquirerImpl createBatch(@RequestBody List<payment_acquirerImpl> payment_acquirers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirers/{id}")
    public payment_acquirerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_acquirers/removebatch")
    public payment_acquirerImpl removeBatch(@RequestBody List<payment_acquirerImpl> payment_acquirers);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_acquirers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_acquirers/updatebatch")
    public payment_acquirerImpl updateBatch(@RequestBody List<payment_acquirerImpl> payment_acquirers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirers/fetchdefault")
    public Page<payment_acquirerImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirers/select")
    public Page<payment_acquirerImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirers/{id}/getdraft")
    public payment_acquirerImpl getDraft(@PathVariable("id") Integer id,@RequestBody payment_acquirerImpl payment_acquirer);



}
