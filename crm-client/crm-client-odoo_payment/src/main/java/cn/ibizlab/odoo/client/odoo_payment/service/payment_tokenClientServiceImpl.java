package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_token;
import cn.ibizlab.odoo.client.odoo_payment.config.odoo_paymentClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipayment_tokenClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_tokenImpl;
import cn.ibizlab.odoo.client.odoo_payment.feign.payment_tokenFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[payment_token] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class payment_tokenClientServiceImpl implements Ipayment_tokenClientService {

    payment_tokenFeignClient payment_tokenFeignClient;

    @Autowired
    public payment_tokenClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_paymentClientProperties odoo_paymentClientProperties) {
        if (odoo_paymentClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_tokenFeignClient = nameBuilder.target(payment_tokenFeignClient.class,"http://"+odoo_paymentClientProperties.getServiceId()+"/") ;
		}else if (odoo_paymentClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_tokenFeignClient = nameBuilder.target(payment_tokenFeignClient.class,odoo_paymentClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipayment_token createModel() {
		return new payment_tokenImpl();
	}


    public void get(Ipayment_token payment_token){
        Ipayment_token clientModel = payment_tokenFeignClient.get(payment_token.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_token.getClass(), false);
        copier.copy(clientModel, payment_token, null);
    }


    public void updateBatch(List<Ipayment_token> payment_tokens){
        if(payment_tokens!=null){
            List<payment_tokenImpl> list = new ArrayList<payment_tokenImpl>();
            for(Ipayment_token ipayment_token :payment_tokens){
                list.add((payment_tokenImpl)ipayment_token) ;
            }
            payment_tokenFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ipayment_token payment_token){
        payment_tokenFeignClient.remove(payment_token.getId()) ;
    }


    public void createBatch(List<Ipayment_token> payment_tokens){
        if(payment_tokens!=null){
            List<payment_tokenImpl> list = new ArrayList<payment_tokenImpl>();
            for(Ipayment_token ipayment_token :payment_tokens){
                list.add((payment_tokenImpl)ipayment_token) ;
            }
            payment_tokenFeignClient.createBatch(list) ;
        }
    }


    public void create(Ipayment_token payment_token){
        Ipayment_token clientModel = payment_tokenFeignClient.create((payment_tokenImpl)payment_token) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_token.getClass(), false);
        copier.copy(clientModel, payment_token, null);
    }


    public void removeBatch(List<Ipayment_token> payment_tokens){
        if(payment_tokens!=null){
            List<payment_tokenImpl> list = new ArrayList<payment_tokenImpl>();
            for(Ipayment_token ipayment_token :payment_tokens){
                list.add((payment_tokenImpl)ipayment_token) ;
            }
            payment_tokenFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ipayment_token> fetchDefault(SearchContext context){
        Page<payment_tokenImpl> page = this.payment_tokenFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ipayment_token payment_token){
        Ipayment_token clientModel = payment_tokenFeignClient.update(payment_token.getId(),(payment_tokenImpl)payment_token) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_token.getClass(), false);
        copier.copy(clientModel, payment_token, null);
    }


    public Page<Ipayment_token> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipayment_token payment_token){
        Ipayment_token clientModel = payment_tokenFeignClient.getDraft(payment_token.getId(),(payment_tokenImpl)payment_token) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_token.getClass(), false);
        copier.copy(clientModel, payment_token, null);
    }



}

