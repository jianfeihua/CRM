package cn.ibizlab.odoo.client.odoo_lunch.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ilunch_product;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_productImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[lunch_product] 服务对象接口
 */
public interface lunch_productFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_products")
    public lunch_productImpl create(@RequestBody lunch_productImpl lunch_product);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_products/{id}")
    public lunch_productImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_products/fetchdefault")
    public Page<lunch_productImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_products/createbatch")
    public lunch_productImpl createBatch(@RequestBody List<lunch_productImpl> lunch_products);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_products/updatebatch")
    public lunch_productImpl updateBatch(@RequestBody List<lunch_productImpl> lunch_products);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_products/removebatch")
    public lunch_productImpl removeBatch(@RequestBody List<lunch_productImpl> lunch_products);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_products/{id}")
    public lunch_productImpl update(@PathVariable("id") Integer id,@RequestBody lunch_productImpl lunch_product);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_products/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_products/select")
    public Page<lunch_productImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_products/{id}/getdraft")
    public lunch_productImpl getDraft(@PathVariable("id") Integer id,@RequestBody lunch_productImpl lunch_product);



}
