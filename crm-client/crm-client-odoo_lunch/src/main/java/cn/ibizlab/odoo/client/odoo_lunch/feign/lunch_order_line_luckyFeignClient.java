package cn.ibizlab.odoo.client.odoo_lunch.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ilunch_order_line_lucky;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_order_line_luckyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[lunch_order_line_lucky] 服务对象接口
 */
public interface lunch_order_line_luckyFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_line_luckies/fetchdefault")
    public Page<lunch_order_line_luckyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_line_luckies/{id}")
    public lunch_order_line_luckyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_line_luckies/createbatch")
    public lunch_order_line_luckyImpl createBatch(@RequestBody List<lunch_order_line_luckyImpl> lunch_order_line_luckies);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_line_luckies/updatebatch")
    public lunch_order_line_luckyImpl updateBatch(@RequestBody List<lunch_order_line_luckyImpl> lunch_order_line_luckies);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_line_luckies")
    public lunch_order_line_luckyImpl create(@RequestBody lunch_order_line_luckyImpl lunch_order_line_lucky);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_line_luckies/{id}")
    public lunch_order_line_luckyImpl update(@PathVariable("id") Integer id,@RequestBody lunch_order_line_luckyImpl lunch_order_line_lucky);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_line_luckies/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_line_luckies/removebatch")
    public lunch_order_line_luckyImpl removeBatch(@RequestBody List<lunch_order_line_luckyImpl> lunch_order_line_luckies);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_line_luckies/select")
    public Page<lunch_order_line_luckyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_line_luckies/{id}/getdraft")
    public lunch_order_line_luckyImpl getDraft(@PathVariable("id") Integer id,@RequestBody lunch_order_line_luckyImpl lunch_order_line_lucky);



}
