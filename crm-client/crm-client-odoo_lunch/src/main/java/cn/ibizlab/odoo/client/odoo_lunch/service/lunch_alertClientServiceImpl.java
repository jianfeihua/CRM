package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_alert;
import cn.ibizlab.odoo.client.odoo_lunch.config.odoo_lunchClientProperties;
import cn.ibizlab.odoo.core.client.service.Ilunch_alertClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_alertImpl;
import cn.ibizlab.odoo.client.odoo_lunch.feign.lunch_alertFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[lunch_alert] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class lunch_alertClientServiceImpl implements Ilunch_alertClientService {

    lunch_alertFeignClient lunch_alertFeignClient;

    @Autowired
    public lunch_alertClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_lunchClientProperties odoo_lunchClientProperties) {
        if (odoo_lunchClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_alertFeignClient = nameBuilder.target(lunch_alertFeignClient.class,"http://"+odoo_lunchClientProperties.getServiceId()+"/") ;
		}else if (odoo_lunchClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_alertFeignClient = nameBuilder.target(lunch_alertFeignClient.class,odoo_lunchClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ilunch_alert createModel() {
		return new lunch_alertImpl();
	}


    public void removeBatch(List<Ilunch_alert> lunch_alerts){
        if(lunch_alerts!=null){
            List<lunch_alertImpl> list = new ArrayList<lunch_alertImpl>();
            for(Ilunch_alert ilunch_alert :lunch_alerts){
                list.add((lunch_alertImpl)ilunch_alert) ;
            }
            lunch_alertFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ilunch_alert> lunch_alerts){
        if(lunch_alerts!=null){
            List<lunch_alertImpl> list = new ArrayList<lunch_alertImpl>();
            for(Ilunch_alert ilunch_alert :lunch_alerts){
                list.add((lunch_alertImpl)ilunch_alert) ;
            }
            lunch_alertFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ilunch_alert lunch_alert){
        lunch_alertFeignClient.remove(lunch_alert.getId()) ;
    }


    public void get(Ilunch_alert lunch_alert){
        Ilunch_alert clientModel = lunch_alertFeignClient.get(lunch_alert.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_alert.getClass(), false);
        copier.copy(clientModel, lunch_alert, null);
    }


    public Page<Ilunch_alert> fetchDefault(SearchContext context){
        Page<lunch_alertImpl> page = this.lunch_alertFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ilunch_alert> lunch_alerts){
        if(lunch_alerts!=null){
            List<lunch_alertImpl> list = new ArrayList<lunch_alertImpl>();
            for(Ilunch_alert ilunch_alert :lunch_alerts){
                list.add((lunch_alertImpl)ilunch_alert) ;
            }
            lunch_alertFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ilunch_alert lunch_alert){
        Ilunch_alert clientModel = lunch_alertFeignClient.update(lunch_alert.getId(),(lunch_alertImpl)lunch_alert) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_alert.getClass(), false);
        copier.copy(clientModel, lunch_alert, null);
    }


    public void create(Ilunch_alert lunch_alert){
        Ilunch_alert clientModel = lunch_alertFeignClient.create((lunch_alertImpl)lunch_alert) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_alert.getClass(), false);
        copier.copy(clientModel, lunch_alert, null);
    }


    public Page<Ilunch_alert> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ilunch_alert lunch_alert){
        Ilunch_alert clientModel = lunch_alertFeignClient.getDraft(lunch_alert.getId(),(lunch_alertImpl)lunch_alert) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_alert.getClass(), false);
        copier.copy(clientModel, lunch_alert, null);
    }



}

