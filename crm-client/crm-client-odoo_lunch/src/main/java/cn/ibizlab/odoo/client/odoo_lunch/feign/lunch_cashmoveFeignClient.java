package cn.ibizlab.odoo.client.odoo_lunch.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ilunch_cashmove;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_cashmoveImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[lunch_cashmove] 服务对象接口
 */
public interface lunch_cashmoveFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_cashmoves/createbatch")
    public lunch_cashmoveImpl createBatch(@RequestBody List<lunch_cashmoveImpl> lunch_cashmoves);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_cashmoves/fetchdefault")
    public Page<lunch_cashmoveImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_cashmoves/updatebatch")
    public lunch_cashmoveImpl updateBatch(@RequestBody List<lunch_cashmoveImpl> lunch_cashmoves);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_cashmoves/removebatch")
    public lunch_cashmoveImpl removeBatch(@RequestBody List<lunch_cashmoveImpl> lunch_cashmoves);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_cashmoves/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_cashmoves")
    public lunch_cashmoveImpl create(@RequestBody lunch_cashmoveImpl lunch_cashmove);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_cashmoves/{id}")
    public lunch_cashmoveImpl update(@PathVariable("id") Integer id,@RequestBody lunch_cashmoveImpl lunch_cashmove);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_cashmoves/{id}")
    public lunch_cashmoveImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_cashmoves/select")
    public Page<lunch_cashmoveImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_cashmoves/{id}/getdraft")
    public lunch_cashmoveImpl getDraft(@PathVariable("id") Integer id,@RequestBody lunch_cashmoveImpl lunch_cashmove);



}
