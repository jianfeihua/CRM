<?xml version="1.0" encoding="UTF-8"?>
<!--
    scan: 当此属性设置为true时，配置文件如果发生改变，将会被重新加载，默认值为true。
    scanPeriod: 设置监测配置文件是否有修改的时间间隔，如果没有给出时间单位，默认单位是毫秒。
            当scan为true时，此属性生效。默认的时间间隔为1分钟。
            时间单位：milliseconds, seconds, minutes , hours
    debug: 当此属性设置为true时，将打印出logback内部日志信息，实时查看logback运行状态。默认值为false。
-->
<configuration scan="false" scanPeriod="60 seconds" debug="false">
    <!--引用yaml文件配置,可以使用${变量名}方式使用-->
    <!--设置变量，name为变量名，value为值，可以使用${变量名}方式使用-->
    <property name="LOG_PATH" value="logs"/>
    <property name="systemname" value="crm" />
    <!--
        定义PatternLayoutEncoder中日志输出格式：
        %d{yyyy-MM-dd HH:mm:ss.SSS} 时间戳及时间格式
        %t              当前线程
        %level        日志级别
        %颜色名(正则)    用于控制台彩色日志生成
        %logger         日志名（类/类+方法）名
        %method         当前logger所在方法。
        %msg            日志信息。
        %n              换行。
        X{key:-defaultVal}、mdc{key:-defaultVal} 输出生成日志事件的线程的 MDC（自定义参数）

        %logger{39}     设置最大允许长度为39，超过将从最左边字符开始简写，但不包含最右边的部分。
        %-20.30logger   logger长度小于20，右边开始填充空格，大于30从前面开始截断
        其他格式标识见官网说明：http://www.logback.cn/06%E7%AC%AC%E5%85%AD%E7%AB%A0Layouts.html
    -->
    <!--日志打印正则，用于文件输出-->
    <property name="LOG_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss.SSS}  [%t][%-5level] %logger [%X{method:-#}]:  %msg%n"/>
    <!--
        Appender: 设置日志信息的去向,常用的有以下几个
        ch.qos.logback.core.ConsoleAppender (控制台)
        ch.qos.logback.core.rolling.RollingFileAppender (文件大小到达指定尺寸的时候产生一个新文件)
        ch.qos.logback.core.FileAppender (文件，不推荐使用)
    -->

    <!-- 控制台输出 -->
    <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>${LOG_PATTERN}</pattern>
        </encoder>
    </appender>

    <!-- 按照每天生成日志文件 -->
    <appender name="file" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!--日志文件输出的文件名-->
            <FileNamePattern>${LOG_PATH}/${systemname}.%d{yyyy-MM-dd}.log</FileNamePattern>
            <!--日志文件保留天数-->
            <MaxHistory>30</MaxHistory>
        </rollingPolicy>
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>${LOG_PATTERN}</pattern>
        </encoder>

        <!--日志文件最大的大小-->
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <MaxFileSize>100MB</MaxFileSize>
        </triggeringPolicy>
    </appender>

    <!--logstash远程日志添加-->
    <appender name="logstash" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
        <!--logstash server所在位置-->
        <destination>127.0.0.1:5044</destination>
        <encoder class="net.logstash.logback.encoder.LogstashEncoder">
            <providers>
                <timestamp>
                    <timeZone>CST</timeZone>
                </timestamp>
            </providers>
        </encoder>
    </appender>

    <!--
        根据日志级别INFO、DEBUG、ERROR分别写入不同文件。
        见appender SYS_INFO、SYS_DEBUG、SYS_ERROR
    -->
    <appender name="SYS_INFO" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--<File>langooo-api/log/info.log</File>-->
        <append>true</append>
        <!--过滤器,只打INFO级别的日志-->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>INFO</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_PATH}/info/${systemname}.%d{yyyy-MM-dd}.log</fileNamePattern>
            <maxHistory>30</maxHistory>
        </rollingPolicy>

        <encoder charset="UTF-8">
            <pattern>${LOG_PATTERN}</pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>
    <appender name="SYS_DEBUG" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--<File>langooo-api/log/error.log</File>-->
        <append>true</append>
        <!--过滤器,只打ERROR级别的日志-->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>DEBUG</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_PATH}/debug/${systemname}.%d{yyyy-MM-dd}.log</fileNamePattern>
            <maxHistory>12</maxHistory>
        </rollingPolicy>

        <encoder charset="UTF-8">
            <pattern>${LOG_PATTERN}</pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>
    <appender name="SYS_ERROR" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--<File>langooo-api/log/error.log</File>-->
        <append>true</append>
        <!--过滤器,只打ERROR级别的日志-->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>ERROR</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_PATH}/error/${systemname}.%d{yyyy-MM-dd}.log</fileNamePattern>
            <maxHistory>12</maxHistory>
        </rollingPolicy>

        <encoder charset="UTF-8">
            <pattern>${LOG_PATTERN}</pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>

    <!--
        根据不同服务（包名），写入不同文件。
        需要搭配<appender>与<logger>使用。
        见resourceAppender及对应的logger
        见serviceAppender及对应的logger
    -->
    <!--指定net.ibizsys.sample.mng.service.humanservice.resource日志文件输出位置，需要定义Logger使用，见下面logger标签-->
    <appender name="resourceAppender" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--<file>/logs/resource-%d.log</file>-->
        <append>true</append>

        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_PATH}/resource/${systemname}.%d{yyyy-MM-dd}.log</fileNamePattern>
            <maxHistory>30</maxHistory>
        </rollingPolicy>

        <encoder charset="UTF-8">
            <pattern>${LOG_PATTERN}</pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>
    <!--指定core日志文件输出位置，需要定义Logger使用，见下面logger标签。-->
    <appender name="serviceAppender" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--<file>/logs/service-%d.log</file>-->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_PATH}/service/${systemname}.%d{yyyy-MM-dd}.log</fileNamePattern>
            <maxHistory>30</maxHistory>
        </rollingPolicy>

        <encoder charset="UTF-8">
            <pattern>${LOG_PATTERN}</pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>

    <logger name="cn.ibizlab.odoo" level="DEBUG" additivity="false">
        <appender-ref ref="serviceAppender"/>
        <appender-ref ref="file"/>
        <appender-ref ref="SYS_INFO"/>
        <appender-ref ref="SYS_DEBUG"/>
        <appender-ref ref="SYS_ERROR"/>
        <appender-ref ref="console"/>
    </logger>
    <!--
    也是<logger>元素，但是它是根logger。只有一个level属性，应为已经被命名为"root".
    level:用来设置打印级别，大小写无关：TRACE, DEBUG, INFO, WARN, ERROR, ALL 和 OFF，
    不能设置为INHERITED或者同义词NULL。默认是DEBUG。
    优先级低于yaml配置。
    当配置了子logger时，子logger优先级更高。
    -->
    <root level="INFO">
    </root>

</configuration>
