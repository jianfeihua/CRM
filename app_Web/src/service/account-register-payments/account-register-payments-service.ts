import { Http,Util } from '@/utils';
import Account_register_paymentsServiceBase from './account-register-payments-service-base';


/**
 * 登记付款服务对象
 *
 * @export
 * @class Account_register_paymentsService
 * @extends {Account_register_paymentsServiceBase}
 */
export default class Account_register_paymentsService extends Account_register_paymentsServiceBase {

    /**
     * Creates an instance of  Account_register_paymentsService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_register_paymentsService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}