import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 付款服务对象基类
 *
 * @export
 * @class Account_paymentServiceBase
 * @extends {EntityServie}
 */
export default class Account_paymentServiceBase extends EntityService {

    /**
     * Creates an instance of  Account_paymentServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_paymentServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Account_paymentServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='account_payment';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'account_payments';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/select`,data,isloading);
        }
            return Http.getInstance().post(`/account_payments/${context.account_payment}/select`,data,isloading);
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/checkkey`,data,isloading);
        }
            return Http.getInstance().post(`/account_payments/${context.account_payment}/checkkey`,data,isloading);
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/account_payments/${context.account_payment}`,data,isloading);
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/account_payments/getdraft`,isloading);
        }
        let res:any = await  Http.getInstance().get(`/account_payments/getdraft`,isloading);
        res.data.account_payment = data.account_payment;
        return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}`,isloading);
        }
            return Http.getInstance().delete(`/account_payments/${context.account_payment}`,isloading);

    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/removebatch`,isloading);
        }
            return Http.getInstance().delete(`/account_payments/${context.account_payment}/removebatch`,isloading);

    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/save`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/account_payments/${context.account_payment}/save`,data,isloading);
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/updatebatch`,data,isloading);
        }
            return Http.getInstance().put(`/account_payments/${context.account_payment}/updatebatch`,data,isloading);
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_payments`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/account_payments`,data,isloading);
        return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}`,isloading);
        }
            let res:any = await Http.getInstance().get(`/account_payments/${context.account_payment}`,isloading);
            return res;

    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_payment){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/createbatch`,data,isloading);
        }
            return Http.getInstance().post(`/account_payments/${context.account_payment}/createbatch`,data,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_paymentServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/account_payments/fetchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/account_payments/fetchdefault`,tempData,isloading);
    }
}