import { Http,Util } from '@/utils';
import Sale_orderServiceBase from './sale-order-service-base';


/**
 * 销售订单服务对象
 *
 * @export
 * @class Sale_orderService
 * @extends {Sale_orderServiceBase}
 */
export default class Sale_orderService extends Sale_orderServiceBase {

    /**
     * Creates an instance of  Sale_orderService.
     * 
     * @param {*} [opts={}]
     * @memberof  Sale_orderService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}