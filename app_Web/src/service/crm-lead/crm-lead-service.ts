import { Http,Util } from '@/utils';
import Crm_leadServiceBase from './crm-lead-service-base';


/**
 * 线索/商机服务对象
 *
 * @export
 * @class Crm_leadService
 * @extends {Crm_leadServiceBase}
 */
export default class Crm_leadService extends Crm_leadServiceBase {

    /**
     * Creates an instance of  Crm_leadService.
     * 
     * @param {*} [opts={}]
     * @memberof  Crm_leadService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}