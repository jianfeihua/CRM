import { Http,Util } from '@/utils';
import Account_journalServiceBase from './account-journal-service-base';


/**
 * 日记账服务对象
 *
 * @export
 * @class Account_journalService
 * @extends {Account_journalServiceBase}
 */
export default class Account_journalService extends Account_journalServiceBase {

    /**
     * Creates an instance of  Account_journalService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_journalService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}