import { Http,Util } from '@/utils';
import Crm_teamServiceBase from './crm-team-service-base';


/**
 * 销售渠道服务对象
 *
 * @export
 * @class Crm_teamService
 * @extends {Crm_teamServiceBase}
 */
export default class Crm_teamService extends Crm_teamServiceBase {

    /**
     * Creates an instance of  Crm_teamService.
     * 
     * @param {*} [opts={}]
     * @memberof  Crm_teamService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}