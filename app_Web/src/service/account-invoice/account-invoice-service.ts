import { Http,Util } from '@/utils';
import Account_invoiceServiceBase from './account-invoice-service-base';


/**
 * 发票服务对象
 *
 * @export
 * @class Account_invoiceService
 * @extends {Account_invoiceServiceBase}
 */
export default class Account_invoiceService extends Account_invoiceServiceBase {

    /**
     * Creates an instance of  Account_invoiceService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_invoiceService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}