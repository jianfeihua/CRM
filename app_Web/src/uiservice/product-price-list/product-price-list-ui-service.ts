import Product_price_listUIServiceBase from './product-price-list-ui-service-base';

/**
 * 基于价格列表版本的单位产品价格UI服务对象
 *
 * @export
 * @class Product_price_listUIService
 */
export default class Product_price_listUIService extends Product_price_listUIServiceBase {

    /**
     * Creates an instance of  Product_price_listUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_price_listUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}