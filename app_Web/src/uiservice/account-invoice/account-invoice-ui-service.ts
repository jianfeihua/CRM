import Account_invoiceUIServiceBase from './account-invoice-ui-service-base';

/**
 * 发票UI服务对象
 *
 * @export
 * @class Account_invoiceUIService
 */
export default class Account_invoiceUIService extends Account_invoiceUIServiceBase {

    /**
     * Creates an instance of  Account_invoiceUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_invoiceUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}