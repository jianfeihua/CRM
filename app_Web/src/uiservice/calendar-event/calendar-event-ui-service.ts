import Calendar_eventUIServiceBase from './calendar-event-ui-service-base';

/**
 * 活动UI服务对象
 *
 * @export
 * @class Calendar_eventUIService
 */
export default class Calendar_eventUIService extends Calendar_eventUIServiceBase {

    /**
     * Creates an instance of  Calendar_eventUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Calendar_eventUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}