import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DynaMain2 部件服务对象
 *
 * @export
 * @class DynaMain2Service
 */
export default class DynaMain2Service extends ControlService {
}