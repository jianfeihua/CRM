import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Trend 部件服务对象
 *
 * @export
 * @class TrendService
 */
export default class TrendService extends ControlService {
}