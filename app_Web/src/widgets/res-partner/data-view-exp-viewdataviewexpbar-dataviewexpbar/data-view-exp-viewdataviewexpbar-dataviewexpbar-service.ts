import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Res_partnerService from '@/service/res-partner/res-partner-service';
import DataViewExpViewdataviewexpbarModel from './data-view-exp-viewdataviewexpbar-dataviewexpbar-model';


/**
 * DataViewExpViewdataviewexpbar 部件服务对象
 *
 * @export
 * @class DataViewExpViewdataviewexpbarService
 */
export default class DataViewExpViewdataviewexpbarService extends ControlService {

    /**
     * 客户服务对象
     *
     * @type {Res_partnerService}
     * @memberof DataViewExpViewdataviewexpbarService
     */
    public appEntityService: Res_partnerService = new Res_partnerService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof DataViewExpViewdataviewexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of DataViewExpViewdataviewexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof DataViewExpViewdataviewexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new DataViewExpViewdataviewexpbarModel();
    }

}