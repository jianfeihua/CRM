import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Calendar_eventService from '@/service/calendar-event/calendar-event-service';
import CalendarEventModel from './calendar-event-calendar-model';


/**
 * CalendarEvent 部件服务对象
 *
 * @export
 * @class CalendarEventService
 */
export default class CalendarEventService extends ControlService {

    /**
     * 活动服务对象
     *
     * @type {Calendar_eventService}
     * @memberof CalendarEventService
     */
    public appEntityService: Calendar_eventService = new Calendar_eventService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof CalendarEventService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of CalendarEventService.
     * 
     * @param {*} [opts={}]
     * @memberof CalendarEventService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new CalendarEventModel();
    }


    /**
     * 事件配置集合
     *
     * @public
     * @type {any[]}
     * @memberof CalendarEvent
     */
    public eventsConfig: any[] = [
        {
          itemName : '活动',
          itemType : 'Event',
          color : '',
          textColor : '',
        },
    ];

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CalendarEventService
     */
    @Errorlog
    public search(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let _this = this;
        return new Promise((resolve: any, reject: any) => {
            let promises:any = [];
            let tempRequest:any;
            tempRequest = this.handleRequestData(action,context,data,true,"Event");
            promises.push(this.appEntityService.FetchDefault(tempRequest.context, tempRequest.data, isloading));
            Promise.all(promises).then((resArray: any) => {
                let _data:any = [];
                resArray.forEach((response:any,resIndex:number) => {
                    if (!response || response.status !== 200) {
                        return;
                    }
                    let _response: any = JSON.parse(JSON.stringify(response));
                    _response.data.forEach((item:any,index:number) =>{
                        _response.data[index].color = _this.eventsConfig[resIndex].color;
                        _response.data[index].textColor = _this.eventsConfig[resIndex].textColor;
                        _response.data[index].itemType = _this.eventsConfig[resIndex].itemType;
                    });
                    ;
                    _this.handleResponse(action, _response,false,_this.eventsConfig[resIndex].itemType);
                    _data.push(..._response.data);
                });
                // 排序
                _data.sort((a:any, b:any)=>{
                    let dateA = new Date(Date.parse(a.start.replace(/-/g, "/")));
                    let dateB = new Date(Date.parse(b.start.replace(/-/g, "/")));
                    return dateA > dateB ? 1 : -1 ;
                });
                let result = {status: 200, data: _data};
                resolve(result);
            }).catch((response: any) => {
                reject(response);
            });  
        });
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CalendarEventService
     */
    @Errorlog
    public update(itemType: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            let result: any;
            let tempRequest:any;
            switch(itemType) {
                case "Event":
                    tempRequest = this.handleRequestData("",context,data,false,"Event");
                    result = this.appEntityService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
            }
            if(result){
                result.then((response: any) => {
                    this.handleResponse("", response);
                    resolve(response);
                }).catch((response: any) => {
                    reject(response);
                });
            }else{
              reject("没有匹配的实体服务");
            }
        });
    }

    /**
     * 处理request请求数据
     * 
     * @param action 行为 
     * @param data 数据
     * @memberof ControlService
     */
    public handleRequestData(action: string,context:any ={},data: any = {},isMerge:boolean = false,itemType:string=""){
        let model: any = this.getMode();
        model.itemType = itemType;
        return super.handleRequestData(action,context,data,isMerge);
    }

    /**
     * 处理response返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ControlService
     */
    public handleResponse(action: string, response: any,isCreate:boolean = false,itemType:string=""){
        let model: any = this.getMode();
        model.itemType = itemType;
        super.handleResponse(action,response,isCreate);
    }

}