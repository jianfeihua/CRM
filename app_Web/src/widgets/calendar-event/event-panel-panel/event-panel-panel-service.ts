import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * EventPanel 部件服务对象
 *
 * @export
 * @class EventPanelService
 */
export default class EventPanelService extends ControlService {
}