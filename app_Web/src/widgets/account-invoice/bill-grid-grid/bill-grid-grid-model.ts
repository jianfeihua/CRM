/**
 * BillGrid 部件模型
 *
 * @export
 * @class BillGridModel
 */
export default class BillGridModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof BillGridGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof BillGridGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'cash_rounding_id',
          prop: 'cash_rounding_id',
          dataType: 'PICKUP',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'refund_invoice_id',
          prop: 'refund_invoice_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_shipping_id',
          prop: 'partner_shipping_id',
          dataType: 'PICKUP',
        },
        {
          name: 'state',
          prop: 'state',
          dataType: 'SSCODELIST',
        },
        {
          name: 'partner_bank_id',
          prop: 'partner_bank_id',
          dataType: 'PICKUP',
        },
        {
          name: 'date',
          prop: 'date',
          dataType: 'DATE',
        },
        {
          name: 'medium_id',
          prop: 'medium_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'vendor_bill_purchase_id',
          prop: 'vendor_bill_purchase_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id_text',
          prop: 'user_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'payment_term_id',
          prop: 'payment_term_id',
          dataType: 'PICKUP',
        },
        {
          name: 'purchase_id',
          prop: 'purchase_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'account_id',
          prop: 'account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'message_needaction_counter',
          prop: 'message_needaction_counter',
          dataType: 'INT',
        },
        {
          name: 'source_id',
          prop: 'source_id',
          dataType: 'PICKUP',
        },
        {
          name: 'incoterm_id',
          prop: 'incoterm_id',
          dataType: 'PICKUP',
        },
        {
          name: 'fiscal_position_id',
          prop: 'fiscal_position_id',
          dataType: 'PICKUP',
        },
        {
          name: 'incoterms_id',
          prop: 'incoterms_id',
          dataType: 'PICKUP',
        },
        {
          name: 'campaign_id',
          prop: 'campaign_id',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_id_text',
          prop: 'partner_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'move_id',
          prop: 'move_id',
          dataType: 'PICKUP',
        },
        {
          name: 'amount_total',
          prop: 'amount_total',
          dataType: 'DECIMAL',
        },
        {
          name: 'journal_id',
          prop: 'journal_id',
          dataType: 'PICKUP',
        },
        {
          name: 'team_id',
          prop: 'team_id',
          dataType: 'PICKUP',
        },
        {
          name: 'commercial_partner_id',
          prop: 'commercial_partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'vendor_bill_id',
          prop: 'vendor_bill_id',
          dataType: 'PICKUP',
        },
        {
          name: 'account_invoice',
          prop: 'id',
        },
      {
        name: 'n_partner_id_text_eq',
        prop: 'n_partner_id_text_eq',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_partner_id_eq',
        prop: 'n_partner_id_eq',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}