/**
 * TeamCard2 部件模型
 *
 * @export
 * @class TeamCard2Model
 */
export default class TeamCard2Model {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof TeamCard2Dataviewexpbar_dataviewMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srficonpath',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},

			{
				name: 'crm_team',
				prop: 'id',
				dataType: 'FONTKEY',
			},


      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      {
        name:'srfparentdata',
        prop:'srfparentdata'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}