import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * PMainTabExpViewtabviewpanel2 部件服务对象
 *
 * @export
 * @class PMainTabExpViewtabviewpanel2Service
 */
export default class PMainTabExpViewtabviewpanel2Service extends ControlService {
}