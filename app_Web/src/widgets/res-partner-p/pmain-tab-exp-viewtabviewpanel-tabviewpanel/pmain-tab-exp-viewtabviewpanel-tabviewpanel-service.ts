import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * PMainTabExpViewtabviewpanel 部件服务对象
 *
 * @export
 * @class PMainTabExpViewtabviewpanelService
 */
export default class PMainTabExpViewtabviewpanelService extends ControlService {
}