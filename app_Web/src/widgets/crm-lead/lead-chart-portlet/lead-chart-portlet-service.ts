import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * LeadChart 部件服务对象
 *
 * @export
 * @class LeadChartService
 */
export default class LeadChartService extends ControlService {
}