import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * AllLeadTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class AllLeadTreeExpViewtreeexpbarService
 */
export default class AllLeadTreeExpViewtreeexpbarService extends ControlService {
}