/**
 * Funnel 部件模型
 *
 * @export
 * @class FunnelModel
 */
export default class FunnelModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof FunnelChartMode
	 */
	public getDataItems(): any[] {
		return [
      {
        name:'query',
        prop:'query'
      },
		]
	}

}