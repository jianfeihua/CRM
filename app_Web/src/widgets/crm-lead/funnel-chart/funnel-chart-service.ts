import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Crm_leadService from '@/service/crm-lead/crm-lead-service';
import FunnelModel from './funnel-chart-model';


/**
 * Funnel 部件服务对象
 *
 * @export
 * @class FunnelService
 */
export default class FunnelService extends ControlService {

    /**
     * 线索/商机服务对象
     *
     * @type {Crm_leadService}
     * @memberof FunnelService
     */
    public appEntityService: Crm_leadService = new Crm_leadService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof FunnelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of FunnelService.
     * 
     * @param {*} [opts={}]
     * @memberof FunnelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new FunnelModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof FunnelService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleSeries(response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
    /**
     * 生成图表数据
     *
     * @param {*} response
     * @memberof FunnelService
     */
    public handleSeries(response: any) {
        let chartOption:any = {};
        let catalogFields: any = ["stage_id_text",];
        let valueFields: any = [[ "id", "ID" ],];
        let otherFields: any = ['planned_revenue'];
        // 数据按分类属性分组处理
        let xFields:any = [];
        let yFields:any = [];
        let oFields: any = [];
        valueFields.forEach((field: any,index: number) => {
            yFields[index] = [];
        });
        response.data.forEach((item:any) => {
            if(xFields.indexOf(item[catalogFields[0]]) > -1){
                let num = xFields.indexOf(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index][num] += item[field[0]];
                });
                oFields[num] += item[otherFields[0]];
            }else{
                xFields.push(item[catalogFields[0]]);
                oFields.push(item[otherFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index].push(item[field[0]]);
                });
            }
        });
        let series: any = [];
        valueFields.forEach((field: any,index: number) => {
            let yData: any = [];
            xFields.forEach((item:any, num: number) => {
                yData.push({value: (100 - (100 / xFields.length) * num), name: item, sum: yFields[index][num], total: oFields[num]});
            });
            yData.sort(function (a:any, b:any) { return a.value - b.value; });
            series.push({
                name:field[1],
                type:"funnel",
                data:yData,
                minSize: '40%',
                label: {
                    position: 'center',
                    formatter: (params: any) => {
                        return `${params.data.name}（${params.data.sum}个, 金额￥${params.data.total}）`
                    }
                },
            });
        });
        chartOption.series = series;
        response.data = chartOption;
    }
}