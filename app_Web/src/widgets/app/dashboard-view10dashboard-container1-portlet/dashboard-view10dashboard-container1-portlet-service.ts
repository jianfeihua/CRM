import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DashboardView10dashboard_container1 部件服务对象
 *
 * @export
 * @class DashboardView10dashboard_container1Service
 */
export default class DashboardView10dashboard_container1Service extends ControlService {
}