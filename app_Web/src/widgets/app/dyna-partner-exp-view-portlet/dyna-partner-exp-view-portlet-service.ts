import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DynaPartnerExpView 部件服务对象
 *
 * @export
 * @class DynaPartnerExpViewService
 */
export default class DynaPartnerExpViewService extends ControlService {
}