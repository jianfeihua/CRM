/**
 * LeaderPortalView_db 部件模型
 *
 * @export
 * @class LeaderPortalView_dbModel
 */
export default class LeaderPortalView_dbModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof LeaderPortalView_dbModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}