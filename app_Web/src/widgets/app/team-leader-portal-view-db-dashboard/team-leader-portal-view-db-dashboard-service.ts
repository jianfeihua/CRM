import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * TeamLeaderPortalView_db 部件服务对象
 *
 * @export
 * @class TeamLeaderPortalView_dbService
 */
export default class TeamLeaderPortalView_dbService extends ControlService {
}