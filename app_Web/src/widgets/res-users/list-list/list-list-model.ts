/**
 * List 部件模型
 *
 * @export
 * @class ListModel
 */
export default class ListModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ListDashboard_sysportlet2_listMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'mobile',
				prop: 'mobile',
				dataType: 'PICKUPDATA',
			},
			{
				name: 'email',
				prop: 'email',
				dataType: 'PICKUPDATA',
			},
			{
				name: 'name',
				prop: 'name',
				dataType: 'PICKUPTEXT',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}