/**
 * SalesPersonCard 部件模型
 *
 * @export
 * @class SalesPersonCardModel
 */
export default class SalesPersonCardModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof SalesPersonCardDataViewMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srficonpath',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'PICKUPTEXT',
			},

			{
				name: 'res_users',
				prop: 'id',
				dataType: 'FONTKEY',
			},



      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      {
        name:'srfparentdata',
        prop:'srfparentdata'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}