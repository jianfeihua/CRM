/**
 * List 部件模型
 *
 * @export
 * @class ListModel
 */
export default class ListModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ListListMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'user_id_text',
				prop: 'user_id_text',
				dataType: 'PICKUPTEXT',
			},
			{
				name: 'activity_type_id_text',
				prop: 'activity_type_id_text',
				dataType: 'PICKUPTEXT',
			},
			{
				name: 'write_date',
				prop: 'write_date',
				dataType: 'DATETIME',
			},
			{
				name: 'create_user_id_text',
				prop: 'create_user_id_text',
				dataType: 'PICKUPTEXT',
			},
			{
				name: 'date_deadline',
				prop: 'date_deadline',
				dataType: 'DATE',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}