import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Mail_activityService from '@/service/mail-activity/mail-activity-service';
import CalendarExpViewcalendarexpbarModel from './calendar-exp-viewcalendarexpbar-calendarexpbar-model';


/**
 * CalendarExpViewcalendarexpbar 部件服务对象
 *
 * @export
 * @class CalendarExpViewcalendarexpbarService
 */
export default class CalendarExpViewcalendarexpbarService extends ControlService {

    /**
     * 活动服务对象
     *
     * @type {Mail_activityService}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public appEntityService: Mail_activityService = new Mail_activityService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of CalendarExpViewcalendarexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof CalendarExpViewcalendarexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new CalendarExpViewcalendarexpbarModel();
    }

}