import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * List 部件服务对象
 *
 * @export
 * @class ListService
 */
export default class ListService extends ControlService {
}