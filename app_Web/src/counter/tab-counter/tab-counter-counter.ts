import TabCounterCounterServiceBase from './tab-counter-counter-base';

/**
 * 分页计数器计数器服务对象
 *
 * @export
 * @class TabCounterCounterService
 */
export default class TabCounterCounterService extends TabCounterCounterServiceBase {

    /**
     * Creates an instance of  TabCounterCounterService.
     * 
     * @param {*} [opts={}]
     * @memberof  TabCounterCounterService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}