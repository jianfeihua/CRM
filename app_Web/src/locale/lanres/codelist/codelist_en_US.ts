export default {
    SALE_ORDER__STATE: {
        'draft': '报价单',
        'sent': '报价已发送',
        'sale': '销售订单',
        'done': '已锁定',
        'cancel': '已取消',
        empty: '',
    },
    ACCOUNT_INVOICE__STATE: {
        empty: '',
    },
    TrueFalse2: {
        'true': '是',
        'false': '否',
        empty: '',
    },
    CRM_LEAD__ACTIVITY_STATE: {
        'overdue': '逾期',
        'today': '今日',
        'planned': '已计划',
        empty: '',
    },
};