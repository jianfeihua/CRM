package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_lead.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Crm_leadDTO]
 */
public class Crm_leadDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Crm_leadActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [MEETING_COUNT]
     *
     */
    @Crm_leadMeeting_countDefault(info = "默认规则")
    private Integer meeting_count;

    @JsonIgnore
    private boolean meeting_countDirtyFlag;

    /**
     * 属性 [IBIZFUNCTION]
     *
     */
    @Crm_leadIbizfunctionDefault(info = "默认规则")
    private String ibizfunction;

    @JsonIgnore
    private boolean ibizfunctionDirtyFlag;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @Crm_leadMessage_bounceDefault(info = "默认规则")
    private Integer message_bounce;

    @JsonIgnore
    private boolean message_bounceDirtyFlag;

    /**
     * 属性 [CITY]
     *
     */
    @Crm_leadCityDefault(info = "默认规则")
    private String city;

    @JsonIgnore
    private boolean cityDirtyFlag;

    /**
     * 属性 [DAY_CLOSE]
     *
     */
    @Crm_leadDay_closeDefault(info = "默认规则")
    private Double day_close;

    @JsonIgnore
    private boolean day_closeDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Crm_leadActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Crm_leadMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Crm_leadMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [EXPECTED_REVENUE]
     *
     */
    @Crm_leadExpected_revenueDefault(info = "默认规则")
    private Double expected_revenue;

    @JsonIgnore
    private boolean expected_revenueDirtyFlag;

    /**
     * 属性 [DATE_CLOSED]
     *
     */
    @Crm_leadDate_closedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_closed;

    @JsonIgnore
    private boolean date_closedDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Crm_leadCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @Crm_leadEmail_ccDefault(info = "默认规则")
    private String email_cc;

    @JsonIgnore
    private boolean email_ccDirtyFlag;

    /**
     * 属性 [CONTACT_NAME]
     *
     */
    @Crm_leadContact_nameDefault(info = "默认规则")
    private String contact_name;

    @JsonIgnore
    private boolean contact_nameDirtyFlag;

    /**
     * 属性 [DATE_LAST_STAGE_UPDATE]
     *
     */
    @Crm_leadDate_last_stage_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_last_stage_update;

    @JsonIgnore
    private boolean date_last_stage_updateDirtyFlag;

    /**
     * 属性 [PLANNED_REVENUE]
     *
     */
    @Crm_leadPlanned_revenueDefault(info = "默认规则")
    private Double planned_revenue;

    @JsonIgnore
    private boolean planned_revenueDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Crm_leadWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Crm_leadActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @Crm_leadDate_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_deadline;

    @JsonIgnore
    private boolean date_deadlineDirtyFlag;

    /**
     * 属性 [ZIP]
     *
     */
    @Crm_leadZipDefault(info = "默认规则")
    private String zip;

    @JsonIgnore
    private boolean zipDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Crm_leadNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Crm_leadIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Crm_leadDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [MOBILE]
     *
     */
    @Crm_leadMobileDefault(info = "默认规则")
    private String mobile;

    @JsonIgnore
    private boolean mobileDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Crm_leadMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Crm_leadMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Crm_leadActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Crm_leadActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Crm_leadTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [WEBSITE]
     *
     */
    @Crm_leadWebsiteDefault(info = "默认规则")
    private String website;

    @JsonIgnore
    private boolean websiteDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Crm_leadMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @Crm_leadEmail_fromDefault(info = "默认规则")
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;

    /**
     * 属性 [DATE_CONVERSION]
     *
     */
    @Crm_leadDate_conversionDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_conversion;

    @JsonIgnore
    private boolean date_conversionDirtyFlag;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @Crm_leadPartner_nameDefault(info = "默认规则")
    private String partner_name;

    @JsonIgnore
    private boolean partner_nameDirtyFlag;

    /**
     * 属性 [SALE_AMOUNT_TOTAL]
     *
     */
    @Crm_leadSale_amount_totalDefault(info = "默认规则")
    private Double sale_amount_total;

    @JsonIgnore
    private boolean sale_amount_totalDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Crm_leadMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Crm_leadMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Crm_lead__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [KANBAN_STATE]
     *
     */
    @Crm_leadKanban_stateDefault(info = "默认规则")
    private String kanban_state;

    @JsonIgnore
    private boolean kanban_stateDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Crm_leadMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Crm_leadMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [REFERRED]
     *
     */
    @Crm_leadReferredDefault(info = "默认规则")
    private String referred;

    @JsonIgnore
    private boolean referredDirtyFlag;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @Crm_leadProbabilityDefault(info = "默认规则")
    private Double probability;

    @JsonIgnore
    private boolean probabilityDirtyFlag;

    /**
     * 属性 [DATE_ACTION_LAST]
     *
     */
    @Crm_leadDate_action_lastDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_action_last;

    @JsonIgnore
    private boolean date_action_lastDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Crm_leadMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [DATE_OPEN]
     *
     */
    @Crm_leadDate_openDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_open;

    @JsonIgnore
    private boolean date_openDirtyFlag;

    /**
     * 属性 [PHONE]
     *
     */
    @Crm_leadPhoneDefault(info = "默认规则")
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Crm_leadMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [DAY_OPEN]
     *
     */
    @Crm_leadDay_openDefault(info = "默认规则")
    private Double day_open;

    @JsonIgnore
    private boolean day_openDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Crm_leadActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [SALE_NUMBER]
     *
     */
    @Crm_leadSale_numberDefault(info = "默认规则")
    private Integer sale_number;

    @JsonIgnore
    private boolean sale_numberDirtyFlag;

    /**
     * 属性 [STREET2]
     *
     */
    @Crm_leadStreet2Default(info = "默认规则")
    private String street2;

    @JsonIgnore
    private boolean street2DirtyFlag;

    /**
     * 属性 [ORDER_IDS]
     *
     */
    @Crm_leadOrder_idsDefault(info = "默认规则")
    private String order_ids;

    @JsonIgnore
    private boolean order_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Crm_leadMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [STREET]
     *
     */
    @Crm_leadStreetDefault(info = "默认规则")
    private String street;

    @JsonIgnore
    private boolean streetDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Crm_leadColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Crm_leadWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Crm_leadActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Crm_leadDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Crm_leadTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @Crm_leadIs_blacklistedDefault(info = "默认规则")
    private String is_blacklisted;

    @JsonIgnore
    private boolean is_blacklistedDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Crm_leadMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Crm_leadPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Crm_leadUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @Crm_leadSource_id_textDefault(info = "默认规则")
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ADDRESS_NAME]
     *
     */
    @Crm_leadPartner_address_nameDefault(info = "默认规则")
    private String partner_address_name;

    @JsonIgnore
    private boolean partner_address_nameDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Crm_leadMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY]
     *
     */
    @Crm_leadCompany_currencyDefault(info = "默认规则")
    private Integer company_currency;

    @JsonIgnore
    private boolean company_currencyDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Crm_leadTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ADDRESS_MOBILE]
     *
     */
    @Crm_leadPartner_address_mobileDefault(info = "默认规则")
    private String partner_address_mobile;

    @JsonIgnore
    private boolean partner_address_mobileDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Crm_leadWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Crm_leadCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [USER_EMAIL]
     *
     */
    @Crm_leadUser_emailDefault(info = "默认规则")
    private String user_email;

    @JsonIgnore
    private boolean user_emailDirtyFlag;

    /**
     * 属性 [USER_LOGIN]
     *
     */
    @Crm_leadUser_loginDefault(info = "默认规则")
    private String user_login;

    @JsonIgnore
    private boolean user_loginDirtyFlag;

    /**
     * 属性 [PARTNER_ADDRESS_PHONE]
     *
     */
    @Crm_leadPartner_address_phoneDefault(info = "默认规则")
    private String partner_address_phone;

    @JsonIgnore
    private boolean partner_address_phoneDirtyFlag;

    /**
     * 属性 [STATE_ID_TEXT]
     *
     */
    @Crm_leadState_id_textDefault(info = "默认规则")
    private String state_id_text;

    @JsonIgnore
    private boolean state_id_textDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @Crm_leadCampaign_id_textDefault(info = "默认规则")
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_IS_BLACKLISTED]
     *
     */
    @Crm_leadPartner_is_blacklistedDefault(info = "默认规则")
    private String partner_is_blacklisted;

    @JsonIgnore
    private boolean partner_is_blacklistedDirtyFlag;

    /**
     * 属性 [PARTNER_ADDRESS_EMAIL]
     *
     */
    @Crm_leadPartner_address_emailDefault(info = "默认规则")
    private String partner_address_email;

    @JsonIgnore
    private boolean partner_address_emailDirtyFlag;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @Crm_leadStage_id_textDefault(info = "默认规则")
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Crm_leadCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Crm_leadCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [TITLE_TEXT]
     *
     */
    @Crm_leadTitle_textDefault(info = "默认规则")
    private String title_text;

    @JsonIgnore
    private boolean title_textDirtyFlag;

    /**
     * 属性 [LOST_REASON_TEXT]
     *
     */
    @Crm_leadLost_reason_textDefault(info = "默认规则")
    private String lost_reason_text;

    @JsonIgnore
    private boolean lost_reason_textDirtyFlag;

    /**
     * 属性 [LOST_REASON]
     *
     */
    @Crm_leadLost_reasonDefault(info = "默认规则")
    private Integer lost_reason;

    @JsonIgnore
    private boolean lost_reasonDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Crm_leadCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Crm_leadWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Crm_leadCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Crm_leadUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [STATE_ID]
     *
     */
    @Crm_leadState_idDefault(info = "默认规则")
    private Integer state_id;

    @JsonIgnore
    private boolean state_idDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Crm_leadMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @Crm_leadStage_idDefault(info = "默认规则")
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Crm_leadSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Crm_leadCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Crm_leadCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Crm_leadPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Crm_leadTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [TITLE]
     *
     */
    @Crm_leadTitleDefault(info = "默认规则")
    private Integer title;

    @JsonIgnore
    private boolean titleDirtyFlag;


    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [MEETING_COUNT]
     */
    @JsonProperty("meeting_count")
    public Integer getMeeting_count(){
        return meeting_count ;
    }

    /**
     * 设置 [MEETING_COUNT]
     */
    @JsonProperty("meeting_count")
    public void setMeeting_count(Integer  meeting_count){
        this.meeting_count = meeting_count ;
        this.meeting_countDirtyFlag = true ;
    }

    /**
     * 获取 [MEETING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_countDirtyFlag(){
        return meeting_countDirtyFlag ;
    }

    /**
     * 获取 [IBIZFUNCTION]
     */
    @JsonProperty("ibizfunction")
    public String getIbizfunction(){
        return ibizfunction ;
    }

    /**
     * 设置 [IBIZFUNCTION]
     */
    @JsonProperty("ibizfunction")
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.ibizfunctionDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZFUNCTION]脏标记
     */
    @JsonIgnore
    public boolean getIbizfunctionDirtyFlag(){
        return ibizfunctionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return message_bounce ;
    }

    /**
     * 设置 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return message_bounceDirtyFlag ;
    }

    /**
     * 获取 [CITY]
     */
    @JsonProperty("city")
    public String getCity(){
        return city ;
    }

    /**
     * 设置 [CITY]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

    /**
     * 获取 [CITY]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return cityDirtyFlag ;
    }

    /**
     * 获取 [DAY_CLOSE]
     */
    @JsonProperty("day_close")
    public Double getDay_close(){
        return day_close ;
    }

    /**
     * 设置 [DAY_CLOSE]
     */
    @JsonProperty("day_close")
    public void setDay_close(Double  day_close){
        this.day_close = day_close ;
        this.day_closeDirtyFlag = true ;
    }

    /**
     * 获取 [DAY_CLOSE]脏标记
     */
    @JsonIgnore
    public boolean getDay_closeDirtyFlag(){
        return day_closeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [EXPECTED_REVENUE]
     */
    @JsonProperty("expected_revenue")
    public Double getExpected_revenue(){
        return expected_revenue ;
    }

    /**
     * 设置 [EXPECTED_REVENUE]
     */
    @JsonProperty("expected_revenue")
    public void setExpected_revenue(Double  expected_revenue){
        this.expected_revenue = expected_revenue ;
        this.expected_revenueDirtyFlag = true ;
    }

    /**
     * 获取 [EXPECTED_REVENUE]脏标记
     */
    @JsonIgnore
    public boolean getExpected_revenueDirtyFlag(){
        return expected_revenueDirtyFlag ;
    }

    /**
     * 获取 [DATE_CLOSED]
     */
    @JsonProperty("date_closed")
    public Timestamp getDate_closed(){
        return date_closed ;
    }

    /**
     * 设置 [DATE_CLOSED]
     */
    @JsonProperty("date_closed")
    public void setDate_closed(Timestamp  date_closed){
        this.date_closed = date_closed ;
        this.date_closedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_CLOSED]脏标记
     */
    @JsonIgnore
    public boolean getDate_closedDirtyFlag(){
        return date_closedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return email_cc ;
    }

    /**
     * 设置 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_CC]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return email_ccDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_NAME]
     */
    @JsonProperty("contact_name")
    public String getContact_name(){
        return contact_name ;
    }

    /**
     * 设置 [CONTACT_NAME]
     */
    @JsonProperty("contact_name")
    public void setContact_name(String  contact_name){
        this.contact_name = contact_name ;
        this.contact_nameDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_NAME]脏标记
     */
    @JsonIgnore
    public boolean getContact_nameDirtyFlag(){
        return contact_nameDirtyFlag ;
    }

    /**
     * 获取 [DATE_LAST_STAGE_UPDATE]
     */
    @JsonProperty("date_last_stage_update")
    public Timestamp getDate_last_stage_update(){
        return date_last_stage_update ;
    }

    /**
     * 设置 [DATE_LAST_STAGE_UPDATE]
     */
    @JsonProperty("date_last_stage_update")
    public void setDate_last_stage_update(Timestamp  date_last_stage_update){
        this.date_last_stage_update = date_last_stage_update ;
        this.date_last_stage_updateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_LAST_STAGE_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean getDate_last_stage_updateDirtyFlag(){
        return date_last_stage_updateDirtyFlag ;
    }

    /**
     * 获取 [PLANNED_REVENUE]
     */
    @JsonProperty("planned_revenue")
    public Double getPlanned_revenue(){
        return planned_revenue ;
    }

    /**
     * 设置 [PLANNED_REVENUE]
     */
    @JsonProperty("planned_revenue")
    public void setPlanned_revenue(Double  planned_revenue){
        this.planned_revenue = planned_revenue ;
        this.planned_revenueDirtyFlag = true ;
    }

    /**
     * 获取 [PLANNED_REVENUE]脏标记
     */
    @JsonIgnore
    public boolean getPlanned_revenueDirtyFlag(){
        return planned_revenueDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return date_deadline ;
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [ZIP]
     */
    @JsonProperty("zip")
    public String getZip(){
        return zip ;
    }

    /**
     * 设置 [ZIP]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

    /**
     * 获取 [ZIP]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return zipDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [MOBILE]
     */
    @JsonProperty("mobile")
    public String getMobile(){
        return mobile ;
    }

    /**
     * 设置 [MOBILE]
     */
    @JsonProperty("mobile")
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.mobileDirtyFlag = true ;
    }

    /**
     * 获取 [MOBILE]脏标记
     */
    @JsonIgnore
    public boolean getMobileDirtyFlag(){
        return mobileDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return website ;
    }

    /**
     * 设置 [WEBSITE]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return websiteDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return email_from ;
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FROM]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return email_fromDirtyFlag ;
    }

    /**
     * 获取 [DATE_CONVERSION]
     */
    @JsonProperty("date_conversion")
    public Timestamp getDate_conversion(){
        return date_conversion ;
    }

    /**
     * 设置 [DATE_CONVERSION]
     */
    @JsonProperty("date_conversion")
    public void setDate_conversion(Timestamp  date_conversion){
        this.date_conversion = date_conversion ;
        this.date_conversionDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_CONVERSION]脏标记
     */
    @JsonIgnore
    public boolean getDate_conversionDirtyFlag(){
        return date_conversionDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return partner_name ;
    }

    /**
     * 设置 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_NAME]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return partner_nameDirtyFlag ;
    }

    /**
     * 获取 [SALE_AMOUNT_TOTAL]
     */
    @JsonProperty("sale_amount_total")
    public Double getSale_amount_total(){
        return sale_amount_total ;
    }

    /**
     * 设置 [SALE_AMOUNT_TOTAL]
     */
    @JsonProperty("sale_amount_total")
    public void setSale_amount_total(Double  sale_amount_total){
        this.sale_amount_total = sale_amount_total ;
        this.sale_amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_AMOUNT_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getSale_amount_totalDirtyFlag(){
        return sale_amount_totalDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public String getKanban_state(){
        return kanban_state ;
    }

    /**
     * 设置 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public void setKanban_state(String  kanban_state){
        this.kanban_state = kanban_state ;
        this.kanban_stateDirtyFlag = true ;
    }

    /**
     * 获取 [KANBAN_STATE]脏标记
     */
    @JsonIgnore
    public boolean getKanban_stateDirtyFlag(){
        return kanban_stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [REFERRED]
     */
    @JsonProperty("referred")
    public String getReferred(){
        return referred ;
    }

    /**
     * 设置 [REFERRED]
     */
    @JsonProperty("referred")
    public void setReferred(String  referred){
        this.referred = referred ;
        this.referredDirtyFlag = true ;
    }

    /**
     * 获取 [REFERRED]脏标记
     */
    @JsonIgnore
    public boolean getReferredDirtyFlag(){
        return referredDirtyFlag ;
    }

    /**
     * 获取 [PROBABILITY]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return probability ;
    }

    /**
     * 设置 [PROBABILITY]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

    /**
     * 获取 [PROBABILITY]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return probabilityDirtyFlag ;
    }

    /**
     * 获取 [DATE_ACTION_LAST]
     */
    @JsonProperty("date_action_last")
    public Timestamp getDate_action_last(){
        return date_action_last ;
    }

    /**
     * 设置 [DATE_ACTION_LAST]
     */
    @JsonProperty("date_action_last")
    public void setDate_action_last(Timestamp  date_action_last){
        this.date_action_last = date_action_last ;
        this.date_action_lastDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_ACTION_LAST]脏标记
     */
    @JsonIgnore
    public boolean getDate_action_lastDirtyFlag(){
        return date_action_lastDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [DATE_OPEN]
     */
    @JsonProperty("date_open")
    public Timestamp getDate_open(){
        return date_open ;
    }

    /**
     * 设置 [DATE_OPEN]
     */
    @JsonProperty("date_open")
    public void setDate_open(Timestamp  date_open){
        this.date_open = date_open ;
        this.date_openDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_OPEN]脏标记
     */
    @JsonIgnore
    public boolean getDate_openDirtyFlag(){
        return date_openDirtyFlag ;
    }

    /**
     * 获取 [PHONE]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return phone ;
    }

    /**
     * 设置 [PHONE]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return phoneDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [DAY_OPEN]
     */
    @JsonProperty("day_open")
    public Double getDay_open(){
        return day_open ;
    }

    /**
     * 设置 [DAY_OPEN]
     */
    @JsonProperty("day_open")
    public void setDay_open(Double  day_open){
        this.day_open = day_open ;
        this.day_openDirtyFlag = true ;
    }

    /**
     * 获取 [DAY_OPEN]脏标记
     */
    @JsonIgnore
    public boolean getDay_openDirtyFlag(){
        return day_openDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [SALE_NUMBER]
     */
    @JsonProperty("sale_number")
    public Integer getSale_number(){
        return sale_number ;
    }

    /**
     * 设置 [SALE_NUMBER]
     */
    @JsonProperty("sale_number")
    public void setSale_number(Integer  sale_number){
        this.sale_number = sale_number ;
        this.sale_numberDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getSale_numberDirtyFlag(){
        return sale_numberDirtyFlag ;
    }

    /**
     * 获取 [STREET2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return street2 ;
    }

    /**
     * 设置 [STREET2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

    /**
     * 获取 [STREET2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return street2DirtyFlag ;
    }

    /**
     * 获取 [ORDER_IDS]
     */
    @JsonProperty("order_ids")
    public String getOrder_ids(){
        return order_ids ;
    }

    /**
     * 设置 [ORDER_IDS]
     */
    @JsonProperty("order_ids")
    public void setOrder_ids(String  order_ids){
        this.order_ids = order_ids ;
        this.order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idsDirtyFlag(){
        return order_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [STREET]
     */
    @JsonProperty("street")
    public String getStreet(){
        return street ;
    }

    /**
     * 设置 [STREET]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

    /**
     * 获取 [STREET]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return streetDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return is_blacklisted ;
    }

    /**
     * 设置 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_BLACKLISTED]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return source_id_text ;
    }

    /**
     * 设置 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return source_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_NAME]
     */
    @JsonProperty("partner_address_name")
    public String getPartner_address_name(){
        return partner_address_name ;
    }

    /**
     * 设置 [PARTNER_ADDRESS_NAME]
     */
    @JsonProperty("partner_address_name")
    public void setPartner_address_name(String  partner_address_name){
        this.partner_address_name = partner_address_name ;
        this.partner_address_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_NAME]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_nameDirtyFlag(){
        return partner_address_nameDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY]
     */
    @JsonProperty("company_currency")
    public Integer getCompany_currency(){
        return company_currency ;
    }

    /**
     * 设置 [COMPANY_CURRENCY]
     */
    @JsonProperty("company_currency")
    public void setCompany_currency(Integer  company_currency){
        this.company_currency = company_currency ;
        this.company_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currencyDirtyFlag(){
        return company_currencyDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_MOBILE]
     */
    @JsonProperty("partner_address_mobile")
    public String getPartner_address_mobile(){
        return partner_address_mobile ;
    }

    /**
     * 设置 [PARTNER_ADDRESS_MOBILE]
     */
    @JsonProperty("partner_address_mobile")
    public void setPartner_address_mobile(String  partner_address_mobile){
        this.partner_address_mobile = partner_address_mobile ;
        this.partner_address_mobileDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_MOBILE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_mobileDirtyFlag(){
        return partner_address_mobileDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [USER_EMAIL]
     */
    @JsonProperty("user_email")
    public String getUser_email(){
        return user_email ;
    }

    /**
     * 设置 [USER_EMAIL]
     */
    @JsonProperty("user_email")
    public void setUser_email(String  user_email){
        this.user_email = user_email ;
        this.user_emailDirtyFlag = true ;
    }

    /**
     * 获取 [USER_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getUser_emailDirtyFlag(){
        return user_emailDirtyFlag ;
    }

    /**
     * 获取 [USER_LOGIN]
     */
    @JsonProperty("user_login")
    public String getUser_login(){
        return user_login ;
    }

    /**
     * 设置 [USER_LOGIN]
     */
    @JsonProperty("user_login")
    public void setUser_login(String  user_login){
        this.user_login = user_login ;
        this.user_loginDirtyFlag = true ;
    }

    /**
     * 获取 [USER_LOGIN]脏标记
     */
    @JsonIgnore
    public boolean getUser_loginDirtyFlag(){
        return user_loginDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_PHONE]
     */
    @JsonProperty("partner_address_phone")
    public String getPartner_address_phone(){
        return partner_address_phone ;
    }

    /**
     * 设置 [PARTNER_ADDRESS_PHONE]
     */
    @JsonProperty("partner_address_phone")
    public void setPartner_address_phone(String  partner_address_phone){
        this.partner_address_phone = partner_address_phone ;
        this.partner_address_phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_phoneDirtyFlag(){
        return partner_address_phoneDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID_TEXT]
     */
    @JsonProperty("state_id_text")
    public String getState_id_text(){
        return state_id_text ;
    }

    /**
     * 设置 [STATE_ID_TEXT]
     */
    @JsonProperty("state_id_text")
    public void setState_id_text(String  state_id_text){
        this.state_id_text = state_id_text ;
        this.state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getState_id_textDirtyFlag(){
        return state_id_textDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return campaign_id_text ;
    }

    /**
     * 设置 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_IS_BLACKLISTED]
     */
    @JsonProperty("partner_is_blacklisted")
    public String getPartner_is_blacklisted(){
        return partner_is_blacklisted ;
    }

    /**
     * 设置 [PARTNER_IS_BLACKLISTED]
     */
    @JsonProperty("partner_is_blacklisted")
    public void setPartner_is_blacklisted(String  partner_is_blacklisted){
        this.partner_is_blacklisted = partner_is_blacklisted ;
        this.partner_is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_IS_BLACKLISTED]脏标记
     */
    @JsonIgnore
    public boolean getPartner_is_blacklistedDirtyFlag(){
        return partner_is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_EMAIL]
     */
    @JsonProperty("partner_address_email")
    public String getPartner_address_email(){
        return partner_address_email ;
    }

    /**
     * 设置 [PARTNER_ADDRESS_EMAIL]
     */
    @JsonProperty("partner_address_email")
    public void setPartner_address_email(String  partner_address_email){
        this.partner_address_email = partner_address_email ;
        this.partner_address_emailDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_emailDirtyFlag(){
        return partner_address_emailDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return stage_id_text ;
    }

    /**
     * 设置 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [TITLE_TEXT]
     */
    @JsonProperty("title_text")
    public String getTitle_text(){
        return title_text ;
    }

    /**
     * 设置 [TITLE_TEXT]
     */
    @JsonProperty("title_text")
    public void setTitle_text(String  title_text){
        this.title_text = title_text ;
        this.title_textDirtyFlag = true ;
    }

    /**
     * 获取 [TITLE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTitle_textDirtyFlag(){
        return title_textDirtyFlag ;
    }

    /**
     * 获取 [LOST_REASON_TEXT]
     */
    @JsonProperty("lost_reason_text")
    public String getLost_reason_text(){
        return lost_reason_text ;
    }

    /**
     * 设置 [LOST_REASON_TEXT]
     */
    @JsonProperty("lost_reason_text")
    public void setLost_reason_text(String  lost_reason_text){
        this.lost_reason_text = lost_reason_text ;
        this.lost_reason_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOST_REASON_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLost_reason_textDirtyFlag(){
        return lost_reason_textDirtyFlag ;
    }

    /**
     * 获取 [LOST_REASON]
     */
    @JsonProperty("lost_reason")
    public Integer getLost_reason(){
        return lost_reason ;
    }

    /**
     * 设置 [LOST_REASON]
     */
    @JsonProperty("lost_reason")
    public void setLost_reason(Integer  lost_reason){
        this.lost_reason = lost_reason ;
        this.lost_reasonDirtyFlag = true ;
    }

    /**
     * 获取 [LOST_REASON]脏标记
     */
    @JsonIgnore
    public boolean getLost_reasonDirtyFlag(){
        return lost_reasonDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return state_id ;
    }

    /**
     * 设置 [STATE_ID]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return state_idDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return stage_id ;
    }

    /**
     * 设置 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return stage_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [TITLE]
     */
    @JsonProperty("title")
    public Integer getTitle(){
        return title ;
    }

    /**
     * 设置 [TITLE]
     */
    @JsonProperty("title")
    public void setTitle(Integer  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

    /**
     * 获取 [TITLE]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return titleDirtyFlag ;
    }



    public Crm_lead toDO() {
        Crm_lead srfdomain = new Crm_lead();
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getMeeting_countDirtyFlag())
            srfdomain.setMeeting_count(meeting_count);
        if(getIbizfunctionDirtyFlag())
            srfdomain.setIbizfunction(ibizfunction);
        if(getMessage_bounceDirtyFlag())
            srfdomain.setMessage_bounce(message_bounce);
        if(getCityDirtyFlag())
            srfdomain.setCity(city);
        if(getDay_closeDirtyFlag())
            srfdomain.setDay_close(day_close);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getExpected_revenueDirtyFlag())
            srfdomain.setExpected_revenue(expected_revenue);
        if(getDate_closedDirtyFlag())
            srfdomain.setDate_closed(date_closed);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getEmail_ccDirtyFlag())
            srfdomain.setEmail_cc(email_cc);
        if(getContact_nameDirtyFlag())
            srfdomain.setContact_name(contact_name);
        if(getDate_last_stage_updateDirtyFlag())
            srfdomain.setDate_last_stage_update(date_last_stage_update);
        if(getPlanned_revenueDirtyFlag())
            srfdomain.setPlanned_revenue(planned_revenue);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getDate_deadlineDirtyFlag())
            srfdomain.setDate_deadline(date_deadline);
        if(getZipDirtyFlag())
            srfdomain.setZip(zip);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getMobileDirtyFlag())
            srfdomain.setMobile(mobile);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getWebsiteDirtyFlag())
            srfdomain.setWebsite(website);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getEmail_fromDirtyFlag())
            srfdomain.setEmail_from(email_from);
        if(getDate_conversionDirtyFlag())
            srfdomain.setDate_conversion(date_conversion);
        if(getPartner_nameDirtyFlag())
            srfdomain.setPartner_name(partner_name);
        if(getSale_amount_totalDirtyFlag())
            srfdomain.setSale_amount_total(sale_amount_total);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getKanban_stateDirtyFlag())
            srfdomain.setKanban_state(kanban_state);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getReferredDirtyFlag())
            srfdomain.setReferred(referred);
        if(getProbabilityDirtyFlag())
            srfdomain.setProbability(probability);
        if(getDate_action_lastDirtyFlag())
            srfdomain.setDate_action_last(date_action_last);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getDate_openDirtyFlag())
            srfdomain.setDate_open(date_open);
        if(getPhoneDirtyFlag())
            srfdomain.setPhone(phone);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getDay_openDirtyFlag())
            srfdomain.setDay_open(day_open);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getSale_numberDirtyFlag())
            srfdomain.setSale_number(sale_number);
        if(getStreet2DirtyFlag())
            srfdomain.setStreet2(street2);
        if(getOrder_idsDirtyFlag())
            srfdomain.setOrder_ids(order_ids);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getStreetDirtyFlag())
            srfdomain.setStreet(street);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getIs_blacklistedDirtyFlag())
            srfdomain.setIs_blacklisted(is_blacklisted);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getSource_id_textDirtyFlag())
            srfdomain.setSource_id_text(source_id_text);
        if(getPartner_address_nameDirtyFlag())
            srfdomain.setPartner_address_name(partner_address_name);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getCompany_currencyDirtyFlag())
            srfdomain.setCompany_currency(company_currency);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getPartner_address_mobileDirtyFlag())
            srfdomain.setPartner_address_mobile(partner_address_mobile);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getUser_emailDirtyFlag())
            srfdomain.setUser_email(user_email);
        if(getUser_loginDirtyFlag())
            srfdomain.setUser_login(user_login);
        if(getPartner_address_phoneDirtyFlag())
            srfdomain.setPartner_address_phone(partner_address_phone);
        if(getState_id_textDirtyFlag())
            srfdomain.setState_id_text(state_id_text);
        if(getCampaign_id_textDirtyFlag())
            srfdomain.setCampaign_id_text(campaign_id_text);
        if(getPartner_is_blacklistedDirtyFlag())
            srfdomain.setPartner_is_blacklisted(partner_is_blacklisted);
        if(getPartner_address_emailDirtyFlag())
            srfdomain.setPartner_address_email(partner_address_email);
        if(getStage_id_textDirtyFlag())
            srfdomain.setStage_id_text(stage_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getTitle_textDirtyFlag())
            srfdomain.setTitle_text(title_text);
        if(getLost_reason_textDirtyFlag())
            srfdomain.setLost_reason_text(lost_reason_text);
        if(getLost_reasonDirtyFlag())
            srfdomain.setLost_reason(lost_reason);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getState_idDirtyFlag())
            srfdomain.setState_id(state_id);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);
        if(getStage_idDirtyFlag())
            srfdomain.setStage_id(stage_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getTitleDirtyFlag())
            srfdomain.setTitle(title);

        return srfdomain;
    }

    public void fromDO(Crm_lead srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getMeeting_countDirtyFlag())
            this.setMeeting_count(srfdomain.getMeeting_count());
        if(srfdomain.getIbizfunctionDirtyFlag())
            this.setIbizfunction(srfdomain.getIbizfunction());
        if(srfdomain.getMessage_bounceDirtyFlag())
            this.setMessage_bounce(srfdomain.getMessage_bounce());
        if(srfdomain.getCityDirtyFlag())
            this.setCity(srfdomain.getCity());
        if(srfdomain.getDay_closeDirtyFlag())
            this.setDay_close(srfdomain.getDay_close());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getExpected_revenueDirtyFlag())
            this.setExpected_revenue(srfdomain.getExpected_revenue());
        if(srfdomain.getDate_closedDirtyFlag())
            this.setDate_closed(srfdomain.getDate_closed());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getEmail_ccDirtyFlag())
            this.setEmail_cc(srfdomain.getEmail_cc());
        if(srfdomain.getContact_nameDirtyFlag())
            this.setContact_name(srfdomain.getContact_name());
        if(srfdomain.getDate_last_stage_updateDirtyFlag())
            this.setDate_last_stage_update(srfdomain.getDate_last_stage_update());
        if(srfdomain.getPlanned_revenueDirtyFlag())
            this.setPlanned_revenue(srfdomain.getPlanned_revenue());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getDate_deadlineDirtyFlag())
            this.setDate_deadline(srfdomain.getDate_deadline());
        if(srfdomain.getZipDirtyFlag())
            this.setZip(srfdomain.getZip());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getMobileDirtyFlag())
            this.setMobile(srfdomain.getMobile());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getWebsiteDirtyFlag())
            this.setWebsite(srfdomain.getWebsite());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getEmail_fromDirtyFlag())
            this.setEmail_from(srfdomain.getEmail_from());
        if(srfdomain.getDate_conversionDirtyFlag())
            this.setDate_conversion(srfdomain.getDate_conversion());
        if(srfdomain.getPartner_nameDirtyFlag())
            this.setPartner_name(srfdomain.getPartner_name());
        if(srfdomain.getSale_amount_totalDirtyFlag())
            this.setSale_amount_total(srfdomain.getSale_amount_total());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getKanban_stateDirtyFlag())
            this.setKanban_state(srfdomain.getKanban_state());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getReferredDirtyFlag())
            this.setReferred(srfdomain.getReferred());
        if(srfdomain.getProbabilityDirtyFlag())
            this.setProbability(srfdomain.getProbability());
        if(srfdomain.getDate_action_lastDirtyFlag())
            this.setDate_action_last(srfdomain.getDate_action_last());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getDate_openDirtyFlag())
            this.setDate_open(srfdomain.getDate_open());
        if(srfdomain.getPhoneDirtyFlag())
            this.setPhone(srfdomain.getPhone());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getDay_openDirtyFlag())
            this.setDay_open(srfdomain.getDay_open());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getSale_numberDirtyFlag())
            this.setSale_number(srfdomain.getSale_number());
        if(srfdomain.getStreet2DirtyFlag())
            this.setStreet2(srfdomain.getStreet2());
        if(srfdomain.getOrder_idsDirtyFlag())
            this.setOrder_ids(srfdomain.getOrder_ids());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getStreetDirtyFlag())
            this.setStreet(srfdomain.getStreet());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getIs_blacklistedDirtyFlag())
            this.setIs_blacklisted(srfdomain.getIs_blacklisted());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getSource_id_textDirtyFlag())
            this.setSource_id_text(srfdomain.getSource_id_text());
        if(srfdomain.getPartner_address_nameDirtyFlag())
            this.setPartner_address_name(srfdomain.getPartner_address_name());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getCompany_currencyDirtyFlag())
            this.setCompany_currency(srfdomain.getCompany_currency());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getPartner_address_mobileDirtyFlag())
            this.setPartner_address_mobile(srfdomain.getPartner_address_mobile());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getUser_emailDirtyFlag())
            this.setUser_email(srfdomain.getUser_email());
        if(srfdomain.getUser_loginDirtyFlag())
            this.setUser_login(srfdomain.getUser_login());
        if(srfdomain.getPartner_address_phoneDirtyFlag())
            this.setPartner_address_phone(srfdomain.getPartner_address_phone());
        if(srfdomain.getState_id_textDirtyFlag())
            this.setState_id_text(srfdomain.getState_id_text());
        if(srfdomain.getCampaign_id_textDirtyFlag())
            this.setCampaign_id_text(srfdomain.getCampaign_id_text());
        if(srfdomain.getPartner_is_blacklistedDirtyFlag())
            this.setPartner_is_blacklisted(srfdomain.getPartner_is_blacklisted());
        if(srfdomain.getPartner_address_emailDirtyFlag())
            this.setPartner_address_email(srfdomain.getPartner_address_email());
        if(srfdomain.getStage_id_textDirtyFlag())
            this.setStage_id_text(srfdomain.getStage_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getTitle_textDirtyFlag())
            this.setTitle_text(srfdomain.getTitle_text());
        if(srfdomain.getLost_reason_textDirtyFlag())
            this.setLost_reason_text(srfdomain.getLost_reason_text());
        if(srfdomain.getLost_reasonDirtyFlag())
            this.setLost_reason(srfdomain.getLost_reason());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getState_idDirtyFlag())
            this.setState_id(srfdomain.getState_id());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());
        if(srfdomain.getStage_idDirtyFlag())
            this.setStage_id(srfdomain.getStage_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getTitleDirtyFlag())
            this.setTitle(srfdomain.getTitle());

    }

    public List<Crm_leadDTO> fromDOPage(List<Crm_lead> poPage)   {
        if(poPage == null)
            return null;
        List<Crm_leadDTO> dtos=new ArrayList<Crm_leadDTO>();
        for(Crm_lead domain : poPage) {
            Crm_leadDTO dto = new Crm_leadDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

