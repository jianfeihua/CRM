package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.r7rt_dyna.valuerule.anno.dynachart.*;
import cn.ibizlab.odoo.core.r7rt_dyna.domain.DynaChart;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[DynaChartDTO]
 */
public class DynaChartDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @DynaChartCreateDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createDate;

    @JsonIgnore
    private boolean createDateDirtyFlag;

    /**
     * 属性 [DYNACHARTNAME]
     *
     */
    @DynaChartDynaChartNameDefault(info = "默认规则")
    private String dynaChartName;

    @JsonIgnore
    private boolean dynaChartNameDirtyFlag;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @DynaChartUpdateDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updateDate;

    @JsonIgnore
    private boolean updateDateDirtyFlag;

    /**
     * 属性 [MODELID]
     *
     */
    @DynaChartModelIdDefault(info = "默认规则")
    private String modelId;

    @JsonIgnore
    private boolean modelIdDirtyFlag;

    /**
     * 属性 [APPID]
     *
     */
    @DynaChartAppIdDefault(info = "默认规则")
    private String appId;

    @JsonIgnore
    private boolean appIdDirtyFlag;

    /**
     * 属性 [MODEL]
     *
     */
    @DynaChartModelDefault(info = "默认规则")
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @DynaChartUpdateManDefault(info = "默认规则")
    private String updateMan;

    @JsonIgnore
    private boolean updateManDirtyFlag;

    /**
     * 属性 [USERID]
     *
     */
    @DynaChartUserIdDefault(info = "默认规则")
    private String userId;

    @JsonIgnore
    private boolean userIdDirtyFlag;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @DynaChartCreateManDefault(info = "默认规则")
    private String createMan;

    @JsonIgnore
    private boolean createManDirtyFlag;

    /**
     * 属性 [DYNACHARTID]
     *
     */
    @DynaChartDynaChartIdDefault(info = "默认规则")
    private String dynaChartId;

    @JsonIgnore
    private boolean dynaChartIdDirtyFlag;


    /**
     * 获取 [CREATEDATE]
     */
    @JsonProperty("createdate")
    public Timestamp getCreateDate(){
        return createDate ;
    }

    /**
     * 设置 [CREATEDATE]
     */
    @JsonProperty("createdate")
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.createDateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getCreateDateDirtyFlag(){
        return createDateDirtyFlag ;
    }

    /**
     * 获取 [DYNACHARTNAME]
     */
    @JsonProperty("dynachartname")
    public String getDynaChartName(){
        return dynaChartName ;
    }

    /**
     * 设置 [DYNACHARTNAME]
     */
    @JsonProperty("dynachartname")
    public void setDynaChartName(String  dynaChartName){
        this.dynaChartName = dynaChartName ;
        this.dynaChartNameDirtyFlag = true ;
    }

    /**
     * 获取 [DYNACHARTNAME]脏标记
     */
    @JsonIgnore
    public boolean getDynaChartNameDirtyFlag(){
        return dynaChartNameDirtyFlag ;
    }

    /**
     * 获取 [UPDATEDATE]
     */
    @JsonProperty("updatedate")
    public Timestamp getUpdateDate(){
        return updateDate ;
    }

    /**
     * 设置 [UPDATEDATE]
     */
    @JsonProperty("updatedate")
    public void setUpdateDate(Timestamp  updateDate){
        this.updateDate = updateDate ;
        this.updateDateDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getUpdateDateDirtyFlag(){
        return updateDateDirtyFlag ;
    }

    /**
     * 获取 [MODELID]
     */
    @JsonProperty("modelid")
    public String getModelId(){
        return modelId ;
    }

    /**
     * 设置 [MODELID]
     */
    @JsonProperty("modelid")
    public void setModelId(String  modelId){
        this.modelId = modelId ;
        this.modelIdDirtyFlag = true ;
    }

    /**
     * 获取 [MODELID]脏标记
     */
    @JsonIgnore
    public boolean getModelIdDirtyFlag(){
        return modelIdDirtyFlag ;
    }

    /**
     * 获取 [APPID]
     */
    @JsonProperty("appid")
    public String getAppId(){
        return appId ;
    }

    /**
     * 设置 [APPID]
     */
    @JsonProperty("appid")
    public void setAppId(String  appId){
        this.appId = appId ;
        this.appIdDirtyFlag = true ;
    }

    /**
     * 获取 [APPID]脏标记
     */
    @JsonIgnore
    public boolean getAppIdDirtyFlag(){
        return appIdDirtyFlag ;
    }

    /**
     * 获取 [MODEL]
     */
    @JsonProperty("model")
    public String getModel(){
        return model ;
    }

    /**
     * 设置 [MODEL]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return modelDirtyFlag ;
    }

    /**
     * 获取 [UPDATEMAN]
     */
    @JsonProperty("updateman")
    public String getUpdateMan(){
        return updateMan ;
    }

    /**
     * 设置 [UPDATEMAN]
     */
    @JsonProperty("updateman")
    public void setUpdateMan(String  updateMan){
        this.updateMan = updateMan ;
        this.updateManDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getUpdateManDirtyFlag(){
        return updateManDirtyFlag ;
    }

    /**
     * 获取 [USERID]
     */
    @JsonProperty("userid")
    public String getUserId(){
        return userId ;
    }

    /**
     * 设置 [USERID]
     */
    @JsonProperty("userid")
    public void setUserId(String  userId){
        this.userId = userId ;
        this.userIdDirtyFlag = true ;
    }

    /**
     * 获取 [USERID]脏标记
     */
    @JsonIgnore
    public boolean getUserIdDirtyFlag(){
        return userIdDirtyFlag ;
    }

    /**
     * 获取 [CREATEMAN]
     */
    @JsonProperty("createman")
    public String getCreateMan(){
        return createMan ;
    }

    /**
     * 设置 [CREATEMAN]
     */
    @JsonProperty("createman")
    public void setCreateMan(String  createMan){
        this.createMan = createMan ;
        this.createManDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getCreateManDirtyFlag(){
        return createManDirtyFlag ;
    }

    /**
     * 获取 [DYNACHARTID]
     */
    @JsonProperty("dynachartid")
    public String getDynaChartId(){
        return dynaChartId ;
    }

    /**
     * 设置 [DYNACHARTID]
     */
    @JsonProperty("dynachartid")
    public void setDynaChartId(String  dynaChartId){
        this.dynaChartId = dynaChartId ;
        this.dynaChartIdDirtyFlag = true ;
    }

    /**
     * 获取 [DYNACHARTID]脏标记
     */
    @JsonIgnore
    public boolean getDynaChartIdDirtyFlag(){
        return dynaChartIdDirtyFlag ;
    }



    public DynaChart toDO() {
        DynaChart srfdomain = new DynaChart();
        if(getCreateDateDirtyFlag())
            srfdomain.setCreateDate(createDate);
        if(getDynaChartNameDirtyFlag())
            srfdomain.setDynaChartName(dynaChartName);
        if(getUpdateDateDirtyFlag())
            srfdomain.setUpdateDate(updateDate);
        if(getModelIdDirtyFlag())
            srfdomain.setModelId(modelId);
        if(getAppIdDirtyFlag())
            srfdomain.setAppId(appId);
        if(getModelDirtyFlag())
            srfdomain.setModel(model);
        if(getUpdateManDirtyFlag())
            srfdomain.setUpdateMan(updateMan);
        if(getUserIdDirtyFlag())
            srfdomain.setUserId(userId);
        if(getCreateManDirtyFlag())
            srfdomain.setCreateMan(createMan);
        if(getDynaChartIdDirtyFlag())
            srfdomain.setDynaChartId(dynaChartId);

        return srfdomain;
    }

    public void fromDO(DynaChart srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreateDateDirtyFlag())
            this.setCreateDate(srfdomain.getCreateDate());
        if(srfdomain.getDynaChartNameDirtyFlag())
            this.setDynaChartName(srfdomain.getDynaChartName());
        if(srfdomain.getUpdateDateDirtyFlag())
            this.setUpdateDate(srfdomain.getUpdateDate());
        if(srfdomain.getModelIdDirtyFlag())
            this.setModelId(srfdomain.getModelId());
        if(srfdomain.getAppIdDirtyFlag())
            this.setAppId(srfdomain.getAppId());
        if(srfdomain.getModelDirtyFlag())
            this.setModel(srfdomain.getModel());
        if(srfdomain.getUpdateManDirtyFlag())
            this.setUpdateMan(srfdomain.getUpdateMan());
        if(srfdomain.getUserIdDirtyFlag())
            this.setUserId(srfdomain.getUserId());
        if(srfdomain.getCreateManDirtyFlag())
            this.setCreateMan(srfdomain.getCreateMan());
        if(srfdomain.getDynaChartIdDirtyFlag())
            this.setDynaChartId(srfdomain.getDynaChartId());

    }

    public List<DynaChartDTO> fromDOPage(List<DynaChart> poPage)   {
        if(poPage == null)
            return null;
        List<DynaChartDTO> dtos=new ArrayList<DynaChartDTO>();
        for(DynaChart domain : poPage) {
            DynaChartDTO dto = new DynaChartDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

