package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Product_price_listDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_price_listService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_price_list" })
@RestController
@RequestMapping("")
public class Product_price_listResource {

    @Autowired
    private IProduct_price_listService product_price_listService;

    public IProduct_price_listService getProduct_price_listService() {
        return this.product_price_listService;
    }

    @ApiOperation(value = "删除数据", tags = {"Product_price_list" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_price_lists/{product_price_list_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_price_list_id") Integer product_price_list_id) {
        Product_price_listDTO product_price_listdto = new Product_price_listDTO();
		Product_price_list domain = new Product_price_list();
		product_price_listdto.setId(product_price_list_id);
		domain.setId(product_price_list_id);
        Boolean rst = product_price_listService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Product_price_list" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_price_lists/{product_price_list_id}")

    public ResponseEntity<Product_price_listDTO> update(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_listDTO product_price_listdto) {
		Product_price_list domain = product_price_listdto.toDO();
        domain.setId(product_price_list_id);
		product_price_listService.update(domain);
		Product_price_listDTO dto = new Product_price_listDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_price_list" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_price_lists/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_price_listDTO> product_price_listdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_price_list" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_price_lists")

    public ResponseEntity<Product_price_listDTO> create(@RequestBody Product_price_listDTO product_price_listdto) {
        Product_price_listDTO dto = new Product_price_listDTO();
        Product_price_list domain = product_price_listdto.toDO();
		product_price_listService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_price_list" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_price_lists/{product_price_list_id}")
    public ResponseEntity<Product_price_listDTO> get(@PathVariable("product_price_list_id") Integer product_price_list_id) {
        Product_price_listDTO dto = new Product_price_listDTO();
        Product_price_list domain = product_price_listService.get(product_price_list_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Product_price_list" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_price_lists/{product_price_list_id}/getdraft")

    public ResponseEntity<Product_price_listDTO> getDraft(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_listDTO product_price_listdto) {
        Product_price_list product_price_list = product_price_listdto.toDO();
    	product_price_list = product_price_listService.getDraft(product_price_list) ;
    	product_price_listdto.fromDO(product_price_list);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_listdto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_price_list" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_price_lists/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_price_listDTO> product_price_listdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_price_list" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_price_lists/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_price_list(@RequestBody List<Product_price_listDTO> product_price_listdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_price_list" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/product_price_lists/fetchdefault")
	public ResponseEntity<Page<Product_price_listDTO>> fetchDefault(Product_price_listSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_price_listDTO> list = new ArrayList<Product_price_listDTO>();
        
        Page<Product_price_list> domains = product_price_listService.searchDefault(context) ;
        for(Product_price_list product_price_list : domains.getContent()){
            Product_price_listDTO dto = new Product_price_listDTO();
            dto.fromDO(product_price_list);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
