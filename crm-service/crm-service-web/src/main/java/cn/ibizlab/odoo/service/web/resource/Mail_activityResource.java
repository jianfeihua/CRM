package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Mail_activityDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activityService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_activity" })
@RestController
@RequestMapping("")
public class Mail_activityResource {

    @Autowired
    private IMail_activityService mail_activityService;

    public IMail_activityService getMail_activityService() {
        return this.mail_activityService;
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Mail_activity" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activities/{mail_activity_id}/getdraft")

    public ResponseEntity<Mail_activityDTO> getDraft(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activityDTO mail_activitydto) {
        Mail_activity mail_activity = mail_activitydto.toDO();
    	mail_activity = mail_activityService.getDraft(mail_activity) ;
    	mail_activitydto.fromDO(mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activitydto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_activity" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activities/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_activity" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activities/{mail_activity_id}")

    public ResponseEntity<Mail_activityDTO> update(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activityDTO mail_activitydto) {
		Mail_activity domain = mail_activitydto.toDO();
        domain.setId(mail_activity_id);
		mail_activityService.update(domain);
		Mail_activityDTO dto = new Mail_activityDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_activity" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activities/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "CheckKey", tags = {"Mail_activity" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activityDTO mail_activitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Save", tags = {"Mail_activity" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities/{mail_activity_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_activityDTO mail_activitydto) {
        Mail_activity mail_activity = mail_activitydto.toDO();
    	Boolean b = mail_activityService.save(mail_activity) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_activity" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activities/{mail_activity_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_id") Integer mail_activity_id) {
        Mail_activityDTO mail_activitydto = new Mail_activityDTO();
		Mail_activity domain = new Mail_activity();
		mail_activitydto.setId(mail_activity_id);
		domain.setId(mail_activity_id);
        Boolean rst = mail_activityService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_activity" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities/createBatch")
    public ResponseEntity<Boolean> createBatchMail_activity(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_activity" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activities/{mail_activity_id}")
    public ResponseEntity<Mail_activityDTO> get(@PathVariable("mail_activity_id") Integer mail_activity_id) {
        Mail_activityDTO dto = new Mail_activityDTO();
        Mail_activity domain = mail_activityService.get(mail_activity_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_activity" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities")

    public ResponseEntity<Mail_activityDTO> create(@RequestBody Mail_activityDTO mail_activitydto) {
        Mail_activityDTO dto = new Mail_activityDTO();
        Mail_activity domain = mail_activitydto.toDO();
		mail_activityService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_activity" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/mail_activities/fetchdefault")
	public ResponseEntity<Page<Mail_activityDTO>> fetchDefault(Mail_activitySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_activityDTO> list = new ArrayList<Mail_activityDTO>();
        
        Page<Mail_activity> domains = mail_activityService.searchDefault(context) ;
        for(Mail_activity mail_activity : domains.getContent()){
            Mail_activityDTO dto = new Mail_activityDTO();
            dto.fromDO(mail_activity);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
