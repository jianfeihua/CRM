package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Crm_activity_reportDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_activity_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_activity_report" })
@RestController
@RequestMapping("")
public class Crm_activity_reportResource {

    @Autowired
    private ICrm_activity_reportService crm_activity_reportService;

    public ICrm_activity_reportService getCrm_activity_reportService() {
        return this.crm_activity_reportService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_activity_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_activity_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_activity_reportDTO> crm_activity_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_activity_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_activity_reports")

    public ResponseEntity<Crm_activity_reportDTO> create(@RequestBody Crm_activity_reportDTO crm_activity_reportdto) {
        Crm_activity_reportDTO dto = new Crm_activity_reportDTO();
        Crm_activity_report domain = crm_activity_reportdto.toDO();
		crm_activity_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_activity_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_activity_reports/{crm_activity_report_id}")

    public ResponseEntity<Crm_activity_reportDTO> update(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_reportDTO crm_activity_reportdto) {
		Crm_activity_report domain = crm_activity_reportdto.toDO();
        domain.setId(crm_activity_report_id);
		crm_activity_reportService.update(domain);
		Crm_activity_reportDTO dto = new Crm_activity_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_activity_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_activity_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_activity_reportDTO> crm_activity_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_activity_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_activity_reports/{crm_activity_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) {
        Crm_activity_reportDTO crm_activity_reportdto = new Crm_activity_reportDTO();
		Crm_activity_report domain = new Crm_activity_report();
		crm_activity_reportdto.setId(crm_activity_report_id);
		domain.setId(crm_activity_report_id);
        Boolean rst = crm_activity_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_activity_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_activity_reports/{crm_activity_report_id}")
    public ResponseEntity<Crm_activity_reportDTO> get(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) {
        Crm_activity_reportDTO dto = new Crm_activity_reportDTO();
        Crm_activity_report domain = crm_activity_reportService.get(crm_activity_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_activity_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_activity_reports/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_activity_report(@RequestBody List<Crm_activity_reportDTO> crm_activity_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Crm_activity_report" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_activity_reports/{crm_activity_report_id}/getdraft")

    public ResponseEntity<Crm_activity_reportDTO> getDraft(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_reportDTO crm_activity_reportdto) {
        Crm_activity_report crm_activity_report = crm_activity_reportdto.toDO();
    	crm_activity_report = crm_activity_reportService.getDraft(crm_activity_report) ;
    	crm_activity_reportdto.fromDO(crm_activity_report);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_reportdto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_activity_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_activity_reports/fetchdefault")
	public ResponseEntity<Page<Crm_activity_reportDTO>> fetchDefault(Crm_activity_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_activity_reportDTO> list = new ArrayList<Crm_activity_reportDTO>();
        
        Page<Crm_activity_report> domains = crm_activity_reportService.searchDefault(context) ;
        for(Crm_activity_report crm_activity_report : domains.getContent()){
            Crm_activity_reportDTO dto = new Crm_activity_reportDTO();
            dto.fromDO(crm_activity_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
