package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Sale_orderDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_orderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;
import cn.ibizlab.odoo.service.web.dto.Sale_order_lineDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_lineService;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_order" })
@RestController
@RequestMapping("")
public class Sale_orderResource {

    @Autowired
    private ISale_orderService sale_orderService;

    public ISale_orderService getSale_orderService() {
        return this.sale_orderService;
    }

    @Autowired
    private ISale_order_lineService sale_order_lineService;

    public ISale_order_lineService getSale_order_lineService() {
        return this.sale_order_lineService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_order" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/sale_orders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_orderDTO> sale_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_order" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/sale_orders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_orderDTO> sale_orderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/sale_orders/{sale_order_id}")

    public ResponseEntity<Sale_orderDTO> update(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_orderDTO sale_orderdto) {
		Sale_order domain = sale_orderdto.toDO();
        domain.setId(sale_order_id);
		sale_orderService.update(domain);
		Sale_orderDTO dto = new Sale_orderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/sale_orders/{sale_order_id}")
    public ResponseEntity<Sale_orderDTO> get(@PathVariable("sale_order_id") Integer sale_order_id) {
        Sale_orderDTO dto = new Sale_orderDTO();
        Sale_order domain = sale_orderService.get(sale_order_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Sale_order" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders/{sale_order_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_orderDTO sale_orderdto) {
        Sale_order sale_order = sale_orderdto.toDO();
    	Boolean b = sale_orderService.save(sale_order) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "CheckKey", tags = {"Sale_order" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_orderDTO sale_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/sale_orders/{sale_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_id") Integer sale_order_id) {
        Sale_orderDTO sale_orderdto = new Sale_orderDTO();
		Sale_order domain = new Sale_order();
		sale_orderdto.setId(sale_order_id);
		domain.setId(sale_order_id);
        Boolean rst = sale_orderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Sale_order" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/sale_orders/{sale_order_id}/getdraft")

    public ResponseEntity<Sale_orderDTO> getDraft(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_orderDTO sale_orderdto) {
        Sale_order sale_order = sale_orderdto.toDO();
    	sale_order = sale_orderService.getDraft(sale_order) ;
    	sale_orderdto.fromDO(sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_orderdto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_order" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders/createBatch")
    public ResponseEntity<Boolean> createBatchSale_order(@RequestBody List<Sale_orderDTO> sale_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders")

    public ResponseEntity<Sale_orderDTO> create(@RequestBody Sale_orderDTO sale_orderdto) {
        Sale_orderDTO dto = new Sale_orderDTO();
        Sale_order domain = sale_orderdto.toDO();
		sale_orderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_order" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/sale_orders/fetchdefault")
	public ResponseEntity<Page<Sale_orderDTO>> fetchDefault(Sale_orderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_orderDTO> list = new ArrayList<Sale_orderDTO>();
        
        Page<Sale_order> domains = sale_orderService.searchDefault(context) ;
        for(Sale_order sale_order : domains.getContent()){
            Sale_orderDTO dto = new Sale_orderDTO();
            dto.fromDO(sale_order);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}



    @ApiOperation(value = "批更新数据", tags = {"Sale_order" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/sale_orders/updatebatch")
    public ResponseEntity<Boolean> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Sale_orderDTO> sale_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_order" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/sale_orders/removebatch")
    public ResponseEntity<Boolean> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Sale_orderDTO> sale_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")

    public ResponseEntity<Sale_orderDTO> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_orderDTO sale_orderdto) {
		Sale_order domain = sale_orderdto.toDO();
        domain.setPartner_id(res_partner_id);
        domain.setId(sale_order_id);
		sale_orderService.update(domain);
		Sale_orderDTO dto = new Sale_orderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    public ResponseEntity<Sale_orderDTO> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id) {
        Sale_orderDTO dto = new Sale_orderDTO();
        Sale_order domain = sale_orderService.get(sale_order_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Sale_order" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders/save")
    public ResponseEntity<Boolean> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Sale_orderDTO sale_orderdto) {
        Sale_order domain = sale_orderdto.toDO();
        domain.setPartner_id(res_partner_id);
    	Boolean b = sale_orderService.save(domain) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "CheckKey", tags = {"Sale_order" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Sale_orderDTO sale_orderdto) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")

    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id) {
        Sale_orderDTO sale_orderdto = new Sale_orderDTO();
		Sale_order domain = new Sale_order();
		sale_orderdto.setId(sale_order_id);
		domain.setId(sale_order_id);
        Boolean rst = sale_orderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Sale_order" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_orderid}/getdraft")

    public ResponseEntity<Sale_orderDTO> getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_orderDTO sale_orderdto) {
        Sale_order sale_order = sale_orderdto.toDO();
    	sale_order = sale_orderService.getDraft(sale_order) ;
    	sale_orderdto.fromDO(sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_orderdto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_order" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders/createbatch")
    public ResponseEntity<Boolean> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Sale_orderDTO> sale_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders")

    public ResponseEntity<Sale_orderDTO> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Sale_orderDTO sale_orderdto) {
        Sale_orderDTO dto = new Sale_orderDTO();
        Sale_order domain = sale_orderdto.toDO();
        domain.setPartner_id(res_partner_id);
		sale_orderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_order" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/sale_orders/fetchdefault")
	public ResponseEntity<Page<Sale_orderDTO>> fetchSale_orderDefault(@PathVariable("res_partner_id") Integer res_partner_id,Sale_orderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_orderDTO> list = new ArrayList<Sale_orderDTO>();
        context.setN_partner_id_eq(res_partner_id);
        Page<Sale_order> domains = sale_orderService.searchDefault(context) ;
        for(Sale_order sale_order : domains.getContent()){
            Sale_orderDTO dto = new Sale_orderDTO();
            dto.fromDO(sale_order);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
