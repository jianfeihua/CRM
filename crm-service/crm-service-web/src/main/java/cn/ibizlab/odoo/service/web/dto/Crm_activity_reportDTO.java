package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_activity_report.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Crm_activity_reportDTO]
 */
public class Crm_activity_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Crm_activity_reportActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @Crm_activity_reportProbabilityDefault(info = "默认规则")
    private Double probability;

    @JsonIgnore
    private boolean probabilityDirtyFlag;

    /**
     * 属性 [LEAD_TYPE]
     *
     */
    @Crm_activity_reportLead_typeDefault(info = "默认规则")
    private String lead_type;

    @JsonIgnore
    private boolean lead_typeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Crm_activity_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Crm_activity_reportDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Crm_activity_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Crm_activity_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [SUBJECT]
     *
     */
    @Crm_activity_reportSubjectDefault(info = "默认规则")
    private String subject;

    @JsonIgnore
    private boolean subjectDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Crm_activity_reportCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Crm_activity_reportTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @Crm_activity_reportMail_activity_type_id_textDefault(info = "默认规则")
    private String mail_activity_type_id_text;

    @JsonIgnore
    private boolean mail_activity_type_id_textDirtyFlag;

    /**
     * 属性 [LEAD_ID_TEXT]
     *
     */
    @Crm_activity_reportLead_id_textDefault(info = "默认规则")
    private String lead_id_text;

    @JsonIgnore
    private boolean lead_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Crm_activity_reportUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [SUBTYPE_ID_TEXT]
     *
     */
    @Crm_activity_reportSubtype_id_textDefault(info = "默认规则")
    private String subtype_id_text;

    @JsonIgnore
    private boolean subtype_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Crm_activity_reportCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Crm_activity_reportPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @Crm_activity_reportStage_id_textDefault(info = "默认规则")
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;

    /**
     * 属性 [AUTHOR_ID_TEXT]
     *
     */
    @Crm_activity_reportAuthor_id_textDefault(info = "默认规则")
    private String author_id_text;

    @JsonIgnore
    private boolean author_id_textDirtyFlag;

    /**
     * 属性 [AUTHOR_ID]
     *
     */
    @Crm_activity_reportAuthor_idDefault(info = "默认规则")
    private Integer author_id;

    @JsonIgnore
    private boolean author_idDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Crm_activity_reportCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @Crm_activity_reportStage_idDefault(info = "默认规则")
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;

    /**
     * 属性 [SUBTYPE_ID]
     *
     */
    @Crm_activity_reportSubtype_idDefault(info = "默认规则")
    private Integer subtype_id;

    @JsonIgnore
    private boolean subtype_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Crm_activity_reportCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [LEAD_ID]
     *
     */
    @Crm_activity_reportLead_idDefault(info = "默认规则")
    private Integer lead_id;

    @JsonIgnore
    private boolean lead_idDirtyFlag;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID]
     *
     */
    @Crm_activity_reportMail_activity_type_idDefault(info = "默认规则")
    private Integer mail_activity_type_id;

    @JsonIgnore
    private boolean mail_activity_type_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Crm_activity_reportUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Crm_activity_reportPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Crm_activity_reportTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;


    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [PROBABILITY]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return probability ;
    }

    /**
     * 设置 [PROBABILITY]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

    /**
     * 获取 [PROBABILITY]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return probabilityDirtyFlag ;
    }

    /**
     * 获取 [LEAD_TYPE]
     */
    @JsonProperty("lead_type")
    public String getLead_type(){
        return lead_type ;
    }

    /**
     * 设置 [LEAD_TYPE]
     */
    @JsonProperty("lead_type")
    public void setLead_type(String  lead_type){
        this.lead_type = lead_type ;
        this.lead_typeDirtyFlag = true ;
    }

    /**
     * 获取 [LEAD_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getLead_typeDirtyFlag(){
        return lead_typeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [SUBJECT]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return subject ;
    }

    /**
     * 设置 [SUBJECT]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

    /**
     * 获取 [SUBJECT]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return subjectDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("mail_activity_type_id_text")
    public String getMail_activity_type_id_text(){
        return mail_activity_type_id_text ;
    }

    /**
     * 设置 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("mail_activity_type_id_text")
    public void setMail_activity_type_id_text(String  mail_activity_type_id_text){
        this.mail_activity_type_id_text = mail_activity_type_id_text ;
        this.mail_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_id_textDirtyFlag(){
        return mail_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [LEAD_ID_TEXT]
     */
    @JsonProperty("lead_id_text")
    public String getLead_id_text(){
        return lead_id_text ;
    }

    /**
     * 设置 [LEAD_ID_TEXT]
     */
    @JsonProperty("lead_id_text")
    public void setLead_id_text(String  lead_id_text){
        this.lead_id_text = lead_id_text ;
        this.lead_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LEAD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLead_id_textDirtyFlag(){
        return lead_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [SUBTYPE_ID_TEXT]
     */
    @JsonProperty("subtype_id_text")
    public String getSubtype_id_text(){
        return subtype_id_text ;
    }

    /**
     * 设置 [SUBTYPE_ID_TEXT]
     */
    @JsonProperty("subtype_id_text")
    public void setSubtype_id_text(String  subtype_id_text){
        this.subtype_id_text = subtype_id_text ;
        this.subtype_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_id_textDirtyFlag(){
        return subtype_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return stage_id_text ;
    }

    /**
     * 设置 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [AUTHOR_ID_TEXT]
     */
    @JsonProperty("author_id_text")
    public String getAuthor_id_text(){
        return author_id_text ;
    }

    /**
     * 设置 [AUTHOR_ID_TEXT]
     */
    @JsonProperty("author_id_text")
    public void setAuthor_id_text(String  author_id_text){
        this.author_id_text = author_id_text ;
        this.author_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHOR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_id_textDirtyFlag(){
        return author_id_textDirtyFlag ;
    }

    /**
     * 获取 [AUTHOR_ID]
     */
    @JsonProperty("author_id")
    public Integer getAuthor_id(){
        return author_id ;
    }

    /**
     * 设置 [AUTHOR_ID]
     */
    @JsonProperty("author_id")
    public void setAuthor_id(Integer  author_id){
        this.author_id = author_id ;
        this.author_idDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHOR_ID]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_idDirtyFlag(){
        return author_idDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return stage_id ;
    }

    /**
     * 设置 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return stage_idDirtyFlag ;
    }

    /**
     * 获取 [SUBTYPE_ID]
     */
    @JsonProperty("subtype_id")
    public Integer getSubtype_id(){
        return subtype_id ;
    }

    /**
     * 设置 [SUBTYPE_ID]
     */
    @JsonProperty("subtype_id")
    public void setSubtype_id(Integer  subtype_id){
        this.subtype_id = subtype_id ;
        this.subtype_idDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idDirtyFlag(){
        return subtype_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [LEAD_ID]
     */
    @JsonProperty("lead_id")
    public Integer getLead_id(){
        return lead_id ;
    }

    /**
     * 设置 [LEAD_ID]
     */
    @JsonProperty("lead_id")
    public void setLead_id(Integer  lead_id){
        this.lead_id = lead_id ;
        this.lead_idDirtyFlag = true ;
    }

    /**
     * 获取 [LEAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getLead_idDirtyFlag(){
        return lead_idDirtyFlag ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("mail_activity_type_id")
    public Integer getMail_activity_type_id(){
        return mail_activity_type_id ;
    }

    /**
     * 设置 [MAIL_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("mail_activity_type_id")
    public void setMail_activity_type_id(Integer  mail_activity_type_id){
        this.mail_activity_type_id = mail_activity_type_id ;
        this.mail_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_idDirtyFlag(){
        return mail_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }



    public Crm_activity_report toDO() {
        Crm_activity_report srfdomain = new Crm_activity_report();
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getProbabilityDirtyFlag())
            srfdomain.setProbability(probability);
        if(getLead_typeDirtyFlag())
            srfdomain.setLead_type(lead_type);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getSubjectDirtyFlag())
            srfdomain.setSubject(subject);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getMail_activity_type_id_textDirtyFlag())
            srfdomain.setMail_activity_type_id_text(mail_activity_type_id_text);
        if(getLead_id_textDirtyFlag())
            srfdomain.setLead_id_text(lead_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getSubtype_id_textDirtyFlag())
            srfdomain.setSubtype_id_text(subtype_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getStage_id_textDirtyFlag())
            srfdomain.setStage_id_text(stage_id_text);
        if(getAuthor_id_textDirtyFlag())
            srfdomain.setAuthor_id_text(author_id_text);
        if(getAuthor_idDirtyFlag())
            srfdomain.setAuthor_id(author_id);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getStage_idDirtyFlag())
            srfdomain.setStage_id(stage_id);
        if(getSubtype_idDirtyFlag())
            srfdomain.setSubtype_id(subtype_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getLead_idDirtyFlag())
            srfdomain.setLead_id(lead_id);
        if(getMail_activity_type_idDirtyFlag())
            srfdomain.setMail_activity_type_id(mail_activity_type_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);

        return srfdomain;
    }

    public void fromDO(Crm_activity_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getProbabilityDirtyFlag())
            this.setProbability(srfdomain.getProbability());
        if(srfdomain.getLead_typeDirtyFlag())
            this.setLead_type(srfdomain.getLead_type());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getSubjectDirtyFlag())
            this.setSubject(srfdomain.getSubject());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getMail_activity_type_id_textDirtyFlag())
            this.setMail_activity_type_id_text(srfdomain.getMail_activity_type_id_text());
        if(srfdomain.getLead_id_textDirtyFlag())
            this.setLead_id_text(srfdomain.getLead_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getSubtype_id_textDirtyFlag())
            this.setSubtype_id_text(srfdomain.getSubtype_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getStage_id_textDirtyFlag())
            this.setStage_id_text(srfdomain.getStage_id_text());
        if(srfdomain.getAuthor_id_textDirtyFlag())
            this.setAuthor_id_text(srfdomain.getAuthor_id_text());
        if(srfdomain.getAuthor_idDirtyFlag())
            this.setAuthor_id(srfdomain.getAuthor_id());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getStage_idDirtyFlag())
            this.setStage_id(srfdomain.getStage_id());
        if(srfdomain.getSubtype_idDirtyFlag())
            this.setSubtype_id(srfdomain.getSubtype_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getLead_idDirtyFlag())
            this.setLead_id(srfdomain.getLead_id());
        if(srfdomain.getMail_activity_type_idDirtyFlag())
            this.setMail_activity_type_id(srfdomain.getMail_activity_type_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());

    }

    public List<Crm_activity_reportDTO> fromDOPage(List<Crm_activity_report> poPage)   {
        if(poPage == null)
            return null;
        List<Crm_activity_reportDTO> dtos=new ArrayList<Crm_activity_reportDTO>();
        for(Crm_activity_report domain : poPage) {
            Crm_activity_reportDTO dto = new Crm_activity_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

