package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Product_productDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_productService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_product" })
@RestController
@RequestMapping("")
public class Product_productResource {

    @Autowired
    private IProduct_productService product_productService;

    public IProduct_productService getProduct_productService() {
        return this.product_productService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_product" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_products/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_productDTO> product_productdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_product" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_products/{product_product_id}")
    public ResponseEntity<Product_productDTO> get(@PathVariable("product_product_id") Integer product_product_id) {
        Product_productDTO dto = new Product_productDTO();
        Product_product domain = product_productService.get(product_product_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Product_product" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products/{product_product_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_productDTO product_productdto) {
        Product_product product_product = product_productdto.toDO();
    	Boolean b = product_productService.save(product_product) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_product" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products")

    public ResponseEntity<Product_productDTO> create(@RequestBody Product_productDTO product_productdto) {
        Product_productDTO dto = new Product_productDTO();
        Product_product domain = product_productdto.toDO();
		product_productService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Product_product" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_productDTO product_productdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_product" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_products/{product_product_id}")

    public ResponseEntity<Product_productDTO> update(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_productDTO product_productdto) {
		Product_product domain = product_productdto.toDO();
        domain.setId(product_product_id);
		product_productService.update(domain);
		Product_productDTO dto = new Product_productDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_product" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_products/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_productDTO> product_productdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Product_product" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_products/{product_product_id}/getdraft")

    public ResponseEntity<Product_productDTO> getDraft(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_productDTO product_productdto) {
        Product_product product_product = product_productdto.toDO();
    	product_product = product_productService.getDraft(product_product) ;
    	product_productdto.fromDO(product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_productdto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_product" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_products/{product_product_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_product_id") Integer product_product_id) {
        Product_productDTO product_productdto = new Product_productDTO();
		Product_product domain = new Product_product();
		product_productdto.setId(product_product_id);
		domain.setId(product_product_id);
        Boolean rst = product_productService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_product" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_product(@RequestBody List<Product_productDTO> product_productdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_product" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/product_products/fetchdefault")
	public ResponseEntity<Page<Product_productDTO>> fetchDefault(Product_productSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_productDTO> list = new ArrayList<Product_productDTO>();
        
        Page<Product_product> domains = product_productService.searchDefault(context) ;
        for(Product_product product_product : domains.getContent()){
            Product_productDTO dto = new Product_productDTO();
            dto.fromDO(product_product);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
