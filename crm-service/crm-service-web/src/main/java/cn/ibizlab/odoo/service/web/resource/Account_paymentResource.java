package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Account_paymentDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_paymentService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_payment" })
@RestController
@RequestMapping("")
public class Account_paymentResource {

    @Autowired
    private IAccount_paymentService account_paymentService;

    public IAccount_paymentService getAccount_paymentService() {
        return this.account_paymentService;
    }

    @ApiOperation(value = "CheckKey", tags = {"Account_payment" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_paymentDTO account_paymentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_payment" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_payments/{account_payment_id}")

    public ResponseEntity<Account_paymentDTO> update(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_paymentDTO account_paymentdto) {
		Account_payment domain = account_paymentdto.toDO();
        domain.setId(account_payment_id);
		account_paymentService.update(domain);
		Account_paymentDTO dto = new Account_paymentDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Account_payment" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_payments/{account_payment_id}/getdraft")

    public ResponseEntity<Account_paymentDTO> getDraft(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_paymentDTO account_paymentdto) {
        Account_payment account_payment = account_paymentdto.toDO();
    	account_payment = account_paymentService.getDraft(account_payment) ;
    	account_paymentdto.fromDO(account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_paymentdto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_payment" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_payments/{account_payment_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_id") Integer account_payment_id) {
        Account_paymentDTO account_paymentdto = new Account_paymentDTO();
		Account_payment domain = new Account_payment();
		account_paymentdto.setId(account_payment_id);
		domain.setId(account_payment_id);
        Boolean rst = account_paymentService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_payment" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_payments/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_paymentDTO> account_paymentdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Save", tags = {"Account_payment" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments/{account_payment_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_paymentDTO account_paymentdto) {
        Account_payment account_payment = account_paymentdto.toDO();
    	Boolean b = account_paymentService.save(account_payment) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_payment" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_payments/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_paymentDTO> account_paymentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_payment" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments")

    public ResponseEntity<Account_paymentDTO> create(@RequestBody Account_paymentDTO account_paymentdto) {
        Account_paymentDTO dto = new Account_paymentDTO();
        Account_payment domain = account_paymentdto.toDO();
		account_paymentService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_payment" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_payments/{account_payment_id}")
    public ResponseEntity<Account_paymentDTO> get(@PathVariable("account_payment_id") Integer account_payment_id) {
        Account_paymentDTO dto = new Account_paymentDTO();
        Account_payment domain = account_paymentService.get(account_payment_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_payment" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_payment(@RequestBody List<Account_paymentDTO> account_paymentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_payment" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/account_payments/fetchdefault")
	public ResponseEntity<Page<Account_paymentDTO>> fetchDefault(Account_paymentSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_paymentDTO> list = new ArrayList<Account_paymentDTO>();
        
        Page<Account_payment> domains = account_paymentService.searchDefault(context) ;
        for(Account_payment account_payment : domains.getContent()){
            Account_paymentDTO dto = new Account_paymentDTO();
            dto.fromDO(account_payment);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}



    @ApiOperation(value = "CheckKey", tags = {"Account_payment" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_paymentDTO account_paymentdto) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_payment" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}")

    public ResponseEntity<Account_paymentDTO> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_paymentDTO account_paymentdto) {
		Account_payment domain = account_paymentdto.toDO();
        domain.setPartner_id(res_partner_id);
        domain.setId(account_payment_id);
		account_paymentService.update(domain);
		Account_paymentDTO dto = new Account_paymentDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Account_payment" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_payments/{account_paymentid}/getdraft")

    public ResponseEntity<Account_paymentDTO> getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_paymentDTO account_paymentdto) {
        Account_payment account_payment = account_paymentdto.toDO();
    	account_payment = account_paymentService.getDraft(account_payment) ;
    	account_paymentdto.fromDO(account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_paymentdto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_payment" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}")

    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id) {
        Account_paymentDTO account_paymentdto = new Account_paymentDTO();
		Account_payment domain = new Account_payment();
		account_paymentdto.setId(account_payment_id);
		domain.setId(account_payment_id);
        Boolean rst = account_paymentService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_payment" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_payments/removebatch")
    public ResponseEntity<Boolean> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_paymentDTO> account_paymentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Save", tags = {"Account_payment" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments/save")
    public ResponseEntity<Boolean> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_paymentDTO account_paymentdto) {
        Account_payment domain = account_paymentdto.toDO();
        domain.setPartner_id(res_partner_id);
    	Boolean b = account_paymentService.save(domain) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_payment" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_payments/updatebatch")
    public ResponseEntity<Boolean> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_paymentDTO> account_paymentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_payment" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments")

    public ResponseEntity<Account_paymentDTO> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_paymentDTO account_paymentdto) {
        Account_paymentDTO dto = new Account_paymentDTO();
        Account_payment domain = account_paymentdto.toDO();
        domain.setPartner_id(res_partner_id);
		account_paymentService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_payment" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    public ResponseEntity<Account_paymentDTO> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id) {
        Account_paymentDTO dto = new Account_paymentDTO();
        Account_payment domain = account_paymentService.get(account_payment_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_payment" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments/createbatch")
    public ResponseEntity<Boolean> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_paymentDTO> account_paymentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_payment" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/account_payments/fetchdefault")
	public ResponseEntity<Page<Account_paymentDTO>> fetchAccount_paymentDefault(@PathVariable("res_partner_id") Integer res_partner_id,Account_paymentSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_paymentDTO> list = new ArrayList<Account_paymentDTO>();
        context.setN_partner_id_eq(res_partner_id);
        Page<Account_payment> domains = account_paymentService.searchDefault(context) ;
        for(Account_payment account_payment : domains.getContent()){
            Account_paymentDTO dto = new Account_paymentDTO();
            dto.fromDO(account_payment);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
