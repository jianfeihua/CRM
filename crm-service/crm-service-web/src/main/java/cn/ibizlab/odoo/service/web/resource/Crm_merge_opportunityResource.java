package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Crm_merge_opportunityDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_merge_opportunityService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_merge_opportunity" })
@RestController
@RequestMapping("")
public class Crm_merge_opportunityResource {

    @Autowired
    private ICrm_merge_opportunityService crm_merge_opportunityService;

    public ICrm_merge_opportunityService getCrm_merge_opportunityService() {
        return this.crm_merge_opportunityService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_merge_opportunity" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_merge_opportunities/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Crm_merge_opportunity" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}/getdraft")

    public ResponseEntity<Crm_merge_opportunityDTO> getDraft(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
        Crm_merge_opportunity crm_merge_opportunity = crm_merge_opportunitydto.toDO();
    	crm_merge_opportunity = crm_merge_opportunityService.getDraft(crm_merge_opportunity) ;
    	crm_merge_opportunitydto.fromDO(crm_merge_opportunity);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunitydto);
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_merge_opportunity" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}")

    public ResponseEntity<Crm_merge_opportunityDTO> update(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
		Crm_merge_opportunity domain = crm_merge_opportunitydto.toDO();
        domain.setId(crm_merge_opportunity_id);
		crm_merge_opportunityService.update(domain);
		Crm_merge_opportunityDTO dto = new Crm_merge_opportunityDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_merge_opportunity" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_merge_opportunities")

    public ResponseEntity<Crm_merge_opportunityDTO> create(@RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
        Crm_merge_opportunityDTO dto = new Crm_merge_opportunityDTO();
        Crm_merge_opportunity domain = crm_merge_opportunitydto.toDO();
		crm_merge_opportunityService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_merge_opportunity" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_merge_opportunities/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_merge_opportunity" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) {
        Crm_merge_opportunityDTO crm_merge_opportunitydto = new Crm_merge_opportunityDTO();
		Crm_merge_opportunity domain = new Crm_merge_opportunity();
		crm_merge_opportunitydto.setId(crm_merge_opportunity_id);
		domain.setId(crm_merge_opportunity_id);
        Boolean rst = crm_merge_opportunityService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_merge_opportunity" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_merge_opportunities/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_merge_opportunity(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_merge_opportunity" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public ResponseEntity<Crm_merge_opportunityDTO> get(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) {
        Crm_merge_opportunityDTO dto = new Crm_merge_opportunityDTO();
        Crm_merge_opportunity domain = crm_merge_opportunityService.get(crm_merge_opportunity_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_merge_opportunity" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_merge_opportunities/fetchdefault")
	public ResponseEntity<Page<Crm_merge_opportunityDTO>> fetchDefault(Crm_merge_opportunitySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_merge_opportunityDTO> list = new ArrayList<Crm_merge_opportunityDTO>();
        
        Page<Crm_merge_opportunity> domains = crm_merge_opportunityService.searchDefault(context) ;
        for(Crm_merge_opportunity crm_merge_opportunity : domains.getContent()){
            Crm_merge_opportunityDTO dto = new Crm_merge_opportunityDTO();
            dto.fromDO(crm_merge_opportunity);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
