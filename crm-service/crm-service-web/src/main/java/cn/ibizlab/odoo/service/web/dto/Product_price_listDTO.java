package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_price_list.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_price_listDTO]
 */
public class Product_price_listDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_price_listDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [QTY5]
     *
     */
    @Product_price_listQty5Default(info = "默认规则")
    private Integer qty5;

    @JsonIgnore
    private boolean qty5DirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_price_list__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [QTY3]
     *
     */
    @Product_price_listQty3Default(info = "默认规则")
    private Integer qty3;

    @JsonIgnore
    private boolean qty3DirtyFlag;

    /**
     * 属性 [QTY4]
     *
     */
    @Product_price_listQty4Default(info = "默认规则")
    private Integer qty4;

    @JsonIgnore
    private boolean qty4DirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_price_listCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [QTY1]
     *
     */
    @Product_price_listQty1Default(info = "默认规则")
    private Integer qty1;

    @JsonIgnore
    private boolean qty1DirtyFlag;

    /**
     * 属性 [QTY2]
     *
     */
    @Product_price_listQty2Default(info = "默认规则")
    private Integer qty2;

    @JsonIgnore
    private boolean qty2DirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_price_listWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_price_listIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_price_listCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_price_listWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRICE_LIST_TEXT]
     *
     */
    @Product_price_listPrice_list_textDefault(info = "默认规则")
    private String price_list_text;

    @JsonIgnore
    private boolean price_list_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_price_listCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRICE_LIST]
     *
     */
    @Product_price_listPrice_listDefault(info = "默认规则")
    private Integer price_list;

    @JsonIgnore
    private boolean price_listDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_price_listWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [QTY5]
     */
    @JsonProperty("qty5")
    public Integer getQty5(){
        return qty5 ;
    }

    /**
     * 设置 [QTY5]
     */
    @JsonProperty("qty5")
    public void setQty5(Integer  qty5){
        this.qty5 = qty5 ;
        this.qty5DirtyFlag = true ;
    }

    /**
     * 获取 [QTY5]脏标记
     */
    @JsonIgnore
    public boolean getQty5DirtyFlag(){
        return qty5DirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [QTY3]
     */
    @JsonProperty("qty3")
    public Integer getQty3(){
        return qty3 ;
    }

    /**
     * 设置 [QTY3]
     */
    @JsonProperty("qty3")
    public void setQty3(Integer  qty3){
        this.qty3 = qty3 ;
        this.qty3DirtyFlag = true ;
    }

    /**
     * 获取 [QTY3]脏标记
     */
    @JsonIgnore
    public boolean getQty3DirtyFlag(){
        return qty3DirtyFlag ;
    }

    /**
     * 获取 [QTY4]
     */
    @JsonProperty("qty4")
    public Integer getQty4(){
        return qty4 ;
    }

    /**
     * 设置 [QTY4]
     */
    @JsonProperty("qty4")
    public void setQty4(Integer  qty4){
        this.qty4 = qty4 ;
        this.qty4DirtyFlag = true ;
    }

    /**
     * 获取 [QTY4]脏标记
     */
    @JsonIgnore
    public boolean getQty4DirtyFlag(){
        return qty4DirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [QTY1]
     */
    @JsonProperty("qty1")
    public Integer getQty1(){
        return qty1 ;
    }

    /**
     * 设置 [QTY1]
     */
    @JsonProperty("qty1")
    public void setQty1(Integer  qty1){
        this.qty1 = qty1 ;
        this.qty1DirtyFlag = true ;
    }

    /**
     * 获取 [QTY1]脏标记
     */
    @JsonIgnore
    public boolean getQty1DirtyFlag(){
        return qty1DirtyFlag ;
    }

    /**
     * 获取 [QTY2]
     */
    @JsonProperty("qty2")
    public Integer getQty2(){
        return qty2 ;
    }

    /**
     * 设置 [QTY2]
     */
    @JsonProperty("qty2")
    public void setQty2(Integer  qty2){
        this.qty2 = qty2 ;
        this.qty2DirtyFlag = true ;
    }

    /**
     * 获取 [QTY2]脏标记
     */
    @JsonIgnore
    public boolean getQty2DirtyFlag(){
        return qty2DirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRICE_LIST_TEXT]
     */
    @JsonProperty("price_list_text")
    public String getPrice_list_text(){
        return price_list_text ;
    }

    /**
     * 设置 [PRICE_LIST_TEXT]
     */
    @JsonProperty("price_list_text")
    public void setPrice_list_text(String  price_list_text){
        this.price_list_text = price_list_text ;
        this.price_list_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_LIST_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_list_textDirtyFlag(){
        return price_list_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRICE_LIST]
     */
    @JsonProperty("price_list")
    public Integer getPrice_list(){
        return price_list ;
    }

    /**
     * 设置 [PRICE_LIST]
     */
    @JsonProperty("price_list")
    public void setPrice_list(Integer  price_list){
        this.price_list = price_list ;
        this.price_listDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_LIST]脏标记
     */
    @JsonIgnore
    public boolean getPrice_listDirtyFlag(){
        return price_listDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Product_price_list toDO() {
        Product_price_list srfdomain = new Product_price_list();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getQty5DirtyFlag())
            srfdomain.setQty5(qty5);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getQty3DirtyFlag())
            srfdomain.setQty3(qty3);
        if(getQty4DirtyFlag())
            srfdomain.setQty4(qty4);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getQty1DirtyFlag())
            srfdomain.setQty1(qty1);
        if(getQty2DirtyFlag())
            srfdomain.setQty2(qty2);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPrice_list_textDirtyFlag())
            srfdomain.setPrice_list_text(price_list_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPrice_listDirtyFlag())
            srfdomain.setPrice_list(price_list);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Product_price_list srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getQty5DirtyFlag())
            this.setQty5(srfdomain.getQty5());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getQty3DirtyFlag())
            this.setQty3(srfdomain.getQty3());
        if(srfdomain.getQty4DirtyFlag())
            this.setQty4(srfdomain.getQty4());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getQty1DirtyFlag())
            this.setQty1(srfdomain.getQty1());
        if(srfdomain.getQty2DirtyFlag())
            this.setQty2(srfdomain.getQty2());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPrice_list_textDirtyFlag())
            this.setPrice_list_text(srfdomain.getPrice_list_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPrice_listDirtyFlag())
            this.setPrice_list(srfdomain.getPrice_list());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Product_price_listDTO> fromDOPage(List<Product_price_list> poPage)   {
        if(poPage == null)
            return null;
        List<Product_price_listDTO> dtos=new ArrayList<Product_price_listDTO>();
        for(Product_price_list domain : poPage) {
            Product_price_listDTO dto = new Product_price_listDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

