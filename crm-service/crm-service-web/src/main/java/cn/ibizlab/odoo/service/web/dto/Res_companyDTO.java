package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_company.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_companyDTO]
 */
public class Res_companyDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [REPORT_HEADER]
     *
     */
    @Res_companyReport_headerDefault(info = "默认规则")
    private String report_header;

    @JsonIgnore
    private boolean report_headerDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Res_companyCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [SALE_QUOTATION_ONBOARDING_STATE]
     *
     */
    @Res_companySale_quotation_onboarding_stateDefault(info = "默认规则")
    private String sale_quotation_onboarding_state;

    @JsonIgnore
    private boolean sale_quotation_onboarding_stateDirtyFlag;

    /**
     * 属性 [QUOTATION_VALIDITY_DAYS]
     *
     */
    @Res_companyQuotation_validity_daysDefault(info = "默认规则")
    private Integer quotation_validity_days;

    @JsonIgnore
    private boolean quotation_validity_daysDirtyFlag;

    /**
     * 属性 [ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE]
     *
     */
    @Res_companyAccount_onboarding_invoice_layout_stateDefault(info = "默认规则")
    private String account_onboarding_invoice_layout_state;

    @JsonIgnore
    private boolean account_onboarding_invoice_layout_stateDirtyFlag;

    /**
     * 属性 [ACCOUNT_NO]
     *
     */
    @Res_companyAccount_noDefault(info = "默认规则")
    private String account_no;

    @JsonIgnore
    private boolean account_noDirtyFlag;

    /**
     * 属性 [BANK_ACCOUNT_CODE_PREFIX]
     *
     */
    @Res_companyBank_account_code_prefixDefault(info = "默认规则")
    private String bank_account_code_prefix;

    @JsonIgnore
    private boolean bank_account_code_prefixDirtyFlag;

    /**
     * 属性 [BANK_JOURNAL_IDS]
     *
     */
    @Res_companyBank_journal_idsDefault(info = "默认规则")
    private String bank_journal_ids;

    @JsonIgnore
    private boolean bank_journal_idsDirtyFlag;

    /**
     * 属性 [ZIP]
     *
     */
    @Res_companyZipDefault(info = "默认规则")
    private String zip;

    @JsonIgnore
    private boolean zipDirtyFlag;

    /**
     * 属性 [PERIOD_LOCK_DATE]
     *
     */
    @Res_companyPeriod_lock_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp period_lock_date;

    @JsonIgnore
    private boolean period_lock_dateDirtyFlag;

    /**
     * 属性 [STATE_ID]
     *
     */
    @Res_companyState_idDefault(info = "默认规则")
    private Integer state_id;

    @JsonIgnore
    private boolean state_idDirtyFlag;

    /**
     * 属性 [TAX_EXIGIBILITY]
     *
     */
    @Res_companyTax_exigibilityDefault(info = "默认规则")
    private String tax_exigibility;

    @JsonIgnore
    private boolean tax_exigibilityDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_IDS]
     *
     */
    @Res_companyResource_calendar_idsDefault(info = "默认规则")
    private String resource_calendar_ids;

    @JsonIgnore
    private boolean resource_calendar_idsDirtyFlag;

    /**
     * 属性 [BANK_IDS]
     *
     */
    @Res_companyBank_idsDefault(info = "默认规则")
    private String bank_ids;

    @JsonIgnore
    private boolean bank_idsDirtyFlag;

    /**
     * 属性 [ACCOUNT_DASHBOARD_ONBOARDING_STATE]
     *
     */
    @Res_companyAccount_dashboard_onboarding_stateDefault(info = "默认规则")
    private String account_dashboard_onboarding_state;

    @JsonIgnore
    private boolean account_dashboard_onboarding_stateDirtyFlag;

    /**
     * 属性 [PROPAGATION_MINIMUM_DELTA]
     *
     */
    @Res_companyPropagation_minimum_deltaDefault(info = "默认规则")
    private Integer propagation_minimum_delta;

    @JsonIgnore
    private boolean propagation_minimum_deltaDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_companyCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SNAILMAIL_COLOR]
     *
     */
    @Res_companySnailmail_colorDefault(info = "默认规则")
    private String snailmail_color;

    @JsonIgnore
    private boolean snailmail_colorDirtyFlag;

    /**
     * 属性 [OVERDUE_MSG]
     *
     */
    @Res_companyOverdue_msgDefault(info = "默认规则")
    private String overdue_msg;

    @JsonIgnore
    private boolean overdue_msgDirtyFlag;

    /**
     * 属性 [CITY]
     *
     */
    @Res_companyCityDefault(info = "默认规则")
    private String city;

    @JsonIgnore
    private boolean cityDirtyFlag;

    /**
     * 属性 [ACCOUNT_SETUP_COA_STATE]
     *
     */
    @Res_companyAccount_setup_coa_stateDefault(info = "默认规则")
    private String account_setup_coa_state;

    @JsonIgnore
    private boolean account_setup_coa_stateDirtyFlag;

    /**
     * 属性 [INVOICE_REFERENCE_TYPE]
     *
     */
    @Res_companyInvoice_reference_typeDefault(info = "默认规则")
    private String invoice_reference_type;

    @JsonIgnore
    private boolean invoice_reference_typeDirtyFlag;

    /**
     * 属性 [CATCHALL]
     *
     */
    @Res_companyCatchallDefault(info = "默认规则")
    private String catchall;

    @JsonIgnore
    private boolean catchallDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_companyDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ANGLO_SAXON_ACCOUNTING]
     *
     */
    @Res_companyAnglo_saxon_accountingDefault(info = "默认规则")
    private String anglo_saxon_accounting;

    @JsonIgnore
    private boolean anglo_saxon_accountingDirtyFlag;

    /**
     * 属性 [SNAILMAIL_DUPLEX]
     *
     */
    @Res_companySnailmail_duplexDefault(info = "默认规则")
    private String snailmail_duplex;

    @JsonIgnore
    private boolean snailmail_duplexDirtyFlag;

    /**
     * 属性 [SOCIAL_GITHUB]
     *
     */
    @Res_companySocial_githubDefault(info = "默认规则")
    private String social_github;

    @JsonIgnore
    private boolean social_githubDirtyFlag;

    /**
     * 属性 [ACCOUNT_SETUP_BANK_DATA_STATE]
     *
     */
    @Res_companyAccount_setup_bank_data_stateDefault(info = "默认规则")
    private String account_setup_bank_data_state;

    @JsonIgnore
    private boolean account_setup_bank_data_stateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_companyIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [STREET2]
     *
     */
    @Res_companyStreet2Default(info = "默认规则")
    private String street2;

    @JsonIgnore
    private boolean street2DirtyFlag;

    /**
     * 属性 [EXPECTS_CHART_OF_ACCOUNTS]
     *
     */
    @Res_companyExpects_chart_of_accountsDefault(info = "默认规则")
    private String expects_chart_of_accounts;

    @JsonIgnore
    private boolean expects_chart_of_accountsDirtyFlag;

    /**
     * 属性 [TRANSFER_ACCOUNT_CODE_PREFIX]
     *
     */
    @Res_companyTransfer_account_code_prefixDefault(info = "默认规则")
    private String transfer_account_code_prefix;

    @JsonIgnore
    private boolean transfer_account_code_prefixDirtyFlag;

    /**
     * 属性 [FISCALYEAR_LAST_DAY]
     *
     */
    @Res_companyFiscalyear_last_dayDefault(info = "默认规则")
    private Integer fiscalyear_last_day;

    @JsonIgnore
    private boolean fiscalyear_last_dayDirtyFlag;

    /**
     * 属性 [USER_IDS]
     *
     */
    @Res_companyUser_idsDefault(info = "默认规则")
    private String user_ids;

    @JsonIgnore
    private boolean user_idsDirtyFlag;

    /**
     * 属性 [ACCOUNT_BANK_RECONCILIATION_START]
     *
     */
    @Res_companyAccount_bank_reconciliation_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp account_bank_reconciliation_start;

    @JsonIgnore
    private boolean account_bank_reconciliation_startDirtyFlag;

    /**
     * 属性 [PORTAL_CONFIRMATION_PAY]
     *
     */
    @Res_companyPortal_confirmation_payDefault(info = "默认规则")
    private String portal_confirmation_pay;

    @JsonIgnore
    private boolean portal_confirmation_payDirtyFlag;

    /**
     * 属性 [QR_CODE]
     *
     */
    @Res_companyQr_codeDefault(info = "默认规则")
    private String qr_code;

    @JsonIgnore
    private boolean qr_codeDirtyFlag;

    /**
     * 属性 [STREET]
     *
     */
    @Res_companyStreetDefault(info = "默认规则")
    private String street;

    @JsonIgnore
    private boolean streetDirtyFlag;

    /**
     * 属性 [ACCOUNT_INVOICE_ONBOARDING_STATE]
     *
     */
    @Res_companyAccount_invoice_onboarding_stateDefault(info = "默认规则")
    private String account_invoice_onboarding_state;

    @JsonIgnore
    private boolean account_invoice_onboarding_stateDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Res_companyChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Res_companySequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [NOMENCLATURE_ID]
     *
     */
    @Res_companyNomenclature_idDefault(info = "默认规则")
    private Integer nomenclature_id;

    @JsonIgnore
    private boolean nomenclature_idDirtyFlag;

    /**
     * 属性 [SOCIAL_GOOGLEPLUS]
     *
     */
    @Res_companySocial_googleplusDefault(info = "默认规则")
    private String social_googleplus;

    @JsonIgnore
    private boolean social_googleplusDirtyFlag;

    /**
     * 属性 [PAYMENT_ACQUIRER_ONBOARDING_STATE]
     *
     */
    @Res_companyPayment_acquirer_onboarding_stateDefault(info = "默认规则")
    private String payment_acquirer_onboarding_state;

    @JsonIgnore
    private boolean payment_acquirer_onboarding_stateDirtyFlag;

    /**
     * 属性 [REPORT_FOOTER]
     *
     */
    @Res_companyReport_footerDefault(info = "默认规则")
    private String report_footer;

    @JsonIgnore
    private boolean report_footerDirtyFlag;

    /**
     * 属性 [PAYMENT_ONBOARDING_PAYMENT_METHOD]
     *
     */
    @Res_companyPayment_onboarding_payment_methodDefault(info = "默认规则")
    private String payment_onboarding_payment_method;

    @JsonIgnore
    private boolean payment_onboarding_payment_methodDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_companyWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [PO_DOUBLE_VALIDATION]
     *
     */
    @Res_companyPo_double_validationDefault(info = "默认规则")
    private String po_double_validation;

    @JsonIgnore
    private boolean po_double_validationDirtyFlag;

    /**
     * 属性 [PO_LEAD]
     *
     */
    @Res_companyPo_leadDefault(info = "默认规则")
    private Double po_lead;

    @JsonIgnore
    private boolean po_leadDirtyFlag;

    /**
     * 属性 [SALE_ONBOARDING_SAMPLE_QUOTATION_STATE]
     *
     */
    @Res_companySale_onboarding_sample_quotation_stateDefault(info = "默认规则")
    private String sale_onboarding_sample_quotation_state;

    @JsonIgnore
    private boolean sale_onboarding_sample_quotation_stateDirtyFlag;

    /**
     * 属性 [SALE_ONBOARDING_ORDER_CONFIRMATION_STATE]
     *
     */
    @Res_companySale_onboarding_order_confirmation_stateDefault(info = "默认规则")
    private String sale_onboarding_order_confirmation_state;

    @JsonIgnore
    private boolean sale_onboarding_order_confirmation_stateDirtyFlag;

    /**
     * 属性 [EXTERNAL_REPORT_LAYOUT_ID]
     *
     */
    @Res_companyExternal_report_layout_idDefault(info = "默认规则")
    private Integer external_report_layout_id;

    @JsonIgnore
    private boolean external_report_layout_idDirtyFlag;

    /**
     * 属性 [SALE_ONBOARDING_PAYMENT_METHOD]
     *
     */
    @Res_companySale_onboarding_payment_methodDefault(info = "默认规则")
    private String sale_onboarding_payment_method;

    @JsonIgnore
    private boolean sale_onboarding_payment_methodDirtyFlag;

    /**
     * 属性 [ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE]
     *
     */
    @Res_companyAccount_onboarding_sample_invoice_stateDefault(info = "默认规则")
    private String account_onboarding_sample_invoice_state;

    @JsonIgnore
    private boolean account_onboarding_sample_invoice_stateDirtyFlag;

    /**
     * 属性 [BASE_ONBOARDING_COMPANY_STATE]
     *
     */
    @Res_companyBase_onboarding_company_stateDefault(info = "默认规则")
    private String base_onboarding_company_state;

    @JsonIgnore
    private boolean base_onboarding_company_stateDirtyFlag;

    /**
     * 属性 [SOCIAL_LINKEDIN]
     *
     */
    @Res_companySocial_linkedinDefault(info = "默认规则")
    private String social_linkedin;

    @JsonIgnore
    private boolean social_linkedinDirtyFlag;

    /**
     * 属性 [MANUFACTURING_LEAD]
     *
     */
    @Res_companyManufacturing_leadDefault(info = "默认规则")
    private Double manufacturing_lead;

    @JsonIgnore
    private boolean manufacturing_leadDirtyFlag;

    /**
     * 属性 [SALE_NOTE]
     *
     */
    @Res_companySale_noteDefault(info = "默认规则")
    private String sale_note;

    @JsonIgnore
    private boolean sale_noteDirtyFlag;

    /**
     * 属性 [PO_DOUBLE_VALIDATION_AMOUNT]
     *
     */
    @Res_companyPo_double_validation_amountDefault(info = "默认规则")
    private Double po_double_validation_amount;

    @JsonIgnore
    private boolean po_double_validation_amountDirtyFlag;

    /**
     * 属性 [PO_LOCK]
     *
     */
    @Res_companyPo_lockDefault(info = "默认规则")
    private String po_lock;

    @JsonIgnore
    private boolean po_lockDirtyFlag;

    /**
     * 属性 [SOCIAL_TWITTER]
     *
     */
    @Res_companySocial_twitterDefault(info = "默认规则")
    private String social_twitter;

    @JsonIgnore
    private boolean social_twitterDirtyFlag;

    /**
     * 属性 [SOCIAL_INSTAGRAM]
     *
     */
    @Res_companySocial_instagramDefault(info = "默认规则")
    private String social_instagram;

    @JsonIgnore
    private boolean social_instagramDirtyFlag;

    /**
     * 属性 [ACCOUNT_SETUP_FY_DATA_STATE]
     *
     */
    @Res_companyAccount_setup_fy_data_stateDefault(info = "默认规则")
    private String account_setup_fy_data_state;

    @JsonIgnore
    private boolean account_setup_fy_data_stateDirtyFlag;

    /**
     * 属性 [TAX_CALCULATION_ROUNDING_METHOD]
     *
     */
    @Res_companyTax_calculation_rounding_methodDefault(info = "默认规则")
    private String tax_calculation_rounding_method;

    @JsonIgnore
    private boolean tax_calculation_rounding_methodDirtyFlag;

    /**
     * 属性 [CASH_ACCOUNT_CODE_PREFIX]
     *
     */
    @Res_companyCash_account_code_prefixDefault(info = "默认规则")
    private String cash_account_code_prefix;

    @JsonIgnore
    private boolean cash_account_code_prefixDirtyFlag;

    /**
     * 属性 [ACCOUNT_ONBOARDING_SALE_TAX_STATE]
     *
     */
    @Res_companyAccount_onboarding_sale_tax_stateDefault(info = "默认规则")
    private String account_onboarding_sale_tax_state;

    @JsonIgnore
    private boolean account_onboarding_sale_tax_stateDirtyFlag;

    /**
     * 属性 [SECURITY_LEAD]
     *
     */
    @Res_companySecurity_leadDefault(info = "默认规则")
    private Double security_lead;

    @JsonIgnore
    private boolean security_leadDirtyFlag;

    /**
     * 属性 [WEBSITE_THEME_ONBOARDING_DONE]
     *
     */
    @Res_companyWebsite_theme_onboarding_doneDefault(info = "默认规则")
    private String website_theme_onboarding_done;

    @JsonIgnore
    private boolean website_theme_onboarding_doneDirtyFlag;

    /**
     * 属性 [INVOICE_IS_PRINT]
     *
     */
    @Res_companyInvoice_is_printDefault(info = "默认规则")
    private String invoice_is_print;

    @JsonIgnore
    private boolean invoice_is_printDirtyFlag;

    /**
     * 属性 [COMPANY_REGISTRY]
     *
     */
    @Res_companyCompany_registryDefault(info = "默认规则")
    private String company_registry;

    @JsonIgnore
    private boolean company_registryDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_company__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [LOGO_WEB]
     *
     */
    @Res_companyLogo_webDefault(info = "默认规则")
    private byte[] logo_web;

    @JsonIgnore
    private boolean logo_webDirtyFlag;

    /**
     * 属性 [FISCALYEAR_LOCK_DATE]
     *
     */
    @Res_companyFiscalyear_lock_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp fiscalyear_lock_date;

    @JsonIgnore
    private boolean fiscalyear_lock_dateDirtyFlag;

    /**
     * 属性 [INVOICE_IS_SNAILMAIL]
     *
     */
    @Res_companyInvoice_is_snailmailDefault(info = "默认规则")
    private String invoice_is_snailmail;

    @JsonIgnore
    private boolean invoice_is_snailmailDirtyFlag;

    /**
     * 属性 [WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE]
     *
     */
    @Res_companyWebsite_sale_onboarding_payment_acquirer_stateDefault(info = "默认规则")
    private String website_sale_onboarding_payment_acquirer_state;

    @JsonIgnore
    private boolean website_sale_onboarding_payment_acquirer_stateDirtyFlag;

    /**
     * 属性 [SOCIAL_FACEBOOK]
     *
     */
    @Res_companySocial_facebookDefault(info = "默认规则")
    private String social_facebook;

    @JsonIgnore
    private boolean social_facebookDirtyFlag;

    /**
     * 属性 [PORTAL_CONFIRMATION_SIGN]
     *
     */
    @Res_companyPortal_confirmation_signDefault(info = "默认规则")
    private String portal_confirmation_sign;

    @JsonIgnore
    private boolean portal_confirmation_signDirtyFlag;

    /**
     * 属性 [PAPERFORMAT_ID]
     *
     */
    @Res_companyPaperformat_idDefault(info = "默认规则")
    private Integer paperformat_id;

    @JsonIgnore
    private boolean paperformat_idDirtyFlag;

    /**
     * 属性 [FISCALYEAR_LAST_MONTH]
     *
     */
    @Res_companyFiscalyear_last_monthDefault(info = "默认规则")
    private String fiscalyear_last_month;

    @JsonIgnore
    private boolean fiscalyear_last_monthDirtyFlag;

    /**
     * 属性 [INVOICE_IS_EMAIL]
     *
     */
    @Res_companyInvoice_is_emailDefault(info = "默认规则")
    private String invoice_is_email;

    @JsonIgnore
    private boolean invoice_is_emailDirtyFlag;

    /**
     * 属性 [SOCIAL_YOUTUBE]
     *
     */
    @Res_companySocial_youtubeDefault(info = "默认规则")
    private String social_youtube;

    @JsonIgnore
    private boolean social_youtubeDirtyFlag;

    /**
     * 属性 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @Res_companyExpense_currency_exchange_account_idDefault(info = "默认规则")
    private Integer expense_currency_exchange_account_id;

    @JsonIgnore
    private boolean expense_currency_exchange_account_idDirtyFlag;

    /**
     * 属性 [PARTNER_GID]
     *
     */
    @Res_companyPartner_gidDefault(info = "默认规则")
    private Integer partner_gid;

    @JsonIgnore
    private boolean partner_gidDirtyFlag;

    /**
     * 属性 [PHONE]
     *
     */
    @Res_companyPhoneDefault(info = "默认规则")
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;

    /**
     * 属性 [LOGO]
     *
     */
    @Res_companyLogoDefault(info = "默认规则")
    private byte[] logo;

    @JsonIgnore
    private boolean logoDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     *
     */
    @Res_companyProperty_stock_account_input_categ_id_textDefault(info = "默认规则")
    private String property_stock_account_input_categ_id_text;

    @JsonIgnore
    private boolean property_stock_account_input_categ_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_PURCHASE_TAX_ID_TEXT]
     *
     */
    @Res_companyAccount_purchase_tax_id_textDefault(info = "默认规则")
    private String account_purchase_tax_id_text;

    @JsonIgnore
    private boolean account_purchase_tax_id_textDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Res_companyNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [INCOTERM_ID_TEXT]
     *
     */
    @Res_companyIncoterm_id_textDefault(info = "默认规则")
    private String incoterm_id_text;

    @JsonIgnore
    private boolean incoterm_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_OPENING_JOURNAL_ID]
     *
     */
    @Res_companyAccount_opening_journal_idDefault(info = "默认规则")
    private Integer account_opening_journal_id;

    @JsonIgnore
    private boolean account_opening_journal_idDirtyFlag;

    /**
     * 属性 [TRANSFER_ACCOUNT_ID_TEXT]
     *
     */
    @Res_companyTransfer_account_id_textDefault(info = "默认规则")
    private String transfer_account_id_text;

    @JsonIgnore
    private boolean transfer_account_id_textDirtyFlag;

    /**
     * 属性 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @Res_companyIncome_currency_exchange_account_idDefault(info = "默认规则")
    private Integer income_currency_exchange_account_id;

    @JsonIgnore
    private boolean income_currency_exchange_account_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Res_companyCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_OPENING_DATE]
     *
     */
    @Res_companyAccount_opening_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp account_opening_date;

    @JsonIgnore
    private boolean account_opening_dateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_companyWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @Res_companyChart_template_id_textDefault(info = "默认规则")
    private String chart_template_id_text;

    @JsonIgnore
    private boolean chart_template_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_SALE_TAX_ID_TEXT]
     *
     */
    @Res_companyAccount_sale_tax_id_textDefault(info = "默认规则")
    private String account_sale_tax_id_text;

    @JsonIgnore
    private boolean account_sale_tax_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_OPENING_MOVE_ID_TEXT]
     *
     */
    @Res_companyAccount_opening_move_id_textDefault(info = "默认规则")
    private String account_opening_move_id_text;

    @JsonIgnore
    private boolean account_opening_move_id_textDirtyFlag;

    /**
     * 属性 [EMAIL]
     *
     */
    @Res_companyEmailDefault(info = "默认规则")
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_companyCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     *
     */
    @Res_companyProperty_stock_account_output_categ_id_textDefault(info = "默认规则")
    private String property_stock_account_output_categ_id_text;

    @JsonIgnore
    private boolean property_stock_account_output_categ_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     *
     */
    @Res_companyProperty_stock_valuation_account_id_textDefault(info = "默认规则")
    private String property_stock_valuation_account_id_text;

    @JsonIgnore
    private boolean property_stock_valuation_account_id_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Res_companyParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [TAX_CASH_BASIS_JOURNAL_ID_TEXT]
     *
     */
    @Res_companyTax_cash_basis_journal_id_textDefault(info = "默认规则")
    private String tax_cash_basis_journal_id_text;

    @JsonIgnore
    private boolean tax_cash_basis_journal_id_textDirtyFlag;

    /**
     * 属性 [INTERNAL_TRANSIT_LOCATION_ID_TEXT]
     *
     */
    @Res_companyInternal_transit_location_id_textDefault(info = "默认规则")
    private String internal_transit_location_id_text;

    @JsonIgnore
    private boolean internal_transit_location_id_textDirtyFlag;

    /**
     * 属性 [WEBSITE]
     *
     */
    @Res_companyWebsiteDefault(info = "默认规则")
    private String website;

    @JsonIgnore
    private boolean websiteDirtyFlag;

    /**
     * 属性 [VAT]
     *
     */
    @Res_companyVatDefault(info = "默认规则")
    private String vat;

    @JsonIgnore
    private boolean vatDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @Res_companyResource_calendar_id_textDefault(info = "默认规则")
    private String resource_calendar_id_text;

    @JsonIgnore
    private boolean resource_calendar_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_EXCHANGE_JOURNAL_ID_TEXT]
     *
     */
    @Res_companyCurrency_exchange_journal_id_textDefault(info = "默认规则")
    private String currency_exchange_journal_id_text;

    @JsonIgnore
    private boolean currency_exchange_journal_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_companyCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Res_companyParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Res_companyCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_companyWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     *
     */
    @Res_companyProperty_stock_account_output_categ_idDefault(info = "默认规则")
    private Integer property_stock_account_output_categ_id;

    @JsonIgnore
    private boolean property_stock_account_output_categ_idDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     *
     */
    @Res_companyProperty_stock_valuation_account_idDefault(info = "默认规则")
    private Integer property_stock_valuation_account_id;

    @JsonIgnore
    private boolean property_stock_valuation_account_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_OPENING_MOVE_ID]
     *
     */
    @Res_companyAccount_opening_move_idDefault(info = "默认规则")
    private Integer account_opening_move_id;

    @JsonIgnore
    private boolean account_opening_move_idDirtyFlag;

    /**
     * 属性 [INTERNAL_TRANSIT_LOCATION_ID]
     *
     */
    @Res_companyInternal_transit_location_idDefault(info = "默认规则")
    private Integer internal_transit_location_id;

    @JsonIgnore
    private boolean internal_transit_location_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_PURCHASE_TAX_ID]
     *
     */
    @Res_companyAccount_purchase_tax_idDefault(info = "默认规则")
    private Integer account_purchase_tax_id;

    @JsonIgnore
    private boolean account_purchase_tax_idDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @Res_companyChart_template_idDefault(info = "默认规则")
    private Integer chart_template_id;

    @JsonIgnore
    private boolean chart_template_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_SALE_TAX_ID]
     *
     */
    @Res_companyAccount_sale_tax_idDefault(info = "默认规则")
    private Integer account_sale_tax_id;

    @JsonIgnore
    private boolean account_sale_tax_idDirtyFlag;

    /**
     * 属性 [TAX_CASH_BASIS_JOURNAL_ID]
     *
     */
    @Res_companyTax_cash_basis_journal_idDefault(info = "默认规则")
    private Integer tax_cash_basis_journal_id;

    @JsonIgnore
    private boolean tax_cash_basis_journal_idDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     *
     */
    @Res_companyProperty_stock_account_input_categ_idDefault(info = "默认规则")
    private Integer property_stock_account_input_categ_id;

    @JsonIgnore
    private boolean property_stock_account_input_categ_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Res_companyPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [INCOTERM_ID]
     *
     */
    @Res_companyIncoterm_idDefault(info = "默认规则")
    private Integer incoterm_id;

    @JsonIgnore
    private boolean incoterm_idDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @Res_companyResource_calendar_idDefault(info = "默认规则")
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;

    /**
     * 属性 [TRANSFER_ACCOUNT_ID]
     *
     */
    @Res_companyTransfer_account_idDefault(info = "默认规则")
    private Integer transfer_account_id;

    @JsonIgnore
    private boolean transfer_account_idDirtyFlag;

    /**
     * 属性 [CURRENCY_EXCHANGE_JOURNAL_ID]
     *
     */
    @Res_companyCurrency_exchange_journal_idDefault(info = "默认规则")
    private Integer currency_exchange_journal_id;

    @JsonIgnore
    private boolean currency_exchange_journal_idDirtyFlag;


    /**
     * 获取 [REPORT_HEADER]
     */
    @JsonProperty("report_header")
    public String getReport_header(){
        return report_header ;
    }

    /**
     * 设置 [REPORT_HEADER]
     */
    @JsonProperty("report_header")
    public void setReport_header(String  report_header){
        this.report_header = report_header ;
        this.report_headerDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_HEADER]脏标记
     */
    @JsonIgnore
    public boolean getReport_headerDirtyFlag(){
        return report_headerDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_QUOTATION_ONBOARDING_STATE]
     */
    @JsonProperty("sale_quotation_onboarding_state")
    public String getSale_quotation_onboarding_state(){
        return sale_quotation_onboarding_state ;
    }

    /**
     * 设置 [SALE_QUOTATION_ONBOARDING_STATE]
     */
    @JsonProperty("sale_quotation_onboarding_state")
    public void setSale_quotation_onboarding_state(String  sale_quotation_onboarding_state){
        this.sale_quotation_onboarding_state = sale_quotation_onboarding_state ;
        this.sale_quotation_onboarding_stateDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_QUOTATION_ONBOARDING_STATE]脏标记
     */
    @JsonIgnore
    public boolean getSale_quotation_onboarding_stateDirtyFlag(){
        return sale_quotation_onboarding_stateDirtyFlag ;
    }

    /**
     * 获取 [QUOTATION_VALIDITY_DAYS]
     */
    @JsonProperty("quotation_validity_days")
    public Integer getQuotation_validity_days(){
        return quotation_validity_days ;
    }

    /**
     * 设置 [QUOTATION_VALIDITY_DAYS]
     */
    @JsonProperty("quotation_validity_days")
    public void setQuotation_validity_days(Integer  quotation_validity_days){
        this.quotation_validity_days = quotation_validity_days ;
        this.quotation_validity_daysDirtyFlag = true ;
    }

    /**
     * 获取 [QUOTATION_VALIDITY_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getQuotation_validity_daysDirtyFlag(){
        return quotation_validity_daysDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE]
     */
    @JsonProperty("account_onboarding_invoice_layout_state")
    public String getAccount_onboarding_invoice_layout_state(){
        return account_onboarding_invoice_layout_state ;
    }

    /**
     * 设置 [ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE]
     */
    @JsonProperty("account_onboarding_invoice_layout_state")
    public void setAccount_onboarding_invoice_layout_state(String  account_onboarding_invoice_layout_state){
        this.account_onboarding_invoice_layout_state = account_onboarding_invoice_layout_state ;
        this.account_onboarding_invoice_layout_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_onboarding_invoice_layout_stateDirtyFlag(){
        return account_onboarding_invoice_layout_stateDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_NO]
     */
    @JsonProperty("account_no")
    public String getAccount_no(){
        return account_no ;
    }

    /**
     * 设置 [ACCOUNT_NO]
     */
    @JsonProperty("account_no")
    public void setAccount_no(String  account_no){
        this.account_no = account_no ;
        this.account_noDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_NO]脏标记
     */
    @JsonIgnore
    public boolean getAccount_noDirtyFlag(){
        return account_noDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("bank_account_code_prefix")
    public String getBank_account_code_prefix(){
        return bank_account_code_prefix ;
    }

    /**
     * 设置 [BANK_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("bank_account_code_prefix")
    public void setBank_account_code_prefix(String  bank_account_code_prefix){
        this.bank_account_code_prefix = bank_account_code_prefix ;
        this.bank_account_code_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACCOUNT_CODE_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_code_prefixDirtyFlag(){
        return bank_account_code_prefixDirtyFlag ;
    }

    /**
     * 获取 [BANK_JOURNAL_IDS]
     */
    @JsonProperty("bank_journal_ids")
    public String getBank_journal_ids(){
        return bank_journal_ids ;
    }

    /**
     * 设置 [BANK_JOURNAL_IDS]
     */
    @JsonProperty("bank_journal_ids")
    public void setBank_journal_ids(String  bank_journal_ids){
        this.bank_journal_ids = bank_journal_ids ;
        this.bank_journal_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_JOURNAL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBank_journal_idsDirtyFlag(){
        return bank_journal_idsDirtyFlag ;
    }

    /**
     * 获取 [ZIP]
     */
    @JsonProperty("zip")
    public String getZip(){
        return zip ;
    }

    /**
     * 设置 [ZIP]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

    /**
     * 获取 [ZIP]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return zipDirtyFlag ;
    }

    /**
     * 获取 [PERIOD_LOCK_DATE]
     */
    @JsonProperty("period_lock_date")
    public Timestamp getPeriod_lock_date(){
        return period_lock_date ;
    }

    /**
     * 设置 [PERIOD_LOCK_DATE]
     */
    @JsonProperty("period_lock_date")
    public void setPeriod_lock_date(Timestamp  period_lock_date){
        this.period_lock_date = period_lock_date ;
        this.period_lock_dateDirtyFlag = true ;
    }

    /**
     * 获取 [PERIOD_LOCK_DATE]脏标记
     */
    @JsonIgnore
    public boolean getPeriod_lock_dateDirtyFlag(){
        return period_lock_dateDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return state_id ;
    }

    /**
     * 设置 [STATE_ID]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return state_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_EXIGIBILITY]
     */
    @JsonProperty("tax_exigibility")
    public String getTax_exigibility(){
        return tax_exigibility ;
    }

    /**
     * 设置 [TAX_EXIGIBILITY]
     */
    @JsonProperty("tax_exigibility")
    public void setTax_exigibility(String  tax_exigibility){
        this.tax_exigibility = tax_exigibility ;
        this.tax_exigibilityDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_EXIGIBILITY]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibilityDirtyFlag(){
        return tax_exigibilityDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_IDS]
     */
    @JsonProperty("resource_calendar_ids")
    public String getResource_calendar_ids(){
        return resource_calendar_ids ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_IDS]
     */
    @JsonProperty("resource_calendar_ids")
    public void setResource_calendar_ids(String  resource_calendar_ids){
        this.resource_calendar_ids = resource_calendar_ids ;
        this.resource_calendar_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_IDS]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idsDirtyFlag(){
        return resource_calendar_idsDirtyFlag ;
    }

    /**
     * 获取 [BANK_IDS]
     */
    @JsonProperty("bank_ids")
    public String getBank_ids(){
        return bank_ids ;
    }

    /**
     * 设置 [BANK_IDS]
     */
    @JsonProperty("bank_ids")
    public void setBank_ids(String  bank_ids){
        this.bank_ids = bank_ids ;
        this.bank_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBank_idsDirtyFlag(){
        return bank_idsDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_DASHBOARD_ONBOARDING_STATE]
     */
    @JsonProperty("account_dashboard_onboarding_state")
    public String getAccount_dashboard_onboarding_state(){
        return account_dashboard_onboarding_state ;
    }

    /**
     * 设置 [ACCOUNT_DASHBOARD_ONBOARDING_STATE]
     */
    @JsonProperty("account_dashboard_onboarding_state")
    public void setAccount_dashboard_onboarding_state(String  account_dashboard_onboarding_state){
        this.account_dashboard_onboarding_state = account_dashboard_onboarding_state ;
        this.account_dashboard_onboarding_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_DASHBOARD_ONBOARDING_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_dashboard_onboarding_stateDirtyFlag(){
        return account_dashboard_onboarding_stateDirtyFlag ;
    }

    /**
     * 获取 [PROPAGATION_MINIMUM_DELTA]
     */
    @JsonProperty("propagation_minimum_delta")
    public Integer getPropagation_minimum_delta(){
        return propagation_minimum_delta ;
    }

    /**
     * 设置 [PROPAGATION_MINIMUM_DELTA]
     */
    @JsonProperty("propagation_minimum_delta")
    public void setPropagation_minimum_delta(Integer  propagation_minimum_delta){
        this.propagation_minimum_delta = propagation_minimum_delta ;
        this.propagation_minimum_deltaDirtyFlag = true ;
    }

    /**
     * 获取 [PROPAGATION_MINIMUM_DELTA]脏标记
     */
    @JsonIgnore
    public boolean getPropagation_minimum_deltaDirtyFlag(){
        return propagation_minimum_deltaDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SNAILMAIL_COLOR]
     */
    @JsonProperty("snailmail_color")
    public String getSnailmail_color(){
        return snailmail_color ;
    }

    /**
     * 设置 [SNAILMAIL_COLOR]
     */
    @JsonProperty("snailmail_color")
    public void setSnailmail_color(String  snailmail_color){
        this.snailmail_color = snailmail_color ;
        this.snailmail_colorDirtyFlag = true ;
    }

    /**
     * 获取 [SNAILMAIL_COLOR]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_colorDirtyFlag(){
        return snailmail_colorDirtyFlag ;
    }

    /**
     * 获取 [OVERDUE_MSG]
     */
    @JsonProperty("overdue_msg")
    public String getOverdue_msg(){
        return overdue_msg ;
    }

    /**
     * 设置 [OVERDUE_MSG]
     */
    @JsonProperty("overdue_msg")
    public void setOverdue_msg(String  overdue_msg){
        this.overdue_msg = overdue_msg ;
        this.overdue_msgDirtyFlag = true ;
    }

    /**
     * 获取 [OVERDUE_MSG]脏标记
     */
    @JsonIgnore
    public boolean getOverdue_msgDirtyFlag(){
        return overdue_msgDirtyFlag ;
    }

    /**
     * 获取 [CITY]
     */
    @JsonProperty("city")
    public String getCity(){
        return city ;
    }

    /**
     * 设置 [CITY]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

    /**
     * 获取 [CITY]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return cityDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_SETUP_COA_STATE]
     */
    @JsonProperty("account_setup_coa_state")
    public String getAccount_setup_coa_state(){
        return account_setup_coa_state ;
    }

    /**
     * 设置 [ACCOUNT_SETUP_COA_STATE]
     */
    @JsonProperty("account_setup_coa_state")
    public void setAccount_setup_coa_state(String  account_setup_coa_state){
        this.account_setup_coa_state = account_setup_coa_state ;
        this.account_setup_coa_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_SETUP_COA_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_setup_coa_stateDirtyFlag(){
        return account_setup_coa_stateDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_REFERENCE_TYPE]
     */
    @JsonProperty("invoice_reference_type")
    public String getInvoice_reference_type(){
        return invoice_reference_type ;
    }

    /**
     * 设置 [INVOICE_REFERENCE_TYPE]
     */
    @JsonProperty("invoice_reference_type")
    public void setInvoice_reference_type(String  invoice_reference_type){
        this.invoice_reference_type = invoice_reference_type ;
        this.invoice_reference_typeDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_REFERENCE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_reference_typeDirtyFlag(){
        return invoice_reference_typeDirtyFlag ;
    }

    /**
     * 获取 [CATCHALL]
     */
    @JsonProperty("catchall")
    public String getCatchall(){
        return catchall ;
    }

    /**
     * 设置 [CATCHALL]
     */
    @JsonProperty("catchall")
    public void setCatchall(String  catchall){
        this.catchall = catchall ;
        this.catchallDirtyFlag = true ;
    }

    /**
     * 获取 [CATCHALL]脏标记
     */
    @JsonIgnore
    public boolean getCatchallDirtyFlag(){
        return catchallDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ANGLO_SAXON_ACCOUNTING]
     */
    @JsonProperty("anglo_saxon_accounting")
    public String getAnglo_saxon_accounting(){
        return anglo_saxon_accounting ;
    }

    /**
     * 设置 [ANGLO_SAXON_ACCOUNTING]
     */
    @JsonProperty("anglo_saxon_accounting")
    public void setAnglo_saxon_accounting(String  anglo_saxon_accounting){
        this.anglo_saxon_accounting = anglo_saxon_accounting ;
        this.anglo_saxon_accountingDirtyFlag = true ;
    }

    /**
     * 获取 [ANGLO_SAXON_ACCOUNTING]脏标记
     */
    @JsonIgnore
    public boolean getAnglo_saxon_accountingDirtyFlag(){
        return anglo_saxon_accountingDirtyFlag ;
    }

    /**
     * 获取 [SNAILMAIL_DUPLEX]
     */
    @JsonProperty("snailmail_duplex")
    public String getSnailmail_duplex(){
        return snailmail_duplex ;
    }

    /**
     * 设置 [SNAILMAIL_DUPLEX]
     */
    @JsonProperty("snailmail_duplex")
    public void setSnailmail_duplex(String  snailmail_duplex){
        this.snailmail_duplex = snailmail_duplex ;
        this.snailmail_duplexDirtyFlag = true ;
    }

    /**
     * 获取 [SNAILMAIL_DUPLEX]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_duplexDirtyFlag(){
        return snailmail_duplexDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_GITHUB]
     */
    @JsonProperty("social_github")
    public String getSocial_github(){
        return social_github ;
    }

    /**
     * 设置 [SOCIAL_GITHUB]
     */
    @JsonProperty("social_github")
    public void setSocial_github(String  social_github){
        this.social_github = social_github ;
        this.social_githubDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_GITHUB]脏标记
     */
    @JsonIgnore
    public boolean getSocial_githubDirtyFlag(){
        return social_githubDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_SETUP_BANK_DATA_STATE]
     */
    @JsonProperty("account_setup_bank_data_state")
    public String getAccount_setup_bank_data_state(){
        return account_setup_bank_data_state ;
    }

    /**
     * 设置 [ACCOUNT_SETUP_BANK_DATA_STATE]
     */
    @JsonProperty("account_setup_bank_data_state")
    public void setAccount_setup_bank_data_state(String  account_setup_bank_data_state){
        this.account_setup_bank_data_state = account_setup_bank_data_state ;
        this.account_setup_bank_data_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_SETUP_BANK_DATA_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_setup_bank_data_stateDirtyFlag(){
        return account_setup_bank_data_stateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [STREET2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return street2 ;
    }

    /**
     * 设置 [STREET2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

    /**
     * 获取 [STREET2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return street2DirtyFlag ;
    }

    /**
     * 获取 [EXPECTS_CHART_OF_ACCOUNTS]
     */
    @JsonProperty("expects_chart_of_accounts")
    public String getExpects_chart_of_accounts(){
        return expects_chart_of_accounts ;
    }

    /**
     * 设置 [EXPECTS_CHART_OF_ACCOUNTS]
     */
    @JsonProperty("expects_chart_of_accounts")
    public void setExpects_chart_of_accounts(String  expects_chart_of_accounts){
        this.expects_chart_of_accounts = expects_chart_of_accounts ;
        this.expects_chart_of_accountsDirtyFlag = true ;
    }

    /**
     * 获取 [EXPECTS_CHART_OF_ACCOUNTS]脏标记
     */
    @JsonIgnore
    public boolean getExpects_chart_of_accountsDirtyFlag(){
        return expects_chart_of_accountsDirtyFlag ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("transfer_account_code_prefix")
    public String getTransfer_account_code_prefix(){
        return transfer_account_code_prefix ;
    }

    /**
     * 设置 [TRANSFER_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("transfer_account_code_prefix")
    public void setTransfer_account_code_prefix(String  transfer_account_code_prefix){
        this.transfer_account_code_prefix = transfer_account_code_prefix ;
        this.transfer_account_code_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_CODE_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_code_prefixDirtyFlag(){
        return transfer_account_code_prefixDirtyFlag ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_DAY]
     */
    @JsonProperty("fiscalyear_last_day")
    public Integer getFiscalyear_last_day(){
        return fiscalyear_last_day ;
    }

    /**
     * 设置 [FISCALYEAR_LAST_DAY]
     */
    @JsonProperty("fiscalyear_last_day")
    public void setFiscalyear_last_day(Integer  fiscalyear_last_day){
        this.fiscalyear_last_day = fiscalyear_last_day ;
        this.fiscalyear_last_dayDirtyFlag = true ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_DAY]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_last_dayDirtyFlag(){
        return fiscalyear_last_dayDirtyFlag ;
    }

    /**
     * 获取 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return user_ids ;
    }

    /**
     * 设置 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return user_idsDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_BANK_RECONCILIATION_START]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public Timestamp getAccount_bank_reconciliation_start(){
        return account_bank_reconciliation_start ;
    }

    /**
     * 设置 [ACCOUNT_BANK_RECONCILIATION_START]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public void setAccount_bank_reconciliation_start(Timestamp  account_bank_reconciliation_start){
        this.account_bank_reconciliation_start = account_bank_reconciliation_start ;
        this.account_bank_reconciliation_startDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_BANK_RECONCILIATION_START]脏标记
     */
    @JsonIgnore
    public boolean getAccount_bank_reconciliation_startDirtyFlag(){
        return account_bank_reconciliation_startDirtyFlag ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_PAY]
     */
    @JsonProperty("portal_confirmation_pay")
    public String getPortal_confirmation_pay(){
        return portal_confirmation_pay ;
    }

    /**
     * 设置 [PORTAL_CONFIRMATION_PAY]
     */
    @JsonProperty("portal_confirmation_pay")
    public void setPortal_confirmation_pay(String  portal_confirmation_pay){
        this.portal_confirmation_pay = portal_confirmation_pay ;
        this.portal_confirmation_payDirtyFlag = true ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_PAY]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_payDirtyFlag(){
        return portal_confirmation_payDirtyFlag ;
    }

    /**
     * 获取 [QR_CODE]
     */
    @JsonProperty("qr_code")
    public String getQr_code(){
        return qr_code ;
    }

    /**
     * 设置 [QR_CODE]
     */
    @JsonProperty("qr_code")
    public void setQr_code(String  qr_code){
        this.qr_code = qr_code ;
        this.qr_codeDirtyFlag = true ;
    }

    /**
     * 获取 [QR_CODE]脏标记
     */
    @JsonIgnore
    public boolean getQr_codeDirtyFlag(){
        return qr_codeDirtyFlag ;
    }

    /**
     * 获取 [STREET]
     */
    @JsonProperty("street")
    public String getStreet(){
        return street ;
    }

    /**
     * 设置 [STREET]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

    /**
     * 获取 [STREET]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return streetDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_INVOICE_ONBOARDING_STATE]
     */
    @JsonProperty("account_invoice_onboarding_state")
    public String getAccount_invoice_onboarding_state(){
        return account_invoice_onboarding_state ;
    }

    /**
     * 设置 [ACCOUNT_INVOICE_ONBOARDING_STATE]
     */
    @JsonProperty("account_invoice_onboarding_state")
    public void setAccount_invoice_onboarding_state(String  account_invoice_onboarding_state){
        this.account_invoice_onboarding_state = account_invoice_onboarding_state ;
        this.account_invoice_onboarding_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_INVOICE_ONBOARDING_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_invoice_onboarding_stateDirtyFlag(){
        return account_invoice_onboarding_stateDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [NOMENCLATURE_ID]
     */
    @JsonProperty("nomenclature_id")
    public Integer getNomenclature_id(){
        return nomenclature_id ;
    }

    /**
     * 设置 [NOMENCLATURE_ID]
     */
    @JsonProperty("nomenclature_id")
    public void setNomenclature_id(Integer  nomenclature_id){
        this.nomenclature_id = nomenclature_id ;
        this.nomenclature_idDirtyFlag = true ;
    }

    /**
     * 获取 [NOMENCLATURE_ID]脏标记
     */
    @JsonIgnore
    public boolean getNomenclature_idDirtyFlag(){
        return nomenclature_idDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_GOOGLEPLUS]
     */
    @JsonProperty("social_googleplus")
    public String getSocial_googleplus(){
        return social_googleplus ;
    }

    /**
     * 设置 [SOCIAL_GOOGLEPLUS]
     */
    @JsonProperty("social_googleplus")
    public void setSocial_googleplus(String  social_googleplus){
        this.social_googleplus = social_googleplus ;
        this.social_googleplusDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_GOOGLEPLUS]脏标记
     */
    @JsonIgnore
    public boolean getSocial_googleplusDirtyFlag(){
        return social_googleplusDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ACQUIRER_ONBOARDING_STATE]
     */
    @JsonProperty("payment_acquirer_onboarding_state")
    public String getPayment_acquirer_onboarding_state(){
        return payment_acquirer_onboarding_state ;
    }

    /**
     * 设置 [PAYMENT_ACQUIRER_ONBOARDING_STATE]
     */
    @JsonProperty("payment_acquirer_onboarding_state")
    public void setPayment_acquirer_onboarding_state(String  payment_acquirer_onboarding_state){
        this.payment_acquirer_onboarding_state = payment_acquirer_onboarding_state ;
        this.payment_acquirer_onboarding_stateDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ACQUIRER_ONBOARDING_STATE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_acquirer_onboarding_stateDirtyFlag(){
        return payment_acquirer_onboarding_stateDirtyFlag ;
    }

    /**
     * 获取 [REPORT_FOOTER]
     */
    @JsonProperty("report_footer")
    public String getReport_footer(){
        return report_footer ;
    }

    /**
     * 设置 [REPORT_FOOTER]
     */
    @JsonProperty("report_footer")
    public void setReport_footer(String  report_footer){
        this.report_footer = report_footer ;
        this.report_footerDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_FOOTER]脏标记
     */
    @JsonIgnore
    public boolean getReport_footerDirtyFlag(){
        return report_footerDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ONBOARDING_PAYMENT_METHOD]
     */
    @JsonProperty("payment_onboarding_payment_method")
    public String getPayment_onboarding_payment_method(){
        return payment_onboarding_payment_method ;
    }

    /**
     * 设置 [PAYMENT_ONBOARDING_PAYMENT_METHOD]
     */
    @JsonProperty("payment_onboarding_payment_method")
    public void setPayment_onboarding_payment_method(String  payment_onboarding_payment_method){
        this.payment_onboarding_payment_method = payment_onboarding_payment_method ;
        this.payment_onboarding_payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ONBOARDING_PAYMENT_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getPayment_onboarding_payment_methodDirtyFlag(){
        return payment_onboarding_payment_methodDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION]
     */
    @JsonProperty("po_double_validation")
    public String getPo_double_validation(){
        return po_double_validation ;
    }

    /**
     * 设置 [PO_DOUBLE_VALIDATION]
     */
    @JsonProperty("po_double_validation")
    public void setPo_double_validation(String  po_double_validation){
        this.po_double_validation = po_double_validation ;
        this.po_double_validationDirtyFlag = true ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validationDirtyFlag(){
        return po_double_validationDirtyFlag ;
    }

    /**
     * 获取 [PO_LEAD]
     */
    @JsonProperty("po_lead")
    public Double getPo_lead(){
        return po_lead ;
    }

    /**
     * 设置 [PO_LEAD]
     */
    @JsonProperty("po_lead")
    public void setPo_lead(Double  po_lead){
        this.po_lead = po_lead ;
        this.po_leadDirtyFlag = true ;
    }

    /**
     * 获取 [PO_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getPo_leadDirtyFlag(){
        return po_leadDirtyFlag ;
    }

    /**
     * 获取 [SALE_ONBOARDING_SAMPLE_QUOTATION_STATE]
     */
    @JsonProperty("sale_onboarding_sample_quotation_state")
    public String getSale_onboarding_sample_quotation_state(){
        return sale_onboarding_sample_quotation_state ;
    }

    /**
     * 设置 [SALE_ONBOARDING_SAMPLE_QUOTATION_STATE]
     */
    @JsonProperty("sale_onboarding_sample_quotation_state")
    public void setSale_onboarding_sample_quotation_state(String  sale_onboarding_sample_quotation_state){
        this.sale_onboarding_sample_quotation_state = sale_onboarding_sample_quotation_state ;
        this.sale_onboarding_sample_quotation_stateDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ONBOARDING_SAMPLE_QUOTATION_STATE]脏标记
     */
    @JsonIgnore
    public boolean getSale_onboarding_sample_quotation_stateDirtyFlag(){
        return sale_onboarding_sample_quotation_stateDirtyFlag ;
    }

    /**
     * 获取 [SALE_ONBOARDING_ORDER_CONFIRMATION_STATE]
     */
    @JsonProperty("sale_onboarding_order_confirmation_state")
    public String getSale_onboarding_order_confirmation_state(){
        return sale_onboarding_order_confirmation_state ;
    }

    /**
     * 设置 [SALE_ONBOARDING_ORDER_CONFIRMATION_STATE]
     */
    @JsonProperty("sale_onboarding_order_confirmation_state")
    public void setSale_onboarding_order_confirmation_state(String  sale_onboarding_order_confirmation_state){
        this.sale_onboarding_order_confirmation_state = sale_onboarding_order_confirmation_state ;
        this.sale_onboarding_order_confirmation_stateDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ONBOARDING_ORDER_CONFIRMATION_STATE]脏标记
     */
    @JsonIgnore
    public boolean getSale_onboarding_order_confirmation_stateDirtyFlag(){
        return sale_onboarding_order_confirmation_stateDirtyFlag ;
    }

    /**
     * 获取 [EXTERNAL_REPORT_LAYOUT_ID]
     */
    @JsonProperty("external_report_layout_id")
    public Integer getExternal_report_layout_id(){
        return external_report_layout_id ;
    }

    /**
     * 设置 [EXTERNAL_REPORT_LAYOUT_ID]
     */
    @JsonProperty("external_report_layout_id")
    public void setExternal_report_layout_id(Integer  external_report_layout_id){
        this.external_report_layout_id = external_report_layout_id ;
        this.external_report_layout_idDirtyFlag = true ;
    }

    /**
     * 获取 [EXTERNAL_REPORT_LAYOUT_ID]脏标记
     */
    @JsonIgnore
    public boolean getExternal_report_layout_idDirtyFlag(){
        return external_report_layout_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ONBOARDING_PAYMENT_METHOD]
     */
    @JsonProperty("sale_onboarding_payment_method")
    public String getSale_onboarding_payment_method(){
        return sale_onboarding_payment_method ;
    }

    /**
     * 设置 [SALE_ONBOARDING_PAYMENT_METHOD]
     */
    @JsonProperty("sale_onboarding_payment_method")
    public void setSale_onboarding_payment_method(String  sale_onboarding_payment_method){
        this.sale_onboarding_payment_method = sale_onboarding_payment_method ;
        this.sale_onboarding_payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ONBOARDING_PAYMENT_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getSale_onboarding_payment_methodDirtyFlag(){
        return sale_onboarding_payment_methodDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE]
     */
    @JsonProperty("account_onboarding_sample_invoice_state")
    public String getAccount_onboarding_sample_invoice_state(){
        return account_onboarding_sample_invoice_state ;
    }

    /**
     * 设置 [ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE]
     */
    @JsonProperty("account_onboarding_sample_invoice_state")
    public void setAccount_onboarding_sample_invoice_state(String  account_onboarding_sample_invoice_state){
        this.account_onboarding_sample_invoice_state = account_onboarding_sample_invoice_state ;
        this.account_onboarding_sample_invoice_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_onboarding_sample_invoice_stateDirtyFlag(){
        return account_onboarding_sample_invoice_stateDirtyFlag ;
    }

    /**
     * 获取 [BASE_ONBOARDING_COMPANY_STATE]
     */
    @JsonProperty("base_onboarding_company_state")
    public String getBase_onboarding_company_state(){
        return base_onboarding_company_state ;
    }

    /**
     * 设置 [BASE_ONBOARDING_COMPANY_STATE]
     */
    @JsonProperty("base_onboarding_company_state")
    public void setBase_onboarding_company_state(String  base_onboarding_company_state){
        this.base_onboarding_company_state = base_onboarding_company_state ;
        this.base_onboarding_company_stateDirtyFlag = true ;
    }

    /**
     * 获取 [BASE_ONBOARDING_COMPANY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getBase_onboarding_company_stateDirtyFlag(){
        return base_onboarding_company_stateDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_LINKEDIN]
     */
    @JsonProperty("social_linkedin")
    public String getSocial_linkedin(){
        return social_linkedin ;
    }

    /**
     * 设置 [SOCIAL_LINKEDIN]
     */
    @JsonProperty("social_linkedin")
    public void setSocial_linkedin(String  social_linkedin){
        this.social_linkedin = social_linkedin ;
        this.social_linkedinDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_LINKEDIN]脏标记
     */
    @JsonIgnore
    public boolean getSocial_linkedinDirtyFlag(){
        return social_linkedinDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURING_LEAD]
     */
    @JsonProperty("manufacturing_lead")
    public Double getManufacturing_lead(){
        return manufacturing_lead ;
    }

    /**
     * 设置 [MANUFACTURING_LEAD]
     */
    @JsonProperty("manufacturing_lead")
    public void setManufacturing_lead(Double  manufacturing_lead){
        this.manufacturing_lead = manufacturing_lead ;
        this.manufacturing_leadDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURING_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getManufacturing_leadDirtyFlag(){
        return manufacturing_leadDirtyFlag ;
    }

    /**
     * 获取 [SALE_NOTE]
     */
    @JsonProperty("sale_note")
    public String getSale_note(){
        return sale_note ;
    }

    /**
     * 设置 [SALE_NOTE]
     */
    @JsonProperty("sale_note")
    public void setSale_note(String  sale_note){
        this.sale_note = sale_note ;
        this.sale_noteDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_NOTE]脏标记
     */
    @JsonIgnore
    public boolean getSale_noteDirtyFlag(){
        return sale_noteDirtyFlag ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION_AMOUNT]
     */
    @JsonProperty("po_double_validation_amount")
    public Double getPo_double_validation_amount(){
        return po_double_validation_amount ;
    }

    /**
     * 设置 [PO_DOUBLE_VALIDATION_AMOUNT]
     */
    @JsonProperty("po_double_validation_amount")
    public void setPo_double_validation_amount(Double  po_double_validation_amount){
        this.po_double_validation_amount = po_double_validation_amount ;
        this.po_double_validation_amountDirtyFlag = true ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validation_amountDirtyFlag(){
        return po_double_validation_amountDirtyFlag ;
    }

    /**
     * 获取 [PO_LOCK]
     */
    @JsonProperty("po_lock")
    public String getPo_lock(){
        return po_lock ;
    }

    /**
     * 设置 [PO_LOCK]
     */
    @JsonProperty("po_lock")
    public void setPo_lock(String  po_lock){
        this.po_lock = po_lock ;
        this.po_lockDirtyFlag = true ;
    }

    /**
     * 获取 [PO_LOCK]脏标记
     */
    @JsonIgnore
    public boolean getPo_lockDirtyFlag(){
        return po_lockDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_TWITTER]
     */
    @JsonProperty("social_twitter")
    public String getSocial_twitter(){
        return social_twitter ;
    }

    /**
     * 设置 [SOCIAL_TWITTER]
     */
    @JsonProperty("social_twitter")
    public void setSocial_twitter(String  social_twitter){
        this.social_twitter = social_twitter ;
        this.social_twitterDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_TWITTER]脏标记
     */
    @JsonIgnore
    public boolean getSocial_twitterDirtyFlag(){
        return social_twitterDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_INSTAGRAM]
     */
    @JsonProperty("social_instagram")
    public String getSocial_instagram(){
        return social_instagram ;
    }

    /**
     * 设置 [SOCIAL_INSTAGRAM]
     */
    @JsonProperty("social_instagram")
    public void setSocial_instagram(String  social_instagram){
        this.social_instagram = social_instagram ;
        this.social_instagramDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_INSTAGRAM]脏标记
     */
    @JsonIgnore
    public boolean getSocial_instagramDirtyFlag(){
        return social_instagramDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_SETUP_FY_DATA_STATE]
     */
    @JsonProperty("account_setup_fy_data_state")
    public String getAccount_setup_fy_data_state(){
        return account_setup_fy_data_state ;
    }

    /**
     * 设置 [ACCOUNT_SETUP_FY_DATA_STATE]
     */
    @JsonProperty("account_setup_fy_data_state")
    public void setAccount_setup_fy_data_state(String  account_setup_fy_data_state){
        this.account_setup_fy_data_state = account_setup_fy_data_state ;
        this.account_setup_fy_data_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_SETUP_FY_DATA_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_setup_fy_data_stateDirtyFlag(){
        return account_setup_fy_data_stateDirtyFlag ;
    }

    /**
     * 获取 [TAX_CALCULATION_ROUNDING_METHOD]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public String getTax_calculation_rounding_method(){
        return tax_calculation_rounding_method ;
    }

    /**
     * 设置 [TAX_CALCULATION_ROUNDING_METHOD]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public void setTax_calculation_rounding_method(String  tax_calculation_rounding_method){
        this.tax_calculation_rounding_method = tax_calculation_rounding_method ;
        this.tax_calculation_rounding_methodDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_CALCULATION_ROUNDING_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getTax_calculation_rounding_methodDirtyFlag(){
        return tax_calculation_rounding_methodDirtyFlag ;
    }

    /**
     * 获取 [CASH_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("cash_account_code_prefix")
    public String getCash_account_code_prefix(){
        return cash_account_code_prefix ;
    }

    /**
     * 设置 [CASH_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("cash_account_code_prefix")
    public void setCash_account_code_prefix(String  cash_account_code_prefix){
        this.cash_account_code_prefix = cash_account_code_prefix ;
        this.cash_account_code_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_ACCOUNT_CODE_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getCash_account_code_prefixDirtyFlag(){
        return cash_account_code_prefixDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ONBOARDING_SALE_TAX_STATE]
     */
    @JsonProperty("account_onboarding_sale_tax_state")
    public String getAccount_onboarding_sale_tax_state(){
        return account_onboarding_sale_tax_state ;
    }

    /**
     * 设置 [ACCOUNT_ONBOARDING_SALE_TAX_STATE]
     */
    @JsonProperty("account_onboarding_sale_tax_state")
    public void setAccount_onboarding_sale_tax_state(String  account_onboarding_sale_tax_state){
        this.account_onboarding_sale_tax_state = account_onboarding_sale_tax_state ;
        this.account_onboarding_sale_tax_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ONBOARDING_SALE_TAX_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_onboarding_sale_tax_stateDirtyFlag(){
        return account_onboarding_sale_tax_stateDirtyFlag ;
    }

    /**
     * 获取 [SECURITY_LEAD]
     */
    @JsonProperty("security_lead")
    public Double getSecurity_lead(){
        return security_lead ;
    }

    /**
     * 设置 [SECURITY_LEAD]
     */
    @JsonProperty("security_lead")
    public void setSecurity_lead(Double  security_lead){
        this.security_lead = security_lead ;
        this.security_leadDirtyFlag = true ;
    }

    /**
     * 获取 [SECURITY_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getSecurity_leadDirtyFlag(){
        return security_leadDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_THEME_ONBOARDING_DONE]
     */
    @JsonProperty("website_theme_onboarding_done")
    public String getWebsite_theme_onboarding_done(){
        return website_theme_onboarding_done ;
    }

    /**
     * 设置 [WEBSITE_THEME_ONBOARDING_DONE]
     */
    @JsonProperty("website_theme_onboarding_done")
    public void setWebsite_theme_onboarding_done(String  website_theme_onboarding_done){
        this.website_theme_onboarding_done = website_theme_onboarding_done ;
        this.website_theme_onboarding_doneDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_THEME_ONBOARDING_DONE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_theme_onboarding_doneDirtyFlag(){
        return website_theme_onboarding_doneDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IS_PRINT]
     */
    @JsonProperty("invoice_is_print")
    public String getInvoice_is_print(){
        return invoice_is_print ;
    }

    /**
     * 设置 [INVOICE_IS_PRINT]
     */
    @JsonProperty("invoice_is_print")
    public void setInvoice_is_print(String  invoice_is_print){
        this.invoice_is_print = invoice_is_print ;
        this.invoice_is_printDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IS_PRINT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_printDirtyFlag(){
        return invoice_is_printDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_REGISTRY]
     */
    @JsonProperty("company_registry")
    public String getCompany_registry(){
        return company_registry ;
    }

    /**
     * 设置 [COMPANY_REGISTRY]
     */
    @JsonProperty("company_registry")
    public void setCompany_registry(String  company_registry){
        this.company_registry = company_registry ;
        this.company_registryDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_REGISTRY]脏标记
     */
    @JsonIgnore
    public boolean getCompany_registryDirtyFlag(){
        return company_registryDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [LOGO_WEB]
     */
    @JsonProperty("logo_web")
    public byte[] getLogo_web(){
        return logo_web ;
    }

    /**
     * 设置 [LOGO_WEB]
     */
    @JsonProperty("logo_web")
    public void setLogo_web(byte[]  logo_web){
        this.logo_web = logo_web ;
        this.logo_webDirtyFlag = true ;
    }

    /**
     * 获取 [LOGO_WEB]脏标记
     */
    @JsonIgnore
    public boolean getLogo_webDirtyFlag(){
        return logo_webDirtyFlag ;
    }

    /**
     * 获取 [FISCALYEAR_LOCK_DATE]
     */
    @JsonProperty("fiscalyear_lock_date")
    public Timestamp getFiscalyear_lock_date(){
        return fiscalyear_lock_date ;
    }

    /**
     * 设置 [FISCALYEAR_LOCK_DATE]
     */
    @JsonProperty("fiscalyear_lock_date")
    public void setFiscalyear_lock_date(Timestamp  fiscalyear_lock_date){
        this.fiscalyear_lock_date = fiscalyear_lock_date ;
        this.fiscalyear_lock_dateDirtyFlag = true ;
    }

    /**
     * 获取 [FISCALYEAR_LOCK_DATE]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_lock_dateDirtyFlag(){
        return fiscalyear_lock_dateDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IS_SNAILMAIL]
     */
    @JsonProperty("invoice_is_snailmail")
    public String getInvoice_is_snailmail(){
        return invoice_is_snailmail ;
    }

    /**
     * 设置 [INVOICE_IS_SNAILMAIL]
     */
    @JsonProperty("invoice_is_snailmail")
    public void setInvoice_is_snailmail(String  invoice_is_snailmail){
        this.invoice_is_snailmail = invoice_is_snailmail ;
        this.invoice_is_snailmailDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IS_SNAILMAIL]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_snailmailDirtyFlag(){
        return invoice_is_snailmailDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE]
     */
    @JsonProperty("website_sale_onboarding_payment_acquirer_state")
    public String getWebsite_sale_onboarding_payment_acquirer_state(){
        return website_sale_onboarding_payment_acquirer_state ;
    }

    /**
     * 设置 [WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE]
     */
    @JsonProperty("website_sale_onboarding_payment_acquirer_state")
    public void setWebsite_sale_onboarding_payment_acquirer_state(String  website_sale_onboarding_payment_acquirer_state){
        this.website_sale_onboarding_payment_acquirer_state = website_sale_onboarding_payment_acquirer_state ;
        this.website_sale_onboarding_payment_acquirer_stateDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_sale_onboarding_payment_acquirer_stateDirtyFlag(){
        return website_sale_onboarding_payment_acquirer_stateDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_FACEBOOK]
     */
    @JsonProperty("social_facebook")
    public String getSocial_facebook(){
        return social_facebook ;
    }

    /**
     * 设置 [SOCIAL_FACEBOOK]
     */
    @JsonProperty("social_facebook")
    public void setSocial_facebook(String  social_facebook){
        this.social_facebook = social_facebook ;
        this.social_facebookDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_FACEBOOK]脏标记
     */
    @JsonIgnore
    public boolean getSocial_facebookDirtyFlag(){
        return social_facebookDirtyFlag ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_SIGN]
     */
    @JsonProperty("portal_confirmation_sign")
    public String getPortal_confirmation_sign(){
        return portal_confirmation_sign ;
    }

    /**
     * 设置 [PORTAL_CONFIRMATION_SIGN]
     */
    @JsonProperty("portal_confirmation_sign")
    public void setPortal_confirmation_sign(String  portal_confirmation_sign){
        this.portal_confirmation_sign = portal_confirmation_sign ;
        this.portal_confirmation_signDirtyFlag = true ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_SIGN]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_signDirtyFlag(){
        return portal_confirmation_signDirtyFlag ;
    }

    /**
     * 获取 [PAPERFORMAT_ID]
     */
    @JsonProperty("paperformat_id")
    public Integer getPaperformat_id(){
        return paperformat_id ;
    }

    /**
     * 设置 [PAPERFORMAT_ID]
     */
    @JsonProperty("paperformat_id")
    public void setPaperformat_id(Integer  paperformat_id){
        this.paperformat_id = paperformat_id ;
        this.paperformat_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAPERFORMAT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPaperformat_idDirtyFlag(){
        return paperformat_idDirtyFlag ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_MONTH]
     */
    @JsonProperty("fiscalyear_last_month")
    public String getFiscalyear_last_month(){
        return fiscalyear_last_month ;
    }

    /**
     * 设置 [FISCALYEAR_LAST_MONTH]
     */
    @JsonProperty("fiscalyear_last_month")
    public void setFiscalyear_last_month(String  fiscalyear_last_month){
        this.fiscalyear_last_month = fiscalyear_last_month ;
        this.fiscalyear_last_monthDirtyFlag = true ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_MONTH]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_last_monthDirtyFlag(){
        return fiscalyear_last_monthDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IS_EMAIL]
     */
    @JsonProperty("invoice_is_email")
    public String getInvoice_is_email(){
        return invoice_is_email ;
    }

    /**
     * 设置 [INVOICE_IS_EMAIL]
     */
    @JsonProperty("invoice_is_email")
    public void setInvoice_is_email(String  invoice_is_email){
        this.invoice_is_email = invoice_is_email ;
        this.invoice_is_emailDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IS_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_emailDirtyFlag(){
        return invoice_is_emailDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_YOUTUBE]
     */
    @JsonProperty("social_youtube")
    public String getSocial_youtube(){
        return social_youtube ;
    }

    /**
     * 设置 [SOCIAL_YOUTUBE]
     */
    @JsonProperty("social_youtube")
    public void setSocial_youtube(String  social_youtube){
        this.social_youtube = social_youtube ;
        this.social_youtubeDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_YOUTUBE]脏标记
     */
    @JsonIgnore
    public boolean getSocial_youtubeDirtyFlag(){
        return social_youtubeDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public Integer getExpense_currency_exchange_account_id(){
        return expense_currency_exchange_account_id ;
    }

    /**
     * 设置 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public void setExpense_currency_exchange_account_id(Integer  expense_currency_exchange_account_id){
        this.expense_currency_exchange_account_id = expense_currency_exchange_account_id ;
        this.expense_currency_exchange_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getExpense_currency_exchange_account_idDirtyFlag(){
        return expense_currency_exchange_account_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_GID]
     */
    @JsonProperty("partner_gid")
    public Integer getPartner_gid(){
        return partner_gid ;
    }

    /**
     * 设置 [PARTNER_GID]
     */
    @JsonProperty("partner_gid")
    public void setPartner_gid(Integer  partner_gid){
        this.partner_gid = partner_gid ;
        this.partner_gidDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_GID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_gidDirtyFlag(){
        return partner_gidDirtyFlag ;
    }

    /**
     * 获取 [PHONE]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return phone ;
    }

    /**
     * 设置 [PHONE]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return phoneDirtyFlag ;
    }

    /**
     * 获取 [LOGO]
     */
    @JsonProperty("logo")
    public byte[] getLogo(){
        return logo ;
    }

    /**
     * 设置 [LOGO]
     */
    @JsonProperty("logo")
    public void setLogo(byte[]  logo){
        this.logo = logo ;
        this.logoDirtyFlag = true ;
    }

    /**
     * 获取 [LOGO]脏标记
     */
    @JsonIgnore
    public boolean getLogoDirtyFlag(){
        return logoDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public String getProperty_stock_account_input_categ_id_text(){
        return property_stock_account_input_categ_id_text ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public void setProperty_stock_account_input_categ_id_text(String  property_stock_account_input_categ_id_text){
        this.property_stock_account_input_categ_id_text = property_stock_account_input_categ_id_text ;
        this.property_stock_account_input_categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_id_textDirtyFlag(){
        return property_stock_account_input_categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_PURCHASE_TAX_ID_TEXT]
     */
    @JsonProperty("account_purchase_tax_id_text")
    public String getAccount_purchase_tax_id_text(){
        return account_purchase_tax_id_text ;
    }

    /**
     * 设置 [ACCOUNT_PURCHASE_TAX_ID_TEXT]
     */
    @JsonProperty("account_purchase_tax_id_text")
    public void setAccount_purchase_tax_id_text(String  account_purchase_tax_id_text){
        this.account_purchase_tax_id_text = account_purchase_tax_id_text ;
        this.account_purchase_tax_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_PURCHASE_TAX_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_purchase_tax_id_textDirtyFlag(){
        return account_purchase_tax_id_textDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [INCOTERM_ID_TEXT]
     */
    @JsonProperty("incoterm_id_text")
    public String getIncoterm_id_text(){
        return incoterm_id_text ;
    }

    /**
     * 设置 [INCOTERM_ID_TEXT]
     */
    @JsonProperty("incoterm_id_text")
    public void setIncoterm_id_text(String  incoterm_id_text){
        this.incoterm_id_text = incoterm_id_text ;
        this.incoterm_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_id_textDirtyFlag(){
        return incoterm_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_JOURNAL_ID]
     */
    @JsonProperty("account_opening_journal_id")
    public Integer getAccount_opening_journal_id(){
        return account_opening_journal_id ;
    }

    /**
     * 设置 [ACCOUNT_OPENING_JOURNAL_ID]
     */
    @JsonProperty("account_opening_journal_id")
    public void setAccount_opening_journal_id(Integer  account_opening_journal_id){
        this.account_opening_journal_id = account_opening_journal_id ;
        this.account_opening_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_journal_idDirtyFlag(){
        return account_opening_journal_idDirtyFlag ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("transfer_account_id_text")
    public String getTransfer_account_id_text(){
        return transfer_account_id_text ;
    }

    /**
     * 设置 [TRANSFER_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("transfer_account_id_text")
    public void setTransfer_account_id_text(String  transfer_account_id_text){
        this.transfer_account_id_text = transfer_account_id_text ;
        this.transfer_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_id_textDirtyFlag(){
        return transfer_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public Integer getIncome_currency_exchange_account_id(){
        return income_currency_exchange_account_id ;
    }

    /**
     * 设置 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public void setIncome_currency_exchange_account_id(Integer  income_currency_exchange_account_id){
        this.income_currency_exchange_account_id = income_currency_exchange_account_id ;
        this.income_currency_exchange_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getIncome_currency_exchange_account_idDirtyFlag(){
        return income_currency_exchange_account_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_DATE]
     */
    @JsonProperty("account_opening_date")
    public Timestamp getAccount_opening_date(){
        return account_opening_date ;
    }

    /**
     * 设置 [ACCOUNT_OPENING_DATE]
     */
    @JsonProperty("account_opening_date")
    public void setAccount_opening_date(Timestamp  account_opening_date){
        this.account_opening_date = account_opening_date ;
        this.account_opening_dateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_DATE]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_dateDirtyFlag(){
        return account_opening_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return chart_template_id_text ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return chart_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_SALE_TAX_ID_TEXT]
     */
    @JsonProperty("account_sale_tax_id_text")
    public String getAccount_sale_tax_id_text(){
        return account_sale_tax_id_text ;
    }

    /**
     * 设置 [ACCOUNT_SALE_TAX_ID_TEXT]
     */
    @JsonProperty("account_sale_tax_id_text")
    public void setAccount_sale_tax_id_text(String  account_sale_tax_id_text){
        this.account_sale_tax_id_text = account_sale_tax_id_text ;
        this.account_sale_tax_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_SALE_TAX_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_sale_tax_id_textDirtyFlag(){
        return account_sale_tax_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_MOVE_ID_TEXT]
     */
    @JsonProperty("account_opening_move_id_text")
    public String getAccount_opening_move_id_text(){
        return account_opening_move_id_text ;
    }

    /**
     * 设置 [ACCOUNT_OPENING_MOVE_ID_TEXT]
     */
    @JsonProperty("account_opening_move_id_text")
    public void setAccount_opening_move_id_text(String  account_opening_move_id_text){
        this.account_opening_move_id_text = account_opening_move_id_text ;
        this.account_opening_move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_move_id_textDirtyFlag(){
        return account_opening_move_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMAIL]
     */
    @JsonProperty("email")
    public String getEmail(){
        return email ;
    }

    /**
     * 设置 [EMAIL]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return emailDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public String getProperty_stock_account_output_categ_id_text(){
        return property_stock_account_output_categ_id_text ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public void setProperty_stock_account_output_categ_id_text(String  property_stock_account_output_categ_id_text){
        this.property_stock_account_output_categ_id_text = property_stock_account_output_categ_id_text ;
        this.property_stock_account_output_categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_id_textDirtyFlag(){
        return property_stock_account_output_categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public String getProperty_stock_valuation_account_id_text(){
        return property_stock_valuation_account_id_text ;
    }

    /**
     * 设置 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public void setProperty_stock_valuation_account_id_text(String  property_stock_valuation_account_id_text){
        this.property_stock_valuation_account_id_text = property_stock_valuation_account_id_text ;
        this.property_stock_valuation_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_id_textDirtyFlag(){
        return property_stock_valuation_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_JOURNAL_ID_TEXT]
     */
    @JsonProperty("tax_cash_basis_journal_id_text")
    public String getTax_cash_basis_journal_id_text(){
        return tax_cash_basis_journal_id_text ;
    }

    /**
     * 设置 [TAX_CASH_BASIS_JOURNAL_ID_TEXT]
     */
    @JsonProperty("tax_cash_basis_journal_id_text")
    public void setTax_cash_basis_journal_id_text(String  tax_cash_basis_journal_id_text){
        this.tax_cash_basis_journal_id_text = tax_cash_basis_journal_id_text ;
        this.tax_cash_basis_journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_journal_id_textDirtyFlag(){
        return tax_cash_basis_journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [INTERNAL_TRANSIT_LOCATION_ID_TEXT]
     */
    @JsonProperty("internal_transit_location_id_text")
    public String getInternal_transit_location_id_text(){
        return internal_transit_location_id_text ;
    }

    /**
     * 设置 [INTERNAL_TRANSIT_LOCATION_ID_TEXT]
     */
    @JsonProperty("internal_transit_location_id_text")
    public void setInternal_transit_location_id_text(String  internal_transit_location_id_text){
        this.internal_transit_location_id_text = internal_transit_location_id_text ;
        this.internal_transit_location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INTERNAL_TRANSIT_LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInternal_transit_location_id_textDirtyFlag(){
        return internal_transit_location_id_textDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return website ;
    }

    /**
     * 设置 [WEBSITE]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return websiteDirtyFlag ;
    }

    /**
     * 获取 [VAT]
     */
    @JsonProperty("vat")
    public String getVat(){
        return vat ;
    }

    /**
     * 设置 [VAT]
     */
    @JsonProperty("vat")
    public void setVat(String  vat){
        this.vat = vat ;
        this.vatDirtyFlag = true ;
    }

    /**
     * 获取 [VAT]脏标记
     */
    @JsonIgnore
    public boolean getVatDirtyFlag(){
        return vatDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return resource_calendar_id_text ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return resource_calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_EXCHANGE_JOURNAL_ID_TEXT]
     */
    @JsonProperty("currency_exchange_journal_id_text")
    public String getCurrency_exchange_journal_id_text(){
        return currency_exchange_journal_id_text ;
    }

    /**
     * 设置 [CURRENCY_EXCHANGE_JOURNAL_ID_TEXT]
     */
    @JsonProperty("currency_exchange_journal_id_text")
    public void setCurrency_exchange_journal_id_text(String  currency_exchange_journal_id_text){
        this.currency_exchange_journal_id_text = currency_exchange_journal_id_text ;
        this.currency_exchange_journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_EXCHANGE_JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_exchange_journal_id_textDirtyFlag(){
        return currency_exchange_journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public Integer getProperty_stock_account_output_categ_id(){
        return property_stock_account_output_categ_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public void setProperty_stock_account_output_categ_id(Integer  property_stock_account_output_categ_id){
        this.property_stock_account_output_categ_id = property_stock_account_output_categ_id ;
        this.property_stock_account_output_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_idDirtyFlag(){
        return property_stock_account_output_categ_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public Integer getProperty_stock_valuation_account_id(){
        return property_stock_valuation_account_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public void setProperty_stock_valuation_account_id(Integer  property_stock_valuation_account_id){
        this.property_stock_valuation_account_id = property_stock_valuation_account_id ;
        this.property_stock_valuation_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_idDirtyFlag(){
        return property_stock_valuation_account_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_MOVE_ID]
     */
    @JsonProperty("account_opening_move_id")
    public Integer getAccount_opening_move_id(){
        return account_opening_move_id ;
    }

    /**
     * 设置 [ACCOUNT_OPENING_MOVE_ID]
     */
    @JsonProperty("account_opening_move_id")
    public void setAccount_opening_move_id(Integer  account_opening_move_id){
        this.account_opening_move_id = account_opening_move_id ;
        this.account_opening_move_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_OPENING_MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_move_idDirtyFlag(){
        return account_opening_move_idDirtyFlag ;
    }

    /**
     * 获取 [INTERNAL_TRANSIT_LOCATION_ID]
     */
    @JsonProperty("internal_transit_location_id")
    public Integer getInternal_transit_location_id(){
        return internal_transit_location_id ;
    }

    /**
     * 设置 [INTERNAL_TRANSIT_LOCATION_ID]
     */
    @JsonProperty("internal_transit_location_id")
    public void setInternal_transit_location_id(Integer  internal_transit_location_id){
        this.internal_transit_location_id = internal_transit_location_id ;
        this.internal_transit_location_idDirtyFlag = true ;
    }

    /**
     * 获取 [INTERNAL_TRANSIT_LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getInternal_transit_location_idDirtyFlag(){
        return internal_transit_location_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_PURCHASE_TAX_ID]
     */
    @JsonProperty("account_purchase_tax_id")
    public Integer getAccount_purchase_tax_id(){
        return account_purchase_tax_id ;
    }

    /**
     * 设置 [ACCOUNT_PURCHASE_TAX_ID]
     */
    @JsonProperty("account_purchase_tax_id")
    public void setAccount_purchase_tax_id(Integer  account_purchase_tax_id){
        this.account_purchase_tax_id = account_purchase_tax_id ;
        this.account_purchase_tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_PURCHASE_TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_purchase_tax_idDirtyFlag(){
        return account_purchase_tax_idDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return chart_template_id ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return chart_template_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_SALE_TAX_ID]
     */
    @JsonProperty("account_sale_tax_id")
    public Integer getAccount_sale_tax_id(){
        return account_sale_tax_id ;
    }

    /**
     * 设置 [ACCOUNT_SALE_TAX_ID]
     */
    @JsonProperty("account_sale_tax_id")
    public void setAccount_sale_tax_id(Integer  account_sale_tax_id){
        this.account_sale_tax_id = account_sale_tax_id ;
        this.account_sale_tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_SALE_TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_sale_tax_idDirtyFlag(){
        return account_sale_tax_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_JOURNAL_ID]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public Integer getTax_cash_basis_journal_id(){
        return tax_cash_basis_journal_id ;
    }

    /**
     * 设置 [TAX_CASH_BASIS_JOURNAL_ID]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public void setTax_cash_basis_journal_id(Integer  tax_cash_basis_journal_id){
        this.tax_cash_basis_journal_id = tax_cash_basis_journal_id ;
        this.tax_cash_basis_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_journal_idDirtyFlag(){
        return tax_cash_basis_journal_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public Integer getProperty_stock_account_input_categ_id(){
        return property_stock_account_input_categ_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public void setProperty_stock_account_input_categ_id(Integer  property_stock_account_input_categ_id){
        this.property_stock_account_input_categ_id = property_stock_account_input_categ_id ;
        this.property_stock_account_input_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_idDirtyFlag(){
        return property_stock_account_input_categ_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [INCOTERM_ID]
     */
    @JsonProperty("incoterm_id")
    public Integer getIncoterm_id(){
        return incoterm_id ;
    }

    /**
     * 设置 [INCOTERM_ID]
     */
    @JsonProperty("incoterm_id")
    public void setIncoterm_id(Integer  incoterm_id){
        this.incoterm_id = incoterm_id ;
        this.incoterm_idDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_idDirtyFlag(){
        return incoterm_idDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return resource_calendar_id ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return resource_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_ID]
     */
    @JsonProperty("transfer_account_id")
    public Integer getTransfer_account_id(){
        return transfer_account_id ;
    }

    /**
     * 设置 [TRANSFER_ACCOUNT_ID]
     */
    @JsonProperty("transfer_account_id")
    public void setTransfer_account_id(Integer  transfer_account_id){
        this.transfer_account_id = transfer_account_id ;
        this.transfer_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_idDirtyFlag(){
        return transfer_account_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_EXCHANGE_JOURNAL_ID]
     */
    @JsonProperty("currency_exchange_journal_id")
    public Integer getCurrency_exchange_journal_id(){
        return currency_exchange_journal_id ;
    }

    /**
     * 设置 [CURRENCY_EXCHANGE_JOURNAL_ID]
     */
    @JsonProperty("currency_exchange_journal_id")
    public void setCurrency_exchange_journal_id(Integer  currency_exchange_journal_id){
        this.currency_exchange_journal_id = currency_exchange_journal_id ;
        this.currency_exchange_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_EXCHANGE_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_exchange_journal_idDirtyFlag(){
        return currency_exchange_journal_idDirtyFlag ;
    }



    public Res_company toDO() {
        Res_company srfdomain = new Res_company();
        if(getReport_headerDirtyFlag())
            srfdomain.setReport_header(report_header);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getSale_quotation_onboarding_stateDirtyFlag())
            srfdomain.setSale_quotation_onboarding_state(sale_quotation_onboarding_state);
        if(getQuotation_validity_daysDirtyFlag())
            srfdomain.setQuotation_validity_days(quotation_validity_days);
        if(getAccount_onboarding_invoice_layout_stateDirtyFlag())
            srfdomain.setAccount_onboarding_invoice_layout_state(account_onboarding_invoice_layout_state);
        if(getAccount_noDirtyFlag())
            srfdomain.setAccount_no(account_no);
        if(getBank_account_code_prefixDirtyFlag())
            srfdomain.setBank_account_code_prefix(bank_account_code_prefix);
        if(getBank_journal_idsDirtyFlag())
            srfdomain.setBank_journal_ids(bank_journal_ids);
        if(getZipDirtyFlag())
            srfdomain.setZip(zip);
        if(getPeriod_lock_dateDirtyFlag())
            srfdomain.setPeriod_lock_date(period_lock_date);
        if(getState_idDirtyFlag())
            srfdomain.setState_id(state_id);
        if(getTax_exigibilityDirtyFlag())
            srfdomain.setTax_exigibility(tax_exigibility);
        if(getResource_calendar_idsDirtyFlag())
            srfdomain.setResource_calendar_ids(resource_calendar_ids);
        if(getBank_idsDirtyFlag())
            srfdomain.setBank_ids(bank_ids);
        if(getAccount_dashboard_onboarding_stateDirtyFlag())
            srfdomain.setAccount_dashboard_onboarding_state(account_dashboard_onboarding_state);
        if(getPropagation_minimum_deltaDirtyFlag())
            srfdomain.setPropagation_minimum_delta(propagation_minimum_delta);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getSnailmail_colorDirtyFlag())
            srfdomain.setSnailmail_color(snailmail_color);
        if(getOverdue_msgDirtyFlag())
            srfdomain.setOverdue_msg(overdue_msg);
        if(getCityDirtyFlag())
            srfdomain.setCity(city);
        if(getAccount_setup_coa_stateDirtyFlag())
            srfdomain.setAccount_setup_coa_state(account_setup_coa_state);
        if(getInvoice_reference_typeDirtyFlag())
            srfdomain.setInvoice_reference_type(invoice_reference_type);
        if(getCatchallDirtyFlag())
            srfdomain.setCatchall(catchall);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getAnglo_saxon_accountingDirtyFlag())
            srfdomain.setAnglo_saxon_accounting(anglo_saxon_accounting);
        if(getSnailmail_duplexDirtyFlag())
            srfdomain.setSnailmail_duplex(snailmail_duplex);
        if(getSocial_githubDirtyFlag())
            srfdomain.setSocial_github(social_github);
        if(getAccount_setup_bank_data_stateDirtyFlag())
            srfdomain.setAccount_setup_bank_data_state(account_setup_bank_data_state);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getStreet2DirtyFlag())
            srfdomain.setStreet2(street2);
        if(getExpects_chart_of_accountsDirtyFlag())
            srfdomain.setExpects_chart_of_accounts(expects_chart_of_accounts);
        if(getTransfer_account_code_prefixDirtyFlag())
            srfdomain.setTransfer_account_code_prefix(transfer_account_code_prefix);
        if(getFiscalyear_last_dayDirtyFlag())
            srfdomain.setFiscalyear_last_day(fiscalyear_last_day);
        if(getUser_idsDirtyFlag())
            srfdomain.setUser_ids(user_ids);
        if(getAccount_bank_reconciliation_startDirtyFlag())
            srfdomain.setAccount_bank_reconciliation_start(account_bank_reconciliation_start);
        if(getPortal_confirmation_payDirtyFlag())
            srfdomain.setPortal_confirmation_pay(portal_confirmation_pay);
        if(getQr_codeDirtyFlag())
            srfdomain.setQr_code(qr_code);
        if(getStreetDirtyFlag())
            srfdomain.setStreet(street);
        if(getAccount_invoice_onboarding_stateDirtyFlag())
            srfdomain.setAccount_invoice_onboarding_state(account_invoice_onboarding_state);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getNomenclature_idDirtyFlag())
            srfdomain.setNomenclature_id(nomenclature_id);
        if(getSocial_googleplusDirtyFlag())
            srfdomain.setSocial_googleplus(social_googleplus);
        if(getPayment_acquirer_onboarding_stateDirtyFlag())
            srfdomain.setPayment_acquirer_onboarding_state(payment_acquirer_onboarding_state);
        if(getReport_footerDirtyFlag())
            srfdomain.setReport_footer(report_footer);
        if(getPayment_onboarding_payment_methodDirtyFlag())
            srfdomain.setPayment_onboarding_payment_method(payment_onboarding_payment_method);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getPo_double_validationDirtyFlag())
            srfdomain.setPo_double_validation(po_double_validation);
        if(getPo_leadDirtyFlag())
            srfdomain.setPo_lead(po_lead);
        if(getSale_onboarding_sample_quotation_stateDirtyFlag())
            srfdomain.setSale_onboarding_sample_quotation_state(sale_onboarding_sample_quotation_state);
        if(getSale_onboarding_order_confirmation_stateDirtyFlag())
            srfdomain.setSale_onboarding_order_confirmation_state(sale_onboarding_order_confirmation_state);
        if(getExternal_report_layout_idDirtyFlag())
            srfdomain.setExternal_report_layout_id(external_report_layout_id);
        if(getSale_onboarding_payment_methodDirtyFlag())
            srfdomain.setSale_onboarding_payment_method(sale_onboarding_payment_method);
        if(getAccount_onboarding_sample_invoice_stateDirtyFlag())
            srfdomain.setAccount_onboarding_sample_invoice_state(account_onboarding_sample_invoice_state);
        if(getBase_onboarding_company_stateDirtyFlag())
            srfdomain.setBase_onboarding_company_state(base_onboarding_company_state);
        if(getSocial_linkedinDirtyFlag())
            srfdomain.setSocial_linkedin(social_linkedin);
        if(getManufacturing_leadDirtyFlag())
            srfdomain.setManufacturing_lead(manufacturing_lead);
        if(getSale_noteDirtyFlag())
            srfdomain.setSale_note(sale_note);
        if(getPo_double_validation_amountDirtyFlag())
            srfdomain.setPo_double_validation_amount(po_double_validation_amount);
        if(getPo_lockDirtyFlag())
            srfdomain.setPo_lock(po_lock);
        if(getSocial_twitterDirtyFlag())
            srfdomain.setSocial_twitter(social_twitter);
        if(getSocial_instagramDirtyFlag())
            srfdomain.setSocial_instagram(social_instagram);
        if(getAccount_setup_fy_data_stateDirtyFlag())
            srfdomain.setAccount_setup_fy_data_state(account_setup_fy_data_state);
        if(getTax_calculation_rounding_methodDirtyFlag())
            srfdomain.setTax_calculation_rounding_method(tax_calculation_rounding_method);
        if(getCash_account_code_prefixDirtyFlag())
            srfdomain.setCash_account_code_prefix(cash_account_code_prefix);
        if(getAccount_onboarding_sale_tax_stateDirtyFlag())
            srfdomain.setAccount_onboarding_sale_tax_state(account_onboarding_sale_tax_state);
        if(getSecurity_leadDirtyFlag())
            srfdomain.setSecurity_lead(security_lead);
        if(getWebsite_theme_onboarding_doneDirtyFlag())
            srfdomain.setWebsite_theme_onboarding_done(website_theme_onboarding_done);
        if(getInvoice_is_printDirtyFlag())
            srfdomain.setInvoice_is_print(invoice_is_print);
        if(getCompany_registryDirtyFlag())
            srfdomain.setCompany_registry(company_registry);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getLogo_webDirtyFlag())
            srfdomain.setLogo_web(logo_web);
        if(getFiscalyear_lock_dateDirtyFlag())
            srfdomain.setFiscalyear_lock_date(fiscalyear_lock_date);
        if(getInvoice_is_snailmailDirtyFlag())
            srfdomain.setInvoice_is_snailmail(invoice_is_snailmail);
        if(getWebsite_sale_onboarding_payment_acquirer_stateDirtyFlag())
            srfdomain.setWebsite_sale_onboarding_payment_acquirer_state(website_sale_onboarding_payment_acquirer_state);
        if(getSocial_facebookDirtyFlag())
            srfdomain.setSocial_facebook(social_facebook);
        if(getPortal_confirmation_signDirtyFlag())
            srfdomain.setPortal_confirmation_sign(portal_confirmation_sign);
        if(getPaperformat_idDirtyFlag())
            srfdomain.setPaperformat_id(paperformat_id);
        if(getFiscalyear_last_monthDirtyFlag())
            srfdomain.setFiscalyear_last_month(fiscalyear_last_month);
        if(getInvoice_is_emailDirtyFlag())
            srfdomain.setInvoice_is_email(invoice_is_email);
        if(getSocial_youtubeDirtyFlag())
            srfdomain.setSocial_youtube(social_youtube);
        if(getExpense_currency_exchange_account_idDirtyFlag())
            srfdomain.setExpense_currency_exchange_account_id(expense_currency_exchange_account_id);
        if(getPartner_gidDirtyFlag())
            srfdomain.setPartner_gid(partner_gid);
        if(getPhoneDirtyFlag())
            srfdomain.setPhone(phone);
        if(getLogoDirtyFlag())
            srfdomain.setLogo(logo);
        if(getProperty_stock_account_input_categ_id_textDirtyFlag())
            srfdomain.setProperty_stock_account_input_categ_id_text(property_stock_account_input_categ_id_text);
        if(getAccount_purchase_tax_id_textDirtyFlag())
            srfdomain.setAccount_purchase_tax_id_text(account_purchase_tax_id_text);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIncoterm_id_textDirtyFlag())
            srfdomain.setIncoterm_id_text(incoterm_id_text);
        if(getAccount_opening_journal_idDirtyFlag())
            srfdomain.setAccount_opening_journal_id(account_opening_journal_id);
        if(getTransfer_account_id_textDirtyFlag())
            srfdomain.setTransfer_account_id_text(transfer_account_id_text);
        if(getIncome_currency_exchange_account_idDirtyFlag())
            srfdomain.setIncome_currency_exchange_account_id(income_currency_exchange_account_id);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getAccount_opening_dateDirtyFlag())
            srfdomain.setAccount_opening_date(account_opening_date);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getChart_template_id_textDirtyFlag())
            srfdomain.setChart_template_id_text(chart_template_id_text);
        if(getAccount_sale_tax_id_textDirtyFlag())
            srfdomain.setAccount_sale_tax_id_text(account_sale_tax_id_text);
        if(getAccount_opening_move_id_textDirtyFlag())
            srfdomain.setAccount_opening_move_id_text(account_opening_move_id_text);
        if(getEmailDirtyFlag())
            srfdomain.setEmail(email);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getProperty_stock_account_output_categ_id_textDirtyFlag())
            srfdomain.setProperty_stock_account_output_categ_id_text(property_stock_account_output_categ_id_text);
        if(getProperty_stock_valuation_account_id_textDirtyFlag())
            srfdomain.setProperty_stock_valuation_account_id_text(property_stock_valuation_account_id_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getTax_cash_basis_journal_id_textDirtyFlag())
            srfdomain.setTax_cash_basis_journal_id_text(tax_cash_basis_journal_id_text);
        if(getInternal_transit_location_id_textDirtyFlag())
            srfdomain.setInternal_transit_location_id_text(internal_transit_location_id_text);
        if(getWebsiteDirtyFlag())
            srfdomain.setWebsite(website);
        if(getVatDirtyFlag())
            srfdomain.setVat(vat);
        if(getResource_calendar_id_textDirtyFlag())
            srfdomain.setResource_calendar_id_text(resource_calendar_id_text);
        if(getCurrency_exchange_journal_id_textDirtyFlag())
            srfdomain.setCurrency_exchange_journal_id_text(currency_exchange_journal_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProperty_stock_account_output_categ_idDirtyFlag())
            srfdomain.setProperty_stock_account_output_categ_id(property_stock_account_output_categ_id);
        if(getProperty_stock_valuation_account_idDirtyFlag())
            srfdomain.setProperty_stock_valuation_account_id(property_stock_valuation_account_id);
        if(getAccount_opening_move_idDirtyFlag())
            srfdomain.setAccount_opening_move_id(account_opening_move_id);
        if(getInternal_transit_location_idDirtyFlag())
            srfdomain.setInternal_transit_location_id(internal_transit_location_id);
        if(getAccount_purchase_tax_idDirtyFlag())
            srfdomain.setAccount_purchase_tax_id(account_purchase_tax_id);
        if(getChart_template_idDirtyFlag())
            srfdomain.setChart_template_id(chart_template_id);
        if(getAccount_sale_tax_idDirtyFlag())
            srfdomain.setAccount_sale_tax_id(account_sale_tax_id);
        if(getTax_cash_basis_journal_idDirtyFlag())
            srfdomain.setTax_cash_basis_journal_id(tax_cash_basis_journal_id);
        if(getProperty_stock_account_input_categ_idDirtyFlag())
            srfdomain.setProperty_stock_account_input_categ_id(property_stock_account_input_categ_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getIncoterm_idDirtyFlag())
            srfdomain.setIncoterm_id(incoterm_id);
        if(getResource_calendar_idDirtyFlag())
            srfdomain.setResource_calendar_id(resource_calendar_id);
        if(getTransfer_account_idDirtyFlag())
            srfdomain.setTransfer_account_id(transfer_account_id);
        if(getCurrency_exchange_journal_idDirtyFlag())
            srfdomain.setCurrency_exchange_journal_id(currency_exchange_journal_id);

        return srfdomain;
    }

    public void fromDO(Res_company srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getReport_headerDirtyFlag())
            this.setReport_header(srfdomain.getReport_header());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getSale_quotation_onboarding_stateDirtyFlag())
            this.setSale_quotation_onboarding_state(srfdomain.getSale_quotation_onboarding_state());
        if(srfdomain.getQuotation_validity_daysDirtyFlag())
            this.setQuotation_validity_days(srfdomain.getQuotation_validity_days());
        if(srfdomain.getAccount_onboarding_invoice_layout_stateDirtyFlag())
            this.setAccount_onboarding_invoice_layout_state(srfdomain.getAccount_onboarding_invoice_layout_state());
        if(srfdomain.getAccount_noDirtyFlag())
            this.setAccount_no(srfdomain.getAccount_no());
        if(srfdomain.getBank_account_code_prefixDirtyFlag())
            this.setBank_account_code_prefix(srfdomain.getBank_account_code_prefix());
        if(srfdomain.getBank_journal_idsDirtyFlag())
            this.setBank_journal_ids(srfdomain.getBank_journal_ids());
        if(srfdomain.getZipDirtyFlag())
            this.setZip(srfdomain.getZip());
        if(srfdomain.getPeriod_lock_dateDirtyFlag())
            this.setPeriod_lock_date(srfdomain.getPeriod_lock_date());
        if(srfdomain.getState_idDirtyFlag())
            this.setState_id(srfdomain.getState_id());
        if(srfdomain.getTax_exigibilityDirtyFlag())
            this.setTax_exigibility(srfdomain.getTax_exigibility());
        if(srfdomain.getResource_calendar_idsDirtyFlag())
            this.setResource_calendar_ids(srfdomain.getResource_calendar_ids());
        if(srfdomain.getBank_idsDirtyFlag())
            this.setBank_ids(srfdomain.getBank_ids());
        if(srfdomain.getAccount_dashboard_onboarding_stateDirtyFlag())
            this.setAccount_dashboard_onboarding_state(srfdomain.getAccount_dashboard_onboarding_state());
        if(srfdomain.getPropagation_minimum_deltaDirtyFlag())
            this.setPropagation_minimum_delta(srfdomain.getPropagation_minimum_delta());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getSnailmail_colorDirtyFlag())
            this.setSnailmail_color(srfdomain.getSnailmail_color());
        if(srfdomain.getOverdue_msgDirtyFlag())
            this.setOverdue_msg(srfdomain.getOverdue_msg());
        if(srfdomain.getCityDirtyFlag())
            this.setCity(srfdomain.getCity());
        if(srfdomain.getAccount_setup_coa_stateDirtyFlag())
            this.setAccount_setup_coa_state(srfdomain.getAccount_setup_coa_state());
        if(srfdomain.getInvoice_reference_typeDirtyFlag())
            this.setInvoice_reference_type(srfdomain.getInvoice_reference_type());
        if(srfdomain.getCatchallDirtyFlag())
            this.setCatchall(srfdomain.getCatchall());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getAnglo_saxon_accountingDirtyFlag())
            this.setAnglo_saxon_accounting(srfdomain.getAnglo_saxon_accounting());
        if(srfdomain.getSnailmail_duplexDirtyFlag())
            this.setSnailmail_duplex(srfdomain.getSnailmail_duplex());
        if(srfdomain.getSocial_githubDirtyFlag())
            this.setSocial_github(srfdomain.getSocial_github());
        if(srfdomain.getAccount_setup_bank_data_stateDirtyFlag())
            this.setAccount_setup_bank_data_state(srfdomain.getAccount_setup_bank_data_state());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getStreet2DirtyFlag())
            this.setStreet2(srfdomain.getStreet2());
        if(srfdomain.getExpects_chart_of_accountsDirtyFlag())
            this.setExpects_chart_of_accounts(srfdomain.getExpects_chart_of_accounts());
        if(srfdomain.getTransfer_account_code_prefixDirtyFlag())
            this.setTransfer_account_code_prefix(srfdomain.getTransfer_account_code_prefix());
        if(srfdomain.getFiscalyear_last_dayDirtyFlag())
            this.setFiscalyear_last_day(srfdomain.getFiscalyear_last_day());
        if(srfdomain.getUser_idsDirtyFlag())
            this.setUser_ids(srfdomain.getUser_ids());
        if(srfdomain.getAccount_bank_reconciliation_startDirtyFlag())
            this.setAccount_bank_reconciliation_start(srfdomain.getAccount_bank_reconciliation_start());
        if(srfdomain.getPortal_confirmation_payDirtyFlag())
            this.setPortal_confirmation_pay(srfdomain.getPortal_confirmation_pay());
        if(srfdomain.getQr_codeDirtyFlag())
            this.setQr_code(srfdomain.getQr_code());
        if(srfdomain.getStreetDirtyFlag())
            this.setStreet(srfdomain.getStreet());
        if(srfdomain.getAccount_invoice_onboarding_stateDirtyFlag())
            this.setAccount_invoice_onboarding_state(srfdomain.getAccount_invoice_onboarding_state());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getNomenclature_idDirtyFlag())
            this.setNomenclature_id(srfdomain.getNomenclature_id());
        if(srfdomain.getSocial_googleplusDirtyFlag())
            this.setSocial_googleplus(srfdomain.getSocial_googleplus());
        if(srfdomain.getPayment_acquirer_onboarding_stateDirtyFlag())
            this.setPayment_acquirer_onboarding_state(srfdomain.getPayment_acquirer_onboarding_state());
        if(srfdomain.getReport_footerDirtyFlag())
            this.setReport_footer(srfdomain.getReport_footer());
        if(srfdomain.getPayment_onboarding_payment_methodDirtyFlag())
            this.setPayment_onboarding_payment_method(srfdomain.getPayment_onboarding_payment_method());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getPo_double_validationDirtyFlag())
            this.setPo_double_validation(srfdomain.getPo_double_validation());
        if(srfdomain.getPo_leadDirtyFlag())
            this.setPo_lead(srfdomain.getPo_lead());
        if(srfdomain.getSale_onboarding_sample_quotation_stateDirtyFlag())
            this.setSale_onboarding_sample_quotation_state(srfdomain.getSale_onboarding_sample_quotation_state());
        if(srfdomain.getSale_onboarding_order_confirmation_stateDirtyFlag())
            this.setSale_onboarding_order_confirmation_state(srfdomain.getSale_onboarding_order_confirmation_state());
        if(srfdomain.getExternal_report_layout_idDirtyFlag())
            this.setExternal_report_layout_id(srfdomain.getExternal_report_layout_id());
        if(srfdomain.getSale_onboarding_payment_methodDirtyFlag())
            this.setSale_onboarding_payment_method(srfdomain.getSale_onboarding_payment_method());
        if(srfdomain.getAccount_onboarding_sample_invoice_stateDirtyFlag())
            this.setAccount_onboarding_sample_invoice_state(srfdomain.getAccount_onboarding_sample_invoice_state());
        if(srfdomain.getBase_onboarding_company_stateDirtyFlag())
            this.setBase_onboarding_company_state(srfdomain.getBase_onboarding_company_state());
        if(srfdomain.getSocial_linkedinDirtyFlag())
            this.setSocial_linkedin(srfdomain.getSocial_linkedin());
        if(srfdomain.getManufacturing_leadDirtyFlag())
            this.setManufacturing_lead(srfdomain.getManufacturing_lead());
        if(srfdomain.getSale_noteDirtyFlag())
            this.setSale_note(srfdomain.getSale_note());
        if(srfdomain.getPo_double_validation_amountDirtyFlag())
            this.setPo_double_validation_amount(srfdomain.getPo_double_validation_amount());
        if(srfdomain.getPo_lockDirtyFlag())
            this.setPo_lock(srfdomain.getPo_lock());
        if(srfdomain.getSocial_twitterDirtyFlag())
            this.setSocial_twitter(srfdomain.getSocial_twitter());
        if(srfdomain.getSocial_instagramDirtyFlag())
            this.setSocial_instagram(srfdomain.getSocial_instagram());
        if(srfdomain.getAccount_setup_fy_data_stateDirtyFlag())
            this.setAccount_setup_fy_data_state(srfdomain.getAccount_setup_fy_data_state());
        if(srfdomain.getTax_calculation_rounding_methodDirtyFlag())
            this.setTax_calculation_rounding_method(srfdomain.getTax_calculation_rounding_method());
        if(srfdomain.getCash_account_code_prefixDirtyFlag())
            this.setCash_account_code_prefix(srfdomain.getCash_account_code_prefix());
        if(srfdomain.getAccount_onboarding_sale_tax_stateDirtyFlag())
            this.setAccount_onboarding_sale_tax_state(srfdomain.getAccount_onboarding_sale_tax_state());
        if(srfdomain.getSecurity_leadDirtyFlag())
            this.setSecurity_lead(srfdomain.getSecurity_lead());
        if(srfdomain.getWebsite_theme_onboarding_doneDirtyFlag())
            this.setWebsite_theme_onboarding_done(srfdomain.getWebsite_theme_onboarding_done());
        if(srfdomain.getInvoice_is_printDirtyFlag())
            this.setInvoice_is_print(srfdomain.getInvoice_is_print());
        if(srfdomain.getCompany_registryDirtyFlag())
            this.setCompany_registry(srfdomain.getCompany_registry());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getLogo_webDirtyFlag())
            this.setLogo_web(srfdomain.getLogo_web());
        if(srfdomain.getFiscalyear_lock_dateDirtyFlag())
            this.setFiscalyear_lock_date(srfdomain.getFiscalyear_lock_date());
        if(srfdomain.getInvoice_is_snailmailDirtyFlag())
            this.setInvoice_is_snailmail(srfdomain.getInvoice_is_snailmail());
        if(srfdomain.getWebsite_sale_onboarding_payment_acquirer_stateDirtyFlag())
            this.setWebsite_sale_onboarding_payment_acquirer_state(srfdomain.getWebsite_sale_onboarding_payment_acquirer_state());
        if(srfdomain.getSocial_facebookDirtyFlag())
            this.setSocial_facebook(srfdomain.getSocial_facebook());
        if(srfdomain.getPortal_confirmation_signDirtyFlag())
            this.setPortal_confirmation_sign(srfdomain.getPortal_confirmation_sign());
        if(srfdomain.getPaperformat_idDirtyFlag())
            this.setPaperformat_id(srfdomain.getPaperformat_id());
        if(srfdomain.getFiscalyear_last_monthDirtyFlag())
            this.setFiscalyear_last_month(srfdomain.getFiscalyear_last_month());
        if(srfdomain.getInvoice_is_emailDirtyFlag())
            this.setInvoice_is_email(srfdomain.getInvoice_is_email());
        if(srfdomain.getSocial_youtubeDirtyFlag())
            this.setSocial_youtube(srfdomain.getSocial_youtube());
        if(srfdomain.getExpense_currency_exchange_account_idDirtyFlag())
            this.setExpense_currency_exchange_account_id(srfdomain.getExpense_currency_exchange_account_id());
        if(srfdomain.getPartner_gidDirtyFlag())
            this.setPartner_gid(srfdomain.getPartner_gid());
        if(srfdomain.getPhoneDirtyFlag())
            this.setPhone(srfdomain.getPhone());
        if(srfdomain.getLogoDirtyFlag())
            this.setLogo(srfdomain.getLogo());
        if(srfdomain.getProperty_stock_account_input_categ_id_textDirtyFlag())
            this.setProperty_stock_account_input_categ_id_text(srfdomain.getProperty_stock_account_input_categ_id_text());
        if(srfdomain.getAccount_purchase_tax_id_textDirtyFlag())
            this.setAccount_purchase_tax_id_text(srfdomain.getAccount_purchase_tax_id_text());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIncoterm_id_textDirtyFlag())
            this.setIncoterm_id_text(srfdomain.getIncoterm_id_text());
        if(srfdomain.getAccount_opening_journal_idDirtyFlag())
            this.setAccount_opening_journal_id(srfdomain.getAccount_opening_journal_id());
        if(srfdomain.getTransfer_account_id_textDirtyFlag())
            this.setTransfer_account_id_text(srfdomain.getTransfer_account_id_text());
        if(srfdomain.getIncome_currency_exchange_account_idDirtyFlag())
            this.setIncome_currency_exchange_account_id(srfdomain.getIncome_currency_exchange_account_id());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getAccount_opening_dateDirtyFlag())
            this.setAccount_opening_date(srfdomain.getAccount_opening_date());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getChart_template_id_textDirtyFlag())
            this.setChart_template_id_text(srfdomain.getChart_template_id_text());
        if(srfdomain.getAccount_sale_tax_id_textDirtyFlag())
            this.setAccount_sale_tax_id_text(srfdomain.getAccount_sale_tax_id_text());
        if(srfdomain.getAccount_opening_move_id_textDirtyFlag())
            this.setAccount_opening_move_id_text(srfdomain.getAccount_opening_move_id_text());
        if(srfdomain.getEmailDirtyFlag())
            this.setEmail(srfdomain.getEmail());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getProperty_stock_account_output_categ_id_textDirtyFlag())
            this.setProperty_stock_account_output_categ_id_text(srfdomain.getProperty_stock_account_output_categ_id_text());
        if(srfdomain.getProperty_stock_valuation_account_id_textDirtyFlag())
            this.setProperty_stock_valuation_account_id_text(srfdomain.getProperty_stock_valuation_account_id_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getTax_cash_basis_journal_id_textDirtyFlag())
            this.setTax_cash_basis_journal_id_text(srfdomain.getTax_cash_basis_journal_id_text());
        if(srfdomain.getInternal_transit_location_id_textDirtyFlag())
            this.setInternal_transit_location_id_text(srfdomain.getInternal_transit_location_id_text());
        if(srfdomain.getWebsiteDirtyFlag())
            this.setWebsite(srfdomain.getWebsite());
        if(srfdomain.getVatDirtyFlag())
            this.setVat(srfdomain.getVat());
        if(srfdomain.getResource_calendar_id_textDirtyFlag())
            this.setResource_calendar_id_text(srfdomain.getResource_calendar_id_text());
        if(srfdomain.getCurrency_exchange_journal_id_textDirtyFlag())
            this.setCurrency_exchange_journal_id_text(srfdomain.getCurrency_exchange_journal_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProperty_stock_account_output_categ_idDirtyFlag())
            this.setProperty_stock_account_output_categ_id(srfdomain.getProperty_stock_account_output_categ_id());
        if(srfdomain.getProperty_stock_valuation_account_idDirtyFlag())
            this.setProperty_stock_valuation_account_id(srfdomain.getProperty_stock_valuation_account_id());
        if(srfdomain.getAccount_opening_move_idDirtyFlag())
            this.setAccount_opening_move_id(srfdomain.getAccount_opening_move_id());
        if(srfdomain.getInternal_transit_location_idDirtyFlag())
            this.setInternal_transit_location_id(srfdomain.getInternal_transit_location_id());
        if(srfdomain.getAccount_purchase_tax_idDirtyFlag())
            this.setAccount_purchase_tax_id(srfdomain.getAccount_purchase_tax_id());
        if(srfdomain.getChart_template_idDirtyFlag())
            this.setChart_template_id(srfdomain.getChart_template_id());
        if(srfdomain.getAccount_sale_tax_idDirtyFlag())
            this.setAccount_sale_tax_id(srfdomain.getAccount_sale_tax_id());
        if(srfdomain.getTax_cash_basis_journal_idDirtyFlag())
            this.setTax_cash_basis_journal_id(srfdomain.getTax_cash_basis_journal_id());
        if(srfdomain.getProperty_stock_account_input_categ_idDirtyFlag())
            this.setProperty_stock_account_input_categ_id(srfdomain.getProperty_stock_account_input_categ_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getIncoterm_idDirtyFlag())
            this.setIncoterm_id(srfdomain.getIncoterm_id());
        if(srfdomain.getResource_calendar_idDirtyFlag())
            this.setResource_calendar_id(srfdomain.getResource_calendar_id());
        if(srfdomain.getTransfer_account_idDirtyFlag())
            this.setTransfer_account_id(srfdomain.getTransfer_account_id());
        if(srfdomain.getCurrency_exchange_journal_idDirtyFlag())
            this.setCurrency_exchange_journal_id(srfdomain.getCurrency_exchange_journal_id());

    }

    public List<Res_companyDTO> fromDOPage(List<Res_company> poPage)   {
        if(poPage == null)
            return null;
        List<Res_companyDTO> dtos=new ArrayList<Res_companyDTO>();
        for(Res_company domain : poPage) {
            Res_companyDTO dto = new Res_companyDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

