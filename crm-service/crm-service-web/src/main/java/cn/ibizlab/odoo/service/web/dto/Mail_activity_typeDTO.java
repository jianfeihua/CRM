package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_activity_type.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_activity_typeDTO]
 */
public class Mail_activity_typeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RES_MODEL_CHANGE]
     *
     */
    @Mail_activity_typeRes_model_changeDefault(info = "默认规则")
    private String res_model_change;

    @JsonIgnore
    private boolean res_model_changeDirtyFlag;

    /**
     * 属性 [SUMMARY]
     *
     */
    @Mail_activity_typeSummaryDefault(info = "默认规则")
    private String summary;

    @JsonIgnore
    private boolean summaryDirtyFlag;

    /**
     * 属性 [FORCE_NEXT]
     *
     */
    @Mail_activity_typeForce_nextDefault(info = "默认规则")
    private String force_next;

    @JsonIgnore
    private boolean force_nextDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Mail_activity_typeSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_activity_typeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CATEGORY]
     *
     */
    @Mail_activity_typeCategoryDefault(info = "默认规则")
    private String category;

    @JsonIgnore
    private boolean categoryDirtyFlag;

    /**
     * 属性 [ICON]
     *
     */
    @Mail_activity_typeIconDefault(info = "默认规则")
    private String icon;

    @JsonIgnore
    private boolean iconDirtyFlag;

    /**
     * 属性 [DELAY_COUNT]
     *
     */
    @Mail_activity_typeDelay_countDefault(info = "默认规则")
    private Integer delay_count;

    @JsonIgnore
    private boolean delay_countDirtyFlag;

    /**
     * 属性 [PREVIOUS_TYPE_IDS]
     *
     */
    @Mail_activity_typePrevious_type_idsDefault(info = "默认规则")
    private String previous_type_ids;

    @JsonIgnore
    private boolean previous_type_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_activity_typeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Mail_activity_typeActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [DELAY_FROM]
     *
     */
    @Mail_activity_typeDelay_fromDefault(info = "默认规则")
    private String delay_from;

    @JsonIgnore
    private boolean delay_fromDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_activity_typeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MAIL_TEMPLATE_IDS]
     *
     */
    @Mail_activity_typeMail_template_idsDefault(info = "默认规则")
    private String mail_template_ids;

    @JsonIgnore
    private boolean mail_template_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_activity_typeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_activity_typeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @Mail_activity_typeRes_model_idDefault(info = "默认规则")
    private Integer res_model_id;

    @JsonIgnore
    private boolean res_model_idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_activity_type__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DECORATION_TYPE]
     *
     */
    @Mail_activity_typeDecoration_typeDefault(info = "默认规则")
    private String decoration_type;

    @JsonIgnore
    private boolean decoration_typeDirtyFlag;

    /**
     * 属性 [NEXT_TYPE_IDS]
     *
     */
    @Mail_activity_typeNext_type_idsDefault(info = "默认规则")
    private String next_type_ids;

    @JsonIgnore
    private boolean next_type_idsDirtyFlag;

    /**
     * 属性 [DELAY_UNIT]
     *
     */
    @Mail_activity_typeDelay_unitDefault(info = "默认规则")
    private String delay_unit;

    @JsonIgnore
    private boolean delay_unitDirtyFlag;

    /**
     * 属性 [INITIAL_RES_MODEL_ID]
     *
     */
    @Mail_activity_typeInitial_res_model_idDefault(info = "默认规则")
    private Integer initial_res_model_id;

    @JsonIgnore
    private boolean initial_res_model_idDirtyFlag;

    /**
     * 属性 [DEFAULT_NEXT_TYPE_ID_TEXT]
     *
     */
    @Mail_activity_typeDefault_next_type_id_textDefault(info = "默认规则")
    private String default_next_type_id_text;

    @JsonIgnore
    private boolean default_next_type_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_activity_typeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_activity_typeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_activity_typeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_activity_typeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [DEFAULT_NEXT_TYPE_ID]
     *
     */
    @Mail_activity_typeDefault_next_type_idDefault(info = "默认规则")
    private Integer default_next_type_id;

    @JsonIgnore
    private boolean default_next_type_idDirtyFlag;


    /**
     * 获取 [RES_MODEL_CHANGE]
     */
    @JsonProperty("res_model_change")
    public String getRes_model_change(){
        return res_model_change ;
    }

    /**
     * 设置 [RES_MODEL_CHANGE]
     */
    @JsonProperty("res_model_change")
    public void setRes_model_change(String  res_model_change){
        this.res_model_change = res_model_change ;
        this.res_model_changeDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL_CHANGE]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_changeDirtyFlag(){
        return res_model_changeDirtyFlag ;
    }

    /**
     * 获取 [SUMMARY]
     */
    @JsonProperty("summary")
    public String getSummary(){
        return summary ;
    }

    /**
     * 设置 [SUMMARY]
     */
    @JsonProperty("summary")
    public void setSummary(String  summary){
        this.summary = summary ;
        this.summaryDirtyFlag = true ;
    }

    /**
     * 获取 [SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getSummaryDirtyFlag(){
        return summaryDirtyFlag ;
    }

    /**
     * 获取 [FORCE_NEXT]
     */
    @JsonProperty("force_next")
    public String getForce_next(){
        return force_next ;
    }

    /**
     * 设置 [FORCE_NEXT]
     */
    @JsonProperty("force_next")
    public void setForce_next(String  force_next){
        this.force_next = force_next ;
        this.force_nextDirtyFlag = true ;
    }

    /**
     * 获取 [FORCE_NEXT]脏标记
     */
    @JsonIgnore
    public boolean getForce_nextDirtyFlag(){
        return force_nextDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY]
     */
    @JsonProperty("category")
    public String getCategory(){
        return category ;
    }

    /**
     * 设置 [CATEGORY]
     */
    @JsonProperty("category")
    public void setCategory(String  category){
        this.category = category ;
        this.categoryDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY]脏标记
     */
    @JsonIgnore
    public boolean getCategoryDirtyFlag(){
        return categoryDirtyFlag ;
    }

    /**
     * 获取 [ICON]
     */
    @JsonProperty("icon")
    public String getIcon(){
        return icon ;
    }

    /**
     * 设置 [ICON]
     */
    @JsonProperty("icon")
    public void setIcon(String  icon){
        this.icon = icon ;
        this.iconDirtyFlag = true ;
    }

    /**
     * 获取 [ICON]脏标记
     */
    @JsonIgnore
    public boolean getIconDirtyFlag(){
        return iconDirtyFlag ;
    }

    /**
     * 获取 [DELAY_COUNT]
     */
    @JsonProperty("delay_count")
    public Integer getDelay_count(){
        return delay_count ;
    }

    /**
     * 设置 [DELAY_COUNT]
     */
    @JsonProperty("delay_count")
    public void setDelay_count(Integer  delay_count){
        this.delay_count = delay_count ;
        this.delay_countDirtyFlag = true ;
    }

    /**
     * 获取 [DELAY_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getDelay_countDirtyFlag(){
        return delay_countDirtyFlag ;
    }

    /**
     * 获取 [PREVIOUS_TYPE_IDS]
     */
    @JsonProperty("previous_type_ids")
    public String getPrevious_type_ids(){
        return previous_type_ids ;
    }

    /**
     * 设置 [PREVIOUS_TYPE_IDS]
     */
    @JsonProperty("previous_type_ids")
    public void setPrevious_type_ids(String  previous_type_ids){
        this.previous_type_ids = previous_type_ids ;
        this.previous_type_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PREVIOUS_TYPE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_type_idsDirtyFlag(){
        return previous_type_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [DELAY_FROM]
     */
    @JsonProperty("delay_from")
    public String getDelay_from(){
        return delay_from ;
    }

    /**
     * 设置 [DELAY_FROM]
     */
    @JsonProperty("delay_from")
    public void setDelay_from(String  delay_from){
        this.delay_from = delay_from ;
        this.delay_fromDirtyFlag = true ;
    }

    /**
     * 获取 [DELAY_FROM]脏标记
     */
    @JsonIgnore
    public boolean getDelay_fromDirtyFlag(){
        return delay_fromDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_IDS]
     */
    @JsonProperty("mail_template_ids")
    public String getMail_template_ids(){
        return mail_template_ids ;
    }

    /**
     * 设置 [MAIL_TEMPLATE_IDS]
     */
    @JsonProperty("mail_template_ids")
    public void setMail_template_ids(String  mail_template_ids){
        this.mail_template_ids = mail_template_ids ;
        this.mail_template_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idsDirtyFlag(){
        return mail_template_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return res_model_id ;
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return res_model_idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DECORATION_TYPE]
     */
    @JsonProperty("decoration_type")
    public String getDecoration_type(){
        return decoration_type ;
    }

    /**
     * 设置 [DECORATION_TYPE]
     */
    @JsonProperty("decoration_type")
    public void setDecoration_type(String  decoration_type){
        this.decoration_type = decoration_type ;
        this.decoration_typeDirtyFlag = true ;
    }

    /**
     * 获取 [DECORATION_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getDecoration_typeDirtyFlag(){
        return decoration_typeDirtyFlag ;
    }

    /**
     * 获取 [NEXT_TYPE_IDS]
     */
    @JsonProperty("next_type_ids")
    public String getNext_type_ids(){
        return next_type_ids ;
    }

    /**
     * 设置 [NEXT_TYPE_IDS]
     */
    @JsonProperty("next_type_ids")
    public void setNext_type_ids(String  next_type_ids){
        this.next_type_ids = next_type_ids ;
        this.next_type_idsDirtyFlag = true ;
    }

    /**
     * 获取 [NEXT_TYPE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getNext_type_idsDirtyFlag(){
        return next_type_idsDirtyFlag ;
    }

    /**
     * 获取 [DELAY_UNIT]
     */
    @JsonProperty("delay_unit")
    public String getDelay_unit(){
        return delay_unit ;
    }

    /**
     * 设置 [DELAY_UNIT]
     */
    @JsonProperty("delay_unit")
    public void setDelay_unit(String  delay_unit){
        this.delay_unit = delay_unit ;
        this.delay_unitDirtyFlag = true ;
    }

    /**
     * 获取 [DELAY_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getDelay_unitDirtyFlag(){
        return delay_unitDirtyFlag ;
    }

    /**
     * 获取 [INITIAL_RES_MODEL_ID]
     */
    @JsonProperty("initial_res_model_id")
    public Integer getInitial_res_model_id(){
        return initial_res_model_id ;
    }

    /**
     * 设置 [INITIAL_RES_MODEL_ID]
     */
    @JsonProperty("initial_res_model_id")
    public void setInitial_res_model_id(Integer  initial_res_model_id){
        this.initial_res_model_id = initial_res_model_id ;
        this.initial_res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [INITIAL_RES_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getInitial_res_model_idDirtyFlag(){
        return initial_res_model_idDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_NEXT_TYPE_ID_TEXT]
     */
    @JsonProperty("default_next_type_id_text")
    public String getDefault_next_type_id_text(){
        return default_next_type_id_text ;
    }

    /**
     * 设置 [DEFAULT_NEXT_TYPE_ID_TEXT]
     */
    @JsonProperty("default_next_type_id_text")
    public void setDefault_next_type_id_text(String  default_next_type_id_text){
        this.default_next_type_id_text = default_next_type_id_text ;
        this.default_next_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_NEXT_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDefault_next_type_id_textDirtyFlag(){
        return default_next_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_NEXT_TYPE_ID]
     */
    @JsonProperty("default_next_type_id")
    public Integer getDefault_next_type_id(){
        return default_next_type_id ;
    }

    /**
     * 设置 [DEFAULT_NEXT_TYPE_ID]
     */
    @JsonProperty("default_next_type_id")
    public void setDefault_next_type_id(Integer  default_next_type_id){
        this.default_next_type_id = default_next_type_id ;
        this.default_next_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_NEXT_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_next_type_idDirtyFlag(){
        return default_next_type_idDirtyFlag ;
    }



    public Mail_activity_type toDO() {
        Mail_activity_type srfdomain = new Mail_activity_type();
        if(getRes_model_changeDirtyFlag())
            srfdomain.setRes_model_change(res_model_change);
        if(getSummaryDirtyFlag())
            srfdomain.setSummary(summary);
        if(getForce_nextDirtyFlag())
            srfdomain.setForce_next(force_next);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCategoryDirtyFlag())
            srfdomain.setCategory(category);
        if(getIconDirtyFlag())
            srfdomain.setIcon(icon);
        if(getDelay_countDirtyFlag())
            srfdomain.setDelay_count(delay_count);
        if(getPrevious_type_idsDirtyFlag())
            srfdomain.setPrevious_type_ids(previous_type_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getDelay_fromDirtyFlag())
            srfdomain.setDelay_from(delay_from);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMail_template_idsDirtyFlag())
            srfdomain.setMail_template_ids(mail_template_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getRes_model_idDirtyFlag())
            srfdomain.setRes_model_id(res_model_id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDecoration_typeDirtyFlag())
            srfdomain.setDecoration_type(decoration_type);
        if(getNext_type_idsDirtyFlag())
            srfdomain.setNext_type_ids(next_type_ids);
        if(getDelay_unitDirtyFlag())
            srfdomain.setDelay_unit(delay_unit);
        if(getInitial_res_model_idDirtyFlag())
            srfdomain.setInitial_res_model_id(initial_res_model_id);
        if(getDefault_next_type_id_textDirtyFlag())
            srfdomain.setDefault_next_type_id_text(default_next_type_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getDefault_next_type_idDirtyFlag())
            srfdomain.setDefault_next_type_id(default_next_type_id);

        return srfdomain;
    }

    public void fromDO(Mail_activity_type srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getRes_model_changeDirtyFlag())
            this.setRes_model_change(srfdomain.getRes_model_change());
        if(srfdomain.getSummaryDirtyFlag())
            this.setSummary(srfdomain.getSummary());
        if(srfdomain.getForce_nextDirtyFlag())
            this.setForce_next(srfdomain.getForce_next());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCategoryDirtyFlag())
            this.setCategory(srfdomain.getCategory());
        if(srfdomain.getIconDirtyFlag())
            this.setIcon(srfdomain.getIcon());
        if(srfdomain.getDelay_countDirtyFlag())
            this.setDelay_count(srfdomain.getDelay_count());
        if(srfdomain.getPrevious_type_idsDirtyFlag())
            this.setPrevious_type_ids(srfdomain.getPrevious_type_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getDelay_fromDirtyFlag())
            this.setDelay_from(srfdomain.getDelay_from());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMail_template_idsDirtyFlag())
            this.setMail_template_ids(srfdomain.getMail_template_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getRes_model_idDirtyFlag())
            this.setRes_model_id(srfdomain.getRes_model_id());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDecoration_typeDirtyFlag())
            this.setDecoration_type(srfdomain.getDecoration_type());
        if(srfdomain.getNext_type_idsDirtyFlag())
            this.setNext_type_ids(srfdomain.getNext_type_ids());
        if(srfdomain.getDelay_unitDirtyFlag())
            this.setDelay_unit(srfdomain.getDelay_unit());
        if(srfdomain.getInitial_res_model_idDirtyFlag())
            this.setInitial_res_model_id(srfdomain.getInitial_res_model_id());
        if(srfdomain.getDefault_next_type_id_textDirtyFlag())
            this.setDefault_next_type_id_text(srfdomain.getDefault_next_type_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getDefault_next_type_idDirtyFlag())
            this.setDefault_next_type_id(srfdomain.getDefault_next_type_id());

    }

    public List<Mail_activity_typeDTO> fromDOPage(List<Mail_activity_type> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_activity_typeDTO> dtos=new ArrayList<Mail_activity_typeDTO>();
        for(Mail_activity_type domain : poPage) {
            Mail_activity_typeDTO dto = new Mail_activity_typeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

