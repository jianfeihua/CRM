package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Account_invoice_lineDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice_line" })
@RestController
@RequestMapping("")
public class Account_invoice_lineResource {

    @Autowired
    private IAccount_invoice_lineService account_invoice_lineService;

    public IAccount_invoice_lineService getAccount_invoice_lineService() {
        return this.account_invoice_lineService;
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoice_lines/{account_invoice_line_id}")

    public ResponseEntity<Account_invoice_lineDTO> update(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_lineDTO account_invoice_linedto) {
		Account_invoice_line domain = account_invoice_linedto.toDO();
        domain.setId(account_invoice_line_id);
		account_invoice_lineService.update(domain);
		Account_invoice_lineDTO dto = new Account_invoice_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Account_invoice_line" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoice_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice_line(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoice_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoice_lines/{account_invoice_line_id}")
    public ResponseEntity<Account_invoice_lineDTO> get(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) {
        Account_invoice_lineDTO dto = new Account_invoice_lineDTO();
        Account_invoice_line domain = account_invoice_lineService.get(account_invoice_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Account_invoice_line" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoice_lines/{account_invoice_line_id}/getdraft")

    public ResponseEntity<Account_invoice_lineDTO> getDraft(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        Account_invoice_line account_invoice_line = account_invoice_linedto.toDO();
    	account_invoice_line = account_invoice_lineService.getDraft(account_invoice_line) ;
    	account_invoice_linedto.fromDO(account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_linedto);
    }

    @ApiOperation(value = "Save", tags = {"Account_invoice_line" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines/{account_invoice_line_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        Account_invoice_line account_invoice_line = account_invoice_linedto.toDO();
    	Boolean b = account_invoice_lineService.save(account_invoice_line) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoice_lines/{account_invoice_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) {
        Account_invoice_lineDTO account_invoice_linedto = new Account_invoice_lineDTO();
		Account_invoice_line domain = new Account_invoice_line();
		account_invoice_linedto.setId(account_invoice_line_id);
		domain.setId(account_invoice_line_id);
        Boolean rst = account_invoice_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines")

    public ResponseEntity<Account_invoice_lineDTO> create(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        Account_invoice_lineDTO dto = new Account_invoice_lineDTO();
        Account_invoice_line domain = account_invoice_linedto.toDO();
		account_invoice_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/account_invoice_lines/fetchdefault")
	public ResponseEntity<Page<Account_invoice_lineDTO>> fetchDefault(Account_invoice_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoice_lineDTO> list = new ArrayList<Account_invoice_lineDTO>();
        
        Page<Account_invoice_line> domains = account_invoice_lineService.searchDefault(context) ;
        for(Account_invoice_line account_invoice_line : domains.getContent()){
            Account_invoice_lineDTO dto = new Account_invoice_lineDTO();
            dto.fromDO(account_invoice_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
