package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_payment.valuerule.anno.payment_acquirer.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Payment_acquirerDTO]
 */
public class Payment_acquirerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FEES_IMPLEMENTED]
     *
     */
    @Payment_acquirerFees_implementedDefault(info = "默认规则")
    private String fees_implemented;

    @JsonIgnore
    private boolean fees_implementedDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Payment_acquirerDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Payment_acquirerImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [DONE_MSG]
     *
     */
    @Payment_acquirerDone_msgDefault(info = "默认规则")
    private String done_msg;

    @JsonIgnore
    private boolean done_msgDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Payment_acquirerDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PAYMENT_ICON_IDS]
     *
     */
    @Payment_acquirerPayment_icon_idsDefault(info = "默认规则")
    private String payment_icon_ids;

    @JsonIgnore
    private boolean payment_icon_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Payment_acquirerWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Payment_acquirerWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Payment_acquirerCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [FEES_INT_VAR]
     *
     */
    @Payment_acquirerFees_int_varDefault(info = "默认规则")
    private Double fees_int_var;

    @JsonIgnore
    private boolean fees_int_varDirtyFlag;

    /**
     * 属性 [FEES_ACTIVE]
     *
     */
    @Payment_acquirerFees_activeDefault(info = "默认规则")
    private String fees_active;

    @JsonIgnore
    private boolean fees_activeDirtyFlag;

    /**
     * 属性 [REGISTRATION_VIEW_TEMPLATE_ID]
     *
     */
    @Payment_acquirerRegistration_view_template_idDefault(info = "默认规则")
    private Integer registration_view_template_id;

    @JsonIgnore
    private boolean registration_view_template_idDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Payment_acquirerWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [POST_MSG]
     *
     */
    @Payment_acquirerPost_msgDefault(info = "默认规则")
    private String post_msg;

    @JsonIgnore
    private boolean post_msgDirtyFlag;

    /**
     * 属性 [ENVIRONMENT]
     *
     */
    @Payment_acquirerEnvironmentDefault(info = "默认规则")
    private String environment;

    @JsonIgnore
    private boolean environmentDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Payment_acquirerSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Payment_acquirerImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [TOKEN_IMPLEMENTED]
     *
     */
    @Payment_acquirerToken_implementedDefault(info = "默认规则")
    private String token_implemented;

    @JsonIgnore
    private boolean token_implementedDirtyFlag;

    /**
     * 属性 [FEES_DOM_FIXED]
     *
     */
    @Payment_acquirerFees_dom_fixedDefault(info = "默认规则")
    private Double fees_dom_fixed;

    @JsonIgnore
    private boolean fees_dom_fixedDirtyFlag;

    /**
     * 属性 [FEES_INT_FIXED]
     *
     */
    @Payment_acquirerFees_int_fixedDefault(info = "默认规则")
    private Double fees_int_fixed;

    @JsonIgnore
    private boolean fees_int_fixedDirtyFlag;

    /**
     * 属性 [MODULE_ID]
     *
     */
    @Payment_acquirerModule_idDefault(info = "默认规则")
    private Integer module_id;

    @JsonIgnore
    private boolean module_idDirtyFlag;

    /**
     * 属性 [AUTHORIZE_IMPLEMENTED]
     *
     */
    @Payment_acquirerAuthorize_implementedDefault(info = "默认规则")
    private String authorize_implemented;

    @JsonIgnore
    private boolean authorize_implementedDirtyFlag;

    /**
     * 属性 [PAYMENT_FLOW]
     *
     */
    @Payment_acquirerPayment_flowDefault(info = "默认规则")
    private String payment_flow;

    @JsonIgnore
    private boolean payment_flowDirtyFlag;

    /**
     * 属性 [SPECIFIC_COUNTRIES]
     *
     */
    @Payment_acquirerSpecific_countriesDefault(info = "默认规则")
    private String specific_countries;

    @JsonIgnore
    private boolean specific_countriesDirtyFlag;

    /**
     * 属性 [FEES_DOM_VAR]
     *
     */
    @Payment_acquirerFees_dom_varDefault(info = "默认规则")
    private Double fees_dom_var;

    @JsonIgnore
    private boolean fees_dom_varDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Payment_acquirerIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Payment_acquirerNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRE_MSG]
     *
     */
    @Payment_acquirerPre_msgDefault(info = "默认规则")
    private String pre_msg;

    @JsonIgnore
    private boolean pre_msgDirtyFlag;

    /**
     * 属性 [COUNTRY_IDS]
     *
     */
    @Payment_acquirerCountry_idsDefault(info = "默认规则")
    private String country_ids;

    @JsonIgnore
    private boolean country_idsDirtyFlag;

    /**
     * 属性 [QR_CODE]
     *
     */
    @Payment_acquirerQr_codeDefault(info = "默认规则")
    private String qr_code;

    @JsonIgnore
    private boolean qr_codeDirtyFlag;

    /**
     * 属性 [PENDING_MSG]
     *
     */
    @Payment_acquirerPending_msgDefault(info = "默认规则")
    private String pending_msg;

    @JsonIgnore
    private boolean pending_msgDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Payment_acquirerImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [VIEW_TEMPLATE_ID]
     *
     */
    @Payment_acquirerView_template_idDefault(info = "默认规则")
    private Integer view_template_id;

    @JsonIgnore
    private boolean view_template_idDirtyFlag;

    /**
     * 属性 [INBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @Payment_acquirerInbound_payment_method_idsDefault(info = "默认规则")
    private String inbound_payment_method_ids;

    @JsonIgnore
    private boolean inbound_payment_method_idsDirtyFlag;

    /**
     * 属性 [SO_REFERENCE_TYPE]
     *
     */
    @Payment_acquirerSo_reference_typeDefault(info = "默认规则")
    private String so_reference_type;

    @JsonIgnore
    private boolean so_reference_typeDirtyFlag;

    /**
     * 属性 [MODULE_STATE]
     *
     */
    @Payment_acquirerModule_stateDefault(info = "默认规则")
    private String module_state;

    @JsonIgnore
    private boolean module_stateDirtyFlag;

    /**
     * 属性 [CANCEL_MSG]
     *
     */
    @Payment_acquirerCancel_msgDefault(info = "默认规则")
    private String cancel_msg;

    @JsonIgnore
    private boolean cancel_msgDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Payment_acquirer__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PROVIDER]
     *
     */
    @Payment_acquirerProviderDefault(info = "默认规则")
    private String provider;

    @JsonIgnore
    private boolean providerDirtyFlag;

    /**
     * 属性 [SAVE_TOKEN]
     *
     */
    @Payment_acquirerSave_tokenDefault(info = "默认规则")
    private String save_token;

    @JsonIgnore
    private boolean save_tokenDirtyFlag;

    /**
     * 属性 [CAPTURE_MANUALLY]
     *
     */
    @Payment_acquirerCapture_manuallyDefault(info = "默认规则")
    private String capture_manually;

    @JsonIgnore
    private boolean capture_manuallyDirtyFlag;

    /**
     * 属性 [ERROR_MSG]
     *
     */
    @Payment_acquirerError_msgDefault(info = "默认规则")
    private String error_msg;

    @JsonIgnore
    private boolean error_msgDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Payment_acquirerWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Payment_acquirerCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Payment_acquirerCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Payment_acquirerJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Payment_acquirerJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Payment_acquirerWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Payment_acquirerCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Payment_acquirerCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [FEES_IMPLEMENTED]
     */
    @JsonProperty("fees_implemented")
    public String getFees_implemented(){
        return fees_implemented ;
    }

    /**
     * 设置 [FEES_IMPLEMENTED]
     */
    @JsonProperty("fees_implemented")
    public void setFees_implemented(String  fees_implemented){
        this.fees_implemented = fees_implemented ;
        this.fees_implementedDirtyFlag = true ;
    }

    /**
     * 获取 [FEES_IMPLEMENTED]脏标记
     */
    @JsonIgnore
    public boolean getFees_implementedDirtyFlag(){
        return fees_implementedDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [DONE_MSG]
     */
    @JsonProperty("done_msg")
    public String getDone_msg(){
        return done_msg ;
    }

    /**
     * 设置 [DONE_MSG]
     */
    @JsonProperty("done_msg")
    public void setDone_msg(String  done_msg){
        this.done_msg = done_msg ;
        this.done_msgDirtyFlag = true ;
    }

    /**
     * 获取 [DONE_MSG]脏标记
     */
    @JsonIgnore
    public boolean getDone_msgDirtyFlag(){
        return done_msgDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ICON_IDS]
     */
    @JsonProperty("payment_icon_ids")
    public String getPayment_icon_ids(){
        return payment_icon_ids ;
    }

    /**
     * 设置 [PAYMENT_ICON_IDS]
     */
    @JsonProperty("payment_icon_ids")
    public void setPayment_icon_ids(String  payment_icon_ids){
        this.payment_icon_ids = payment_icon_ids ;
        this.payment_icon_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ICON_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPayment_icon_idsDirtyFlag(){
        return payment_icon_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [FEES_INT_VAR]
     */
    @JsonProperty("fees_int_var")
    public Double getFees_int_var(){
        return fees_int_var ;
    }

    /**
     * 设置 [FEES_INT_VAR]
     */
    @JsonProperty("fees_int_var")
    public void setFees_int_var(Double  fees_int_var){
        this.fees_int_var = fees_int_var ;
        this.fees_int_varDirtyFlag = true ;
    }

    /**
     * 获取 [FEES_INT_VAR]脏标记
     */
    @JsonIgnore
    public boolean getFees_int_varDirtyFlag(){
        return fees_int_varDirtyFlag ;
    }

    /**
     * 获取 [FEES_ACTIVE]
     */
    @JsonProperty("fees_active")
    public String getFees_active(){
        return fees_active ;
    }

    /**
     * 设置 [FEES_ACTIVE]
     */
    @JsonProperty("fees_active")
    public void setFees_active(String  fees_active){
        this.fees_active = fees_active ;
        this.fees_activeDirtyFlag = true ;
    }

    /**
     * 获取 [FEES_ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getFees_activeDirtyFlag(){
        return fees_activeDirtyFlag ;
    }

    /**
     * 获取 [REGISTRATION_VIEW_TEMPLATE_ID]
     */
    @JsonProperty("registration_view_template_id")
    public Integer getRegistration_view_template_id(){
        return registration_view_template_id ;
    }

    /**
     * 设置 [REGISTRATION_VIEW_TEMPLATE_ID]
     */
    @JsonProperty("registration_view_template_id")
    public void setRegistration_view_template_id(Integer  registration_view_template_id){
        this.registration_view_template_id = registration_view_template_id ;
        this.registration_view_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [REGISTRATION_VIEW_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_view_template_idDirtyFlag(){
        return registration_view_template_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [POST_MSG]
     */
    @JsonProperty("post_msg")
    public String getPost_msg(){
        return post_msg ;
    }

    /**
     * 设置 [POST_MSG]
     */
    @JsonProperty("post_msg")
    public void setPost_msg(String  post_msg){
        this.post_msg = post_msg ;
        this.post_msgDirtyFlag = true ;
    }

    /**
     * 获取 [POST_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPost_msgDirtyFlag(){
        return post_msgDirtyFlag ;
    }

    /**
     * 获取 [ENVIRONMENT]
     */
    @JsonProperty("environment")
    public String getEnvironment(){
        return environment ;
    }

    /**
     * 设置 [ENVIRONMENT]
     */
    @JsonProperty("environment")
    public void setEnvironment(String  environment){
        this.environment = environment ;
        this.environmentDirtyFlag = true ;
    }

    /**
     * 获取 [ENVIRONMENT]脏标记
     */
    @JsonIgnore
    public boolean getEnvironmentDirtyFlag(){
        return environmentDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [TOKEN_IMPLEMENTED]
     */
    @JsonProperty("token_implemented")
    public String getToken_implemented(){
        return token_implemented ;
    }

    /**
     * 设置 [TOKEN_IMPLEMENTED]
     */
    @JsonProperty("token_implemented")
    public void setToken_implemented(String  token_implemented){
        this.token_implemented = token_implemented ;
        this.token_implementedDirtyFlag = true ;
    }

    /**
     * 获取 [TOKEN_IMPLEMENTED]脏标记
     */
    @JsonIgnore
    public boolean getToken_implementedDirtyFlag(){
        return token_implementedDirtyFlag ;
    }

    /**
     * 获取 [FEES_DOM_FIXED]
     */
    @JsonProperty("fees_dom_fixed")
    public Double getFees_dom_fixed(){
        return fees_dom_fixed ;
    }

    /**
     * 设置 [FEES_DOM_FIXED]
     */
    @JsonProperty("fees_dom_fixed")
    public void setFees_dom_fixed(Double  fees_dom_fixed){
        this.fees_dom_fixed = fees_dom_fixed ;
        this.fees_dom_fixedDirtyFlag = true ;
    }

    /**
     * 获取 [FEES_DOM_FIXED]脏标记
     */
    @JsonIgnore
    public boolean getFees_dom_fixedDirtyFlag(){
        return fees_dom_fixedDirtyFlag ;
    }

    /**
     * 获取 [FEES_INT_FIXED]
     */
    @JsonProperty("fees_int_fixed")
    public Double getFees_int_fixed(){
        return fees_int_fixed ;
    }

    /**
     * 设置 [FEES_INT_FIXED]
     */
    @JsonProperty("fees_int_fixed")
    public void setFees_int_fixed(Double  fees_int_fixed){
        this.fees_int_fixed = fees_int_fixed ;
        this.fees_int_fixedDirtyFlag = true ;
    }

    /**
     * 获取 [FEES_INT_FIXED]脏标记
     */
    @JsonIgnore
    public boolean getFees_int_fixedDirtyFlag(){
        return fees_int_fixedDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ID]
     */
    @JsonProperty("module_id")
    public Integer getModule_id(){
        return module_id ;
    }

    /**
     * 设置 [MODULE_ID]
     */
    @JsonProperty("module_id")
    public void setModule_id(Integer  module_id){
        this.module_id = module_id ;
        this.module_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ID]脏标记
     */
    @JsonIgnore
    public boolean getModule_idDirtyFlag(){
        return module_idDirtyFlag ;
    }

    /**
     * 获取 [AUTHORIZE_IMPLEMENTED]
     */
    @JsonProperty("authorize_implemented")
    public String getAuthorize_implemented(){
        return authorize_implemented ;
    }

    /**
     * 设置 [AUTHORIZE_IMPLEMENTED]
     */
    @JsonProperty("authorize_implemented")
    public void setAuthorize_implemented(String  authorize_implemented){
        this.authorize_implemented = authorize_implemented ;
        this.authorize_implementedDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHORIZE_IMPLEMENTED]脏标记
     */
    @JsonIgnore
    public boolean getAuthorize_implementedDirtyFlag(){
        return authorize_implementedDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_FLOW]
     */
    @JsonProperty("payment_flow")
    public String getPayment_flow(){
        return payment_flow ;
    }

    /**
     * 设置 [PAYMENT_FLOW]
     */
    @JsonProperty("payment_flow")
    public void setPayment_flow(String  payment_flow){
        this.payment_flow = payment_flow ;
        this.payment_flowDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_FLOW]脏标记
     */
    @JsonIgnore
    public boolean getPayment_flowDirtyFlag(){
        return payment_flowDirtyFlag ;
    }

    /**
     * 获取 [SPECIFIC_COUNTRIES]
     */
    @JsonProperty("specific_countries")
    public String getSpecific_countries(){
        return specific_countries ;
    }

    /**
     * 设置 [SPECIFIC_COUNTRIES]
     */
    @JsonProperty("specific_countries")
    public void setSpecific_countries(String  specific_countries){
        this.specific_countries = specific_countries ;
        this.specific_countriesDirtyFlag = true ;
    }

    /**
     * 获取 [SPECIFIC_COUNTRIES]脏标记
     */
    @JsonIgnore
    public boolean getSpecific_countriesDirtyFlag(){
        return specific_countriesDirtyFlag ;
    }

    /**
     * 获取 [FEES_DOM_VAR]
     */
    @JsonProperty("fees_dom_var")
    public Double getFees_dom_var(){
        return fees_dom_var ;
    }

    /**
     * 设置 [FEES_DOM_VAR]
     */
    @JsonProperty("fees_dom_var")
    public void setFees_dom_var(Double  fees_dom_var){
        this.fees_dom_var = fees_dom_var ;
        this.fees_dom_varDirtyFlag = true ;
    }

    /**
     * 获取 [FEES_DOM_VAR]脏标记
     */
    @JsonIgnore
    public boolean getFees_dom_varDirtyFlag(){
        return fees_dom_varDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRE_MSG]
     */
    @JsonProperty("pre_msg")
    public String getPre_msg(){
        return pre_msg ;
    }

    /**
     * 设置 [PRE_MSG]
     */
    @JsonProperty("pre_msg")
    public void setPre_msg(String  pre_msg){
        this.pre_msg = pre_msg ;
        this.pre_msgDirtyFlag = true ;
    }

    /**
     * 获取 [PRE_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPre_msgDirtyFlag(){
        return pre_msgDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_IDS]
     */
    @JsonProperty("country_ids")
    public String getCountry_ids(){
        return country_ids ;
    }

    /**
     * 设置 [COUNTRY_IDS]
     */
    @JsonProperty("country_ids")
    public void setCountry_ids(String  country_ids){
        this.country_ids = country_ids ;
        this.country_idsDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idsDirtyFlag(){
        return country_idsDirtyFlag ;
    }

    /**
     * 获取 [QR_CODE]
     */
    @JsonProperty("qr_code")
    public String getQr_code(){
        return qr_code ;
    }

    /**
     * 设置 [QR_CODE]
     */
    @JsonProperty("qr_code")
    public void setQr_code(String  qr_code){
        this.qr_code = qr_code ;
        this.qr_codeDirtyFlag = true ;
    }

    /**
     * 获取 [QR_CODE]脏标记
     */
    @JsonIgnore
    public boolean getQr_codeDirtyFlag(){
        return qr_codeDirtyFlag ;
    }

    /**
     * 获取 [PENDING_MSG]
     */
    @JsonProperty("pending_msg")
    public String getPending_msg(){
        return pending_msg ;
    }

    /**
     * 设置 [PENDING_MSG]
     */
    @JsonProperty("pending_msg")
    public void setPending_msg(String  pending_msg){
        this.pending_msg = pending_msg ;
        this.pending_msgDirtyFlag = true ;
    }

    /**
     * 获取 [PENDING_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPending_msgDirtyFlag(){
        return pending_msgDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [VIEW_TEMPLATE_ID]
     */
    @JsonProperty("view_template_id")
    public Integer getView_template_id(){
        return view_template_id ;
    }

    /**
     * 设置 [VIEW_TEMPLATE_ID]
     */
    @JsonProperty("view_template_id")
    public void setView_template_id(Integer  view_template_id){
        this.view_template_id = view_template_id ;
        this.view_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [VIEW_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getView_template_idDirtyFlag(){
        return view_template_idDirtyFlag ;
    }

    /**
     * 获取 [INBOUND_PAYMENT_METHOD_IDS]
     */
    @JsonProperty("inbound_payment_method_ids")
    public String getInbound_payment_method_ids(){
        return inbound_payment_method_ids ;
    }

    /**
     * 设置 [INBOUND_PAYMENT_METHOD_IDS]
     */
    @JsonProperty("inbound_payment_method_ids")
    public void setInbound_payment_method_ids(String  inbound_payment_method_ids){
        this.inbound_payment_method_ids = inbound_payment_method_ids ;
        this.inbound_payment_method_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INBOUND_PAYMENT_METHOD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInbound_payment_method_idsDirtyFlag(){
        return inbound_payment_method_idsDirtyFlag ;
    }

    /**
     * 获取 [SO_REFERENCE_TYPE]
     */
    @JsonProperty("so_reference_type")
    public String getSo_reference_type(){
        return so_reference_type ;
    }

    /**
     * 设置 [SO_REFERENCE_TYPE]
     */
    @JsonProperty("so_reference_type")
    public void setSo_reference_type(String  so_reference_type){
        this.so_reference_type = so_reference_type ;
        this.so_reference_typeDirtyFlag = true ;
    }

    /**
     * 获取 [SO_REFERENCE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getSo_reference_typeDirtyFlag(){
        return so_reference_typeDirtyFlag ;
    }

    /**
     * 获取 [MODULE_STATE]
     */
    @JsonProperty("module_state")
    public String getModule_state(){
        return module_state ;
    }

    /**
     * 设置 [MODULE_STATE]
     */
    @JsonProperty("module_state")
    public void setModule_state(String  module_state){
        this.module_state = module_state ;
        this.module_stateDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_STATE]脏标记
     */
    @JsonIgnore
    public boolean getModule_stateDirtyFlag(){
        return module_stateDirtyFlag ;
    }

    /**
     * 获取 [CANCEL_MSG]
     */
    @JsonProperty("cancel_msg")
    public String getCancel_msg(){
        return cancel_msg ;
    }

    /**
     * 设置 [CANCEL_MSG]
     */
    @JsonProperty("cancel_msg")
    public void setCancel_msg(String  cancel_msg){
        this.cancel_msg = cancel_msg ;
        this.cancel_msgDirtyFlag = true ;
    }

    /**
     * 获取 [CANCEL_MSG]脏标记
     */
    @JsonIgnore
    public boolean getCancel_msgDirtyFlag(){
        return cancel_msgDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PROVIDER]
     */
    @JsonProperty("provider")
    public String getProvider(){
        return provider ;
    }

    /**
     * 设置 [PROVIDER]
     */
    @JsonProperty("provider")
    public void setProvider(String  provider){
        this.provider = provider ;
        this.providerDirtyFlag = true ;
    }

    /**
     * 获取 [PROVIDER]脏标记
     */
    @JsonIgnore
    public boolean getProviderDirtyFlag(){
        return providerDirtyFlag ;
    }

    /**
     * 获取 [SAVE_TOKEN]
     */
    @JsonProperty("save_token")
    public String getSave_token(){
        return save_token ;
    }

    /**
     * 设置 [SAVE_TOKEN]
     */
    @JsonProperty("save_token")
    public void setSave_token(String  save_token){
        this.save_token = save_token ;
        this.save_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [SAVE_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getSave_tokenDirtyFlag(){
        return save_tokenDirtyFlag ;
    }

    /**
     * 获取 [CAPTURE_MANUALLY]
     */
    @JsonProperty("capture_manually")
    public String getCapture_manually(){
        return capture_manually ;
    }

    /**
     * 设置 [CAPTURE_MANUALLY]
     */
    @JsonProperty("capture_manually")
    public void setCapture_manually(String  capture_manually){
        this.capture_manually = capture_manually ;
        this.capture_manuallyDirtyFlag = true ;
    }

    /**
     * 获取 [CAPTURE_MANUALLY]脏标记
     */
    @JsonIgnore
    public boolean getCapture_manuallyDirtyFlag(){
        return capture_manuallyDirtyFlag ;
    }

    /**
     * 获取 [ERROR_MSG]
     */
    @JsonProperty("error_msg")
    public String getError_msg(){
        return error_msg ;
    }

    /**
     * 设置 [ERROR_MSG]
     */
    @JsonProperty("error_msg")
    public void setError_msg(String  error_msg){
        this.error_msg = error_msg ;
        this.error_msgDirtyFlag = true ;
    }

    /**
     * 获取 [ERROR_MSG]脏标记
     */
    @JsonIgnore
    public boolean getError_msgDirtyFlag(){
        return error_msgDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Payment_acquirer toDO() {
        Payment_acquirer srfdomain = new Payment_acquirer();
        if(getFees_implementedDirtyFlag())
            srfdomain.setFees_implemented(fees_implemented);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getDone_msgDirtyFlag())
            srfdomain.setDone_msg(done_msg);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getPayment_icon_idsDirtyFlag())
            srfdomain.setPayment_icon_ids(payment_icon_ids);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getFees_int_varDirtyFlag())
            srfdomain.setFees_int_var(fees_int_var);
        if(getFees_activeDirtyFlag())
            srfdomain.setFees_active(fees_active);
        if(getRegistration_view_template_idDirtyFlag())
            srfdomain.setRegistration_view_template_id(registration_view_template_id);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getPost_msgDirtyFlag())
            srfdomain.setPost_msg(post_msg);
        if(getEnvironmentDirtyFlag())
            srfdomain.setEnvironment(environment);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getToken_implementedDirtyFlag())
            srfdomain.setToken_implemented(token_implemented);
        if(getFees_dom_fixedDirtyFlag())
            srfdomain.setFees_dom_fixed(fees_dom_fixed);
        if(getFees_int_fixedDirtyFlag())
            srfdomain.setFees_int_fixed(fees_int_fixed);
        if(getModule_idDirtyFlag())
            srfdomain.setModule_id(module_id);
        if(getAuthorize_implementedDirtyFlag())
            srfdomain.setAuthorize_implemented(authorize_implemented);
        if(getPayment_flowDirtyFlag())
            srfdomain.setPayment_flow(payment_flow);
        if(getSpecific_countriesDirtyFlag())
            srfdomain.setSpecific_countries(specific_countries);
        if(getFees_dom_varDirtyFlag())
            srfdomain.setFees_dom_var(fees_dom_var);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPre_msgDirtyFlag())
            srfdomain.setPre_msg(pre_msg);
        if(getCountry_idsDirtyFlag())
            srfdomain.setCountry_ids(country_ids);
        if(getQr_codeDirtyFlag())
            srfdomain.setQr_code(qr_code);
        if(getPending_msgDirtyFlag())
            srfdomain.setPending_msg(pending_msg);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getView_template_idDirtyFlag())
            srfdomain.setView_template_id(view_template_id);
        if(getInbound_payment_method_idsDirtyFlag())
            srfdomain.setInbound_payment_method_ids(inbound_payment_method_ids);
        if(getSo_reference_typeDirtyFlag())
            srfdomain.setSo_reference_type(so_reference_type);
        if(getModule_stateDirtyFlag())
            srfdomain.setModule_state(module_state);
        if(getCancel_msgDirtyFlag())
            srfdomain.setCancel_msg(cancel_msg);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getProviderDirtyFlag())
            srfdomain.setProvider(provider);
        if(getSave_tokenDirtyFlag())
            srfdomain.setSave_token(save_token);
        if(getCapture_manuallyDirtyFlag())
            srfdomain.setCapture_manually(capture_manually);
        if(getError_msgDirtyFlag())
            srfdomain.setError_msg(error_msg);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Payment_acquirer srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getFees_implementedDirtyFlag())
            this.setFees_implemented(srfdomain.getFees_implemented());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getDone_msgDirtyFlag())
            this.setDone_msg(srfdomain.getDone_msg());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getPayment_icon_idsDirtyFlag())
            this.setPayment_icon_ids(srfdomain.getPayment_icon_ids());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getFees_int_varDirtyFlag())
            this.setFees_int_var(srfdomain.getFees_int_var());
        if(srfdomain.getFees_activeDirtyFlag())
            this.setFees_active(srfdomain.getFees_active());
        if(srfdomain.getRegistration_view_template_idDirtyFlag())
            this.setRegistration_view_template_id(srfdomain.getRegistration_view_template_id());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getPost_msgDirtyFlag())
            this.setPost_msg(srfdomain.getPost_msg());
        if(srfdomain.getEnvironmentDirtyFlag())
            this.setEnvironment(srfdomain.getEnvironment());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getToken_implementedDirtyFlag())
            this.setToken_implemented(srfdomain.getToken_implemented());
        if(srfdomain.getFees_dom_fixedDirtyFlag())
            this.setFees_dom_fixed(srfdomain.getFees_dom_fixed());
        if(srfdomain.getFees_int_fixedDirtyFlag())
            this.setFees_int_fixed(srfdomain.getFees_int_fixed());
        if(srfdomain.getModule_idDirtyFlag())
            this.setModule_id(srfdomain.getModule_id());
        if(srfdomain.getAuthorize_implementedDirtyFlag())
            this.setAuthorize_implemented(srfdomain.getAuthorize_implemented());
        if(srfdomain.getPayment_flowDirtyFlag())
            this.setPayment_flow(srfdomain.getPayment_flow());
        if(srfdomain.getSpecific_countriesDirtyFlag())
            this.setSpecific_countries(srfdomain.getSpecific_countries());
        if(srfdomain.getFees_dom_varDirtyFlag())
            this.setFees_dom_var(srfdomain.getFees_dom_var());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPre_msgDirtyFlag())
            this.setPre_msg(srfdomain.getPre_msg());
        if(srfdomain.getCountry_idsDirtyFlag())
            this.setCountry_ids(srfdomain.getCountry_ids());
        if(srfdomain.getQr_codeDirtyFlag())
            this.setQr_code(srfdomain.getQr_code());
        if(srfdomain.getPending_msgDirtyFlag())
            this.setPending_msg(srfdomain.getPending_msg());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getView_template_idDirtyFlag())
            this.setView_template_id(srfdomain.getView_template_id());
        if(srfdomain.getInbound_payment_method_idsDirtyFlag())
            this.setInbound_payment_method_ids(srfdomain.getInbound_payment_method_ids());
        if(srfdomain.getSo_reference_typeDirtyFlag())
            this.setSo_reference_type(srfdomain.getSo_reference_type());
        if(srfdomain.getModule_stateDirtyFlag())
            this.setModule_state(srfdomain.getModule_state());
        if(srfdomain.getCancel_msgDirtyFlag())
            this.setCancel_msg(srfdomain.getCancel_msg());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getProviderDirtyFlag())
            this.setProvider(srfdomain.getProvider());
        if(srfdomain.getSave_tokenDirtyFlag())
            this.setSave_token(srfdomain.getSave_token());
        if(srfdomain.getCapture_manuallyDirtyFlag())
            this.setCapture_manually(srfdomain.getCapture_manually());
        if(srfdomain.getError_msgDirtyFlag())
            this.setError_msg(srfdomain.getError_msg());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Payment_acquirerDTO> fromDOPage(List<Payment_acquirer> poPage)   {
        if(poPage == null)
            return null;
        List<Payment_acquirerDTO> dtos=new ArrayList<Payment_acquirerDTO>();
        for(Payment_acquirer domain : poPage) {
            Payment_acquirerDTO dto = new Payment_acquirerDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

