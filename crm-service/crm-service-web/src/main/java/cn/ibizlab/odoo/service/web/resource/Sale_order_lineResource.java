package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.service.web.dto.Sale_order_lineDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_lineSearchContext;

@Api(tags = {"Sale_order_line" })
@RestController
@RequestMapping("")
public class Sale_order_lineResource {

    @Autowired
    private ISale_order_lineService sale_order_lineService;

    public ISale_order_lineService getSale_order_lineService() {
        return this.sale_order_lineService;
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Sale_order_line" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/sale_order_lines/{sale_order_line_id}/getdraft")

    public ResponseEntity<Sale_order_lineDTO> getDraft(@PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_lineDTO sale_order_linedto) {
        Sale_order_line sale_order_line = sale_order_linedto.toDO();
    	sale_order_line = sale_order_lineService.getDraft(sale_order_line) ;
    	sale_order_linedto.fromDO(sale_order_line);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_linedto);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_order_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/sale_order_lines/{sale_order_line_id}")
    public ResponseEntity<Sale_order_lineDTO> get(@PathVariable("sale_order_line_id") Integer sale_order_line_id) {
        Sale_order_lineDTO dto = new Sale_order_lineDTO();
        Sale_order_line domain = sale_order_lineService.get(sale_order_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_order_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/sale_order_lines/{sale_order_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_line_id") Integer sale_order_line_id) {
        Sale_order_lineDTO sale_order_linedto = new Sale_order_lineDTO();
		Sale_order_line domain = new Sale_order_line();
		sale_order_linedto.setId(sale_order_line_id);
		domain.setId(sale_order_line_id);
        Boolean rst = sale_order_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_order_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_order_lines/createBatch")
    public ResponseEntity<Boolean> createBatchSale_order_line(@RequestBody List<Sale_order_lineDTO> sale_order_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_order_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/sale_order_lines/{sale_order_line_id}")

    public ResponseEntity<Sale_order_lineDTO> update(@PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_lineDTO sale_order_linedto) {
		Sale_order_line domain = sale_order_linedto.toDO();
        domain.setId(sale_order_line_id);
		sale_order_lineService.update(domain);
		Sale_order_lineDTO dto = new Sale_order_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_order_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_order_lines")

    public ResponseEntity<Sale_order_lineDTO> create(@RequestBody Sale_order_lineDTO sale_order_linedto) {
        Sale_order_lineDTO dto = new Sale_order_lineDTO();
        Sale_order_line domain = sale_order_linedto.toDO();
		sale_order_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Sale_order_line" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_order_lineDTO sale_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_order_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/sale_order_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_order_lineDTO> sale_order_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Save", tags = {"Sale_order_line" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_order_lines/{sale_order_line_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_order_lineDTO sale_order_linedto) {
        Sale_order_line sale_order_line = sale_order_linedto.toDO();
    	Boolean b = sale_order_lineService.save(sale_order_line) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_order_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/sale_order_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_lineDTO> sale_order_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_order_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/sale_order_lines/fetchdefault")
	public ResponseEntity<Page<Sale_order_lineDTO>> fetchDefault(Sale_order_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_order_lineDTO> list = new ArrayList<Sale_order_lineDTO>();
        
        Page<Sale_order_line> domains = sale_order_lineService.searchDefault(context) ;
        for(Sale_order_line sale_order_line : domains.getContent()){
            Sale_order_lineDTO dto = new Sale_order_lineDTO();
            dto.fromDO(sale_order_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
