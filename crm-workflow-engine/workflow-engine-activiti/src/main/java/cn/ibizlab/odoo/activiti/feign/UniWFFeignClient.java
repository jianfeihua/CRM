package cn.ibizlab.odoo.activiti.feign;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface UniWFFeignClient {
    /**
     * 获取下一步用户
     * @param jo
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/getwfusers")
    JSONObject getWFUsers(@RequestBody JSONObject jo);

    /**
     * 开始流程
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wfprocessstarted")
    JSONObject wfProcessStarted(@RequestBody JSONObject jo);

    /**
     * 流程结束
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wfprocesscompleted")
    JSONObject wfProcessCompleted(@RequestBody JSONObject jo) ;

    /**
     * 流程取消
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wfprocesscancelled")
    JSONObject wfProcessCancelled(@RequestBody JSONObject jo) ;

    /**
     * 步骤开始
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wfactivitystarted")
    JSONObject wfActivityStarted(@RequestBody JSONObject jo) ;

    /**
     * 步骤完成
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wfactivitycompleted")
    JSONObject wfActivityCompleted(@RequestBody JSONObject jo) ;

    /**
     * 任务创建
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wftaskcreated")
    JSONObject wfTaskCreated(@RequestBody JSONObject jo);

    /**
     * 任务分配
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wftaskassigned")
    JSONObject wfTaskAssigned(@RequestBody JSONObject jo) ;

    /**
     * 任务完成
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfenginecallbackservice/wftaskcpmoleted")
    JSONObject wfTaskCompleted(@RequestBody JSONObject jo);


}
