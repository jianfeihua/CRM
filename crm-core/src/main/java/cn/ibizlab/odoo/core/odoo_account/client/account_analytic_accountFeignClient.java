package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_accountSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_analytic_account] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-analytic-account", fallback = account_analytic_accountFallback.class)
public interface account_analytic_accountFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts")
    Account_analytic_account create(@RequestBody Account_analytic_account account_analytic_account);

    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts/batch")
    Boolean createBatch(@RequestBody List<Account_analytic_account> account_analytic_accounts);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_accounts/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_accounts/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts/searchdefault")
    Page<Account_analytic_account> searchDefault(@RequestBody Account_analytic_accountSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_accounts/{id}")
    Account_analytic_account get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_accounts/{id}")
    Account_analytic_account update(@PathVariable("id") Integer id,@RequestBody Account_analytic_account account_analytic_account);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_accounts/batch")
    Boolean updateBatch(@RequestBody List<Account_analytic_account> account_analytic_accounts);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_accounts/select")
    Page<Account_analytic_account> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_accounts/getdraft")
    Account_analytic_account getDraft();


}
