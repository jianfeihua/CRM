package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_requiredSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_char_required] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-char-required", fallback = base_import_tests_models_char_requiredFallback.class)
public interface base_import_tests_models_char_requiredFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_requireds/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_requireds/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds/searchdefault")
    Page<Base_import_tests_models_char_required> searchDefault(@RequestBody Base_import_tests_models_char_requiredSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds")
    Base_import_tests_models_char_required create(@RequestBody Base_import_tests_models_char_required base_import_tests_models_char_required);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_char_required> base_import_tests_models_char_requireds);



    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_requireds/{id}")
    Base_import_tests_models_char_required update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_char_required base_import_tests_models_char_required);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_requireds/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_char_required> base_import_tests_models_char_requireds);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_requireds/{id}")
    Base_import_tests_models_char_required get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_requireds/select")
    Page<Base_import_tests_models_char_required> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_requireds/getdraft")
    Base_import_tests_models_char_required getDraft();


}
