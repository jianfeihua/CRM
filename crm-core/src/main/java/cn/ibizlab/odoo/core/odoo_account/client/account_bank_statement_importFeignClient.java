package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_importSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_bank_statement_import] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-bank-statement-import", fallback = account_bank_statement_importFallback.class)
public interface account_bank_statement_importFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_imports/{id}")
    Account_bank_statement_import update(@PathVariable("id") Integer id,@RequestBody Account_bank_statement_import account_bank_statement_import);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_imports/batch")
    Boolean updateBatch(@RequestBody List<Account_bank_statement_import> account_bank_statement_imports);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_imports/{id}")
    Account_bank_statement_import get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports")
    Account_bank_statement_import create(@RequestBody Account_bank_statement_import account_bank_statement_import);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports/batch")
    Boolean createBatch(@RequestBody List<Account_bank_statement_import> account_bank_statement_imports);




    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_imports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_imports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports/searchdefault")
    Page<Account_bank_statement_import> searchDefault(@RequestBody Account_bank_statement_importSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_imports/select")
    Page<Account_bank_statement_import> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_imports/getdraft")
    Account_bank_statement_import getDraft();


}
