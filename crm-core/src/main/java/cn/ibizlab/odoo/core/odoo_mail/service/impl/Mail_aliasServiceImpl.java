package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_aliasService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_aliasFeignClient;

/**
 * 实体[EMail别名] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_aliasServiceImpl implements IMail_aliasService {

    @Autowired
    mail_aliasFeignClient mail_aliasFeignClient;


    @Override
    public Mail_alias getDraft(Mail_alias et) {
        et=mail_aliasFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mail_alias et) {
        Mail_alias rt = mail_aliasFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_alias> list){
        mail_aliasFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_aliasFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_aliasFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Mail_alias et) {
        Mail_alias rt = mail_aliasFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_alias> list){
        mail_aliasFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_alias get(Integer id) {
		Mail_alias et=mail_aliasFeignClient.get(id);
        if(et==null){
            et=new Mail_alias();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_alias> searchDefault(Mail_aliasSearchContext context) {
        Page<Mail_alias> mail_aliass=mail_aliasFeignClient.searchDefault(context);
        return mail_aliass;
    }


}


