package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_applicant] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-applicant", fallback = hr_applicantFallback.class)
public interface hr_applicantFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/hr_applicants/{id}")
    Hr_applicant get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicants/searchdefault")
    Page<Hr_applicant> searchDefault(@RequestBody Hr_applicantSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicants")
    Hr_applicant create(@RequestBody Hr_applicant hr_applicant);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicants/batch")
    Boolean createBatch(@RequestBody List<Hr_applicant> hr_applicants);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicants/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicants/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/hr_applicants/{id}")
    Hr_applicant update(@PathVariable("id") Integer id,@RequestBody Hr_applicant hr_applicant);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_applicants/batch")
    Boolean updateBatch(@RequestBody List<Hr_applicant> hr_applicants);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_applicants/select")
    Page<Hr_applicant> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_applicants/getdraft")
    Hr_applicant getDraft();


}
