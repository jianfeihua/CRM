package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_setup_bank_manual_configService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_setup_bank_manual_configFeignClient;

/**
 * 实体[银行设置通用配置] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_setup_bank_manual_configServiceImpl implements IAccount_setup_bank_manual_configService {

    @Autowired
    account_setup_bank_manual_configFeignClient account_setup_bank_manual_configFeignClient;


    @Override
    public boolean create(Account_setup_bank_manual_config et) {
        Account_setup_bank_manual_config rt = account_setup_bank_manual_configFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_setup_bank_manual_config> list){
        account_setup_bank_manual_configFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_setup_bank_manual_config et) {
        Account_setup_bank_manual_config rt = account_setup_bank_manual_configFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_setup_bank_manual_config> list){
        account_setup_bank_manual_configFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_setup_bank_manual_config getDraft(Account_setup_bank_manual_config et) {
        et=account_setup_bank_manual_configFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_setup_bank_manual_configFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_setup_bank_manual_configFeignClient.removeBatch(idList);
    }

    @Override
    public Account_setup_bank_manual_config get(Integer id) {
		Account_setup_bank_manual_config et=account_setup_bank_manual_configFeignClient.get(id);
        if(et==null){
            et=new Account_setup_bank_manual_config();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_setup_bank_manual_config> searchDefault(Account_setup_bank_manual_configSearchContext context) {
        Page<Account_setup_bank_manual_config> account_setup_bank_manual_configs=account_setup_bank_manual_configFeignClient.searchDefault(context);
        return account_setup_bank_manual_configs;
    }


}


