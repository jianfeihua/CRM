package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_term_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_payment_term_line] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-payment-term-line", fallback = account_payment_term_lineFallback.class)
public interface account_payment_term_lineFeignClient {




    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_term_lines/{id}")
    Account_payment_term_line get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines/searchdefault")
    Page<Account_payment_term_line> searchDefault(@RequestBody Account_payment_term_lineSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines")
    Account_payment_term_line create(@RequestBody Account_payment_term_line account_payment_term_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines/batch")
    Boolean createBatch(@RequestBody List<Account_payment_term_line> account_payment_term_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_payment_term_lines/{id}")
    Account_payment_term_line update(@PathVariable("id") Integer id,@RequestBody Account_payment_term_line account_payment_term_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_payment_term_lines/batch")
    Boolean updateBatch(@RequestBody List<Account_payment_term_line> account_payment_term_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_term_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_term_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_term_lines/select")
    Page<Account_payment_term_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_term_lines/getdraft")
    Account_payment_term_line getDraft();


}
