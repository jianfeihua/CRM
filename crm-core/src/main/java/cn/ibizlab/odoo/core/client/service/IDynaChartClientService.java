package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.IDynaChart;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[DynaChart] 服务对象接口
 */
public interface IDynaChartClientService{

    public IDynaChart createModel() ;

    public Page<IDynaChart> select(SearchContext context);

    public void get(IDynaChart dynachart);

    public void getDraft(IDynaChart dynachart);

    public void checkKey(IDynaChart dynachart);

    public void update(IDynaChart dynachart);

    public void remove(IDynaChart dynachart);

    public void create(IDynaChart dynachart);

    public void save(IDynaChart dynachart);

    public Page<IDynaChart> fetchDefault(SearchContext context);

}
