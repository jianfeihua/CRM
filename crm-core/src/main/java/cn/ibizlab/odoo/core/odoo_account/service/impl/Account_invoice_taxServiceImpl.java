package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_taxService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_taxFeignClient;

/**
 * 实体[发票税率] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_taxServiceImpl implements IAccount_invoice_taxService {

    @Autowired
    account_invoice_taxFeignClient account_invoice_taxFeignClient;


    @Override
    public Account_invoice_tax getDraft(Account_invoice_tax et) {
        et=account_invoice_taxFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_invoice_tax et) {
        Account_invoice_tax rt = account_invoice_taxFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice_tax> list){
        account_invoice_taxFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_invoice_tax get(Integer id) {
		Account_invoice_tax et=account_invoice_taxFeignClient.get(id);
        if(et==null){
            et=new Account_invoice_tax();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_invoice_tax et) {
        Account_invoice_tax rt = account_invoice_taxFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_tax> list){
        account_invoice_taxFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoice_taxFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoice_taxFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context) {
        Page<Account_invoice_tax> account_invoice_taxs=account_invoice_taxFeignClient.searchDefault(context);
        return account_invoice_taxs;
    }


}


