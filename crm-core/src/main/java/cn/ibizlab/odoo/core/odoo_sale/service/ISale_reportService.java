package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_reportSearchContext;


/**
 * 实体[Sale_report] 服务对象接口
 */
public interface ISale_reportService{

    Sale_report get(Integer key) ;
    boolean create(Sale_report et) ;
    void createBatch(List<Sale_report> list) ;
    Sale_report getDraft(Sale_report et) ;
    boolean update(Sale_report et) ;
    void updateBatch(List<Sale_report> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Sale_report> searchDefault(Sale_reportSearchContext context) ;

}



