package cn.ibizlab.odoo.core.odoo_base_import.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_preview;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_previewSearchContext;


/**
 * 实体[Base_import_tests_models_preview] 服务对象接口
 */
public interface IBase_import_tests_models_previewService{

    Base_import_tests_models_preview get(Integer key) ;
    Base_import_tests_models_preview getDraft(Base_import_tests_models_preview et) ;
    boolean update(Base_import_tests_models_preview et) ;
    void updateBatch(List<Base_import_tests_models_preview> list) ;
    boolean create(Base_import_tests_models_preview et) ;
    void createBatch(List<Base_import_tests_models_preview> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_import_tests_models_preview> searchDefault(Base_import_tests_models_previewSearchContext context) ;

}



