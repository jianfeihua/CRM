package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_orderFeignClient;

/**
 * 实体[采购订单] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_orderServiceImpl implements IPurchase_orderService {

    @Autowired
    purchase_orderFeignClient purchase_orderFeignClient;


    @Override
    public Purchase_order get(Integer id) {
		Purchase_order et=purchase_orderFeignClient.get(id);
        if(et==null){
            et=new Purchase_order();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=purchase_orderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        purchase_orderFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Purchase_order et) {
        Purchase_order rt = purchase_orderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Purchase_order> list){
        purchase_orderFeignClient.updateBatch(list) ;
    }

    @Override
    public Purchase_order getDraft(Purchase_order et) {
        et=purchase_orderFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Purchase_order et) {
        Purchase_order rt = purchase_orderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_order> list){
        purchase_orderFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_order> searchDefault(Purchase_orderSearchContext context) {
        Page<Purchase_order> purchase_orders=purchase_orderFeignClient.searchDefault(context);
        return purchase_orders;
    }


}


