package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_templateSearchContext;


/**
 * 实体[Account_fiscal_position_template] 服务对象接口
 */
public interface IAccount_fiscal_position_templateService{

    boolean create(Account_fiscal_position_template et) ;
    void createBatch(List<Account_fiscal_position_template> list) ;
    boolean update(Account_fiscal_position_template et) ;
    void updateBatch(List<Account_fiscal_position_template> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_fiscal_position_template getDraft(Account_fiscal_position_template et) ;
    Account_fiscal_position_template get(Integer key) ;
    Page<Account_fiscal_position_template> searchDefault(Account_fiscal_position_templateSearchContext context) ;

}



