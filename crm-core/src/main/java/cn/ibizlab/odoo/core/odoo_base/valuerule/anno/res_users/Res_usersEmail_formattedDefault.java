package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_users;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_users.Res_usersEmail_formattedDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_users
 * 属性：Email_formatted
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_usersEmail_formattedDefaultValidator.class})
public @interface Res_usersEmail_formattedDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
