package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_country;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_country] 服务对象接口
 */
public interface Ires_countryClientService{

    public Ires_country createModel() ;

    public void update(Ires_country res_country);

    public void updateBatch(List<Ires_country> res_countries);

    public void createBatch(List<Ires_country> res_countries);

    public void create(Ires_country res_country);

    public void removeBatch(List<Ires_country> res_countries);

    public Page<Ires_country> fetchDefault(SearchContext context);

    public void remove(Ires_country res_country);

    public void get(Ires_country res_country);

    public Page<Ires_country> select(SearchContext context);

    public void getDraft(Ires_country res_country);

}
