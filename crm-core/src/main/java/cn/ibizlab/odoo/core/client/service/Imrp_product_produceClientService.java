package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_product_produce;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_product_produce] 服务对象接口
 */
public interface Imrp_product_produceClientService{

    public Imrp_product_produce createModel() ;

    public void createBatch(List<Imrp_product_produce> mrp_product_produces);

    public Page<Imrp_product_produce> fetchDefault(SearchContext context);

    public void removeBatch(List<Imrp_product_produce> mrp_product_produces);

    public void updateBatch(List<Imrp_product_produce> mrp_product_produces);

    public void get(Imrp_product_produce mrp_product_produce);

    public void remove(Imrp_product_produce mrp_product_produce);

    public void create(Imrp_product_produce mrp_product_produce);

    public void update(Imrp_product_produce mrp_product_produce);

    public Page<Imrp_product_produce> select(SearchContext context);

    public void getDraft(Imrp_product_produce mrp_product_produce);

}
