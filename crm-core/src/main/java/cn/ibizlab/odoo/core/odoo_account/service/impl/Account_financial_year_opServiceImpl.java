package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_financial_year_opService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_financial_year_opFeignClient;

/**
 * 实体[上个财政年的期初] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_financial_year_opServiceImpl implements IAccount_financial_year_opService {

    @Autowired
    account_financial_year_opFeignClient account_financial_year_opFeignClient;


    @Override
    public boolean update(Account_financial_year_op et) {
        Account_financial_year_op rt = account_financial_year_opFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_financial_year_op> list){
        account_financial_year_opFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_financial_year_op getDraft(Account_financial_year_op et) {
        et=account_financial_year_opFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_financial_year_op et) {
        Account_financial_year_op rt = account_financial_year_opFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_financial_year_op> list){
        account_financial_year_opFeignClient.createBatch(list) ;
    }

    @Override
    public Account_financial_year_op get(Integer id) {
		Account_financial_year_op et=account_financial_year_opFeignClient.get(id);
        if(et==null){
            et=new Account_financial_year_op();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_financial_year_opFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_financial_year_opFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_financial_year_op> searchDefault(Account_financial_year_opSearchContext context) {
        Page<Account_financial_year_op> account_financial_year_ops=account_financial_year_opFeignClient.searchDefault(context);
        return account_financial_year_ops;
    }


}


