package cn.ibizlab.odoo.core.r7rt_dyna.valuerule.anno.dynachart;

import cn.ibizlab.odoo.core.r7rt_dyna.valuerule.validator.dynachart.DynaChartCreateDateDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：DynaChart
 * 属性：CreateDate
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {DynaChartCreateDateDefaultValidator.class})
public @interface DynaChartCreateDateDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
