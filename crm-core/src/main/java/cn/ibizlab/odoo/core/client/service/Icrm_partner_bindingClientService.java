package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_partner_binding;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_partner_binding] 服务对象接口
 */
public interface Icrm_partner_bindingClientService{

    public Icrm_partner_binding createModel() ;

    public void update(Icrm_partner_binding crm_partner_binding);

    public void updateBatch(List<Icrm_partner_binding> crm_partner_bindings);

    public void removeBatch(List<Icrm_partner_binding> crm_partner_bindings);

    public void get(Icrm_partner_binding crm_partner_binding);

    public void remove(Icrm_partner_binding crm_partner_binding);

    public Page<Icrm_partner_binding> fetchDefault(SearchContext context);

    public void createBatch(List<Icrm_partner_binding> crm_partner_bindings);

    public void create(Icrm_partner_binding crm_partner_binding);

    public Page<Icrm_partner_binding> select(SearchContext context);

    public void getDraft(Icrm_partner_binding crm_partner_binding);

}
