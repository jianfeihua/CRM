package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_lead_tagFeignClient;

/**
 * 实体[线索标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_lead_tagServiceImpl implements ICrm_lead_tagService {

    @Autowired
    crm_lead_tagFeignClient crm_lead_tagFeignClient;


    @Override
    public Crm_lead_tag getDraft(Crm_lead_tag et) {
        et=crm_lead_tagFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_lead_tagFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_lead_tagFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Crm_lead_tag et) {
        Crm_lead_tag rt = crm_lead_tagFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_lead_tag> list){
        crm_lead_tagFeignClient.updateBatch(list) ;
    }

    @Override
    public Crm_lead_tag get(Integer id) {
		Crm_lead_tag et=crm_lead_tagFeignClient.get(id);
        if(et==null){
            et=new Crm_lead_tag();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Crm_lead_tag et) {
        Crm_lead_tag rt = crm_lead_tagFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lead_tag> list){
        crm_lead_tagFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lead_tag> searchDefault(Crm_lead_tagSearchContext context) {
        Page<Crm_lead_tag> crm_lead_tags=crm_lead_tagFeignClient.searchDefault(context);
        return crm_lead_tags;
    }


}


