package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_mail_compose_messageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_mail_compose_messageFeignClient;

/**
 * 实体[调查的功能EMail撰写向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_mail_compose_messageServiceImpl implements ISurvey_mail_compose_messageService {

    @Autowired
    survey_mail_compose_messageFeignClient survey_mail_compose_messageFeignClient;


    @Override
    public boolean create(Survey_mail_compose_message et) {
        Survey_mail_compose_message rt = survey_mail_compose_messageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_mail_compose_message> list){
        survey_mail_compose_messageFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_mail_compose_messageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_mail_compose_messageFeignClient.removeBatch(idList);
    }

    @Override
    public Survey_mail_compose_message get(Integer id) {
		Survey_mail_compose_message et=survey_mail_compose_messageFeignClient.get(id);
        if(et==null){
            et=new Survey_mail_compose_message();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Survey_mail_compose_message et) {
        Survey_mail_compose_message rt = survey_mail_compose_messageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_mail_compose_message> list){
        survey_mail_compose_messageFeignClient.updateBatch(list) ;
    }

    @Override
    public Survey_mail_compose_message getDraft(Survey_mail_compose_message et) {
        et=survey_mail_compose_messageFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_mail_compose_message> searchDefault(Survey_mail_compose_messageSearchContext context) {
        Page<Survey_mail_compose_message> survey_mail_compose_messages=survey_mail_compose_messageFeignClient.searchDefault(context);
        return survey_mail_compose_messages;
    }


}


