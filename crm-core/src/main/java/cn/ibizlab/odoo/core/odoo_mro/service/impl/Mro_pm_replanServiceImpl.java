package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_replanSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_replanService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_replanFeignClient;

/**
 * 实体[Replan PM] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_replanServiceImpl implements IMro_pm_replanService {

    @Autowired
    mro_pm_replanFeignClient mro_pm_replanFeignClient;


    @Override
    public boolean create(Mro_pm_replan et) {
        Mro_pm_replan rt = mro_pm_replanFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_replan> list){
        mro_pm_replanFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_pm_replanFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_pm_replanFeignClient.removeBatch(idList);
    }

    @Override
    public Mro_pm_replan getDraft(Mro_pm_replan et) {
        et=mro_pm_replanFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mro_pm_replan et) {
        Mro_pm_replan rt = mro_pm_replanFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_pm_replan> list){
        mro_pm_replanFeignClient.updateBatch(list) ;
    }

    @Override
    public Mro_pm_replan get(Integer id) {
		Mro_pm_replan et=mro_pm_replanFeignClient.get(id);
        if(et==null){
            et=new Mro_pm_replan();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_replan> searchDefault(Mro_pm_replanSearchContext context) {
        Page<Mro_pm_replan> mro_pm_replans=mro_pm_replanFeignClient.searchDefault(context);
        return mro_pm_replans;
    }


}


