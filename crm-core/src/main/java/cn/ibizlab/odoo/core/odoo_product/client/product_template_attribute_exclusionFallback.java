package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_template_attribute_exclusion] 服务对象接口
 */
@Component
public class product_template_attribute_exclusionFallback implements product_template_attribute_exclusionFeignClient{


    public Page<Product_template_attribute_exclusion> searchDefault(Product_template_attribute_exclusionSearchContext context){
            return null;
     }


    public Product_template_attribute_exclusion get(Integer id){
            return null;
     }


    public Product_template_attribute_exclusion create(Product_template_attribute_exclusion product_template_attribute_exclusion){
            return null;
     }
    public Boolean createBatch(List<Product_template_attribute_exclusion> product_template_attribute_exclusions){
            return false;
     }

    public Product_template_attribute_exclusion update(Integer id, Product_template_attribute_exclusion product_template_attribute_exclusion){
            return null;
     }
    public Boolean updateBatch(List<Product_template_attribute_exclusion> product_template_attribute_exclusions){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Product_template_attribute_exclusion> select(){
            return null;
     }

    public Product_template_attribute_exclusion getDraft(){
            return null;
    }



}
