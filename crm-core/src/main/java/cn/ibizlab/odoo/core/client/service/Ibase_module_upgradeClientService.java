package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_module_upgrade;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_module_upgrade] 服务对象接口
 */
public interface Ibase_module_upgradeClientService{

    public Ibase_module_upgrade createModel() ;

    public void update(Ibase_module_upgrade base_module_upgrade);

    public void createBatch(List<Ibase_module_upgrade> base_module_upgrades);

    public void remove(Ibase_module_upgrade base_module_upgrade);

    public void get(Ibase_module_upgrade base_module_upgrade);

    public Page<Ibase_module_upgrade> fetchDefault(SearchContext context);

    public void removeBatch(List<Ibase_module_upgrade> base_module_upgrades);

    public void create(Ibase_module_upgrade base_module_upgrade);

    public void updateBatch(List<Ibase_module_upgrade> base_module_upgrades);

    public Page<Ibase_module_upgrade> select(SearchContext context);

    public void getDraft(Ibase_module_upgrade base_module_upgrade);

}
