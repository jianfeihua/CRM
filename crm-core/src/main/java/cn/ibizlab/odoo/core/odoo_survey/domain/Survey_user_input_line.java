package cn.ibizlab.odoo.core.odoo_survey.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [调查用户输入明细] 对象
 */
@Data
public class Survey_user_input_line extends EntityClient implements Serializable {

    /**
     * 回复日期
     */
    @DEField(name = "value_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "value_date" , format="yyyy-MM-dd")
    @JsonProperty("value_date")
    private Timestamp valueDate;

    /**
     * 自由文本答案
     */
    @DEField(name = "value_free_text")
    @JSONField(name = "value_free_text")
    @JsonProperty("value_free_text")
    private String valueFreeText;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建日期
     */
    @DEField(name = "date_create")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_create" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_create")
    private Timestamp dateCreate;

    /**
     * 忽略
     */
    @JSONField(name = "skipped")
    @JsonProperty("skipped")
    private String skipped;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 这个选项分配的分数
     */
    @DEField(name = "quizz_mark")
    @JSONField(name = "quizz_mark")
    @JsonProperty("quizz_mark")
    private Double quizzMark;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 回复类型
     */
    @DEField(name = "answer_type")
    @JSONField(name = "answer_type")
    @JsonProperty("answer_type")
    private String answerType;

    /**
     * 文本答案
     */
    @DEField(name = "value_text")
    @JSONField(name = "value_text")
    @JsonProperty("value_text")
    private String valueText;

    /**
     * 数字答案
     */
    @DEField(name = "value_number")
    @JSONField(name = "value_number")
    @JsonProperty("value_number")
    private Double valueNumber;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 页
     */
    @JSONField(name = "page_id")
    @JsonProperty("page_id")
    private Integer pageId;

    /**
     * 用户输入
     */
    @DEField(name = "user_input_id")
    @JSONField(name = "user_input_id")
    @JsonProperty("user_input_id")
    private Integer userInputId;

    /**
     * 建议答案
     */
    @DEField(name = "value_suggested")
    @JSONField(name = "value_suggested")
    @JsonProperty("value_suggested")
    private Integer valueSuggested;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 答案行
     */
    @DEField(name = "value_suggested_row")
    @JSONField(name = "value_suggested_row")
    @JsonProperty("value_suggested_row")
    private Integer valueSuggestedRow;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 疑问
     */
    @DEField(name = "question_id")
    @JSONField(name = "question_id")
    @JsonProperty("question_id")
    private Integer questionId;

    /**
     * 问卷
     */
    @DEField(name = "survey_id")
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    private Integer surveyId;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoovalue")
    @JsonProperty("odoovalue")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label odooValue;

    /**
     * 
     */
    @JSONField(name = "odoovaluesuggested")
    @JsonProperty("odoovaluesuggested")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label odooValueSuggested;

    /**
     * 
     */
    @JSONField(name = "odooquestion")
    @JsonProperty("odooquestion")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question odooQuestion;

    /**
     * 
     */
    @JSONField(name = "odoosurvey")
    @JsonProperty("odoosurvey")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey odooSurvey;

    /**
     * 
     */
    @JSONField(name = "odoouserinput")
    @JsonProperty("odoouserinput")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input odooUserInput;




    /**
     * 设置 [回复日期]
     */
    public void setValueDate(Timestamp valueDate){
        this.valueDate = valueDate ;
        this.modify("value_date",valueDate);
    }
    /**
     * 设置 [自由文本答案]
     */
    public void setValueFreeText(String valueFreeText){
        this.valueFreeText = valueFreeText ;
        this.modify("value_free_text",valueFreeText);
    }
    /**
     * 设置 [创建日期]
     */
    public void setDateCreate(Timestamp dateCreate){
        this.dateCreate = dateCreate ;
        this.modify("date_create",dateCreate);
    }
    /**
     * 设置 [忽略]
     */
    public void setSkipped(String skipped){
        this.skipped = skipped ;
        this.modify("skipped",skipped);
    }
    /**
     * 设置 [这个选项分配的分数]
     */
    public void setQuizzMark(Double quizzMark){
        this.quizzMark = quizzMark ;
        this.modify("quizz_mark",quizzMark);
    }
    /**
     * 设置 [回复类型]
     */
    public void setAnswerType(String answerType){
        this.answerType = answerType ;
        this.modify("answer_type",answerType);
    }
    /**
     * 设置 [文本答案]
     */
    public void setValueText(String valueText){
        this.valueText = valueText ;
        this.modify("value_text",valueText);
    }
    /**
     * 设置 [数字答案]
     */
    public void setValueNumber(Double valueNumber){
        this.valueNumber = valueNumber ;
        this.modify("value_number",valueNumber);
    }
    /**
     * 设置 [用户输入]
     */
    public void setUserInputId(Integer userInputId){
        this.userInputId = userInputId ;
        this.modify("user_input_id",userInputId);
    }
    /**
     * 设置 [建议答案]
     */
    public void setValueSuggested(Integer valueSuggested){
        this.valueSuggested = valueSuggested ;
        this.modify("value_suggested",valueSuggested);
    }
    /**
     * 设置 [答案行]
     */
    public void setValueSuggestedRow(Integer valueSuggestedRow){
        this.valueSuggestedRow = valueSuggestedRow ;
        this.modify("value_suggested_row",valueSuggestedRow);
    }
    /**
     * 设置 [疑问]
     */
    public void setQuestionId(Integer questionId){
        this.questionId = questionId ;
        this.modify("question_id",questionId);
    }
    /**
     * 设置 [问卷]
     */
    public void setSurveyId(Integer surveyId){
        this.surveyId = surveyId ;
        this.modify("survey_id",surveyId);
    }

}


