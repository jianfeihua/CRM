package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [repair_order] 对象
 */
public interface Irepair_order {

    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [收货地址]
     */
    public void setAddress_id(Integer address_id);
    
    /**
     * 设置 [收货地址]
     */
    public Integer getAddress_id();

    /**
     * 获取 [收货地址]脏标记
     */
    public boolean getAddress_idDirtyFlag();
    /**
     * 获取 [收货地址]
     */
    public void setAddress_id_text(String address_id_text);
    
    /**
     * 设置 [收货地址]
     */
    public String getAddress_id_text();

    /**
     * 获取 [收货地址]脏标记
     */
    public boolean getAddress_id_textDirtyFlag();
    /**
     * 获取 [税]
     */
    public void setAmount_tax(Double amount_tax);
    
    /**
     * 设置 [税]
     */
    public Double getAmount_tax();

    /**
     * 获取 [税]脏标记
     */
    public boolean getAmount_taxDirtyFlag();
    /**
     * 获取 [合计]
     */
    public void setAmount_total(Double amount_total);
    
    /**
     * 设置 [合计]
     */
    public Double getAmount_total();

    /**
     * 获取 [合计]脏标记
     */
    public boolean getAmount_totalDirtyFlag();
    /**
     * 获取 [未税金额]
     */
    public void setAmount_untaxed(Double amount_untaxed);
    
    /**
     * 设置 [未税金额]
     */
    public Double getAmount_untaxed();

    /**
     * 获取 [未税金额]脏标记
     */
    public boolean getAmount_untaxedDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建者]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建者]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建者]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建者]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建者]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建者]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [默认地址]
     */
    public void setDefault_address_id(Integer default_address_id);
    
    /**
     * 设置 [默认地址]
     */
    public Integer getDefault_address_id();

    /**
     * 获取 [默认地址]脏标记
     */
    public boolean getDefault_address_idDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [作业]
     */
    public void setFees_lines(String fees_lines);
    
    /**
     * 设置 [作业]
     */
    public String getFees_lines();

    /**
     * 获取 [作业]脏标记
     */
    public boolean getFees_linesDirtyFlag();
    /**
     * 获取 [质保到期]
     */
    public void setGuarantee_limit(Timestamp guarantee_limit);
    
    /**
     * 设置 [质保到期]
     */
    public Timestamp getGuarantee_limit();

    /**
     * 获取 [质保到期]脏标记
     */
    public boolean getGuarantee_limitDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [内部备注]
     */
    public void setInternal_notes(String internal_notes);
    
    /**
     * 设置 [内部备注]
     */
    public String getInternal_notes();

    /**
     * 获取 [内部备注]脏标记
     */
    public boolean getInternal_notesDirtyFlag();
    /**
     * 获取 [已开票]
     */
    public void setInvoiced(String invoiced);
    
    /**
     * 设置 [已开票]
     */
    public String getInvoiced();

    /**
     * 获取 [已开票]脏标记
     */
    public boolean getInvoicedDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id(Integer invoice_id);
    
    /**
     * 设置 [发票]
     */
    public Integer getInvoice_id();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id_text(String invoice_id_text);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_id_text();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_id_textDirtyFlag();
    /**
     * 获取 [开票方式]
     */
    public void setInvoice_method(String invoice_method);
    
    /**
     * 设置 [开票方式]
     */
    public String getInvoice_method();

    /**
     * 获取 [开票方式]脏标记
     */
    public boolean getInvoice_methodDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [地点]
     */
    public Integer getLocation_id();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [地点]
     */
    public String getLocation_id_text();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [批次/序列号]
     */
    public void setLot_id(Integer lot_id);
    
    /**
     * 设置 [批次/序列号]
     */
    public Integer getLot_id();

    /**
     * 获取 [批次/序列号]脏标记
     */
    public boolean getLot_idDirtyFlag();
    /**
     * 获取 [批次/序列号]
     */
    public void setLot_id_text(String lot_id_text);
    
    /**
     * 设置 [批次/序列号]
     */
    public String getLot_id_text();

    /**
     * 获取 [批次/序列号]脏标记
     */
    public boolean getLot_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [移动]
     */
    public void setMove_id(Integer move_id);
    
    /**
     * 设置 [移动]
     */
    public Integer getMove_id();

    /**
     * 获取 [移动]脏标记
     */
    public boolean getMove_idDirtyFlag();
    /**
     * 获取 [移动]
     */
    public void setMove_id_text(String move_id_text);
    
    /**
     * 设置 [移动]
     */
    public String getMove_id_text();

    /**
     * 获取 [移动]脏标记
     */
    public boolean getMove_id_textDirtyFlag();
    /**
     * 获取 [维修参照]
     */
    public void setName(String name);
    
    /**
     * 设置 [维修参照]
     */
    public String getName();

    /**
     * 获取 [维修参照]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [零件]
     */
    public void setOperations(String operations);
    
    /**
     * 设置 [零件]
     */
    public String getOperations();

    /**
     * 获取 [零件]脏标记
     */
    public boolean getOperationsDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [开票地址]
     */
    public void setPartner_invoice_id(Integer partner_invoice_id);
    
    /**
     * 设置 [开票地址]
     */
    public Integer getPartner_invoice_id();

    /**
     * 获取 [开票地址]脏标记
     */
    public boolean getPartner_invoice_idDirtyFlag();
    /**
     * 获取 [开票地址]
     */
    public void setPartner_invoice_id_text(String partner_invoice_id_text);
    
    /**
     * 设置 [开票地址]
     */
    public String getPartner_invoice_id_text();

    /**
     * 获取 [开票地址]脏标记
     */
    public boolean getPartner_invoice_id_textDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id(Integer pricelist_id);
    
    /**
     * 设置 [价格表]
     */
    public Integer getPricelist_id();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_idDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id_text(String pricelist_id_text);
    
    /**
     * 设置 [价格表]
     */
    public String getPricelist_id_text();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_id_textDirtyFlag();
    /**
     * 获取 [待维修产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [待维修产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [待维修产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [待维修产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [待维修产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [待维修产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [产品量度单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [产品量度单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [产品量度单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [产品量度单位]
     */
    public void setProduct_uom_text(String product_uom_text);
    
    /**
     * 设置 [产品量度单位]
     */
    public String getProduct_uom_text();

    /**
     * 获取 [产品量度单位]脏标记
     */
    public boolean getProduct_uom_textDirtyFlag();
    /**
     * 获取 [报价单说明]
     */
    public void setQuotation_notes(String quotation_notes);
    
    /**
     * 设置 [报价单说明]
     */
    public String getQuotation_notes();

    /**
     * 获取 [报价单说明]脏标记
     */
    public boolean getQuotation_notesDirtyFlag();
    /**
     * 获取 [已维修]
     */
    public void setRepaired(String repaired);
    
    /**
     * 设置 [已维修]
     */
    public String getRepaired();

    /**
     * 获取 [已维修]脏标记
     */
    public boolean getRepairedDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [追踪]
     */
    public void setTracking(String tracking);
    
    /**
     * 设置 [追踪]
     */
    public String getTracking();

    /**
     * 获取 [追踪]脏标记
     */
    public boolean getTrackingDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改时间]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改时间]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改时间]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
