package cn.ibizlab.odoo.core.odoo_barcodes.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.odoo.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_barcodes.service.IBarcodes_barcode_events_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_barcodes.client.barcodes_barcode_events_mixinFeignClient;

/**
 * 实体[条形码事件混合] 服务对象接口实现
 */
@Slf4j
@Service
public class Barcodes_barcode_events_mixinServiceImpl implements IBarcodes_barcode_events_mixinService {

    @Autowired
    barcodes_barcode_events_mixinFeignClient barcodes_barcode_events_mixinFeignClient;


    @Override
    public boolean create(Barcodes_barcode_events_mixin et) {
        Barcodes_barcode_events_mixin rt = barcodes_barcode_events_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Barcodes_barcode_events_mixin> list){
        barcodes_barcode_events_mixinFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Barcodes_barcode_events_mixin et) {
        Barcodes_barcode_events_mixin rt = barcodes_barcode_events_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Barcodes_barcode_events_mixin> list){
        barcodes_barcode_events_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=barcodes_barcode_events_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        barcodes_barcode_events_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public Barcodes_barcode_events_mixin get(Integer id) {
		Barcodes_barcode_events_mixin et=barcodes_barcode_events_mixinFeignClient.get(id);
        if(et==null){
            et=new Barcodes_barcode_events_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Barcodes_barcode_events_mixin getDraft(Barcodes_barcode_events_mixin et) {
        et=barcodes_barcode_events_mixinFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Barcodes_barcode_events_mixin> searchDefault(Barcodes_barcode_events_mixinSearchContext context) {
        Page<Barcodes_barcode_events_mixin> barcodes_barcode_events_mixins=barcodes_barcode_events_mixinFeignClient.searchDefault(context);
        return barcodes_barcode_events_mixins;
    }


}


