package cn.ibizlab.odoo.core.odoo_rating.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[rating_mixin] 服务对象接口
 */
@Component
public class rating_mixinFallback implements rating_mixinFeignClient{


    public Rating_mixin create(Rating_mixin rating_mixin){
            return null;
     }
    public Boolean createBatch(List<Rating_mixin> rating_mixins){
            return false;
     }

    public Rating_mixin get(Integer id){
            return null;
     }



    public Rating_mixin update(Integer id, Rating_mixin rating_mixin){
            return null;
     }
    public Boolean updateBatch(List<Rating_mixin> rating_mixins){
            return false;
     }



    public Page<Rating_mixin> searchDefault(Rating_mixinSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Rating_mixin> select(){
            return null;
     }

    public Rating_mixin getDraft(){
            return null;
    }



}
