package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantSearchContext;


/**
 * 实体[Stock_quant] 服务对象接口
 */
public interface IStock_quantService{

    boolean create(Stock_quant et) ;
    void createBatch(List<Stock_quant> list) ;
    Stock_quant getDraft(Stock_quant et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_quant et) ;
    void updateBatch(List<Stock_quant> list) ;
    Stock_quant get(Integer key) ;
    Page<Stock_quant> searchDefault(Stock_quantSearchContext context) ;

}



