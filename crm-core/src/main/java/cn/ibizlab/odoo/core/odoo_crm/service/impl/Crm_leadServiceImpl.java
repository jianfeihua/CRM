package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_leadService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_leadFeignClient;

/**
 * 实体[线索/商机] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_leadServiceImpl implements ICrm_leadService {

    @Autowired
    crm_leadFeignClient crm_leadFeignClient;


    @Override
    public Crm_lead getDraft(Crm_lead et) {
        et=crm_leadFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_leadFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_leadFeignClient.removeBatch(idList);
    }

    @Override
    @Transactional
    public boolean save(Crm_lead et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!crm_leadFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Crm_lead> list) {
        crm_leadFeignClient.saveBatch(list) ;
    }

    @Override
    public Crm_lead get(Integer id) {
		Crm_lead et=crm_leadFeignClient.get(id);
        if(et==null){
            et=new Crm_lead();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Crm_lead et) {
        Crm_lead rt = crm_leadFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lead> list){
        crm_leadFeignClient.createBatch(list) ;
    }

    @Override
    public boolean checkKey(Crm_lead et) {
        return crm_leadFeignClient.checkKey(et);
    }
    @Override
    public boolean update(Crm_lead et) {
        Crm_lead rt = crm_leadFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_lead> list){
        crm_leadFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lead> searchDefault(Crm_leadSearchContext context) {
        Page<Crm_lead> crm_leads=crm_leadFeignClient.searchDefault(context);
        return crm_leads;
    }

    /**
     * 查询集合 默认查询(商机类型排序)
     */
    @Override
    public Page<Crm_lead> searchDefault2(Crm_leadSearchContext context) {
        Page<Crm_lead> crm_leads=crm_leadFeignClient.searchDefault(context);
        return crm_leads;
    }


}


