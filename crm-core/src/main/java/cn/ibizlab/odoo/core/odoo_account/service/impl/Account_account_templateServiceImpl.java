package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_account_templateFeignClient;

/**
 * 实体[科目模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_account_templateServiceImpl implements IAccount_account_templateService {

    @Autowired
    account_account_templateFeignClient account_account_templateFeignClient;


    @Override
    public Account_account_template get(Integer id) {
		Account_account_template et=account_account_templateFeignClient.get(id);
        if(et==null){
            et=new Account_account_template();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_account_template et) {
        Account_account_template rt = account_account_templateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_account_template> list){
        account_account_templateFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_account_templateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_account_templateFeignClient.removeBatch(idList);
    }

    @Override
    public Account_account_template getDraft(Account_account_template et) {
        et=account_account_templateFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_account_template et) {
        Account_account_template rt = account_account_templateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_account_template> list){
        account_account_templateFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_account_template> searchDefault(Account_account_templateSearchContext context) {
        Page<Account_account_template> account_account_templates=account_account_templateFeignClient.searchDefault(context);
        return account_account_templates;
    }


}


