package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [EMail别名] 对象
 */
@Data
public class Mail_alias extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 上级记录ID
     */
    @DEField(name = "alias_parent_thread_id")
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 网域别名
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 模型别名
     */
    @DEField(name = "alias_model_id")
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;

    /**
     * 默认值
     */
    @DEField(name = "alias_defaults")
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 别名
     */
    @DEField(name = "alias_name")
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 记录线索ID
     */
    @DEField(name = "alias_force_thread_id")
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 上级模型
     */
    @DEField(name = "alias_parent_model_id")
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 安全联系人别名
     */
    @DEField(name = "alias_contact")
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 所有者
     */
    @JSONField(name = "alias_user_id_text")
    @JsonProperty("alias_user_id_text")
    private String aliasUserIdText;

    /**
     * 所有者
     */
    @DEField(name = "alias_user_id")
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Integer aliasUserId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odooaliasuser")
    @JsonProperty("odooaliasuser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooAliasUser;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [上级记录ID]
     */
    public void setAliasParentThreadId(Integer aliasParentThreadId){
        this.aliasParentThreadId = aliasParentThreadId ;
        this.modify("alias_parent_thread_id",aliasParentThreadId);
    }
    /**
     * 设置 [模型别名]
     */
    public void setAliasModelId(Integer aliasModelId){
        this.aliasModelId = aliasModelId ;
        this.modify("alias_model_id",aliasModelId);
    }
    /**
     * 设置 [默认值]
     */
    public void setAliasDefaults(String aliasDefaults){
        this.aliasDefaults = aliasDefaults ;
        this.modify("alias_defaults",aliasDefaults);
    }
    /**
     * 设置 [别名]
     */
    public void setAliasName(String aliasName){
        this.aliasName = aliasName ;
        this.modify("alias_name",aliasName);
    }
    /**
     * 设置 [记录线索ID]
     */
    public void setAliasForceThreadId(Integer aliasForceThreadId){
        this.aliasForceThreadId = aliasForceThreadId ;
        this.modify("alias_force_thread_id",aliasForceThreadId);
    }
    /**
     * 设置 [上级模型]
     */
    public void setAliasParentModelId(Integer aliasParentModelId){
        this.aliasParentModelId = aliasParentModelId ;
        this.modify("alias_parent_model_id",aliasParentModelId);
    }
    /**
     * 设置 [安全联系人别名]
     */
    public void setAliasContact(String aliasContact){
        this.aliasContact = aliasContact ;
        this.modify("alias_contact",aliasContact);
    }
    /**
     * 设置 [所有者]
     */
    public void setAliasUserId(Integer aliasUserId){
        this.aliasUserId = aliasUserId ;
        this.modify("alias_user_id",aliasUserId);
    }

}


