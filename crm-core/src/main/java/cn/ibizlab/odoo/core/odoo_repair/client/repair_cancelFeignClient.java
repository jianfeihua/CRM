package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[repair_cancel] 服务对象接口
 */
@FeignClient(value = "odoo-repair", contextId = "repair-cancel", fallback = repair_cancelFallback.class)
public interface repair_cancelFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/repair_cancels")
    Repair_cancel create(@RequestBody Repair_cancel repair_cancel);

    @RequestMapping(method = RequestMethod.POST, value = "/repair_cancels/batch")
    Boolean createBatch(@RequestBody List<Repair_cancel> repair_cancels);



    @RequestMapping(method = RequestMethod.GET, value = "/repair_cancels/{id}")
    Repair_cancel get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/repair_cancels/searchdefault")
    Page<Repair_cancel> searchDefault(@RequestBody Repair_cancelSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/repair_cancels/{id}")
    Repair_cancel update(@PathVariable("id") Integer id,@RequestBody Repair_cancel repair_cancel);

    @RequestMapping(method = RequestMethod.PUT, value = "/repair_cancels/batch")
    Boolean updateBatch(@RequestBody List<Repair_cancel> repair_cancels);



    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_cancels/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_cancels/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/repair_cancels/select")
    Page<Repair_cancel> select();


    @RequestMapping(method = RequestMethod.GET, value = "/repair_cancels/getdraft")
    Repair_cancel getDraft();


}
