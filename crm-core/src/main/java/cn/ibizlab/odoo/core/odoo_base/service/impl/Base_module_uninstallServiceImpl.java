package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_uninstallService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_module_uninstallFeignClient;

/**
 * 实体[模块卸载] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_module_uninstallServiceImpl implements IBase_module_uninstallService {

    @Autowired
    base_module_uninstallFeignClient base_module_uninstallFeignClient;


    @Override
    public boolean update(Base_module_uninstall et) {
        Base_module_uninstall rt = base_module_uninstallFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_module_uninstall> list){
        base_module_uninstallFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_module_uninstall get(Integer id) {
		Base_module_uninstall et=base_module_uninstallFeignClient.get(id);
        if(et==null){
            et=new Base_module_uninstall();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_module_uninstallFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_module_uninstallFeignClient.removeBatch(idList);
    }

    @Override
    public Base_module_uninstall getDraft(Base_module_uninstall et) {
        et=base_module_uninstallFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Base_module_uninstall et) {
        Base_module_uninstall rt = base_module_uninstallFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_module_uninstall> list){
        base_module_uninstallFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_module_uninstall> searchDefault(Base_module_uninstallSearchContext context) {
        Page<Base_module_uninstall> base_module_uninstalls=base_module_uninstallFeignClient.searchDefault(context);
        return base_module_uninstalls;
    }


}


