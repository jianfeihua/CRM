package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_position_taxService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_position_taxFeignClient;

/**
 * 实体[税率的科目调整] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_position_taxServiceImpl implements IAccount_fiscal_position_taxService {

    @Autowired
    account_fiscal_position_taxFeignClient account_fiscal_position_taxFeignClient;


    @Override
    public boolean create(Account_fiscal_position_tax et) {
        Account_fiscal_position_tax rt = account_fiscal_position_taxFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_position_tax> list){
        account_fiscal_position_taxFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_fiscal_position_tax et) {
        Account_fiscal_position_tax rt = account_fiscal_position_taxFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_fiscal_position_tax> list){
        account_fiscal_position_taxFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_fiscal_position_tax getDraft(Account_fiscal_position_tax et) {
        et=account_fiscal_position_taxFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_fiscal_position_taxFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_fiscal_position_taxFeignClient.removeBatch(idList);
    }

    @Override
    public Account_fiscal_position_tax get(Integer id) {
		Account_fiscal_position_tax et=account_fiscal_position_taxFeignClient.get(id);
        if(et==null){
            et=new Account_fiscal_position_tax();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_position_tax> searchDefault(Account_fiscal_position_taxSearchContext context) {
        Page<Account_fiscal_position_tax> account_fiscal_position_taxs=account_fiscal_position_taxFeignClient.searchDefault(context);
        return account_fiscal_position_taxs;
    }


}


