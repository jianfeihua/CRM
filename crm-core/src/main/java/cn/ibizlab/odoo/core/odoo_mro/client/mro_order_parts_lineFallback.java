package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_order_parts_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_order_parts_line] 服务对象接口
 */
@Component
public class mro_order_parts_lineFallback implements mro_order_parts_lineFeignClient{

    public Mro_order_parts_line update(Integer id, Mro_order_parts_line mro_order_parts_line){
            return null;
     }
    public Boolean updateBatch(List<Mro_order_parts_line> mro_order_parts_lines){
            return false;
     }


    public Page<Mro_order_parts_line> searchDefault(Mro_order_parts_lineSearchContext context){
            return null;
     }



    public Mro_order_parts_line get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mro_order_parts_line create(Mro_order_parts_line mro_order_parts_line){
            return null;
     }
    public Boolean createBatch(List<Mro_order_parts_line> mro_order_parts_lines){
            return false;
     }



    public Page<Mro_order_parts_line> select(){
            return null;
     }

    public Mro_order_parts_line getDraft(){
            return null;
    }



}
