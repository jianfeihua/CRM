package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_leave;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_leave] 服务对象接口
 */
public interface Ihr_leaveClientService{

    public Ihr_leave createModel() ;

    public Page<Ihr_leave> fetchDefault(SearchContext context);

    public void createBatch(List<Ihr_leave> hr_leaves);

    public void remove(Ihr_leave hr_leave);

    public void removeBatch(List<Ihr_leave> hr_leaves);

    public void updateBatch(List<Ihr_leave> hr_leaves);

    public void create(Ihr_leave hr_leave);

    public void get(Ihr_leave hr_leave);

    public void update(Ihr_leave hr_leave);

    public Page<Ihr_leave> select(SearchContext context);

    public void getDraft(Ihr_leave hr_leave);

}
