package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_stateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_stateFeignClient;

/**
 * 实体[车辆状态] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_stateServiceImpl implements IFleet_vehicle_stateService {

    @Autowired
    fleet_vehicle_stateFeignClient fleet_vehicle_stateFeignClient;


    @Override
    public Fleet_vehicle_state get(Integer id) {
		Fleet_vehicle_state et=fleet_vehicle_stateFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_state();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Fleet_vehicle_state et) {
        Fleet_vehicle_state rt = fleet_vehicle_stateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_state> list){
        fleet_vehicle_stateFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Fleet_vehicle_state et) {
        Fleet_vehicle_state rt = fleet_vehicle_stateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_state> list){
        fleet_vehicle_stateFeignClient.updateBatch(list) ;
    }

    @Override
    public Fleet_vehicle_state getDraft(Fleet_vehicle_state et) {
        et=fleet_vehicle_stateFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_stateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_stateFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_state> searchDefault(Fleet_vehicle_stateSearchContext context) {
        Page<Fleet_vehicle_state> fleet_vehicle_states=fleet_vehicle_stateFeignClient.searchDefault(context);
        return fleet_vehicle_states;
    }


}


