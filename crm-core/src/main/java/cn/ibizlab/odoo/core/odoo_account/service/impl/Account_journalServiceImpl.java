package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_journalSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_journalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_journalFeignClient;

/**
 * 实体[日记账] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_journalServiceImpl implements IAccount_journalService {

    @Autowired
    account_journalFeignClient account_journalFeignClient;


    @Override
    public Account_journal get(Integer id) {
		Account_journal et=account_journalFeignClient.get(id);
        if(et==null){
            et=new Account_journal();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_journal getDraft(Account_journal et) {
        et=account_journalFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_journal et) {
        Account_journal rt = account_journalFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_journal> list){
        account_journalFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_journal et) {
        Account_journal rt = account_journalFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_journal> list){
        account_journalFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_journalFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_journalFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_journal> searchDefault(Account_journalSearchContext context) {
        Page<Account_journal> account_journals=account_journalFeignClient.searchDefault(context);
        return account_journals;
    }


}


