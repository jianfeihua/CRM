package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_message_subtype;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_message_subtype] 服务对象接口
 */
public interface Imail_message_subtypeClientService{

    public Imail_message_subtype createModel() ;

    public void updateBatch(List<Imail_message_subtype> mail_message_subtypes);

    public void get(Imail_message_subtype mail_message_subtype);

    public void update(Imail_message_subtype mail_message_subtype);

    public void remove(Imail_message_subtype mail_message_subtype);

    public void removeBatch(List<Imail_message_subtype> mail_message_subtypes);

    public void create(Imail_message_subtype mail_message_subtype);

    public void createBatch(List<Imail_message_subtype> mail_message_subtypes);

    public Page<Imail_message_subtype> fetchDefault(SearchContext context);

    public Page<Imail_message_subtype> select(SearchContext context);

    public void getDraft(Imail_message_subtype mail_message_subtype);

}
