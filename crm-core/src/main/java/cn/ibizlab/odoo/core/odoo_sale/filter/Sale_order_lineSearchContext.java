package cn.ibizlab.odoo.core.odoo_sale.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Sale_order_line] 查询条件对象
 */
@Slf4j
@Data
public class Sale_order_lineSearchContext extends SearchContextBase {
	private String n_qty_delivered_method_eq;//[更新数量的方法]

	private String n_invoice_status_eq;//[发票状态]

	private String n_display_type_eq;//[显示类型]

	private String n_name_like;//[说明]

	private String n_event_ticket_id_text_eq;//[活动入场券]

	private String n_event_ticket_id_text_like;//[活动入场券]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_linked_line_id_text_eq;//[链接的订单明细]

	private String n_linked_line_id_text_like;//[链接的订单明细]

	private String n_order_partner_id_text_eq;//[客户]

	private String n_order_partner_id_text_like;//[客户]

	private String n_currency_id_text_eq;//[币种]

	private String n_currency_id_text_like;//[币种]

	private String n_product_id_text_eq;//[产品]

	private String n_product_id_text_like;//[产品]

	private String n_product_packaging_text_eq;//[包裹]

	private String n_product_packaging_text_like;//[包裹]

	private String n_order_id_text_eq;//[订单关联]

	private String n_order_id_text_like;//[订单关联]

	private String n_event_id_text_eq;//[活动]

	private String n_event_id_text_like;//[活动]

	private String n_salesman_id_text_eq;//[销售员]

	private String n_salesman_id_text_like;//[销售员]

	private String n_product_uom_text_eq;//[计量单位]

	private String n_product_uom_text_like;//[计量单位]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private String n_route_id_text_eq;//[路线]

	private String n_route_id_text_like;//[路线]

	private Integer n_salesman_id_eq;//[销售员]

	private Integer n_currency_id_eq;//[币种]

	private Integer n_order_id_eq;//[订单关联]

	private Integer n_event_id_eq;//[活动]

	private Integer n_linked_line_id_eq;//[链接的订单明细]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_route_id_eq;//[路线]

	private Integer n_order_partner_id_eq;//[客户]

	private Integer n_product_uom_eq;//[计量单位]

	private Integer n_product_id_eq;//[产品]

	private Integer n_company_id_eq;//[公司]

	private Integer n_product_packaging_eq;//[包裹]

	private Integer n_event_ticket_id_eq;//[活动入场券]

}



