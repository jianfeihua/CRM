package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_required;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_requiredSearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_m2o_requiredService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_m2o_requiredFeignClient;

/**
 * 实体[测试:基本导入模型，多对一必选] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_m2o_requiredServiceImpl implements IBase_import_tests_models_m2o_requiredService {

    @Autowired
    base_import_tests_models_m2o_requiredFeignClient base_import_tests_models_m2o_requiredFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_tests_models_m2o_requiredFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_tests_models_m2o_requiredFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Base_import_tests_models_m2o_required et) {
        Base_import_tests_models_m2o_required rt = base_import_tests_models_m2o_requiredFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_tests_models_m2o_required> list){
        base_import_tests_models_m2o_requiredFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_import_tests_models_m2o_required getDraft(Base_import_tests_models_m2o_required et) {
        et=base_import_tests_models_m2o_requiredFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Base_import_tests_models_m2o_required et) {
        Base_import_tests_models_m2o_required rt = base_import_tests_models_m2o_requiredFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_m2o_required> list){
        base_import_tests_models_m2o_requiredFeignClient.createBatch(list) ;
    }

    @Override
    public Base_import_tests_models_m2o_required get(Integer id) {
		Base_import_tests_models_m2o_required et=base_import_tests_models_m2o_requiredFeignClient.get(id);
        if(et==null){
            et=new Base_import_tests_models_m2o_required();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_m2o_required> searchDefault(Base_import_tests_models_m2o_requiredSearchContext context) {
        Page<Base_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds=base_import_tests_models_m2o_requiredFeignClient.searchDefault(context);
        return base_import_tests_models_m2o_requireds;
    }


}


