package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [rating_mixin] 对象
 */
public interface Irating_mixin {

    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [评级数]
     */
    public void setRating_count(Integer rating_count);
    
    /**
     * 设置 [评级数]
     */
    public Integer getRating_count();

    /**
     * 获取 [评级数]脏标记
     */
    public boolean getRating_countDirtyFlag();
    /**
     * 获取 [评级]
     */
    public void setRating_ids(String rating_ids);
    
    /**
     * 设置 [评级]
     */
    public String getRating_ids();

    /**
     * 获取 [评级]脏标记
     */
    public boolean getRating_idsDirtyFlag();
    /**
     * 获取 [最新反馈评级]
     */
    public void setRating_last_feedback(String rating_last_feedback);
    
    /**
     * 设置 [最新反馈评级]
     */
    public String getRating_last_feedback();

    /**
     * 获取 [最新反馈评级]脏标记
     */
    public boolean getRating_last_feedbackDirtyFlag();
    /**
     * 获取 [最新图像评级]
     */
    public void setRating_last_image(byte[] rating_last_image);
    
    /**
     * 设置 [最新图像评级]
     */
    public byte[] getRating_last_image();

    /**
     * 获取 [最新图像评级]脏标记
     */
    public boolean getRating_last_imageDirtyFlag();
    /**
     * 获取 [最新值评级]
     */
    public void setRating_last_value(Double rating_last_value);
    
    /**
     * 设置 [最新值评级]
     */
    public Double getRating_last_value();

    /**
     * 获取 [最新值评级]脏标记
     */
    public boolean getRating_last_valueDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
