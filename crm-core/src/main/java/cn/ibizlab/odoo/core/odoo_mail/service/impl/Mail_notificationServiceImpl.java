package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_notificationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_notificationFeignClient;

/**
 * 实体[消息通知] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_notificationServiceImpl implements IMail_notificationService {

    @Autowired
    mail_notificationFeignClient mail_notificationFeignClient;


    @Override
    public Mail_notification getDraft(Mail_notification et) {
        et=mail_notificationFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_notification et) {
        Mail_notification rt = mail_notificationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_notification> list){
        mail_notificationFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_notification et) {
        Mail_notification rt = mail_notificationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_notification> list){
        mail_notificationFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_notification get(Integer id) {
		Mail_notification et=mail_notificationFeignClient.get(id);
        if(et==null){
            et=new Mail_notification();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_notificationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_notificationFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_notification> searchDefault(Mail_notificationSearchContext context) {
        Page<Mail_notification> mail_notifications=mail_notificationFeignClient.searchDefault(context);
        return mail_notifications;
    }


}


