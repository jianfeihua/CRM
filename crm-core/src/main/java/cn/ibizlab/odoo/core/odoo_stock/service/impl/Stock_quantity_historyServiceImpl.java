package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantity_historySearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quantity_historyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_quantity_historyFeignClient;

/**
 * 实体[库存数量历史] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_quantity_historyServiceImpl implements IStock_quantity_historyService {

    @Autowired
    stock_quantity_historyFeignClient stock_quantity_historyFeignClient;


    @Override
    public Stock_quantity_history get(Integer id) {
		Stock_quantity_history et=stock_quantity_historyFeignClient.get(id);
        if(et==null){
            et=new Stock_quantity_history();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_quantity_historyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_quantity_historyFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Stock_quantity_history et) {
        Stock_quantity_history rt = stock_quantity_historyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_quantity_history> list){
        stock_quantity_historyFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Stock_quantity_history et) {
        Stock_quantity_history rt = stock_quantity_historyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_quantity_history> list){
        stock_quantity_historyFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_quantity_history getDraft(Stock_quantity_history et) {
        et=stock_quantity_historyFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_quantity_history> searchDefault(Stock_quantity_historySearchContext context) {
        Page<Stock_quantity_history> stock_quantity_historys=stock_quantity_historyFeignClient.searchDefault(context);
        return stock_quantity_historys;
    }


}


