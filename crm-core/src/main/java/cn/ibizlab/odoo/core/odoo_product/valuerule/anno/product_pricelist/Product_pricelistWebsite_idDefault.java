package cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_pricelist;

import cn.ibizlab.odoo.core.odoo_product.valuerule.validator.product_pricelist.Product_pricelistWebsite_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Product_pricelist
 * 属性：Website_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Product_pricelistWebsite_idDefaultValidator.class})
public @interface Product_pricelistWebsite_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
