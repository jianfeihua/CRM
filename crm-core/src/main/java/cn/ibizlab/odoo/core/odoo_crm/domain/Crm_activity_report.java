package cn.ibizlab.odoo.core.odoo_crm.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [CRM活动分析] 对象
 */
@Data
public class Crm_activity_report extends EntityClient implements Serializable {

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 概率
     */
    @JSONField(name = "probability")
    @JsonProperty("probability")
    private Double probability;

    /**
     * 类型
     */
    @DEField(name = "lead_type")
    @JSONField(name = "lead_type")
    @JsonProperty("lead_type")
    private String leadType;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 摘要
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * 活动类型
     */
    @JSONField(name = "mail_activity_type_id_text")
    @JsonProperty("mail_activity_type_id_text")
    private String mailActivityTypeIdText;

    /**
     * 线索
     */
    @JSONField(name = "lead_id_text")
    @JsonProperty("lead_id_text")
    private String leadIdText;

    /**
     * 销售员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 子类型
     */
    @JSONField(name = "subtype_id_text")
    @JsonProperty("subtype_id_text")
    private String subtypeIdText;

    /**
     * 国家
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 业务合作伙伴/客户
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 阶段
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;

    /**
     * 创建人
     */
    @JSONField(name = "author_id_text")
    @JsonProperty("author_id_text")
    private String authorIdText;

    /**
     * 创建人
     */
    @DEField(name = "author_id")
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Integer authorId;

    /**
     * 国家
     */
    @DEField(name = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 阶段
     */
    @DEField(name = "stage_id")
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Integer stageId;

    /**
     * 子类型
     */
    @DEField(name = "subtype_id")
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Integer subtypeId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 线索
     */
    @DEField(name = "lead_id")
    @JSONField(name = "lead_id")
    @JsonProperty("lead_id")
    private Integer leadId;

    /**
     * 活动类型
     */
    @DEField(name = "mail_activity_type_id")
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Integer mailActivityTypeId;

    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 业务合作伙伴/客户
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;


    /**
     * 
     */
    @JSONField(name = "odoolead")
    @JsonProperty("odoolead")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead odooLead;

    /**
     * 
     */
    @JSONField(name = "odoostage")
    @JsonProperty("odoostage")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage odooStage;

    /**
     * 
     */
    @JSONField(name = "odooteam")
    @JsonProperty("odooteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JSONField(name = "odoomailactivitytype")
    @JsonProperty("odoomailactivitytype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type odooMailActivityType;

    /**
     * 
     */
    @JSONField(name = "odoosubtype")
    @JsonProperty("odoosubtype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype odooSubtype;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocountry")
    @JsonProperty("odoocountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JSONField(name = "odooauthor")
    @JsonProperty("odooauthor")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooAuthor;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;




    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [概率]
     */
    public void setProbability(Double probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }
    /**
     * 设置 [类型]
     */
    public void setLeadType(String leadType){
        this.leadType = leadType ;
        this.modify("lead_type",leadType);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [摘要]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }
    /**
     * 设置 [创建人]
     */
    public void setAuthorId(Integer authorId){
        this.authorId = authorId ;
        this.modify("author_id",authorId);
    }
    /**
     * 设置 [国家]
     */
    public void setCountryId(Integer countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }
    /**
     * 设置 [阶段]
     */
    public void setStageId(Integer stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }
    /**
     * 设置 [子类型]
     */
    public void setSubtypeId(Integer subtypeId){
        this.subtypeId = subtypeId ;
        this.modify("subtype_id",subtypeId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [线索]
     */
    public void setLeadId(Integer leadId){
        this.leadId = leadId ;
        this.modify("lead_id",leadId);
    }
    /**
     * 设置 [活动类型]
     */
    public void setMailActivityTypeId(Integer mailActivityTypeId){
        this.mailActivityTypeId = mailActivityTypeId ;
        this.modify("mail_activity_type_id",mailActivityTypeId);
    }
    /**
     * 设置 [销售员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [业务合作伙伴/客户]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Integer teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

}


