package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_image;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_imageSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_imageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_imageFeignClient;

/**
 * 实体[产品图片] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_imageServiceImpl implements IProduct_imageService {

    @Autowired
    product_imageFeignClient product_imageFeignClient;


    @Override
    public Product_image getDraft(Product_image et) {
        et=product_imageFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Product_image et) {
        Product_image rt = product_imageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_image> list){
        product_imageFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_imageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_imageFeignClient.removeBatch(idList);
    }

    @Override
    public Product_image get(Integer id) {
		Product_image et=product_imageFeignClient.get(id);
        if(et==null){
            et=new Product_image();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Product_image et) {
        Product_image rt = product_imageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_image> list){
        product_imageFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_image> searchDefault(Product_imageSearchContext context) {
        Page<Product_image> product_images=product_imageFeignClient.searchDefault(context);
        return product_images;
    }


}


