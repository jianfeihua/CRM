package cn.ibizlab.odoo.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;


/**
 * 实体[Repair_cancel] 服务对象接口
 */
public interface IRepair_cancelService{

    Repair_cancel getDraft(Repair_cancel et) ;
    boolean update(Repair_cancel et) ;
    void updateBatch(List<Repair_cancel> list) ;
    boolean create(Repair_cancel et) ;
    void createBatch(List<Repair_cancel> list) ;
    Repair_cancel get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Repair_cancel> searchDefault(Repair_cancelSearchContext context) ;

}



