package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;


/**
 * 实体[Account_bank_statement] 服务对象接口
 */
public interface IAccount_bank_statementService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_bank_statement et) ;
    void createBatch(List<Account_bank_statement> list) ;
    Account_bank_statement get(Integer key) ;
    Account_bank_statement getDraft(Account_bank_statement et) ;
    boolean update(Account_bank_statement et) ;
    void updateBatch(List<Account_bank_statement> list) ;
    Page<Account_bank_statement> searchDefault(Account_bank_statementSearchContext context) ;

}



