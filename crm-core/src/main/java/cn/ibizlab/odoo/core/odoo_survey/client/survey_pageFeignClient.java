package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_pageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_page] 服务对象接口
 */
@FeignClient(value = "odoo-survey", contextId = "survey-page", fallback = survey_pageFallback.class)
public interface survey_pageFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/survey_pages/{id}")
    Survey_page get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/survey_pages/{id}")
    Survey_page update(@PathVariable("id") Integer id,@RequestBody Survey_page survey_page);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_pages/batch")
    Boolean updateBatch(@RequestBody List<Survey_page> survey_pages);





    @RequestMapping(method = RequestMethod.POST, value = "/survey_pages/searchdefault")
    Page<Survey_page> searchDefault(@RequestBody Survey_pageSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_pages/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_pages/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/survey_pages")
    Survey_page create(@RequestBody Survey_page survey_page);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_pages/batch")
    Boolean createBatch(@RequestBody List<Survey_page> survey_pages);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_pages/select")
    Page<Survey_page> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_pages/getdraft")
    Survey_page getDraft();


}
