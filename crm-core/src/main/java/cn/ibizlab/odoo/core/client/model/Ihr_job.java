package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_job] 对象
 */
public interface Ihr_job {

    /**
     * 获取 [工作地点]
     */
    public void setAddress_id(Integer address_id);
    
    /**
     * 设置 [工作地点]
     */
    public Integer getAddress_id();

    /**
     * 获取 [工作地点]脏标记
     */
    public boolean getAddress_idDirtyFlag();
    /**
     * 获取 [工作地点]
     */
    public void setAddress_id_text(String address_id_text);
    
    /**
     * 设置 [工作地点]
     */
    public String getAddress_id_text();

    /**
     * 获取 [工作地点]脏标记
     */
    public boolean getAddress_id_textDirtyFlag();
    /**
     * 获取 [别名联系人安全]
     */
    public void setAlias_contact(String alias_contact);
    
    /**
     * 设置 [别名联系人安全]
     */
    public String getAlias_contact();

    /**
     * 获取 [别名联系人安全]脏标记
     */
    public boolean getAlias_contactDirtyFlag();
    /**
     * 获取 [默认值]
     */
    public void setAlias_defaults(String alias_defaults);
    
    /**
     * 设置 [默认值]
     */
    public String getAlias_defaults();

    /**
     * 获取 [默认值]脏标记
     */
    public boolean getAlias_defaultsDirtyFlag();
    /**
     * 获取 [网域别名]
     */
    public void setAlias_domain(String alias_domain);
    
    /**
     * 设置 [网域别名]
     */
    public String getAlias_domain();

    /**
     * 获取 [网域别名]脏标记
     */
    public boolean getAlias_domainDirtyFlag();
    /**
     * 获取 [记录线索ID]
     */
    public void setAlias_force_thread_id(Integer alias_force_thread_id);
    
    /**
     * 设置 [记录线索ID]
     */
    public Integer getAlias_force_thread_id();

    /**
     * 获取 [记录线索ID]脏标记
     */
    public boolean getAlias_force_thread_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_id(Integer alias_id);
    
    /**
     * 设置 [别名]
     */
    public Integer getAlias_id();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_idDirtyFlag();
    /**
     * 获取 [模型别名]
     */
    public void setAlias_model_id(Integer alias_model_id);
    
    /**
     * 设置 [模型别名]
     */
    public Integer getAlias_model_id();

    /**
     * 获取 [模型别名]脏标记
     */
    public boolean getAlias_model_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_name(String alias_name);
    
    /**
     * 设置 [别名]
     */
    public String getAlias_name();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_nameDirtyFlag();
    /**
     * 获取 [上级模型]
     */
    public void setAlias_parent_model_id(Integer alias_parent_model_id);
    
    /**
     * 设置 [上级模型]
     */
    public Integer getAlias_parent_model_id();

    /**
     * 获取 [上级模型]脏标记
     */
    public boolean getAlias_parent_model_idDirtyFlag();
    /**
     * 获取 [上级记录ID]
     */
    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);
    
    /**
     * 设置 [上级记录ID]
     */
    public Integer getAlias_parent_thread_id();

    /**
     * 获取 [上级记录ID]脏标记
     */
    public boolean getAlias_parent_thread_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setAlias_user_id(Integer alias_user_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getAlias_user_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getAlias_user_idDirtyFlag();
    /**
     * 获取 [应用数量]
     */
    public void setApplication_count(Integer application_count);
    
    /**
     * 设置 [应用数量]
     */
    public Integer getApplication_count();

    /**
     * 获取 [应用数量]脏标记
     */
    public boolean getApplication_countDirtyFlag();
    /**
     * 获取 [求职申请]
     */
    public void setApplication_ids(String application_ids);
    
    /**
     * 设置 [求职申请]
     */
    public String getApplication_ids();

    /**
     * 获取 [求职申请]脏标记
     */
    public boolean getApplication_idsDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [工作说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [工作说明]
     */
    public String getDescription();

    /**
     * 获取 [工作说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [文档数]
     */
    public void setDocuments_count(Integer documents_count);
    
    /**
     * 设置 [文档数]
     */
    public Integer getDocuments_count();

    /**
     * 获取 [文档数]脏标记
     */
    public boolean getDocuments_countDirtyFlag();
    /**
     * 获取 [文档]
     */
    public void setDocument_ids(String document_ids);
    
    /**
     * 设置 [文档]
     */
    public String getDocument_ids();

    /**
     * 获取 [文档]脏标记
     */
    public boolean getDocument_idsDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_ids(String employee_ids);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee_ids();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_idsDirtyFlag();
    /**
     * 获取 [预计员工数合计]
     */
    public void setExpected_employees(Integer expected_employees);
    
    /**
     * 设置 [预计员工数合计]
     */
    public Integer getExpected_employees();

    /**
     * 获取 [预计员工数合计]脏标记
     */
    public boolean getExpected_employeesDirtyFlag();
    /**
     * 获取 [人力资源主管]
     */
    public void setHr_responsible_id(Integer hr_responsible_id);
    
    /**
     * 设置 [人力资源主管]
     */
    public Integer getHr_responsible_id();

    /**
     * 获取 [人力资源主管]脏标记
     */
    public boolean getHr_responsible_idDirtyFlag();
    /**
     * 获取 [人力资源主管]
     */
    public void setHr_responsible_id_text(String hr_responsible_id_text);
    
    /**
     * 设置 [人力资源主管]
     */
    public String getHr_responsible_id_text();

    /**
     * 获取 [人力资源主管]脏标记
     */
    public boolean getHr_responsible_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setIs_published(String is_published);
    
    /**
     * 设置 [已发布]
     */
    public String getIs_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getIs_publishedDirtyFlag();
    /**
     * 获取 [SEO优化]
     */
    public void setIs_seo_optimized(String is_seo_optimized);
    
    /**
     * 设置 [SEO优化]
     */
    public String getIs_seo_optimized();

    /**
     * 获取 [SEO优化]脏标记
     */
    public boolean getIs_seo_optimizedDirtyFlag();
    /**
     * 获取 [部门经理]
     */
    public void setManager_id(Integer manager_id);
    
    /**
     * 设置 [部门经理]
     */
    public Integer getManager_id();

    /**
     * 获取 [部门经理]脏标记
     */
    public boolean getManager_idDirtyFlag();
    /**
     * 获取 [部门经理]
     */
    public void setManager_id_text(String manager_id_text);
    
    /**
     * 设置 [部门经理]
     */
    public String getManager_id_text();

    /**
     * 获取 [部门经理]脏标记
     */
    public boolean getManager_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [信息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [信息]
     */
    public String getMessage_ids();

    /**
     * 获取 [信息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [工作岗位]
     */
    public void setName(String name);
    
    /**
     * 设置 [工作岗位]
     */
    public String getName();

    /**
     * 获取 [工作岗位]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [当前员工数量]
     */
    public void setNo_of_employee(Integer no_of_employee);
    
    /**
     * 设置 [当前员工数量]
     */
    public Integer getNo_of_employee();

    /**
     * 获取 [当前员工数量]脏标记
     */
    public boolean getNo_of_employeeDirtyFlag();
    /**
     * 获取 [已雇用员工]
     */
    public void setNo_of_hired_employee(Integer no_of_hired_employee);
    
    /**
     * 设置 [已雇用员工]
     */
    public Integer getNo_of_hired_employee();

    /**
     * 获取 [已雇用员工]脏标记
     */
    public boolean getNo_of_hired_employeeDirtyFlag();
    /**
     * 获取 [期望的新员工]
     */
    public void setNo_of_recruitment(Integer no_of_recruitment);
    
    /**
     * 设置 [期望的新员工]
     */
    public Integer getNo_of_recruitment();

    /**
     * 获取 [期望的新员工]脏标记
     */
    public boolean getNo_of_recruitmentDirtyFlag();
    /**
     * 获取 [要求]
     */
    public void setRequirements(String requirements);
    
    /**
     * 设置 [要求]
     */
    public String getRequirements();

    /**
     * 获取 [要求]脏标记
     */
    public boolean getRequirementsDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [招聘负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [招聘负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [招聘负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [招聘负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [招聘负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [招聘负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站说明]
     */
    public void setWebsite_description(String website_description);
    
    /**
     * 设置 [网站说明]
     */
    public String getWebsite_description();

    /**
     * 获取 [网站说明]脏标记
     */
    public boolean getWebsite_descriptionDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [网站元说明]
     */
    public void setWebsite_meta_description(String website_meta_description);
    
    /**
     * 设置 [网站元说明]
     */
    public String getWebsite_meta_description();

    /**
     * 获取 [网站元说明]脏标记
     */
    public boolean getWebsite_meta_descriptionDirtyFlag();
    /**
     * 获取 [网站meta关键词]
     */
    public void setWebsite_meta_keywords(String website_meta_keywords);
    
    /**
     * 设置 [网站meta关键词]
     */
    public String getWebsite_meta_keywords();

    /**
     * 获取 [网站meta关键词]脏标记
     */
    public boolean getWebsite_meta_keywordsDirtyFlag();
    /**
     * 获取 [网站opengraph图像]
     */
    public void setWebsite_meta_og_img(String website_meta_og_img);
    
    /**
     * 设置 [网站opengraph图像]
     */
    public String getWebsite_meta_og_img();

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    public boolean getWebsite_meta_og_imgDirtyFlag();
    /**
     * 获取 [网站meta标题]
     */
    public void setWebsite_meta_title(String website_meta_title);
    
    /**
     * 设置 [网站meta标题]
     */
    public String getWebsite_meta_title();

    /**
     * 获取 [网站meta标题]脏标记
     */
    public boolean getWebsite_meta_titleDirtyFlag();
    /**
     * 获取 [在当前网站显示]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [在当前网站显示]
     */
    public String getWebsite_published();

    /**
     * 获取 [在当前网站显示]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
