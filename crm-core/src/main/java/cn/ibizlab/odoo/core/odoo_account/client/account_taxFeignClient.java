package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_taxSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_tax] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-tax", fallback = account_taxFallback.class)
public interface account_taxFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/account_taxes/{id}")
    Account_tax get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_taxes")
    Account_tax create(@RequestBody Account_tax account_tax);

    @RequestMapping(method = RequestMethod.POST, value = "/account_taxes/batch")
    Boolean createBatch(@RequestBody List<Account_tax> account_taxes);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_taxes/{id}")
    Account_tax update(@PathVariable("id") Integer id,@RequestBody Account_tax account_tax);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_taxes/batch")
    Boolean updateBatch(@RequestBody List<Account_tax> account_taxes);




    @RequestMapping(method = RequestMethod.POST, value = "/account_taxes/searchdefault")
    Page<Account_tax> searchDefault(@RequestBody Account_taxSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_taxes/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_taxes/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_taxes/select")
    Page<Account_tax> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_taxes/getdraft")
    Account_tax getDraft();


}
