package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_package_levelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_package_levelFeignClient;

/**
 * 实体[库存包装层级] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_package_levelServiceImpl implements IStock_package_levelService {

    @Autowired
    stock_package_levelFeignClient stock_package_levelFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=stock_package_levelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_package_levelFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_package_level getDraft(Stock_package_level et) {
        et=stock_package_levelFeignClient.getDraft();
        return et;
    }

    @Override
    public Stock_package_level get(Integer id) {
		Stock_package_level et=stock_package_levelFeignClient.get(id);
        if(et==null){
            et=new Stock_package_level();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Stock_package_level et) {
        Stock_package_level rt = stock_package_levelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_package_level> list){
        stock_package_levelFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Stock_package_level et) {
        Stock_package_level rt = stock_package_levelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_package_level> list){
        stock_package_levelFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_package_level> searchDefault(Stock_package_levelSearchContext context) {
        Page<Stock_package_level> stock_package_levels=stock_package_levelFeignClient.searchDefault(context);
        return stock_package_levels;
    }


}


