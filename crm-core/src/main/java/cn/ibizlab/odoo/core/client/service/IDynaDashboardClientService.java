package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.IDynaDashboard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[DynaDashboard] 服务对象接口
 */
public interface IDynaDashboardClientService{

    public IDynaDashboard createModel() ;

    public Page<IDynaDashboard> select(SearchContext context);

    public void save(IDynaDashboard dynadashboard);

    public void update(IDynaDashboard dynadashboard);

    public void get(IDynaDashboard dynadashboard);

    public void remove(IDynaDashboard dynadashboard);

    public void checkKey(IDynaDashboard dynadashboard);

    public void getDraft(IDynaDashboard dynadashboard);

    public void create(IDynaDashboard dynadashboard);

    public Page<IDynaDashboard> fetchDefault(SearchContext context);

}
