package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_fiscal_year] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-fiscal-year", fallback = account_fiscal_yearFallback.class)
public interface account_fiscal_yearFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years/searchdefault")
    Page<Account_fiscal_year> searchDefault(@RequestBody Account_fiscal_yearSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_years/{id}")
    Account_fiscal_year get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_years/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_years/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_years/{id}")
    Account_fiscal_year update(@PathVariable("id") Integer id,@RequestBody Account_fiscal_year account_fiscal_year);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_years/batch")
    Boolean updateBatch(@RequestBody List<Account_fiscal_year> account_fiscal_years);


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years")
    Account_fiscal_year create(@RequestBody Account_fiscal_year account_fiscal_year);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years/batch")
    Boolean createBatch(@RequestBody List<Account_fiscal_year> account_fiscal_years);


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_years/select")
    Page<Account_fiscal_year> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_years/getdraft")
    Account_fiscal_year getDraft();


}
