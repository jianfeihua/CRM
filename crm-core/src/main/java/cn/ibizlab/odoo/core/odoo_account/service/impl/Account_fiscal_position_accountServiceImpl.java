package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_position_accountService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_position_accountFeignClient;

/**
 * 实体[会计税科目映射] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_position_accountServiceImpl implements IAccount_fiscal_position_accountService {

    @Autowired
    account_fiscal_position_accountFeignClient account_fiscal_position_accountFeignClient;


    @Override
    public boolean update(Account_fiscal_position_account et) {
        Account_fiscal_position_account rt = account_fiscal_position_accountFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_fiscal_position_account> list){
        account_fiscal_position_accountFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_fiscal_position_account getDraft(Account_fiscal_position_account et) {
        et=account_fiscal_position_accountFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_fiscal_position_account get(Integer id) {
		Account_fiscal_position_account et=account_fiscal_position_accountFeignClient.get(id);
        if(et==null){
            et=new Account_fiscal_position_account();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_fiscal_position_account et) {
        Account_fiscal_position_account rt = account_fiscal_position_accountFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_position_account> list){
        account_fiscal_position_accountFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_fiscal_position_accountFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_fiscal_position_accountFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_position_account> searchDefault(Account_fiscal_position_accountSearchContext context) {
        Page<Account_fiscal_position_account> account_fiscal_position_accounts=account_fiscal_position_accountFeignClient.searchDefault(context);
        return account_fiscal_position_accounts;
    }


}


