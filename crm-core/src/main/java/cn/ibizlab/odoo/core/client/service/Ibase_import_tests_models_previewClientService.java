package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_preview;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_preview] 服务对象接口
 */
public interface Ibase_import_tests_models_previewClientService{

    public Ibase_import_tests_models_preview createModel() ;

    public void createBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews);

    public Page<Ibase_import_tests_models_preview> fetchDefault(SearchContext context);

    public void updateBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews);

    public void create(Ibase_import_tests_models_preview base_import_tests_models_preview);

    public void update(Ibase_import_tests_models_preview base_import_tests_models_preview);

    public void get(Ibase_import_tests_models_preview base_import_tests_models_preview);

    public void removeBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews);

    public void remove(Ibase_import_tests_models_preview base_import_tests_models_preview);

    public Page<Ibase_import_tests_models_preview> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_preview base_import_tests_models_preview);

}
