package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[repair_fee] 服务对象接口
 */
@Component
public class repair_feeFallback implements repair_feeFeignClient{

    public Repair_fee get(Integer id){
            return null;
     }


    public Repair_fee create(Repair_fee repair_fee){
            return null;
     }
    public Boolean createBatch(List<Repair_fee> repair_fees){
            return false;
     }



    public Repair_fee update(Integer id, Repair_fee repair_fee){
            return null;
     }
    public Boolean updateBatch(List<Repair_fee> repair_fees){
            return false;
     }



    public Page<Repair_fee> searchDefault(Repair_feeSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Repair_fee> select(){
            return null;
     }

    public Repair_fee getDraft(){
            return null;
    }



}
