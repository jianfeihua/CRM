package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_reportFeignClient;

/**
 * 实体[采购报表] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_reportServiceImpl implements IPurchase_reportService {

    @Autowired
    purchase_reportFeignClient purchase_reportFeignClient;


    @Override
    public boolean update(Purchase_report et) {
        Purchase_report rt = purchase_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Purchase_report> list){
        purchase_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Purchase_report et) {
        Purchase_report rt = purchase_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_report> list){
        purchase_reportFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=purchase_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        purchase_reportFeignClient.removeBatch(idList);
    }

    @Override
    public Purchase_report get(Integer id) {
		Purchase_report et=purchase_reportFeignClient.get(id);
        if(et==null){
            et=new Purchase_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Purchase_report getDraft(Purchase_report et) {
        et=purchase_reportFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_report> searchDefault(Purchase_reportSearchContext context) {
        Page<Purchase_report> purchase_reports=purchase_reportFeignClient.searchDefault(context);
        return purchase_reports;
    }


}


