package cn.ibizlab.odoo.core.odoo_maintenance.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [保养设备] 对象
 */
@Data
public class Maintenance_equipment extends EntityClient implements Serializable {

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 用于
     */
    @DEField(name = "equipment_assign_to")
    @JSONField(name = "equipment_assign_to")
    @JsonProperty("equipment_assign_to")
    private String equipmentAssignTo;

    /**
     * 地点
     */
    @JSONField(name = "location")
    @JsonProperty("location")
    private String location;

    /**
     * 保修截止日期
     */
    @DEField(name = "warranty_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warranty_date" , format="yyyy-MM-dd")
    @JsonProperty("warranty_date")
    private Timestamp warrantyDate;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 设备名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 保养时长
     */
    @DEField(name = "maintenance_duration")
    @JSONField(name = "maintenance_duration")
    @JsonProperty("maintenance_duration")
    private Double maintenanceDuration;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 型号
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 当前维护
     */
    @DEField(name = "maintenance_open_count")
    @JSONField(name = "maintenance_open_count")
    @JsonProperty("maintenance_open_count")
    private Integer maintenanceOpenCount;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 维修统计
     */
    @DEField(name = "maintenance_count")
    @JSONField(name = "maintenance_count")
    @JsonProperty("maintenance_count")
    private Integer maintenanceCount;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 笔记
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 供应商参考
     */
    @DEField(name = "partner_ref")
    @JSONField(name = "partner_ref")
    @JsonProperty("partner_ref")
    private String partnerRef;

    /**
     * 保养
     */
    @JSONField(name = "maintenance_ids")
    @JsonProperty("maintenance_ids")
    private String maintenanceIds;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 序列号
     */
    @DEField(name = "serial_no")
    @JSONField(name = "serial_no")
    @JsonProperty("serial_no")
    private String serialNo;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 成本
     */
    @JSONField(name = "cost")
    @JsonProperty("cost")
    private Double cost;

    /**
     * 下次预防维护日期
     */
    @DEField(name = "next_action_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_action_date" , format="yyyy-MM-dd")
    @JsonProperty("next_action_date")
    private Timestamp nextActionDate;

    /**
     * 分配日期
     */
    @DEField(name = "assign_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "assign_date" , format="yyyy-MM-dd")
    @JsonProperty("assign_date")
    private Timestamp assignDate;

    /**
     * 实际日期
     */
    @DEField(name = "effective_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effective_date" , format="yyyy-MM-dd")
    @JsonProperty("effective_date")
    private Timestamp effectiveDate;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 报废日期
     */
    @DEField(name = "scrap_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scrap_date" , format="yyyy-MM-dd")
    @JsonProperty("scrap_date")
    private Timestamp scrapDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 预防维护间隔天数
     */
    @JSONField(name = "period")
    @JsonProperty("period")
    private Integer period;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 分配到部门
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;

    /**
     * 分配到员工
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;

    /**
     * 技术员
     */
    @JSONField(name = "technician_user_id_text")
    @JsonProperty("technician_user_id_text")
    private String technicianUserIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 设备类别
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 供应商
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 所有者
     */
    @JSONField(name = "owner_user_id_text")
    @JsonProperty("owner_user_id_text")
    private String ownerUserIdText;

    /**
     * 保养团队
     */
    @JSONField(name = "maintenance_team_id_text")
    @JsonProperty("maintenance_team_id_text")
    private String maintenanceTeamIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 技术员
     */
    @DEField(name = "technician_user_id")
    @JSONField(name = "technician_user_id")
    @JsonProperty("technician_user_id")
    private Integer technicianUserId;

    /**
     * 供应商
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 设备类别
     */
    @DEField(name = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    /**
     * 分配到员工
     */
    @DEField(name = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Integer employeeId;

    /**
     * 保养团队
     */
    @DEField(name = "maintenance_team_id")
    @JSONField(name = "maintenance_team_id")
    @JsonProperty("maintenance_team_id")
    private Integer maintenanceTeamId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 所有者
     */
    @DEField(name = "owner_user_id")
    @JSONField(name = "owner_user_id")
    @JsonProperty("owner_user_id")
    private Integer ownerUserId;

    /**
     * 分配到部门
     */
    @DEField(name = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Integer departmentId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoodepartment")
    @JsonProperty("odoodepartment")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JSONField(name = "odooemployee")
    @JsonProperty("odooemployee")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JSONField(name = "odoocategory")
    @JsonProperty("odoocategory")
    private cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category odooCategory;

    /**
     * 
     */
    @JSONField(name = "odoomaintenanceteam")
    @JsonProperty("odoomaintenanceteam")
    private cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team odooMaintenanceTeam;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odooowneruser")
    @JsonProperty("odooowneruser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooOwnerUser;

    /**
     * 
     */
    @JSONField(name = "odootechnicianuser")
    @JsonProperty("odootechnicianuser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooTechnicianUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [用于]
     */
    public void setEquipmentAssignTo(String equipmentAssignTo){
        this.equipmentAssignTo = equipmentAssignTo ;
        this.modify("equipment_assign_to",equipmentAssignTo);
    }
    /**
     * 设置 [地点]
     */
    public void setLocation(String location){
        this.location = location ;
        this.modify("location",location);
    }
    /**
     * 设置 [保修截止日期]
     */
    public void setWarrantyDate(Timestamp warrantyDate){
        this.warrantyDate = warrantyDate ;
        this.modify("warranty_date",warrantyDate);
    }
    /**
     * 设置 [设备名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [保养时长]
     */
    public void setMaintenanceDuration(Double maintenanceDuration){
        this.maintenanceDuration = maintenanceDuration ;
        this.modify("maintenance_duration",maintenanceDuration);
    }
    /**
     * 设置 [型号]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }
    /**
     * 设置 [当前维护]
     */
    public void setMaintenanceOpenCount(Integer maintenanceOpenCount){
        this.maintenanceOpenCount = maintenanceOpenCount ;
        this.modify("maintenance_open_count",maintenanceOpenCount);
    }
    /**
     * 设置 [维修统计]
     */
    public void setMaintenanceCount(Integer maintenanceCount){
        this.maintenanceCount = maintenanceCount ;
        this.modify("maintenance_count",maintenanceCount);
    }
    /**
     * 设置 [笔记]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [供应商参考]
     */
    public void setPartnerRef(String partnerRef){
        this.partnerRef = partnerRef ;
        this.modify("partner_ref",partnerRef);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [序列号]
     */
    public void setSerialNo(String serialNo){
        this.serialNo = serialNo ;
        this.modify("serial_no",serialNo);
    }
    /**
     * 设置 [成本]
     */
    public void setCost(Double cost){
        this.cost = cost ;
        this.modify("cost",cost);
    }
    /**
     * 设置 [下次预防维护日期]
     */
    public void setNextActionDate(Timestamp nextActionDate){
        this.nextActionDate = nextActionDate ;
        this.modify("next_action_date",nextActionDate);
    }
    /**
     * 设置 [分配日期]
     */
    public void setAssignDate(Timestamp assignDate){
        this.assignDate = assignDate ;
        this.modify("assign_date",assignDate);
    }
    /**
     * 设置 [实际日期]
     */
    public void setEffectiveDate(Timestamp effectiveDate){
        this.effectiveDate = effectiveDate ;
        this.modify("effective_date",effectiveDate);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [报废日期]
     */
    public void setScrapDate(Timestamp scrapDate){
        this.scrapDate = scrapDate ;
        this.modify("scrap_date",scrapDate);
    }
    /**
     * 设置 [预防维护间隔天数]
     */
    public void setPeriod(Integer period){
        this.period = period ;
        this.modify("period",period);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [技术员]
     */
    public void setTechnicianUserId(Integer technicianUserId){
        this.technicianUserId = technicianUserId ;
        this.modify("technician_user_id",technicianUserId);
    }
    /**
     * 设置 [供应商]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [设备类别]
     */
    public void setCategoryId(Integer categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }
    /**
     * 设置 [分配到员工]
     */
    public void setEmployeeId(Integer employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }
    /**
     * 设置 [保养团队]
     */
    public void setMaintenanceTeamId(Integer maintenanceTeamId){
        this.maintenanceTeamId = maintenanceTeamId ;
        this.modify("maintenance_team_id",maintenanceTeamId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [所有者]
     */
    public void setOwnerUserId(Integer ownerUserId){
        this.ownerUserId = ownerUserId ;
        this.modify("owner_user_id",ownerUserId);
    }
    /**
     * 设置 [分配到部门]
     */
    public void setDepartmentId(Integer departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }

}


