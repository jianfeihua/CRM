package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_move_lineFeignClient;

/**
 * 实体[日记账项目] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_move_lineServiceImpl implements IAccount_move_lineService {

    @Autowired
    account_move_lineFeignClient account_move_lineFeignClient;


    @Override
    public boolean update(Account_move_line et) {
        Account_move_line rt = account_move_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_move_line> list){
        account_move_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_move_line getDraft(Account_move_line et) {
        et=account_move_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_move_line et) {
        Account_move_line rt = account_move_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_move_line> list){
        account_move_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Account_move_line get(Integer id) {
		Account_move_line et=account_move_lineFeignClient.get(id);
        if(et==null){
            et=new Account_move_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_move_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_move_lineFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_move_line> searchDefault(Account_move_lineSearchContext context) {
        Page<Account_move_line> account_move_lines=account_move_lineFeignClient.searchDefault(context);
        return account_move_lines;
    }


}


