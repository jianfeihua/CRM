package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;


/**
 * 实体[Account_fiscal_year] 服务对象接口
 */
public interface IAccount_fiscal_yearService{

    Account_fiscal_year getDraft(Account_fiscal_year et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_fiscal_year et) ;
    void updateBatch(List<Account_fiscal_year> list) ;
    Account_fiscal_year get(Integer key) ;
    boolean create(Account_fiscal_year et) ;
    void createBatch(List<Account_fiscal_year> list) ;
    Page<Account_fiscal_year> searchDefault(Account_fiscal_yearSearchContext context) ;

}



