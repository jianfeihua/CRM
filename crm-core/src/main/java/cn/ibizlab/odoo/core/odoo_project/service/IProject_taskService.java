package cn.ibizlab.odoo.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_project.domain.Project_task;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_taskSearchContext;


/**
 * 实体[Project_task] 服务对象接口
 */
public interface IProject_taskService{

    boolean update(Project_task et) ;
    void updateBatch(List<Project_task> list) ;
    Project_task getDraft(Project_task et) ;
    Project_task get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Project_task et) ;
    void createBatch(List<Project_task> list) ;
    Page<Project_task> searchDefault(Project_taskSearchContext context) ;

}



