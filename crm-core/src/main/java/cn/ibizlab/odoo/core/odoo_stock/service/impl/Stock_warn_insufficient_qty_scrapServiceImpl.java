package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_scrapSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_scrapService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_warn_insufficient_qty_scrapFeignClient;

/**
 * 实体[报废数量不足发出警告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warn_insufficient_qty_scrapServiceImpl implements IStock_warn_insufficient_qty_scrapService {

    @Autowired
    stock_warn_insufficient_qty_scrapFeignClient stock_warn_insufficient_qty_scrapFeignClient;


    @Override
    public Stock_warn_insufficient_qty_scrap getDraft(Stock_warn_insufficient_qty_scrap et) {
        et=stock_warn_insufficient_qty_scrapFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_warn_insufficient_qty_scrapFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_warn_insufficient_qty_scrapFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_warn_insufficient_qty_scrap et) {
        Stock_warn_insufficient_qty_scrap rt = stock_warn_insufficient_qty_scrapFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_warn_insufficient_qty_scrap> list){
        stock_warn_insufficient_qty_scrapFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_warn_insufficient_qty_scrap et) {
        Stock_warn_insufficient_qty_scrap rt = stock_warn_insufficient_qty_scrapFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warn_insufficient_qty_scrap> list){
        stock_warn_insufficient_qty_scrapFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_warn_insufficient_qty_scrap get(Integer id) {
		Stock_warn_insufficient_qty_scrap et=stock_warn_insufficient_qty_scrapFeignClient.get(id);
        if(et==null){
            et=new Stock_warn_insufficient_qty_scrap();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warn_insufficient_qty_scrap> searchDefault(Stock_warn_insufficient_qty_scrapSearchContext context) {
        Page<Stock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps=stock_warn_insufficient_qty_scrapFeignClient.searchDefault(context);
        return stock_warn_insufficient_qty_scraps;
    }


}


