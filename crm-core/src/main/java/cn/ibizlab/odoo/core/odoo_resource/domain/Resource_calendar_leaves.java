package cn.ibizlab.odoo.core.odoo_resource.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [休假详情] 对象
 */
@Data
public class Resource_calendar_leaves extends EntityClient implements Serializable {

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 原因
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 时间类型
     */
    @DEField(name = "time_type")
    @JSONField(name = "time_type")
    @JsonProperty("time_type")
    private String timeType;

    /**
     * 结束日期
     */
    @DEField(name = "date_to")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_to")
    private Timestamp dateTo;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 开始日期
     */
    @DEField(name = "date_from")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_from")
    private Timestamp dateFrom;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 工作时间
     */
    @JSONField(name = "calendar_id_text")
    @JsonProperty("calendar_id_text")
    private String calendarIdText;

    /**
     * 休假申请
     */
    @JSONField(name = "holiday_id_text")
    @JsonProperty("holiday_id_text")
    private String holidayIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 资源
     */
    @JSONField(name = "resource_id_text")
    @JsonProperty("resource_id_text")
    private String resourceIdText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 休假申请
     */
    @DEField(name = "holiday_id")
    @JSONField(name = "holiday_id")
    @JsonProperty("holiday_id")
    private Integer holidayId;

    /**
     * 工作时间
     */
    @DEField(name = "calendar_id")
    @JSONField(name = "calendar_id")
    @JsonProperty("calendar_id")
    private Integer calendarId;

    /**
     * 资源
     */
    @DEField(name = "resource_id")
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    private Integer resourceId;


    /**
     * 
     */
    @JSONField(name = "odooholiday")
    @JsonProperty("odooholiday")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave odooHoliday;

    /**
     * 
     */
    @JSONField(name = "odoocalendar")
    @JsonProperty("odoocalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooCalendar;

    /**
     * 
     */
    @JSONField(name = "odooresource")
    @JsonProperty("odooresource")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource odooResource;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [原因]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [时间类型]
     */
    public void setTimeType(String timeType){
        this.timeType = timeType ;
        this.modify("time_type",timeType);
    }
    /**
     * 设置 [结束日期]
     */
    public void setDateTo(Timestamp dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }
    /**
     * 设置 [开始日期]
     */
    public void setDateFrom(Timestamp dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [休假申请]
     */
    public void setHolidayId(Integer holidayId){
        this.holidayId = holidayId ;
        this.modify("holiday_id",holidayId);
    }
    /**
     * 设置 [工作时间]
     */
    public void setCalendarId(Integer calendarId){
        this.calendarId = calendarId ;
        this.modify("calendar_id",calendarId);
    }
    /**
     * 设置 [资源]
     */
    public void setResourceId(Integer resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }

}


