package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_followersSearchContext;


/**
 * 实体[Mail_followers] 服务对象接口
 */
public interface IMail_followersService{

    Mail_followers getDraft(Mail_followers et) ;
    Mail_followers get(Integer key) ;
    boolean create(Mail_followers et) ;
    void createBatch(List<Mail_followers> list) ;
    boolean update(Mail_followers et) ;
    void updateBatch(List<Mail_followers> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_followers> searchDefault(Mail_followersSearchContext context) ;

}



