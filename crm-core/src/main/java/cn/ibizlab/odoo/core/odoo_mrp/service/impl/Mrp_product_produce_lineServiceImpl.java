package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produce_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_product_produce_lineFeignClient;

/**
 * 实体[记录生产明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_product_produce_lineServiceImpl implements IMrp_product_produce_lineService {

    @Autowired
    mrp_product_produce_lineFeignClient mrp_product_produce_lineFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_product_produce_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_product_produce_lineFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_product_produce_line get(Integer id) {
		Mrp_product_produce_line et=mrp_product_produce_lineFeignClient.get(id);
        if(et==null){
            et=new Mrp_product_produce_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mrp_product_produce_line et) {
        Mrp_product_produce_line rt = mrp_product_produce_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_product_produce_line> list){
        mrp_product_produce_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public Mrp_product_produce_line getDraft(Mrp_product_produce_line et) {
        et=mrp_product_produce_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mrp_product_produce_line et) {
        Mrp_product_produce_line rt = mrp_product_produce_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_product_produce_line> list){
        mrp_product_produce_lineFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_product_produce_line> searchDefault(Mrp_product_produce_lineSearchContext context) {
        Page<Mrp_product_produce_line> mrp_product_produce_lines=mrp_product_produce_lineFeignClient.searchDefault(context);
        return mrp_product_produce_lines;
    }


}


