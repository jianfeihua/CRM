package cn.ibizlab.odoo.core.odoo_asset.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[asset_asset] 服务对象接口
 */
@Component
public class asset_assetFallback implements asset_assetFeignClient{

    public Asset_asset get(Integer id){
            return null;
     }


    public Asset_asset create(Asset_asset asset_asset){
            return null;
     }
    public Boolean createBatch(List<Asset_asset> asset_assets){
            return false;
     }

    public Asset_asset update(Integer id, Asset_asset asset_asset){
            return null;
     }
    public Boolean updateBatch(List<Asset_asset> asset_assets){
            return false;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Asset_asset> searchDefault(Asset_assetSearchContext context){
            return null;
     }


    public Page<Asset_asset> select(){
            return null;
     }

    public Asset_asset getDraft(){
            return null;
    }



}
