package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_lang;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_langSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_langService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_langFeignClient;

/**
 * 实体[语言] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_langServiceImpl implements IRes_langService {

    @Autowired
    res_langFeignClient res_langFeignClient;


    @Override
    public Res_lang getDraft(Res_lang et) {
        et=res_langFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Res_lang et) {
        Res_lang rt = res_langFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_lang> list){
        res_langFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Res_lang et) {
        Res_lang rt = res_langFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_lang> list){
        res_langFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_langFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_langFeignClient.removeBatch(idList);
    }

    @Override
    public Res_lang get(Integer id) {
		Res_lang et=res_langFeignClient.get(id);
        if(et==null){
            et=new Res_lang();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_lang> searchDefault(Res_langSearchContext context) {
        Page<Res_lang> res_langs=res_langFeignClient.searchDefault(context);
        return res_langs;
    }


}


