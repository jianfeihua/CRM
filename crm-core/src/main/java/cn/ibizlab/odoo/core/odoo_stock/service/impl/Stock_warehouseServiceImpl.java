package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouseService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_warehouseFeignClient;

/**
 * 实体[仓库] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warehouseServiceImpl implements IStock_warehouseService {

    @Autowired
    stock_warehouseFeignClient stock_warehouseFeignClient;


    @Override
    public Stock_warehouse get(Integer id) {
		Stock_warehouse et=stock_warehouseFeignClient.get(id);
        if(et==null){
            et=new Stock_warehouse();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Stock_warehouse getDraft(Stock_warehouse et) {
        et=stock_warehouseFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Stock_warehouse et) {
        Stock_warehouse rt = stock_warehouseFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warehouse> list){
        stock_warehouseFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Stock_warehouse et) {
        Stock_warehouse rt = stock_warehouseFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_warehouse> list){
        stock_warehouseFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_warehouseFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_warehouseFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warehouse> searchDefault(Stock_warehouseSearchContext context) {
        Page<Stock_warehouse> stock_warehouses=stock_warehouseFeignClient.searchDefault(context);
        return stock_warehouses;
    }


}


