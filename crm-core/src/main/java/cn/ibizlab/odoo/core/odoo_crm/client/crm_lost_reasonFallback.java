package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_lost_reason] 服务对象接口
 */
@Component
public class crm_lost_reasonFallback implements crm_lost_reasonFeignClient{

    public Crm_lost_reason update(Integer id, Crm_lost_reason crm_lost_reason){
            return null;
     }
    public Boolean updateBatch(List<Crm_lost_reason> crm_lost_reasons){
            return false;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Crm_lost_reason create(Crm_lost_reason crm_lost_reason){
            return null;
     }
    public Boolean createBatch(List<Crm_lost_reason> crm_lost_reasons){
            return false;
     }

    public Page<Crm_lost_reason> searchDefault(Crm_lost_reasonSearchContext context){
            return null;
     }


    public Crm_lost_reason get(Integer id){
            return null;
     }



    public Page<Crm_lost_reason> select(){
            return null;
     }

    public Crm_lost_reason getDraft(){
            return null;
    }



}
