package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Component
public class website_sale_payment_acquirer_onboarding_wizardFallback implements website_sale_payment_acquirer_onboarding_wizardFeignClient{

    public Page<Website_sale_payment_acquirer_onboarding_wizard> searchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context){
            return null;
     }


    public Website_sale_payment_acquirer_onboarding_wizard create(Website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean createBatch(List<Website_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
            return false;
     }



    public Website_sale_payment_acquirer_onboarding_wizard get(Integer id){
            return null;
     }


    public Website_sale_payment_acquirer_onboarding_wizard update(Integer id, Website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean updateBatch(List<Website_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Website_sale_payment_acquirer_onboarding_wizard> select(){
            return null;
     }

    public Website_sale_payment_acquirer_onboarding_wizard getDraft(){
            return null;
    }



}
