package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_advance_payment_invService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_advance_payment_invFeignClient;

/**
 * 实体[销售预付款发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_advance_payment_invServiceImpl implements ISale_advance_payment_invService {

    @Autowired
    sale_advance_payment_invFeignClient sale_advance_payment_invFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=sale_advance_payment_invFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_advance_payment_invFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Sale_advance_payment_inv et) {
        Sale_advance_payment_inv rt = sale_advance_payment_invFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_advance_payment_inv> list){
        sale_advance_payment_invFeignClient.updateBatch(list) ;
    }

    @Override
    public Sale_advance_payment_inv getDraft(Sale_advance_payment_inv et) {
        et=sale_advance_payment_invFeignClient.getDraft();
        return et;
    }

    @Override
    public Sale_advance_payment_inv get(Integer id) {
		Sale_advance_payment_inv et=sale_advance_payment_invFeignClient.get(id);
        if(et==null){
            et=new Sale_advance_payment_inv();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Sale_advance_payment_inv et) {
        Sale_advance_payment_inv rt = sale_advance_payment_invFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_advance_payment_inv> list){
        sale_advance_payment_invFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_advance_payment_inv> searchDefault(Sale_advance_payment_invSearchContext context) {
        Page<Sale_advance_payment_inv> sale_advance_payment_invs=sale_advance_payment_invFeignClient.searchDefault(context);
        return sale_advance_payment_invs;
    }


}


