package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_categoryFeignClient;

/**
 * 实体[产品种类] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_categoryServiceImpl implements IProduct_categoryService {

    @Autowired
    product_categoryFeignClient product_categoryFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=product_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_categoryFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Product_category et) {
        Product_category rt = product_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_category> list){
        product_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Product_category et) {
        Product_category rt = product_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_category> list){
        product_categoryFeignClient.updateBatch(list) ;
    }

    @Override
    @Transactional
    public boolean save(Product_category et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!product_categoryFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Product_category> list) {
        product_categoryFeignClient.saveBatch(list) ;
    }

    @Override
    public Product_category getDraft(Product_category et) {
        et=product_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean checkKey(Product_category et) {
        return product_categoryFeignClient.checkKey(et);
    }
    @Override
    public Product_category get(Integer id) {
		Product_category et=product_categoryFeignClient.get(id);
        if(et==null){
            et=new Product_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_category> searchDefault(Product_categorySearchContext context) {
        Page<Product_category> product_categorys=product_categoryFeignClient.searchDefault(context);
        return product_categorys;
    }


}


