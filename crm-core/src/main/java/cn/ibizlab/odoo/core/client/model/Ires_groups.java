package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_groups] 对象
 */
public interface Ires_groups {

    /**
     * 获取 [应用]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [应用]
     */
    public Integer getCategory_id();

    /**
     * 获取 [应用]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setComment(String comment);
    
    /**
     * 设置 [备注]
     */
    public String getComment();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getCommentDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [群组名称]
     */
    public void setFull_name(String full_name);
    
    /**
     * 设置 [群组名称]
     */
    public String getFull_name();

    /**
     * 获取 [群组名称]脏标记
     */
    public boolean getFull_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [继承]
     */
    public void setImplied_ids(String implied_ids);
    
    /**
     * 设置 [继承]
     */
    public String getImplied_ids();

    /**
     * 获取 [继承]脏标记
     */
    public boolean getImplied_idsDirtyFlag();
    /**
     * 获取 [访问菜单]
     */
    public void setMenu_access(String menu_access);
    
    /**
     * 设置 [访问菜单]
     */
    public String getMenu_access();

    /**
     * 获取 [访问菜单]脏标记
     */
    public boolean getMenu_accessDirtyFlag();
    /**
     * 获取 [访问控制]
     */
    public void setModel_access(String model_access);
    
    /**
     * 设置 [访问控制]
     */
    public String getModel_access();

    /**
     * 获取 [访问控制]脏标记
     */
    public boolean getModel_accessDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [规则]
     */
    public void setRule_groups(String rule_groups);
    
    /**
     * 设置 [规则]
     */
    public String getRule_groups();

    /**
     * 获取 [规则]脏标记
     */
    public boolean getRule_groupsDirtyFlag();
    /**
     * 获取 [共享用户组]
     */
    public void setShare(String share);
    
    /**
     * 设置 [共享用户组]
     */
    public String getShare();

    /**
     * 获取 [共享用户组]脏标记
     */
    public boolean getShareDirtyFlag();
    /**
     * 获取 [及物继承]
     */
    public void setTrans_implied_ids(String trans_implied_ids);
    
    /**
     * 设置 [及物继承]
     */
    public String getTrans_implied_ids();

    /**
     * 获取 [及物继承]脏标记
     */
    public boolean getTrans_implied_idsDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUsers(String users);
    
    /**
     * 设置 [用户]
     */
    public String getUsers();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUsersDirtyFlag();
    /**
     * 获取 [视图]
     */
    public void setView_access(String view_access);
    
    /**
     * 设置 [视图]
     */
    public String getView_access();

    /**
     * 获取 [视图]脏标记
     */
    public boolean getView_accessDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
