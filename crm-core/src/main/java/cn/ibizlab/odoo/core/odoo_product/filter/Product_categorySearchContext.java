package cn.ibizlab.odoo.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Product_category] 查询条件对象
 */
@Slf4j
@Data
public class Product_categorySearchContext extends SearchContextBase {
	private String n_name_like;//[名称]

	private String n_property_valuation_eq;//[库存计价]

	private String n_property_cost_method_eq;//[成本方法]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_parent_id_text_eq;//[上级类别]

	private String n_parent_id_text_like;//[上级类别]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_removal_strategy_id_text_eq;//[强制下架策略]

	private String n_removal_strategy_id_text_like;//[强制下架策略]

	private Integer n_parent_id_eq;//[上级类别]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_removal_strategy_id_eq;//[强制下架策略]

	private Integer n_write_uid_eq;//[最后更新人]

}



