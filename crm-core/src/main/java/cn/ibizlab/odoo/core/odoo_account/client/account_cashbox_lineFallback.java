package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cashbox_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_cashbox_line] 服务对象接口
 */
@Component
public class account_cashbox_lineFallback implements account_cashbox_lineFeignClient{

    public Account_cashbox_line update(Integer id, Account_cashbox_line account_cashbox_line){
            return null;
     }
    public Boolean updateBatch(List<Account_cashbox_line> account_cashbox_lines){
            return false;
     }



    public Page<Account_cashbox_line> searchDefault(Account_cashbox_lineSearchContext context){
            return null;
     }


    public Account_cashbox_line get(Integer id){
            return null;
     }


    public Account_cashbox_line create(Account_cashbox_line account_cashbox_line){
            return null;
     }
    public Boolean createBatch(List<Account_cashbox_line> account_cashbox_lines){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Account_cashbox_line> select(){
            return null;
     }

    public Account_cashbox_line getDraft(){
            return null;
    }



}
