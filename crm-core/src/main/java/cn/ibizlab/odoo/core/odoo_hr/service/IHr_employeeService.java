package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employeeSearchContext;


/**
 * 实体[Hr_employee] 服务对象接口
 */
public interface IHr_employeeService{

    boolean create(Hr_employee et) ;
    void createBatch(List<Hr_employee> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Hr_employee et) ;
    void updateBatch(List<Hr_employee> list) ;
    Hr_employee get(Integer key) ;
    Hr_employee getDraft(Hr_employee et) ;
    Page<Hr_employee> searchDefault(Hr_employeeSearchContext context) ;

}



