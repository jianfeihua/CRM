package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_tokenService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_payment.client.payment_tokenFeignClient;

/**
 * 实体[付款令牌] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_tokenServiceImpl implements IPayment_tokenService {

    @Autowired
    payment_tokenFeignClient payment_tokenFeignClient;


    @Override
    public boolean create(Payment_token et) {
        Payment_token rt = payment_tokenFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_token> list){
        payment_tokenFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Payment_token et) {
        Payment_token rt = payment_tokenFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Payment_token> list){
        payment_tokenFeignClient.updateBatch(list) ;
    }

    @Override
    public Payment_token getDraft(Payment_token et) {
        et=payment_tokenFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=payment_tokenFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        payment_tokenFeignClient.removeBatch(idList);
    }

    @Override
    public Payment_token get(Integer id) {
		Payment_token et=payment_tokenFeignClient.get(id);
        if(et==null){
            et=new Payment_token();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_token> searchDefault(Payment_tokenSearchContext context) {
        Page<Payment_token> payment_tokens=payment_tokenFeignClient.searchDefault(context);
        return payment_tokens;
    }


}


