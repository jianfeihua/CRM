package cn.ibizlab.odoo.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;


/**
 * 实体[Resource_test] 服务对象接口
 */
public interface IResource_testService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Resource_test et) ;
    void updateBatch(List<Resource_test> list) ;
    boolean create(Resource_test et) ;
    void createBatch(List<Resource_test> list) ;
    Resource_test get(Integer key) ;
    Resource_test getDraft(Resource_test et) ;
    Page<Resource_test> searchDefault(Resource_testSearchContext context) ;

}



