package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;


/**
 * 实体[Res_config_settings] 服务对象接口
 */
public interface IRes_config_settingsService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Res_config_settings get(Integer key) ;
    Res_config_settings getDraft(Res_config_settings et) ;
    boolean update(Res_config_settings et) ;
    void updateBatch(List<Res_config_settings> list) ;
    boolean create(Res_config_settings et) ;
    void createBatch(List<Res_config_settings> list) ;
    Page<Res_config_settings> searchDefault(Res_config_settingsSearchContext context) ;

}



