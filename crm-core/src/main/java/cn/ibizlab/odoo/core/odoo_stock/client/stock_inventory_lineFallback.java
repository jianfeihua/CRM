package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventory_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_inventory_line] 服务对象接口
 */
@Component
public class stock_inventory_lineFallback implements stock_inventory_lineFeignClient{




    public Stock_inventory_line update(Integer id, Stock_inventory_line stock_inventory_line){
            return null;
     }
    public Boolean updateBatch(List<Stock_inventory_line> stock_inventory_lines){
            return false;
     }


    public Stock_inventory_line create(Stock_inventory_line stock_inventory_line){
            return null;
     }
    public Boolean createBatch(List<Stock_inventory_line> stock_inventory_lines){
            return false;
     }

    public Page<Stock_inventory_line> searchDefault(Stock_inventory_lineSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Stock_inventory_line get(Integer id){
            return null;
     }


    public Page<Stock_inventory_line> select(){
            return null;
     }

    public Stock_inventory_line getDraft(){
            return null;
    }



}
