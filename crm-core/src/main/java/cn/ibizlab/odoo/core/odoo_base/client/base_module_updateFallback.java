package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_module_update] 服务对象接口
 */
@Component
public class base_module_updateFallback implements base_module_updateFeignClient{


    public Base_module_update update(Integer id, Base_module_update base_module_update){
            return null;
     }
    public Boolean updateBatch(List<Base_module_update> base_module_updates){
            return false;
     }



    public Base_module_update create(Base_module_update base_module_update){
            return null;
     }
    public Boolean createBatch(List<Base_module_update> base_module_updates){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Base_module_update> searchDefault(Base_module_updateSearchContext context){
            return null;
     }


    public Base_module_update get(Integer id){
            return null;
     }



    public Page<Base_module_update> select(){
            return null;
     }

    public Base_module_update getDraft(){
            return null;
    }



}
