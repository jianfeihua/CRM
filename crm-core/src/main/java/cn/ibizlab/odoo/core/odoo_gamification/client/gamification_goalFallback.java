package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[gamification_goal] 服务对象接口
 */
@Component
public class gamification_goalFallback implements gamification_goalFeignClient{

    public Page<Gamification_goal> searchDefault(Gamification_goalSearchContext context){
            return null;
     }




    public Gamification_goal get(Integer id){
            return null;
     }



    public Gamification_goal update(Integer id, Gamification_goal gamification_goal){
            return null;
     }
    public Boolean updateBatch(List<Gamification_goal> gamification_goals){
            return false;
     }


    public Gamification_goal create(Gamification_goal gamification_goal){
            return null;
     }
    public Boolean createBatch(List<Gamification_goal> gamification_goals){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Gamification_goal> select(){
            return null;
     }

    public Gamification_goal getDraft(){
            return null;
    }



}
