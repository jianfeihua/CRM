package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_expense_sheet] 对象
 */
public interface Ihr_expense_sheet {

    /**
     * 获取 [日期]
     */
    public void setAccounting_date(Timestamp accounting_date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getAccounting_date();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getAccounting_dateDirtyFlag();
    /**
     * 获取 [日记账分录]
     */
    public void setAccount_move_id(Integer account_move_id);
    
    /**
     * 设置 [日记账分录]
     */
    public Integer getAccount_move_id();

    /**
     * 获取 [日记账分录]脏标记
     */
    public boolean getAccount_move_idDirtyFlag();
    /**
     * 获取 [日记账分录]
     */
    public void setAccount_move_id_text(String account_move_id_text);
    
    /**
     * 设置 [日记账分录]
     */
    public String getAccount_move_id_text();

    /**
     * 获取 [日记账分录]脏标记
     */
    public boolean getAccount_move_id_textDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [员工家庭地址]
     */
    public void setAddress_id(Integer address_id);
    
    /**
     * 设置 [员工家庭地址]
     */
    public Integer getAddress_id();

    /**
     * 获取 [员工家庭地址]脏标记
     */
    public boolean getAddress_idDirtyFlag();
    /**
     * 获取 [员工家庭地址]
     */
    public void setAddress_id_text(String address_id_text);
    
    /**
     * 设置 [员工家庭地址]
     */
    public String getAddress_id_text();

    /**
     * 获取 [员工家庭地址]脏标记
     */
    public boolean getAddress_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setAttachment_number(Integer attachment_number);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getAttachment_number();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getAttachment_numberDirtyFlag();
    /**
     * 获取 [银行日记账]
     */
    public void setBank_journal_id(Integer bank_journal_id);
    
    /**
     * 设置 [银行日记账]
     */
    public Integer getBank_journal_id();

    /**
     * 获取 [银行日记账]脏标记
     */
    public boolean getBank_journal_idDirtyFlag();
    /**
     * 获取 [银行日记账]
     */
    public void setBank_journal_id_text(String bank_journal_id_text);
    
    /**
     * 设置 [银行日记账]
     */
    public String getBank_journal_id_text();

    /**
     * 获取 [银行日记账]脏标记
     */
    public boolean getBank_journal_id_textDirtyFlag();
    /**
     * 获取 [可重置]
     */
    public void setCan_reset(String can_reset);
    
    /**
     * 设置 [可重置]
     */
    public String getCan_reset();

    /**
     * 获取 [可重置]脏标记
     */
    public boolean getCan_resetDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id(Integer employee_id);
    
    /**
     * 设置 [员工]
     */
    public Integer getEmployee_id();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_idDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id_text(String employee_id_text);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee_id_text();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_id_textDirtyFlag();
    /**
     * 获取 [费用行]
     */
    public void setExpense_line_ids(String expense_line_ids);
    
    /**
     * 设置 [费用行]
     */
    public String getExpense_line_ids();

    /**
     * 获取 [费用行]脏标记
     */
    public boolean getExpense_line_idsDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [处理行中不同币种]
     */
    public void setIs_multiple_currency(String is_multiple_currency);
    
    /**
     * 设置 [处理行中不同币种]
     */
    public String getIs_multiple_currency();

    /**
     * 获取 [处理行中不同币种]脏标记
     */
    public boolean getIs_multiple_currencyDirtyFlag();
    /**
     * 获取 [费用日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [费用日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [费用日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [费用日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [费用日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [费用日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [信息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [信息]
     */
    public String getMessage_ids();

    /**
     * 获取 [信息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [费用报告摘要]
     */
    public void setName(String name);
    
    /**
     * 设置 [费用报告摘要]
     */
    public String getName();

    /**
     * 获取 [费用报告摘要]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [支付]
     */
    public void setPayment_mode(String payment_mode);
    
    /**
     * 设置 [支付]
     */
    public String getPayment_mode();

    /**
     * 获取 [支付]脏标记
     */
    public boolean getPayment_modeDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [金额总计]
     */
    public void setTotal_amount(Double total_amount);
    
    /**
     * 设置 [金额总计]
     */
    public Double getTotal_amount();

    /**
     * 获取 [金额总计]脏标记
     */
    public boolean getTotal_amountDirtyFlag();
    /**
     * 获取 [经理]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [经理]
     */
    public Integer getUser_id();

    /**
     * 获取 [经理]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [经理]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [经理]
     */
    public String getUser_id_text();

    /**
     * 获取 [经理]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
