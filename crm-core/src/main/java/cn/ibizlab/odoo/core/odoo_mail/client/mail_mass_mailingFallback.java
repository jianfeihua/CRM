package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing] 服务对象接口
 */
@Component
public class mail_mass_mailingFallback implements mail_mass_mailingFeignClient{


    public Mail_mass_mailing create(Mail_mass_mailing mail_mass_mailing){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing> mail_mass_mailings){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mail_mass_mailing> searchDefault(Mail_mass_mailingSearchContext context){
            return null;
     }


    public Mail_mass_mailing update(Integer id, Mail_mass_mailing mail_mass_mailing){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing> mail_mass_mailings){
            return false;
     }



    public Mail_mass_mailing get(Integer id){
            return null;
     }


    public Page<Mail_mass_mailing> select(){
            return null;
     }

    public Mail_mass_mailing getDraft(){
            return null;
    }



}
