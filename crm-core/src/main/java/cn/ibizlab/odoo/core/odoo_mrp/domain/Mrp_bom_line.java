package cn.ibizlab.odoo.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [物料清单明细行] 对象
 */
@Data
public class Mrp_bom_line extends EntityClient implements Serializable {

    /**
     * 应用于变体
     */
    @JSONField(name = "attribute_value_ids")
    @JsonProperty("attribute_value_ids")
    private String attributeValueIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 数量
     */
    @DEField(name = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 参考BOM中的BOM行
     */
    @JSONField(name = "child_line_ids")
    @JsonProperty("child_line_ids")
    private String childLineIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 有效的产品属性值
     */
    @JSONField(name = "valid_product_attribute_value_ids")
    @JsonProperty("valid_product_attribute_value_ids")
    private String validProductAttributeValueIds;

    /**
     * 有附件
     */
    @JSONField(name = "has_attachments")
    @JsonProperty("has_attachments")
    private String hasAttachments;

    /**
     * 子 BOM
     */
    @JSONField(name = "child_bom_id")
    @JsonProperty("child_bom_id")
    private Integer childBomId;

    /**
     * 产品模板
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;

    /**
     * 计量单位
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 工艺
     */
    @JSONField(name = "routing_id_text")
    @JsonProperty("routing_id_text")
    private String routingIdText;

    /**
     * 父产品模板
     */
    @JSONField(name = "parent_product_tmpl_id")
    @JsonProperty("parent_product_tmpl_id")
    private Integer parentProductTmplId;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 零件
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 投料作业
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    private String operationIdText;

    /**
     * 投料作业
     */
    @DEField(name = "operation_id")
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    private Integer operationId;

    /**
     * 计量单位
     */
    @DEField(name = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 父级 BoM
     */
    @DEField(name = "bom_id")
    @JSONField(name = "bom_id")
    @JsonProperty("bom_id")
    private Integer bomId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 工艺
     */
    @DEField(name = "routing_id")
    @JSONField(name = "routing_id")
    @JsonProperty("routing_id")
    private Integer routingId;

    /**
     * 零件
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;


    /**
     * 
     */
    @JSONField(name = "odoobom")
    @JsonProperty("odoobom")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom odooBom;

    /**
     * 
     */
    @JSONField(name = "odoooperation")
    @JsonProperty("odoooperation")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter odooOperation;

    /**
     * 
     */
    @JSONField(name = "odoorouting")
    @JsonProperty("odoorouting")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing odooRouting;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [投料作业]
     */
    public void setOperationId(Integer operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }
    /**
     * 设置 [计量单位]
     */
    public void setProductUomId(Integer productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }
    /**
     * 设置 [父级 BoM]
     */
    public void setBomId(Integer bomId){
        this.bomId = bomId ;
        this.modify("bom_id",bomId);
    }
    /**
     * 设置 [工艺]
     */
    public void setRoutingId(Integer routingId){
        this.routingId = routingId ;
        this.modify("routing_id",routingId);
    }
    /**
     * 设置 [零件]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

}


