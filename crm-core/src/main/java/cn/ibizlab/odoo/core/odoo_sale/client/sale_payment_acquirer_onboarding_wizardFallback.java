package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Component
public class sale_payment_acquirer_onboarding_wizardFallback implements sale_payment_acquirer_onboarding_wizardFeignClient{

    public Sale_payment_acquirer_onboarding_wizard update(Integer id, Sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean updateBatch(List<Sale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Sale_payment_acquirer_onboarding_wizard get(Integer id){
            return null;
     }


    public Page<Sale_payment_acquirer_onboarding_wizard> searchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context){
            return null;
     }



    public Sale_payment_acquirer_onboarding_wizard create(Sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean createBatch(List<Sale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards){
            return false;
     }


    public Page<Sale_payment_acquirer_onboarding_wizard> select(){
            return null;
     }

    public Sale_payment_acquirer_onboarding_wizard getDraft(){
            return null;
    }



}
