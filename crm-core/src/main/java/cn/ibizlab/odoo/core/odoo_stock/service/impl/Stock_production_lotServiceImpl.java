package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_production_lotSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_production_lotService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_production_lotFeignClient;

/**
 * 实体[批次/序列号] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_production_lotServiceImpl implements IStock_production_lotService {

    @Autowired
    stock_production_lotFeignClient stock_production_lotFeignClient;


    @Override
    public boolean update(Stock_production_lot et) {
        Stock_production_lot rt = stock_production_lotFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_production_lot> list){
        stock_production_lotFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_production_lotFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_production_lotFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_production_lot get(Integer id) {
		Stock_production_lot et=stock_production_lotFeignClient.get(id);
        if(et==null){
            et=new Stock_production_lot();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Stock_production_lot et) {
        Stock_production_lot rt = stock_production_lotFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_production_lot> list){
        stock_production_lotFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_production_lot getDraft(Stock_production_lot et) {
        et=stock_production_lotFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_production_lot> searchDefault(Stock_production_lotSearchContext context) {
        Page<Stock_production_lot> stock_production_lots=stock_production_lotFeignClient.searchDefault(context);
        return stock_production_lots;
    }


}


