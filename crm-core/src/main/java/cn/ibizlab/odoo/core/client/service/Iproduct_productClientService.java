package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_product;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_product] 服务对象接口
 */
public interface Iproduct_productClientService{

    public Iproduct_product createModel() ;

    public void create(Iproduct_product product_product);

    public void get(Iproduct_product product_product);

    public Page<Iproduct_product> fetchDefault(SearchContext context);

    public void removeBatch(List<Iproduct_product> product_products);

    public void createBatch(List<Iproduct_product> product_products);

    public void remove(Iproduct_product product_product);

    public void updateBatch(List<Iproduct_product> product_products);

    public void update(Iproduct_product product_product);

    public Page<Iproduct_product> select(SearchContext context);

    public void save(Iproduct_product product_product);

    public void checkKey(Iproduct_product product_product);

    public void getDraft(Iproduct_product product_product);

}
