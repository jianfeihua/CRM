package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_parameterSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_parameterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_parameterFeignClient;

/**
 * 实体[Asset Parameters] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_parameterServiceImpl implements IMro_pm_parameterService {

    @Autowired
    mro_pm_parameterFeignClient mro_pm_parameterFeignClient;


    @Override
    public boolean update(Mro_pm_parameter et) {
        Mro_pm_parameter rt = mro_pm_parameterFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_pm_parameter> list){
        mro_pm_parameterFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_pm_parameterFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_pm_parameterFeignClient.removeBatch(idList);
    }

    @Override
    public Mro_pm_parameter getDraft(Mro_pm_parameter et) {
        et=mro_pm_parameterFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mro_pm_parameter et) {
        Mro_pm_parameter rt = mro_pm_parameterFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_parameter> list){
        mro_pm_parameterFeignClient.createBatch(list) ;
    }

    @Override
    public Mro_pm_parameter get(Integer id) {
		Mro_pm_parameter et=mro_pm_parameterFeignClient.get(id);
        if(et==null){
            et=new Mro_pm_parameter();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_parameter> searchDefault(Mro_pm_parameterSearchContext context) {
        Page<Mro_pm_parameter> mro_pm_parameters=mro_pm_parameterFeignClient.searchDefault(context);
        return mro_pm_parameters;
    }


}


