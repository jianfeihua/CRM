package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_model_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_reconcile_model_templateFeignClient;

/**
 * 实体[核销模型模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_reconcile_model_templateServiceImpl implements IAccount_reconcile_model_templateService {

    @Autowired
    account_reconcile_model_templateFeignClient account_reconcile_model_templateFeignClient;


    @Override
    public Account_reconcile_model_template get(Integer id) {
		Account_reconcile_model_template et=account_reconcile_model_templateFeignClient.get(id);
        if(et==null){
            et=new Account_reconcile_model_template();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Account_reconcile_model_template et) {
        Account_reconcile_model_template rt = account_reconcile_model_templateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_reconcile_model_template> list){
        account_reconcile_model_templateFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_reconcile_model_templateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_reconcile_model_templateFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_reconcile_model_template et) {
        Account_reconcile_model_template rt = account_reconcile_model_templateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_reconcile_model_template> list){
        account_reconcile_model_templateFeignClient.createBatch(list) ;
    }

    @Override
    public Account_reconcile_model_template getDraft(Account_reconcile_model_template et) {
        et=account_reconcile_model_templateFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_reconcile_model_template> searchDefault(Account_reconcile_model_templateSearchContext context) {
        Page<Account_reconcile_model_template> account_reconcile_model_templates=account_reconcile_model_templateFeignClient.searchDefault(context);
        return account_reconcile_model_templates;
    }


}


