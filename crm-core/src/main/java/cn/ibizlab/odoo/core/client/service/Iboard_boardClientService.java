package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iboard_board;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[board_board] 服务对象接口
 */
public interface Iboard_boardClientService{

    public Iboard_board createModel() ;

    public void removeBatch(List<Iboard_board> board_boards);

    public void remove(Iboard_board board_board);

    public void create(Iboard_board board_board);

    public void update(Iboard_board board_board);

    public Page<Iboard_board> fetchDefault(SearchContext context);

    public void createBatch(List<Iboard_board> board_boards);

    public void get(Iboard_board board_board);

    public void updateBatch(List<Iboard_board> board_boards);

    public Page<Iboard_board> select(SearchContext context);

    public void getDraft(Iboard_board board_board);

}
