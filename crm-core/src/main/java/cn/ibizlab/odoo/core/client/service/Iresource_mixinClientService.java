package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iresource_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_mixin] 服务对象接口
 */
public interface Iresource_mixinClientService{

    public Iresource_mixin createModel() ;

    public void create(Iresource_mixin resource_mixin);

    public void createBatch(List<Iresource_mixin> resource_mixins);

    public Page<Iresource_mixin> fetchDefault(SearchContext context);

    public void remove(Iresource_mixin resource_mixin);

    public void removeBatch(List<Iresource_mixin> resource_mixins);

    public void get(Iresource_mixin resource_mixin);

    public void update(Iresource_mixin resource_mixin);

    public void updateBatch(List<Iresource_mixin> resource_mixins);

    public Page<Iresource_mixin> select(SearchContext context);

    public void getDraft(Iresource_mixin resource_mixin);

}
