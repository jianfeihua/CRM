package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_alias_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_alias_mixin] 服务对象接口
 */
@Component
public class mail_alias_mixinFallback implements mail_alias_mixinFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Mail_alias_mixin> searchDefault(Mail_alias_mixinSearchContext context){
            return null;
     }



    public Mail_alias_mixin update(Integer id, Mail_alias_mixin mail_alias_mixin){
            return null;
     }
    public Boolean updateBatch(List<Mail_alias_mixin> mail_alias_mixins){
            return false;
     }


    public Mail_alias_mixin create(Mail_alias_mixin mail_alias_mixin){
            return null;
     }
    public Boolean createBatch(List<Mail_alias_mixin> mail_alias_mixins){
            return false;
     }

    public Mail_alias_mixin get(Integer id){
            return null;
     }



    public Page<Mail_alias_mixin> select(){
            return null;
     }

    public Mail_alias_mixin getDraft(){
            return null;
    }



}
