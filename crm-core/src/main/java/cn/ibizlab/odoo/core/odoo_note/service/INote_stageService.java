package cn.ibizlab.odoo.core.odoo_note.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_note.domain.Note_stage;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_stageSearchContext;


/**
 * 实体[Note_stage] 服务对象接口
 */
public interface INote_stageService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Note_stage et) ;
    void createBatch(List<Note_stage> list) ;
    boolean update(Note_stage et) ;
    void updateBatch(List<Note_stage> list) ;
    Note_stage getDraft(Note_stage et) ;
    Note_stage get(Integer key) ;
    Page<Note_stage> searchDefault(Note_stageSearchContext context) ;

}



