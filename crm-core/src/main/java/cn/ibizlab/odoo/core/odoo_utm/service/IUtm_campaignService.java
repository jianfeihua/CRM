package cn.ibizlab.odoo.core.odoo_utm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_campaignSearchContext;


/**
 * 实体[Utm_campaign] 服务对象接口
 */
public interface IUtm_campaignService{

    boolean update(Utm_campaign et) ;
    void updateBatch(List<Utm_campaign> list) ;
    Utm_campaign get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Utm_campaign et) ;
    void createBatch(List<Utm_campaign> list) ;
    Utm_campaign getDraft(Utm_campaign et) ;
    Page<Utm_campaign> searchDefault(Utm_campaignSearchContext context) ;

}



