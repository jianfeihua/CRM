package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icalendar_event_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_event_type] 服务对象接口
 */
public interface Icalendar_event_typeClientService{

    public Icalendar_event_type createModel() ;

    public void createBatch(List<Icalendar_event_type> calendar_event_types);

    public Page<Icalendar_event_type> fetchDefault(SearchContext context);

    public void update(Icalendar_event_type calendar_event_type);

    public void get(Icalendar_event_type calendar_event_type);

    public void create(Icalendar_event_type calendar_event_type);

    public void remove(Icalendar_event_type calendar_event_type);

    public void updateBatch(List<Icalendar_event_type> calendar_event_types);

    public void removeBatch(List<Icalendar_event_type> calendar_event_types);

    public Page<Icalendar_event_type> select(SearchContext context);

    public void getDraft(Icalendar_event_type calendar_event_type);

}
