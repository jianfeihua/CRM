package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_public_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_public_categoryFeignClient;

/**
 * 实体[网站产品目录] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_public_categoryServiceImpl implements IProduct_public_categoryService {

    @Autowired
    product_public_categoryFeignClient product_public_categoryFeignClient;


    @Override
    public Product_public_category get(Integer id) {
		Product_public_category et=product_public_categoryFeignClient.get(id);
        if(et==null){
            et=new Product_public_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Product_public_category getDraft(Product_public_category et) {
        et=product_public_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Product_public_category et) {
        Product_public_category rt = product_public_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_public_category> list){
        product_public_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_public_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_public_categoryFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Product_public_category et) {
        Product_public_category rt = product_public_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_public_category> list){
        product_public_categoryFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_public_category> searchDefault(Product_public_categorySearchContext context) {
        Page<Product_public_category> product_public_categorys=product_public_categoryFeignClient.searchDefault(context);
        return product_public_categorys;
    }


}


