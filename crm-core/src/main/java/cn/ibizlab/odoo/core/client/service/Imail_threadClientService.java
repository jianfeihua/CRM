package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_thread;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_thread] 服务对象接口
 */
public interface Imail_threadClientService{

    public Imail_thread createModel() ;

    public void updateBatch(List<Imail_thread> mail_threads);

    public void update(Imail_thread mail_thread);

    public void get(Imail_thread mail_thread);

    public void remove(Imail_thread mail_thread);

    public void createBatch(List<Imail_thread> mail_threads);

    public void removeBatch(List<Imail_thread> mail_threads);

    public Page<Imail_thread> fetchDefault(SearchContext context);

    public void create(Imail_thread mail_thread);

    public Page<Imail_thread> select(SearchContext context);

    public void getDraft(Imail_thread mail_thread);

}
