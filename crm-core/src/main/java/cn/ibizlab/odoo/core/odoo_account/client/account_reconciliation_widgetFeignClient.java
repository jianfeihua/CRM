package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_reconciliation_widget] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-reconciliation-widget", fallback = account_reconciliation_widgetFallback.class)
public interface account_reconciliation_widgetFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/account_reconciliation_widgets/{id}")
    Account_reconciliation_widget update(@PathVariable("id") Integer id,@RequestBody Account_reconciliation_widget account_reconciliation_widget);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_reconciliation_widgets/batch")
    Boolean updateBatch(@RequestBody List<Account_reconciliation_widget> account_reconciliation_widgets);



    @RequestMapping(method = RequestMethod.GET, value = "/account_reconciliation_widgets/{id}")
    Account_reconciliation_widget get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets/searchdefault")
    Page<Account_reconciliation_widget> searchDefault(@RequestBody Account_reconciliation_widgetSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets")
    Account_reconciliation_widget create(@RequestBody Account_reconciliation_widget account_reconciliation_widget);

    @RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets/batch")
    Boolean createBatch(@RequestBody List<Account_reconciliation_widget> account_reconciliation_widgets);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_reconciliation_widgets/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_reconciliation_widgets/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/account_reconciliation_widgets/select")
    Page<Account_reconciliation_widget> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_reconciliation_widgets/getdraft")
    Account_reconciliation_widget getDraft();


}
