package cn.ibizlab.odoo.core.odoo_web_editor.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;


/**
 * 实体[Web_editor_converter_test_sub] 服务对象接口
 */
public interface IWeb_editor_converter_test_subService{

    boolean update(Web_editor_converter_test_sub et) ;
    void updateBatch(List<Web_editor_converter_test_sub> list) ;
    Web_editor_converter_test_sub getDraft(Web_editor_converter_test_sub et) ;
    Web_editor_converter_test_sub get(Integer key) ;
    boolean create(Web_editor_converter_test_sub et) ;
    void createBatch(List<Web_editor_converter_test_sub> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Web_editor_converter_test_sub> searchDefault(Web_editor_converter_test_subSearchContext context) ;

}



