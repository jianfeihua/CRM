package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_automatic_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_partner_merge_automatic_wizardFeignClient;

/**
 * 实体[合并业务伙伴向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_partner_merge_automatic_wizardServiceImpl implements IBase_partner_merge_automatic_wizardService {

    @Autowired
    base_partner_merge_automatic_wizardFeignClient base_partner_merge_automatic_wizardFeignClient;


    @Override
    public boolean update(Base_partner_merge_automatic_wizard et) {
        Base_partner_merge_automatic_wizard rt = base_partner_merge_automatic_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_partner_merge_automatic_wizard> list){
        base_partner_merge_automatic_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_partner_merge_automatic_wizard getDraft(Base_partner_merge_automatic_wizard et) {
        et=base_partner_merge_automatic_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Base_partner_merge_automatic_wizard et) {
        Base_partner_merge_automatic_wizard rt = base_partner_merge_automatic_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_partner_merge_automatic_wizard> list){
        base_partner_merge_automatic_wizardFeignClient.createBatch(list) ;
    }

    @Override
    public Base_partner_merge_automatic_wizard get(Integer id) {
		Base_partner_merge_automatic_wizard et=base_partner_merge_automatic_wizardFeignClient.get(id);
        if(et==null){
            et=new Base_partner_merge_automatic_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_partner_merge_automatic_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_partner_merge_automatic_wizardFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_partner_merge_automatic_wizard> searchDefault(Base_partner_merge_automatic_wizardSearchContext context) {
        Page<Base_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards=base_partner_merge_automatic_wizardFeignClient.searchDefault(context);
        return base_partner_merge_automatic_wizards;
    }


}


