package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_assignation_log;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_assignation_log] 服务对象接口
 */
public interface Ifleet_vehicle_assignation_logClientService{

    public Ifleet_vehicle_assignation_log createModel() ;

    public void removeBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs);

    public void updateBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs);

    public void update(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

    public void createBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs);

    public void get(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

    public void create(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

    public Page<Ifleet_vehicle_assignation_log> fetchDefault(SearchContext context);

    public void remove(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

    public Page<Ifleet_vehicle_assignation_log> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

}
