package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_blacklist_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_blacklist_mixinFeignClient;

/**
 * 实体[mixin邮件黑名单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_blacklist_mixinServiceImpl implements IMail_blacklist_mixinService {

    @Autowired
    mail_blacklist_mixinFeignClient mail_blacklist_mixinFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mail_blacklist_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_blacklist_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Mail_blacklist_mixin et) {
        Mail_blacklist_mixin rt = mail_blacklist_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_blacklist_mixin> list){
        mail_blacklist_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_blacklist_mixin get(Integer id) {
		Mail_blacklist_mixin et=mail_blacklist_mixinFeignClient.get(id);
        if(et==null){
            et=new Mail_blacklist_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_blacklist_mixin et) {
        Mail_blacklist_mixin rt = mail_blacklist_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_blacklist_mixin> list){
        mail_blacklist_mixinFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_blacklist_mixin getDraft(Mail_blacklist_mixin et) {
        et=mail_blacklist_mixinFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_blacklist_mixin> searchDefault(Mail_blacklist_mixinSearchContext context) {
        Page<Mail_blacklist_mixin> mail_blacklist_mixins=mail_blacklist_mixinFeignClient.searchDefault(context);
        return mail_blacklist_mixins;
    }


}


