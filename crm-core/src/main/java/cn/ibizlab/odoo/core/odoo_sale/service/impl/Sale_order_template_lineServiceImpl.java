package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_template_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_order_template_lineFeignClient;

/**
 * 实体[报价单模板行] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_order_template_lineServiceImpl implements ISale_order_template_lineService {

    @Autowired
    sale_order_template_lineFeignClient sale_order_template_lineFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=sale_order_template_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_order_template_lineFeignClient.removeBatch(idList);
    }

    @Override
    public Sale_order_template_line getDraft(Sale_order_template_line et) {
        et=sale_order_template_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Sale_order_template_line et) {
        Sale_order_template_line rt = sale_order_template_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order_template_line> list){
        sale_order_template_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Sale_order_template_line get(Integer id) {
		Sale_order_template_line et=sale_order_template_lineFeignClient.get(id);
        if(et==null){
            et=new Sale_order_template_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Sale_order_template_line et) {
        Sale_order_template_line rt = sale_order_template_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_order_template_line> list){
        sale_order_template_lineFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order_template_line> searchDefault(Sale_order_template_lineSearchContext context) {
        Page<Sale_order_template_line> sale_order_template_lines=sale_order_template_lineFeignClient.searchDefault(context);
        return sale_order_template_lines;
    }


}


