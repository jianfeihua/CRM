package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_goal] 服务对象接口
 */
@FeignClient(value = "odoo-gamification", contextId = "gamification-goal", fallback = gamification_goalFallback.class)
public interface gamification_goalFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goals/searchdefault")
    Page<Gamification_goal> searchDefault(@RequestBody Gamification_goalSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goals/{id}")
    Gamification_goal get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goals/{id}")
    Gamification_goal update(@PathVariable("id") Integer id,@RequestBody Gamification_goal gamification_goal);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goals/batch")
    Boolean updateBatch(@RequestBody List<Gamification_goal> gamification_goals);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goals")
    Gamification_goal create(@RequestBody Gamification_goal gamification_goal);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goals/batch")
    Boolean createBatch(@RequestBody List<Gamification_goal> gamification_goals);


    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goals/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goals/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goals/select")
    Page<Gamification_goal> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goals/getdraft")
    Gamification_goal getDraft();


}
