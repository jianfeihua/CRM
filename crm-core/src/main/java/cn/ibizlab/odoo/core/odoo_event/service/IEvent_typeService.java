package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_type;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;


/**
 * 实体[Event_type] 服务对象接口
 */
public interface IEvent_typeService{

    Event_type get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Event_type et) ;
    void updateBatch(List<Event_type> list) ;
    Event_type getDraft(Event_type et) ;
    boolean create(Event_type et) ;
    void createBatch(List<Event_type> list) ;
    Page<Event_type> searchDefault(Event_typeSearchContext context) ;

}



