package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_holidays_summary_deptService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_holidays_summary_deptFeignClient;

/**
 * 实体[按部门的休假摘要报表] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_holidays_summary_deptServiceImpl implements IHr_holidays_summary_deptService {

    @Autowired
    hr_holidays_summary_deptFeignClient hr_holidays_summary_deptFeignClient;


    @Override
    public Hr_holidays_summary_dept getDraft(Hr_holidays_summary_dept et) {
        et=hr_holidays_summary_deptFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Hr_holidays_summary_dept et) {
        Hr_holidays_summary_dept rt = hr_holidays_summary_deptFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_holidays_summary_dept> list){
        hr_holidays_summary_deptFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_holidays_summary_deptFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_holidays_summary_deptFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Hr_holidays_summary_dept et) {
        Hr_holidays_summary_dept rt = hr_holidays_summary_deptFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_holidays_summary_dept> list){
        hr_holidays_summary_deptFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_holidays_summary_dept get(Integer id) {
		Hr_holidays_summary_dept et=hr_holidays_summary_deptFeignClient.get(id);
        if(et==null){
            et=new Hr_holidays_summary_dept();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_holidays_summary_dept> searchDefault(Hr_holidays_summary_deptSearchContext context) {
        Page<Hr_holidays_summary_dept> hr_holidays_summary_depts=hr_holidays_summary_deptFeignClient.searchDefault(context);
        return hr_holidays_summary_depts;
    }


}


