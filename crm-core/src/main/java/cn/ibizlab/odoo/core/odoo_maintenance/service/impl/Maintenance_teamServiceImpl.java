package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_teamService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_teamFeignClient;

/**
 * 实体[保养团队] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_teamServiceImpl implements IMaintenance_teamService {

    @Autowired
    maintenance_teamFeignClient maintenance_teamFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=maintenance_teamFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        maintenance_teamFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Maintenance_team et) {
        Maintenance_team rt = maintenance_teamFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Maintenance_team> list){
        maintenance_teamFeignClient.updateBatch(list) ;
    }

    @Override
    public Maintenance_team getDraft(Maintenance_team et) {
        et=maintenance_teamFeignClient.getDraft();
        return et;
    }

    @Override
    public Maintenance_team get(Integer id) {
		Maintenance_team et=maintenance_teamFeignClient.get(id);
        if(et==null){
            et=new Maintenance_team();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Maintenance_team et) {
        Maintenance_team rt = maintenance_teamFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_team> list){
        maintenance_teamFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_team> searchDefault(Maintenance_teamSearchContext context) {
        Page<Maintenance_team> maintenance_teams=maintenance_teamFeignClient.searchDefault(context);
        return maintenance_teams;
    }


}


