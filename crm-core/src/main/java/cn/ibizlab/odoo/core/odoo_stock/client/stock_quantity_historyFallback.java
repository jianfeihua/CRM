package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantity_historySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_quantity_history] 服务对象接口
 */
@Component
public class stock_quantity_historyFallback implements stock_quantity_historyFeignClient{

    public Stock_quantity_history update(Integer id, Stock_quantity_history stock_quantity_history){
            return null;
     }
    public Boolean updateBatch(List<Stock_quantity_history> stock_quantity_histories){
            return false;
     }


    public Stock_quantity_history create(Stock_quantity_history stock_quantity_history){
            return null;
     }
    public Boolean createBatch(List<Stock_quantity_history> stock_quantity_histories){
            return false;
     }



    public Page<Stock_quantity_history> searchDefault(Stock_quantity_historySearchContext context){
            return null;
     }


    public Stock_quantity_history get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_quantity_history> select(){
            return null;
     }

    public Stock_quantity_history getDraft(){
            return null;
    }



}
