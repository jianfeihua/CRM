package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_bom_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_bom_line] 服务对象接口
 */
public interface Imrp_bom_lineClientService{

    public Imrp_bom_line createModel() ;

    public void updateBatch(List<Imrp_bom_line> mrp_bom_lines);

    public Page<Imrp_bom_line> fetchDefault(SearchContext context);

    public void createBatch(List<Imrp_bom_line> mrp_bom_lines);

    public void removeBatch(List<Imrp_bom_line> mrp_bom_lines);

    public void update(Imrp_bom_line mrp_bom_line);

    public void get(Imrp_bom_line mrp_bom_line);

    public void remove(Imrp_bom_line mrp_bom_line);

    public void create(Imrp_bom_line mrp_bom_line);

    public Page<Imrp_bom_line> select(SearchContext context);

    public void getDraft(Imrp_bom_line mrp_bom_line);

}
