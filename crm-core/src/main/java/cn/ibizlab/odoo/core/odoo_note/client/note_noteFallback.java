package cn.ibizlab.odoo.core.odoo_note.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_note;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_noteSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[note_note] 服务对象接口
 */
@Component
public class note_noteFallback implements note_noteFeignClient{


    public Note_note update(Integer id, Note_note note_note){
            return null;
     }
    public Boolean updateBatch(List<Note_note> note_notes){
            return false;
     }


    public Page<Note_note> searchDefault(Note_noteSearchContext context){
            return null;
     }


    public Note_note get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Note_note create(Note_note note_note){
            return null;
     }
    public Boolean createBatch(List<Note_note> note_notes){
            return false;
     }

    public Page<Note_note> select(){
            return null;
     }

    public Note_note getDraft(){
            return null;
    }



}
