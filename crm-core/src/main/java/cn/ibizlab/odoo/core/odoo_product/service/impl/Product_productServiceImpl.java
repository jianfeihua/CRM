package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_productService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_productFeignClient;

/**
 * 实体[产品] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_productServiceImpl implements IProduct_productService {

    @Autowired
    product_productFeignClient product_productFeignClient;


    @Override
    public Product_product get(Integer id) {
		Product_product et=product_productFeignClient.get(id);
        if(et==null){
            et=new Product_product();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    @Transactional
    public boolean save(Product_product et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!product_productFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Product_product> list) {
        product_productFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean create(Product_product et) {
        Product_product rt = product_productFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_product> list){
        product_productFeignClient.createBatch(list) ;
    }

    @Override
    public boolean checkKey(Product_product et) {
        return product_productFeignClient.checkKey(et);
    }
    @Override
    public boolean update(Product_product et) {
        Product_product rt = product_productFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_product> list){
        product_productFeignClient.updateBatch(list) ;
    }

    @Override
    public Product_product getDraft(Product_product et) {
        et=product_productFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_productFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_productFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_product> searchDefault(Product_productSearchContext context) {
        Page<Product_product> product_products=product_productFeignClient.searchDefault(context);
        return product_products;
    }


}


