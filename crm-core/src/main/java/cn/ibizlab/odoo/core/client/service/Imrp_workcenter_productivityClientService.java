package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter_productivity] 服务对象接口
 */
public interface Imrp_workcenter_productivityClientService{

    public Imrp_workcenter_productivity createModel() ;

    public void updateBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities);

    public void remove(Imrp_workcenter_productivity mrp_workcenter_productivity);

    public void update(Imrp_workcenter_productivity mrp_workcenter_productivity);

    public Page<Imrp_workcenter_productivity> fetchDefault(SearchContext context);

    public void create(Imrp_workcenter_productivity mrp_workcenter_productivity);

    public void createBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities);

    public void removeBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities);

    public void get(Imrp_workcenter_productivity mrp_workcenter_productivity);

    public Page<Imrp_workcenter_productivity> select(SearchContext context);

    public void getDraft(Imrp_workcenter_productivity mrp_workcenter_productivity);

}
