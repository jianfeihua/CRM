package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_order] 服务对象接口
 */
@Component
public class sale_orderFallback implements sale_orderFeignClient{


    public Sale_order update(Integer id, Sale_order sale_order){
            return null;
     }
    public Boolean updateBatch(List<Sale_order> sale_orders){
            return false;
     }


    public Page<Sale_order> searchDefault(Sale_orderSearchContext context){
            return null;
     }



    public Sale_order create(Sale_order sale_order){
            return null;
     }
    public Boolean createBatch(List<Sale_order> sale_orders){
            return false;
     }


    public Sale_order get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Sale_order> select(){
            return null;
     }

    public Boolean save(Sale_order sale_order){
            return false;
     }
    public Boolean saveBatch(List<Sale_order> sale_orders){
            return false;
     }

    public Boolean checkKey(Sale_order sale_order){
            return false;
     }


    public Sale_order getDraft(){
            return null;
    }



}
