package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_document] 服务对象接口
 */
@FeignClient(value = "odoo-mrp", contextId = "mrp-document", fallback = mrp_documentFallback.class)
public interface mrp_documentFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_documents/searchdefault")
    Page<Mrp_document> searchDefault(@RequestBody Mrp_documentSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_documents")
    Mrp_document create(@RequestBody Mrp_document mrp_document);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_documents/batch")
    Boolean createBatch(@RequestBody List<Mrp_document> mrp_documents);




    @RequestMapping(method = RequestMethod.GET, value = "/mrp_documents/{id}")
    Mrp_document get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_documents/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_documents/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_documents/{id}")
    Mrp_document update(@PathVariable("id") Integer id,@RequestBody Mrp_document mrp_document);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_documents/batch")
    Boolean updateBatch(@RequestBody List<Mrp_document> mrp_documents);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_documents/select")
    Page<Mrp_document> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_documents/getdraft")
    Mrp_document getDraft();


}
