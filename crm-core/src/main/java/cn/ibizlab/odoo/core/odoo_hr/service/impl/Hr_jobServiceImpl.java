package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_jobSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_jobService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_jobFeignClient;

/**
 * 实体[工作岗位] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_jobServiceImpl implements IHr_jobService {

    @Autowired
    hr_jobFeignClient hr_jobFeignClient;


    @Override
    public boolean update(Hr_job et) {
        Hr_job rt = hr_jobFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_job> list){
        hr_jobFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_job getDraft(Hr_job et) {
        et=hr_jobFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Hr_job et) {
        Hr_job rt = hr_jobFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_job> list){
        hr_jobFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_jobFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_jobFeignClient.removeBatch(idList);
    }

    @Override
    public Hr_job get(Integer id) {
		Hr_job et=hr_jobFeignClient.get(id);
        if(et==null){
            et=new Hr_job();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_job> searchDefault(Hr_jobSearchContext context) {
        Page<Hr_job> hr_jobs=hr_jobFeignClient.searchDefault(context);
        return hr_jobs;
    }


}


