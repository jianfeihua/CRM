package cn.ibizlab.odoo.core.odoo_sms.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_apiService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sms.client.sms_apiFeignClient;

/**
 * 实体[短信API] 服务对象接口实现
 */
@Slf4j
@Service
public class Sms_apiServiceImpl implements ISms_apiService {

    @Autowired
    sms_apiFeignClient sms_apiFeignClient;


    @Override
    public Sms_api get(Integer id) {
		Sms_api et=sms_apiFeignClient.get(id);
        if(et==null){
            et=new Sms_api();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=sms_apiFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sms_apiFeignClient.removeBatch(idList);
    }

    @Override
    public Sms_api getDraft(Sms_api et) {
        et=sms_apiFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Sms_api et) {
        Sms_api rt = sms_apiFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sms_api> list){
        sms_apiFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Sms_api et) {
        Sms_api rt = sms_apiFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sms_api> list){
        sms_apiFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sms_api> searchDefault(Sms_apiSearchContext context) {
        Page<Sms_api> sms_apis=sms_apiFeignClient.searchDefault(context);
        return sms_apis;
    }


}


