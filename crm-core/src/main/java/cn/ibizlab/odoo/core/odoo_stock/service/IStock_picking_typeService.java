package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;


/**
 * 实体[Stock_picking_type] 服务对象接口
 */
public interface IStock_picking_typeService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_picking_type et) ;
    void updateBatch(List<Stock_picking_type> list) ;
    Stock_picking_type getDraft(Stock_picking_type et) ;
    boolean create(Stock_picking_type et) ;
    void createBatch(List<Stock_picking_type> list) ;
    Stock_picking_type get(Integer key) ;
    Page<Stock_picking_type> searchDefault(Stock_picking_typeSearchContext context) ;

}



