package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_installerSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_installerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_config_installerFeignClient;

/**
 * 实体[配置安装器] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_config_installerServiceImpl implements IRes_config_installerService {

    @Autowired
    res_config_installerFeignClient res_config_installerFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=res_config_installerFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_config_installerFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Res_config_installer et) {
        Res_config_installer rt = res_config_installerFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_config_installer> list){
        res_config_installerFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Res_config_installer et) {
        Res_config_installer rt = res_config_installerFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_config_installer> list){
        res_config_installerFeignClient.updateBatch(list) ;
    }

    @Override
    public Res_config_installer getDraft(Res_config_installer et) {
        et=res_config_installerFeignClient.getDraft();
        return et;
    }

    @Override
    public Res_config_installer get(Integer id) {
		Res_config_installer et=res_config_installerFeignClient.get(id);
        if(et==null){
            et=new Res_config_installer();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_config_installer> searchDefault(Res_config_installerSearchContext context) {
        Page<Res_config_installer> res_config_installers=res_config_installerFeignClient.searchDefault(context);
        return res_config_installers;
    }


}


