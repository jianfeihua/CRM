package cn.ibizlab.odoo.core.odoo_digest.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[digest_tip] 服务对象接口
 */
@Component
public class digest_tipFallback implements digest_tipFeignClient{


    public Page<Digest_tip> searchDefault(Digest_tipSearchContext context){
            return null;
     }


    public Digest_tip get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Digest_tip update(Integer id, Digest_tip digest_tip){
            return null;
     }
    public Boolean updateBatch(List<Digest_tip> digest_tips){
            return false;
     }


    public Digest_tip create(Digest_tip digest_tip){
            return null;
     }
    public Boolean createBatch(List<Digest_tip> digest_tips){
            return false;
     }


    public Page<Digest_tip> select(){
            return null;
     }

    public Digest_tip getDraft(){
            return null;
    }



}
