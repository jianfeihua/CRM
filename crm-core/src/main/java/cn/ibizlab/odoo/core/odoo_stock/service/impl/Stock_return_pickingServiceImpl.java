package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_pickingSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_return_pickingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_return_pickingFeignClient;

/**
 * 实体[退回拣货] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_return_pickingServiceImpl implements IStock_return_pickingService {

    @Autowired
    stock_return_pickingFeignClient stock_return_pickingFeignClient;


    @Override
    public boolean update(Stock_return_picking et) {
        Stock_return_picking rt = stock_return_pickingFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_return_picking> list){
        stock_return_pickingFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_return_picking getDraft(Stock_return_picking et) {
        et=stock_return_pickingFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Stock_return_picking et) {
        Stock_return_picking rt = stock_return_pickingFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_return_picking> list){
        stock_return_pickingFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_return_picking get(Integer id) {
		Stock_return_picking et=stock_return_pickingFeignClient.get(id);
        if(et==null){
            et=new Stock_return_picking();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_return_pickingFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_return_pickingFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_return_picking> searchDefault(Stock_return_pickingSearchContext context) {
        Page<Stock_return_picking> stock_return_pickings=stock_return_pickingFeignClient.searchDefault(context);
        return stock_return_pickings;
    }


}


