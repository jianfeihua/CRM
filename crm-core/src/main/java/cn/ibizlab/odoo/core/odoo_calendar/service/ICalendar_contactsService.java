package cn.ibizlab.odoo.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;


/**
 * 实体[Calendar_contacts] 服务对象接口
 */
public interface ICalendar_contactsService{

    Calendar_contacts get(Integer key) ;
    Calendar_contacts getDraft(Calendar_contacts et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Calendar_contacts et) ;
    void updateBatch(List<Calendar_contacts> list) ;
    boolean create(Calendar_contacts et) ;
    void createBatch(List<Calendar_contacts> list) ;
    Page<Calendar_contacts> searchDefault(Calendar_contactsSearchContext context) ;

}



