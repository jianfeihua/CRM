package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_requestSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_request] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-request", fallback = mro_requestFallback.class)
public interface mro_requestFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_requests/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_requests/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_requests")
    Mro_request create(@RequestBody Mro_request mro_request);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_requests/batch")
    Boolean createBatch(@RequestBody List<Mro_request> mro_requests);



    @RequestMapping(method = RequestMethod.PUT, value = "/mro_requests/{id}")
    Mro_request update(@PathVariable("id") Integer id,@RequestBody Mro_request mro_request);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_requests/batch")
    Boolean updateBatch(@RequestBody List<Mro_request> mro_requests);





    @RequestMapping(method = RequestMethod.POST, value = "/mro_requests/searchdefault")
    Page<Mro_request> searchDefault(@RequestBody Mro_requestSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_requests/{id}")
    Mro_request get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_requests/select")
    Page<Mro_request> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_requests/getdraft")
    Mro_request getDraft();


}
