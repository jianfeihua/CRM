package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_importSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_language_import] 服务对象接口
 */
@Component
public class base_language_importFallback implements base_language_importFeignClient{


    public Base_language_import create(Base_language_import base_language_import){
            return null;
     }
    public Boolean createBatch(List<Base_language_import> base_language_imports){
            return false;
     }

    public Base_language_import update(Integer id, Base_language_import base_language_import){
            return null;
     }
    public Boolean updateBatch(List<Base_language_import> base_language_imports){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_language_import get(Integer id){
            return null;
     }




    public Page<Base_language_import> searchDefault(Base_language_importSearchContext context){
            return null;
     }


    public Page<Base_language_import> select(){
            return null;
     }

    public Base_language_import getDraft(){
            return null;
    }



}
