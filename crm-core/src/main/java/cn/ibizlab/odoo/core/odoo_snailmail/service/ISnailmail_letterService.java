package cn.ibizlab.odoo.core.odoo_snailmail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;


/**
 * 实体[Snailmail_letter] 服务对象接口
 */
public interface ISnailmail_letterService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Snailmail_letter get(Integer key) ;
    boolean update(Snailmail_letter et) ;
    void updateBatch(List<Snailmail_letter> list) ;
    Snailmail_letter getDraft(Snailmail_letter et) ;
    boolean create(Snailmail_letter et) ;
    void createBatch(List<Snailmail_letter> list) ;
    Page<Snailmail_letter> searchDefault(Snailmail_letterSearchContext context) ;

}



