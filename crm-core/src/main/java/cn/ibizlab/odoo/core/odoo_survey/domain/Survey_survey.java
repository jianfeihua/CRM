package cn.ibizlab.odoo.core.odoo_survey.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [问卷] 对象
 */
@Data
public class Survey_survey extends EntityClient implements Serializable {

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 用户可返回
     */
    @DEField(name = "users_can_go_back")
    @JSONField(name = "users_can_go_back")
    @JsonProperty("users_can_go_back")
    private String usersCanGoBack;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 已开始的调查数量
     */
    @JSONField(name = "tot_start_survey")
    @JsonProperty("tot_start_survey")
    private Integer totStartSurvey;

    /**
     * 结果链接
     */
    @JSONField(name = "result_url")
    @JsonProperty("result_url")
    private String resultUrl;

    /**
     * 打印链接
     */
    @JSONField(name = "print_url")
    @JsonProperty("print_url")
    private String printUrl;

    /**
     * 测验模式
     */
    @DEField(name = "quizz_mode")
    @JSONField(name = "quizz_mode")
    @JsonProperty("quizz_mode")
    private String quizzMode;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 已完成的调查数量
     */
    @JSONField(name = "tot_comp_survey")
    @JsonProperty("tot_comp_survey")
    private Integer totCompSurvey;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 公开链接（HTML版）
     */
    @JSONField(name = "public_url_html")
    @JsonProperty("public_url_html")
    private String publicUrlHtml;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 用户回应
     */
    @JSONField(name = "user_input_ids")
    @JsonProperty("user_input_ids")
    private String userInputIds;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 页面
     */
    @JSONField(name = "page_ids")
    @JsonProperty("page_ids")
    private String pageIds;

    /**
     * 需要登录
     */
    @DEField(name = "auth_required")
    @JSONField(name = "auth_required")
    @JsonProperty("auth_required")
    private String authRequired;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 已发送调查者数量
     */
    @JSONField(name = "tot_sent_survey")
    @JsonProperty("tot_sent_survey")
    private Integer totSentSurvey;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 已经设计了
     */
    @JSONField(name = "designed")
    @JsonProperty("designed")
    private String designed;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 称谓
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;

    /**
     * 感谢留言
     */
    @DEField(name = "thank_you_message")
    @JSONField(name = "thank_you_message")
    @JsonProperty("thank_you_message")
    private String thankYouMessage;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 公开链接
     */
    @JSONField(name = "public_url")
    @JsonProperty("public_url")
    private String publicUrl;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 已关闭
     */
    @JSONField(name = "is_closed")
    @JsonProperty("is_closed")
    private String isClosed;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 阶段
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;

    /**
     * 邮件模板
     */
    @JSONField(name = "email_template_id_text")
    @JsonProperty("email_template_id_text")
    private String emailTemplateIdText;

    /**
     * 阶段
     */
    @DEField(name = "stage_id")
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Integer stageId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 邮件模板
     */
    @DEField(name = "email_template_id")
    @JSONField(name = "email_template_id")
    @JsonProperty("email_template_id")
    private Integer emailTemplateId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooemailtemplate")
    @JsonProperty("odooemailtemplate")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template odooEmailTemplate;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoostage")
    @JsonProperty("odoostage")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage odooStage;




    /**
     * 设置 [用户可返回]
     */
    public void setUsersCanGoBack(String usersCanGoBack){
        this.usersCanGoBack = usersCanGoBack ;
        this.modify("users_can_go_back",usersCanGoBack);
    }
    /**
     * 设置 [测验模式]
     */
    public void setQuizzMode(String quizzMode){
        this.quizzMode = quizzMode ;
        this.modify("quizz_mode",quizzMode);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [需要登录]
     */
    public void setAuthRequired(String authRequired){
        this.authRequired = authRequired ;
        this.modify("auth_required",authRequired);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [称谓]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }
    /**
     * 设置 [感谢留言]
     */
    public void setThankYouMessage(String thankYouMessage){
        this.thankYouMessage = thankYouMessage ;
        this.modify("thank_you_message",thankYouMessage);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [阶段]
     */
    public void setStageId(Integer stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }
    /**
     * 设置 [邮件模板]
     */
    public void setEmailTemplateId(Integer emailTemplateId){
        this.emailTemplateId = emailTemplateId ;
        this.modify("email_template_id",emailTemplateId);
    }

}


