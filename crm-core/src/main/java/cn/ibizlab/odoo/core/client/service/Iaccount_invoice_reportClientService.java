package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_report] 服务对象接口
 */
public interface Iaccount_invoice_reportClientService{

    public Iaccount_invoice_report createModel() ;

    public void createBatch(List<Iaccount_invoice_report> account_invoice_reports);

    public void removeBatch(List<Iaccount_invoice_report> account_invoice_reports);

    public void updateBatch(List<Iaccount_invoice_report> account_invoice_reports);

    public void remove(Iaccount_invoice_report account_invoice_report);

    public Page<Iaccount_invoice_report> fetchDefault(SearchContext context);

    public void get(Iaccount_invoice_report account_invoice_report);

    public void create(Iaccount_invoice_report account_invoice_report);

    public void update(Iaccount_invoice_report account_invoice_report);

    public Page<Iaccount_invoice_report> select(SearchContext context);

    public void getDraft(Iaccount_invoice_report account_invoice_report);

}
