package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipurchase_bill_union;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[purchase_bill_union] 服务对象接口
 */
public interface Ipurchase_bill_unionClientService{

    public Ipurchase_bill_union createModel() ;

    public void remove(Ipurchase_bill_union purchase_bill_union);

    public void create(Ipurchase_bill_union purchase_bill_union);

    public Page<Ipurchase_bill_union> fetchDefault(SearchContext context);

    public void get(Ipurchase_bill_union purchase_bill_union);

    public void createBatch(List<Ipurchase_bill_union> purchase_bill_unions);

    public void removeBatch(List<Ipurchase_bill_union> purchase_bill_unions);

    public void updateBatch(List<Ipurchase_bill_union> purchase_bill_unions);

    public void update(Ipurchase_bill_union purchase_bill_union);

    public Page<Ipurchase_bill_union> select(SearchContext context);

    public void getDraft(Ipurchase_bill_union purchase_bill_union);

}
