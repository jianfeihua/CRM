package cn.ibizlab.odoo.core.odoo_im_livechat.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [实时聊天支持操作员报告] 对象
 */
@Data
public class Im_livechat_report_operator extends EntityClient implements Serializable {

    /**
     * 该回答了
     */
    @DEField(name = "time_to_answer")
    @JSONField(name = "time_to_answer")
    @JsonProperty("time_to_answer")
    private Double timeToAnswer;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 会话的开始日期
     */
    @DEField(name = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * # 会话
     */
    @DEField(name = "nbr_channel")
    @JSONField(name = "nbr_channel")
    @JsonProperty("nbr_channel")
    private Integer nbrChannel;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 平均时间
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 渠道
     */
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    private String livechatChannelIdText;

    /**
     * 对话
     */
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    private String channelIdText;

    /**
     * 运算符
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 对话
     */
    @DEField(name = "channel_id")
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Integer channelId;

    /**
     * 运算符
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 渠道
     */
    @DEField(name = "livechat_channel_id")
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    private Integer livechatChannelId;


    /**
     * 
     */
    @JSONField(name = "odoolivechatchannel")
    @JsonProperty("odoolivechatchannel")
    private cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel odooLivechatChannel;

    /**
     * 
     */
    @JSONField(name = "odoochannel")
    @JsonProperty("odoochannel")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel odooChannel;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;




    /**
     * 设置 [该回答了]
     */
    public void setTimeToAnswer(Double timeToAnswer){
        this.timeToAnswer = timeToAnswer ;
        this.modify("time_to_answer",timeToAnswer);
    }
    /**
     * 设置 [会话的开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }
    /**
     * 设置 [# 会话]
     */
    public void setNbrChannel(Integer nbrChannel){
        this.nbrChannel = nbrChannel ;
        this.modify("nbr_channel",nbrChannel);
    }
    /**
     * 设置 [平均时间]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }
    /**
     * 设置 [对话]
     */
    public void setChannelId(Integer channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }
    /**
     * 设置 [运算符]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [渠道]
     */
    public void setLivechatChannelId(Integer livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }

}


