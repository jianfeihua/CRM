package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_task_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_project.client.project_task_typeFeignClient;

/**
 * 实体[任务阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_task_typeServiceImpl implements IProject_task_typeService {

    @Autowired
    project_task_typeFeignClient project_task_typeFeignClient;


    @Override
    public Project_task_type get(Integer id) {
		Project_task_type et=project_task_typeFeignClient.get(id);
        if(et==null){
            et=new Project_task_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Project_task_type et) {
        Project_task_type rt = project_task_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_task_type> list){
        project_task_typeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=project_task_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        project_task_typeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Project_task_type et) {
        Project_task_type rt = project_task_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Project_task_type> list){
        project_task_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public Project_task_type getDraft(Project_task_type et) {
        et=project_task_typeFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_task_type> searchDefault(Project_task_typeSearchContext context) {
        Page<Project_task_type> project_task_types=project_task_typeFeignClient.searchDefault(context);
        return project_task_types;
    }


}


