package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_group;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_group] 服务对象接口
 */
public interface Iaccount_groupClientService{

    public Iaccount_group createModel() ;

    public void removeBatch(List<Iaccount_group> account_groups);

    public Page<Iaccount_group> fetchDefault(SearchContext context);

    public void update(Iaccount_group account_group);

    public void create(Iaccount_group account_group);

    public void createBatch(List<Iaccount_group> account_groups);

    public void get(Iaccount_group account_group);

    public void remove(Iaccount_group account_group);

    public void updateBatch(List<Iaccount_group> account_groups);

    public Page<Iaccount_group> select(SearchContext context);

    public void getDraft(Iaccount_group account_group);

}
