package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_groups] 服务对象接口
 */
@Component
public class res_groupsFallback implements res_groupsFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Res_groups create(Res_groups res_groups){
            return null;
     }
    public Boolean createBatch(List<Res_groups> res_groups){
            return false;
     }

    public Res_groups update(Integer id, Res_groups res_groups){
            return null;
     }
    public Boolean updateBatch(List<Res_groups> res_groups){
            return false;
     }



    public Res_groups get(Integer id){
            return null;
     }



    public Page<Res_groups> searchDefault(Res_groupsSearchContext context){
            return null;
     }


    public Page<Res_groups> select(){
            return null;
     }

    public Res_groups getDraft(){
            return null;
    }



}
