package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;


/**
 * 实体[Mail_mass_mailing_campaign] 服务对象接口
 */
public interface IMail_mass_mailing_campaignService{

    Mail_mass_mailing_campaign get(Integer key) ;
    Mail_mass_mailing_campaign getDraft(Mail_mass_mailing_campaign et) ;
    boolean create(Mail_mass_mailing_campaign et) ;
    void createBatch(List<Mail_mass_mailing_campaign> list) ;
    boolean update(Mail_mass_mailing_campaign et) ;
    void updateBatch(List<Mail_mass_mailing_campaign> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_mass_mailing_campaign> searchDefault(Mail_mass_mailing_campaignSearchContext context) ;

}



