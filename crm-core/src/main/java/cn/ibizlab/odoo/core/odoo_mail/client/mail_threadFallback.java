package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_threadSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_thread] 服务对象接口
 */
@Component
public class mail_threadFallback implements mail_threadFeignClient{


    public Mail_thread update(Integer id, Mail_thread mail_thread){
            return null;
     }
    public Boolean updateBatch(List<Mail_thread> mail_threads){
            return false;
     }


    public Mail_thread get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Mail_thread> searchDefault(Mail_threadSearchContext context){
            return null;
     }


    public Mail_thread create(Mail_thread mail_thread){
            return null;
     }
    public Boolean createBatch(List<Mail_thread> mail_threads){
            return false;
     }

    public Page<Mail_thread> select(){
            return null;
     }

    public Mail_thread getDraft(){
            return null;
    }



}
