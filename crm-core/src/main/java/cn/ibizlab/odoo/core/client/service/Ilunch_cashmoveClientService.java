package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ilunch_cashmove;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_cashmove] 服务对象接口
 */
public interface Ilunch_cashmoveClientService{

    public Ilunch_cashmove createModel() ;

    public void createBatch(List<Ilunch_cashmove> lunch_cashmoves);

    public Page<Ilunch_cashmove> fetchDefault(SearchContext context);

    public void updateBatch(List<Ilunch_cashmove> lunch_cashmoves);

    public void removeBatch(List<Ilunch_cashmove> lunch_cashmoves);

    public void remove(Ilunch_cashmove lunch_cashmove);

    public void create(Ilunch_cashmove lunch_cashmove);

    public void update(Ilunch_cashmove lunch_cashmove);

    public void get(Ilunch_cashmove lunch_cashmove);

    public Page<Ilunch_cashmove> select(SearchContext context);

    public void getDraft(Ilunch_cashmove lunch_cashmove);

}
