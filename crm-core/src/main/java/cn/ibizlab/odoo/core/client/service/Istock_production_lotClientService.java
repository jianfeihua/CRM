package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_production_lot;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_production_lot] 服务对象接口
 */
public interface Istock_production_lotClientService{

    public Istock_production_lot createModel() ;

    public void update(Istock_production_lot stock_production_lot);

    public void updateBatch(List<Istock_production_lot> stock_production_lots);

    public void createBatch(List<Istock_production_lot> stock_production_lots);

    public Page<Istock_production_lot> fetchDefault(SearchContext context);

    public void get(Istock_production_lot stock_production_lot);

    public void removeBatch(List<Istock_production_lot> stock_production_lots);

    public void create(Istock_production_lot stock_production_lot);

    public void remove(Istock_production_lot stock_production_lot);

    public Page<Istock_production_lot> select(SearchContext context);

    public void getDraft(Istock_production_lot stock_production_lot);

}
