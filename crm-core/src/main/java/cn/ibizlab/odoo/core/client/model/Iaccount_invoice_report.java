package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_invoice_report] 对象
 */
public interface Iaccount_invoice_report {

    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id(Integer account_analytic_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAccount_analytic_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id_text(String account_analytic_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAccount_analytic_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_id_textDirtyFlag();
    /**
     * 获取 [收／付款账户]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [收／付款账户]
     */
    public Integer getAccount_id();

    /**
     * 获取 [收／付款账户]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [收／付款账户]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [收／付款账户]
     */
    public String getAccount_id_text();

    /**
     * 获取 [收／付款账户]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [收入/费用科目]
     */
    public void setAccount_line_id(Integer account_line_id);
    
    /**
     * 设置 [收入/费用科目]
     */
    public Integer getAccount_line_id();

    /**
     * 获取 [收入/费用科目]脏标记
     */
    public boolean getAccount_line_idDirtyFlag();
    /**
     * 获取 [收入/费用科目]
     */
    public void setAccount_line_id_text(String account_line_id_text);
    
    /**
     * 设置 [收入/费用科目]
     */
    public String getAccount_line_id_text();

    /**
     * 获取 [收入/费用科目]脏标记
     */
    public boolean getAccount_line_id_textDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setAmount_total(Double amount_total);
    
    /**
     * 设置 [总计]
     */
    public Double getAmount_total();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getAmount_totalDirtyFlag();
    /**
     * 获取 [产品类别]
     */
    public void setCateg_id(Integer categ_id);
    
    /**
     * 设置 [产品类别]
     */
    public Integer getCateg_id();

    /**
     * 获取 [产品类别]脏标记
     */
    public boolean getCateg_idDirtyFlag();
    /**
     * 获取 [产品类别]
     */
    public void setCateg_id_text(String categ_id_text);
    
    /**
     * 设置 [产品类别]
     */
    public String getCateg_id_text();

    /**
     * 获取 [产品类别]脏标记
     */
    public boolean getCateg_id_textDirtyFlag();
    /**
     * 获取 [业务伙伴公司]
     */
    public void setCommercial_partner_id(Integer commercial_partner_id);
    
    /**
     * 设置 [业务伙伴公司]
     */
    public Integer getCommercial_partner_id();

    /**
     * 获取 [业务伙伴公司]脏标记
     */
    public boolean getCommercial_partner_idDirtyFlag();
    /**
     * 获取 [业务伙伴公司]
     */
    public void setCommercial_partner_id_text(String commercial_partner_id_text);
    
    /**
     * 设置 [业务伙伴公司]
     */
    public String getCommercial_partner_id_text();

    /**
     * 获取 [业务伙伴公司]脏标记
     */
    public boolean getCommercial_partner_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [业务伙伴的国家]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [业务伙伴的国家]
     */
    public Integer getCountry_id();

    /**
     * 获取 [业务伙伴的国家]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [业务伙伴的国家]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [业务伙伴的国家]
     */
    public String getCountry_id_text();

    /**
     * 获取 [业务伙伴的国家]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [汇率]
     */
    public void setCurrency_rate(Double currency_rate);
    
    /**
     * 设置 [汇率]
     */
    public Double getCurrency_rate();

    /**
     * 获取 [汇率]脏标记
     */
    public boolean getCurrency_rateDirtyFlag();
    /**
     * 获取 [开票日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [开票日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [开票日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [到期日期]
     */
    public void setDate_due(Timestamp date_due);
    
    /**
     * 设置 [到期日期]
     */
    public Timestamp getDate_due();

    /**
     * 获取 [到期日期]脏标记
     */
    public boolean getDate_dueDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id(Integer fiscal_position_id);
    
    /**
     * 设置 [税科目调整]
     */
    public Integer getFiscal_position_id();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_idDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id_text(String fiscal_position_id_text);
    
    /**
     * 设置 [税科目调整]
     */
    public String getFiscal_position_id_text();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id(Integer invoice_id);
    
    /**
     * 设置 [发票]
     */
    public Integer getInvoice_id();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id_text(String invoice_id_text);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_id_text();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_id_textDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [行数]
     */
    public void setNbr(Integer nbr);
    
    /**
     * 设置 [行数]
     */
    public Integer getNbr();

    /**
     * 获取 [行数]脏标记
     */
    public boolean getNbrDirtyFlag();
    /**
     * 获取 [发票 #]
     */
    public void setNumber(String number);
    
    /**
     * 设置 [发票 #]
     */
    public String getNumber();

    /**
     * 获取 [发票 #]脏标记
     */
    public boolean getNumberDirtyFlag();
    /**
     * 获取 [银行账户]
     */
    public void setPartner_bank_id(Integer partner_bank_id);
    
    /**
     * 设置 [银行账户]
     */
    public Integer getPartner_bank_id();

    /**
     * 获取 [银行账户]脏标记
     */
    public boolean getPartner_bank_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id(Integer payment_term_id);
    
    /**
     * 设置 [付款条款]
     */
    public Integer getPayment_term_id();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_idDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id_text(String payment_term_id_text);
    
    /**
     * 设置 [付款条款]
     */
    public String getPayment_term_id_text();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_id_textDirtyFlag();
    /**
     * 获取 [平均价格]
     */
    public void setPrice_average(Double price_average);
    
    /**
     * 设置 [平均价格]
     */
    public Double getPrice_average();

    /**
     * 获取 [平均价格]脏标记
     */
    public boolean getPrice_averageDirtyFlag();
    /**
     * 获取 [不含税总计]
     */
    public void setPrice_total(Double price_total);
    
    /**
     * 设置 [不含税总计]
     */
    public Double getPrice_total();

    /**
     * 获取 [不含税总计]脏标记
     */
    public boolean getPrice_totalDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [到期金额]
     */
    public void setResidual(Double residual);
    
    /**
     * 设置 [到期金额]
     */
    public Double getResidual();

    /**
     * 获取 [到期金额]脏标记
     */
    public boolean getResidualDirtyFlag();
    /**
     * 获取 [发票状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [发票状态]
     */
    public String getState();

    /**
     * 获取 [发票状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [类型]
     */
    public String getType();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [参考计量单位]
     */
    public void setUom_name(String uom_name);
    
    /**
     * 设置 [参考计量单位]
     */
    public String getUom_name();

    /**
     * 获取 [参考计量单位]脏标记
     */
    public boolean getUom_nameDirtyFlag();
    /**
     * 获取 [本币平均价格]
     */
    public void setUser_currency_price_average(Double user_currency_price_average);
    
    /**
     * 设置 [本币平均价格]
     */
    public Double getUser_currency_price_average();

    /**
     * 获取 [本币平均价格]脏标记
     */
    public boolean getUser_currency_price_averageDirtyFlag();
    /**
     * 获取 [当前货币不含税总计]
     */
    public void setUser_currency_price_total(Double user_currency_price_total);
    
    /**
     * 设置 [当前货币不含税总计]
     */
    public Double getUser_currency_price_total();

    /**
     * 获取 [当前货币不含税总计]脏标记
     */
    public boolean getUser_currency_price_totalDirtyFlag();
    /**
     * 获取 [余额总计]
     */
    public void setUser_currency_residual(Double user_currency_residual);
    
    /**
     * 设置 [余额总计]
     */
    public Double getUser_currency_residual();

    /**
     * 获取 [余额总计]脏标记
     */
    public boolean getUser_currency_residualDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
