package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_usersService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_usersFeignClient;

/**
 * 实体[用户] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_usersServiceImpl implements IRes_usersService {

    @Autowired
    res_usersFeignClient res_usersFeignClient;


    @Override
    public boolean update(Res_users et) {
        Res_users rt = res_usersFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_users> list){
        res_usersFeignClient.updateBatch(list) ;
    }

    @Override
    @Transactional
    public boolean save(Res_users et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!res_usersFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Res_users> list) {
        res_usersFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean create(Res_users et) {
        Res_users rt = res_usersFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_users> list){
        res_usersFeignClient.createBatch(list) ;
    }

    @Override
    public boolean checkKey(Res_users et) {
        return res_usersFeignClient.checkKey(et);
    }
    @Override
    public Res_users get(Integer id) {
		Res_users et=res_usersFeignClient.get(id);
        if(et==null){
            et=new Res_users();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_usersFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_usersFeignClient.removeBatch(idList);
    }

    @Override
    public Res_users getDraft(Res_users et) {
        et=res_usersFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_users> searchDefault(Res_usersSearchContext context) {
        Page<Res_users> res_userss=res_usersFeignClient.searchDefault(context);
        return res_userss;
    }


}


