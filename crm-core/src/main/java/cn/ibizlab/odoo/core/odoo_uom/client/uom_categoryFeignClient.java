package cn.ibizlab.odoo.core.odoo_uom.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[uom_category] 服务对象接口
 */
@FeignClient(value = "odoo-uom", contextId = "uom-category", fallback = uom_categoryFallback.class)
public interface uom_categoryFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/uom_categories/{id}")
    Uom_category get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/uom_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/uom_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/uom_categories")
    Uom_category create(@RequestBody Uom_category uom_category);

    @RequestMapping(method = RequestMethod.POST, value = "/uom_categories/batch")
    Boolean createBatch(@RequestBody List<Uom_category> uom_categories);



    @RequestMapping(method = RequestMethod.POST, value = "/uom_categories/searchdefault")
    Page<Uom_category> searchDefault(@RequestBody Uom_categorySearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/uom_categories/{id}")
    Uom_category update(@PathVariable("id") Integer id,@RequestBody Uom_category uom_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/uom_categories/batch")
    Boolean updateBatch(@RequestBody List<Uom_category> uom_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/uom_categories/select")
    Page<Uom_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/uom_categories/getdraft")
    Uom_category getDraft();


}
