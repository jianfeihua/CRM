package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badgeSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badgeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_badgeFeignClient;

/**
 * 实体[游戏化徽章] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_badgeServiceImpl implements IGamification_badgeService {

    @Autowired
    gamification_badgeFeignClient gamification_badgeFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_badgeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_badgeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Gamification_badge et) {
        Gamification_badge rt = gamification_badgeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_badge> list){
        gamification_badgeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Gamification_badge et) {
        Gamification_badge rt = gamification_badgeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_badge> list){
        gamification_badgeFeignClient.createBatch(list) ;
    }

    @Override
    public Gamification_badge get(Integer id) {
		Gamification_badge et=gamification_badgeFeignClient.get(id);
        if(et==null){
            et=new Gamification_badge();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Gamification_badge getDraft(Gamification_badge et) {
        et=gamification_badgeFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_badge> searchDefault(Gamification_badgeSearchContext context) {
        Page<Gamification_badge> gamification_badges=gamification_badgeFeignClient.searchDefault(context);
        return gamification_badges;
    }


}


