package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_recruitment_stage] 对象
 */
public interface Ihr_recruitment_stage {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [在招聘管道收起]
     */
    public void setFold(String fold);
    
    /**
     * 设置 [在招聘管道收起]
     */
    public String getFold();

    /**
     * 获取 [在招聘管道收起]脏标记
     */
    public boolean getFoldDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [具体职位]
     */
    public void setJob_id(Integer job_id);
    
    /**
     * 设置 [具体职位]
     */
    public Integer getJob_id();

    /**
     * 获取 [具体职位]脏标记
     */
    public boolean getJob_idDirtyFlag();
    /**
     * 获取 [具体职位]
     */
    public void setJob_id_text(String job_id_text);
    
    /**
     * 设置 [具体职位]
     */
    public String getJob_id_text();

    /**
     * 获取 [具体职位]脏标记
     */
    public boolean getJob_id_textDirtyFlag();
    /**
     * 获取 [红色的看板标签]
     */
    public void setLegend_blocked(String legend_blocked);
    
    /**
     * 设置 [红色的看板标签]
     */
    public String getLegend_blocked();

    /**
     * 获取 [红色的看板标签]脏标记
     */
    public boolean getLegend_blockedDirtyFlag();
    /**
     * 获取 [绿色看板标签]
     */
    public void setLegend_done(String legend_done);
    
    /**
     * 设置 [绿色看板标签]
     */
    public String getLegend_done();

    /**
     * 获取 [绿色看板标签]脏标记
     */
    public boolean getLegend_doneDirtyFlag();
    /**
     * 获取 [灰色看板标签]
     */
    public void setLegend_normal(String legend_normal);
    
    /**
     * 设置 [灰色看板标签]
     */
    public String getLegend_normal();

    /**
     * 获取 [灰色看板标签]脏标记
     */
    public boolean getLegend_normalDirtyFlag();
    /**
     * 获取 [阶段名]
     */
    public void setName(String name);
    
    /**
     * 设置 [阶段名]
     */
    public String getName();

    /**
     * 获取 [阶段名]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [要求]
     */
    public void setRequirements(String requirements);
    
    /**
     * 设置 [要求]
     */
    public String getRequirements();

    /**
     * 获取 [要求]脏标记
     */
    public boolean getRequirementsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [自动发邮件]
     */
    public void setTemplate_id(Integer template_id);
    
    /**
     * 设置 [自动发邮件]
     */
    public Integer getTemplate_id();

    /**
     * 获取 [自动发邮件]脏标记
     */
    public boolean getTemplate_idDirtyFlag();
    /**
     * 获取 [自动发邮件]
     */
    public void setTemplate_id_text(String template_id_text);
    
    /**
     * 设置 [自动发邮件]
     */
    public String getTemplate_id_text();

    /**
     * 获取 [自动发邮件]脏标记
     */
    public boolean getTemplate_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
