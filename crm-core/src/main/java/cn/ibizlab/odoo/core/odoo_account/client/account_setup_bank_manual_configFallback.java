package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_setup_bank_manual_config] 服务对象接口
 */
@Component
public class account_setup_bank_manual_configFallback implements account_setup_bank_manual_configFeignClient{

    public Account_setup_bank_manual_config update(Integer id, Account_setup_bank_manual_config account_setup_bank_manual_config){
            return null;
     }
    public Boolean updateBatch(List<Account_setup_bank_manual_config> account_setup_bank_manual_configs){
            return false;
     }


    public Account_setup_bank_manual_config create(Account_setup_bank_manual_config account_setup_bank_manual_config){
            return null;
     }
    public Boolean createBatch(List<Account_setup_bank_manual_config> account_setup_bank_manual_configs){
            return false;
     }

    public Account_setup_bank_manual_config get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Account_setup_bank_manual_config> searchDefault(Account_setup_bank_manual_configSearchContext context){
            return null;
     }


    public Page<Account_setup_bank_manual_config> select(){
            return null;
     }

    public Account_setup_bank_manual_config getDraft(){
            return null;
    }



}
