package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_workcenter] 服务对象接口
 */
@FeignClient(value = "odoo-mrp", contextId = "mrp-workcenter", fallback = mrp_workcenterFallback.class)
public interface mrp_workcenterFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/searchdefault")
    Page<Mrp_workcenter> searchDefault(@RequestBody Mrp_workcenterSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters")
    Mrp_workcenter create(@RequestBody Mrp_workcenter mrp_workcenter);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/batch")
    Boolean createBatch(@RequestBody List<Mrp_workcenter> mrp_workcenters);



    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/{id}")
    Mrp_workcenter get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/{id}")
    Mrp_workcenter update(@PathVariable("id") Integer id,@RequestBody Mrp_workcenter mrp_workcenter);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/batch")
    Boolean updateBatch(@RequestBody List<Mrp_workcenter> mrp_workcenters);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/select")
    Page<Mrp_workcenter> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/getdraft")
    Mrp_workcenter getDraft();


}
