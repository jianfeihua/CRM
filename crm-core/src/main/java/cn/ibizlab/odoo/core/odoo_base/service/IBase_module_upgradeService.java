package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_upgradeSearchContext;


/**
 * 实体[Base_module_upgrade] 服务对象接口
 */
public interface IBase_module_upgradeService{

    Base_module_upgrade get(Integer key) ;
    boolean update(Base_module_upgrade et) ;
    void updateBatch(List<Base_module_upgrade> list) ;
    boolean create(Base_module_upgrade et) ;
    void createBatch(List<Base_module_upgrade> list) ;
    Base_module_upgrade getDraft(Base_module_upgrade et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_module_upgrade> searchDefault(Base_module_upgradeSearchContext context) ;

}



