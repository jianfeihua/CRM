package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_taskService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_taskFeignClient;

/**
 * 实体[Maintenance Task] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_taskServiceImpl implements IMro_taskService {

    @Autowired
    mro_taskFeignClient mro_taskFeignClient;


    @Override
    public boolean update(Mro_task et) {
        Mro_task rt = mro_taskFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_task> list){
        mro_taskFeignClient.updateBatch(list) ;
    }

    @Override
    public Mro_task getDraft(Mro_task et) {
        et=mro_taskFeignClient.getDraft();
        return et;
    }

    @Override
    public Mro_task get(Integer id) {
		Mro_task et=mro_taskFeignClient.get(id);
        if(et==null){
            et=new Mro_task();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_taskFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_taskFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mro_task et) {
        Mro_task rt = mro_taskFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_task> list){
        mro_taskFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_task> searchDefault(Mro_taskSearchContext context) {
        Page<Mro_task> mro_tasks=mro_taskFeignClient.searchDefault(context);
        return mro_tasks;
    }


}


