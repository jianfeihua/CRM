package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [payment_acquirer] 对象
 */
public interface Ipayment_acquirer {

    /**
     * 获取 [批准支持的机制]
     */
    public void setAuthorize_implemented(String authorize_implemented);
    
    /**
     * 设置 [批准支持的机制]
     */
    public String getAuthorize_implemented();

    /**
     * 获取 [批准支持的机制]脏标记
     */
    public boolean getAuthorize_implementedDirtyFlag();
    /**
     * 获取 [取消消息]
     */
    public void setCancel_msg(String cancel_msg);
    
    /**
     * 设置 [取消消息]
     */
    public String getCancel_msg();

    /**
     * 获取 [取消消息]脏标记
     */
    public boolean getCancel_msgDirtyFlag();
    /**
     * 获取 [手动获取金额]
     */
    public void setCapture_manually(String capture_manually);
    
    /**
     * 设置 [手动获取金额]
     */
    public String getCapture_manually();

    /**
     * 获取 [手动获取金额]脏标记
     */
    public boolean getCapture_manuallyDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_ids(String country_ids);
    
    /**
     * 设置 [国家]
     */
    public String getCountry_ids();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [完成的信息]
     */
    public void setDone_msg(String done_msg);
    
    /**
     * 设置 [完成的信息]
     */
    public String getDone_msg();

    /**
     * 获取 [完成的信息]脏标记
     */
    public boolean getDone_msgDirtyFlag();
    /**
     * 获取 [环境]
     */
    public void setEnvironment(String environment);
    
    /**
     * 设置 [环境]
     */
    public String getEnvironment();

    /**
     * 获取 [环境]脏标记
     */
    public boolean getEnvironmentDirtyFlag();
    /**
     * 获取 [错误消息]
     */
    public void setError_msg(String error_msg);
    
    /**
     * 设置 [错误消息]
     */
    public String getError_msg();

    /**
     * 获取 [错误消息]脏标记
     */
    public boolean getError_msgDirtyFlag();
    /**
     * 获取 [添加额外的费用]
     */
    public void setFees_active(String fees_active);
    
    /**
     * 设置 [添加额外的费用]
     */
    public String getFees_active();

    /**
     * 获取 [添加额外的费用]脏标记
     */
    public boolean getFees_activeDirtyFlag();
    /**
     * 获取 [国内固定费用]
     */
    public void setFees_dom_fixed(Double fees_dom_fixed);
    
    /**
     * 设置 [国内固定费用]
     */
    public Double getFees_dom_fixed();

    /**
     * 获取 [国内固定费用]脏标记
     */
    public boolean getFees_dom_fixedDirtyFlag();
    /**
     * 获取 [动态内部费用(百分比)]
     */
    public void setFees_dom_var(Double fees_dom_var);
    
    /**
     * 设置 [动态内部费用(百分比)]
     */
    public Double getFees_dom_var();

    /**
     * 获取 [动态内部费用(百分比)]脏标记
     */
    public boolean getFees_dom_varDirtyFlag();
    /**
     * 获取 [支持费用计算]
     */
    public void setFees_implemented(String fees_implemented);
    
    /**
     * 设置 [支持费用计算]
     */
    public String getFees_implemented();

    /**
     * 获取 [支持费用计算]脏标记
     */
    public boolean getFees_implementedDirtyFlag();
    /**
     * 获取 [固定的手续费]
     */
    public void setFees_int_fixed(Double fees_int_fixed);
    
    /**
     * 设置 [固定的手续费]
     */
    public Double getFees_int_fixed();

    /**
     * 获取 [固定的手续费]脏标记
     */
    public boolean getFees_int_fixedDirtyFlag();
    /**
     * 获取 [可变的交易费用（百分比）]
     */
    public void setFees_int_var(Double fees_int_var);
    
    /**
     * 设置 [可变的交易费用（百分比）]
     */
    public Double getFees_int_var();

    /**
     * 获取 [可变的交易费用（百分比）]脏标记
     */
    public boolean getFees_int_varDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getImage();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [中等尺寸图像]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [中等尺寸图像]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [小尺寸图像]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [小尺寸图像]
     */
    public byte[] getImage_small();

    /**
     * 获取 [小尺寸图像]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [未收款]
     */
    public void setInbound_payment_method_ids(String inbound_payment_method_ids);
    
    /**
     * 设置 [未收款]
     */
    public String getInbound_payment_method_ids();

    /**
     * 获取 [未收款]脏标记
     */
    public boolean getInbound_payment_method_idsDirtyFlag();
    /**
     * 获取 [付款日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [付款日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [付款日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [付款日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [付款日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [付款日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [对应模块]
     */
    public void setModule_id(Integer module_id);
    
    /**
     * 设置 [对应模块]
     */
    public Integer getModule_id();

    /**
     * 获取 [对应模块]脏标记
     */
    public boolean getModule_idDirtyFlag();
    /**
     * 获取 [安装状态]
     */
    public void setModule_state(String module_state);
    
    /**
     * 设置 [安装状态]
     */
    public String getModule_state();

    /**
     * 获取 [安装状态]脏标记
     */
    public boolean getModule_stateDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [立即支付]
     */
    public void setPayment_flow(String payment_flow);
    
    /**
     * 设置 [立即支付]
     */
    public String getPayment_flow();

    /**
     * 获取 [立即支付]脏标记
     */
    public boolean getPayment_flowDirtyFlag();
    /**
     * 获取 [支持的支付图标]
     */
    public void setPayment_icon_ids(String payment_icon_ids);
    
    /**
     * 设置 [支持的支付图标]
     */
    public String getPayment_icon_ids();

    /**
     * 获取 [支持的支付图标]脏标记
     */
    public boolean getPayment_icon_idsDirtyFlag();
    /**
     * 获取 [待定消息]
     */
    public void setPending_msg(String pending_msg);
    
    /**
     * 设置 [待定消息]
     */
    public String getPending_msg();

    /**
     * 获取 [待定消息]脏标记
     */
    public boolean getPending_msgDirtyFlag();
    /**
     * 获取 [感谢留言]
     */
    public void setPost_msg(String post_msg);
    
    /**
     * 设置 [感谢留言]
     */
    public String getPost_msg();

    /**
     * 获取 [感谢留言]脏标记
     */
    public boolean getPost_msgDirtyFlag();
    /**
     * 获取 [帮助信息]
     */
    public void setPre_msg(String pre_msg);
    
    /**
     * 设置 [帮助信息]
     */
    public String getPre_msg();

    /**
     * 获取 [帮助信息]脏标记
     */
    public boolean getPre_msgDirtyFlag();
    /**
     * 获取 [服务商]
     */
    public void setProvider(String provider);
    
    /**
     * 设置 [服务商]
     */
    public String getProvider();

    /**
     * 获取 [服务商]脏标记
     */
    public boolean getProviderDirtyFlag();
    /**
     * 获取 [使用SEPA QR 二维码]
     */
    public void setQr_code(String qr_code);
    
    /**
     * 设置 [使用SEPA QR 二维码]
     */
    public String getQr_code();

    /**
     * 获取 [使用SEPA QR 二维码]脏标记
     */
    public boolean getQr_codeDirtyFlag();
    /**
     * 获取 [S2S表单模板]
     */
    public void setRegistration_view_template_id(Integer registration_view_template_id);
    
    /**
     * 设置 [S2S表单模板]
     */
    public Integer getRegistration_view_template_id();

    /**
     * 获取 [S2S表单模板]脏标记
     */
    public boolean getRegistration_view_template_idDirtyFlag();
    /**
     * 获取 [保存卡]
     */
    public void setSave_token(String save_token);
    
    /**
     * 设置 [保存卡]
     */
    public String getSave_token();

    /**
     * 获取 [保存卡]脏标记
     */
    public boolean getSave_tokenDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [交流]
     */
    public void setSo_reference_type(String so_reference_type);
    
    /**
     * 设置 [交流]
     */
    public String getSo_reference_type();

    /**
     * 获取 [交流]脏标记
     */
    public boolean getSo_reference_typeDirtyFlag();
    /**
     * 获取 [特定国家/地区]
     */
    public void setSpecific_countries(String specific_countries);
    
    /**
     * 设置 [特定国家/地区]
     */
    public String getSpecific_countries();

    /**
     * 获取 [特定国家/地区]脏标记
     */
    public boolean getSpecific_countriesDirtyFlag();
    /**
     * 获取 [支持保存卡片资料]
     */
    public void setToken_implemented(String token_implemented);
    
    /**
     * 设置 [支持保存卡片资料]
     */
    public String getToken_implemented();

    /**
     * 获取 [支持保存卡片资料]脏标记
     */
    public boolean getToken_implementedDirtyFlag();
    /**
     * 获取 [窗体按钮模板]
     */
    public void setView_template_id(Integer view_template_id);
    
    /**
     * 设置 [窗体按钮模板]
     */
    public Integer getView_template_id();

    /**
     * 获取 [窗体按钮模板]脏标记
     */
    public boolean getView_template_idDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [在门户/网站可见]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [在门户/网站可见]
     */
    public String getWebsite_published();

    /**
     * 获取 [在门户/网站可见]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
