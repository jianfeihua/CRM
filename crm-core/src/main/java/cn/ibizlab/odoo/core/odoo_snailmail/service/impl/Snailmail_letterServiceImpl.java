package cn.ibizlab.odoo.core.odoo_snailmail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;
import cn.ibizlab.odoo.core.odoo_snailmail.service.ISnailmail_letterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_snailmail.client.snailmail_letterFeignClient;

/**
 * 实体[Snailmail 信纸] 服务对象接口实现
 */
@Slf4j
@Service
public class Snailmail_letterServiceImpl implements ISnailmail_letterService {

    @Autowired
    snailmail_letterFeignClient snailmail_letterFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=snailmail_letterFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        snailmail_letterFeignClient.removeBatch(idList);
    }

    @Override
    public Snailmail_letter get(Integer id) {
		Snailmail_letter et=snailmail_letterFeignClient.get(id);
        if(et==null){
            et=new Snailmail_letter();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Snailmail_letter et) {
        Snailmail_letter rt = snailmail_letterFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Snailmail_letter> list){
        snailmail_letterFeignClient.updateBatch(list) ;
    }

    @Override
    public Snailmail_letter getDraft(Snailmail_letter et) {
        et=snailmail_letterFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Snailmail_letter et) {
        Snailmail_letter rt = snailmail_letterFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Snailmail_letter> list){
        snailmail_letterFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Snailmail_letter> searchDefault(Snailmail_letterSearchContext context) {
        Page<Snailmail_letter> snailmail_letters=snailmail_letterFeignClient.searchDefault(context);
        return snailmail_letters;
    }


}


