package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_destinationSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_package_destinationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_package_destinationFeignClient;

/**
 * 实体[包裹目的地] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_package_destinationServiceImpl implements IStock_package_destinationService {

    @Autowired
    stock_package_destinationFeignClient stock_package_destinationFeignClient;


    @Override
    public Stock_package_destination getDraft(Stock_package_destination et) {
        et=stock_package_destinationFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_package_destinationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_package_destinationFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_package_destination et) {
        Stock_package_destination rt = stock_package_destinationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_package_destination> list){
        stock_package_destinationFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_package_destination get(Integer id) {
		Stock_package_destination et=stock_package_destinationFeignClient.get(id);
        if(et==null){
            et=new Stock_package_destination();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Stock_package_destination et) {
        Stock_package_destination rt = stock_package_destinationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_package_destination> list){
        stock_package_destinationFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_package_destination> searchDefault(Stock_package_destinationSearchContext context) {
        Page<Stock_package_destination> stock_package_destinations=stock_package_destinationFeignClient.searchDefault(context);
        return stock_package_destinations;
    }


}


