package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_model] 服务对象接口
 */
@Component
public class fleet_vehicle_modelFallback implements fleet_vehicle_modelFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Fleet_vehicle_model create(Fleet_vehicle_model fleet_vehicle_model){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_model> fleet_vehicle_models){
            return false;
     }

    public Page<Fleet_vehicle_model> searchDefault(Fleet_vehicle_modelSearchContext context){
            return null;
     }


    public Fleet_vehicle_model update(Integer id, Fleet_vehicle_model fleet_vehicle_model){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_model> fleet_vehicle_models){
            return false;
     }


    public Fleet_vehicle_model get(Integer id){
            return null;
     }





    public Page<Fleet_vehicle_model> select(){
            return null;
     }

    public Fleet_vehicle_model getDraft(){
            return null;
    }



}
