package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_immediate_transferSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_immediate_transferService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_immediate_transferFeignClient;

/**
 * 实体[立即调拨] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_immediate_transferServiceImpl implements IStock_immediate_transferService {

    @Autowired
    stock_immediate_transferFeignClient stock_immediate_transferFeignClient;


    @Override
    public boolean update(Stock_immediate_transfer et) {
        Stock_immediate_transfer rt = stock_immediate_transferFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_immediate_transfer> list){
        stock_immediate_transferFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_immediate_transfer get(Integer id) {
		Stock_immediate_transfer et=stock_immediate_transferFeignClient.get(id);
        if(et==null){
            et=new Stock_immediate_transfer();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Stock_immediate_transfer getDraft(Stock_immediate_transfer et) {
        et=stock_immediate_transferFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Stock_immediate_transfer et) {
        Stock_immediate_transfer rt = stock_immediate_transferFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_immediate_transfer> list){
        stock_immediate_transferFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_immediate_transferFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_immediate_transferFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_immediate_transfer> searchDefault(Stock_immediate_transferSearchContext context) {
        Page<Stock_immediate_transfer> stock_immediate_transfers=stock_immediate_transferFeignClient.searchDefault(context);
        return stock_immediate_transfers;
    }


}


