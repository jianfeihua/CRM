package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [群发邮件阶段] 对象
 */
@Data
public class Mail_statistics_report extends EntityClient implements Serializable {

    /**
     * 点击率
     */
    @JSONField(name = "clicked")
    @JsonProperty("clicked")
    private Integer clicked;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 被退回
     */
    @JSONField(name = "bounced")
    @JsonProperty("bounced")
    private Integer bounced;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 已回复
     */
    @JSONField(name = "replied")
    @JsonProperty("replied")
    private Integer replied;

    /**
     * 群发邮件营销
     */
    @JSONField(name = "campaign")
    @JsonProperty("campaign")
    private String campaign;

    /**
     * 计划日期
     */
    @DEField(name = "scheduled_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;

    /**
     * 已汇
     */
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Integer sent;

    /**
     * 从
     */
    @DEField(name = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 已送货
     */
    @JSONField(name = "delivered")
    @JsonProperty("delivered")
    private Integer delivered;

    /**
     * 群发邮件
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 已开启
     */
    @JSONField(name = "opened")
    @JsonProperty("opened")
    private Integer opened;





    /**
     * 设置 [点击率]
     */
    public void setClicked(Integer clicked){
        this.clicked = clicked ;
        this.modify("clicked",clicked);
    }
    /**
     * 设置 [被退回]
     */
    public void setBounced(Integer bounced){
        this.bounced = bounced ;
        this.modify("bounced",bounced);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [已回复]
     */
    public void setReplied(Integer replied){
        this.replied = replied ;
        this.modify("replied",replied);
    }
    /**
     * 设置 [群发邮件营销]
     */
    public void setCampaign(String campaign){
        this.campaign = campaign ;
        this.modify("campaign",campaign);
    }
    /**
     * 设置 [计划日期]
     */
    public void setScheduledDate(Timestamp scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }
    /**
     * 设置 [已汇]
     */
    public void setSent(Integer sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }
    /**
     * 设置 [从]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }
    /**
     * 设置 [已送货]
     */
    public void setDelivered(Integer delivered){
        this.delivered = delivered ;
        this.modify("delivered",delivered);
    }
    /**
     * 设置 [群发邮件]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [已开启]
     */
    public void setOpened(Integer opened){
        this.opened = opened ;
        this.modify("opened",opened);
    }

}


