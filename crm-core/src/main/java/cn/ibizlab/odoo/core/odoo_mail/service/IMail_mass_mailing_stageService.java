package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;


/**
 * 实体[Mail_mass_mailing_stage] 服务对象接口
 */
public interface IMail_mass_mailing_stageService{

    Mail_mass_mailing_stage getDraft(Mail_mass_mailing_stage et) ;
    boolean update(Mail_mass_mailing_stage et) ;
    void updateBatch(List<Mail_mass_mailing_stage> list) ;
    boolean create(Mail_mass_mailing_stage et) ;
    void createBatch(List<Mail_mass_mailing_stage> list) ;
    Mail_mass_mailing_stage get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_mass_mailing_stage> searchDefault(Mail_mass_mailing_stageSearchContext context) ;

}



