package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_groupSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_country_groupService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_country_groupFeignClient;

/**
 * 实体[国家/地区群组] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_country_groupServiceImpl implements IRes_country_groupService {

    @Autowired
    res_country_groupFeignClient res_country_groupFeignClient;


    @Override
    public Res_country_group getDraft(Res_country_group et) {
        et=res_country_groupFeignClient.getDraft();
        return et;
    }

    @Override
    public Res_country_group get(Integer id) {
		Res_country_group et=res_country_groupFeignClient.get(id);
        if(et==null){
            et=new Res_country_group();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_country_groupFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_country_groupFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Res_country_group et) {
        Res_country_group rt = res_country_groupFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_country_group> list){
        res_country_groupFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Res_country_group et) {
        Res_country_group rt = res_country_groupFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_country_group> list){
        res_country_groupFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_country_group> searchDefault(Res_country_groupSearchContext context) {
        Page<Res_country_group> res_country_groups=res_country_groupFeignClient.searchDefault(context);
        return res_country_groups;
    }


}


