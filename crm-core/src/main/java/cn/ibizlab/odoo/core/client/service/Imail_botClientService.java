package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_bot;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_bot] 服务对象接口
 */
public interface Imail_botClientService{

    public Imail_bot createModel() ;

    public void update(Imail_bot mail_bot);

    public void create(Imail_bot mail_bot);

    public void createBatch(List<Imail_bot> mail_bots);

    public Page<Imail_bot> fetchDefault(SearchContext context);

    public void get(Imail_bot mail_bot);

    public void removeBatch(List<Imail_bot> mail_bots);

    public void remove(Imail_bot mail_bot);

    public void updateBatch(List<Imail_bot> mail_bots);

    public Page<Imail_bot> select(SearchContext context);

    public void getDraft(Imail_bot mail_bot);

}
