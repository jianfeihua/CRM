package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_currency;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_currency] 服务对象接口
 */
public interface Ires_currencyClientService{

    public Ires_currency createModel() ;

    public void get(Ires_currency res_currency);

    public void updateBatch(List<Ires_currency> res_currencies);

    public void remove(Ires_currency res_currency);

    public void update(Ires_currency res_currency);

    public Page<Ires_currency> fetchDefault(SearchContext context);

    public void create(Ires_currency res_currency);

    public void removeBatch(List<Ires_currency> res_currencies);

    public void createBatch(List<Ires_currency> res_currencies);

    public Page<Ires_currency> select(SearchContext context);

    public void getDraft(Ires_currency res_currency);

}
