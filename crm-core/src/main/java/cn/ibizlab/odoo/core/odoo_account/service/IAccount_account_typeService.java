package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_typeSearchContext;


/**
 * 实体[Account_account_type] 服务对象接口
 */
public interface IAccount_account_typeService{

    Account_account_type get(Integer key) ;
    Account_account_type getDraft(Account_account_type et) ;
    boolean update(Account_account_type et) ;
    void updateBatch(List<Account_account_type> list) ;
    boolean create(Account_account_type et) ;
    void createBatch(List<Account_account_type> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_account_type> searchDefault(Account_account_typeSearchContext context) ;

}



