package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_cancelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_repair.client.repair_cancelFeignClient;

/**
 * 实体[取消维修] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_cancelServiceImpl implements IRepair_cancelService {

    @Autowired
    repair_cancelFeignClient repair_cancelFeignClient;


    @Override
    public Repair_cancel getDraft(Repair_cancel et) {
        et=repair_cancelFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Repair_cancel et) {
        Repair_cancel rt = repair_cancelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Repair_cancel> list){
        repair_cancelFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Repair_cancel et) {
        Repair_cancel rt = repair_cancelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_cancel> list){
        repair_cancelFeignClient.createBatch(list) ;
    }

    @Override
    public Repair_cancel get(Integer id) {
		Repair_cancel et=repair_cancelFeignClient.get(id);
        if(et==null){
            et=new Repair_cancel();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=repair_cancelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        repair_cancelFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_cancel> searchDefault(Repair_cancelSearchContext context) {
        Page<Repair_cancel> repair_cancels=repair_cancelFeignClient.searchDefault(context);
        return repair_cancels;
    }


}


