package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;


/**
 * 实体[Mrp_workcenter_productivity_loss_type] 服务对象接口
 */
public interface IMrp_workcenter_productivity_loss_typeService{

    boolean update(Mrp_workcenter_productivity_loss_type et) ;
    void updateBatch(List<Mrp_workcenter_productivity_loss_type> list) ;
    boolean create(Mrp_workcenter_productivity_loss_type et) ;
    void createBatch(List<Mrp_workcenter_productivity_loss_type> list) ;
    Mrp_workcenter_productivity_loss_type getDraft(Mrp_workcenter_productivity_loss_type et) ;
    Mrp_workcenter_productivity_loss_type get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mrp_workcenter_productivity_loss_type> searchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context) ;

}



