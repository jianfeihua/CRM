package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_setup_bank_manual_config;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_setup_bank_manual_config] 服务对象接口
 */
public interface Iaccount_setup_bank_manual_configClientService{

    public Iaccount_setup_bank_manual_config createModel() ;

    public void update(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

    public void create(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

    public void get(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

    public void updateBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs);

    public void remove(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

    public void removeBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs);

    public void createBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs);

    public Page<Iaccount_setup_bank_manual_config> fetchDefault(SearchContext context);

    public Page<Iaccount_setup_bank_manual_config> select(SearchContext context);

    public void getDraft(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

}
