package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_update_translationsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_update_translationsFeignClient;

/**
 * 实体[更新翻译] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_update_translationsServiceImpl implements IBase_update_translationsService {

    @Autowired
    base_update_translationsFeignClient base_update_translationsFeignClient;


    @Override
    public Base_update_translations getDraft(Base_update_translations et) {
        et=base_update_translationsFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Base_update_translations et) {
        Base_update_translations rt = base_update_translationsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_update_translations> list){
        base_update_translationsFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_update_translations get(Integer id) {
		Base_update_translations et=base_update_translationsFeignClient.get(id);
        if(et==null){
            et=new Base_update_translations();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Base_update_translations et) {
        Base_update_translations rt = base_update_translationsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_update_translations> list){
        base_update_translationsFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_update_translationsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_update_translationsFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_update_translations> searchDefault(Base_update_translationsSearchContext context) {
        Page<Base_update_translations> base_update_translationss=base_update_translationsFeignClient.searchDefault(context);
        return base_update_translationss;
    }


}


