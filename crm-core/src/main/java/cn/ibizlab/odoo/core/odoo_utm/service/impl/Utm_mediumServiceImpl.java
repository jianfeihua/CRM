package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mediumSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mediumService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_utm.client.utm_mediumFeignClient;

/**
 * 实体[UTM媒体] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_mediumServiceImpl implements IUtm_mediumService {

    @Autowired
    utm_mediumFeignClient utm_mediumFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=utm_mediumFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        utm_mediumFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Utm_medium et) {
        Utm_medium rt = utm_mediumFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_medium> list){
        utm_mediumFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Utm_medium et) {
        Utm_medium rt = utm_mediumFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Utm_medium> list){
        utm_mediumFeignClient.updateBatch(list) ;
    }

    @Override
    public Utm_medium get(Integer id) {
		Utm_medium et=utm_mediumFeignClient.get(id);
        if(et==null){
            et=new Utm_medium();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Utm_medium getDraft(Utm_medium et) {
        et=utm_mediumFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_medium> searchDefault(Utm_mediumSearchContext context) {
        Page<Utm_medium> utm_mediums=utm_mediumFeignClient.searchDefault(context);
        return utm_mediums;
    }


}


