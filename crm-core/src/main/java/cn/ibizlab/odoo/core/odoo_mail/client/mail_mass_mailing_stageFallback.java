package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing_stage] 服务对象接口
 */
@Component
public class mail_mass_mailing_stageFallback implements mail_mass_mailing_stageFeignClient{

    public Mail_mass_mailing_stage create(Mail_mass_mailing_stage mail_mass_mailing_stage){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing_stage> mail_mass_mailing_stages){
            return false;
     }




    public Mail_mass_mailing_stage update(Integer id, Mail_mass_mailing_stage mail_mass_mailing_stage){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing_stage> mail_mass_mailing_stages){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mail_mass_mailing_stage get(Integer id){
            return null;
     }


    public Page<Mail_mass_mailing_stage> searchDefault(Mail_mass_mailing_stageSearchContext context){
            return null;
     }


    public Page<Mail_mass_mailing_stage> select(){
            return null;
     }

    public Mail_mass_mailing_stage getDraft(){
            return null;
    }



}
