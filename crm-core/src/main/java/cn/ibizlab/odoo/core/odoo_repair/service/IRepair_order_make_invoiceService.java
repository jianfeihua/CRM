package cn.ibizlab.odoo.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;


/**
 * 实体[Repair_order_make_invoice] 服务对象接口
 */
public interface IRepair_order_make_invoiceService{

    boolean update(Repair_order_make_invoice et) ;
    void updateBatch(List<Repair_order_make_invoice> list) ;
    Repair_order_make_invoice get(Integer key) ;
    Repair_order_make_invoice getDraft(Repair_order_make_invoice et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Repair_order_make_invoice et) ;
    void createBatch(List<Repair_order_make_invoice> list) ;
    Page<Repair_order_make_invoice> searchDefault(Repair_order_make_invoiceSearchContext context) ;

}



