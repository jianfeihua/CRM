package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_incotermsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_incoterms] 服务对象接口
 */
@Component
public class account_incotermsFallback implements account_incotermsFeignClient{


    public Page<Account_incoterms> searchDefault(Account_incotermsSearchContext context){
            return null;
     }


    public Account_incoterms update(Integer id, Account_incoterms account_incoterms){
            return null;
     }
    public Boolean updateBatch(List<Account_incoterms> account_incoterms){
            return false;
     }


    public Account_incoterms create(Account_incoterms account_incoterms){
            return null;
     }
    public Boolean createBatch(List<Account_incoterms> account_incoterms){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_incoterms get(Integer id){
            return null;
     }




    public Page<Account_incoterms> select(){
            return null;
     }

    public Account_incoterms getDraft(){
            return null;
    }



}
