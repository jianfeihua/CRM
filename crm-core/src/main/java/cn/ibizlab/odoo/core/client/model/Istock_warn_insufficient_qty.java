package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_warn_insufficient_qty] 对象
 */
public interface Istock_warn_insufficient_qty {

    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [即时库存]
     */
    public void setQuant_ids(String quant_ids);
    
    /**
     * 设置 [即时库存]
     */
    public String getQuant_ids();

    /**
     * 获取 [即时库存]脏标记
     */
    public boolean getQuant_idsDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
