package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_service_typeSearchContext;


/**
 * 实体[Fleet_service_type] 服务对象接口
 */
public interface IFleet_service_typeService{

    boolean update(Fleet_service_type et) ;
    void updateBatch(List<Fleet_service_type> list) ;
    Fleet_service_type get(Integer key) ;
    Fleet_service_type getDraft(Fleet_service_type et) ;
    boolean create(Fleet_service_type et) ;
    void createBatch(List<Fleet_service_type> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Fleet_service_type> searchDefault(Fleet_service_typeSearchContext context) ;

}



