package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Irepair_fee;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_fee] 服务对象接口
 */
public interface Irepair_feeClientService{

    public Irepair_fee createModel() ;

    public void get(Irepair_fee repair_fee);

    public void create(Irepair_fee repair_fee);

    public void updateBatch(List<Irepair_fee> repair_fees);

    public void createBatch(List<Irepair_fee> repair_fees);

    public void update(Irepair_fee repair_fee);

    public void removeBatch(List<Irepair_fee> repair_fees);

    public Page<Irepair_fee> fetchDefault(SearchContext context);

    public void remove(Irepair_fee repair_fee);

    public Page<Irepair_fee> select(SearchContext context);

    public void getDraft(Irepair_fee repair_fee);

}
