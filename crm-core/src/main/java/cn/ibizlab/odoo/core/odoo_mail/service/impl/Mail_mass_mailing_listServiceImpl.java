package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_listService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_listFeignClient;

/**
 * 实体[邮件列表] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_listServiceImpl implements IMail_mass_mailing_listService {

    @Autowired
    mail_mass_mailing_listFeignClient mail_mass_mailing_listFeignClient;


    @Override
    public boolean update(Mail_mass_mailing_list et) {
        Mail_mass_mailing_list rt = mail_mass_mailing_listFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mass_mailing_list> list){
        mail_mass_mailing_listFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_mass_mailing_list et) {
        Mail_mass_mailing_list rt = mail_mass_mailing_listFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_list> list){
        mail_mass_mailing_listFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_mass_mailing_list get(Integer id) {
		Mail_mass_mailing_list et=mail_mass_mailing_listFeignClient.get(id);
        if(et==null){
            et=new Mail_mass_mailing_list();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mass_mailing_listFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mass_mailing_listFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_mass_mailing_list getDraft(Mail_mass_mailing_list et) {
        et=mail_mass_mailing_listFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_list> searchDefault(Mail_mass_mailing_listSearchContext context) {
        Page<Mail_mass_mailing_list> mail_mass_mailing_lists=mail_mass_mailing_listFeignClient.searchDefault(context);
        return mail_mass_mailing_lists;
    }


}


