package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_campaign;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing_campaign] 服务对象接口
 */
public interface Imail_mass_mailing_campaignClientService{

    public Imail_mass_mailing_campaign createModel() ;

    public void update(Imail_mass_mailing_campaign mail_mass_mailing_campaign);

    public void remove(Imail_mass_mailing_campaign mail_mass_mailing_campaign);

    public Page<Imail_mass_mailing_campaign> fetchDefault(SearchContext context);

    public void createBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns);

    public void updateBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns);

    public void get(Imail_mass_mailing_campaign mail_mass_mailing_campaign);

    public void create(Imail_mass_mailing_campaign mail_mass_mailing_campaign);

    public void removeBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns);

    public Page<Imail_mass_mailing_campaign> select(SearchContext context);

    public void getDraft(Imail_mass_mailing_campaign mail_mass_mailing_campaign);

}
