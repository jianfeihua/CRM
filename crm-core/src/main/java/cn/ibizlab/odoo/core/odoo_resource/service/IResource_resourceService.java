package cn.ibizlab.odoo.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;


/**
 * 实体[Resource_resource] 服务对象接口
 */
public interface IResource_resourceService{

    boolean update(Resource_resource et) ;
    void updateBatch(List<Resource_resource> list) ;
    Resource_resource get(Integer key) ;
    boolean create(Resource_resource et) ;
    void createBatch(List<Resource_resource> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Resource_resource getDraft(Resource_resource et) ;
    Page<Resource_resource> searchDefault(Resource_resourceSearchContext context) ;

}



