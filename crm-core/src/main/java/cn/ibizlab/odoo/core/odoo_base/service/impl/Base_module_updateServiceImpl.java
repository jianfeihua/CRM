package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_updateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_module_updateFeignClient;

/**
 * 实体[更新模块] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_module_updateServiceImpl implements IBase_module_updateService {

    @Autowired
    base_module_updateFeignClient base_module_updateFeignClient;


    @Override
    public Base_module_update getDraft(Base_module_update et) {
        et=base_module_updateFeignClient.getDraft();
        return et;
    }

    @Override
    public Base_module_update get(Integer id) {
		Base_module_update et=base_module_updateFeignClient.get(id);
        if(et==null){
            et=new Base_module_update();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Base_module_update et) {
        Base_module_update rt = base_module_updateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_module_update> list){
        base_module_updateFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Base_module_update et) {
        Base_module_update rt = base_module_updateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_module_update> list){
        base_module_updateFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_module_updateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_module_updateFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_module_update> searchDefault(Base_module_updateSearchContext context) {
        Page<Base_module_update> base_module_updates=base_module_updateFeignClient.searchDefault(context);
        return base_module_updates;
    }


}


