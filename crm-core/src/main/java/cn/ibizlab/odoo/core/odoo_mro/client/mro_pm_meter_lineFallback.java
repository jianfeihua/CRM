package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_meter_line] 服务对象接口
 */
@Component
public class mro_pm_meter_lineFallback implements mro_pm_meter_lineFeignClient{

    public Mro_pm_meter_line create(Mro_pm_meter_line mro_pm_meter_line){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_meter_line> mro_pm_meter_lines){
            return false;
     }

    public Mro_pm_meter_line get(Integer id){
            return null;
     }



    public Mro_pm_meter_line update(Integer id, Mro_pm_meter_line mro_pm_meter_line){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_meter_line> mro_pm_meter_lines){
            return false;
     }


    public Page<Mro_pm_meter_line> searchDefault(Mro_pm_meter_lineSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mro_pm_meter_line> select(){
            return null;
     }

    public Mro_pm_meter_line getDraft(){
            return null;
    }



}
