package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badge_user_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_badge_user_wizardFeignClient;

/**
 * 实体[游戏化用户徽章向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_badge_user_wizardServiceImpl implements IGamification_badge_user_wizardService {

    @Autowired
    gamification_badge_user_wizardFeignClient gamification_badge_user_wizardFeignClient;


    @Override
    public boolean update(Gamification_badge_user_wizard et) {
        Gamification_badge_user_wizard rt = gamification_badge_user_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_badge_user_wizard> list){
        gamification_badge_user_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public Gamification_badge_user_wizard get(Integer id) {
		Gamification_badge_user_wizard et=gamification_badge_user_wizardFeignClient.get(id);
        if(et==null){
            et=new Gamification_badge_user_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Gamification_badge_user_wizard et) {
        Gamification_badge_user_wizard rt = gamification_badge_user_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_badge_user_wizard> list){
        gamification_badge_user_wizardFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_badge_user_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_badge_user_wizardFeignClient.removeBatch(idList);
    }

    @Override
    public Gamification_badge_user_wizard getDraft(Gamification_badge_user_wizard et) {
        et=gamification_badge_user_wizardFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_badge_user_wizard> searchDefault(Gamification_badge_user_wizardSearchContext context) {
        Page<Gamification_badge_user_wizard> gamification_badge_user_wizards=gamification_badge_user_wizardFeignClient.searchDefault(context);
        return gamification_badge_user_wizards;
    }


}


