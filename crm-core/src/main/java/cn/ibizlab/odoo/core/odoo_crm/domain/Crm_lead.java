package cn.ibizlab.odoo.core.odoo_crm.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [线索/商机] 对象
 */
@Data
public class Crm_lead extends EntityClient implements Serializable {

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * #会议
     */
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;

    /**
     * 工作岗位
     */
    @JSONField(name = "ibizfunction")
    @JsonProperty("ibizfunction")
    private String ibizfunction;

    /**
     * 退回
     */
    @DEField(name = "message_bounce")
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 城市
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;

    /**
     * 关闭日期
     */
    @DEField(name = "day_close")
    @JSONField(name = "day_close")
    @JsonProperty("day_close")
    private Double dayClose;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 按比例分摊收入
     */
    @DEField(name = "expected_revenue")
    @JSONField(name = "expected_revenue")
    @JsonProperty("expected_revenue")
    private Double expectedRevenue;

    /**
     * 关闭日期
     */
    @DEField(name = "date_closed")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_closed" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_closed")
    private Timestamp dateClosed;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 全局抄送
     */
    @DEField(name = "email_cc")
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;

    /**
     * 联系人姓名
     */
    @DEField(name = "contact_name")
    @JSONField(name = "contact_name")
    @JsonProperty("contact_name")
    private String contactName;

    /**
     * 最后阶段更新
     */
    @DEField(name = "date_last_stage_update")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_last_stage_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_last_stage_update")
    private Timestamp dateLastStageUpdate;

    /**
     * 预期收益
     */
    @DEField(name = "planned_revenue")
    @JSONField(name = "planned_revenue")
    @JsonProperty("planned_revenue")
    private Double plannedRevenue;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 预期结束
     */
    @DEField(name = "date_deadline")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;

    /**
     * 邮政编码
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;

    /**
     * 商机
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 便签
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 手机
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 网站
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * EMail
     */
    @DEField(name = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 转换日期
     */
    @DEField(name = "date_conversion")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_conversion" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_conversion")
    private Timestamp dateConversion;

    /**
     * 客户名称
     */
    @DEField(name = "partner_name")
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;

    /**
     * 销售订单的总数
     */
    @JSONField(name = "sale_amount_total")
    @JsonProperty("sale_amount_total")
    private Double saleAmountTotal;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 需要激活
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 看板状态
     */
    @JSONField(name = "kanban_state")
    @JsonProperty("kanban_state")
    private String kanbanState;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 引荐于
     */
    @JSONField(name = "referred")
    @JsonProperty("referred")
    private String referred;

    /**
     * 概率
     */
    @JSONField(name = "probability")
    @JsonProperty("probability")
    private Double probability;

    /**
     * 最近行动
     */
    @DEField(name = "date_action_last")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_action_last" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_action_last")
    private Timestamp dateActionLast;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 分配日期
     */
    @DEField(name = "date_open")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_open" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_open")
    private Timestamp dateOpen;

    /**
     * 电话
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 分配天数
     */
    @DEField(name = "day_open")
    @JSONField(name = "day_open")
    @JsonProperty("day_open")
    private Double dayOpen;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 报价单的数量
     */
    @JSONField(name = "sale_number")
    @JsonProperty("sale_number")
    private Integer saleNumber;

    /**
     * 街道 2
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;

    /**
     * 订单
     */
    @JSONField(name = "order_ids")
    @JsonProperty("order_ids")
    private String orderIds;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 街道
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 标签
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * 黑名单
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private String isBlacklisted;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 优先级
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 销售员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 来源
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;

    /**
     * 业务伙伴联系姓名
     */
    @JSONField(name = "partner_address_name")
    @JsonProperty("partner_address_name")
    private String partnerAddressName;

    /**
     * 媒介
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;

    /**
     * 币种
     */
    @JSONField(name = "company_currency")
    @JsonProperty("company_currency")
    private Integer companyCurrency;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * Partner Contact Mobile
     */
    @JSONField(name = "partner_address_mobile")
    @JsonProperty("partner_address_mobile")
    private String partnerAddressMobile;

    /**
     * 最后更新
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 用户EMail
     */
    @JSONField(name = "user_email")
    @JsonProperty("user_email")
    private String userEmail;

    /**
     * 用户 登录
     */
    @JSONField(name = "user_login")
    @JsonProperty("user_login")
    private String userLogin;

    /**
     * 合作伙伴联系电话
     */
    @JSONField(name = "partner_address_phone")
    @JsonProperty("partner_address_phone")
    private String partnerAddressPhone;

    /**
     * 省份
     */
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;

    /**
     * 营销
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;

    /**
     * 合作伙伴黑名单
     */
    @JSONField(name = "partner_is_blacklisted")
    @JsonProperty("partner_is_blacklisted")
    private String partnerIsBlacklisted;

    /**
     * 业务伙伴联系EMail
     */
    @JSONField(name = "partner_address_email")
    @JsonProperty("partner_address_email")
    private String partnerAddressEmail;

    /**
     * 阶段
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 国家
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 称谓
     */
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    private String titleText;

    /**
     * 失去原因
     */
    @JSONField(name = "lost_reason_text")
    @JsonProperty("lost_reason_text")
    private String lostReasonText;

    /**
     * 失去原因
     */
    @DEField(name = "lost_reason")
    @JSONField(name = "lost_reason")
    @JsonProperty("lost_reason")
    private Integer lostReason;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 省份
     */
    @DEField(name = "state_id")
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;

    /**
     * 媒介
     */
    @DEField(name = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Integer mediumId;

    /**
     * 阶段
     */
    @DEField(name = "stage_id")
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Integer stageId;

    /**
     * 来源
     */
    @DEField(name = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;

    /**
     * 国家
     */
    @DEField(name = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Integer campaignId;

    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;

    /**
     * 称谓
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private Integer title;


    /**
     * 
     */
    @JSONField(name = "odoolost")
    @JsonProperty("odoolost")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason odooLost;

    /**
     * 
     */
    @JSONField(name = "odoostage")
    @JsonProperty("odoostage")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage odooStage;

    /**
     * 
     */
    @JSONField(name = "odooteam")
    @JsonProperty("odooteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoostate")
    @JsonProperty("odoostate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country_state odooState;

    /**
     * 
     */
    @JSONField(name = "odoocountry")
    @JsonProperty("odoocountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JSONField(name = "odootitle")
    @JsonProperty("odootitle")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title odooTitle;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoocampaign")
    @JsonProperty("odoocampaign")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JSONField(name = "odoomedium")
    @JsonProperty("odoomedium")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JSONField(name = "odoosource")
    @JsonProperty("odoosource")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source odooSource;




    /**
     * 设置 [工作岗位]
     */
    public void setIbizfunction(String ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.modify("ibizfunction",ibizfunction);
    }
    /**
     * 设置 [退回]
     */
    public void setMessageBounce(Integer messageBounce){
        this.messageBounce = messageBounce ;
        this.modify("message_bounce",messageBounce);
    }
    /**
     * 设置 [城市]
     */
    public void setCity(String city){
        this.city = city ;
        this.modify("city",city);
    }
    /**
     * 设置 [关闭日期]
     */
    public void setDayClose(Double dayClose){
        this.dayClose = dayClose ;
        this.modify("day_close",dayClose);
    }
    /**
     * 设置 [按比例分摊收入]
     */
    public void setExpectedRevenue(Double expectedRevenue){
        this.expectedRevenue = expectedRevenue ;
        this.modify("expected_revenue",expectedRevenue);
    }
    /**
     * 设置 [关闭日期]
     */
    public void setDateClosed(Timestamp dateClosed){
        this.dateClosed = dateClosed ;
        this.modify("date_closed",dateClosed);
    }
    /**
     * 设置 [全局抄送]
     */
    public void setEmailCc(String emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }
    /**
     * 设置 [联系人姓名]
     */
    public void setContactName(String contactName){
        this.contactName = contactName ;
        this.modify("contact_name",contactName);
    }
    /**
     * 设置 [最后阶段更新]
     */
    public void setDateLastStageUpdate(Timestamp dateLastStageUpdate){
        this.dateLastStageUpdate = dateLastStageUpdate ;
        this.modify("date_last_stage_update",dateLastStageUpdate);
    }
    /**
     * 设置 [预期收益]
     */
    public void setPlannedRevenue(Double plannedRevenue){
        this.plannedRevenue = plannedRevenue ;
        this.modify("planned_revenue",plannedRevenue);
    }
    /**
     * 设置 [预期结束]
     */
    public void setDateDeadline(Timestamp dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }
    /**
     * 设置 [邮政编码]
     */
    public void setZip(String zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }
    /**
     * 设置 [商机]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [便签]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [手机]
     */
    public void setMobile(String mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }
    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsite(String website){
        this.website = website ;
        this.modify("website",website);
    }
    /**
     * 设置 [EMail]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }
    /**
     * 设置 [转换日期]
     */
    public void setDateConversion(Timestamp dateConversion){
        this.dateConversion = dateConversion ;
        this.modify("date_conversion",dateConversion);
    }
    /**
     * 设置 [客户名称]
     */
    public void setPartnerName(String partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [引荐于]
     */
    public void setReferred(String referred){
        this.referred = referred ;
        this.modify("referred",referred);
    }
    /**
     * 设置 [概率]
     */
    public void setProbability(Double probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }
    /**
     * 设置 [最近行动]
     */
    public void setDateActionLast(Timestamp dateActionLast){
        this.dateActionLast = dateActionLast ;
        this.modify("date_action_last",dateActionLast);
    }
    /**
     * 设置 [分配日期]
     */
    public void setDateOpen(Timestamp dateOpen){
        this.dateOpen = dateOpen ;
        this.modify("date_open",dateOpen);
    }
    /**
     * 设置 [电话]
     */
    public void setPhone(String phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }
    /**
     * 设置 [分配天数]
     */
    public void setDayOpen(Double dayOpen){
        this.dayOpen = dayOpen ;
        this.modify("day_open",dayOpen);
    }
    /**
     * 设置 [街道 2]
     */
    public void setStreet2(String street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }
    /**
     * 设置 [街道]
     */
    public void setStreet(String street){
        this.street = street ;
        this.modify("street",street);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }
    /**
     * 设置 [失去原因]
     */
    public void setLostReason(Integer lostReason){
        this.lostReason = lostReason ;
        this.modify("lost_reason",lostReason);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [销售员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [省份]
     */
    public void setStateId(Integer stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }
    /**
     * 设置 [媒介]
     */
    public void setMediumId(Integer mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }
    /**
     * 设置 [阶段]
     */
    public void setStageId(Integer stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }
    /**
     * 设置 [来源]
     */
    public void setSourceId(Integer sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }
    /**
     * 设置 [国家]
     */
    public void setCountryId(Integer countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }
    /**
     * 设置 [营销]
     */
    public void setCampaignId(Integer campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }
    /**
     * 设置 [客户]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Integer teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }
    /**
     * 设置 [称谓]
     */
    public void setTitle(Integer title){
        this.title = title ;
        this.modify("title",title);
    }

}


