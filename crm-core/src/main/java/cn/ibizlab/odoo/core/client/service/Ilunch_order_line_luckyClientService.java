package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ilunch_order_line_lucky;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_order_line_lucky] 服务对象接口
 */
public interface Ilunch_order_line_luckyClientService{

    public Ilunch_order_line_lucky createModel() ;

    public Page<Ilunch_order_line_lucky> fetchDefault(SearchContext context);

    public void get(Ilunch_order_line_lucky lunch_order_line_lucky);

    public void createBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies);

    public void updateBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies);

    public void create(Ilunch_order_line_lucky lunch_order_line_lucky);

    public void update(Ilunch_order_line_lucky lunch_order_line_lucky);

    public void remove(Ilunch_order_line_lucky lunch_order_line_lucky);

    public void removeBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies);

    public Page<Ilunch_order_line_lucky> select(SearchContext context);

    public void getDraft(Ilunch_order_line_lucky lunch_order_line_lucky);

}
