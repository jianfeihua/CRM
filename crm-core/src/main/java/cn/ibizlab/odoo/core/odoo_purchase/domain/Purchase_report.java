package cn.ibizlab.odoo.core.odoo_purchase.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [采购报表] 对象
 */
@Data
public class Purchase_report extends EntityClient implements Serializable {

    /**
     * 交货天数
     */
    @DEField(name = "delay_pass")
    @JSONField(name = "delay_pass")
    @JsonProperty("delay_pass")
    private Double delayPass;

    /**
     * 批准日期
     */
    @DEField(name = "date_approve")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_approve" , format="yyyy-MM-dd")
    @JsonProperty("date_approve")
    private Timestamp dateApprove;

    /**
     * 体积
     */
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;

    /**
     * # 明细行
     */
    @DEField(name = "nbr_lines")
    @JSONField(name = "nbr_lines")
    @JsonProperty("nbr_lines")
    private Integer nbrLines;

    /**
     * 采购 - 标准单价
     */
    @JSONField(name = "negociation")
    @JsonProperty("negociation")
    private Double negociation;

    /**
     * 订单状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 总价
     */
    @DEField(name = "price_total")
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private Double priceTotal;

    /**
     * 毛重
     */
    @JSONField(name = "weight")
    @JsonProperty("weight")
    private Double weight;

    /**
     * 单据日期
     */
    @DEField(name = "date_order")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_order" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_order")
    private Timestamp dateOrder;

    /**
     * 平均价格
     */
    @DEField(name = "price_average")
    @JSONField(name = "price_average")
    @JsonProperty("price_average")
    private Double priceAverage;

    /**
     * 验证天数
     */
    @JSONField(name = "delay")
    @JsonProperty("delay")
    private Double delay;

    /**
     * 数量
     */
    @DEField(name = "unit_quantity")
    @JSONField(name = "unit_quantity")
    @JsonProperty("unit_quantity")
    private Double unitQuantity;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 产品价值
     */
    @DEField(name = "price_standard")
    @JSONField(name = "price_standard")
    @JsonProperty("price_standard")
    private Double priceStandard;

    /**
     * 产品种类
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 供应商
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 产品模板
     */
    @JSONField(name = "product_tmpl_id_text")
    @JsonProperty("product_tmpl_id_text")
    private String productTmplIdText;

    /**
     * 业务伙伴国家
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 仓库
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;

    /**
     * 税科目调整
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    private String fiscalPositionIdText;

    /**
     * 分析账户
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    private String accountAnalyticIdText;

    /**
     * 采购员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 商业实体
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;

    /**
     * 参考计量单位
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 产品种类
     */
    @DEField(name = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 仓库
     */
    @DEField(name = "picking_type_id")
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Integer pickingTypeId;

    /**
     * 采购员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 商业实体
     */
    @DEField(name = "commercial_partner_id")
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Integer commercialPartnerId;

    /**
     * 产品模板
     */
    @DEField(name = "product_tmpl_id")
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;

    /**
     * 供应商
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 参考计量单位
     */
    @DEField(name = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Integer productUom;

    /**
     * 业务伙伴国家
     */
    @DEField(name = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 税科目调整
     */
    @DEField(name = "fiscal_position_id")
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    private Integer fiscalPositionId;

    /**
     * 分析账户
     */
    @DEField(name = "account_analytic_id")
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    private Integer accountAnalyticId;


    /**
     * 
     */
    @JSONField(name = "odooaccountanalytic")
    @JsonProperty("odooaccountanalytic")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic;

    /**
     * 
     */
    @JSONField(name = "odoofiscalposition")
    @JsonProperty("odoofiscalposition")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition;

    /**
     * 
     */
    @JSONField(name = "odoocategory")
    @JsonProperty("odoocategory")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_category odooCategory;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odooproducttmpl")
    @JsonProperty("odooproducttmpl")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_template odooProductTmpl;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocountry")
    @JsonProperty("odoocountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocommercialpartner")
    @JsonProperty("odoocommercialpartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooCommercialPartner;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoopickingtype")
    @JsonProperty("odoopickingtype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooPickingType;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [交货天数]
     */
    public void setDelayPass(Double delayPass){
        this.delayPass = delayPass ;
        this.modify("delay_pass",delayPass);
    }
    /**
     * 设置 [批准日期]
     */
    public void setDateApprove(Timestamp dateApprove){
        this.dateApprove = dateApprove ;
        this.modify("date_approve",dateApprove);
    }
    /**
     * 设置 [体积]
     */
    public void setVolume(Double volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }
    /**
     * 设置 [# 明细行]
     */
    public void setNbrLines(Integer nbrLines){
        this.nbrLines = nbrLines ;
        this.modify("nbr_lines",nbrLines);
    }
    /**
     * 设置 [采购 - 标准单价]
     */
    public void setNegociation(Double negociation){
        this.negociation = negociation ;
        this.modify("negociation",negociation);
    }
    /**
     * 设置 [订单状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [总价]
     */
    public void setPriceTotal(Double priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }
    /**
     * 设置 [毛重]
     */
    public void setWeight(Double weight){
        this.weight = weight ;
        this.modify("weight",weight);
    }
    /**
     * 设置 [单据日期]
     */
    public void setDateOrder(Timestamp dateOrder){
        this.dateOrder = dateOrder ;
        this.modify("date_order",dateOrder);
    }
    /**
     * 设置 [平均价格]
     */
    public void setPriceAverage(Double priceAverage){
        this.priceAverage = priceAverage ;
        this.modify("price_average",priceAverage);
    }
    /**
     * 设置 [验证天数]
     */
    public void setDelay(Double delay){
        this.delay = delay ;
        this.modify("delay",delay);
    }
    /**
     * 设置 [数量]
     */
    public void setUnitQuantity(Double unitQuantity){
        this.unitQuantity = unitQuantity ;
        this.modify("unit_quantity",unitQuantity);
    }
    /**
     * 设置 [产品价值]
     */
    public void setPriceStandard(Double priceStandard){
        this.priceStandard = priceStandard ;
        this.modify("price_standard",priceStandard);
    }
    /**
     * 设置 [产品种类]
     */
    public void setCategoryId(Integer categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [仓库]
     */
    public void setPickingTypeId(Integer pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }
    /**
     * 设置 [采购员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [商业实体]
     */
    public void setCommercialPartnerId(Integer commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }
    /**
     * 设置 [产品模板]
     */
    public void setProductTmplId(Integer productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }
    /**
     * 设置 [供应商]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [参考计量单位]
     */
    public void setProductUom(Integer productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }
    /**
     * 设置 [业务伙伴国家]
     */
    public void setCountryId(Integer countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }
    /**
     * 设置 [税科目调整]
     */
    public void setFiscalPositionId(Integer fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }
    /**
     * 设置 [分析账户]
     */
    public void setAccountAnalyticId(Integer accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

}


