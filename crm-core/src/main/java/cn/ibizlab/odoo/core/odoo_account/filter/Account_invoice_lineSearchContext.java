package cn.ibizlab.odoo.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Account_invoice_line] 查询条件对象
 */
@Slf4j
@Data
public class Account_invoice_lineSearchContext extends SearchContextBase {
	private String n_display_type_eq;//[显示类型]

	private String n_name_like;//[说明]

	private String n_currency_id_text_eq;//[币种]

	private String n_currency_id_text_like;//[币种]

	private String n_account_analytic_id_text_eq;//[分析账户]

	private String n_account_analytic_id_text_like;//[分析账户]

	private String n_product_id_text_eq;//[产品]

	private String n_product_id_text_like;//[产品]

	private String n_invoice_id_text_eq;//[发票参考]

	private String n_invoice_id_text_like;//[发票参考]

	private String n_partner_id_text_eq;//[业务伙伴]

	private String n_partner_id_text_like;//[业务伙伴]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_uom_id_text_eq;//[计量单位]

	private String n_uom_id_text_like;//[计量单位]

	private String n_account_id_text_eq;//[科目]

	private String n_account_id_text_like;//[科目]

	private String n_purchase_line_id_text_eq;//[采购订单行]

	private String n_purchase_line_id_text_like;//[采购订单行]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private Integer n_invoice_id_eq;//[发票参考]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_product_id_eq;//[产品]

	private Integer n_account_id_eq;//[科目]

	private Integer n_purchase_line_id_eq;//[采购订单行]

	private Integer n_account_analytic_id_eq;//[分析账户]

	private Integer n_partner_id_eq;//[业务伙伴]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_currency_id_eq;//[币种]

	private Integer n_uom_id_eq;//[计量单位]

	private Integer n_company_id_eq;//[公司]

}



