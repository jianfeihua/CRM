package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_incotermsSearchContext;


/**
 * 实体[Account_incoterms] 服务对象接口
 */
public interface IAccount_incotermsService{

    Account_incoterms getDraft(Account_incoterms et) ;
    Account_incoterms get(Integer key) ;
    boolean create(Account_incoterms et) ;
    void createBatch(List<Account_incoterms> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_incoterms et) ;
    void updateBatch(List<Account_incoterms> list) ;
    Page<Account_incoterms> searchDefault(Account_incotermsSearchContext context) ;

}



