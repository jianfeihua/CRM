package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [calendar_event] 对象
 */
public interface Icalendar_event {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [提醒]
     */
    public void setAlarm_ids(String alarm_ids);
    
    /**
     * 设置 [提醒]
     */
    public String getAlarm_ids();

    /**
     * 获取 [提醒]脏标记
     */
    public boolean getAlarm_idsDirtyFlag();
    /**
     * 获取 [全天]
     */
    public void setAllday(String allday);
    
    /**
     * 设置 [全天]
     */
    public String getAllday();

    /**
     * 获取 [全天]脏标记
     */
    public boolean getAlldayDirtyFlag();
    /**
     * 获取 [申请人]
     */
    public void setApplicant_id(Integer applicant_id);
    
    /**
     * 设置 [申请人]
     */
    public Integer getApplicant_id();

    /**
     * 获取 [申请人]脏标记
     */
    public boolean getApplicant_idDirtyFlag();
    /**
     * 获取 [申请人]
     */
    public void setApplicant_id_text(String applicant_id_text);
    
    /**
     * 设置 [申请人]
     */
    public String getApplicant_id_text();

    /**
     * 获取 [申请人]脏标记
     */
    public boolean getApplicant_id_textDirtyFlag();
    /**
     * 获取 [参与者]
     */
    public void setAttendee_ids(String attendee_ids);
    
    /**
     * 设置 [参与者]
     */
    public String getAttendee_ids();

    /**
     * 获取 [参与者]脏标记
     */
    public boolean getAttendee_idsDirtyFlag();
    /**
     * 获取 [出席者状态]
     */
    public void setAttendee_status(String attendee_status);
    
    /**
     * 设置 [出席者状态]
     */
    public String getAttendee_status();

    /**
     * 获取 [出席者状态]脏标记
     */
    public boolean getAttendee_statusDirtyFlag();
    /**
     * 获取 [按 天]
     */
    public void setByday(String byday);
    
    /**
     * 设置 [按 天]
     */
    public String getByday();

    /**
     * 获取 [按 天]脏标记
     */
    public boolean getBydayDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setCateg_ids(String categ_ids);
    
    /**
     * 设置 [标签]
     */
    public String getCateg_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getCateg_idsDirtyFlag();
    /**
     * 获取 [重复]
     */
    public void setCount(Integer count);
    
    /**
     * 设置 [重复]
     */
    public Integer getCount();

    /**
     * 获取 [重复]脏标记
     */
    public boolean getCountDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDay(Integer day);
    
    /**
     * 设置 [日期]
     */
    public Integer getDay();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDayDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDisplay_start(String display_start);
    
    /**
     * 设置 [日期]
     */
    public String getDisplay_start();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDisplay_startDirtyFlag();
    /**
     * 获取 [活动时间]
     */
    public void setDisplay_time(String display_time);
    
    /**
     * 设置 [活动时间]
     */
    public String getDisplay_time();

    /**
     * 获取 [活动时间]脏标记
     */
    public boolean getDisplay_timeDirtyFlag();
    /**
     * 获取 [持续时间]
     */
    public void setDuration(Double duration);
    
    /**
     * 设置 [持续时间]
     */
    public Double getDuration();

    /**
     * 获取 [持续时间]脏标记
     */
    public boolean getDurationDirtyFlag();
    /**
     * 获取 [重复终止]
     */
    public void setEnd_type(String end_type);
    
    /**
     * 设置 [重复终止]
     */
    public String getEnd_type();

    /**
     * 获取 [重复终止]脏标记
     */
    public boolean getEnd_typeDirtyFlag();
    /**
     * 获取 [重复直到]
     */
    public void setFinal_date(Timestamp final_date);
    
    /**
     * 设置 [重复直到]
     */
    public Timestamp getFinal_date();

    /**
     * 获取 [重复直到]脏标记
     */
    public boolean getFinal_dateDirtyFlag();
    /**
     * 获取 [周五]
     */
    public void setFr(String fr);
    
    /**
     * 设置 [周五]
     */
    public String getFr();

    /**
     * 获取 [周五]脏标记
     */
    public boolean getFrDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [重复]
     */
    public void setInterval(Integer interval);
    
    /**
     * 设置 [重复]
     */
    public Integer getInterval();

    /**
     * 获取 [重复]脏标记
     */
    public boolean getIntervalDirtyFlag();
    /**
     * 获取 [出席者]
     */
    public void setIs_attendee(String is_attendee);
    
    /**
     * 设置 [出席者]
     */
    public String getIs_attendee();

    /**
     * 获取 [出席者]脏标记
     */
    public boolean getIs_attendeeDirtyFlag();
    /**
     * 获取 [事件是否突出显示]
     */
    public void setIs_highlighted(String is_highlighted);
    
    /**
     * 设置 [事件是否突出显示]
     */
    public String getIs_highlighted();

    /**
     * 获取 [事件是否突出显示]脏标记
     */
    public boolean getIs_highlightedDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setLocation(String location);
    
    /**
     * 设置 [地点]
     */
    public String getLocation();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getLocationDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [周一]
     */
    public void setMo(String mo);
    
    /**
     * 设置 [周一]
     */
    public String getMo();

    /**
     * 获取 [周一]脏标记
     */
    public boolean getMoDirtyFlag();
    /**
     * 获取 [选项]
     */
    public void setMonth_by(String month_by);
    
    /**
     * 设置 [选项]
     */
    public String getMonth_by();

    /**
     * 获取 [选项]脏标记
     */
    public boolean getMonth_byDirtyFlag();
    /**
     * 获取 [会议主题]
     */
    public void setName(String name);
    
    /**
     * 设置 [会议主题]
     */
    public String getName();

    /**
     * 获取 [会议主题]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setOpportunity_id(Integer opportunity_id);
    
    /**
     * 设置 [商机]
     */
    public Integer getOpportunity_id();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getOpportunity_idDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setOpportunity_id_text(String opportunity_id_text);
    
    /**
     * 设置 [商机]
     */
    public String getOpportunity_id_text();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getOpportunity_id_textDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getPartner_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [与会者]
     */
    public void setPartner_ids(String partner_ids);
    
    /**
     * 设置 [与会者]
     */
    public String getPartner_ids();

    /**
     * 获取 [与会者]脏标记
     */
    public boolean getPartner_idsDirtyFlag();
    /**
     * 获取 [隐私]
     */
    public void setPrivacy(String privacy);
    
    /**
     * 设置 [隐私]
     */
    public String getPrivacy();

    /**
     * 获取 [隐私]脏标记
     */
    public boolean getPrivacyDirtyFlag();
    /**
     * 获取 [循环]
     */
    public void setRecurrency(String recurrency);
    
    /**
     * 设置 [循环]
     */
    public String getRecurrency();

    /**
     * 获取 [循环]脏标记
     */
    public boolean getRecurrencyDirtyFlag();
    /**
     * 获取 [循环ID]
     */
    public void setRecurrent_id(Integer recurrent_id);
    
    /**
     * 设置 [循环ID]
     */
    public Integer getRecurrent_id();

    /**
     * 获取 [循环ID]脏标记
     */
    public boolean getRecurrent_idDirtyFlag();
    /**
     * 获取 [循环ID日期]
     */
    public void setRecurrent_id_date(Timestamp recurrent_id_date);
    
    /**
     * 设置 [循环ID日期]
     */
    public Timestamp getRecurrent_id_date();

    /**
     * 获取 [循环ID日期]脏标记
     */
    public boolean getRecurrent_id_dateDirtyFlag();
    /**
     * 获取 [文档ID]
     */
    public void setRes_id(Integer res_id);
    
    /**
     * 设置 [文档ID]
     */
    public Integer getRes_id();

    /**
     * 获取 [文档ID]脏标记
     */
    public boolean getRes_idDirtyFlag();
    /**
     * 获取 [文档模型名称]
     */
    public void setRes_model(String res_model);
    
    /**
     * 设置 [文档模型名称]
     */
    public String getRes_model();

    /**
     * 获取 [文档模型名称]脏标记
     */
    public boolean getRes_modelDirtyFlag();
    /**
     * 获取 [文档模型]
     */
    public void setRes_model_id(Integer res_model_id);
    
    /**
     * 设置 [文档模型]
     */
    public Integer getRes_model_id();

    /**
     * 获取 [文档模型]脏标记
     */
    public boolean getRes_model_idDirtyFlag();
    /**
     * 获取 [循环规则]
     */
    public void setRrule(String rrule);
    
    /**
     * 设置 [循环规则]
     */
    public String getRrule();

    /**
     * 获取 [循环规则]脏标记
     */
    public boolean getRruleDirtyFlag();
    /**
     * 获取 [重新提起]
     */
    public void setRrule_type(String rrule_type);
    
    /**
     * 设置 [重新提起]
     */
    public String getRrule_type();

    /**
     * 获取 [重新提起]脏标记
     */
    public boolean getRrule_typeDirtyFlag();
    /**
     * 获取 [周六]
     */
    public void setSa(String sa);
    
    /**
     * 设置 [周六]
     */
    public String getSa();

    /**
     * 获取 [周六]脏标记
     */
    public boolean getSaDirtyFlag();
    /**
     * 获取 [显示时间为]
     */
    public void setShow_as(String show_as);
    
    /**
     * 设置 [显示时间为]
     */
    public String getShow_as();

    /**
     * 获取 [显示时间为]脏标记
     */
    public boolean getShow_asDirtyFlag();
    /**
     * 获取 [开始]
     */
    public void setStart(Timestamp start);
    
    /**
     * 设置 [开始]
     */
    public Timestamp getStart();

    /**
     * 获取 [开始]脏标记
     */
    public boolean getStartDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setStart_date(Timestamp start_date);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getStart_date();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getStart_dateDirtyFlag();
    /**
     * 获取 [开始时间]
     */
    public void setStart_datetime(Timestamp start_datetime);
    
    /**
     * 设置 [开始时间]
     */
    public Timestamp getStart_datetime();

    /**
     * 获取 [开始时间]脏标记
     */
    public boolean getStart_datetimeDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [停止]
     */
    public void setStop(Timestamp stop);
    
    /**
     * 设置 [停止]
     */
    public Timestamp getStop();

    /**
     * 获取 [停止]脏标记
     */
    public boolean getStopDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setStop_date(Timestamp stop_date);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getStop_date();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getStop_dateDirtyFlag();
    /**
     * 获取 [结束日期时间]
     */
    public void setStop_datetime(Timestamp stop_datetime);
    
    /**
     * 设置 [结束日期时间]
     */
    public Timestamp getStop_datetime();

    /**
     * 获取 [结束日期时间]脏标记
     */
    public boolean getStop_datetimeDirtyFlag();
    /**
     * 获取 [周日]
     */
    public void setSu(String su);
    
    /**
     * 设置 [周日]
     */
    public String getSu();

    /**
     * 获取 [周日]脏标记
     */
    public boolean getSuDirtyFlag();
    /**
     * 获取 [周四]
     */
    public void setTh(String th);
    
    /**
     * 设置 [周四]
     */
    public String getTh();

    /**
     * 获取 [周四]脏标记
     */
    public boolean getThDirtyFlag();
    /**
     * 获取 [周二]
     */
    public void setTu(String tu);
    
    /**
     * 设置 [周二]
     */
    public String getTu();

    /**
     * 获取 [周二]脏标记
     */
    public boolean getTuDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getUser_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getUser_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [周三]
     */
    public void setWe(String we);
    
    /**
     * 设置 [周三]
     */
    public String getWe();

    /**
     * 获取 [周三]脏标记
     */
    public boolean getWeDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [工作日]
     */
    public void setWeek_list(String week_list);
    
    /**
     * 设置 [工作日]
     */
    public String getWeek_list();

    /**
     * 获取 [工作日]脏标记
     */
    public boolean getWeek_listDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
