package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_update_translations] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "base-update-translations", fallback = base_update_translationsFallback.class)
public interface base_update_translationsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_update_translations/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_update_translations/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_update_translations/{id}")
    Base_update_translations update(@PathVariable("id") Integer id,@RequestBody Base_update_translations base_update_translations);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_update_translations/batch")
    Boolean updateBatch(@RequestBody List<Base_update_translations> base_update_translations);


    @RequestMapping(method = RequestMethod.POST, value = "/base_update_translations")
    Base_update_translations create(@RequestBody Base_update_translations base_update_translations);

    @RequestMapping(method = RequestMethod.POST, value = "/base_update_translations/batch")
    Boolean createBatch(@RequestBody List<Base_update_translations> base_update_translations);




    @RequestMapping(method = RequestMethod.POST, value = "/base_update_translations/searchdefault")
    Page<Base_update_translations> searchDefault(@RequestBody Base_update_translationsSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/base_update_translations/{id}")
    Base_update_translations get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/base_update_translations/select")
    Page<Base_update_translations> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_update_translations/getdraft")
    Base_update_translations getDraft();


}
