package cn.ibizlab.odoo.core.odoo_product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [产品模板] 对象
 */
@Data
public class Product_template extends EntityClient implements Serializable {

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 样式
     */
    @JSONField(name = "website_style_ids")
    @JsonProperty("website_style_ids")
    private String websiteStyleIds;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 采购
     */
    @DEField(name = "purchase_ok")
    @JSONField(name = "purchase_ok")
    @JsonProperty("purchase_ok")
    private String purchaseOk;

    /**
     * 订货规则
     */
    @JSONField(name = "nbr_reordering_rules")
    @JsonProperty("nbr_reordering_rules")
    private Integer nbrReorderingRules;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 产品包裹
     */
    @JSONField(name = "packaging_ids")
    @JsonProperty("packaging_ids")
    private String packagingIds;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 最新反馈评级
     */
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    private String ratingLastFeedback;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * Valid Product Attributes Without No Variant Attributes
     */
    @JSONField(name = "valid_product_attribute_wnva_ids")
    @JsonProperty("valid_product_attribute_wnva_ids")
    private String validProductAttributeWnvaIds;

    /**
     * 可用阈值
     */
    @DEField(name = "available_threshold")
    @JSONField(name = "available_threshold")
    @JsonProperty("available_threshold")
    private Double availableThreshold;

    /**
     * 采购订单行
     */
    @DEField(name = "purchase_line_warn")
    @JSONField(name = "purchase_line_warn")
    @JsonProperty("purchase_line_warn")
    private String purchaseLineWarn;

    /**
     * 供应商
     */
    @JSONField(name = "seller_ids")
    @JsonProperty("seller_ids")
    private String sellerIds;

    /**
     * 已生产
     */
    @JSONField(name = "mrp_product_qty")
    @JsonProperty("mrp_product_qty")
    private Double mrpProductQty;

    /**
     * 产品
     */
    @JSONField(name = "product_variant_id")
    @JsonProperty("product_variant_id")
    private Integer productVariantId;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 评级数
     */
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;

    /**
     * 成本方法
     */
    @JSONField(name = "property_cost_method")
    @JsonProperty("property_cost_method")
    private String propertyCostMethod;

    /**
     * 自动采购
     */
    @DEField(name = "service_to_purchase")
    @JSONField(name = "service_to_purchase")
    @JsonProperty("service_to_purchase")
    private String serviceToPurchase;

    /**
     * 库存进货科目
     */
    @JSONField(name = "property_stock_account_input")
    @JsonProperty("property_stock_account_input")
    private Integer propertyStockAccountInput;

    /**
     * 采购订单明细的消息
     */
    @DEField(name = "purchase_line_warn_msg")
    @JSONField(name = "purchase_line_warn_msg")
    @JsonProperty("purchase_line_warn_msg")
    private String purchaseLineWarnMsg;

    /**
     * Can be Part
     */
    @JSONField(name = "isparts")
    @JsonProperty("isparts")
    private String isparts;

    /**
     * 网站价格差异
     */
    @JSONField(name = "website_price_difference")
    @JsonProperty("website_price_difference")
    private String websitePriceDifference;

    /**
     * 网站meta标题
     */
    @DEField(name = "website_meta_title")
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 收货说明
     */
    @DEField(name = "description_pickingin")
    @JSONField(name = "description_pickingin")
    @JsonProperty("description_pickingin")
    private String descriptionPickingin;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * POS类别
     */
    @DEField(name = "pos_categ_id")
    @JSONField(name = "pos_categ_id")
    @JsonProperty("pos_categ_id")
    private Integer posCategId;

    /**
     * 可选产品
     */
    @JSONField(name = "optional_product_ids")
    @JsonProperty("optional_product_ids")
    private String optionalProductIds;

    /**
     * 测量的重量单位
     */
    @JSONField(name = "weight_uom_id")
    @JsonProperty("weight_uom_id")
    private Integer weightUomId;

    /**
     * 隐藏费用政策
     */
    @JSONField(name = "hide_expense_policy")
    @JsonProperty("hide_expense_policy")
    private String hideExpensePolicy;

    /**
     * 动作数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 开票策略
     */
    @DEField(name = "invoice_policy")
    @JSONField(name = "invoice_policy")
    @JsonProperty("invoice_policy")
    private String invoicePolicy;

    /**
     * 网站价格
     */
    @JSONField(name = "website_price")
    @JsonProperty("website_price")
    private Double websitePrice;

    /**
     * 产品类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 有效的产品属性值
     */
    @JSONField(name = "valid_product_attribute_value_ids")
    @JsonProperty("valid_product_attribute_value_ids")
    private String validProductAttributeValueIds;

    /**
     * Valid Archived Variants
     */
    @JSONField(name = "valid_archived_variant_ids")
    @JsonProperty("valid_archived_variant_ids")
    private String validArchivedVariantIds;

    /**
     * 重开收据规则
     */
    @DEField(name = "expense_policy")
    @JSONField(name = "expense_policy")
    @JsonProperty("expense_policy")
    private String expensePolicy;

    /**
     * SEO优化
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 最新值评级
     */
    @DEField(name = "rating_last_value")
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 收入科目
     */
    @JSONField(name = "property_account_income_id")
    @JsonProperty("property_account_income_id")
    private Integer propertyAccountIncomeId;

    /**
     * 替代产品
     */
    @JSONField(name = "alternative_product_ids")
    @JsonProperty("alternative_product_ids")
    private String alternativeProductIds;

    /**
     * Valid Existing Variants
     */
    @JSONField(name = "valid_existing_variant_ids")
    @JsonProperty("valid_existing_variant_ids")
    private String validExistingVariantIds;

    /**
     * # 产品变体
     */
    @JSONField(name = "product_variant_count")
    @JsonProperty("product_variant_count")
    private Integer productVariantCount;

    /**
     * 已采购
     */
    @JSONField(name = "purchased_product_qty")
    @JsonProperty("purchased_product_qty")
    private Double purchasedProductQty;

    /**
     * Valid Product Attribute Lines
     */
    @JSONField(name = "valid_product_template_attribute_line_ids")
    @JsonProperty("valid_product_template_attribute_line_ids")
    private String validProductTemplateAttributeLineIds;

    /**
     * 拣货说明
     */
    @DEField(name = "description_picking")
    @JSONField(name = "description_picking")
    @JsonProperty("description_picking")
    private String descriptionPicking;

    /**
     * 是一张活动票吗？
     */
    @DEField(name = "event_ok")
    @JSONField(name = "event_ok")
    @JsonProperty("event_ok")
    private String eventOk;

    /**
     * 成本
     */
    @JSONField(name = "standard_price")
    @JsonProperty("standard_price")
    private Double standardPrice;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * BOM组件
     */
    @JSONField(name = "bom_line_ids")
    @JsonProperty("bom_line_ids")
    private String bomLineIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最新图像评级
     */
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;

    /**
     * 在手数量
     */
    @JSONField(name = "qty_available")
    @JsonProperty("qty_available")
    private Double qtyAvailable;

    /**
     * 成本方法
     */
    @JSONField(name = "cost_method")
    @JsonProperty("cost_method")
    private String costMethod;

    /**
     * 网站opengraph图像
     */
    @DEField(name = "website_meta_og_img")
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 网站meta关键词
     */
    @DEField(name = "website_meta_keywords")
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 小尺寸图像
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 价格表
     */
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Integer pricelistId;

    /**
     * 尺寸 X
     */
    @DEField(name = "website_size_x")
    @JSONField(name = "website_size_x")
    @JsonProperty("website_size_x")
    private Integer websiteSizeX;

    /**
     * 价格
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 出租
     */
    @JSONField(name = "rental")
    @JsonProperty("rental")
    private String rental;

    /**
     * 出向
     */
    @JSONField(name = "outgoing_qty")
    @JsonProperty("outgoing_qty")
    private Double outgoingQty;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 库存出货科目
     */
    @JSONField(name = "property_stock_account_output")
    @JsonProperty("property_stock_account_output")
    private Integer propertyStockAccountOutput;

    /**
     * 路线
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 费用科目
     */
    @JSONField(name = "property_account_expense_id")
    @JsonProperty("property_account_expense_id")
    private Integer propertyAccountExpenseId;

    /**
     * 已售出
     */
    @JSONField(name = "sales_count")
    @JsonProperty("sales_count")
    private Double salesCount;

    /**
     * 重订货最小数量
     */
    @JSONField(name = "reordering_min_qty")
    @JsonProperty("reordering_min_qty")
    private Double reorderingMinQty;

    /**
     * 称重
     */
    @DEField(name = "to_weight")
    @JSONField(name = "to_weight")
    @JsonProperty("to_weight")
    private String toWeight;

    /**
     * Valid Product Attribute Values Without No Variant Attributes
     */
    @JSONField(name = "valid_product_attribute_value_wnva_ids")
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    private String validProductAttributeValueWnvaIds;

    /**
     * 价格表项目
     */
    @JSONField(name = "item_ids")
    @JsonProperty("item_ids")
    private String itemIds;

    /**
     * 进项税
     */
    @JSONField(name = "supplier_taxes_id")
    @JsonProperty("supplier_taxes_id")
    private String supplierTaxesId;

    /**
     * 体积
     */
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 生产位置
     */
    @JSONField(name = "property_stock_production")
    @JsonProperty("property_stock_production")
    private Integer propertyStockProduction;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 库存位置
     */
    @JSONField(name = "property_stock_inventory")
    @JsonProperty("property_stock_inventory")
    private Integer propertyStockInventory;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 销售订单行
     */
    @DEField(name = "sale_line_warn")
    @JSONField(name = "sale_line_warn")
    @JsonProperty("sale_line_warn")
    private String saleLineWarn;

    /**
     * 销售价格
     */
    @DEField(name = "list_price")
    @JSONField(name = "list_price")
    @JsonProperty("list_price")
    private Double listPrice;

    /**
     * 网站产品目录
     */
    @JSONField(name = "public_categ_ids")
    @JsonProperty("public_categ_ids")
    private String publicCategIds;

    /**
     * 计价
     */
    @JSONField(name = "valuation")
    @JsonProperty("valuation")
    private String valuation;

    /**
     * 出库单说明
     */
    @DEField(name = "description_pickingout")
    @JSONField(name = "description_pickingout")
    @JsonProperty("description_pickingout")
    private String descriptionPickingout;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 附件产品
     */
    @JSONField(name = "accessory_product_ids")
    @JsonProperty("accessory_product_ids")
    private String accessoryProductIds;

    /**
     * 类别路线
     */
    @JSONField(name = "route_from_categ_ids")
    @JsonProperty("route_from_categ_ids")
    private String routeFromCategIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 网站序列
     */
    @DEField(name = "website_sequence")
    @JSONField(name = "website_sequence")
    @JsonProperty("website_sequence")
    private Integer websiteSequence;

    /**
     * 是产品变体
     */
    @JSONField(name = "is_product_variant")
    @JsonProperty("is_product_variant")
    private String isProductVariant;

    /**
     * 地点
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 入库
     */
    @JSONField(name = "incoming_qty")
    @JsonProperty("incoming_qty")
    private Double incomingQty;

    /**
     * 评级
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * 网站元说明
     */
    @DEField(name = "website_meta_description")
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 物料清单
     */
    @JSONField(name = "bom_ids")
    @JsonProperty("bom_ids")
    private String bomIds;

    /**
     * 销售订单行消息
     */
    @DEField(name = "sale_line_warn_msg")
    @JSONField(name = "sale_line_warn_msg")
    @JsonProperty("sale_line_warn_msg")
    private String saleLineWarnMsg;

    /**
     * 控制策略
     */
    @DEField(name = "purchase_method")
    @JSONField(name = "purchase_method")
    @JsonProperty("purchase_method")
    private String purchaseMethod;

    /**
     * 制造提前期(日)
     */
    @DEField(name = "produce_delay")
    @JSONField(name = "produce_delay")
    @JsonProperty("produce_delay")
    private Double produceDelay;

    /**
     * # 物料清单
     */
    @JSONField(name = "bom_count")
    @JsonProperty("bom_count")
    private Integer bomCount;

    /**
     * 销项税
     */
    @JSONField(name = "taxes_id")
    @JsonProperty("taxes_id")
    private String taxesId;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 报销
     */
    @DEField(name = "can_be_expensed")
    @JSONField(name = "can_be_expensed")
    @JsonProperty("can_be_expensed")
    private String canBeExpensed;

    /**
     * 销售
     */
    @DEField(name = "sale_ok")
    @JSONField(name = "sale_ok")
    @JsonProperty("sale_ok")
    private String saleOk;

    /**
     * 跟踪服务
     */
    @DEField(name = "service_type")
    @JSONField(name = "service_type")
    @JsonProperty("service_type")
    private String serviceType;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 追踪
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * Valid Product Attribute Lines Without No Variant Attributes
     */
    @JSONField(name = "valid_product_template_attribute_line_wnva_ids")
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    private String validProductTemplateAttributeLineWnvaIds;

    /**
     * 价格差异科目
     */
    @JSONField(name = "property_account_creditor_price_difference")
    @JsonProperty("property_account_creditor_price_difference")
    private Integer propertyAccountCreditorPriceDifference;

    /**
     * 库存可用性
     */
    @DEField(name = "inventory_availability")
    @JSONField(name = "inventory_availability")
    @JsonProperty("inventory_availability")
    private String inventoryAvailability;

    /**
     * 尺寸 Y
     */
    @DEField(name = "website_size_y")
    @JSONField(name = "website_size_y")
    @JsonProperty("website_size_y")
    private Integer websiteSizeY;

    /**
     * 中等尺寸图像
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 有效的产品属性
     */
    @JSONField(name = "valid_product_attribute_ids")
    @JsonProperty("valid_product_attribute_ids")
    private String validProductAttributeIds;

    /**
     * 公开价格
     */
    @JSONField(name = "lst_price")
    @JsonProperty("lst_price")
    private Double lstPrice;

    /**
     * 自定义消息
     */
    @DEField(name = "custom_message")
    @JSONField(name = "custom_message")
    @JsonProperty("custom_message")
    private String customMessage;

    /**
     * POS可用
     */
    @DEField(name = "available_in_pos")
    @JSONField(name = "available_in_pos")
    @JsonProperty("available_in_pos")
    private String availableInPos;

    /**
     * 重量计量单位标签
     */
    @JSONField(name = "weight_uom_name")
    @JsonProperty("weight_uom_name")
    private String weightUomName;

    /**
     * 成本币种
     */
    @JSONField(name = "cost_currency_id")
    @JsonProperty("cost_currency_id")
    private Integer costCurrencyId;

    /**
     * 产品属性
     */
    @JSONField(name = "attribute_line_ids")
    @JsonProperty("attribute_line_ids")
    private String attributeLineIds;

    /**
     * 重量
     */
    @JSONField(name = "weight")
    @JsonProperty("weight")
    private Double weight;

    /**
     * 预测数量
     */
    @JSONField(name = "virtual_available")
    @JsonProperty("virtual_available")
    private Double virtualAvailable;

    /**
     * 图片
     */
    @JSONField(name = "product_image_ids")
    @JsonProperty("product_image_ids")
    private String productImageIds;

    /**
     * # BOM 使用的地方
     */
    @JSONField(name = "used_in_bom_count")
    @JsonProperty("used_in_bom_count")
    private Integer usedInBomCount;

    /**
     * 内部参考
     */
    @DEField(name = "default_code")
    @JSONField(name = "default_code")
    @JsonProperty("default_code")
    private String defaultCode;

    /**
     * 条码
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 库存计价
     */
    @JSONField(name = "property_valuation")
    @JsonProperty("property_valuation")
    private String propertyValuation;

    /**
     * 网站的说明
     */
    @DEField(name = "website_description")
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 产品
     */
    @JSONField(name = "product_variant_ids")
    @JsonProperty("product_variant_ids")
    private String productVariantIds;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 网站公开价格
     */
    @JSONField(name = "website_public_price")
    @JsonProperty("website_public_price")
    private Double websitePublicPrice;

    /**
     * 重订货最大数量
     */
    @JSONField(name = "reordering_max_qty")
    @JsonProperty("reordering_max_qty")
    private Double reorderingMaxQty;

    /**
     * 客户前置时间
     */
    @DEField(name = "sale_delay")
    @JSONField(name = "sale_delay")
    @JsonProperty("sale_delay")
    private Double saleDelay;

    /**
     * 变种卖家
     */
    @JSONField(name = "variant_seller_ids")
    @JsonProperty("variant_seller_ids")
    private String variantSellerIds;

    /**
     * 采购说明
     */
    @DEField(name = "description_purchase")
    @JSONField(name = "description_purchase")
    @JsonProperty("description_purchase")
    private String descriptionPurchase;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 销售说明
     */
    @DEField(name = "description_sale")
    @JSONField(name = "description_sale")
    @JsonProperty("description_sale")
    private String descriptionSale;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 采购计量单位
     */
    @JSONField(name = "uom_po_id_text")
    @JsonProperty("uom_po_id_text")
    private String uomPoIdText;

    /**
     * 单位名称
     */
    @JSONField(name = "uom_name")
    @JsonProperty("uom_name")
    private String uomName;

    /**
     * 负责人
     */
    @JSONField(name = "responsible_id_text")
    @JsonProperty("responsible_id_text")
    private String responsibleIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 产品种类
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    private String categIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 产品种类
     */
    @DEField(name = "categ_id")
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Integer categId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 计量单位
     */
    @DEField(name = "uom_id")
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    private Integer uomId;

    /**
     * 采购计量单位
     */
    @DEField(name = "uom_po_id")
    @JSONField(name = "uom_po_id")
    @JsonProperty("uom_po_id")
    private Integer uomPoId;

    /**
     * 负责人
     */
    @DEField(name = "responsible_id")
    @JSONField(name = "responsible_id")
    @JsonProperty("responsible_id")
    private Integer responsibleId;


    /**
     * 
     */
    @JSONField(name = "odoocateg")
    @JsonProperty("odoocateg")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_category odooCateg;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odooresponsible")
    @JsonProperty("odooresponsible")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooResponsible;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoouom")
    @JsonProperty("odoouom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooUom;

    /**
     * 
     */
    @JSONField(name = "odoouompo")
    @JsonProperty("odoouompo")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooUomPo;




    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [采购]
     */
    public void setPurchaseOk(String purchaseOk){
        this.purchaseOk = purchaseOk ;
        this.modify("purchase_ok",purchaseOk);
    }
    /**
     * 设置 [可用阈值]
     */
    public void setAvailableThreshold(Double availableThreshold){
        this.availableThreshold = availableThreshold ;
        this.modify("available_threshold",availableThreshold);
    }
    /**
     * 设置 [采购订单行]
     */
    public void setPurchaseLineWarn(String purchaseLineWarn){
        this.purchaseLineWarn = purchaseLineWarn ;
        this.modify("purchase_line_warn",purchaseLineWarn);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [自动采购]
     */
    public void setServiceToPurchase(String serviceToPurchase){
        this.serviceToPurchase = serviceToPurchase ;
        this.modify("service_to_purchase",serviceToPurchase);
    }
    /**
     * 设置 [采购订单明细的消息]
     */
    public void setPurchaseLineWarnMsg(String purchaseLineWarnMsg){
        this.purchaseLineWarnMsg = purchaseLineWarnMsg ;
        this.modify("purchase_line_warn_msg",purchaseLineWarnMsg);
    }
    /**
     * 设置 [Can be Part]
     */
    public void setIsparts(String isparts){
        this.isparts = isparts ;
        this.modify("isparts",isparts);
    }
    /**
     * 设置 [网站meta标题]
     */
    public void setWebsiteMetaTitle(String websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }
    /**
     * 设置 [收货说明]
     */
    public void setDescriptionPickingin(String descriptionPickingin){
        this.descriptionPickingin = descriptionPickingin ;
        this.modify("description_pickingin",descriptionPickingin);
    }
    /**
     * 设置 [POS类别]
     */
    public void setPosCategId(Integer posCategId){
        this.posCategId = posCategId ;
        this.modify("pos_categ_id",posCategId);
    }
    /**
     * 设置 [开票策略]
     */
    public void setInvoicePolicy(String invoicePolicy){
        this.invoicePolicy = invoicePolicy ;
        this.modify("invoice_policy",invoicePolicy);
    }
    /**
     * 设置 [产品类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [重开收据规则]
     */
    public void setExpensePolicy(String expensePolicy){
        this.expensePolicy = expensePolicy ;
        this.modify("expense_policy",expensePolicy);
    }
    /**
     * 设置 [最新值评级]
     */
    public void setRatingLastValue(Double ratingLastValue){
        this.ratingLastValue = ratingLastValue ;
        this.modify("rating_last_value",ratingLastValue);
    }
    /**
     * 设置 [拣货说明]
     */
    public void setDescriptionPicking(String descriptionPicking){
        this.descriptionPicking = descriptionPicking ;
        this.modify("description_picking",descriptionPicking);
    }
    /**
     * 设置 [是一张活动票吗？]
     */
    public void setEventOk(String eventOk){
        this.eventOk = eventOk ;
        this.modify("event_ok",eventOk);
    }
    /**
     * 设置 [网站opengraph图像]
     */
    public void setWebsiteMetaOgImg(String websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [网站meta关键词]
     */
    public void setWebsiteMetaKeywords(String websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }
    /**
     * 设置 [尺寸 X]
     */
    public void setWebsiteSizeX(Integer websiteSizeX){
        this.websiteSizeX = websiteSizeX ;
        this.modify("website_size_x",websiteSizeX);
    }
    /**
     * 设置 [出租]
     */
    public void setRental(String rental){
        this.rental = rental ;
        this.modify("rental",rental);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [称重]
     */
    public void setToWeight(String toWeight){
        this.toWeight = toWeight ;
        this.modify("to_weight",toWeight);
    }
    /**
     * 设置 [体积]
     */
    public void setVolume(Double volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [销售订单行]
     */
    public void setSaleLineWarn(String saleLineWarn){
        this.saleLineWarn = saleLineWarn ;
        this.modify("sale_line_warn",saleLineWarn);
    }
    /**
     * 设置 [销售价格]
     */
    public void setListPrice(Double listPrice){
        this.listPrice = listPrice ;
        this.modify("list_price",listPrice);
    }
    /**
     * 设置 [出库单说明]
     */
    public void setDescriptionPickingout(String descriptionPickingout){
        this.descriptionPickingout = descriptionPickingout ;
        this.modify("description_pickingout",descriptionPickingout);
    }
    /**
     * 设置 [已发布]
     */
    public void setIsPublished(String isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [网站序列]
     */
    public void setWebsiteSequence(Integer websiteSequence){
        this.websiteSequence = websiteSequence ;
        this.modify("website_sequence",websiteSequence);
    }
    /**
     * 设置 [网站元说明]
     */
    public void setWebsiteMetaDescription(String websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }
    /**
     * 设置 [销售订单行消息]
     */
    public void setSaleLineWarnMsg(String saleLineWarnMsg){
        this.saleLineWarnMsg = saleLineWarnMsg ;
        this.modify("sale_line_warn_msg",saleLineWarnMsg);
    }
    /**
     * 设置 [控制策略]
     */
    public void setPurchaseMethod(String purchaseMethod){
        this.purchaseMethod = purchaseMethod ;
        this.modify("purchase_method",purchaseMethod);
    }
    /**
     * 设置 [制造提前期(日)]
     */
    public void setProduceDelay(Double produceDelay){
        this.produceDelay = produceDelay ;
        this.modify("produce_delay",produceDelay);
    }
    /**
     * 设置 [报销]
     */
    public void setCanBeExpensed(String canBeExpensed){
        this.canBeExpensed = canBeExpensed ;
        this.modify("can_be_expensed",canBeExpensed);
    }
    /**
     * 设置 [销售]
     */
    public void setSaleOk(String saleOk){
        this.saleOk = saleOk ;
        this.modify("sale_ok",saleOk);
    }
    /**
     * 设置 [跟踪服务]
     */
    public void setServiceType(String serviceType){
        this.serviceType = serviceType ;
        this.modify("service_type",serviceType);
    }
    /**
     * 设置 [追踪]
     */
    public void setTracking(String tracking){
        this.tracking = tracking ;
        this.modify("tracking",tracking);
    }
    /**
     * 设置 [库存可用性]
     */
    public void setInventoryAvailability(String inventoryAvailability){
        this.inventoryAvailability = inventoryAvailability ;
        this.modify("inventory_availability",inventoryAvailability);
    }
    /**
     * 设置 [尺寸 Y]
     */
    public void setWebsiteSizeY(Integer websiteSizeY){
        this.websiteSizeY = websiteSizeY ;
        this.modify("website_size_y",websiteSizeY);
    }
    /**
     * 设置 [自定义消息]
     */
    public void setCustomMessage(String customMessage){
        this.customMessage = customMessage ;
        this.modify("custom_message",customMessage);
    }
    /**
     * 设置 [POS可用]
     */
    public void setAvailableInPos(String availableInPos){
        this.availableInPos = availableInPos ;
        this.modify("available_in_pos",availableInPos);
    }
    /**
     * 设置 [重量]
     */
    public void setWeight(Double weight){
        this.weight = weight ;
        this.modify("weight",weight);
    }
    /**
     * 设置 [内部参考]
     */
    public void setDefaultCode(String defaultCode){
        this.defaultCode = defaultCode ;
        this.modify("default_code",defaultCode);
    }
    /**
     * 设置 [网站的说明]
     */
    public void setWebsiteDescription(String websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }
    /**
     * 设置 [客户前置时间]
     */
    public void setSaleDelay(Double saleDelay){
        this.saleDelay = saleDelay ;
        this.modify("sale_delay",saleDelay);
    }
    /**
     * 设置 [采购说明]
     */
    public void setDescriptionPurchase(String descriptionPurchase){
        this.descriptionPurchase = descriptionPurchase ;
        this.modify("description_purchase",descriptionPurchase);
    }
    /**
     * 设置 [销售说明]
     */
    public void setDescriptionSale(String descriptionSale){
        this.descriptionSale = descriptionSale ;
        this.modify("description_sale",descriptionSale);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [产品种类]
     */
    public void setCategId(Integer categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }
    /**
     * 设置 [计量单位]
     */
    public void setUomId(Integer uomId){
        this.uomId = uomId ;
        this.modify("uom_id",uomId);
    }
    /**
     * 设置 [采购计量单位]
     */
    public void setUomPoId(Integer uomPoId){
        this.uomPoId = uomPoId ;
        this.modify("uom_po_id",uomPoId);
    }
    /**
     * 设置 [负责人]
     */
    public void setResponsibleId(Integer responsibleId){
        this.responsibleId = responsibleId ;
        this.modify("responsible_id",responsibleId);
    }

}


