package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_leave_reportFeignClient;

/**
 * 实体[请假摘要/报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leave_reportServiceImpl implements IHr_leave_reportService {

    @Autowired
    hr_leave_reportFeignClient hr_leave_reportFeignClient;


    @Override
    public Hr_leave_report getDraft(Hr_leave_report et) {
        et=hr_leave_reportFeignClient.getDraft();
        return et;
    }

    @Override
    public Hr_leave_report get(Integer id) {
		Hr_leave_report et=hr_leave_reportFeignClient.get(id);
        if(et==null){
            et=new Hr_leave_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Hr_leave_report et) {
        Hr_leave_report rt = hr_leave_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave_report> list){
        hr_leave_reportFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Hr_leave_report et) {
        Hr_leave_report rt = hr_leave_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_leave_report> list){
        hr_leave_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_leave_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_leave_reportFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave_report> searchDefault(Hr_leave_reportSearchContext context) {
        Page<Hr_leave_report> hr_leave_reports=hr_leave_reportFeignClient.searchDefault(context);
        return hr_leave_reports;
    }


}


