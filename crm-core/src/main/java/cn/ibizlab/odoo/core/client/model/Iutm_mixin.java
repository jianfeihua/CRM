package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [utm_mixin] 对象
 */
public interface Iutm_mixin {

    /**
     * 获取 [营销]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [营销]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id_text(String campaign_id_text);
    
    /**
     * 设置 [营销]
     */
    public String getCampaign_id_text();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒体]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒体]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [来源]
     */
    public Integer getSource_id();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id_text(String source_id_text);
    
    /**
     * 设置 [来源]
     */
    public String getSource_id_text();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_id_textDirtyFlag();
    /**
     * 获取 [修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
