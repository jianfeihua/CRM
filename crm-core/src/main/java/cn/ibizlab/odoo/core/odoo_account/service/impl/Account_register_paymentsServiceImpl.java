package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_register_paymentsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_register_paymentsFeignClient;

/**
 * 实体[登记付款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_register_paymentsServiceImpl implements IAccount_register_paymentsService {

    @Autowired
    account_register_paymentsFeignClient account_register_paymentsFeignClient;


    @Override
    public boolean checkKey(Account_register_payments et) {
        return account_register_paymentsFeignClient.checkKey(et);
    }
    @Override
    @Transactional
    public boolean save(Account_register_payments et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!account_register_paymentsFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Account_register_payments> list) {
        account_register_paymentsFeignClient.saveBatch(list) ;
    }

    @Override
    public Account_register_payments get(Integer id) {
		Account_register_payments et=account_register_paymentsFeignClient.get(id);
        if(et==null){
            et=new Account_register_payments();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_register_payments et) {
        Account_register_payments rt = account_register_paymentsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_register_payments> list){
        account_register_paymentsFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_register_payments et) {
        Account_register_payments rt = account_register_paymentsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_register_payments> list){
        account_register_paymentsFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_register_paymentsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_register_paymentsFeignClient.removeBatch(idList);
    }

    @Override
    public Account_register_payments getDraft(Account_register_payments et) {
        et=account_register_paymentsFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_register_payments> searchDefault(Account_register_paymentsSearchContext context) {
        Page<Account_register_payments> account_register_paymentss=account_register_paymentsFeignClient.searchDefault(context);
        return account_register_paymentss;
    }


}


