package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_botSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_botService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_botFeignClient;

/**
 * 实体[邮件机器人] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_botServiceImpl implements IMail_botService {

    @Autowired
    mail_botFeignClient mail_botFeignClient;


    @Override
    public boolean update(Mail_bot et) {
        Mail_bot rt = mail_botFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_bot> list){
        mail_botFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_bot get(Integer id) {
		Mail_bot et=mail_botFeignClient.get(id);
        if(et==null){
            et=new Mail_bot();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_bot et) {
        Mail_bot rt = mail_botFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_bot> list){
        mail_botFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_botFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_botFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_bot getDraft(Mail_bot et) {
        et=mail_botFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_bot> searchDefault(Mail_botSearchContext context) {
        Page<Mail_bot> mail_bots=mail_botFeignClient.searchDefault(context);
        return mail_bots;
    }


}


