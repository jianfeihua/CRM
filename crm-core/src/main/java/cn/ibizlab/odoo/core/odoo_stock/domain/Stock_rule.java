package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [库存规则] 对象
 */
@Data
public class Stock_rule extends EntityClient implements Serializable {

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 移动供应方法
     */
    @DEField(name = "procure_method")
    @JSONField(name = "procure_method")
    @JsonProperty("procure_method")
    private String procureMethod;

    /**
     * 传播取消以及拆分
     */
    @JSONField(name = "propagate")
    @JsonProperty("propagate")
    private String propagate;

    /**
     * 固定的补货组
     */
    @DEField(name = "group_id")
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;

    /**
     * 规则消息
     */
    @JSONField(name = "rule_message")
    @JsonProperty("rule_message")
    private String ruleMessage;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 自动移动
     */
    @JSONField(name = "auto")
    @JsonProperty("auto")
    private String auto;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 延迟
     */
    @JSONField(name = "delay")
    @JsonProperty("delay")
    private Integer delay;

    /**
     * 补货组的传播
     */
    @DEField(name = "group_propagation_option")
    @JSONField(name = "group_propagation_option")
    @JsonProperty("group_propagation_option")
    private String groupPropagationOption;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 动作
     */
    @JSONField(name = "action")
    @JsonProperty("action")
    private String action;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 业务伙伴地址
     */
    @JSONField(name = "partner_address_id_text")
    @JsonProperty("partner_address_id_text")
    private String partnerAddressIdText;

    /**
     * 目的位置
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 路线
     */
    @JSONField(name = "route_id_text")
    @JsonProperty("route_id_text")
    private String routeIdText;

    /**
     * 路线序列
     */
    @JSONField(name = "route_sequence")
    @JsonProperty("route_sequence")
    private Integer routeSequence;

    /**
     * 作业类型
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;

    /**
     * 传播的仓库
     */
    @JSONField(name = "propagate_warehouse_id_text")
    @JsonProperty("propagate_warehouse_id_text")
    private String propagateWarehouseIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 源位置
     */
    @JSONField(name = "location_src_id_text")
    @JsonProperty("location_src_id_text")
    private String locationSrcIdText;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;

    /**
     * 目的位置
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 业务伙伴地址
     */
    @DEField(name = "partner_address_id")
    @JSONField(name = "partner_address_id")
    @JsonProperty("partner_address_id")
    private Integer partnerAddressId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 作业类型
     */
    @DEField(name = "picking_type_id")
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Integer pickingTypeId;

    /**
     * 路线
     */
    @DEField(name = "route_id")
    @JSONField(name = "route_id")
    @JsonProperty("route_id")
    private Integer routeId;

    /**
     * 源位置
     */
    @DEField(name = "location_src_id")
    @JSONField(name = "location_src_id")
    @JsonProperty("location_src_id")
    private Integer locationSrcId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 传播的仓库
     */
    @DEField(name = "propagate_warehouse_id")
    @JSONField(name = "propagate_warehouse_id")
    @JsonProperty("propagate_warehouse_id")
    private Integer propagateWarehouseId;


    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartneraddress")
    @JsonProperty("odoopartneraddress")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartnerAddress;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odooroute")
    @JsonProperty("odooroute")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route odooRoute;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JSONField(name = "odoolocationsrc")
    @JsonProperty("odoolocationsrc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocationSrc;

    /**
     * 
     */
    @JSONField(name = "odoopickingtype")
    @JsonProperty("odoopickingtype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooPickingType;

    /**
     * 
     */
    @JSONField(name = "odoopropagatewarehouse")
    @JsonProperty("odoopropagatewarehouse")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooPropagateWarehouse;

    /**
     * 
     */
    @JSONField(name = "odoowarehouse")
    @JsonProperty("odoowarehouse")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooWarehouse;




    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [移动供应方法]
     */
    public void setProcureMethod(String procureMethod){
        this.procureMethod = procureMethod ;
        this.modify("procure_method",procureMethod);
    }
    /**
     * 设置 [传播取消以及拆分]
     */
    public void setPropagate(String propagate){
        this.propagate = propagate ;
        this.modify("propagate",propagate);
    }
    /**
     * 设置 [固定的补货组]
     */
    public void setGroupId(Integer groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [自动移动]
     */
    public void setAuto(String auto){
        this.auto = auto ;
        this.modify("auto",auto);
    }
    /**
     * 设置 [延迟]
     */
    public void setDelay(Integer delay){
        this.delay = delay ;
        this.modify("delay",delay);
    }
    /**
     * 设置 [补货组的传播]
     */
    public void setGroupPropagationOption(String groupPropagationOption){
        this.groupPropagationOption = groupPropagationOption ;
        this.modify("group_propagation_option",groupPropagationOption);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [动作]
     */
    public void setAction(String action){
        this.action = action ;
        this.modify("action",action);
    }
    /**
     * 设置 [目的位置]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [业务伙伴地址]
     */
    public void setPartnerAddressId(Integer partnerAddressId){
        this.partnerAddressId = partnerAddressId ;
        this.modify("partner_address_id",partnerAddressId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Integer warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }
    /**
     * 设置 [作业类型]
     */
    public void setPickingTypeId(Integer pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }
    /**
     * 设置 [路线]
     */
    public void setRouteId(Integer routeId){
        this.routeId = routeId ;
        this.modify("route_id",routeId);
    }
    /**
     * 设置 [源位置]
     */
    public void setLocationSrcId(Integer locationSrcId){
        this.locationSrcId = locationSrcId ;
        this.modify("location_src_id",locationSrcId);
    }
    /**
     * 设置 [传播的仓库]
     */
    public void setPropagateWarehouseId(Integer propagateWarehouseId){
        this.propagateWarehouseId = propagateWarehouseId ;
        this.modify("propagate_warehouse_id",propagateWarehouseId);
    }

}


