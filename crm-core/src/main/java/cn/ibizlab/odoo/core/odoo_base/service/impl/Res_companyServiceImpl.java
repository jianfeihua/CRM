package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_companyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_companyFeignClient;

/**
 * 实体[公司] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_companyServiceImpl implements IRes_companyService {

    @Autowired
    res_companyFeignClient res_companyFeignClient;


    @Override
    public Res_company get(Integer id) {
		Res_company et=res_companyFeignClient.get(id);
        if(et==null){
            et=new Res_company();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Res_company getDraft(Res_company et) {
        et=res_companyFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_companyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_companyFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Res_company et) {
        Res_company rt = res_companyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_company> list){
        res_companyFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Res_company et) {
        Res_company rt = res_companyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_company> list){
        res_companyFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_company> searchDefault(Res_companySearchContext context) {
        Page<Res_company> res_companys=res_companyFeignClient.searchDefault(context);
        return res_companys;
    }


}


