package cn.ibizlab.odoo.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[portal_mixin] 服务对象接口
 */
@FeignClient(value = "odoo-portal", contextId = "portal-mixin", fallback = portal_mixinFallback.class)
public interface portal_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins")
    Portal_mixin create(@RequestBody Portal_mixin portal_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/batch")
    Boolean createBatch(@RequestBody List<Portal_mixin> portal_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/searchdefault")
    Page<Portal_mixin> searchDefault(@RequestBody Portal_mixinSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/{id}")
    Portal_mixin update(@PathVariable("id") Integer id,@RequestBody Portal_mixin portal_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/batch")
    Boolean updateBatch(@RequestBody List<Portal_mixin> portal_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/{id}")
    Portal_mixin get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/select")
    Page<Portal_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/getdraft")
    Portal_mixin getDraft();


}
