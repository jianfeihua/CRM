package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [sale_payment_acquirer_onboarding_wizard] 对象
 */
public interface Isale_payment_acquirer_onboarding_wizard {

    /**
     * 获取 [账户号码]
     */
    public void setAcc_number(String acc_number);
    
    /**
     * 设置 [账户号码]
     */
    public String getAcc_number();

    /**
     * 获取 [账户号码]脏标记
     */
    public boolean getAcc_numberDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [银行名称]
     */
    public void setJournal_name(String journal_name);
    
    /**
     * 设置 [银行名称]
     */
    public String getJournal_name();

    /**
     * 获取 [银行名称]脏标记
     */
    public boolean getJournal_nameDirtyFlag();
    /**
     * 获取 [方法]
     */
    public void setManual_name(String manual_name);
    
    /**
     * 设置 [方法]
     */
    public String getManual_name();

    /**
     * 获取 [方法]脏标记
     */
    public boolean getManual_nameDirtyFlag();
    /**
     * 获取 [支付说明]
     */
    public void setManual_post_msg(String manual_post_msg);
    
    /**
     * 设置 [支付说明]
     */
    public String getManual_post_msg();

    /**
     * 获取 [支付说明]脏标记
     */
    public boolean getManual_post_msgDirtyFlag();
    /**
     * 获取 [付款方法]
     */
    public void setPayment_method(String payment_method);
    
    /**
     * 设置 [付款方法]
     */
    public String getPayment_method();

    /**
     * 获取 [付款方法]脏标记
     */
    public boolean getPayment_methodDirtyFlag();
    /**
     * 获取 [PayPal EMailID]
     */
    public void setPaypal_email_account(String paypal_email_account);
    
    /**
     * 设置 [PayPal EMailID]
     */
    public String getPaypal_email_account();

    /**
     * 获取 [PayPal EMailID]脏标记
     */
    public boolean getPaypal_email_accountDirtyFlag();
    /**
     * 获取 [Paypal 付款数据传输标记]
     */
    public void setPaypal_pdt_token(String paypal_pdt_token);
    
    /**
     * 设置 [Paypal 付款数据传输标记]
     */
    public String getPaypal_pdt_token();

    /**
     * 获取 [Paypal 付款数据传输标记]脏标记
     */
    public boolean getPaypal_pdt_tokenDirtyFlag();
    /**
     * 获取 [Paypal 销售商 ID]
     */
    public void setPaypal_seller_account(String paypal_seller_account);
    
    /**
     * 设置 [Paypal 销售商 ID]
     */
    public String getPaypal_seller_account();

    /**
     * 获取 [Paypal 销售商 ID]脏标记
     */
    public boolean getPaypal_seller_accountDirtyFlag();
    /**
     * 获取 [Stripe 公钥]
     */
    public void setStripe_publishable_key(String stripe_publishable_key);
    
    /**
     * 设置 [Stripe 公钥]
     */
    public String getStripe_publishable_key();

    /**
     * 获取 [Stripe 公钥]脏标记
     */
    public boolean getStripe_publishable_keyDirtyFlag();
    /**
     * 获取 [Stripe 密钥]
     */
    public void setStripe_secret_key(String stripe_secret_key);
    
    /**
     * 设置 [Stripe 密钥]
     */
    public String getStripe_secret_key();

    /**
     * 获取 [Stripe 密钥]脏标记
     */
    public boolean getStripe_secret_keyDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
