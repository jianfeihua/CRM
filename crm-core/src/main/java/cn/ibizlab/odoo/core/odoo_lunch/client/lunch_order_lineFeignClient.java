package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_order_line] 服务对象接口
 */
@FeignClient(value = "odoo-lunch", contextId = "lunch-order-line", fallback = lunch_order_lineFallback.class)
public interface lunch_order_lineFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/lunch_order_lines/{id}")
    Lunch_order_line get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_lines/{id}")
    Lunch_order_line update(@PathVariable("id") Integer id,@RequestBody Lunch_order_line lunch_order_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_lines/batch")
    Boolean updateBatch(@RequestBody List<Lunch_order_line> lunch_order_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines/searchdefault")
    Page<Lunch_order_line> searchDefault(@RequestBody Lunch_order_lineSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines")
    Lunch_order_line create(@RequestBody Lunch_order_line lunch_order_line);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines/batch")
    Boolean createBatch(@RequestBody List<Lunch_order_line> lunch_order_lines);



    @RequestMapping(method = RequestMethod.GET, value = "/lunch_order_lines/select")
    Page<Lunch_order_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_order_lines/getdraft")
    Lunch_order_line getDraft();


}
