package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_track_lineFeignClient;

/**
 * 实体[库存追溯行] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_track_lineServiceImpl implements IStock_track_lineService {

    @Autowired
    stock_track_lineFeignClient stock_track_lineFeignClient;


    @Override
    public Stock_track_line get(Integer id) {
		Stock_track_line et=stock_track_lineFeignClient.get(id);
        if(et==null){
            et=new Stock_track_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Stock_track_line et) {
        Stock_track_line rt = stock_track_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_track_line> list){
        stock_track_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_track_line getDraft(Stock_track_line et) {
        et=stock_track_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_track_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_track_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_track_line et) {
        Stock_track_line rt = stock_track_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_track_line> list){
        stock_track_lineFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_track_line> searchDefault(Stock_track_lineSearchContext context) {
        Page<Stock_track_line> stock_track_lines=stock_track_lineFeignClient.searchDefault(context);
        return stock_track_lines;
    }


}


