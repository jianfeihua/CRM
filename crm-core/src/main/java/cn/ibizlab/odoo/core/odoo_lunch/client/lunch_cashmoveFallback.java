package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_cashmove] 服务对象接口
 */
@Component
public class lunch_cashmoveFallback implements lunch_cashmoveFeignClient{


    public Page<Lunch_cashmove> searchDefault(Lunch_cashmoveSearchContext context){
            return null;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Lunch_cashmove create(Lunch_cashmove lunch_cashmove){
            return null;
     }
    public Boolean createBatch(List<Lunch_cashmove> lunch_cashmoves){
            return false;
     }

    public Lunch_cashmove update(Integer id, Lunch_cashmove lunch_cashmove){
            return null;
     }
    public Boolean updateBatch(List<Lunch_cashmove> lunch_cashmoves){
            return false;
     }


    public Lunch_cashmove get(Integer id){
            return null;
     }


    public Page<Lunch_cashmove> select(){
            return null;
     }

    public Lunch_cashmove getDraft(){
            return null;
    }



}
