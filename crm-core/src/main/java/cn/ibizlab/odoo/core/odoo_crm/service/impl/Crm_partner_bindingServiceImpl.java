package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_partner_bindingSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_partner_bindingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_partner_bindingFeignClient;

/**
 * 实体[在CRM向导中处理业务伙伴的绑定或生成。] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_partner_bindingServiceImpl implements ICrm_partner_bindingService {

    @Autowired
    crm_partner_bindingFeignClient crm_partner_bindingFeignClient;


    @Override
    public Crm_partner_binding getDraft(Crm_partner_binding et) {
        et=crm_partner_bindingFeignClient.getDraft();
        return et;
    }

    @Override
    public Crm_partner_binding get(Integer id) {
		Crm_partner_binding et=crm_partner_bindingFeignClient.get(id);
        if(et==null){
            et=new Crm_partner_binding();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Crm_partner_binding et) {
        Crm_partner_binding rt = crm_partner_bindingFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_partner_binding> list){
        crm_partner_bindingFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Crm_partner_binding et) {
        Crm_partner_binding rt = crm_partner_bindingFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_partner_binding> list){
        crm_partner_bindingFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_partner_bindingFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_partner_bindingFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_partner_binding> searchDefault(Crm_partner_bindingSearchContext context) {
        Page<Crm_partner_binding> crm_partner_bindings=crm_partner_bindingFeignClient.searchDefault(context);
        return crm_partner_bindings;
    }


}


