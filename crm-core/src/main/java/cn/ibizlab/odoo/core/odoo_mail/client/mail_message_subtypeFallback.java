package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_message_subtype] 服务对象接口
 */
@Component
public class mail_message_subtypeFallback implements mail_message_subtypeFeignClient{


    public Mail_message_subtype get(Integer id){
            return null;
     }


    public Mail_message_subtype update(Integer id, Mail_message_subtype mail_message_subtype){
            return null;
     }
    public Boolean updateBatch(List<Mail_message_subtype> mail_message_subtypes){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Mail_message_subtype create(Mail_message_subtype mail_message_subtype){
            return null;
     }
    public Boolean createBatch(List<Mail_message_subtype> mail_message_subtypes){
            return false;
     }


    public Page<Mail_message_subtype> searchDefault(Mail_message_subtypeSearchContext context){
            return null;
     }


    public Page<Mail_message_subtype> select(){
            return null;
     }

    public Mail_message_subtype getDraft(){
            return null;
    }



}
