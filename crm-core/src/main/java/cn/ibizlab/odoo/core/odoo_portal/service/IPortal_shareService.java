package cn.ibizlab.odoo.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;


/**
 * 实体[Portal_share] 服务对象接口
 */
public interface IPortal_shareService{

    boolean update(Portal_share et) ;
    void updateBatch(List<Portal_share> list) ;
    Portal_share get(Integer key) ;
    boolean create(Portal_share et) ;
    void createBatch(List<Portal_share> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Portal_share getDraft(Portal_share et) ;
    Page<Portal_share> searchDefault(Portal_shareSearchContext context) ;

}



