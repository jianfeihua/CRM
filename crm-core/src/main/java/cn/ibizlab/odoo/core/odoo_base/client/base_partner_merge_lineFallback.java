package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_partner_merge_line] 服务对象接口
 */
@Component
public class base_partner_merge_lineFallback implements base_partner_merge_lineFeignClient{

    public Base_partner_merge_line create(Base_partner_merge_line base_partner_merge_line){
            return null;
     }
    public Boolean createBatch(List<Base_partner_merge_line> base_partner_merge_lines){
            return false;
     }

    public Base_partner_merge_line update(Integer id, Base_partner_merge_line base_partner_merge_line){
            return null;
     }
    public Boolean updateBatch(List<Base_partner_merge_line> base_partner_merge_lines){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Base_partner_merge_line> searchDefault(Base_partner_merge_lineSearchContext context){
            return null;
     }


    public Base_partner_merge_line get(Integer id){
            return null;
     }


    public Page<Base_partner_merge_line> select(){
            return null;
     }

    public Base_partner_merge_line getDraft(){
            return null;
    }



}
