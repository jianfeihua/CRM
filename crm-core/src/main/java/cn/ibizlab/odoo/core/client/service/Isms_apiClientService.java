package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isms_api;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sms_api] 服务对象接口
 */
public interface Isms_apiClientService{

    public Isms_api createModel() ;

    public void create(Isms_api sms_api);

    public void updateBatch(List<Isms_api> sms_apis);

    public void removeBatch(List<Isms_api> sms_apis);

    public Page<Isms_api> fetchDefault(SearchContext context);

    public void remove(Isms_api sms_api);

    public void get(Isms_api sms_api);

    public void update(Isms_api sms_api);

    public void createBatch(List<Isms_api> sms_apis);

    public Page<Isms_api> select(SearchContext context);

    public void getDraft(Isms_api sms_api);

}
