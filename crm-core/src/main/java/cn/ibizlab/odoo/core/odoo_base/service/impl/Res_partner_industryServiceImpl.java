package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_industrySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_industryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_partner_industryFeignClient;

/**
 * 实体[工业] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_industryServiceImpl implements IRes_partner_industryService {

    @Autowired
    res_partner_industryFeignClient res_partner_industryFeignClient;


    @Override
    public boolean update(Res_partner_industry et) {
        Res_partner_industry rt = res_partner_industryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_partner_industry> list){
        res_partner_industryFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Res_partner_industry et) {
        Res_partner_industry rt = res_partner_industryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_industry> list){
        res_partner_industryFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_partner_industryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_partner_industryFeignClient.removeBatch(idList);
    }

    @Override
    public Res_partner_industry get(Integer id) {
		Res_partner_industry et=res_partner_industryFeignClient.get(id);
        if(et==null){
            et=new Res_partner_industry();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Res_partner_industry getDraft(Res_partner_industry et) {
        et=res_partner_industryFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_industry> searchDefault(Res_partner_industrySearchContext context) {
        Page<Res_partner_industry> res_partner_industrys=res_partner_industryFeignClient.searchDefault(context);
        return res_partner_industrys;
    }


}


