package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_sourceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_utm.client.utm_sourceFeignClient;

/**
 * 实体[UTM来源] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_sourceServiceImpl implements IUtm_sourceService {

    @Autowired
    utm_sourceFeignClient utm_sourceFeignClient;


    @Override
    public Utm_source get(Integer id) {
		Utm_source et=utm_sourceFeignClient.get(id);
        if(et==null){
            et=new Utm_source();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Utm_source et) {
        Utm_source rt = utm_sourceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_source> list){
        utm_sourceFeignClient.createBatch(list) ;
    }

    @Override
    public Utm_source getDraft(Utm_source et) {
        et=utm_sourceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=utm_sourceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        utm_sourceFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Utm_source et) {
        Utm_source rt = utm_sourceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Utm_source> list){
        utm_sourceFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_source> searchDefault(Utm_sourceSearchContext context) {
        Page<Utm_source> utm_sources=utm_sourceFeignClient.searchDefault(context);
        return utm_sources;
    }


}


