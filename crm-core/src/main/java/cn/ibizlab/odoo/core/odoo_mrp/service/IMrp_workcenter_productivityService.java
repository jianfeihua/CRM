package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;


/**
 * 实体[Mrp_workcenter_productivity] 服务对象接口
 */
public interface IMrp_workcenter_productivityService{

    boolean create(Mrp_workcenter_productivity et) ;
    void createBatch(List<Mrp_workcenter_productivity> list) ;
    boolean update(Mrp_workcenter_productivity et) ;
    void updateBatch(List<Mrp_workcenter_productivity> list) ;
    Mrp_workcenter_productivity get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_workcenter_productivity getDraft(Mrp_workcenter_productivity et) ;
    Page<Mrp_workcenter_productivity> searchDefault(Mrp_workcenter_productivitySearchContext context) ;

}



