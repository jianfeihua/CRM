package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_groupsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_groupsFeignClient;

/**
 * 实体[访问群] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_groupsServiceImpl implements IRes_groupsService {

    @Autowired
    res_groupsFeignClient res_groupsFeignClient;


    @Override
    public Res_groups get(Integer id) {
		Res_groups et=res_groupsFeignClient.get(id);
        if(et==null){
            et=new Res_groups();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Res_groups et) {
        Res_groups rt = res_groupsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_groups> list){
        res_groupsFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_groupsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_groupsFeignClient.removeBatch(idList);
    }

    @Override
    public Res_groups getDraft(Res_groups et) {
        et=res_groupsFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Res_groups et) {
        Res_groups rt = res_groupsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_groups> list){
        res_groupsFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_groups> searchDefault(Res_groupsSearchContext context) {
        Page<Res_groups> res_groupss=res_groupsFeignClient.searchDefault(context);
        return res_groupss;
    }


}


