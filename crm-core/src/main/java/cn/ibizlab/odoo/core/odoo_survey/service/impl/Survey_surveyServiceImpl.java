package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_surveyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_surveyFeignClient;

/**
 * 实体[问卷] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_surveyServiceImpl implements ISurvey_surveyService {

    @Autowired
    survey_surveyFeignClient survey_surveyFeignClient;


    @Override
    public Survey_survey get(Integer id) {
		Survey_survey et=survey_surveyFeignClient.get(id);
        if(et==null){
            et=new Survey_survey();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Survey_survey et) {
        Survey_survey rt = survey_surveyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_survey> list){
        survey_surveyFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Survey_survey et) {
        Survey_survey rt = survey_surveyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_survey> list){
        survey_surveyFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_surveyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_surveyFeignClient.removeBatch(idList);
    }

    @Override
    public Survey_survey getDraft(Survey_survey et) {
        et=survey_surveyFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_survey> searchDefault(Survey_surveySearchContext context) {
        Page<Survey_survey> survey_surveys=survey_surveyFeignClient.searchDefault(context);
        return survey_surveys;
    }


}


