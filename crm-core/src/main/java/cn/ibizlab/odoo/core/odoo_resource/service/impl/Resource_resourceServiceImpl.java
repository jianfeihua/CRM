package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_resourceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_resource.client.resource_resourceFeignClient;

/**
 * 实体[资源] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_resourceServiceImpl implements IResource_resourceService {

    @Autowired
    resource_resourceFeignClient resource_resourceFeignClient;


    @Override
    public boolean update(Resource_resource et) {
        Resource_resource rt = resource_resourceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Resource_resource> list){
        resource_resourceFeignClient.updateBatch(list) ;
    }

    @Override
    public Resource_resource get(Integer id) {
		Resource_resource et=resource_resourceFeignClient.get(id);
        if(et==null){
            et=new Resource_resource();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Resource_resource et) {
        Resource_resource rt = resource_resourceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_resource> list){
        resource_resourceFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=resource_resourceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        resource_resourceFeignClient.removeBatch(idList);
    }

    @Override
    public Resource_resource getDraft(Resource_resource et) {
        et=resource_resourceFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_resource> searchDefault(Resource_resourceSearchContext context) {
        Page<Resource_resource> resource_resources=resource_resourceFeignClient.searchDefault(context);
        return resource_resources;
    }


}


