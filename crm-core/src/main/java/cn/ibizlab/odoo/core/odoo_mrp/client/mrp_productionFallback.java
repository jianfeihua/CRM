package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_productionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_production] 服务对象接口
 */
@Component
public class mrp_productionFallback implements mrp_productionFeignClient{



    public Mrp_production create(Mrp_production mrp_production){
            return null;
     }
    public Boolean createBatch(List<Mrp_production> mrp_productions){
            return false;
     }

    public Page<Mrp_production> searchDefault(Mrp_productionSearchContext context){
            return null;
     }



    public Mrp_production get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mrp_production update(Integer id, Mrp_production mrp_production){
            return null;
     }
    public Boolean updateBatch(List<Mrp_production> mrp_productions){
            return false;
     }


    public Page<Mrp_production> select(){
            return null;
     }

    public Mrp_production getDraft(){
            return null;
    }



}
