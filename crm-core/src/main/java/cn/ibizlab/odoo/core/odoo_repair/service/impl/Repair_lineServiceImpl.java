package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_repair.client.repair_lineFeignClient;

/**
 * 实体[修理明细行(零件)] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_lineServiceImpl implements IRepair_lineService {

    @Autowired
    repair_lineFeignClient repair_lineFeignClient;


    @Override
    public Repair_line get(Integer id) {
		Repair_line et=repair_lineFeignClient.get(id);
        if(et==null){
            et=new Repair_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Repair_line getDraft(Repair_line et) {
        et=repair_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Repair_line et) {
        Repair_line rt = repair_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Repair_line> list){
        repair_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=repair_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        repair_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Repair_line et) {
        Repair_line rt = repair_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_line> list){
        repair_lineFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_line> searchDefault(Repair_lineSearchContext context) {
        Page<Repair_line> repair_lines=repair_lineFeignClient.searchDefault(context);
        return repair_lines;
    }


}


