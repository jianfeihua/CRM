package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currency_rateSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currency_rateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_currency_rateFeignClient;

/**
 * 实体[汇率] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_currency_rateServiceImpl implements IRes_currency_rateService {

    @Autowired
    res_currency_rateFeignClient res_currency_rateFeignClient;


    @Override
    public boolean create(Res_currency_rate et) {
        Res_currency_rate rt = res_currency_rateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_currency_rate> list){
        res_currency_rateFeignClient.createBatch(list) ;
    }

    @Override
    public Res_currency_rate get(Integer id) {
		Res_currency_rate et=res_currency_rateFeignClient.get(id);
        if(et==null){
            et=new Res_currency_rate();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Res_currency_rate getDraft(Res_currency_rate et) {
        et=res_currency_rateFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_currency_rateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_currency_rateFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Res_currency_rate et) {
        Res_currency_rate rt = res_currency_rateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_currency_rate> list){
        res_currency_rateFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_currency_rate> searchDefault(Res_currency_rateSearchContext context) {
        Page<Res_currency_rate> res_currency_rates=res_currency_rateFeignClient.searchDefault(context);
        return res_currency_rates;
    }


}


