package cn.ibizlab.odoo.core.odoo_payment.valuerule.anno.payment_icon;

import cn.ibizlab.odoo.core.odoo_payment.valuerule.validator.payment_icon.Payment_iconImage_payment_formDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Payment_icon
 * 属性：Image_payment_form
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Payment_iconImage_payment_formDefaultValidator.class})
public @interface Payment_iconImage_payment_formDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
