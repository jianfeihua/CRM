package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_valueSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_template_attribute_value] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-template-attribute-value", fallback = product_template_attribute_valueFallback.class)
public interface product_template_attribute_valueFeignClient {



    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_values/{id}")
    Product_template_attribute_value get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_values")
    Product_template_attribute_value create(@RequestBody Product_template_attribute_value product_template_attribute_value);

    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_values/batch")
    Boolean createBatch(@RequestBody List<Product_template_attribute_value> product_template_attribute_values);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_values/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_values/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_values/searchdefault")
    Page<Product_template_attribute_value> searchDefault(@RequestBody Product_template_attribute_valueSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_values/{id}")
    Product_template_attribute_value update(@PathVariable("id") Integer id,@RequestBody Product_template_attribute_value product_template_attribute_value);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_values/batch")
    Boolean updateBatch(@RequestBody List<Product_template_attribute_value> product_template_attribute_values);


    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_values/select")
    Page<Product_template_attribute_value> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_values/getdraft")
    Product_template_attribute_value getDraft();


}
