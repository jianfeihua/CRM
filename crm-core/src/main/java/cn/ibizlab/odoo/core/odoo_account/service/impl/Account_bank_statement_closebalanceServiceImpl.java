package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_closebalanceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_closebalanceFeignClient;

/**
 * 实体[银行对账单期末余额] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_closebalanceServiceImpl implements IAccount_bank_statement_closebalanceService {

    @Autowired
    account_bank_statement_closebalanceFeignClient account_bank_statement_closebalanceFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_bank_statement_closebalanceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_bank_statement_closebalanceFeignClient.removeBatch(idList);
    }

    @Override
    public Account_bank_statement_closebalance getDraft(Account_bank_statement_closebalance et) {
        et=account_bank_statement_closebalanceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_bank_statement_closebalance et) {
        Account_bank_statement_closebalance rt = account_bank_statement_closebalanceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_closebalance> list){
        account_bank_statement_closebalanceFeignClient.createBatch(list) ;
    }

    @Override
    public Account_bank_statement_closebalance get(Integer id) {
		Account_bank_statement_closebalance et=account_bank_statement_closebalanceFeignClient.get(id);
        if(et==null){
            et=new Account_bank_statement_closebalance();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Account_bank_statement_closebalance et) {
        Account_bank_statement_closebalance rt = account_bank_statement_closebalanceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_bank_statement_closebalance> list){
        account_bank_statement_closebalanceFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_closebalance> searchDefault(Account_bank_statement_closebalanceSearchContext context) {
        Page<Account_bank_statement_closebalance> account_bank_statement_closebalances=account_bank_statement_closebalanceFeignClient.searchDefault(context);
        return account_bank_statement_closebalances;
    }


}


