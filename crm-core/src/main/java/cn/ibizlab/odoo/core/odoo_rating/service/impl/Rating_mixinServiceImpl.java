package cn.ibizlab.odoo.core.odoo_rating.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_rating.client.rating_mixinFeignClient;

/**
 * 实体[混合评级] 服务对象接口实现
 */
@Slf4j
@Service
public class Rating_mixinServiceImpl implements IRating_mixinService {

    @Autowired
    rating_mixinFeignClient rating_mixinFeignClient;


    @Override
    public Rating_mixin getDraft(Rating_mixin et) {
        et=rating_mixinFeignClient.getDraft();
        return et;
    }

    @Override
    public Rating_mixin get(Integer id) {
		Rating_mixin et=rating_mixinFeignClient.get(id);
        if(et==null){
            et=new Rating_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Rating_mixin et) {
        Rating_mixin rt = rating_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Rating_mixin> list){
        rating_mixinFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Rating_mixin et) {
        Rating_mixin rt = rating_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Rating_mixin> list){
        rating_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=rating_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        rating_mixinFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Rating_mixin> searchDefault(Rating_mixinSearchContext context) {
        Page<Rating_mixin> rating_mixins=rating_mixinFeignClient.searchDefault(context);
        return rating_mixins;
    }


}


