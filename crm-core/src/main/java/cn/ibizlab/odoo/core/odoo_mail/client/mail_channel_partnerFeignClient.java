package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_channel_partner] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-channel-partner", fallback = mail_channel_partnerFallback.class)
public interface mail_channel_partnerFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners")
    Mail_channel_partner create(@RequestBody Mail_channel_partner mail_channel_partner);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners/batch")
    Boolean createBatch(@RequestBody List<Mail_channel_partner> mail_channel_partners);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_channel_partners/{id}")
    Mail_channel_partner update(@PathVariable("id") Integer id,@RequestBody Mail_channel_partner mail_channel_partner);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_channel_partners/batch")
    Boolean updateBatch(@RequestBody List<Mail_channel_partner> mail_channel_partners);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners/searchdefault")
    Page<Mail_channel_partner> searchDefault(@RequestBody Mail_channel_partnerSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_channel_partners/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_channel_partners/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_channel_partners/{id}")
    Mail_channel_partner get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_channel_partners/select")
    Page<Mail_channel_partner> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_channel_partners/getdraft")
    Mail_channel_partner getDraft();


}
