package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [bus_presence] 对象
 */
public interface Ibus_presence {

    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最后在线]
     */
    public void setLast_poll(Timestamp last_poll);
    
    /**
     * 设置 [最后在线]
     */
    public Timestamp getLast_poll();

    /**
     * 获取 [最后在线]脏标记
     */
    public boolean getLast_pollDirtyFlag();
    /**
     * 获取 [最后登录]
     */
    public void setLast_presence(Timestamp last_presence);
    
    /**
     * 设置 [最后登录]
     */
    public Timestamp getLast_presence();

    /**
     * 获取 [最后登录]脏标记
     */
    public boolean getLast_presenceDirtyFlag();
    /**
     * 获取 [IM的状态]
     */
    public void setStatus(String status);
    
    /**
     * 设置 [IM的状态]
     */
    public String getStatus();

    /**
     * 获取 [IM的状态]脏标记
     */
    public boolean getStatusDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [用户]
     */
    public Integer getUser_id();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [用户]
     */
    public String getUser_id_text();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后修改时间]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改时间]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改时间]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
