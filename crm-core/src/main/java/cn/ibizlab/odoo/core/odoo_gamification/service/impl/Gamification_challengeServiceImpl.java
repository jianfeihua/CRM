package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_challengeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_challengeFeignClient;

/**
 * 实体[游戏化挑战] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_challengeServiceImpl implements IGamification_challengeService {

    @Autowired
    gamification_challengeFeignClient gamification_challengeFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_challengeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_challengeFeignClient.removeBatch(idList);
    }

    @Override
    public Gamification_challenge get(Integer id) {
		Gamification_challenge et=gamification_challengeFeignClient.get(id);
        if(et==null){
            et=new Gamification_challenge();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Gamification_challenge et) {
        Gamification_challenge rt = gamification_challengeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_challenge> list){
        gamification_challengeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Gamification_challenge et) {
        Gamification_challenge rt = gamification_challengeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_challenge> list){
        gamification_challengeFeignClient.createBatch(list) ;
    }

    @Override
    public Gamification_challenge getDraft(Gamification_challenge et) {
        et=gamification_challengeFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_challenge> searchDefault(Gamification_challengeSearchContext context) {
        Page<Gamification_challenge> gamification_challenges=gamification_challengeFeignClient.searchDefault(context);
        return gamification_challenges;
    }


}


