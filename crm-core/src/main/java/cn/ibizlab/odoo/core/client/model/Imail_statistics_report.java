package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_statistics_report] 对象
 */
public interface Imail_statistics_report {

    /**
     * 获取 [被退回]
     */
    public void setBounced(Integer bounced);
    
    /**
     * 设置 [被退回]
     */
    public Integer getBounced();

    /**
     * 获取 [被退回]脏标记
     */
    public boolean getBouncedDirtyFlag();
    /**
     * 获取 [群发邮件营销]
     */
    public void setCampaign(String campaign);
    
    /**
     * 设置 [群发邮件营销]
     */
    public String getCampaign();

    /**
     * 获取 [群发邮件营销]脏标记
     */
    public boolean getCampaignDirtyFlag();
    /**
     * 获取 [点击率]
     */
    public void setClicked(Integer clicked);
    
    /**
     * 设置 [点击率]
     */
    public Integer getClicked();

    /**
     * 获取 [点击率]脏标记
     */
    public boolean getClickedDirtyFlag();
    /**
     * 获取 [已送货]
     */
    public void setDelivered(Integer delivered);
    
    /**
     * 设置 [已送货]
     */
    public Integer getDelivered();

    /**
     * 获取 [已送货]脏标记
     */
    public boolean getDeliveredDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [从]
     */
    public void setEmail_from(String email_from);
    
    /**
     * 设置 [从]
     */
    public String getEmail_from();

    /**
     * 获取 [从]脏标记
     */
    public boolean getEmail_fromDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [群发邮件]
     */
    public void setName(String name);
    
    /**
     * 设置 [群发邮件]
     */
    public String getName();

    /**
     * 获取 [群发邮件]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [已开启]
     */
    public void setOpened(Integer opened);
    
    /**
     * 设置 [已开启]
     */
    public Integer getOpened();

    /**
     * 获取 [已开启]脏标记
     */
    public boolean getOpenedDirtyFlag();
    /**
     * 获取 [已回复]
     */
    public void setReplied(Integer replied);
    
    /**
     * 设置 [已回复]
     */
    public Integer getReplied();

    /**
     * 获取 [已回复]脏标记
     */
    public boolean getRepliedDirtyFlag();
    /**
     * 获取 [计划日期]
     */
    public void setScheduled_date(Timestamp scheduled_date);
    
    /**
     * 设置 [计划日期]
     */
    public Timestamp getScheduled_date();

    /**
     * 获取 [计划日期]脏标记
     */
    public boolean getScheduled_dateDirtyFlag();
    /**
     * 获取 [已汇]
     */
    public void setSent(Integer sent);
    
    /**
     * 设置 [已汇]
     */
    public Integer getSent();

    /**
     * 获取 [已汇]脏标记
     */
    public boolean getSentDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
