package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [maintenance_equipment_category] 对象
 */
public interface Imaintenance_equipment_category {

    /**
     * 获取 [别名联系人安全]
     */
    public void setAlias_contact(String alias_contact);
    
    /**
     * 设置 [别名联系人安全]
     */
    public String getAlias_contact();

    /**
     * 获取 [别名联系人安全]脏标记
     */
    public boolean getAlias_contactDirtyFlag();
    /**
     * 获取 [默认值]
     */
    public void setAlias_defaults(String alias_defaults);
    
    /**
     * 设置 [默认值]
     */
    public String getAlias_defaults();

    /**
     * 获取 [默认值]脏标记
     */
    public boolean getAlias_defaultsDirtyFlag();
    /**
     * 获取 [别名网域]
     */
    public void setAlias_domain(String alias_domain);
    
    /**
     * 设置 [别名网域]
     */
    public String getAlias_domain();

    /**
     * 获取 [别名网域]脏标记
     */
    public boolean getAlias_domainDirtyFlag();
    /**
     * 获取 [记录线索ID]
     */
    public void setAlias_force_thread_id(Integer alias_force_thread_id);
    
    /**
     * 设置 [记录线索ID]
     */
    public Integer getAlias_force_thread_id();

    /**
     * 获取 [记录线索ID]脏标记
     */
    public boolean getAlias_force_thread_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_id(Integer alias_id);
    
    /**
     * 设置 [别名]
     */
    public Integer getAlias_id();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_idDirtyFlag();
    /**
     * 获取 [别名模型]
     */
    public void setAlias_model_id(Integer alias_model_id);
    
    /**
     * 设置 [别名模型]
     */
    public Integer getAlias_model_id();

    /**
     * 获取 [别名模型]脏标记
     */
    public boolean getAlias_model_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_name(String alias_name);
    
    /**
     * 设置 [别名]
     */
    public String getAlias_name();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_nameDirtyFlag();
    /**
     * 获取 [上级模型]
     */
    public void setAlias_parent_model_id(Integer alias_parent_model_id);
    
    /**
     * 设置 [上级模型]
     */
    public Integer getAlias_parent_model_id();

    /**
     * 获取 [上级模型]脏标记
     */
    public boolean getAlias_parent_model_idDirtyFlag();
    /**
     * 获取 [上级记录线程ID]
     */
    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);
    
    /**
     * 设置 [上级记录线程ID]
     */
    public Integer getAlias_parent_thread_id();

    /**
     * 获取 [上级记录线程ID]脏标记
     */
    public boolean getAlias_parent_thread_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setAlias_user_id(Integer alias_user_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getAlias_user_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getAlias_user_idDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [设备]
     */
    public void setEquipment_count(Integer equipment_count);
    
    /**
     * 设置 [设备]
     */
    public Integer getEquipment_count();

    /**
     * 获取 [设备]脏标记
     */
    public boolean getEquipment_countDirtyFlag();
    /**
     * 获取 [设备]
     */
    public void setEquipment_ids(String equipment_ids);
    
    /**
     * 设置 [设备]
     */
    public String getEquipment_ids();

    /**
     * 获取 [设备]脏标记
     */
    public boolean getEquipment_idsDirtyFlag();
    /**
     * 获取 [在保养管道中折叠]
     */
    public void setFold(String fold);
    
    /**
     * 设置 [在保养管道中折叠]
     */
    public String getFold();

    /**
     * 获取 [在保养管道中折叠]脏标记
     */
    public boolean getFoldDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [维修统计]
     */
    public void setMaintenance_count(Integer maintenance_count);
    
    /**
     * 设置 [维修统计]
     */
    public Integer getMaintenance_count();

    /**
     * 获取 [维修统计]脏标记
     */
    public boolean getMaintenance_countDirtyFlag();
    /**
     * 获取 [保养]
     */
    public void setMaintenance_ids(String maintenance_ids);
    
    /**
     * 设置 [保养]
     */
    public String getMaintenance_ids();

    /**
     * 获取 [保养]脏标记
     */
    public boolean getMaintenance_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [类别名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [类别名称]
     */
    public String getName();

    /**
     * 获取 [类别名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [注释]
     */
    public void setNote(String note);
    
    /**
     * 设置 [注释]
     */
    public String getNote();

    /**
     * 获取 [注释]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setTechnician_user_id(Integer technician_user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getTechnician_user_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getTechnician_user_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setTechnician_user_id_text(String technician_user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getTechnician_user_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getTechnician_user_id_textDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
