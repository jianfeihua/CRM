package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [银行对账单明细] 对象
 */
@Data
public class Account_bank_statement_line extends EntityClient implements Serializable {

    /**
     * 合作伙伴名称
     */
    @DEField(name = "partner_name")
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 银行账户号码
     */
    @DEField(name = "account_number")
    @JSONField(name = "account_number")
    @JsonProperty("account_number")
    private String accountNumber;

    /**
     * 参考
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 备注
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 日记账项目
     */
    @JSONField(name = "journal_entry_ids")
    @JsonProperty("journal_entry_ids")
    private String journalEntryIds;

    /**
     * 标签
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 日记账分录名称
     */
    @DEField(name = "move_name")
    @JSONField(name = "move_name")
    @JsonProperty("move_name")
    private String moveName;

    /**
     * 导入ID
     */
    @DEField(name = "unique_import_id")
    @JSONField(name = "unique_import_id")
    @JsonProperty("unique_import_id")
    private String uniqueImportId;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * POS报表
     */
    @DEField(name = "pos_statement_id")
    @JSONField(name = "pos_statement_id")
    @JsonProperty("pos_statement_id")
    private Integer posStatementId;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 货币金额
     */
    @DEField(name = "amount_currency")
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private Double amountCurrency;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 现金分类账
     */
    @JSONField(name = "journal_currency_id")
    @JsonProperty("journal_currency_id")
    private Integer journalCurrencyId;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 报告
     */
    @JSONField(name = "statement_id_text")
    @JsonProperty("statement_id_text")
    private String statementIdText;

    /**
     * 对方科目
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 对方科目
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 银行账户
     */
    @DEField(name = "bank_account_id")
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Integer bankAccountId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 报告
     */
    @DEField(name = "statement_id")
    @JSONField(name = "statement_id")
    @JsonProperty("statement_id")
    private Integer statementId;


    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JSONField(name = "odoostatement")
    @JsonProperty("odoostatement")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement odooStatement;

    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoobankaccount")
    @JsonProperty("odoobankaccount")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooBankAccount;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [合作伙伴名称]
     */
    public void setPartnerName(String partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [银行账户号码]
     */
    public void setAccountNumber(String accountNumber){
        this.accountNumber = accountNumber ;
        this.modify("account_number",accountNumber);
    }
    /**
     * 设置 [参考]
     */
    public void setRef(String ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }
    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [标签]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [日记账分录名称]
     */
    public void setMoveName(String moveName){
        this.moveName = moveName ;
        this.modify("move_name",moveName);
    }
    /**
     * 设置 [导入ID]
     */
    public void setUniqueImportId(String uniqueImportId){
        this.uniqueImportId = uniqueImportId ;
        this.modify("unique_import_id",uniqueImportId);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [POS报表]
     */
    public void setPosStatementId(Integer posStatementId){
        this.posStatementId = posStatementId ;
        this.modify("pos_statement_id",posStatementId);
    }
    /**
     * 设置 [货币金额]
     */
    public void setAmountCurrency(Double amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }
    /**
     * 设置 [对方科目]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [银行账户]
     */
    public void setBankAccountId(Integer bankAccountId){
        this.bankAccountId = bankAccountId ;
        this.modify("bank_account_id",bankAccountId);
    }
    /**
     * 设置 [日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [报告]
     */
    public void setStatementId(Integer statementId){
        this.statementId = statementId ;
        this.modify("statement_id",statementId);
    }

}


