package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_lead2opportunity_partner_mass] 服务对象接口
 */
@Component
public class crm_lead2opportunity_partner_massFallback implements crm_lead2opportunity_partner_massFeignClient{


    public Page<Crm_lead2opportunity_partner_mass> searchDefault(Crm_lead2opportunity_partner_massSearchContext context){
            return null;
     }


    public Crm_lead2opportunity_partner_mass update(Integer id, Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
            return null;
     }
    public Boolean updateBatch(List<Crm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Crm_lead2opportunity_partner_mass get(Integer id){
            return null;
     }


    public Crm_lead2opportunity_partner_mass create(Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
            return null;
     }
    public Boolean createBatch(List<Crm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
            return false;
     }



    public Page<Crm_lead2opportunity_partner_mass> select(){
            return null;
     }

    public Crm_lead2opportunity_partner_mass getDraft(){
            return null;
    }



}
