package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_model] 服务对象接口
 */
public interface Ifleet_vehicle_modelClientService{

    public Ifleet_vehicle_model createModel() ;

    public void remove(Ifleet_vehicle_model fleet_vehicle_model);

    public void create(Ifleet_vehicle_model fleet_vehicle_model);

    public Page<Ifleet_vehicle_model> fetchDefault(SearchContext context);

    public void update(Ifleet_vehicle_model fleet_vehicle_model);

    public void get(Ifleet_vehicle_model fleet_vehicle_model);

    public void updateBatch(List<Ifleet_vehicle_model> fleet_vehicle_models);

    public void createBatch(List<Ifleet_vehicle_model> fleet_vehicle_models);

    public void removeBatch(List<Ifleet_vehicle_model> fleet_vehicle_models);

    public Page<Ifleet_vehicle_model> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_model fleet_vehicle_model);

}
