package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_analytic_line] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-analytic-line", fallback = account_analytic_lineFallback.class)
public interface account_analytic_lineFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_lines/{id}")
    Account_analytic_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines")
    Account_analytic_line create(@RequestBody Account_analytic_line account_analytic_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines/batch")
    Boolean createBatch(@RequestBody List<Account_analytic_line> account_analytic_lines);




    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_lines/{id}")
    Account_analytic_line update(@PathVariable("id") Integer id,@RequestBody Account_analytic_line account_analytic_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_lines/batch")
    Boolean updateBatch(@RequestBody List<Account_analytic_line> account_analytic_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines/searchdefault")
    Page<Account_analytic_line> searchDefault(@RequestBody Account_analytic_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_lines/select")
    Page<Account_analytic_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_lines/getdraft")
    Account_analytic_line getDraft();


}
