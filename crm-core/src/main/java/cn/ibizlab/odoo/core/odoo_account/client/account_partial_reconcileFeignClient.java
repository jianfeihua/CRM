package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_partial_reconcile] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-partial-reconcile", fallback = account_partial_reconcileFallback.class)
public interface account_partial_reconcileFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/account_partial_reconciles/{id}")
    Account_partial_reconcile get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_partial_reconciles/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_partial_reconciles/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_partial_reconciles/{id}")
    Account_partial_reconcile update(@PathVariable("id") Integer id,@RequestBody Account_partial_reconcile account_partial_reconcile);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_partial_reconciles/batch")
    Boolean updateBatch(@RequestBody List<Account_partial_reconcile> account_partial_reconciles);



    @RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles")
    Account_partial_reconcile create(@RequestBody Account_partial_reconcile account_partial_reconcile);

    @RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles/batch")
    Boolean createBatch(@RequestBody List<Account_partial_reconcile> account_partial_reconciles);



    @RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles/searchdefault")
    Page<Account_partial_reconcile> searchDefault(@RequestBody Account_partial_reconcileSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/account_partial_reconciles/select")
    Page<Account_partial_reconcile> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_partial_reconciles/getdraft")
    Account_partial_reconcile getDraft();


}
