package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_tag] 服务对象接口
 */
public interface Ifleet_vehicle_tagClientService{

    public Ifleet_vehicle_tag createModel() ;

    public void createBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags);

    public void removeBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags);

    public void remove(Ifleet_vehicle_tag fleet_vehicle_tag);

    public Page<Ifleet_vehicle_tag> fetchDefault(SearchContext context);

    public void create(Ifleet_vehicle_tag fleet_vehicle_tag);

    public void updateBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags);

    public void update(Ifleet_vehicle_tag fleet_vehicle_tag);

    public void get(Ifleet_vehicle_tag fleet_vehicle_tag);

    public Page<Ifleet_vehicle_tag> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_tag fleet_vehicle_tag);

}
