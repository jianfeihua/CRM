package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_groupSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_group] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-group", fallback = account_groupFallback.class)
public interface account_groupFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/account_groups/searchdefault")
    Page<Account_group> searchDefault(@RequestBody Account_groupSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_groups/{id}")
    Account_group update(@PathVariable("id") Integer id,@RequestBody Account_group account_group);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_groups/batch")
    Boolean updateBatch(@RequestBody List<Account_group> account_groups);


    @RequestMapping(method = RequestMethod.POST, value = "/account_groups")
    Account_group create(@RequestBody Account_group account_group);

    @RequestMapping(method = RequestMethod.POST, value = "/account_groups/batch")
    Boolean createBatch(@RequestBody List<Account_group> account_groups);



    @RequestMapping(method = RequestMethod.GET, value = "/account_groups/{id}")
    Account_group get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_groups/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_groups/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/account_groups/select")
    Page<Account_group> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_groups/getdraft")
    Account_group getDraft();


}
