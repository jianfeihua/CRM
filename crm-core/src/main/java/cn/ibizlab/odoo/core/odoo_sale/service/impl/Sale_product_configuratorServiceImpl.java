package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_product_configuratorSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_product_configuratorService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_product_configuratorFeignClient;

/**
 * 实体[销售产品配置器] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_product_configuratorServiceImpl implements ISale_product_configuratorService {

    @Autowired
    sale_product_configuratorFeignClient sale_product_configuratorFeignClient;


    @Override
    public Sale_product_configurator getDraft(Sale_product_configurator et) {
        et=sale_product_configuratorFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Sale_product_configurator et) {
        Sale_product_configurator rt = sale_product_configuratorFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_product_configurator> list){
        sale_product_configuratorFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=sale_product_configuratorFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_product_configuratorFeignClient.removeBatch(idList);
    }

    @Override
    public Sale_product_configurator get(Integer id) {
		Sale_product_configurator et=sale_product_configuratorFeignClient.get(id);
        if(et==null){
            et=new Sale_product_configurator();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Sale_product_configurator et) {
        Sale_product_configurator rt = sale_product_configuratorFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_product_configurator> list){
        sale_product_configuratorFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_product_configurator> searchDefault(Sale_product_configuratorSearchContext context) {
        Page<Sale_product_configurator> sale_product_configurators=sale_product_configuratorFeignClient.searchDefault(context);
        return sale_product_configurators;
    }


}


