package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icalendar_contacts;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_contacts] 服务对象接口
 */
public interface Icalendar_contactsClientService{

    public Icalendar_contacts createModel() ;

    public void update(Icalendar_contacts calendar_contacts);

    public void create(Icalendar_contacts calendar_contacts);

    public Page<Icalendar_contacts> fetchDefault(SearchContext context);

    public void remove(Icalendar_contacts calendar_contacts);

    public void get(Icalendar_contacts calendar_contacts);

    public void createBatch(List<Icalendar_contacts> calendar_contacts);

    public void updateBatch(List<Icalendar_contacts> calendar_contacts);

    public void removeBatch(List<Icalendar_contacts> calendar_contacts);

    public Page<Icalendar_contacts> select(SearchContext context);

    public void getDraft(Icalendar_contacts calendar_contacts);

}
