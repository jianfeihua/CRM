package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_custom_valueSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_attribute_custom_value] 服务对象接口
 */
@Component
public class product_attribute_custom_valueFallback implements product_attribute_custom_valueFeignClient{

    public Product_attribute_custom_value get(Integer id){
            return null;
     }


    public Page<Product_attribute_custom_value> searchDefault(Product_attribute_custom_valueSearchContext context){
            return null;
     }




    public Product_attribute_custom_value create(Product_attribute_custom_value product_attribute_custom_value){
            return null;
     }
    public Boolean createBatch(List<Product_attribute_custom_value> product_attribute_custom_values){
            return false;
     }


    public Product_attribute_custom_value update(Integer id, Product_attribute_custom_value product_attribute_custom_value){
            return null;
     }
    public Boolean updateBatch(List<Product_attribute_custom_value> product_attribute_custom_values){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Product_attribute_custom_value> select(){
            return null;
     }

    public Product_attribute_custom_value getDraft(){
            return null;
    }



}
