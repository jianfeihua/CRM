package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mail_statistics] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-mail-statistics", fallback = mail_mail_statisticsFallback.class)
public interface mail_mail_statisticsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/searchdefault")
    Page<Mail_mail_statistics> searchDefault(@RequestBody Mail_mail_statisticsSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics")
    Mail_mail_statistics create(@RequestBody Mail_mail_statistics mail_mail_statistics);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/batch")
    Boolean createBatch(@RequestBody List<Mail_mail_statistics> mail_mail_statistics);




    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/{id}")
    Mail_mail_statistics update(@PathVariable("id") Integer id,@RequestBody Mail_mail_statistics mail_mail_statistics);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/batch")
    Boolean updateBatch(@RequestBody List<Mail_mail_statistics> mail_mail_statistics);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/{id}")
    Mail_mail_statistics get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/select")
    Page<Mail_mail_statistics> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/getdraft")
    Mail_mail_statistics getDraft();


}
