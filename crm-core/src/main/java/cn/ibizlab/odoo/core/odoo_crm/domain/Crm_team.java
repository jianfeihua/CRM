package cn.ibizlab.odoo.core.odoo_crm.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [销售渠道] 对象
 */
@Data
public class Crm_team extends EntityClient implements Serializable {

    /**
     * 商机收入
     */
    @JSONField(name = "opportunities_amount")
    @JsonProperty("opportunities_amount")
    private Integer opportunitiesAmount;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 渠道
     */
    @DEField(name = "use_opportunities")
    @JSONField(name = "use_opportunities")
    @JsonProperty("use_opportunities")
    private String useOpportunities;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 发票报价单
     */
    @JSONField(name = "quotations_count")
    @JsonProperty("quotations_count")
    private Integer quotationsCount;

    /**
     * 类型
     */
    @JSONField(name = "dashboard_graph_type")
    @JsonProperty("dashboard_graph_type")
    private String dashboardGraphType;

    /**
     * 显示仪表
     */
    @JSONField(name = "is_favorite")
    @JsonProperty("is_favorite")
    private String isFavorite;

    /**
     * 开启的商机数
     */
    @JSONField(name = "opportunities_count")
    @JsonProperty("opportunities_count")
    private Integer opportunitiesCount;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 报价单
     */
    @DEField(name = "use_quotations")
    @JSONField(name = "use_quotations")
    @JsonProperty("use_quotations")
    private String useQuotations;

    /**
     * 回复 至
     */
    @DEField(name = "reply_to")
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 最喜欢的成员
     */
    @JSONField(name = "favorite_user_ids")
    @JsonProperty("favorite_user_ids")
    private String favoriteUserIds;

    /**
     * 团队类型
     */
    @DEField(name = "team_type")
    @JSONField(name = "team_type")
    @JsonProperty("team_type")
    private String teamType;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 内容
     */
    @DEField(name = "dashboard_graph_model")
    @JSONField(name = "dashboard_graph_model")
    @JsonProperty("dashboard_graph_model")
    private String dashboardGraphModel;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 预计关闭
     */
    @JSONField(name = "dashboard_graph_period_pipeline")
    @JsonProperty("dashboard_graph_period_pipeline")
    private String dashboardGraphPeriodPipeline;

    /**
     * 遗弃购物车数量
     */
    @JSONField(name = "abandoned_carts_count")
    @JsonProperty("abandoned_carts_count")
    private Integer abandonedCartsCount;

    /**
     * POS
     */
    @JSONField(name = "pos_config_ids")
    @JsonProperty("pos_config_ids")
    private String posConfigIds;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 线索
     */
    @DEField(name = "use_leads")
    @JSONField(name = "use_leads")
    @JsonProperty("use_leads")
    private String useLeads;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 网站
     */
    @JSONField(name = "website_ids")
    @JsonProperty("website_ids")
    private String websiteIds;

    /**
     * 发票金额
     */
    @JSONField(name = "quotations_amount")
    @JsonProperty("quotations_amount")
    private Integer quotationsAmount;

    /**
     * 本月已开发票
     */
    @JSONField(name = "invoiced")
    @JsonProperty("invoiced")
    private Integer invoiced;

    /**
     * 遗弃购物车数量
     */
    @JSONField(name = "abandoned_carts_amount")
    @JsonProperty("abandoned_carts_amount")
    private Integer abandonedCartsAmount;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 发票销售单
     */
    @JSONField(name = "sales_to_invoice_count")
    @JsonProperty("sales_to_invoice_count")
    private Integer salesToInvoiceCount;

    /**
     * 需要激活
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 未分派线索
     */
    @JSONField(name = "unassigned_leads_count")
    @JsonProperty("unassigned_leads_count")
    private Integer unassignedLeadsCount;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * POS分组
     */
    @DEField(name = "dashboard_graph_group_pos")
    @JSONField(name = "dashboard_graph_group_pos")
    @JsonProperty("dashboard_graph_group_pos")
    private String dashboardGraphGroupPos;

    /**
     * 渠道人员
     */
    @JSONField(name = "member_ids")
    @JsonProperty("member_ids")
    private String memberIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 发票目标
     */
    @DEField(name = "invoiced_target")
    @JSONField(name = "invoiced_target")
    @JsonProperty("invoiced_target")
    private Integer invoicedTarget;

    /**
     * 分组
     */
    @DEField(name = "dashboard_graph_group")
    @JSONField(name = "dashboard_graph_group")
    @JsonProperty("dashboard_graph_group")
    private String dashboardGraphGroup;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 数据仪表图
     */
    @JSONField(name = "dashboard_graph_data")
    @JsonProperty("dashboard_graph_data")
    private String dashboardGraphData;

    /**
     * 比例
     */
    @DEField(name = "dashboard_graph_period")
    @JSONField(name = "dashboard_graph_period")
    @JsonProperty("dashboard_graph_period")
    private String dashboardGraphPeriod;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 开放POS会议
     */
    @JSONField(name = "pos_sessions_open_count")
    @JsonProperty("pos_sessions_open_count")
    private Integer posSessionsOpenCount;

    /**
     * 设定开票目标
     */
    @DEField(name = "use_invoices")
    @JSONField(name = "use_invoices")
    @JsonProperty("use_invoices")
    private String useInvoices;

    /**
     * 仪表板按钮
     */
    @JSONField(name = "dashboard_button_name")
    @JsonProperty("dashboard_button_name")
    private String dashboardButtonName;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 分组方式
     */
    @DEField(name = "dashboard_graph_group_pipeline")
    @JSONField(name = "dashboard_graph_group_pipeline")
    @JsonProperty("dashboard_graph_group_pipeline")
    private String dashboardGraphGroupPipeline;

    /**
     * 会议销售金额
     */
    @JSONField(name = "pos_order_amount_total")
    @JsonProperty("pos_order_amount_total")
    private Double posOrderAmountTotal;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 销售团队
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 网域别名
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 模型别名
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;

    /**
     * 上级模型
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 安全联系人别名
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 所有者
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Integer aliasUserId;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 默认值
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;

    /**
     * 上级记录ID
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 记录线索ID
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 团队负责人
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 别名
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 最后更新
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 团队负责人
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;


    /**
     * 
     */
    @JSONField(name = "odooalias")
    @JsonProperty("odooalias")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [渠道]
     */
    public void setUseOpportunities(String useOpportunities){
        this.useOpportunities = useOpportunities ;
        this.modify("use_opportunities",useOpportunities);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [报价单]
     */
    public void setUseQuotations(String useQuotations){
        this.useQuotations = useQuotations ;
        this.modify("use_quotations",useQuotations);
    }
    /**
     * 设置 [回复 至]
     */
    public void setReplyTo(String replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }
    /**
     * 设置 [团队类型]
     */
    public void setTeamType(String teamType){
        this.teamType = teamType ;
        this.modify("team_type",teamType);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [内容]
     */
    public void setDashboardGraphModel(String dashboardGraphModel){
        this.dashboardGraphModel = dashboardGraphModel ;
        this.modify("dashboard_graph_model",dashboardGraphModel);
    }
    /**
     * 设置 [线索]
     */
    public void setUseLeads(String useLeads){
        this.useLeads = useLeads ;
        this.modify("use_leads",useLeads);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [POS分组]
     */
    public void setDashboardGraphGroupPos(String dashboardGraphGroupPos){
        this.dashboardGraphGroupPos = dashboardGraphGroupPos ;
        this.modify("dashboard_graph_group_pos",dashboardGraphGroupPos);
    }
    /**
     * 设置 [发票目标]
     */
    public void setInvoicedTarget(Integer invoicedTarget){
        this.invoicedTarget = invoicedTarget ;
        this.modify("invoiced_target",invoicedTarget);
    }
    /**
     * 设置 [分组]
     */
    public void setDashboardGraphGroup(String dashboardGraphGroup){
        this.dashboardGraphGroup = dashboardGraphGroup ;
        this.modify("dashboard_graph_group",dashboardGraphGroup);
    }
    /**
     * 设置 [比例]
     */
    public void setDashboardGraphPeriod(String dashboardGraphPeriod){
        this.dashboardGraphPeriod = dashboardGraphPeriod ;
        this.modify("dashboard_graph_period",dashboardGraphPeriod);
    }
    /**
     * 设置 [设定开票目标]
     */
    public void setUseInvoices(String useInvoices){
        this.useInvoices = useInvoices ;
        this.modify("use_invoices",useInvoices);
    }
    /**
     * 设置 [分组方式]
     */
    public void setDashboardGraphGroupPipeline(String dashboardGraphGroupPipeline){
        this.dashboardGraphGroupPipeline = dashboardGraphGroupPipeline ;
        this.modify("dashboard_graph_group_pipeline",dashboardGraphGroupPipeline);
    }
    /**
     * 设置 [销售团队]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [别名]
     */
    public void setAliasId(Integer aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [团队负责人]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

}


