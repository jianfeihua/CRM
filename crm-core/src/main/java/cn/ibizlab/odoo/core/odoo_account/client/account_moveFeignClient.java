package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_moveSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_move] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-move", fallback = account_moveFallback.class)
public interface account_moveFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_moves/searchdefault")
    Page<Account_move> searchDefault(@RequestBody Account_moveSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_moves/{id}")
    Account_move update(@PathVariable("id") Integer id,@RequestBody Account_move account_move);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_moves/batch")
    Boolean updateBatch(@RequestBody List<Account_move> account_moves);


    @RequestMapping(method = RequestMethod.GET, value = "/account_moves/{id}")
    Account_move get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_moves")
    Account_move create(@RequestBody Account_move account_move);

    @RequestMapping(method = RequestMethod.POST, value = "/account_moves/batch")
    Boolean createBatch(@RequestBody List<Account_move> account_moves);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_moves/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_moves/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/account_moves/select")
    Page<Account_move> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_moves/getdraft")
    Account_move getDraft();


}
