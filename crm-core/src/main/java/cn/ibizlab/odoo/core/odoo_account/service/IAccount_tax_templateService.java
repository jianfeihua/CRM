package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_templateSearchContext;


/**
 * 实体[Account_tax_template] 服务对象接口
 */
public interface IAccount_tax_templateService{

    boolean create(Account_tax_template et) ;
    void createBatch(List<Account_tax_template> list) ;
    boolean update(Account_tax_template et) ;
    void updateBatch(List<Account_tax_template> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_tax_template get(Integer key) ;
    Account_tax_template getDraft(Account_tax_template et) ;
    Page<Account_tax_template> searchDefault(Account_tax_templateSearchContext context) ;

}



