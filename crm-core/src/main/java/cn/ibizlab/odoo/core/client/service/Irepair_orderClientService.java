package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Irepair_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_order] 服务对象接口
 */
public interface Irepair_orderClientService{

    public Irepair_order createModel() ;

    public void createBatch(List<Irepair_order> repair_orders);

    public void update(Irepair_order repair_order);

    public void removeBatch(List<Irepair_order> repair_orders);

    public void updateBatch(List<Irepair_order> repair_orders);

    public void create(Irepair_order repair_order);

    public Page<Irepair_order> fetchDefault(SearchContext context);

    public void get(Irepair_order repair_order);

    public void remove(Irepair_order repair_order);

    public Page<Irepair_order> select(SearchContext context);

    public void getDraft(Irepair_order repair_order);

}
