package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_leave_allocation] 对象
 */
public interface Ihr_leave_allocation {

    /**
     * 获取 [权责发生制]
     */
    public void setAccrual(String accrual);
    
    /**
     * 设置 [权责发生制]
     */
    public String getAccrual();

    /**
     * 获取 [权责发生制]脏标记
     */
    public boolean getAccrualDirtyFlag();
    /**
     * 获取 [余额限额]
     */
    public void setAccrual_limit(Integer accrual_limit);
    
    /**
     * 设置 [余额限额]
     */
    public Integer getAccrual_limit();

    /**
     * 获取 [余额限额]脏标记
     */
    public boolean getAccrual_limitDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [能批准]
     */
    public void setCan_approve(String can_approve);
    
    /**
     * 设置 [能批准]
     */
    public String getCan_approve();

    /**
     * 获取 [能批准]脏标记
     */
    public boolean getCan_approveDirtyFlag();
    /**
     * 获取 [能重置]
     */
    public void setCan_reset(String can_reset);
    
    /**
     * 设置 [能重置]
     */
    public String getCan_reset();

    /**
     * 获取 [能重置]脏标记
     */
    public boolean getCan_resetDirtyFlag();
    /**
     * 获取 [员工标签]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [员工标签]
     */
    public Integer getCategory_id();

    /**
     * 获取 [员工标签]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [员工标签]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [员工标签]
     */
    public String getCategory_id_text();

    /**
     * 获取 [员工标签]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_from(Timestamp date_from);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_from();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_fromDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setDate_to(Timestamp date_to);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getDate_to();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getDate_toDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [分配 （天/小时）]
     */
    public void setDuration_display(String duration_display);
    
    /**
     * 设置 [分配 （天/小时）]
     */
    public String getDuration_display();

    /**
     * 获取 [分配 （天/小时）]脏标记
     */
    public boolean getDuration_displayDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id(Integer employee_id);
    
    /**
     * 设置 [员工]
     */
    public Integer getEmployee_id();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_idDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id_text(String employee_id_text);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee_id_text();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_id_textDirtyFlag();
    /**
     * 获取 [首次审批]
     */
    public void setFirst_approver_id(Integer first_approver_id);
    
    /**
     * 设置 [首次审批]
     */
    public Integer getFirst_approver_id();

    /**
     * 获取 [首次审批]脏标记
     */
    public boolean getFirst_approver_idDirtyFlag();
    /**
     * 获取 [首次审批]
     */
    public void setFirst_approver_id_text(String first_approver_id_text);
    
    /**
     * 设置 [首次审批]
     */
    public String getFirst_approver_id_text();

    /**
     * 获取 [首次审批]脏标记
     */
    public boolean getFirst_approver_id_textDirtyFlag();
    /**
     * 获取 [休假类型]
     */
    public void setHoliday_status_id(Integer holiday_status_id);
    
    /**
     * 设置 [休假类型]
     */
    public Integer getHoliday_status_id();

    /**
     * 获取 [休假类型]脏标记
     */
    public boolean getHoliday_status_idDirtyFlag();
    /**
     * 获取 [休假类型]
     */
    public void setHoliday_status_id_text(String holiday_status_id_text);
    
    /**
     * 设置 [休假类型]
     */
    public String getHoliday_status_id_text();

    /**
     * 获取 [休假类型]脏标记
     */
    public boolean getHoliday_status_id_textDirtyFlag();
    /**
     * 获取 [分配模式]
     */
    public void setHoliday_type(String holiday_type);
    
    /**
     * 设置 [分配模式]
     */
    public String getHoliday_type();

    /**
     * 获取 [分配模式]脏标记
     */
    public boolean getHoliday_typeDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [两个间隔之间的单位数]
     */
    public void setInterval_number(Integer interval_number);
    
    /**
     * 设置 [两个间隔之间的单位数]
     */
    public Integer getInterval_number();

    /**
     * 获取 [两个间隔之间的单位数]脏标记
     */
    public boolean getInterval_numberDirtyFlag();
    /**
     * 获取 [两个区间之间的时间单位]
     */
    public void setInterval_unit(String interval_unit);
    
    /**
     * 设置 [两个区间之间的时间单位]
     */
    public String getInterval_unit();

    /**
     * 获取 [两个区间之间的时间单位]脏标记
     */
    public boolean getInterval_unitDirtyFlag();
    /**
     * 获取 [链接申请]
     */
    public void setLinked_request_ids(String linked_request_ids);
    
    /**
     * 设置 [链接申请]
     */
    public String getLinked_request_ids();

    /**
     * 获取 [链接申请]脏标记
     */
    public boolean getLinked_request_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [信息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [信息]
     */
    public String getMessage_ids();

    /**
     * 获取 [信息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setMode_company_id(Integer mode_company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getMode_company_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getMode_company_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setMode_company_id_text(String mode_company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getMode_company_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getMode_company_id_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [下一次应计分配的日期]
     */
    public void setNextcall(Timestamp nextcall);
    
    /**
     * 设置 [下一次应计分配的日期]
     */
    public Timestamp getNextcall();

    /**
     * 获取 [下一次应计分配的日期]脏标记
     */
    public boolean getNextcallDirtyFlag();
    /**
     * 获取 [理由]
     */
    public void setNotes(String notes);
    
    /**
     * 设置 [理由]
     */
    public String getNotes();

    /**
     * 获取 [理由]脏标记
     */
    public boolean getNotesDirtyFlag();
    /**
     * 获取 [天数]
     */
    public void setNumber_of_days(Double number_of_days);
    
    /**
     * 设置 [天数]
     */
    public Double getNumber_of_days();

    /**
     * 获取 [天数]脏标记
     */
    public boolean getNumber_of_daysDirtyFlag();
    /**
     * 获取 [持续时间（天）]
     */
    public void setNumber_of_days_display(Double number_of_days_display);
    
    /**
     * 设置 [持续时间（天）]
     */
    public Double getNumber_of_days_display();

    /**
     * 获取 [持续时间（天）]脏标记
     */
    public boolean getNumber_of_days_displayDirtyFlag();
    /**
     * 获取 [时长(小时)]
     */
    public void setNumber_of_hours_display(Double number_of_hours_display);
    
    /**
     * 设置 [时长(小时)]
     */
    public Double getNumber_of_hours_display();

    /**
     * 获取 [时长(小时)]脏标记
     */
    public boolean getNumber_of_hours_displayDirtyFlag();
    /**
     * 获取 [每个间隔的单位数]
     */
    public void setNumber_per_interval(Double number_per_interval);
    
    /**
     * 设置 [每个间隔的单位数]
     */
    public Double getNumber_per_interval();

    /**
     * 获取 [每个间隔的单位数]脏标记
     */
    public boolean getNumber_per_intervalDirtyFlag();
    /**
     * 获取 [上级]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [第二次审批]
     */
    public void setSecond_approver_id(Integer second_approver_id);
    
    /**
     * 设置 [第二次审批]
     */
    public Integer getSecond_approver_id();

    /**
     * 获取 [第二次审批]脏标记
     */
    public boolean getSecond_approver_idDirtyFlag();
    /**
     * 获取 [第二次审批]
     */
    public void setSecond_approver_id_text(String second_approver_id_text);
    
    /**
     * 设置 [第二次审批]
     */
    public String getSecond_approver_id_text();

    /**
     * 获取 [第二次审批]脏标记
     */
    public boolean getSecond_approver_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [休假]
     */
    public void setType_request_unit(String type_request_unit);
    
    /**
     * 设置 [休假]
     */
    public String getType_request_unit();

    /**
     * 获取 [休假]脏标记
     */
    public boolean getType_request_unitDirtyFlag();
    /**
     * 获取 [在每个区间添加的时间单位]
     */
    public void setUnit_per_interval(String unit_per_interval);
    
    /**
     * 设置 [在每个区间添加的时间单位]
     */
    public String getUnit_per_interval();

    /**
     * 获取 [在每个区间添加的时间单位]脏标记
     */
    public boolean getUnit_per_intervalDirtyFlag();
    /**
     * 获取 [验证人]
     */
    public void setValidation_type(String validation_type);
    
    /**
     * 设置 [验证人]
     */
    public String getValidation_type();

    /**
     * 获取 [验证人]脏标记
     */
    public boolean getValidation_typeDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
