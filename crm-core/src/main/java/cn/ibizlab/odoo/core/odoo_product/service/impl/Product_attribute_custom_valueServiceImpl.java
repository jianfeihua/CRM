package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_custom_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attribute_custom_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_attribute_custom_valueFeignClient;

/**
 * 实体[产品属性自定义值] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_attribute_custom_valueServiceImpl implements IProduct_attribute_custom_valueService {

    @Autowired
    product_attribute_custom_valueFeignClient product_attribute_custom_valueFeignClient;


    @Override
    public boolean create(Product_attribute_custom_value et) {
        Product_attribute_custom_value rt = product_attribute_custom_valueFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_attribute_custom_value> list){
        product_attribute_custom_valueFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_attribute_custom_valueFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_attribute_custom_valueFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Product_attribute_custom_value et) {
        Product_attribute_custom_value rt = product_attribute_custom_valueFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_attribute_custom_value> list){
        product_attribute_custom_valueFeignClient.updateBatch(list) ;
    }

    @Override
    public Product_attribute_custom_value getDraft(Product_attribute_custom_value et) {
        et=product_attribute_custom_valueFeignClient.getDraft();
        return et;
    }

    @Override
    public Product_attribute_custom_value get(Integer id) {
		Product_attribute_custom_value et=product_attribute_custom_valueFeignClient.get(id);
        if(et==null){
            et=new Product_attribute_custom_value();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_attribute_custom_value> searchDefault(Product_attribute_custom_valueSearchContext context) {
        Page<Product_attribute_custom_value> product_attribute_custom_values=product_attribute_custom_valueFeignClient.searchDefault(context);
        return product_attribute_custom_values;
    }


}


