package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_unreconcileSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_unreconcile] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-unreconcile", fallback = account_unreconcileFallback.class)
public interface account_unreconcileFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles/searchdefault")
    Page<Account_unreconcile> searchDefault(@RequestBody Account_unreconcileSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_unreconciles/{id}")
    Account_unreconcile get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles")
    Account_unreconcile create(@RequestBody Account_unreconcile account_unreconcile);

    @RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles/batch")
    Boolean createBatch(@RequestBody List<Account_unreconcile> account_unreconciles);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_unreconciles/{id}")
    Account_unreconcile update(@PathVariable("id") Integer id,@RequestBody Account_unreconcile account_unreconcile);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_unreconciles/batch")
    Boolean updateBatch(@RequestBody List<Account_unreconcile> account_unreconciles);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_unreconciles/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_unreconciles/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_unreconciles/select")
    Page<Account_unreconcile> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_unreconciles/getdraft")
    Account_unreconcile getDraft();


}
