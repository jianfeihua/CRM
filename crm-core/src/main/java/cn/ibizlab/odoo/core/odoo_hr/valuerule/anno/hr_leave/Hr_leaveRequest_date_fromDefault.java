package cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_leave;

import cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_leave.Hr_leaveRequest_date_fromDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Hr_leave
 * 属性：Request_date_from
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Hr_leaveRequest_date_fromDefaultValidator.class})
public @interface Hr_leaveRequest_date_fromDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
