package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_transactionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_payment.client.payment_transactionFeignClient;

/**
 * 实体[付款交易] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_transactionServiceImpl implements IPayment_transactionService {

    @Autowired
    payment_transactionFeignClient payment_transactionFeignClient;


    @Override
    public boolean create(Payment_transaction et) {
        Payment_transaction rt = payment_transactionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_transaction> list){
        payment_transactionFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=payment_transactionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        payment_transactionFeignClient.removeBatch(idList);
    }

    @Override
    public Payment_transaction get(Integer id) {
		Payment_transaction et=payment_transactionFeignClient.get(id);
        if(et==null){
            et=new Payment_transaction();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Payment_transaction getDraft(Payment_transaction et) {
        et=payment_transactionFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Payment_transaction et) {
        Payment_transaction rt = payment_transactionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Payment_transaction> list){
        payment_transactionFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context) {
        Page<Payment_transaction> payment_transactions=payment_transactionFeignClient.searchDefault(context);
        return payment_transactions;
    }


}


