package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_supplierinfo;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_supplierinfo] 服务对象接口
 */
public interface Iproduct_supplierinfoClientService{

    public Iproduct_supplierinfo createModel() ;

    public Page<Iproduct_supplierinfo> fetchDefault(SearchContext context);

    public void createBatch(List<Iproduct_supplierinfo> product_supplierinfos);

    public void update(Iproduct_supplierinfo product_supplierinfo);

    public void remove(Iproduct_supplierinfo product_supplierinfo);

    public void removeBatch(List<Iproduct_supplierinfo> product_supplierinfos);

    public void updateBatch(List<Iproduct_supplierinfo> product_supplierinfos);

    public void create(Iproduct_supplierinfo product_supplierinfo);

    public void get(Iproduct_supplierinfo product_supplierinfo);

    public Page<Iproduct_supplierinfo> select(SearchContext context);

    public void getDraft(Iproduct_supplierinfo product_supplierinfo);

}
