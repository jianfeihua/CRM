package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_ruleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_ruleFeignClient;

/**
 * 实体[Preventive Maintenance Rule] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_ruleServiceImpl implements IMro_pm_ruleService {

    @Autowired
    mro_pm_ruleFeignClient mro_pm_ruleFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mro_pm_ruleFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_pm_ruleFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mro_pm_rule et) {
        Mro_pm_rule rt = mro_pm_ruleFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_rule> list){
        mro_pm_ruleFeignClient.createBatch(list) ;
    }

    @Override
    public Mro_pm_rule get(Integer id) {
		Mro_pm_rule et=mro_pm_ruleFeignClient.get(id);
        if(et==null){
            et=new Mro_pm_rule();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mro_pm_rule getDraft(Mro_pm_rule et) {
        et=mro_pm_ruleFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mro_pm_rule et) {
        Mro_pm_rule rt = mro_pm_ruleFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_pm_rule> list){
        mro_pm_ruleFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_rule> searchDefault(Mro_pm_ruleSearchContext context) {
        Page<Mro_pm_rule> mro_pm_rules=mro_pm_ruleFeignClient.searchDefault(context);
        return mro_pm_rules;
    }


}


