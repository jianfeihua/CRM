package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Inote_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[note_tag] 服务对象接口
 */
public interface Inote_tagClientService{

    public Inote_tag createModel() ;

    public void removeBatch(List<Inote_tag> note_tags);

    public void get(Inote_tag note_tag);

    public void create(Inote_tag note_tag);

    public Page<Inote_tag> fetchDefault(SearchContext context);

    public void remove(Inote_tag note_tag);

    public void createBatch(List<Inote_tag> note_tags);

    public void updateBatch(List<Inote_tag> note_tags);

    public void update(Inote_tag note_tag);

    public Page<Inote_tag> select(SearchContext context);

    public void getDraft(Inote_tag note_tag);

}
