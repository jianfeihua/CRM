package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_lead_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead_tag] 服务对象接口
 */
public interface Icrm_lead_tagClientService{

    public Icrm_lead_tag createModel() ;

    public void remove(Icrm_lead_tag crm_lead_tag);

    public void get(Icrm_lead_tag crm_lead_tag);

    public void update(Icrm_lead_tag crm_lead_tag);

    public void updateBatch(List<Icrm_lead_tag> crm_lead_tags);

    public void create(Icrm_lead_tag crm_lead_tag);

    public Page<Icrm_lead_tag> fetchDefault(SearchContext context);

    public void removeBatch(List<Icrm_lead_tag> crm_lead_tags);

    public void createBatch(List<Icrm_lead_tag> crm_lead_tags);

    public Page<Icrm_lead_tag> select(SearchContext context);

    public void getDraft(Icrm_lead_tag crm_lead_tag);

}
