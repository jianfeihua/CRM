package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_redirectService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_website.client.website_redirectFeignClient;

/**
 * 实体[网站重定向] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_redirectServiceImpl implements IWebsite_redirectService {

    @Autowired
    website_redirectFeignClient website_redirectFeignClient;


    @Override
    public boolean update(Website_redirect et) {
        Website_redirect rt = website_redirectFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Website_redirect> list){
        website_redirectFeignClient.updateBatch(list) ;
    }

    @Override
    public Website_redirect get(Integer id) {
		Website_redirect et=website_redirectFeignClient.get(id);
        if(et==null){
            et=new Website_redirect();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Website_redirect et) {
        Website_redirect rt = website_redirectFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_redirect> list){
        website_redirectFeignClient.createBatch(list) ;
    }

    @Override
    public Website_redirect getDraft(Website_redirect et) {
        et=website_redirectFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=website_redirectFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        website_redirectFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_redirect> searchDefault(Website_redirectSearchContext context) {
        Page<Website_redirect> website_redirects=website_redirectFeignClient.searchDefault(context);
        return website_redirects;
    }


}


