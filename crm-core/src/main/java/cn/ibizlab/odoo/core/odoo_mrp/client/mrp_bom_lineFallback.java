package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_bom_line] 服务对象接口
 */
@Component
public class mrp_bom_lineFallback implements mrp_bom_lineFeignClient{


    public Page<Mrp_bom_line> searchDefault(Mrp_bom_lineSearchContext context){
            return null;
     }




    public Mrp_bom_line update(Integer id, Mrp_bom_line mrp_bom_line){
            return null;
     }
    public Boolean updateBatch(List<Mrp_bom_line> mrp_bom_lines){
            return false;
     }


    public Mrp_bom_line get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mrp_bom_line create(Mrp_bom_line mrp_bom_line){
            return null;
     }
    public Boolean createBatch(List<Mrp_bom_line> mrp_bom_lines){
            return false;
     }

    public Page<Mrp_bom_line> select(){
            return null;
     }

    public Mrp_bom_line getDraft(){
            return null;
    }



}
