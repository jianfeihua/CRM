package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_botSearchContext;


/**
 * 实体[Mail_bot] 服务对象接口
 */
public interface IMail_botService{

    boolean update(Mail_bot et) ;
    void updateBatch(List<Mail_bot> list) ;
    Mail_bot get(Integer key) ;
    boolean create(Mail_bot et) ;
    void createBatch(List<Mail_bot> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_bot getDraft(Mail_bot et) ;
    Page<Mail_bot> searchDefault(Mail_botSearchContext context) ;

}



