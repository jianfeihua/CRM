package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelistService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_pricelistFeignClient;

/**
 * 实体[价格表] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_pricelistServiceImpl implements IProduct_pricelistService {

    @Autowired
    product_pricelistFeignClient product_pricelistFeignClient;


    @Override
    public Product_pricelist get(Integer id) {
		Product_pricelist et=product_pricelistFeignClient.get(id);
        if(et==null){
            et=new Product_pricelist();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    @Transactional
    public boolean save(Product_pricelist et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!product_pricelistFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Product_pricelist> list) {
        product_pricelistFeignClient.saveBatch(list) ;
    }

    @Override
    public Product_pricelist getDraft(Product_pricelist et) {
        et=product_pricelistFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Product_pricelist et) {
        Product_pricelist rt = product_pricelistFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_pricelist> list){
        product_pricelistFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_pricelistFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_pricelistFeignClient.removeBatch(idList);
    }

    @Override
    public boolean checkKey(Product_pricelist et) {
        return product_pricelistFeignClient.checkKey(et);
    }
    @Override
    public boolean create(Product_pricelist et) {
        Product_pricelist rt = product_pricelistFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_pricelist> list){
        product_pricelistFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_pricelist> searchDefault(Product_pricelistSearchContext context) {
        Page<Product_pricelist> product_pricelists=product_pricelistFeignClient.searchDefault(context);
        return product_pricelists;
    }


}


