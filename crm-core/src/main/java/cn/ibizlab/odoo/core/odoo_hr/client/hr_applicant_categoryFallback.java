package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicant_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_applicant_category] 服务对象接口
 */
@Component
public class hr_applicant_categoryFallback implements hr_applicant_categoryFeignClient{


    public Hr_applicant_category get(Integer id){
            return null;
     }


    public Hr_applicant_category create(Hr_applicant_category hr_applicant_category){
            return null;
     }
    public Boolean createBatch(List<Hr_applicant_category> hr_applicant_categories){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Hr_applicant_category> searchDefault(Hr_applicant_categorySearchContext context){
            return null;
     }


    public Hr_applicant_category update(Integer id, Hr_applicant_category hr_applicant_category){
            return null;
     }
    public Boolean updateBatch(List<Hr_applicant_category> hr_applicant_categories){
            return false;
     }



    public Page<Hr_applicant_category> select(){
            return null;
     }

    public Hr_applicant_category getDraft(){
            return null;
    }



}
