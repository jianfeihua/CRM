package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_public_category] 服务对象接口
 */
@Component
public class product_public_categoryFallback implements product_public_categoryFeignClient{

    public Product_public_category get(Integer id){
            return null;
     }


    public Product_public_category create(Product_public_category product_public_category){
            return null;
     }
    public Boolean createBatch(List<Product_public_category> product_public_categories){
            return false;
     }


    public Page<Product_public_category> searchDefault(Product_public_categorySearchContext context){
            return null;
     }



    public Product_public_category update(Integer id, Product_public_category product_public_category){
            return null;
     }
    public Boolean updateBatch(List<Product_public_category> product_public_categories){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Product_public_category> select(){
            return null;
     }

    public Product_public_category getDraft(){
            return null;
    }



}
