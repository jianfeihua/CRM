package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_register_payments] 服务对象接口
 */
@Component
public class account_register_paymentsFallback implements account_register_paymentsFeignClient{

    public Account_register_payments create(Account_register_payments account_register_payments){
            return null;
     }
    public Boolean createBatch(List<Account_register_payments> account_register_payments){
            return false;
     }



    public Page<Account_register_payments> searchDefault(Account_register_paymentsSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_register_payments update(Integer id, Account_register_payments account_register_payments){
            return null;
     }
    public Boolean updateBatch(List<Account_register_payments> account_register_payments){
            return false;
     }


    public Account_register_payments get(Integer id){
            return null;
     }


    public Page<Account_register_payments> select(){
            return null;
     }

    public Boolean checkKey(Account_register_payments account_register_payments){
            return false;
     }


    public Boolean save(Account_register_payments account_register_payments){
            return false;
     }
    public Boolean saveBatch(List<Account_register_payments> account_register_payments){
            return false;
     }

    public Account_register_payments getDraft(){
            return null;
    }



}
