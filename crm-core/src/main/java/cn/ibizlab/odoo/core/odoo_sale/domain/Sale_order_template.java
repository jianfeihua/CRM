package cn.ibizlab.odoo.core.odoo_sale.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [报价单模板] 对象
 */
@Data
public class Sale_order_template extends EntityClient implements Serializable {

    /**
     * 报价单模板
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 在线签名
     */
    @DEField(name = "require_signature")
    @JSONField(name = "require_signature")
    @JsonProperty("require_signature")
    private String requireSignature;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 在线支付
     */
    @DEField(name = "require_payment")
    @JSONField(name = "require_payment")
    @JsonProperty("require_payment")
    private String requirePayment;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 报价单时长
     */
    @DEField(name = "number_of_days")
    @JSONField(name = "number_of_days")
    @JsonProperty("number_of_days")
    private Integer numberOfDays;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 条款和条件
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 可选产品
     */
    @JSONField(name = "sale_order_template_option_ids")
    @JsonProperty("sale_order_template_option_ids")
    private String saleOrderTemplateOptionIds;

    /**
     * 明细行
     */
    @JSONField(name = "sale_order_template_line_ids")
    @JsonProperty("sale_order_template_line_ids")
    private String saleOrderTemplateLineIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 确认邮件
     */
    @JSONField(name = "mail_template_id_text")
    @JsonProperty("mail_template_id_text")
    private String mailTemplateIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 确认邮件
     */
    @DEField(name = "mail_template_id")
    @JSONField(name = "mail_template_id")
    @JsonProperty("mail_template_id")
    private Integer mailTemplateId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoomailtemplate")
    @JsonProperty("odoomailtemplate")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template odooMailTemplate;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [报价单模板]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [在线签名]
     */
    public void setRequireSignature(String requireSignature){
        this.requireSignature = requireSignature ;
        this.modify("require_signature",requireSignature);
    }
    /**
     * 设置 [在线支付]
     */
    public void setRequirePayment(String requirePayment){
        this.requirePayment = requirePayment ;
        this.modify("require_payment",requirePayment);
    }
    /**
     * 设置 [报价单时长]
     */
    public void setNumberOfDays(Integer numberOfDays){
        this.numberOfDays = numberOfDays ;
        this.modify("number_of_days",numberOfDays);
    }
    /**
     * 设置 [条款和条件]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [确认邮件]
     */
    public void setMailTemplateId(Integer mailTemplateId){
        this.mailTemplateId = mailTemplateId ;
        this.modify("mail_template_id",mailTemplateId);
    }

}


