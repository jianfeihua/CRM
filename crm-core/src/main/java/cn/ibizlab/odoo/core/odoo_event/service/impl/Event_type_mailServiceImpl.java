package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_type_mailService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_type_mailFeignClient;

/**
 * 实体[基于活动分类的邮件调度] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_type_mailServiceImpl implements IEvent_type_mailService {

    @Autowired
    event_type_mailFeignClient event_type_mailFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=event_type_mailFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_type_mailFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Event_type_mail et) {
        Event_type_mail rt = event_type_mailFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_type_mail> list){
        event_type_mailFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Event_type_mail et) {
        Event_type_mail rt = event_type_mailFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_type_mail> list){
        event_type_mailFeignClient.updateBatch(list) ;
    }

    @Override
    public Event_type_mail getDraft(Event_type_mail et) {
        et=event_type_mailFeignClient.getDraft();
        return et;
    }

    @Override
    public Event_type_mail get(Integer id) {
		Event_type_mail et=event_type_mailFeignClient.get(id);
        if(et==null){
            et=new Event_type_mail();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_type_mail> searchDefault(Event_type_mailSearchContext context) {
        Page<Event_type_mail> event_type_mails=event_type_mailFeignClient.searchDefault(context);
        return event_type_mails;
    }


}


