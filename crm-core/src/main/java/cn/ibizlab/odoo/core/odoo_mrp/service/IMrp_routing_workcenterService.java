package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;


/**
 * 实体[Mrp_routing_workcenter] 服务对象接口
 */
public interface IMrp_routing_workcenterService{

    Mrp_routing_workcenter getDraft(Mrp_routing_workcenter et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mrp_routing_workcenter et) ;
    void createBatch(List<Mrp_routing_workcenter> list) ;
    boolean update(Mrp_routing_workcenter et) ;
    void updateBatch(List<Mrp_routing_workcenter> list) ;
    Mrp_routing_workcenter get(Integer key) ;
    Page<Mrp_routing_workcenter> searchDefault(Mrp_routing_workcenterSearchContext context) ;

}



