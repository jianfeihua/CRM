package cn.ibizlab.odoo.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_campaignSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[utm_campaign] 服务对象接口
 */
@FeignClient(value = "odoo-utm", contextId = "utm-campaign", fallback = utm_campaignFallback.class)
public interface utm_campaignFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_campaigns/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_campaigns/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_campaigns/{id}")
    Utm_campaign get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/utm_campaigns/{id}")
    Utm_campaign update(@PathVariable("id") Integer id,@RequestBody Utm_campaign utm_campaign);

    @RequestMapping(method = RequestMethod.PUT, value = "/utm_campaigns/batch")
    Boolean updateBatch(@RequestBody List<Utm_campaign> utm_campaigns);



    @RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns")
    Utm_campaign create(@RequestBody Utm_campaign utm_campaign);

    @RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns/batch")
    Boolean createBatch(@RequestBody List<Utm_campaign> utm_campaigns);





    @RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns/searchdefault")
    Page<Utm_campaign> searchDefault(@RequestBody Utm_campaignSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_campaigns/select")
    Page<Utm_campaign> select();


    @RequestMapping(method = RequestMethod.GET, value = "/utm_campaigns/getdraft")
    Utm_campaign getDraft();


}
