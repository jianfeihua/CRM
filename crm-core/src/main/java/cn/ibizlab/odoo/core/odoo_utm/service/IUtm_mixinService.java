package cn.ibizlab.odoo.core.odoo_utm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mixinSearchContext;


/**
 * 实体[Utm_mixin] 服务对象接口
 */
public interface IUtm_mixinService{

    boolean update(Utm_mixin et) ;
    void updateBatch(List<Utm_mixin> list) ;
    Utm_mixin getDraft(Utm_mixin et) ;
    Utm_mixin get(Integer key) ;
    boolean create(Utm_mixin et) ;
    void createBatch(List<Utm_mixin> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Utm_mixin> searchDefault(Utm_mixinSearchContext context) ;

}



