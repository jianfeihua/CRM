package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_closebalance;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_bank_statement_closebalance] 服务对象接口
 */
public interface Iaccount_bank_statement_closebalanceClientService{

    public Iaccount_bank_statement_closebalance createModel() ;

    public Page<Iaccount_bank_statement_closebalance> fetchDefault(SearchContext context);

    public void update(Iaccount_bank_statement_closebalance account_bank_statement_closebalance);

    public void remove(Iaccount_bank_statement_closebalance account_bank_statement_closebalance);

    public void get(Iaccount_bank_statement_closebalance account_bank_statement_closebalance);

    public void create(Iaccount_bank_statement_closebalance account_bank_statement_closebalance);

    public void createBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances);

    public void removeBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances);

    public void updateBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances);

    public Page<Iaccount_bank_statement_closebalance> select(SearchContext context);

    public void getDraft(Iaccount_bank_statement_closebalance account_bank_statement_closebalance);

}
