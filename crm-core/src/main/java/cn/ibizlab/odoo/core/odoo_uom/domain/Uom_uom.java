package cn.ibizlab.odoo.core.odoo_uom.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [产品计量单位] 对象
 */
@Data
public class Uom_uom extends EntityClient implements Serializable {

    /**
     * 单位
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 更大比率
     */
    @JSONField(name = "factor_inv")
    @JsonProperty("factor_inv")
    private Double factorInv;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 舍入精度
     */
    @JSONField(name = "rounding")
    @JsonProperty("rounding")
    private Double rounding;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 比例
     */
    @JSONField(name = "factor")
    @JsonProperty("factor")
    private Double factor;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 类型
     */
    @DEField(name = "uom_type")
    @JSONField(name = "uom_type")
    @JsonProperty("uom_type")
    private String uomType;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 类别
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;

    /**
     * 分组销售点中的产品
     */
    @JSONField(name = "is_pos_groupable")
    @JsonProperty("is_pos_groupable")
    private String isPosGroupable;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 计量单位的类别
     */
    @JSONField(name = "measure_type")
    @JsonProperty("measure_type")
    private String measureType;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 类别
     */
    @DEField(name = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoocategory")
    @JsonProperty("odoocategory")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category odooCategory;


    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_lines")
    @JsonProperty("sale_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> saleOrderLine;



    /**
     * 设置 [单位]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [舍入精度]
     */
    public void setRounding(Double rounding){
        this.rounding = rounding ;
        this.modify("rounding",rounding);
    }
    /**
     * 设置 [比例]
     */
    public void setFactor(Double factor){
        this.factor = factor ;
        this.modify("factor",factor);
    }
    /**
     * 设置 [类型]
     */
    public void setUomType(String uomType){
        this.uomType = uomType ;
        this.modify("uom_type",uomType);
    }
    /**
     * 设置 [类别]
     */
    public void setCategoryId(Integer categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

}


