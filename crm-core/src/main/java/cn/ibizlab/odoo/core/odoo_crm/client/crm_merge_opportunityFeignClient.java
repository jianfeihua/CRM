package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_merge_opportunity] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-merge-opportunity", fallback = crm_merge_opportunityFallback.class)
public interface crm_merge_opportunityFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/searchdefault")
    Page<Crm_merge_opportunity> searchDefault(@RequestBody Crm_merge_opportunitySearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/{id}")
    Crm_merge_opportunity update(@PathVariable("id") Integer id,@RequestBody Crm_merge_opportunity crm_merge_opportunity);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/batch")
    Boolean updateBatch(@RequestBody List<Crm_merge_opportunity> crm_merge_opportunities);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities")
    Crm_merge_opportunity create(@RequestBody Crm_merge_opportunity crm_merge_opportunity);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/batch")
    Boolean createBatch(@RequestBody List<Crm_merge_opportunity> crm_merge_opportunities);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/{id}")
    Crm_merge_opportunity get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/select")
    Page<Crm_merge_opportunity> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/getdraft")
    Crm_merge_opportunity getDraft();


}
