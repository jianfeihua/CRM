package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[calendar_contacts] 服务对象接口
 */
@FeignClient(value = "odoo-calendar", contextId = "calendar-contacts", fallback = calendar_contactsFallback.class)
public interface calendar_contactsFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_contacts/{id}")
    Calendar_contacts update(@PathVariable("id") Integer id,@RequestBody Calendar_contacts calendar_contacts);

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_contacts/batch")
    Boolean updateBatch(@RequestBody List<Calendar_contacts> calendar_contacts);


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts")
    Calendar_contacts create(@RequestBody Calendar_contacts calendar_contacts);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts/batch")
    Boolean createBatch(@RequestBody List<Calendar_contacts> calendar_contacts);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts/searchdefault")
    Page<Calendar_contacts> searchDefault(@RequestBody Calendar_contactsSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_contacts/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_contacts/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_contacts/{id}")
    Calendar_contacts get(@PathVariable("id") Integer id);





    @RequestMapping(method = RequestMethod.GET, value = "/calendar_contacts/select")
    Page<Calendar_contacts> select();


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_contacts/getdraft")
    Calendar_contacts getDraft();


}
