package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[repair_line] 服务对象接口
 */
@FeignClient(value = "odoo-repair", contextId = "repair-line", fallback = repair_lineFallback.class)
public interface repair_lineFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/repair_lines/searchdefault")
    Page<Repair_line> searchDefault(@RequestBody Repair_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/repair_lines/{id}")
    Repair_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/repair_lines/{id}")
    Repair_line update(@PathVariable("id") Integer id,@RequestBody Repair_line repair_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/repair_lines/batch")
    Boolean updateBatch(@RequestBody List<Repair_line> repair_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/repair_lines")
    Repair_line create(@RequestBody Repair_line repair_line);

    @RequestMapping(method = RequestMethod.POST, value = "/repair_lines/batch")
    Boolean createBatch(@RequestBody List<Repair_line> repair_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/repair_lines/select")
    Page<Repair_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/repair_lines/getdraft")
    Repair_line getDraft();


}
