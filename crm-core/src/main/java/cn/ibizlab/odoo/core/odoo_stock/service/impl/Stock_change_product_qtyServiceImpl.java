package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_product_qtyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_change_product_qtyFeignClient;

/**
 * 实体[更改产品数量] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_change_product_qtyServiceImpl implements IStock_change_product_qtyService {

    @Autowired
    stock_change_product_qtyFeignClient stock_change_product_qtyFeignClient;


    @Override
    public boolean update(Stock_change_product_qty et) {
        Stock_change_product_qty rt = stock_change_product_qtyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_change_product_qty> list){
        stock_change_product_qtyFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_change_product_qty getDraft(Stock_change_product_qty et) {
        et=stock_change_product_qtyFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Stock_change_product_qty et) {
        Stock_change_product_qty rt = stock_change_product_qtyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_change_product_qty> list){
        stock_change_product_qtyFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_change_product_qty get(Integer id) {
		Stock_change_product_qty et=stock_change_product_qtyFeignClient.get(id);
        if(et==null){
            et=new Stock_change_product_qty();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_change_product_qtyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_change_product_qtyFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_change_product_qty> searchDefault(Stock_change_product_qtySearchContext context) {
        Page<Stock_change_product_qty> stock_change_product_qtys=stock_change_product_qtyFeignClient.searchDefault(context);
        return stock_change_product_qtys;
    }


}


