package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[repair_fee] 服务对象接口
 */
@FeignClient(value = "odoo-repair", contextId = "repair-fee", fallback = repair_feeFallback.class)
public interface repair_feeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/repair_fees/{id}")
    Repair_fee get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/repair_fees")
    Repair_fee create(@RequestBody Repair_fee repair_fee);

    @RequestMapping(method = RequestMethod.POST, value = "/repair_fees/batch")
    Boolean createBatch(@RequestBody List<Repair_fee> repair_fees);




    @RequestMapping(method = RequestMethod.PUT, value = "/repair_fees/{id}")
    Repair_fee update(@PathVariable("id") Integer id,@RequestBody Repair_fee repair_fee);

    @RequestMapping(method = RequestMethod.PUT, value = "/repair_fees/batch")
    Boolean updateBatch(@RequestBody List<Repair_fee> repair_fees);




    @RequestMapping(method = RequestMethod.POST, value = "/repair_fees/searchdefault")
    Page<Repair_fee> searchDefault(@RequestBody Repair_feeSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_fees/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_fees/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/repair_fees/select")
    Page<Repair_fee> select();


    @RequestMapping(method = RequestMethod.GET, value = "/repair_fees/getdraft")
    Repair_fee getDraft();


}
