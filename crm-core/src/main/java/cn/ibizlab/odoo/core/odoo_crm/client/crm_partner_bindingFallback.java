package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_partner_bindingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_partner_binding] 服务对象接口
 */
@Component
public class crm_partner_bindingFallback implements crm_partner_bindingFeignClient{

    public Crm_partner_binding update(Integer id, Crm_partner_binding crm_partner_binding){
            return null;
     }
    public Boolean updateBatch(List<Crm_partner_binding> crm_partner_bindings){
            return false;
     }




    public Crm_partner_binding get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Crm_partner_binding> searchDefault(Crm_partner_bindingSearchContext context){
            return null;
     }



    public Crm_partner_binding create(Crm_partner_binding crm_partner_binding){
            return null;
     }
    public Boolean createBatch(List<Crm_partner_binding> crm_partner_bindings){
            return false;
     }

    public Page<Crm_partner_binding> select(){
            return null;
     }

    public Crm_partner_binding getDraft(){
            return null;
    }



}
