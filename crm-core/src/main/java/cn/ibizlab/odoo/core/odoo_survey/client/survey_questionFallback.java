package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_question] 服务对象接口
 */
@Component
public class survey_questionFallback implements survey_questionFeignClient{

    public Page<Survey_question> searchDefault(Survey_questionSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Survey_question update(Integer id, Survey_question survey_question){
            return null;
     }
    public Boolean updateBatch(List<Survey_question> survey_questions){
            return false;
     }


    public Survey_question get(Integer id){
            return null;
     }


    public Survey_question create(Survey_question survey_question){
            return null;
     }
    public Boolean createBatch(List<Survey_question> survey_questions){
            return false;
     }




    public Page<Survey_question> select(){
            return null;
     }

    public Survey_question getDraft(){
            return null;
    }



}
