package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mrp_production] 对象
 */
public interface Imrp_production {

    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [材料可用性]
     */
    public void setAvailability(String availability);
    
    /**
     * 设置 [材料可用性]
     */
    public String getAvailability();

    /**
     * 获取 [材料可用性]脏标记
     */
    public boolean getAvailabilityDirtyFlag();
    /**
     * 获取 [物料清单]
     */
    public void setBom_id(Integer bom_id);
    
    /**
     * 设置 [物料清单]
     */
    public Integer getBom_id();

    /**
     * 获取 [物料清单]脏标记
     */
    public boolean getBom_idDirtyFlag();
    /**
     * 获取 [检查生产的数量]
     */
    public void setCheck_to_done(String check_to_done);
    
    /**
     * 设置 [检查生产的数量]
     */
    public String getCheck_to_done();

    /**
     * 获取 [检查生产的数量]脏标记
     */
    public boolean getCheck_to_doneDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [消费量少于计划数量]
     */
    public void setConsumed_less_than_planned(String consumed_less_than_planned);
    
    /**
     * 设置 [消费量少于计划数量]
     */
    public String getConsumed_less_than_planned();

    /**
     * 获取 [消费量少于计划数量]脏标记
     */
    public boolean getConsumed_less_than_plannedDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setDate_finished(Timestamp date_finished);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getDate_finished();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getDate_finishedDirtyFlag();
    /**
     * 获取 [截止日期结束]
     */
    public void setDate_planned_finished(Timestamp date_planned_finished);
    
    /**
     * 设置 [截止日期结束]
     */
    public Timestamp getDate_planned_finished();

    /**
     * 获取 [截止日期结束]脏标记
     */
    public boolean getDate_planned_finishedDirtyFlag();
    /**
     * 获取 [截止日期开始]
     */
    public void setDate_planned_start(Timestamp date_planned_start);
    
    /**
     * 设置 [截止日期开始]
     */
    public Timestamp getDate_planned_start();

    /**
     * 获取 [截止日期开始]脏标记
     */
    public boolean getDate_planned_startDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_start(Timestamp date_start);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_start();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_startDirtyFlag();
    /**
     * 获取 [出库单]
     */
    public void setDelivery_count(Integer delivery_count);
    
    /**
     * 设置 [出库单]
     */
    public Integer getDelivery_count();

    /**
     * 获取 [出库单]脏标记
     */
    public boolean getDelivery_countDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [完工产品]
     */
    public void setFinished_move_line_ids(String finished_move_line_ids);
    
    /**
     * 设置 [完工产品]
     */
    public String getFinished_move_line_ids();

    /**
     * 获取 [完工产品]脏标记
     */
    public boolean getFinished_move_line_idsDirtyFlag();
    /**
     * 获取 [有移动]
     */
    public void setHas_moves(String has_moves);
    
    /**
     * 设置 [有移动]
     */
    public String getHas_moves();

    /**
     * 获取 [有移动]脏标记
     */
    public boolean getHas_movesDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [是锁定]
     */
    public void setIs_locked(String is_locked);
    
    /**
     * 设置 [是锁定]
     */
    public String getIs_locked();

    /**
     * 获取 [是锁定]脏标记
     */
    public boolean getIs_lockedDirtyFlag();
    /**
     * 获取 [成品位置]
     */
    public void setLocation_dest_id(Integer location_dest_id);
    
    /**
     * 设置 [成品位置]
     */
    public Integer getLocation_dest_id();

    /**
     * 获取 [成品位置]脏标记
     */
    public boolean getLocation_dest_idDirtyFlag();
    /**
     * 获取 [成品位置]
     */
    public void setLocation_dest_id_text(String location_dest_id_text);
    
    /**
     * 设置 [成品位置]
     */
    public String getLocation_dest_id_text();

    /**
     * 获取 [成品位置]脏标记
     */
    public boolean getLocation_dest_id_textDirtyFlag();
    /**
     * 获取 [原料位置]
     */
    public void setLocation_src_id(Integer location_src_id);
    
    /**
     * 设置 [原料位置]
     */
    public Integer getLocation_src_id();

    /**
     * 获取 [原料位置]脏标记
     */
    public boolean getLocation_src_idDirtyFlag();
    /**
     * 获取 [原料位置]
     */
    public void setLocation_src_id_text(String location_src_id_text);
    
    /**
     * 设置 [原料位置]
     */
    public String getLocation_src_id_text();

    /**
     * 获取 [原料位置]脏标记
     */
    public boolean getLocation_src_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [生产货物的库存移动]
     */
    public void setMove_dest_ids(String move_dest_ids);
    
    /**
     * 设置 [生产货物的库存移动]
     */
    public String getMove_dest_ids();

    /**
     * 获取 [生产货物的库存移动]脏标记
     */
    public boolean getMove_dest_idsDirtyFlag();
    /**
     * 获取 [产成品]
     */
    public void setMove_finished_ids(String move_finished_ids);
    
    /**
     * 设置 [产成品]
     */
    public String getMove_finished_ids();

    /**
     * 获取 [产成品]脏标记
     */
    public boolean getMove_finished_idsDirtyFlag();
    /**
     * 获取 [原材料]
     */
    public void setMove_raw_ids(String move_raw_ids);
    
    /**
     * 设置 [原材料]
     */
    public String getMove_raw_ids();

    /**
     * 获取 [原材料]脏标记
     */
    public boolean getMove_raw_idsDirtyFlag();
    /**
     * 获取 [参考]
     */
    public void setName(String name);
    
    /**
     * 设置 [参考]
     */
    public String getName();

    /**
     * 获取 [参考]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [来源]
     */
    public String getOrigin();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [与此制造订单相关的拣货]
     */
    public void setPicking_ids(String picking_ids);
    
    /**
     * 设置 [与此制造订单相关的拣货]
     */
    public String getPicking_ids();

    /**
     * 获取 [与此制造订单相关的拣货]脏标记
     */
    public boolean getPicking_idsDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id(Integer picking_type_id);
    
    /**
     * 设置 [作业类型]
     */
    public Integer getPicking_type_id();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_idDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id_text(String picking_type_id_text);
    
    /**
     * 设置 [作业类型]
     */
    public String getPicking_type_id_text();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_id_textDirtyFlag();
    /**
     * 获取 [允许发布库存]
     */
    public void setPost_visible(String post_visible);
    
    /**
     * 设置 [允许发布库存]
     */
    public String getPost_visible();

    /**
     * 获取 [允许发布库存]脏标记
     */
    public boolean getPost_visibleDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [补货组]
     */
    public void setProcurement_group_id(Integer procurement_group_id);
    
    /**
     * 设置 [补货组]
     */
    public Integer getProcurement_group_id();

    /**
     * 获取 [补货组]脏标记
     */
    public boolean getProcurement_group_idDirtyFlag();
    /**
     * 获取 [生产位置]
     */
    public void setProduction_location_id(Integer production_location_id);
    
    /**
     * 设置 [生产位置]
     */
    public Integer getProduction_location_id();

    /**
     * 获取 [生产位置]脏标记
     */
    public boolean getProduction_location_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [待生产数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [待生产数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [待生产数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [计量单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [数量总计]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [数量总计]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [数量总计]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [传播取消以及拆分]
     */
    public void setPropagate(String propagate);
    
    /**
     * 设置 [传播取消以及拆分]
     */
    public String getPropagate();

    /**
     * 获取 [传播取消以及拆分]脏标记
     */
    public boolean getPropagateDirtyFlag();
    /**
     * 获取 [已生产数量]
     */
    public void setQty_produced(Double qty_produced);
    
    /**
     * 设置 [已生产数量]
     */
    public Double getQty_produced();

    /**
     * 获取 [已生产数量]脏标记
     */
    public boolean getQty_producedDirtyFlag();
    /**
     * 获取 [工艺]
     */
    public void setRouting_id(Integer routing_id);
    
    /**
     * 设置 [工艺]
     */
    public Integer getRouting_id();

    /**
     * 获取 [工艺]脏标记
     */
    public boolean getRouting_idDirtyFlag();
    /**
     * 获取 [工艺]
     */
    public void setRouting_id_text(String routing_id_text);
    
    /**
     * 设置 [工艺]
     */
    public String getRouting_id_text();

    /**
     * 获取 [工艺]脏标记
     */
    public boolean getRouting_id_textDirtyFlag();
    /**
     * 获取 [报废转移]
     */
    public void setScrap_count(Integer scrap_count);
    
    /**
     * 设置 [报废转移]
     */
    public Integer getScrap_count();

    /**
     * 获取 [报废转移]脏标记
     */
    public boolean getScrap_countDirtyFlag();
    /**
     * 获取 [报废]
     */
    public void setScrap_ids(String scrap_ids);
    
    /**
     * 设置 [报废]
     */
    public String getScrap_ids();

    /**
     * 获取 [报废]脏标记
     */
    public boolean getScrap_idsDirtyFlag();
    /**
     * 获取 [显示最后一批]
     */
    public void setShow_final_lots(String show_final_lots);
    
    /**
     * 设置 [显示最后一批]
     */
    public String getShow_final_lots();

    /**
     * 获取 [显示最后一批]脏标记
     */
    public boolean getShow_final_lotsDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [允许取消预留库存]
     */
    public void setUnreserve_visible(String unreserve_visible);
    
    /**
     * 设置 [允许取消预留库存]
     */
    public String getUnreserve_visible();

    /**
     * 获取 [允许取消预留库存]脏标记
     */
    public boolean getUnreserve_visibleDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [# 工单]
     */
    public void setWorkorder_count(Integer workorder_count);
    
    /**
     * 设置 [# 工单]
     */
    public Integer getWorkorder_count();

    /**
     * 获取 [# 工单]脏标记
     */
    public boolean getWorkorder_countDirtyFlag();
    /**
     * 获取 [# 完工工单]
     */
    public void setWorkorder_done_count(Integer workorder_done_count);
    
    /**
     * 设置 [# 完工工单]
     */
    public Integer getWorkorder_done_count();

    /**
     * 获取 [# 完工工单]脏标记
     */
    public boolean getWorkorder_done_countDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWorkorder_ids(String workorder_ids);
    
    /**
     * 设置 [工单]
     */
    public String getWorkorder_ids();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWorkorder_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
