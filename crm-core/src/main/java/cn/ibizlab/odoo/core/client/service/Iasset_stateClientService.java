package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iasset_state;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[asset_state] 服务对象接口
 */
public interface Iasset_stateClientService{

    public Iasset_state createModel() ;

    public void updateBatch(List<Iasset_state> asset_states);

    public void get(Iasset_state asset_state);

    public void create(Iasset_state asset_state);

    public Page<Iasset_state> fetchDefault(SearchContext context);

    public void removeBatch(List<Iasset_state> asset_states);

    public void remove(Iasset_state asset_state);

    public void createBatch(List<Iasset_state> asset_states);

    public void update(Iasset_state asset_state);

    public Page<Iasset_state> select(SearchContext context);

    public void getDraft(Iasset_state asset_state);

}
