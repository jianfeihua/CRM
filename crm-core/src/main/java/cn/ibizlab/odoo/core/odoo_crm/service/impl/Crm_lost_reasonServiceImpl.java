package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lost_reasonService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_lost_reasonFeignClient;

/**
 * 实体[丢单原因] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_lost_reasonServiceImpl implements ICrm_lost_reasonService {

    @Autowired
    crm_lost_reasonFeignClient crm_lost_reasonFeignClient;


    @Override
    public Crm_lost_reason get(Integer id) {
		Crm_lost_reason et=crm_lost_reasonFeignClient.get(id);
        if(et==null){
            et=new Crm_lost_reason();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_lost_reasonFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_lost_reasonFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Crm_lost_reason et) {
        Crm_lost_reason rt = crm_lost_reasonFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_lost_reason> list){
        crm_lost_reasonFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Crm_lost_reason et) {
        Crm_lost_reason rt = crm_lost_reasonFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lost_reason> list){
        crm_lost_reasonFeignClient.createBatch(list) ;
    }

    @Override
    public Crm_lost_reason getDraft(Crm_lost_reason et) {
        et=crm_lost_reasonFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lost_reason> searchDefault(Crm_lost_reasonSearchContext context) {
        Page<Crm_lost_reason> crm_lost_reasons=crm_lost_reasonFeignClient.searchDefault(context);
        return crm_lost_reasons;
    }


}


