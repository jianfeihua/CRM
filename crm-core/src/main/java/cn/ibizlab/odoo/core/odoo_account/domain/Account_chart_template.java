package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [科目表模版] 对象
 */
@Data
public class Account_chart_template extends EntityClient implements Serializable {

    /**
     * # 数字
     */
    @DEField(name = "code_digits")
    @JSONField(name = "code_digits")
    @JsonProperty("code_digits")
    private Integer codeDigits;

    /**
     * 主转账帐户的前缀
     */
    @DEField(name = "transfer_account_code_prefix")
    @JSONField(name = "transfer_account_code_prefix")
    @JsonProperty("transfer_account_code_prefix")
    private String transferAccountCodePrefix;

    /**
     * 银行科目的前缀
     */
    @DEField(name = "bank_account_code_prefix")
    @JSONField(name = "bank_account_code_prefix")
    @JsonProperty("bank_account_code_prefix")
    private String bankAccountCodePrefix;

    /**
     * 口语
     */
    @DEField(name = "spoken_languages")
    @JSONField(name = "spoken_languages")
    @JsonProperty("spoken_languages")
    private String spokenLanguages;

    /**
     * 税模板列表
     */
    @JSONField(name = "tax_template_ids")
    @JsonProperty("tax_template_ids")
    private String taxTemplateIds;

    /**
     * 可显示？
     */
    @JSONField(name = "visible")
    @JsonProperty("visible")
    private String visible;

    /**
     * 关联的科目模板
     */
    @JSONField(name = "account_ids")
    @JsonProperty("account_ids")
    private String accountIds;

    /**
     * 税率完整集合
     */
    @DEField(name = "complete_tax_set")
    @JSONField(name = "complete_tax_set")
    @JsonProperty("complete_tax_set")
    private String completeTaxSet;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 主现金科目的前缀
     */
    @DEField(name = "cash_account_code_prefix")
    @JSONField(name = "cash_account_code_prefix")
    @JsonProperty("cash_account_code_prefix")
    private String cashAccountCodePrefix;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 使用anglo-saxon会计
     */
    @DEField(name = "use_anglo_saxon")
    @JSONField(name = "use_anglo_saxon")
    @JsonProperty("use_anglo_saxon")
    private String useAngloSaxon;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 汇率损失科目
     */
    @JSONField(name = "expense_currency_exchange_account_id_text")
    @JsonProperty("expense_currency_exchange_account_id_text")
    private String expenseCurrencyExchangeAccountIdText;

    /**
     * 应收科目
     */
    @JSONField(name = "property_account_receivable_id_text")
    @JsonProperty("property_account_receivable_id_text")
    private String propertyAccountReceivableIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 产品模板的费用科目
     */
    @JSONField(name = "property_account_expense_id_text")
    @JsonProperty("property_account_expense_id_text")
    private String propertyAccountExpenseIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 上级表模板
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;

    /**
     * 库存计价的入库科目
     */
    @JSONField(name = "property_stock_account_input_categ_id_text")
    @JsonProperty("property_stock_account_input_categ_id_text")
    private String propertyStockAccountInputCategIdText;

    /**
     * 产品模板的收入科目
     */
    @JSONField(name = "property_account_income_id_text")
    @JsonProperty("property_account_income_id_text")
    private String propertyAccountIncomeIdText;

    /**
     * 费用科目的类别
     */
    @JSONField(name = "property_account_expense_categ_id_text")
    @JsonProperty("property_account_expense_categ_id_text")
    private String propertyAccountExpenseCategIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 汇率增益科目
     */
    @JSONField(name = "income_currency_exchange_account_id_text")
    @JsonProperty("income_currency_exchange_account_id_text")
    private String incomeCurrencyExchangeAccountIdText;

    /**
     * 应付科目
     */
    @JSONField(name = "property_account_payable_id_text")
    @JsonProperty("property_account_payable_id_text")
    private String propertyAccountPayableIdText;

    /**
     * 收入科目的类别
     */
    @JSONField(name = "property_account_income_categ_id_text")
    @JsonProperty("property_account_income_categ_id_text")
    private String propertyAccountIncomeCategIdText;

    /**
     * 库存计价的科目模板
     */
    @JSONField(name = "property_stock_valuation_account_id_text")
    @JsonProperty("property_stock_valuation_account_id_text")
    private String propertyStockValuationAccountIdText;

    /**
     * 库存计价的出货科目
     */
    @JSONField(name = "property_stock_account_output_categ_id_text")
    @JsonProperty("property_stock_account_output_categ_id_text")
    private String propertyStockAccountOutputCategIdText;

    /**
     * 库存计价的科目模板
     */
    @DEField(name = "property_stock_valuation_account_id")
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Integer propertyStockValuationAccountId;

    /**
     * 汇率损失科目
     */
    @DEField(name = "expense_currency_exchange_account_id")
    @JSONField(name = "expense_currency_exchange_account_id")
    @JsonProperty("expense_currency_exchange_account_id")
    private Integer expenseCurrencyExchangeAccountId;

    /**
     * 应付科目
     */
    @DEField(name = "property_account_payable_id")
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    private Integer propertyAccountPayableId;

    /**
     * 收入科目的类别
     */
    @DEField(name = "property_account_income_categ_id")
    @JSONField(name = "property_account_income_categ_id")
    @JsonProperty("property_account_income_categ_id")
    private Integer propertyAccountIncomeCategId;

    /**
     * 库存计价的出货科目
     */
    @DEField(name = "property_stock_account_output_categ_id")
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Integer propertyStockAccountOutputCategId;

    /**
     * 产品模板的费用科目
     */
    @DEField(name = "property_account_expense_id")
    @JSONField(name = "property_account_expense_id")
    @JsonProperty("property_account_expense_id")
    private Integer propertyAccountExpenseId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 上级表模板
     */
    @DEField(name = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 应收科目
     */
    @DEField(name = "property_account_receivable_id")
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    private Integer propertyAccountReceivableId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 费用科目的类别
     */
    @DEField(name = "property_account_expense_categ_id")
    @JSONField(name = "property_account_expense_categ_id")
    @JsonProperty("property_account_expense_categ_id")
    private Integer propertyAccountExpenseCategId;

    /**
     * 汇率增益科目
     */
    @DEField(name = "income_currency_exchange_account_id")
    @JSONField(name = "income_currency_exchange_account_id")
    @JsonProperty("income_currency_exchange_account_id")
    private Integer incomeCurrencyExchangeAccountId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 库存计价的入库科目
     */
    @DEField(name = "property_stock_account_input_categ_id")
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Integer propertyStockAccountInputCategId;

    /**
     * 产品模板的收入科目
     */
    @DEField(name = "property_account_income_id")
    @JSONField(name = "property_account_income_id")
    @JsonProperty("property_account_income_id")
    private Integer propertyAccountIncomeId;


    /**
     * 
     */
    @JSONField(name = "odooexpensecurrencyexchangeaccount")
    @JsonProperty("odooexpensecurrencyexchangeaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooExpenseCurrencyExchangeAccount;

    /**
     * 
     */
    @JSONField(name = "odooincomecurrencyexchangeaccount")
    @JsonProperty("odooincomecurrencyexchangeaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooIncomeCurrencyExchangeAccount;

    /**
     * 
     */
    @JSONField(name = "odoopropertyaccountexpensecateg")
    @JsonProperty("odoopropertyaccountexpensecateg")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyAccountExpenseCateg;

    /**
     * 
     */
    @JSONField(name = "odoopropertyaccountexpense")
    @JsonProperty("odoopropertyaccountexpense")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyAccountExpense;

    /**
     * 
     */
    @JSONField(name = "odoopropertyaccountincomecateg")
    @JsonProperty("odoopropertyaccountincomecateg")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyAccountIncomeCateg;

    /**
     * 
     */
    @JSONField(name = "odoopropertyaccountincome")
    @JsonProperty("odoopropertyaccountincome")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyAccountIncome;

    /**
     * 
     */
    @JSONField(name = "odoopropertyaccountpayable")
    @JsonProperty("odoopropertyaccountpayable")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyAccountPayable;

    /**
     * 
     */
    @JSONField(name = "odoopropertyaccountreceivable")
    @JsonProperty("odoopropertyaccountreceivable")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyAccountReceivable;

    /**
     * 
     */
    @JSONField(name = "odoopropertystockaccountinputcateg")
    @JsonProperty("odoopropertystockaccountinputcateg")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyStockAccountInputCateg;

    /**
     * 
     */
    @JSONField(name = "odoopropertystockaccountoutputcateg")
    @JsonProperty("odoopropertystockaccountoutputcateg")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyStockAccountOutputCateg;

    /**
     * 
     */
    @JSONField(name = "odoopropertystockvaluationaccount")
    @JsonProperty("odoopropertystockvaluationaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooPropertyStockValuationAccount;

    /**
     * 
     */
    @JSONField(name = "odooparent")
    @JsonProperty("odooparent")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template odooParent;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [# 数字]
     */
    public void setCodeDigits(Integer codeDigits){
        this.codeDigits = codeDigits ;
        this.modify("code_digits",codeDigits);
    }
    /**
     * 设置 [主转账帐户的前缀]
     */
    public void setTransferAccountCodePrefix(String transferAccountCodePrefix){
        this.transferAccountCodePrefix = transferAccountCodePrefix ;
        this.modify("transfer_account_code_prefix",transferAccountCodePrefix);
    }
    /**
     * 设置 [银行科目的前缀]
     */
    public void setBankAccountCodePrefix(String bankAccountCodePrefix){
        this.bankAccountCodePrefix = bankAccountCodePrefix ;
        this.modify("bank_account_code_prefix",bankAccountCodePrefix);
    }
    /**
     * 设置 [口语]
     */
    public void setSpokenLanguages(String spokenLanguages){
        this.spokenLanguages = spokenLanguages ;
        this.modify("spoken_languages",spokenLanguages);
    }
    /**
     * 设置 [可显示？]
     */
    public void setVisible(String visible){
        this.visible = visible ;
        this.modify("visible",visible);
    }
    /**
     * 设置 [税率完整集合]
     */
    public void setCompleteTaxSet(String completeTaxSet){
        this.completeTaxSet = completeTaxSet ;
        this.modify("complete_tax_set",completeTaxSet);
    }
    /**
     * 设置 [主现金科目的前缀]
     */
    public void setCashAccountCodePrefix(String cashAccountCodePrefix){
        this.cashAccountCodePrefix = cashAccountCodePrefix ;
        this.modify("cash_account_code_prefix",cashAccountCodePrefix);
    }
    /**
     * 设置 [使用anglo-saxon会计]
     */
    public void setUseAngloSaxon(String useAngloSaxon){
        this.useAngloSaxon = useAngloSaxon ;
        this.modify("use_anglo_saxon",useAngloSaxon);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [库存计价的科目模板]
     */
    public void setPropertyStockValuationAccountId(Integer propertyStockValuationAccountId){
        this.propertyStockValuationAccountId = propertyStockValuationAccountId ;
        this.modify("property_stock_valuation_account_id",propertyStockValuationAccountId);
    }
    /**
     * 设置 [汇率损失科目]
     */
    public void setExpenseCurrencyExchangeAccountId(Integer expenseCurrencyExchangeAccountId){
        this.expenseCurrencyExchangeAccountId = expenseCurrencyExchangeAccountId ;
        this.modify("expense_currency_exchange_account_id",expenseCurrencyExchangeAccountId);
    }
    /**
     * 设置 [应付科目]
     */
    public void setPropertyAccountPayableId(Integer propertyAccountPayableId){
        this.propertyAccountPayableId = propertyAccountPayableId ;
        this.modify("property_account_payable_id",propertyAccountPayableId);
    }
    /**
     * 设置 [收入科目的类别]
     */
    public void setPropertyAccountIncomeCategId(Integer propertyAccountIncomeCategId){
        this.propertyAccountIncomeCategId = propertyAccountIncomeCategId ;
        this.modify("property_account_income_categ_id",propertyAccountIncomeCategId);
    }
    /**
     * 设置 [库存计价的出货科目]
     */
    public void setPropertyStockAccountOutputCategId(Integer propertyStockAccountOutputCategId){
        this.propertyStockAccountOutputCategId = propertyStockAccountOutputCategId ;
        this.modify("property_stock_account_output_categ_id",propertyStockAccountOutputCategId);
    }
    /**
     * 设置 [产品模板的费用科目]
     */
    public void setPropertyAccountExpenseId(Integer propertyAccountExpenseId){
        this.propertyAccountExpenseId = propertyAccountExpenseId ;
        this.modify("property_account_expense_id",propertyAccountExpenseId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [上级表模板]
     */
    public void setParentId(Integer parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }
    /**
     * 设置 [应收科目]
     */
    public void setPropertyAccountReceivableId(Integer propertyAccountReceivableId){
        this.propertyAccountReceivableId = propertyAccountReceivableId ;
        this.modify("property_account_receivable_id",propertyAccountReceivableId);
    }
    /**
     * 设置 [费用科目的类别]
     */
    public void setPropertyAccountExpenseCategId(Integer propertyAccountExpenseCategId){
        this.propertyAccountExpenseCategId = propertyAccountExpenseCategId ;
        this.modify("property_account_expense_categ_id",propertyAccountExpenseCategId);
    }
    /**
     * 设置 [汇率增益科目]
     */
    public void setIncomeCurrencyExchangeAccountId(Integer incomeCurrencyExchangeAccountId){
        this.incomeCurrencyExchangeAccountId = incomeCurrencyExchangeAccountId ;
        this.modify("income_currency_exchange_account_id",incomeCurrencyExchangeAccountId);
    }
    /**
     * 设置 [库存计价的入库科目]
     */
    public void setPropertyStockAccountInputCategId(Integer propertyStockAccountInputCategId){
        this.propertyStockAccountInputCategId = propertyStockAccountInputCategId ;
        this.modify("property_stock_account_input_categ_id",propertyStockAccountInputCategId);
    }
    /**
     * 设置 [产品模板的收入科目]
     */
    public void setPropertyAccountIncomeId(Integer propertyAccountIncomeId){
        this.propertyAccountIncomeId = propertyAccountIncomeId ;
        this.modify("property_account_income_id",propertyAccountIncomeId);
    }

}


