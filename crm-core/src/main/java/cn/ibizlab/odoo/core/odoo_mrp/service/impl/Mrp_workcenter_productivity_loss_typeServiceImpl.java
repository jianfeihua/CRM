package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivity_loss_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workcenter_productivity_loss_typeFeignClient;

/**
 * 实体[MRP工单生产力损失] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workcenter_productivity_loss_typeServiceImpl implements IMrp_workcenter_productivity_loss_typeService {

    @Autowired
    mrp_workcenter_productivity_loss_typeFeignClient mrp_workcenter_productivity_loss_typeFeignClient;


    @Override
    public boolean update(Mrp_workcenter_productivity_loss_type et) {
        Mrp_workcenter_productivity_loss_type rt = mrp_workcenter_productivity_loss_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_workcenter_productivity_loss_type> list){
        mrp_workcenter_productivity_loss_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mrp_workcenter_productivity_loss_type et) {
        Mrp_workcenter_productivity_loss_type rt = mrp_workcenter_productivity_loss_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workcenter_productivity_loss_type> list){
        mrp_workcenter_productivity_loss_typeFeignClient.createBatch(list) ;
    }

    @Override
    public Mrp_workcenter_productivity_loss_type getDraft(Mrp_workcenter_productivity_loss_type et) {
        et=mrp_workcenter_productivity_loss_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public Mrp_workcenter_productivity_loss_type get(Integer id) {
		Mrp_workcenter_productivity_loss_type et=mrp_workcenter_productivity_loss_typeFeignClient.get(id);
        if(et==null){
            et=new Mrp_workcenter_productivity_loss_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_workcenter_productivity_loss_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_workcenter_productivity_loss_typeFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workcenter_productivity_loss_type> searchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context) {
        Page<Mrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types=mrp_workcenter_productivity_loss_typeFeignClient.searchDefault(context);
        return mrp_workcenter_productivity_loss_types;
    }


}


