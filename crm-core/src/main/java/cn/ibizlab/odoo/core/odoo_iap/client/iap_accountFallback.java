package cn.ibizlab.odoo.core.odoo_iap.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[iap_account] 服务对象接口
 */
@Component
public class iap_accountFallback implements iap_accountFeignClient{

    public Page<Iap_account> searchDefault(Iap_accountSearchContext context){
            return null;
     }



    public Iap_account create(Iap_account iap_account){
            return null;
     }
    public Boolean createBatch(List<Iap_account> iap_accounts){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Iap_account update(Integer id, Iap_account iap_account){
            return null;
     }
    public Boolean updateBatch(List<Iap_account> iap_accounts){
            return false;
     }


    public Iap_account get(Integer id){
            return null;
     }


    public Page<Iap_account> select(){
            return null;
     }

    public Iap_account getDraft(){
            return null;
    }



}
