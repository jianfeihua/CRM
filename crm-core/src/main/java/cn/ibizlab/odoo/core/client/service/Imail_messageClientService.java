package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_message] 服务对象接口
 */
public interface Imail_messageClientService{

    public Imail_message createModel() ;

    public void remove(Imail_message mail_message);

    public void updateBatch(List<Imail_message> mail_messages);

    public void create(Imail_message mail_message);

    public void createBatch(List<Imail_message> mail_messages);

    public Page<Imail_message> fetchDefault(SearchContext context);

    public void get(Imail_message mail_message);

    public void removeBatch(List<Imail_message> mail_messages);

    public void update(Imail_message mail_message);

    public Page<Imail_message> select(SearchContext context);

    public void checkKey(Imail_message mail_message);

    public void getDraft(Imail_message mail_message);

    public void save(Imail_message mail_message);

}
