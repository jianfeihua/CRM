package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_routing_workcenter;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_routing_workcenter] 服务对象接口
 */
public interface Imrp_routing_workcenterClientService{

    public Imrp_routing_workcenter createModel() ;

    public void createBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters);

    public void remove(Imrp_routing_workcenter mrp_routing_workcenter);

    public void get(Imrp_routing_workcenter mrp_routing_workcenter);

    public void updateBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters);

    public void removeBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters);

    public Page<Imrp_routing_workcenter> fetchDefault(SearchContext context);

    public void update(Imrp_routing_workcenter mrp_routing_workcenter);

    public void create(Imrp_routing_workcenter mrp_routing_workcenter);

    public Page<Imrp_routing_workcenter> select(SearchContext context);

    public void getDraft(Imrp_routing_workcenter mrp_routing_workcenter);

}
