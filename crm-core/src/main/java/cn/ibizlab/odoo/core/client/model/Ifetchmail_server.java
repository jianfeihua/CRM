package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [fetchmail_server] 对象
 */
public interface Ifetchmail_server {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [保存附件]
     */
    public void setAttach(String attach);
    
    /**
     * 设置 [保存附件]
     */
    public String getAttach();

    /**
     * 获取 [保存附件]脏标记
     */
    public boolean getAttachDirtyFlag();
    /**
     * 获取 [配置]
     */
    public void setConfiguration(String configuration);
    
    /**
     * 设置 [配置]
     */
    public String getConfiguration();

    /**
     * 获取 [配置]脏标记
     */
    public boolean getConfigurationDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [最后收取日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [最后收取日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [最后收取日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [SSL/TLS]
     */
    public void setIs_ssl(String is_ssl);
    
    /**
     * 设置 [SSL/TLS]
     */
    public String getIs_ssl();

    /**
     * 获取 [SSL/TLS]脏标记
     */
    public boolean getIs_sslDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [创建新记录]
     */
    public void setObject_id(Integer object_id);
    
    /**
     * 设置 [创建新记录]
     */
    public Integer getObject_id();

    /**
     * 获取 [创建新记录]脏标记
     */
    public boolean getObject_idDirtyFlag();
    /**
     * 获取 [保留原件]
     */
    public void setOriginal(String original);
    
    /**
     * 设置 [保留原件]
     */
    public String getOriginal();

    /**
     * 获取 [保留原件]脏标记
     */
    public boolean getOriginalDirtyFlag();
    /**
     * 获取 [密码]
     */
    public void setPassword(String password);
    
    /**
     * 设置 [密码]
     */
    public String getPassword();

    /**
     * 获取 [密码]脏标记
     */
    public boolean getPasswordDirtyFlag();
    /**
     * 获取 [端口]
     */
    public void setPort(Integer port);
    
    /**
     * 设置 [端口]
     */
    public Integer getPort();

    /**
     * 获取 [端口]脏标记
     */
    public boolean getPortDirtyFlag();
    /**
     * 获取 [服务器优先级]
     */
    public void setPriority(Integer priority);
    
    /**
     * 设置 [服务器优先级]
     */
    public Integer getPriority();

    /**
     * 获取 [服务器优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [脚本]
     */
    public void setScript(String script);
    
    /**
     * 设置 [脚本]
     */
    public String getScript();

    /**
     * 获取 [脚本]脏标记
     */
    public boolean getScriptDirtyFlag();
    /**
     * 获取 [服务器名称]
     */
    public void setServer(String server);
    
    /**
     * 设置 [服务器名称]
     */
    public String getServer();

    /**
     * 获取 [服务器名称]脏标记
     */
    public boolean getServerDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [服务器类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [服务器类型]
     */
    public String getType();

    /**
     * 获取 [服务器类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [用户名]
     */
    public void setUser(String user);
    
    /**
     * 设置 [用户名]
     */
    public String getUser();

    /**
     * 获取 [用户名]脏标记
     */
    public boolean getUserDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
