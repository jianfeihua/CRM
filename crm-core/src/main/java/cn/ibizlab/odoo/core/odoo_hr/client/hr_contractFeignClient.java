package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contractSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_contract] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-contract", fallback = hr_contractFallback.class)
public interface hr_contractFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/hr_contracts/searchdefault")
    Page<Hr_contract> searchDefault(@RequestBody Hr_contractSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_contracts/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_contracts/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_contracts/{id}")
    Hr_contract get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/hr_contracts")
    Hr_contract create(@RequestBody Hr_contract hr_contract);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_contracts/batch")
    Boolean createBatch(@RequestBody List<Hr_contract> hr_contracts);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_contracts/{id}")
    Hr_contract update(@PathVariable("id") Integer id,@RequestBody Hr_contract hr_contract);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_contracts/batch")
    Boolean updateBatch(@RequestBody List<Hr_contract> hr_contracts);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_contracts/select")
    Page<Hr_contract> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_contracts/getdraft")
    Hr_contract getDraft();


}
