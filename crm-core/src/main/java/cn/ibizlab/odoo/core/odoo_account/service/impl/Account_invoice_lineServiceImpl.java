package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_lineFeignClient;

/**
 * 实体[发票行] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_lineServiceImpl implements IAccount_invoice_lineService {

    @Autowired
    account_invoice_lineFeignClient account_invoice_lineFeignClient;


    @Override
    public boolean update(Account_invoice_line et) {
        Account_invoice_line rt = account_invoice_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice_line> list){
        account_invoice_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean checkKey(Account_invoice_line et) {
        return account_invoice_lineFeignClient.checkKey(et);
    }
    @Override
    public Account_invoice_line get(Integer id) {
		Account_invoice_line et=account_invoice_lineFeignClient.get(id);
        if(et==null){
            et=new Account_invoice_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_invoice_line getDraft(Account_invoice_line et) {
        et=account_invoice_lineFeignClient.getDraft();
        return et;
    }

    @Override
    @Transactional
    public boolean save(Account_invoice_line et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!account_invoice_lineFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Account_invoice_line> list) {
        account_invoice_lineFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoice_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoice_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_invoice_line et) {
        Account_invoice_line rt = account_invoice_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_line> list){
        account_invoice_lineFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_line> searchDefault(Account_invoice_lineSearchContext context) {
        Page<Account_invoice_line> account_invoice_lines=account_invoice_lineFeignClient.searchDefault(context);
        return account_invoice_lines;
    }


}


