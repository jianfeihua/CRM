package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_templateFeignClient;

/**
 * 实体[产品模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_templateServiceImpl implements IProduct_templateService {

    @Autowired
    product_templateFeignClient product_templateFeignClient;


    @Override
    public boolean create(Product_template et) {
        Product_template rt = product_templateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_template> list){
        product_templateFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_templateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_templateFeignClient.removeBatch(idList);
    }

    @Override
    public Product_template get(Integer id) {
		Product_template et=product_templateFeignClient.get(id);
        if(et==null){
            et=new Product_template();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Product_template getDraft(Product_template et) {
        et=product_templateFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Product_template et) {
        Product_template rt = product_templateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_template> list){
        product_templateFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_template> searchDefault(Product_templateSearchContext context) {
        Page<Product_template> product_templates=product_templateFeignClient.searchDefault(context);
        return product_templates;
    }


}


