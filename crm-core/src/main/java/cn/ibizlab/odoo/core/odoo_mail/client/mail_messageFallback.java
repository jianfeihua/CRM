package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_message] 服务对象接口
 */
@Component
public class mail_messageFallback implements mail_messageFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Mail_message create(Mail_message mail_message){
            return null;
     }
    public Boolean createBatch(List<Mail_message> mail_messages){
            return false;
     }


    public Page<Mail_message> searchDefault(Mail_messageSearchContext context){
            return null;
     }


    public Mail_message get(Integer id){
            return null;
     }



    public Mail_message update(Integer id, Mail_message mail_message){
            return null;
     }
    public Boolean updateBatch(List<Mail_message> mail_messages){
            return false;
     }


    public Page<Mail_message> select(){
            return null;
     }

    public Boolean checkKey(Mail_message mail_message){
            return false;
     }


    public Mail_message getDraft(){
            return null;
    }



    public Boolean save(Mail_message mail_message){
            return false;
     }
    public Boolean saveBatch(List<Mail_message> mail_messages){
            return false;
     }

}
