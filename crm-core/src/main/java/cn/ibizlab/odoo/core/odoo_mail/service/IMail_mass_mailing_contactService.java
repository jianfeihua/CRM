package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;


/**
 * 实体[Mail_mass_mailing_contact] 服务对象接口
 */
public interface IMail_mass_mailing_contactService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_mass_mailing_contact get(Integer key) ;
    boolean update(Mail_mass_mailing_contact et) ;
    void updateBatch(List<Mail_mass_mailing_contact> list) ;
    Mail_mass_mailing_contact getDraft(Mail_mass_mailing_contact et) ;
    boolean create(Mail_mass_mailing_contact et) ;
    void createBatch(List<Mail_mass_mailing_contact> list) ;
    Page<Mail_mass_mailing_contact> searchDefault(Mail_mass_mailing_contactSearchContext context) ;

}



