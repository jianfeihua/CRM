package cn.ibizlab.odoo.core.odoo_web_editor.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;


/**
 * 实体[Web_editor_converter_test] 服务对象接口
 */
public interface IWeb_editor_converter_testService{

    boolean create(Web_editor_converter_test et) ;
    void createBatch(List<Web_editor_converter_test> list) ;
    Web_editor_converter_test get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Web_editor_converter_test getDraft(Web_editor_converter_test et) ;
    boolean update(Web_editor_converter_test et) ;
    void updateBatch(List<Web_editor_converter_test> list) ;
    Page<Web_editor_converter_test> searchDefault(Web_editor_converter_testSearchContext context) ;

}



