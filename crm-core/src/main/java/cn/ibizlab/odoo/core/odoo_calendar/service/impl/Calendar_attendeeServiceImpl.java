package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_attendeeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_attendeeFeignClient;

/**
 * 实体[日历出席者信息] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_attendeeServiceImpl implements ICalendar_attendeeService {

    @Autowired
    calendar_attendeeFeignClient calendar_attendeeFeignClient;


    @Override
    public Calendar_attendee getDraft(Calendar_attendee et) {
        et=calendar_attendeeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Calendar_attendee et) {
        Calendar_attendee rt = calendar_attendeeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Calendar_attendee> list){
        calendar_attendeeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=calendar_attendeeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        calendar_attendeeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Calendar_attendee et) {
        Calendar_attendee rt = calendar_attendeeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_attendee> list){
        calendar_attendeeFeignClient.createBatch(list) ;
    }

    @Override
    public Calendar_attendee get(Integer id) {
		Calendar_attendee et=calendar_attendeeFeignClient.get(id);
        if(et==null){
            et=new Calendar_attendee();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_attendee> searchDefault(Calendar_attendeeSearchContext context) {
        Page<Calendar_attendee> calendar_attendees=calendar_attendeeFeignClient.searchDefault(context);
        return calendar_attendees;
    }


}


