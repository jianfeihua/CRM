package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[repair_cancel] 服务对象接口
 */
@Component
public class repair_cancelFallback implements repair_cancelFeignClient{

    public Repair_cancel create(Repair_cancel repair_cancel){
            return null;
     }
    public Boolean createBatch(List<Repair_cancel> repair_cancels){
            return false;
     }


    public Repair_cancel get(Integer id){
            return null;
     }


    public Page<Repair_cancel> searchDefault(Repair_cancelSearchContext context){
            return null;
     }


    public Repair_cancel update(Integer id, Repair_cancel repair_cancel){
            return null;
     }
    public Boolean updateBatch(List<Repair_cancel> repair_cancels){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Repair_cancel> select(){
            return null;
     }

    public Repair_cancel getDraft(){
            return null;
    }



}
