package cn.ibizlab.odoo.core.odoo_note.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_stage;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_note.service.INote_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_note.client.note_stageFeignClient;

/**
 * 实体[便签阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Note_stageServiceImpl implements INote_stageService {

    @Autowired
    note_stageFeignClient note_stageFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=note_stageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        note_stageFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Note_stage et) {
        Note_stage rt = note_stageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Note_stage> list){
        note_stageFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Note_stage et) {
        Note_stage rt = note_stageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Note_stage> list){
        note_stageFeignClient.updateBatch(list) ;
    }

    @Override
    public Note_stage getDraft(Note_stage et) {
        et=note_stageFeignClient.getDraft();
        return et;
    }

    @Override
    public Note_stage get(Integer id) {
		Note_stage et=note_stageFeignClient.get(id);
        if(et==null){
            et=new Note_stage();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Note_stage> searchDefault(Note_stageSearchContext context) {
        Page<Note_stage> note_stages=note_stageFeignClient.searchDefault(context);
        return note_stages;
    }


}


