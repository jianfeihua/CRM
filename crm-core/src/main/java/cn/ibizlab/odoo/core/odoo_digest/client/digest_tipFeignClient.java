package cn.ibizlab.odoo.core.odoo_digest.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[digest_tip] 服务对象接口
 */
@FeignClient(value = "odoo-digest", contextId = "digest-tip", fallback = digest_tipFallback.class)
public interface digest_tipFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/digest_tips/searchdefault")
    Page<Digest_tip> searchDefault(@RequestBody Digest_tipSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/digest_tips/{id}")
    Digest_tip get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/digest_tips/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/digest_tips/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/digest_tips/{id}")
    Digest_tip update(@PathVariable("id") Integer id,@RequestBody Digest_tip digest_tip);

    @RequestMapping(method = RequestMethod.PUT, value = "/digest_tips/batch")
    Boolean updateBatch(@RequestBody List<Digest_tip> digest_tips);


    @RequestMapping(method = RequestMethod.POST, value = "/digest_tips")
    Digest_tip create(@RequestBody Digest_tip digest_tip);

    @RequestMapping(method = RequestMethod.POST, value = "/digest_tips/batch")
    Boolean createBatch(@RequestBody List<Digest_tip> digest_tips);



    @RequestMapping(method = RequestMethod.GET, value = "/digest_tips/select")
    Page<Digest_tip> select();


    @RequestMapping(method = RequestMethod.GET, value = "/digest_tips/getdraft")
    Digest_tip getDraft();


}
