package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_replenish] 服务对象接口
 */
@Component
public class product_replenishFallback implements product_replenishFeignClient{




    public Page<Product_replenish> searchDefault(Product_replenishSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Product_replenish update(Integer id, Product_replenish product_replenish){
            return null;
     }
    public Boolean updateBatch(List<Product_replenish> product_replenishes){
            return false;
     }


    public Product_replenish get(Integer id){
            return null;
     }


    public Product_replenish create(Product_replenish product_replenish){
            return null;
     }
    public Boolean createBatch(List<Product_replenish> product_replenishes){
            return false;
     }

    public Page<Product_replenish> select(){
            return null;
     }

    public Product_replenish getDraft(){
            return null;
    }



}
