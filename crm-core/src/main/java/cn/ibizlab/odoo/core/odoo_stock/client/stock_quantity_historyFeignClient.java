package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantity_historySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_quantity_history] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-quantity-history", fallback = stock_quantity_historyFallback.class)
public interface stock_quantity_historyFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_quantity_histories/{id}")
    Stock_quantity_history update(@PathVariable("id") Integer id,@RequestBody Stock_quantity_history stock_quantity_history);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_quantity_histories/batch")
    Boolean updateBatch(@RequestBody List<Stock_quantity_history> stock_quantity_histories);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories")
    Stock_quantity_history create(@RequestBody Stock_quantity_history stock_quantity_history);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories/batch")
    Boolean createBatch(@RequestBody List<Stock_quantity_history> stock_quantity_histories);





    @RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories/searchdefault")
    Page<Stock_quantity_history> searchDefault(@RequestBody Stock_quantity_historySearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_quantity_histories/{id}")
    Stock_quantity_history get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_quantity_histories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_quantity_histories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_quantity_histories/select")
    Page<Stock_quantity_history> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_quantity_histories/getdraft")
    Stock_quantity_history getDraft();


}
