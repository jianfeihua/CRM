package cn.ibizlab.odoo.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[utm_source] 服务对象接口
 */
@Component
public class utm_sourceFallback implements utm_sourceFeignClient{



    public Utm_source create(Utm_source utm_source){
            return null;
     }
    public Boolean createBatch(List<Utm_source> utm_sources){
            return false;
     }

    public Utm_source get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Utm_source> searchDefault(Utm_sourceSearchContext context){
            return null;
     }


    public Utm_source update(Integer id, Utm_source utm_source){
            return null;
     }
    public Boolean updateBatch(List<Utm_source> utm_sources){
            return false;
     }


    public Page<Utm_source> select(){
            return null;
     }

    public Utm_source getDraft(){
            return null;
    }



}
