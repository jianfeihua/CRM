package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expenseSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expenseService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_expenseFeignClient;

/**
 * 实体[费用] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_expenseServiceImpl implements IHr_expenseService {

    @Autowired
    hr_expenseFeignClient hr_expenseFeignClient;


    @Override
    public boolean update(Hr_expense et) {
        Hr_expense rt = hr_expenseFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_expense> list){
        hr_expenseFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_expenseFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_expenseFeignClient.removeBatch(idList);
    }

    @Override
    public Hr_expense get(Integer id) {
		Hr_expense et=hr_expenseFeignClient.get(id);
        if(et==null){
            et=new Hr_expense();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Hr_expense et) {
        Hr_expense rt = hr_expenseFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_expense> list){
        hr_expenseFeignClient.createBatch(list) ;
    }

    @Override
    public Hr_expense getDraft(Hr_expense et) {
        et=hr_expenseFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_expense> searchDefault(Hr_expenseSearchContext context) {
        Page<Hr_expense> hr_expenses=hr_expenseFeignClient.searchDefault(context);
        return hr_expenses;
    }


}


