package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_image;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_imageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_image] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-image", fallback = product_imageFallback.class)
public interface product_imageFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/product_images")
    Product_image create(@RequestBody Product_image product_image);

    @RequestMapping(method = RequestMethod.POST, value = "/product_images/batch")
    Boolean createBatch(@RequestBody List<Product_image> product_images);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_images/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_images/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_images/{id}")
    Product_image update(@PathVariable("id") Integer id,@RequestBody Product_image product_image);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_images/batch")
    Boolean updateBatch(@RequestBody List<Product_image> product_images);




    @RequestMapping(method = RequestMethod.POST, value = "/product_images/searchdefault")
    Page<Product_image> searchDefault(@RequestBody Product_imageSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/product_images/{id}")
    Product_image get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/product_images/select")
    Page<Product_image> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_images/getdraft")
    Product_image getDraft();


}
