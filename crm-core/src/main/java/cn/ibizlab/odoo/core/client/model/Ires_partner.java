package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_partner] 对象
 */
public interface Ires_partner {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一个活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一个活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一个活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [附加信息]
     */
    public void setAdditional_info(String additional_info);
    
    /**
     * 设置 [附加信息]
     */
    public String getAdditional_info();

    /**
     * 获取 [附加信息]脏标记
     */
    public boolean getAdditional_infoDirtyFlag();
    /**
     * 获取 [银行]
     */
    public void setBank_account_count(Integer bank_account_count);
    
    /**
     * 设置 [银行]
     */
    public Integer getBank_account_count();

    /**
     * 获取 [银行]脏标记
     */
    public boolean getBank_account_countDirtyFlag();
    /**
     * 获取 [银行]
     */
    public void setBank_ids(String bank_ids);
    
    /**
     * 设置 [银行]
     */
    public String getBank_ids();

    /**
     * 获取 [银行]脏标记
     */
    public boolean getBank_idsDirtyFlag();
    /**
     * 获取 [条码]
     */
    public void setBarcode(String barcode);
    
    /**
     * 设置 [条码]
     */
    public String getBarcode();

    /**
     * 获取 [条码]脏标记
     */
    public boolean getBarcodeDirtyFlag();
    /**
     * 获取 [最后的提醒已经标志为已读]
     */
    public void setCalendar_last_notif_ack(Timestamp calendar_last_notif_ack);
    
    /**
     * 设置 [最后的提醒已经标志为已读]
     */
    public Timestamp getCalendar_last_notif_ack();

    /**
     * 获取 [最后的提醒已经标志为已读]脏标记
     */
    public boolean getCalendar_last_notif_ackDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setCategory_id(String category_id);
    
    /**
     * 设置 [标签]
     */
    public String getCategory_id();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setChannel_ids(String channel_ids);
    
    /**
     * 设置 [渠道]
     */
    public String getChannel_ids();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getChannel_idsDirtyFlag();
    /**
     * 获取 [联系人]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [联系人]
     */
    public String getChild_ids();

    /**
     * 获取 [联系人]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [城市]
     */
    public void setCity(String city);
    
    /**
     * 设置 [城市]
     */
    public String getCity();

    /**
     * 获取 [城市]脏标记
     */
    public boolean getCityDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [便签]
     */
    public void setComment(String comment);
    
    /**
     * 设置 [便签]
     */
    public String getComment();

    /**
     * 获取 [便签]脏标记
     */
    public boolean getCommentDirtyFlag();
    /**
     * 获取 [公司名称实体]
     */
    public void setCommercial_company_name(String commercial_company_name);
    
    /**
     * 设置 [公司名称实体]
     */
    public String getCommercial_company_name();

    /**
     * 获取 [公司名称实体]脏标记
     */
    public boolean getCommercial_company_nameDirtyFlag();
    /**
     * 获取 [商业实体]
     */
    public void setCommercial_partner_id(Integer commercial_partner_id);
    
    /**
     * 设置 [商业实体]
     */
    public Integer getCommercial_partner_id();

    /**
     * 获取 [商业实体]脏标记
     */
    public boolean getCommercial_partner_idDirtyFlag();
    /**
     * 获取 [商业实体]
     */
    public void setCommercial_partner_id_text(String commercial_partner_id_text);
    
    /**
     * 设置 [商业实体]
     */
    public String getCommercial_partner_id_text();

    /**
     * 获取 [商业实体]脏标记
     */
    public boolean getCommercial_partner_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [公司名称]
     */
    public void setCompany_name(String company_name);
    
    /**
     * 设置 [公司名称]
     */
    public String getCompany_name();

    /**
     * 获取 [公司名称]脏标记
     */
    public boolean getCompany_nameDirtyFlag();
    /**
     * 获取 [公司类别]
     */
    public void setCompany_type(String company_type);
    
    /**
     * 设置 [公司类别]
     */
    public String getCompany_type();

    /**
     * 获取 [公司类别]脏标记
     */
    public boolean getCompany_typeDirtyFlag();
    /**
     * 获取 [完整地址]
     */
    public void setContact_address(String contact_address);
    
    /**
     * 设置 [完整地址]
     */
    public String getContact_address();

    /**
     * 获取 [完整地址]脏标记
     */
    public boolean getContact_addressDirtyFlag();
    /**
     * 获取 [合同统计]
     */
    public void setContracts_count(Integer contracts_count);
    
    /**
     * 设置 [合同统计]
     */
    public Integer getContracts_count();

    /**
     * 获取 [合同统计]脏标记
     */
    public boolean getContracts_countDirtyFlag();
    /**
     * 获取 [客户合同]
     */
    public void setContract_ids(String contract_ids);
    
    /**
     * 设置 [客户合同]
     */
    public String getContract_ids();

    /**
     * 获取 [客户合同]脏标记
     */
    public boolean getContract_idsDirtyFlag();
    /**
     * 获取 [国家/地区]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [国家/地区]
     */
    public Integer getCountry_id();

    /**
     * 获取 [国家/地区]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [国家/地区]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [国家/地区]
     */
    public String getCountry_id_text();

    /**
     * 获取 [国家/地区]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [应收总计]
     */
    public void setCredit(Double credit);
    
    /**
     * 设置 [应收总计]
     */
    public Double getCredit();

    /**
     * 获取 [应收总计]脏标记
     */
    public boolean getCreditDirtyFlag();
    /**
     * 获取 [信用额度]
     */
    public void setCredit_limit(Double credit_limit);
    
    /**
     * 设置 [信用额度]
     */
    public Double getCredit_limit();

    /**
     * 获取 [信用额度]脏标记
     */
    public boolean getCredit_limitDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setCustomer(String customer);
    
    /**
     * 设置 [客户]
     */
    public String getCustomer();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getCustomerDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [应付总计]
     */
    public void setDebit(Double debit);
    
    /**
     * 设置 [应付总计]
     */
    public Double getDebit();

    /**
     * 获取 [应付总计]脏标记
     */
    public boolean getDebitDirtyFlag();
    /**
     * 获取 [应付限额]
     */
    public void setDebit_limit(Double debit_limit);
    
    /**
     * 设置 [应付限额]
     */
    public Double getDebit_limit();

    /**
     * 获取 [应付限额]脏标记
     */
    public boolean getDebit_limitDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail(String email);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmailDirtyFlag();
    /**
     * 获取 [格式化的邮件]
     */
    public void setEmail_formatted(String email_formatted);
    
    /**
     * 设置 [格式化的邮件]
     */
    public String getEmail_formatted();

    /**
     * 获取 [格式化的邮件]脏标记
     */
    public boolean getEmail_formattedDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee(String employee);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployeeDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setEvent_count(Integer event_count);
    
    /**
     * 设置 [活动]
     */
    public Integer getEvent_count();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getEvent_countDirtyFlag();
    /**
     * 获取 [有未核销的分录]
     */
    public void setHas_unreconciled_entries(String has_unreconciled_entries);
    
    /**
     * 设置 [有未核销的分录]
     */
    public String getHas_unreconciled_entries();

    /**
     * 获取 [有未核销的分录]脏标记
     */
    public boolean getHas_unreconciled_entriesDirtyFlag();
    /**
     * 获取 [工作岗位]
     */
    public void setIbizfunction(String ibizfunction);
    
    /**
     * 设置 [工作岗位]
     */
    public String getIbizfunction();

    /**
     * 获取 [工作岗位]脏标记
     */
    public boolean getIbizfunctionDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getImage();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [中等尺寸图像]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [中等尺寸图像]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [小尺寸图像]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [小尺寸图像]
     */
    public byte[] getImage_small();

    /**
     * 获取 [小尺寸图像]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [IM的状态]
     */
    public void setIm_status(String im_status);
    
    /**
     * 设置 [IM的状态]
     */
    public String getIm_status();

    /**
     * 获取 [IM的状态]脏标记
     */
    public boolean getIm_statusDirtyFlag();
    /**
     * 获取 [工业]
     */
    public void setIndustry_id(Integer industry_id);
    
    /**
     * 设置 [工业]
     */
    public Integer getIndustry_id();

    /**
     * 获取 [工业]脏标记
     */
    public boolean getIndustry_idDirtyFlag();
    /**
     * 获取 [工业]
     */
    public void setIndustry_id_text(String industry_id_text);
    
    /**
     * 设置 [工业]
     */
    public String getIndustry_id_text();

    /**
     * 获取 [工业]脏标记
     */
    public boolean getIndustry_id_textDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_ids(String invoice_ids);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_ids();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idsDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_warn(String invoice_warn);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_warn();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_warnDirtyFlag();
    /**
     * 获取 [发票消息]
     */
    public void setInvoice_warn_msg(String invoice_warn_msg);
    
    /**
     * 设置 [发票消息]
     */
    public String getInvoice_warn_msg();

    /**
     * 获取 [发票消息]脏标记
     */
    public boolean getInvoice_warn_msgDirtyFlag();
    /**
     * 获取 [黑名单]
     */
    public void setIs_blacklisted(String is_blacklisted);
    
    /**
     * 设置 [黑名单]
     */
    public String getIs_blacklisted();

    /**
     * 获取 [黑名单]脏标记
     */
    public boolean getIs_blacklistedDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setIs_company(String is_company);
    
    /**
     * 设置 [公司]
     */
    public String getIs_company();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getIs_companyDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setIs_published(String is_published);
    
    /**
     * 设置 [已发布]
     */
    public String getIs_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getIs_publishedDirtyFlag();
    /**
     * 获取 [SEO优化]
     */
    public void setIs_seo_optimized(String is_seo_optimized);
    
    /**
     * 设置 [SEO优化]
     */
    public String getIs_seo_optimized();

    /**
     * 获取 [SEO优化]脏标记
     */
    public boolean getIs_seo_optimizedDirtyFlag();
    /**
     * 获取 [日记账项目]
     */
    public void setJournal_item_count(Integer journal_item_count);
    
    /**
     * 设置 [日记账项目]
     */
    public Integer getJournal_item_count();

    /**
     * 获取 [日记账项目]脏标记
     */
    public boolean getJournal_item_countDirtyFlag();
    /**
     * 获取 [语言]
     */
    public void setLang(String lang);
    
    /**
     * 设置 [语言]
     */
    public String getLang();

    /**
     * 获取 [语言]脏标记
     */
    public boolean getLangDirtyFlag();
    /**
     * 获取 [最近的发票和付款匹配时间]
     */
    public void setLast_time_entries_checked(Timestamp last_time_entries_checked);
    
    /**
     * 设置 [最近的发票和付款匹配时间]
     */
    public Timestamp getLast_time_entries_checked();

    /**
     * 获取 [最近的发票和付款匹配时间]脏标记
     */
    public boolean getLast_time_entries_checkedDirtyFlag();
    /**
     * 获取 [最近的在线销售订单]
     */
    public void setLast_website_so_id(Integer last_website_so_id);
    
    /**
     * 设置 [最近的在线销售订单]
     */
    public Integer getLast_website_so_id();

    /**
     * 获取 [最近的在线销售订单]脏标记
     */
    public boolean getLast_website_so_idDirtyFlag();
    /**
     * 获取 [#会议]
     */
    public void setMeeting_count(Integer meeting_count);
    
    /**
     * 设置 [#会议]
     */
    public Integer getMeeting_count();

    /**
     * 获取 [#会议]脏标记
     */
    public boolean getMeeting_countDirtyFlag();
    /**
     * 获取 [会议]
     */
    public void setMeeting_ids(String meeting_ids);
    
    /**
     * 设置 [会议]
     */
    public String getMeeting_ids();

    /**
     * 获取 [会议]脏标记
     */
    public boolean getMeeting_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [退回]
     */
    public void setMessage_bounce(Integer message_bounce);
    
    /**
     * 设置 [退回]
     */
    public Integer getMessage_bounce();

    /**
     * 获取 [退回]脏标记
     */
    public boolean getMessage_bounceDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误个数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误个数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误个数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [前置操作]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [前置操作]
     */
    public String getMessage_needaction();

    /**
     * 获取 [前置操作]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [操作次数]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [操作次数]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [操作次数]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [手机]
     */
    public void setMobile(String mobile);
    
    /**
     * 设置 [手机]
     */
    public String getMobile();

    /**
     * 获取 [手机]脏标记
     */
    public boolean getMobileDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setOpportunity_count(Integer opportunity_count);
    
    /**
     * 设置 [商机]
     */
    public Integer getOpportunity_count();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getOpportunity_countDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setOpportunity_ids(String opportunity_ids);
    
    /**
     * 设置 [商机]
     */
    public String getOpportunity_ids();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getOpportunity_idsDirtyFlag();
    /**
     * 获取 [关联公司]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [关联公司]
     */
    public Integer getParent_id();

    /**
     * 获取 [关联公司]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级名称]
     */
    public void setParent_name(String parent_name);
    
    /**
     * 设置 [上级名称]
     */
    public String getParent_name();

    /**
     * 获取 [上级名称]脏标记
     */
    public boolean getParent_nameDirtyFlag();
    /**
     * 获取 [公司数据库ID]
     */
    public void setPartner_gid(Integer partner_gid);
    
    /**
     * 设置 [公司数据库ID]
     */
    public Integer getPartner_gid();

    /**
     * 获取 [公司数据库ID]脏标记
     */
    public boolean getPartner_gidDirtyFlag();
    /**
     * 获取 [共享合作伙伴]
     */
    public void setPartner_share(String partner_share);
    
    /**
     * 设置 [共享合作伙伴]
     */
    public String getPartner_share();

    /**
     * 获取 [共享合作伙伴]脏标记
     */
    public boolean getPartner_shareDirtyFlag();
    /**
     * 获取 [付款令牌计数]
     */
    public void setPayment_token_count(Integer payment_token_count);
    
    /**
     * 设置 [付款令牌计数]
     */
    public Integer getPayment_token_count();

    /**
     * 获取 [付款令牌计数]脏标记
     */
    public boolean getPayment_token_countDirtyFlag();
    /**
     * 获取 [付款令牌]
     */
    public void setPayment_token_ids(String payment_token_ids);
    
    /**
     * 设置 [付款令牌]
     */
    public String getPayment_token_ids();

    /**
     * 获取 [付款令牌]脏标记
     */
    public boolean getPayment_token_idsDirtyFlag();
    /**
     * 获取 [电话]
     */
    public void setPhone(String phone);
    
    /**
     * 设置 [电话]
     */
    public String getPhone();

    /**
     * 获取 [电话]脏标记
     */
    public boolean getPhoneDirtyFlag();
    /**
     * 获取 [库存拣货]
     */
    public void setPicking_warn(String picking_warn);
    
    /**
     * 设置 [库存拣货]
     */
    public String getPicking_warn();

    /**
     * 获取 [库存拣货]脏标记
     */
    public boolean getPicking_warnDirtyFlag();
    /**
     * 获取 [库存拣货单消息]
     */
    public void setPicking_warn_msg(String picking_warn_msg);
    
    /**
     * 设置 [库存拣货单消息]
     */
    public String getPicking_warn_msg();

    /**
     * 获取 [库存拣货单消息]脏标记
     */
    public boolean getPicking_warn_msgDirtyFlag();
    /**
     * 获取 [销售点订单计数]
     */
    public void setPos_order_count(Integer pos_order_count);
    
    /**
     * 设置 [销售点订单计数]
     */
    public Integer getPos_order_count();

    /**
     * 获取 [销售点订单计数]脏标记
     */
    public boolean getPos_order_countDirtyFlag();
    /**
     * 获取 [应付账款]
     */
    public void setProperty_account_payable_id(Integer property_account_payable_id);
    
    /**
     * 设置 [应付账款]
     */
    public Integer getProperty_account_payable_id();

    /**
     * 获取 [应付账款]脏标记
     */
    public boolean getProperty_account_payable_idDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setProperty_account_position_id(Integer property_account_position_id);
    
    /**
     * 设置 [税科目调整]
     */
    public Integer getProperty_account_position_id();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getProperty_account_position_idDirtyFlag();
    /**
     * 获取 [应收账款]
     */
    public void setProperty_account_receivable_id(Integer property_account_receivable_id);
    
    /**
     * 设置 [应收账款]
     */
    public Integer getProperty_account_receivable_id();

    /**
     * 获取 [应收账款]脏标记
     */
    public boolean getProperty_account_receivable_idDirtyFlag();
    /**
     * 获取 [客户付款条款]
     */
    public void setProperty_payment_term_id(Integer property_payment_term_id);
    
    /**
     * 设置 [客户付款条款]
     */
    public Integer getProperty_payment_term_id();

    /**
     * 获取 [客户付款条款]脏标记
     */
    public boolean getProperty_payment_term_idDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setProperty_product_pricelist(Integer property_product_pricelist);
    
    /**
     * 设置 [价格表]
     */
    public Integer getProperty_product_pricelist();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getProperty_product_pricelistDirtyFlag();
    /**
     * 获取 [供应商货币]
     */
    public void setProperty_purchase_currency_id(Integer property_purchase_currency_id);
    
    /**
     * 设置 [供应商货币]
     */
    public Integer getProperty_purchase_currency_id();

    /**
     * 获取 [供应商货币]脏标记
     */
    public boolean getProperty_purchase_currency_idDirtyFlag();
    /**
     * 获取 [客户位置]
     */
    public void setProperty_stock_customer(Integer property_stock_customer);
    
    /**
     * 设置 [客户位置]
     */
    public Integer getProperty_stock_customer();

    /**
     * 获取 [客户位置]脏标记
     */
    public boolean getProperty_stock_customerDirtyFlag();
    /**
     * 获取 [供应商位置]
     */
    public void setProperty_stock_supplier(Integer property_stock_supplier);
    
    /**
     * 设置 [供应商位置]
     */
    public Integer getProperty_stock_supplier();

    /**
     * 获取 [供应商位置]脏标记
     */
    public boolean getProperty_stock_supplierDirtyFlag();
    /**
     * 获取 [供应商付款条款]
     */
    public void setProperty_supplier_payment_term_id(Integer property_supplier_payment_term_id);
    
    /**
     * 设置 [供应商付款条款]
     */
    public Integer getProperty_supplier_payment_term_id();

    /**
     * 获取 [供应商付款条款]脏标记
     */
    public boolean getProperty_supplier_payment_term_idDirtyFlag();
    /**
     * 获取 [采购订单数]
     */
    public void setPurchase_order_count(Integer purchase_order_count);
    
    /**
     * 设置 [采购订单数]
     */
    public Integer getPurchase_order_count();

    /**
     * 获取 [采购订单数]脏标记
     */
    public boolean getPurchase_order_countDirtyFlag();
    /**
     * 获取 [采购订单]
     */
    public void setPurchase_warn(String purchase_warn);
    
    /**
     * 设置 [采购订单]
     */
    public String getPurchase_warn();

    /**
     * 获取 [采购订单]脏标记
     */
    public boolean getPurchase_warnDirtyFlag();
    /**
     * 获取 [采购订单消息]
     */
    public void setPurchase_warn_msg(String purchase_warn_msg);
    
    /**
     * 设置 [采购订单消息]
     */
    public String getPurchase_warn_msg();

    /**
     * 获取 [采购订单消息]脏标记
     */
    public boolean getPurchase_warn_msgDirtyFlag();
    /**
     * 获取 [内部参考]
     */
    public void setRef(String ref);
    
    /**
     * 设置 [内部参考]
     */
    public String getRef();

    /**
     * 获取 [内部参考]脏标记
     */
    public boolean getRefDirtyFlag();
    /**
     * 获取 [公司是指业务伙伴]
     */
    public void setRef_company_ids(String ref_company_ids);
    
    /**
     * 设置 [公司是指业务伙伴]
     */
    public String getRef_company_ids();

    /**
     * 获取 [公司是指业务伙伴]脏标记
     */
    public boolean getRef_company_idsDirtyFlag();
    /**
     * 获取 [销售订单个数]
     */
    public void setSale_order_count(Integer sale_order_count);
    
    /**
     * 设置 [销售订单个数]
     */
    public Integer getSale_order_count();

    /**
     * 获取 [销售订单个数]脏标记
     */
    public boolean getSale_order_countDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_order_ids(String sale_order_ids);
    
    /**
     * 设置 [销售订单]
     */
    public String getSale_order_ids();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_order_idsDirtyFlag();
    /**
     * 获取 [销售警告]
     */
    public void setSale_warn(String sale_warn);
    
    /**
     * 设置 [销售警告]
     */
    public String getSale_warn();

    /**
     * 获取 [销售警告]脏标记
     */
    public boolean getSale_warnDirtyFlag();
    /**
     * 获取 [销售订单消息]
     */
    public void setSale_warn_msg(String sale_warn_msg);
    
    /**
     * 设置 [销售订单消息]
     */
    public String getSale_warn_msg();

    /**
     * 获取 [销售订单消息]脏标记
     */
    public boolean getSale_warn_msgDirtyFlag();
    /**
     * 获取 [自己]
     */
    public void setSelf(Integer self);
    
    /**
     * 设置 [自己]
     */
    public Integer getSelf();

    /**
     * 获取 [自己]脏标记
     */
    public boolean getSelfDirtyFlag();
    /**
     * 获取 [注册到期]
     */
    public void setSignup_expiration(Timestamp signup_expiration);
    
    /**
     * 设置 [注册到期]
     */
    public Timestamp getSignup_expiration();

    /**
     * 获取 [注册到期]脏标记
     */
    public boolean getSignup_expirationDirtyFlag();
    /**
     * 获取 [注册令牌 Token]
     */
    public void setSignup_token(String signup_token);
    
    /**
     * 设置 [注册令牌 Token]
     */
    public String getSignup_token();

    /**
     * 获取 [注册令牌 Token]脏标记
     */
    public boolean getSignup_tokenDirtyFlag();
    /**
     * 获取 [注册令牌（Token）类型]
     */
    public void setSignup_type(String signup_type);
    
    /**
     * 设置 [注册令牌（Token）类型]
     */
    public String getSignup_type();

    /**
     * 获取 [注册令牌（Token）类型]脏标记
     */
    public boolean getSignup_typeDirtyFlag();
    /**
     * 获取 [注册网址]
     */
    public void setSignup_url(String signup_url);
    
    /**
     * 设置 [注册网址]
     */
    public String getSignup_url();

    /**
     * 获取 [注册网址]脏标记
     */
    public boolean getSignup_urlDirtyFlag();
    /**
     * 获取 [注册令牌（ Token  ）是有效的]
     */
    public void setSignup_valid(String signup_valid);
    
    /**
     * 设置 [注册令牌（ Token  ）是有效的]
     */
    public String getSignup_valid();

    /**
     * 获取 [注册令牌（ Token  ）是有效的]脏标记
     */
    public boolean getSignup_validDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setState_id(Integer state_id);
    
    /**
     * 设置 [省/ 州]
     */
    public Integer getState_id();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getState_idDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setState_id_text(String state_id_text);
    
    /**
     * 设置 [省/ 州]
     */
    public String getState_id_text();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getState_id_textDirtyFlag();
    /**
     * 获取 [街道]
     */
    public void setStreet(String street);
    
    /**
     * 设置 [街道]
     */
    public String getStreet();

    /**
     * 获取 [街道]脏标记
     */
    public boolean getStreetDirtyFlag();
    /**
     * 获取 [街道 2]
     */
    public void setStreet2(String street2);
    
    /**
     * 设置 [街道 2]
     */
    public String getStreet2();

    /**
     * 获取 [街道 2]脏标记
     */
    public boolean getStreet2DirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setSupplier(String supplier);
    
    /**
     * 设置 [供应商]
     */
    public String getSupplier();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getSupplierDirtyFlag();
    /**
     * 获取 [＃供应商账单]
     */
    public void setSupplier_invoice_count(Integer supplier_invoice_count);
    
    /**
     * 设置 [＃供应商账单]
     */
    public Integer getSupplier_invoice_count();

    /**
     * 获取 [＃供应商账单]脏标记
     */
    public boolean getSupplier_invoice_countDirtyFlag();
    /**
     * 获取 [# 任务]
     */
    public void setTask_count(Integer task_count);
    
    /**
     * 设置 [# 任务]
     */
    public Integer getTask_count();

    /**
     * 获取 [# 任务]脏标记
     */
    public boolean getTask_countDirtyFlag();
    /**
     * 获取 [任务]
     */
    public void setTask_ids(String task_ids);
    
    /**
     * 设置 [任务]
     */
    public String getTask_ids();

    /**
     * 获取 [任务]脏标记
     */
    public boolean getTask_idsDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [称谓]
     */
    public void setTitle(Integer title);
    
    /**
     * 设置 [称谓]
     */
    public Integer getTitle();

    /**
     * 获取 [称谓]脏标记
     */
    public boolean getTitleDirtyFlag();
    /**
     * 获取 [称谓]
     */
    public void setTitle_text(String title_text);
    
    /**
     * 设置 [称谓]
     */
    public String getTitle_text();

    /**
     * 获取 [称谓]脏标记
     */
    public boolean getTitle_textDirtyFlag();
    /**
     * 获取 [已开票总计]
     */
    public void setTotal_invoiced(Double total_invoiced);
    
    /**
     * 设置 [已开票总计]
     */
    public Double getTotal_invoiced();

    /**
     * 获取 [已开票总计]脏标记
     */
    public boolean getTotal_invoicedDirtyFlag();
    /**
     * 获取 [对此债务人的信任度]
     */
    public void setTrust(String trust);
    
    /**
     * 设置 [对此债务人的信任度]
     */
    public String getTrust();

    /**
     * 获取 [对此债务人的信任度]脏标记
     */
    public boolean getTrustDirtyFlag();
    /**
     * 获取 [地址类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [地址类型]
     */
    public String getType();

    /**
     * 获取 [地址类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [时区]
     */
    public void setTz(String tz);
    
    /**
     * 设置 [时区]
     */
    public String getTz();

    /**
     * 获取 [时区]脏标记
     */
    public boolean getTzDirtyFlag();
    /**
     * 获取 [时区偏移]
     */
    public void setTz_offset(String tz_offset);
    
    /**
     * 设置 [时区偏移]
     */
    public String getTz_offset();

    /**
     * 获取 [时区偏移]脏标记
     */
    public boolean getTz_offsetDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_ids(String user_ids);
    
    /**
     * 设置 [用户]
     */
    public String getUser_ids();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_idsDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [税号]
     */
    public void setVat(String vat);
    
    /**
     * 设置 [税号]
     */
    public String getVat();

    /**
     * 获取 [税号]脏标记
     */
    public boolean getVatDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite(String website);
    
    /**
     * 设置 [网站]
     */
    public String getWebsite();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsiteDirtyFlag();
    /**
     * 获取 [网站业务伙伴的详细说明]
     */
    public void setWebsite_description(String website_description);
    
    /**
     * 设置 [网站业务伙伴的详细说明]
     */
    public String getWebsite_description();

    /**
     * 获取 [网站业务伙伴的详细说明]脏标记
     */
    public boolean getWebsite_descriptionDirtyFlag();
    /**
     * 获取 [登记网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [登记网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [登记网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [网站元说明]
     */
    public void setWebsite_meta_description(String website_meta_description);
    
    /**
     * 设置 [网站元说明]
     */
    public String getWebsite_meta_description();

    /**
     * 获取 [网站元说明]脏标记
     */
    public boolean getWebsite_meta_descriptionDirtyFlag();
    /**
     * 获取 [网站meta关键词]
     */
    public void setWebsite_meta_keywords(String website_meta_keywords);
    
    /**
     * 设置 [网站meta关键词]
     */
    public String getWebsite_meta_keywords();

    /**
     * 获取 [网站meta关键词]脏标记
     */
    public boolean getWebsite_meta_keywordsDirtyFlag();
    /**
     * 获取 [网站opengraph图像]
     */
    public void setWebsite_meta_og_img(String website_meta_og_img);
    
    /**
     * 设置 [网站opengraph图像]
     */
    public String getWebsite_meta_og_img();

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    public boolean getWebsite_meta_og_imgDirtyFlag();
    /**
     * 获取 [网站meta标题]
     */
    public void setWebsite_meta_title(String website_meta_title);
    
    /**
     * 设置 [网站meta标题]
     */
    public String getWebsite_meta_title();

    /**
     * 获取 [网站meta标题]脏标记
     */
    public boolean getWebsite_meta_titleDirtyFlag();
    /**
     * 获取 [在当前网站显示]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [在当前网站显示]
     */
    public String getWebsite_published();

    /**
     * 获取 [在当前网站显示]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [网站业务伙伴简介]
     */
    public void setWebsite_short_description(String website_short_description);
    
    /**
     * 设置 [网站业务伙伴简介]
     */
    public String getWebsite_short_description();

    /**
     * 获取 [网站业务伙伴简介]脏标记
     */
    public boolean getWebsite_short_descriptionDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [邮政编码]
     */
    public void setZip(String zip);
    
    /**
     * 设置 [邮政编码]
     */
    public String getZip();

    /**
     * 获取 [邮政编码]脏标记
     */
    public boolean getZipDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
	
    /**
     * 获取 [res_partner_banks]
     */
    public List<cn.ibizlab.odoo.core.client.model.Ires_partner_bank> getRes_partner_banks();

    /**
     * 获取 [res_partner_banks]
     */
    public void setRes_partner_banks(List <cn.ibizlab.odoo.core.client.model.Ires_partner_bank> res_partner_banks);

}
