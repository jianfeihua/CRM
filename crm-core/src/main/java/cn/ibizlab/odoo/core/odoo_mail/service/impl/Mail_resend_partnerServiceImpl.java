package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_partnerSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_partnerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_resend_partnerFeignClient;

/**
 * 实体[业务伙伴需要额外信息重发EMail] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_resend_partnerServiceImpl implements IMail_resend_partnerService {

    @Autowired
    mail_resend_partnerFeignClient mail_resend_partnerFeignClient;


    @Override
    public Mail_resend_partner getDraft(Mail_resend_partner et) {
        et=mail_resend_partnerFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_resend_partnerFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_resend_partnerFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_resend_partner get(Integer id) {
		Mail_resend_partner et=mail_resend_partnerFeignClient.get(id);
        if(et==null){
            et=new Mail_resend_partner();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_resend_partner et) {
        Mail_resend_partner rt = mail_resend_partnerFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_resend_partner> list){
        mail_resend_partnerFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mail_resend_partner et) {
        Mail_resend_partner rt = mail_resend_partnerFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_resend_partner> list){
        mail_resend_partnerFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_resend_partner> searchDefault(Mail_resend_partnerSearchContext context) {
        Page<Mail_resend_partner> mail_resend_partners=mail_resend_partnerFeignClient.searchDefault(context);
        return mail_resend_partners;
    }


}


