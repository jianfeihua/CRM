package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_journalSearchContext;


/**
 * 实体[Account_journal] 服务对象接口
 */
public interface IAccount_journalService{

    Account_journal get(Integer key) ;
    Account_journal getDraft(Account_journal et) ;
    boolean create(Account_journal et) ;
    void createBatch(List<Account_journal> list) ;
    boolean update(Account_journal et) ;
    void updateBatch(List<Account_journal> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_journal> searchDefault(Account_journalSearchContext context) ;

}



