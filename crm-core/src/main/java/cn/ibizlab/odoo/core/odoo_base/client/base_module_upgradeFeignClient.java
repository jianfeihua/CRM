package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_upgradeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_module_upgrade] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "base-module-upgrade", fallback = base_module_upgradeFallback.class)
public interface base_module_upgradeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_upgrades/{id}")
    Base_module_upgrade update(@PathVariable("id") Integer id,@RequestBody Base_module_upgrade base_module_upgrade);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_upgrades/batch")
    Boolean updateBatch(@RequestBody List<Base_module_upgrade> base_module_upgrades);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_upgrades/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_upgrades/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_upgrades/{id}")
    Base_module_upgrade get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades/searchdefault")
    Page<Base_module_upgrade> searchDefault(@RequestBody Base_module_upgradeSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades")
    Base_module_upgrade create(@RequestBody Base_module_upgrade base_module_upgrade);

    @RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades/batch")
    Boolean createBatch(@RequestBody List<Base_module_upgrade> base_module_upgrades);



    @RequestMapping(method = RequestMethod.GET, value = "/base_module_upgrades/select")
    Page<Base_module_upgrade> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_upgrades/getdraft")
    Base_module_upgrade getDraft();


}
