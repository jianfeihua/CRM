package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_requiredSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_char_required] 服务对象接口
 */
@Component
public class base_import_tests_models_char_requiredFallback implements base_import_tests_models_char_requiredFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Base_import_tests_models_char_required> searchDefault(Base_import_tests_models_char_requiredSearchContext context){
            return null;
     }



    public Base_import_tests_models_char_required create(Base_import_tests_models_char_required base_import_tests_models_char_required){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_char_required> base_import_tests_models_char_requireds){
            return false;
     }


    public Base_import_tests_models_char_required update(Integer id, Base_import_tests_models_char_required base_import_tests_models_char_required){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_char_required> base_import_tests_models_char_requireds){
            return false;
     }


    public Base_import_tests_models_char_required get(Integer id){
            return null;
     }



    public Page<Base_import_tests_models_char_required> select(){
            return null;
     }

    public Base_import_tests_models_char_required getDraft(){
            return null;
    }



}
