package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;


/**
 * 实体[Fleet_vehicle_cost] 服务对象接口
 */
public interface IFleet_vehicle_costService{

    Fleet_vehicle_cost getDraft(Fleet_vehicle_cost et) ;
    Fleet_vehicle_cost get(Integer key) ;
    boolean create(Fleet_vehicle_cost et) ;
    void createBatch(List<Fleet_vehicle_cost> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Fleet_vehicle_cost et) ;
    void updateBatch(List<Fleet_vehicle_cost> list) ;
    Page<Fleet_vehicle_cost> searchDefault(Fleet_vehicle_costSearchContext context) ;

}



