package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_assignation_log] 服务对象接口
 */
@Component
public class fleet_vehicle_assignation_logFallback implements fleet_vehicle_assignation_logFeignClient{



    public Fleet_vehicle_assignation_log update(Integer id, Fleet_vehicle_assignation_log fleet_vehicle_assignation_log){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
            return false;
     }



    public Fleet_vehicle_assignation_log get(Integer id){
            return null;
     }


    public Fleet_vehicle_assignation_log create(Fleet_vehicle_assignation_log fleet_vehicle_assignation_log){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
            return false;
     }

    public Page<Fleet_vehicle_assignation_log> searchDefault(Fleet_vehicle_assignation_logSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Fleet_vehicle_assignation_log> select(){
            return null;
     }

    public Fleet_vehicle_assignation_log getDraft(){
            return null;
    }



}
