package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [发票] 对象
 */
@Data
public class Account_invoice extends EntityClient implements Serializable {

    /**
     * 未到期贷项
     */
    @JSONField(name = "outstanding_credits_debits_widget")
    @JsonProperty("outstanding_credits_debits_widget")
    private String outstandingCreditsDebitsWidget;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 日记账分录名称
     */
    @DEField(name = "move_name")
    @JSONField(name = "move_name")
    @JsonProperty("move_name")
    private String moveName;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 门户访问网址
     */
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    private String accessUrl;

    /**
     * 付款凭证明细
     */
    @JSONField(name = "payment_move_line_ids")
    @JsonProperty("payment_move_line_ids")
    private String paymentMoveLineIds;

    /**
     * 源文档
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 会计日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 公司货币的合计
     */
    @DEField(name = "amount_total_company_signed")
    @JSONField(name = "amount_total_company_signed")
    @JsonProperty("amount_total_company_signed")
    private Double amountTotalCompanySigned;

    /**
     * 操作编号
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 按公司本位币计的不含税金额
     */
    @DEField(name = "amount_untaxed_signed")
    @JSONField(name = "amount_untaxed_signed")
    @JsonProperty("amount_untaxed_signed")
    private Double amountUntaxedSigned;

    /**
     * 到期金额
     */
    @JSONField(name = "residual")
    @JsonProperty("residual")
    private Double residual;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 有未清项
     */
    @JSONField(name = "has_outstanding")
    @JsonProperty("has_outstanding")
    private String hasOutstanding;

    /**
     * 额外的信息
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 按组分配税额
     */
    @JSONField(name = "amount_by_group")
    @JsonProperty("amount_by_group")
    private byte[] amountByGroup;

    /**
     * 支付挂件
     */
    @JSONField(name = "payments_widget")
    @JsonProperty("payments_widget")
    private String paymentsWidget;

    /**
     * 已付／已核销
     */
    @JSONField(name = "reconciled")
    @JsonProperty("reconciled")
    private String reconciled;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 已汇
     */
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private String sent;

    /**
     * 以发票币种总计
     */
    @DEField(name = "amount_total_signed")
    @JSONField(name = "amount_total_signed")
    @JsonProperty("amount_total_signed")
    private Double amountTotalSigned;

    /**
     * 发票行
     */
    @JSONField(name = "invoice_line_ids")
    @JsonProperty("invoice_line_ids")
    private String invoiceLineIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 访问警告
     */
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    private String accessWarning;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 发票使用币种的逾期金额
     */
    @DEField(name = "residual_signed")
    @JSONField(name = "residual_signed")
    @JsonProperty("residual_signed")
    private Double residualSigned;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 安全令牌
     */
    @DEField(name = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 供应商名称
     */
    @DEField(name = "vendor_display_name")
    @JSONField(name = "vendor_display_name")
    @JsonProperty("vendor_display_name")
    private String vendorDisplayName;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 未税金额
     */
    @DEField(name = "amount_untaxed")
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private Double amountUntaxed;

    /**
     * 交易
     */
    @JSONField(name = "transaction_ids")
    @JsonProperty("transaction_ids")
    private String transactionIds;

    /**
     * 到期日期
     */
    @DEField(name = "date_due")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_due" , format="yyyy-MM-dd")
    @JsonProperty("date_due")
    private Timestamp dateDue;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 已授权的交易
     */
    @JSONField(name = "authorized_transaction_ids")
    @JsonProperty("authorized_transaction_ids")
    private String authorizedTransactionIds;

    /**
     * 税率明细行
     */
    @JSONField(name = "tax_line_ids")
    @JsonProperty("tax_line_ids")
    private String taxLineIds;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 源邮箱
     */
    @DEField(name = "source_email")
    @JSONField(name = "source_email")
    @JsonProperty("source_email")
    private String sourceEmail;

    /**
     * 税率
     */
    @DEField(name = "amount_tax")
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private Double amountTax;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * Untaxed Amount in Invoice Currency
     */
    @JSONField(name = "amount_untaxed_invoice_signed")
    @JsonProperty("amount_untaxed_invoice_signed")
    private Double amountUntaxedInvoiceSigned;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 下一个号码前缀
     */
    @JSONField(name = "sequence_number_next_prefix")
    @JsonProperty("sequence_number_next_prefix")
    private String sequenceNumberNextPrefix;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 退款发票
     */
    @JSONField(name = "refund_invoice_ids")
    @JsonProperty("refund_invoice_ids")
    private String refundInvoiceIds;

    /**
     * 公司使用币种的逾期金额
     */
    @DEField(name = "residual_company_signed")
    @JSONField(name = "residual_company_signed")
    @JsonProperty("residual_company_signed")
    private Double residualCompanySigned;

    /**
     * 开票日期
     */
    @DEField(name = "date_invoice")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_invoice" , format="yyyy-MM-dd")
    @JsonProperty("date_invoice")
    private Timestamp dateInvoice;

    /**
     * 参考/说明
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 付款参考:
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 下一号码
     */
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private String sequenceNumberNext;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * Tax in Invoice Currency
     */
    @JSONField(name = "amount_tax_signed")
    @JsonProperty("amount_tax_signed")
    private Double amountTaxSigned;

    /**
     * 发票标示
     */
    @JSONField(name = "invoice_icon")
    @JsonProperty("invoice_icon")
    private String invoiceIcon;

    /**
     * 付款
     */
    @JSONField(name = "payment_ids")
    @JsonProperty("payment_ids")
    private String paymentIds;

    /**
     * 需要一个动作消息的编码
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 总计
     */
    @DEField(name = "amount_total")
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 添加采购订单
     */
    @JSONField(name = "purchase_id_text")
    @JsonProperty("purchase_id_text")
    private String purchaseIdText;

    /**
     * 自动完成
     */
    @JSONField(name = "vendor_bill_purchase_id_text")
    @JsonProperty("vendor_bill_purchase_id_text")
    private String vendorBillPurchaseIdText;

    /**
     * 来源
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;

    /**
     * 营销
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;

    /**
     * 送货地址
     */
    @JSONField(name = "partner_shipping_id_text")
    @JsonProperty("partner_shipping_id_text")
    private String partnerShippingIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 贸易条款
     */
    @JSONField(name = "incoterms_id_text")
    @JsonProperty("incoterms_id_text")
    private String incotermsIdText;

    /**
     * 公司货币
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 科目
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 国际贸易术语
     */
    @JSONField(name = "incoterm_id_text")
    @JsonProperty("incoterm_id_text")
    private String incotermIdText;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 媒体
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 销售员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 现金舍入方式
     */
    @JSONField(name = "cash_rounding_id_text")
    @JsonProperty("cash_rounding_id_text")
    private String cashRoundingIdText;

    /**
     * 供应商账单
     */
    @JSONField(name = "vendor_bill_id_text")
    @JsonProperty("vendor_bill_id_text")
    private String vendorBillIdText;

    /**
     * 此发票为信用票的发票
     */
    @JSONField(name = "refund_invoice_id_text")
    @JsonProperty("refund_invoice_id_text")
    private String refundInvoiceIdText;

    /**
     * 税科目调整
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    private String fiscalPositionIdText;

    /**
     * 号码
     */
    @JSONField(name = "number")
    @JsonProperty("number")
    private String number;

    /**
     * 商业实体
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * 付款条款
     */
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    private String paymentTermIdText;

    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 科目
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Integer mediumId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 日记账分录
     */
    @DEField(name = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 付款条款
     */
    @DEField(name = "payment_term_id")
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    private Integer paymentTermId;

    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 国际贸易术语
     */
    @DEField(name = "incoterm_id")
    @JSONField(name = "incoterm_id")
    @JsonProperty("incoterm_id")
    private Integer incotermId;

    /**
     * 添加采购订单
     */
    @DEField(name = "purchase_id")
    @JSONField(name = "purchase_id")
    @JsonProperty("purchase_id")
    private Integer purchaseId;

    /**
     * 税科目调整
     */
    @DEField(name = "fiscal_position_id")
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    private Integer fiscalPositionId;

    /**
     * 此发票为信用票的发票
     */
    @DEField(name = "refund_invoice_id")
    @JSONField(name = "refund_invoice_id")
    @JsonProperty("refund_invoice_id")
    private Integer refundInvoiceId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 银行账户
     */
    @DEField(name = "partner_bank_id")
    @JSONField(name = "partner_bank_id")
    @JsonProperty("partner_bank_id")
    private Integer partnerBankId;

    /**
     * 现金舍入方式
     */
    @DEField(name = "cash_rounding_id")
    @JSONField(name = "cash_rounding_id")
    @JsonProperty("cash_rounding_id")
    private Integer cashRoundingId;

    /**
     * 商业实体
     */
    @DEField(name = "commercial_partner_id")
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Integer commercialPartnerId;

    /**
     * 送货地址
     */
    @DEField(name = "partner_shipping_id")
    @JSONField(name = "partner_shipping_id")
    @JsonProperty("partner_shipping_id")
    private Integer partnerShippingId;

    /**
     * 贸易条款
     */
    @DEField(name = "incoterms_id")
    @JSONField(name = "incoterms_id")
    @JsonProperty("incoterms_id")
    private Integer incotermsId;

    /**
     * 来源
     */
    @DEField(name = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;

    /**
     * 供应商账单
     */
    @DEField(name = "vendor_bill_id")
    @JSONField(name = "vendor_bill_id")
    @JsonProperty("vendor_bill_id")
    private Integer vendorBillId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 自动完成
     */
    @DEField(name = "vendor_bill_purchase_id")
    @JSONField(name = "vendor_bill_purchase_id")
    @JsonProperty("vendor_bill_purchase_id")
    private Integer vendorBillPurchaseId;

    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Integer campaignId;


    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JSONField(name = "odoocashrounding")
    @JsonProperty("odoocashrounding")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding odooCashRounding;

    /**
     * 
     */
    @JSONField(name = "odoofiscalposition")
    @JsonProperty("odoofiscalposition")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition;

    /**
     * 
     */
    @JSONField(name = "odooincoterms")
    @JsonProperty("odooincoterms")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms odooIncoterms;

    /**
     * 
     */
    @JSONField(name = "odooincoterm")
    @JsonProperty("odooincoterm")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms odooIncoterm;

    /**
     * 
     */
    @JSONField(name = "odoorefundinvoice")
    @JsonProperty("odoorefundinvoice")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice odooRefundInvoice;

    /**
     * 
     */
    @JSONField(name = "odoovendorbill")
    @JsonProperty("odoovendorbill")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice odooVendorBill;

    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoomove")
    @JsonProperty("odoomove")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_move odooMove;

    /**
     * 
     */
    @JSONField(name = "odoopaymentterm")
    @JsonProperty("odoopaymentterm")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term odooPaymentTerm;

    /**
     * 
     */
    @JSONField(name = "odooteam")
    @JsonProperty("odooteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JSONField(name = "odoovendorbillpurchase")
    @JsonProperty("odoovendorbillpurchase")
    private cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union odooVendorBillPurchase;

    /**
     * 
     */
    @JSONField(name = "odoopurchase")
    @JsonProperty("odoopurchase")
    private cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order odooPurchase;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartnerbank")
    @JsonProperty("odoopartnerbank")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooPartnerBank;

    /**
     * 
     */
    @JSONField(name = "odoocommercialpartner")
    @JsonProperty("odoocommercialpartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooCommercialPartner;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoopartnershipping")
    @JsonProperty("odoopartnershipping")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartnerShipping;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoocampaign")
    @JsonProperty("odoocampaign")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JSONField(name = "odoomedium")
    @JsonProperty("odoomedium")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JSONField(name = "odoosource")
    @JsonProperty("odoosource")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source odooSource;




    /**
     * 设置 [日记账分录名称]
     */
    public void setMoveName(String moveName){
        this.moveName = moveName ;
        this.modify("move_name",moveName);
    }
    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }
    /**
     * 设置 [会计日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [公司货币的合计]
     */
    public void setAmountTotalCompanySigned(Double amountTotalCompanySigned){
        this.amountTotalCompanySigned = amountTotalCompanySigned ;
        this.modify("amount_total_company_signed",amountTotalCompanySigned);
    }
    /**
     * 设置 [按公司本位币计的不含税金额]
     */
    public void setAmountUntaxedSigned(Double amountUntaxedSigned){
        this.amountUntaxedSigned = amountUntaxedSigned ;
        this.modify("amount_untaxed_signed",amountUntaxedSigned);
    }
    /**
     * 设置 [到期金额]
     */
    public void setResidual(Double residual){
        this.residual = residual ;
        this.modify("residual",residual);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [额外的信息]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }
    /**
     * 设置 [已付／已核销]
     */
    public void setReconciled(String reconciled){
        this.reconciled = reconciled ;
        this.modify("reconciled",reconciled);
    }
    /**
     * 设置 [已汇]
     */
    public void setSent(String sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }
    /**
     * 设置 [以发票币种总计]
     */
    public void setAmountTotalSigned(Double amountTotalSigned){
        this.amountTotalSigned = amountTotalSigned ;
        this.modify("amount_total_signed",amountTotalSigned);
    }
    /**
     * 设置 [发票使用币种的逾期金额]
     */
    public void setResidualSigned(Double residualSigned){
        this.residualSigned = residualSigned ;
        this.modify("residual_signed",residualSigned);
    }
    /**
     * 设置 [安全令牌]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }
    /**
     * 设置 [供应商名称]
     */
    public void setVendorDisplayName(String vendorDisplayName){
        this.vendorDisplayName = vendorDisplayName ;
        this.modify("vendor_display_name",vendorDisplayName);
    }
    /**
     * 设置 [未税金额]
     */
    public void setAmountUntaxed(Double amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }
    /**
     * 设置 [到期日期]
     */
    public void setDateDue(Timestamp dateDue){
        this.dateDue = dateDue ;
        this.modify("date_due",dateDue);
    }
    /**
     * 设置 [源邮箱]
     */
    public void setSourceEmail(String sourceEmail){
        this.sourceEmail = sourceEmail ;
        this.modify("source_email",sourceEmail);
    }
    /**
     * 设置 [税率]
     */
    public void setAmountTax(Double amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [公司使用币种的逾期金额]
     */
    public void setResidualCompanySigned(Double residualCompanySigned){
        this.residualCompanySigned = residualCompanySigned ;
        this.modify("residual_company_signed",residualCompanySigned);
    }
    /**
     * 设置 [开票日期]
     */
    public void setDateInvoice(Timestamp dateInvoice){
        this.dateInvoice = dateInvoice ;
        this.modify("date_invoice",dateInvoice);
    }
    /**
     * 设置 [参考/说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [付款参考:]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [总计]
     */
    public void setAmountTotal(Double amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }
    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [销售员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Integer teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }
    /**
     * 设置 [科目]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [媒体]
     */
    public void setMediumId(Integer mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [日记账分录]
     */
    public void setMoveId(Integer moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }
    /**
     * 设置 [付款条款]
     */
    public void setPaymentTermId(Integer paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }
    /**
     * 设置 [日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [国际贸易术语]
     */
    public void setIncotermId(Integer incotermId){
        this.incotermId = incotermId ;
        this.modify("incoterm_id",incotermId);
    }
    /**
     * 设置 [添加采购订单]
     */
    public void setPurchaseId(Integer purchaseId){
        this.purchaseId = purchaseId ;
        this.modify("purchase_id",purchaseId);
    }
    /**
     * 设置 [税科目调整]
     */
    public void setFiscalPositionId(Integer fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }
    /**
     * 设置 [此发票为信用票的发票]
     */
    public void setRefundInvoiceId(Integer refundInvoiceId){
        this.refundInvoiceId = refundInvoiceId ;
        this.modify("refund_invoice_id",refundInvoiceId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [银行账户]
     */
    public void setPartnerBankId(Integer partnerBankId){
        this.partnerBankId = partnerBankId ;
        this.modify("partner_bank_id",partnerBankId);
    }
    /**
     * 设置 [现金舍入方式]
     */
    public void setCashRoundingId(Integer cashRoundingId){
        this.cashRoundingId = cashRoundingId ;
        this.modify("cash_rounding_id",cashRoundingId);
    }
    /**
     * 设置 [商业实体]
     */
    public void setCommercialPartnerId(Integer commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }
    /**
     * 设置 [送货地址]
     */
    public void setPartnerShippingId(Integer partnerShippingId){
        this.partnerShippingId = partnerShippingId ;
        this.modify("partner_shipping_id",partnerShippingId);
    }
    /**
     * 设置 [贸易条款]
     */
    public void setIncotermsId(Integer incotermsId){
        this.incotermsId = incotermsId ;
        this.modify("incoterms_id",incotermsId);
    }
    /**
     * 设置 [来源]
     */
    public void setSourceId(Integer sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }
    /**
     * 设置 [供应商账单]
     */
    public void setVendorBillId(Integer vendorBillId){
        this.vendorBillId = vendorBillId ;
        this.modify("vendor_bill_id",vendorBillId);
    }
    /**
     * 设置 [自动完成]
     */
    public void setVendorBillPurchaseId(Integer vendorBillPurchaseId){
        this.vendorBillPurchaseId = vendorBillPurchaseId ;
        this.modify("vendor_bill_purchase_id",vendorBillPurchaseId);
    }
    /**
     * 设置 [营销]
     */
    public void setCampaignId(Integer campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

}


