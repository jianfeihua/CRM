package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_order_line] 服务对象接口
 */
@FeignClient(value = "odoo-sale", contextId = "sale-order-line", fallback = sale_order_lineFallback.class)
public interface sale_order_lineFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_lines/{id}")
    Sale_order_line update(@PathVariable("id") Integer id,@RequestBody Sale_order_line sale_order_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_lines/batch")
    Boolean updateBatch(@RequestBody List<Sale_order_line> sale_order_lines);



    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_lines/{id}")
    Sale_order_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines")
    Sale_order_line create(@RequestBody Sale_order_line sale_order_line);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/batch")
    Boolean createBatch(@RequestBody List<Sale_order_line> sale_order_lines);




    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/searchdefault")
    Page<Sale_order_line> searchDefault(@RequestBody Sale_order_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_lines/select")
    Page<Sale_order_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_lines/getdraft")
    Sale_order_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/checkkey")
    Boolean checkKey(@RequestBody Sale_order_line sale_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/save")
    Boolean save(@RequestBody Sale_order_line sale_order_line);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/save")
    Boolean saveBatch(@RequestBody List<Sale_order_line> sale_order_lines);


}
