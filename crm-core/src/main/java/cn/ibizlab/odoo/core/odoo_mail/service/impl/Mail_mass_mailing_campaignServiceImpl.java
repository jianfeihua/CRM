package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_campaignService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_campaignFeignClient;

/**
 * 实体[群发邮件营销] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_campaignServiceImpl implements IMail_mass_mailing_campaignService {

    @Autowired
    mail_mass_mailing_campaignFeignClient mail_mass_mailing_campaignFeignClient;


    @Override
    public Mail_mass_mailing_campaign get(Integer id) {
		Mail_mass_mailing_campaign et=mail_mass_mailing_campaignFeignClient.get(id);
        if(et==null){
            et=new Mail_mass_mailing_campaign();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mail_mass_mailing_campaign getDraft(Mail_mass_mailing_campaign et) {
        et=mail_mass_mailing_campaignFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mail_mass_mailing_campaign et) {
        Mail_mass_mailing_campaign rt = mail_mass_mailing_campaignFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_campaign> list){
        mail_mass_mailing_campaignFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mail_mass_mailing_campaign et) {
        Mail_mass_mailing_campaign rt = mail_mass_mailing_campaignFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mass_mailing_campaign> list){
        mail_mass_mailing_campaignFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mass_mailing_campaignFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mass_mailing_campaignFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_campaign> searchDefault(Mail_mass_mailing_campaignSearchContext context) {
        Page<Mail_mass_mailing_campaign> mail_mass_mailing_campaigns=mail_mass_mailing_campaignFeignClient.searchDefault(context);
        return mail_mass_mailing_campaigns;
    }


}


