package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [国家/地区] 对象
 */
@Data
public class Res_country extends EntityClient implements Serializable {

    /**
     * 输入视图
     */
    @DEField(name = "address_view_id")
    @JSONField(name = "address_view_id")
    @JsonProperty("address_view_id")
    private Integer addressViewId;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 国家/地区分组
     */
    @JSONField(name = "country_group_ids")
    @JsonProperty("country_group_ids")
    private String countryGroupIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 省份
     */
    @JSONField(name = "state_ids")
    @JsonProperty("state_ids")
    private String stateIds;

    /**
     * 客户姓名位置
     */
    @DEField(name = "name_position")
    @JSONField(name = "name_position")
    @JsonProperty("name_position")
    private String namePosition;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 国家/地区长途区号
     */
    @DEField(name = "phone_code")
    @JSONField(name = "phone_code")
    @JsonProperty("phone_code")
    private Integer phoneCode;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 国家/地区代码
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 增值税标签
     */
    @DEField(name = "vat_label")
    @JSONField(name = "vat_label")
    @JsonProperty("vat_label")
    private String vatLabel;

    /**
     * 国家/地区名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 报表布局
     */
    @DEField(name = "address_format")
    @JSONField(name = "address_format")
    @JsonProperty("address_format")
    private String addressFormat;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;


    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [输入视图]
     */
    public void setAddressViewId(Integer addressViewId){
        this.addressViewId = addressViewId ;
        this.modify("address_view_id",addressViewId);
    }
    /**
     * 设置 [客户姓名位置]
     */
    public void setNamePosition(String namePosition){
        this.namePosition = namePosition ;
        this.modify("name_position",namePosition);
    }
    /**
     * 设置 [国家/地区长途区号]
     */
    public void setPhoneCode(Integer phoneCode){
        this.phoneCode = phoneCode ;
        this.modify("phone_code",phoneCode);
    }
    /**
     * 设置 [国家/地区代码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }
    /**
     * 设置 [增值税标签]
     */
    public void setVatLabel(String vatLabel){
        this.vatLabel = vatLabel ;
        this.modify("vat_label",vatLabel);
    }
    /**
     * 设置 [国家/地区名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [报表布局]
     */
    public void setAddressFormat(String addressFormat){
        this.addressFormat = addressFormat ;
        this.modify("address_format",addressFormat);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

}


