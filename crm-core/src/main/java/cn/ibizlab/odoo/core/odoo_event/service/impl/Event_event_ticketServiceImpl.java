package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_event_ticketSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_event_ticketService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_event_ticketFeignClient;

/**
 * 实体[活动入场券] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_event_ticketServiceImpl implements IEvent_event_ticketService {

    @Autowired
    event_event_ticketFeignClient event_event_ticketFeignClient;


    @Override
    public Event_event_ticket getDraft(Event_event_ticket et) {
        et=event_event_ticketFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Event_event_ticket et) {
        Event_event_ticket rt = event_event_ticketFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_event_ticket> list){
        event_event_ticketFeignClient.updateBatch(list) ;
    }

    @Override
    public Event_event_ticket get(Integer id) {
		Event_event_ticket et=event_event_ticketFeignClient.get(id);
        if(et==null){
            et=new Event_event_ticket();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=event_event_ticketFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_event_ticketFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Event_event_ticket et) {
        Event_event_ticket rt = event_event_ticketFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_event_ticket> list){
        event_event_ticketFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_event_ticket> searchDefault(Event_event_ticketSearchContext context) {
        Page<Event_event_ticket> event_event_tickets=event_event_ticketFeignClient.searchDefault(context);
        return event_event_tickets;
    }


}


