package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mail_registrationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_mail_registrationFeignClient;

/**
 * 实体[登记邮件调度] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_mail_registrationServiceImpl implements IEvent_mail_registrationService {

    @Autowired
    event_mail_registrationFeignClient event_mail_registrationFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=event_mail_registrationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_mail_registrationFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Event_mail_registration et) {
        Event_mail_registration rt = event_mail_registrationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_mail_registration> list){
        event_mail_registrationFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Event_mail_registration et) {
        Event_mail_registration rt = event_mail_registrationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_mail_registration> list){
        event_mail_registrationFeignClient.createBatch(list) ;
    }

    @Override
    public Event_mail_registration getDraft(Event_mail_registration et) {
        et=event_mail_registrationFeignClient.getDraft();
        return et;
    }

    @Override
    public Event_mail_registration get(Integer id) {
		Event_mail_registration et=event_mail_registrationFeignClient.get(id);
        if(et==null){
            et=new Event_mail_registration();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_mail_registration> searchDefault(Event_mail_registrationSearchContext context) {
        Page<Event_mail_registration> event_mail_registrations=event_mail_registrationFeignClient.searchDefault(context);
        return event_mail_registrations;
    }


}


