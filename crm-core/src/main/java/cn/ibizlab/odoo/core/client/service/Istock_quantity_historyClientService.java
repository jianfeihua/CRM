package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_quantity_history;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_quantity_history] 服务对象接口
 */
public interface Istock_quantity_historyClientService{

    public Istock_quantity_history createModel() ;

    public void update(Istock_quantity_history stock_quantity_history);

    public void create(Istock_quantity_history stock_quantity_history);

    public void updateBatch(List<Istock_quantity_history> stock_quantity_histories);

    public void removeBatch(List<Istock_quantity_history> stock_quantity_histories);

    public Page<Istock_quantity_history> fetchDefault(SearchContext context);

    public void get(Istock_quantity_history stock_quantity_history);

    public void createBatch(List<Istock_quantity_history> stock_quantity_histories);

    public void remove(Istock_quantity_history stock_quantity_history);

    public Page<Istock_quantity_history> select(SearchContext context);

    public void getDraft(Istock_quantity_history stock_quantity_history);

}
