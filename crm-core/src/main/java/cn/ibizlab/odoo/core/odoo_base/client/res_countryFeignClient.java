package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_country] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-country", fallback = res_countryFallback.class)
public interface res_countryFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/res_countries/{id}")
    Res_country update(@PathVariable("id") Integer id,@RequestBody Res_country res_country);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_countries/batch")
    Boolean updateBatch(@RequestBody List<Res_country> res_countries);




    @RequestMapping(method = RequestMethod.POST, value = "/res_countries")
    Res_country create(@RequestBody Res_country res_country);

    @RequestMapping(method = RequestMethod.POST, value = "/res_countries/batch")
    Boolean createBatch(@RequestBody List<Res_country> res_countries);




    @RequestMapping(method = RequestMethod.POST, value = "/res_countries/searchdefault")
    Page<Res_country> searchDefault(@RequestBody Res_countrySearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_countries/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_countries/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/res_countries/{id}")
    Res_country get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/res_countries/select")
    Page<Res_country> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_countries/getdraft")
    Res_country getDraft();


}
