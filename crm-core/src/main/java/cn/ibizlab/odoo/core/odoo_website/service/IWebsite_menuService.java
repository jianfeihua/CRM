package cn.ibizlab.odoo.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;


/**
 * 实体[Website_menu] 服务对象接口
 */
public interface IWebsite_menuService{

    boolean update(Website_menu et) ;
    void updateBatch(List<Website_menu> list) ;
    Website_menu get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Website_menu et) ;
    void createBatch(List<Website_menu> list) ;
    Website_menu getDraft(Website_menu et) ;
    Page<Website_menu> searchDefault(Website_menuSearchContext context) ;

}



