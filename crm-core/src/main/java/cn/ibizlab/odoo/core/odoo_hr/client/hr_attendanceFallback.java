package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_attendance] 服务对象接口
 */
@Component
public class hr_attendanceFallback implements hr_attendanceFeignClient{

    public Hr_attendance update(Integer id, Hr_attendance hr_attendance){
            return null;
     }
    public Boolean updateBatch(List<Hr_attendance> hr_attendances){
            return false;
     }



    public Hr_attendance create(Hr_attendance hr_attendance){
            return null;
     }
    public Boolean createBatch(List<Hr_attendance> hr_attendances){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Hr_attendance get(Integer id){
            return null;
     }


    public Page<Hr_attendance> searchDefault(Hr_attendanceSearchContext context){
            return null;
     }


    public Page<Hr_attendance> select(){
            return null;
     }

    public Hr_attendance getDraft(){
            return null;
    }



}
