package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [发票行] 对象
 */
@Data
public class Account_invoice_line extends EntityClient implements Serializable {

    /**
     * 显示类型
     */
    @DEField(name = "display_type")
    @JSONField(name = "display_type")
    @JsonProperty("display_type")
    private String displayType;

    /**
     * 说明
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 金额 (不含税)
     */
    @DEField(name = "price_subtotal")
    @JSONField(name = "price_subtotal")
    @JsonProperty("price_subtotal")
    private Double priceSubtotal;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 源文档
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 销售订单明细
     */
    @JSONField(name = "sale_line_ids")
    @JsonProperty("sale_line_ids")
    private String saleLineIds;

    /**
     * 折扣 (%)
     */
    @JSONField(name = "discount")
    @JsonProperty("discount")
    private Double discount;

    /**
     * 金额 (含税)
     */
    @DEField(name = "price_total")
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private Double priceTotal;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 数量
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private Double quantity;

    /**
     * 税率金额
     */
    @JSONField(name = "price_tax")
    @JsonProperty("price_tax")
    private Double priceTax;

    /**
     * 签核的金额
     */
    @DEField(name = "price_subtotal_signed")
    @JSONField(name = "price_subtotal_signed")
    @JsonProperty("price_subtotal_signed")
    private Double priceSubtotalSigned;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 单价
     */
    @DEField(name = "price_unit")
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 税率设置
     */
    @JSONField(name = "invoice_line_tax_ids")
    @JsonProperty("invoice_line_tax_ids")
    private String invoiceLineTaxIds;

    /**
     * 分析标签
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;

    /**
     * 舍入明细
     */
    @DEField(name = "is_rounding_line")
    @JSONField(name = "is_rounding_line")
    @JsonProperty("is_rounding_line")
    private String isRoundingLine;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 分析账户
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    private String accountAnalyticIdText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 发票参考
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 类型
     */
    @JSONField(name = "invoice_type")
    @JsonProperty("invoice_type")
    private String invoiceType;

    /**
     * 产品图片
     */
    @JSONField(name = "product_image")
    @JsonProperty("product_image")
    private byte[] productImage;

    /**
     * 计量单位
     */
    @JSONField(name = "uom_id_text")
    @JsonProperty("uom_id_text")
    private String uomIdText;

    /**
     * 公司货币
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 采购订单
     */
    @JSONField(name = "purchase_id")
    @JsonProperty("purchase_id")
    private Integer purchaseId;

    /**
     * 科目
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 采购订单行
     */
    @JSONField(name = "purchase_line_id_text")
    @JsonProperty("purchase_line_id_text")
    private String purchaseLineIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 发票参考
     */
    @DEField(name = "invoice_id")
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Integer invoiceId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 科目
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 采购订单行
     */
    @DEField(name = "purchase_line_id")
    @JSONField(name = "purchase_line_id")
    @JsonProperty("purchase_line_id")
    private Integer purchaseLineId;

    /**
     * 分析账户
     */
    @DEField(name = "account_analytic_id")
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    private Integer accountAnalyticId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 计量单位
     */
    @DEField(name = "uom_id")
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    private Integer uomId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;


    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JSONField(name = "odooaccountanalytic")
    @JsonProperty("odooaccountanalytic")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic;

    /**
     * 
     */
    @JSONField(name = "odooinvoice")
    @JsonProperty("odooinvoice")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice odooInvoice;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoopurchaseline")
    @JsonProperty("odoopurchaseline")
    private cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line odooPurchaseLine;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoouom")
    @JsonProperty("odoouom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooUom;




    /**
     * 设置 [显示类型]
     */
    public void setDisplayType(String displayType){
        this.displayType = displayType ;
        this.modify("display_type",displayType);
    }
    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [金额 (不含税)]
     */
    public void setPriceSubtotal(Double priceSubtotal){
        this.priceSubtotal = priceSubtotal ;
        this.modify("price_subtotal",priceSubtotal);
    }
    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }
    /**
     * 设置 [折扣 (%)]
     */
    public void setDiscount(Double discount){
        this.discount = discount ;
        this.modify("discount",discount);
    }
    /**
     * 设置 [金额 (含税)]
     */
    public void setPriceTotal(Double priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }
    /**
     * 设置 [数量]
     */
    public void setQuantity(Double quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }
    /**
     * 设置 [签核的金额]
     */
    public void setPriceSubtotalSigned(Double priceSubtotalSigned){
        this.priceSubtotalSigned = priceSubtotalSigned ;
        this.modify("price_subtotal_signed",priceSubtotalSigned);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [单价]
     */
    public void setPriceUnit(Double priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }
    /**
     * 设置 [舍入明细]
     */
    public void setIsRoundingLine(String isRoundingLine){
        this.isRoundingLine = isRoundingLine ;
        this.modify("is_rounding_line",isRoundingLine);
    }
    /**
     * 设置 [发票参考]
     */
    public void setInvoiceId(Integer invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [科目]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [采购订单行]
     */
    public void setPurchaseLineId(Integer purchaseLineId){
        this.purchaseLineId = purchaseLineId ;
        this.modify("purchase_line_id",purchaseLineId);
    }
    /**
     * 设置 [分析账户]
     */
    public void setAccountAnalyticId(Integer accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [计量单位]
     */
    public void setUomId(Integer uomId){
        this.uomId = uomId ;
        this.modify("uom_id",uomId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

}


