package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklistSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_blacklist] 服务对象接口
 */
@Component
public class mail_blacklistFallback implements mail_blacklistFeignClient{

    public Page<Mail_blacklist> searchDefault(Mail_blacklistSearchContext context){
            return null;
     }



    public Mail_blacklist get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mail_blacklist update(Integer id, Mail_blacklist mail_blacklist){
            return null;
     }
    public Boolean updateBatch(List<Mail_blacklist> mail_blacklists){
            return false;
     }


    public Mail_blacklist create(Mail_blacklist mail_blacklist){
            return null;
     }
    public Boolean createBatch(List<Mail_blacklist> mail_blacklists){
            return false;
     }


    public Page<Mail_blacklist> select(){
            return null;
     }

    public Mail_blacklist getDraft(){
            return null;
    }



}
