package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_report] 服务对象接口
 */
@Component
public class account_invoice_reportFallback implements account_invoice_reportFeignClient{




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Account_invoice_report> searchDefault(Account_invoice_reportSearchContext context){
            return null;
     }


    public Account_invoice_report get(Integer id){
            return null;
     }


    public Account_invoice_report create(Account_invoice_report account_invoice_report){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_report> account_invoice_reports){
            return false;
     }

    public Account_invoice_report update(Integer id, Account_invoice_report account_invoice_report){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_report> account_invoice_reports){
            return false;
     }


    public Page<Account_invoice_report> select(){
            return null;
     }

    public Account_invoice_report getDraft(){
            return null;
    }



}
