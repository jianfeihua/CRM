package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_activity_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_activity_reportFeignClient;

/**
 * 实体[CRM活动分析] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_activity_reportServiceImpl implements ICrm_activity_reportService {

    @Autowired
    crm_activity_reportFeignClient crm_activity_reportFeignClient;


    @Override
    public boolean create(Crm_activity_report et) {
        Crm_activity_report rt = crm_activity_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_activity_report> list){
        crm_activity_reportFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Crm_activity_report et) {
        Crm_activity_report rt = crm_activity_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_activity_report> list){
        crm_activity_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_activity_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_activity_reportFeignClient.removeBatch(idList);
    }

    @Override
    public Crm_activity_report get(Integer id) {
		Crm_activity_report et=crm_activity_reportFeignClient.get(id);
        if(et==null){
            et=new Crm_activity_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Crm_activity_report getDraft(Crm_activity_report et) {
        et=crm_activity_reportFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_activity_report> searchDefault(Crm_activity_reportSearchContext context) {
        Page<Crm_activity_report> crm_activity_reports=crm_activity_reportFeignClient.searchDefault(context);
        return crm_activity_reports;
    }


}


