package cn.ibizlab.odoo.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;


/**
 * 实体[Purchase_report] 服务对象接口
 */
public interface IPurchase_reportService{

    boolean update(Purchase_report et) ;
    void updateBatch(List<Purchase_report> list) ;
    boolean create(Purchase_report et) ;
    void createBatch(List<Purchase_report> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Purchase_report get(Integer key) ;
    Purchase_report getDraft(Purchase_report et) ;
    Page<Purchase_report> searchDefault(Purchase_reportSearchContext context) ;

}



