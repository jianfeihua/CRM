package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [event_event] 对象
 */
public interface Ievent_event {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setAddress_id(Integer address_id);
    
    /**
     * 设置 [地点]
     */
    public Integer getAddress_id();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getAddress_idDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setAddress_id_text(String address_id_text);
    
    /**
     * 设置 [地点]
     */
    public String getAddress_id_text();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getAddress_id_textDirtyFlag();
    /**
     * 获取 [自动确认注册]
     */
    public void setAuto_confirm(String auto_confirm);
    
    /**
     * 设置 [自动确认注册]
     */
    public String getAuto_confirm();

    /**
     * 获取 [自动确认注册]脏标记
     */
    public boolean getAuto_confirmDirtyFlag();
    /**
     * 获取 [徽章背面]
     */
    public void setBadge_back(String badge_back);
    
    /**
     * 设置 [徽章背面]
     */
    public String getBadge_back();

    /**
     * 获取 [徽章背面]脏标记
     */
    public boolean getBadge_backDirtyFlag();
    /**
     * 获取 [徽章字体]
     */
    public void setBadge_front(String badge_front);
    
    /**
     * 设置 [徽章字体]
     */
    public String getBadge_front();

    /**
     * 获取 [徽章字体]脏标记
     */
    public boolean getBadge_frontDirtyFlag();
    /**
     * 获取 [徽章内部左边]
     */
    public void setBadge_innerleft(String badge_innerleft);
    
    /**
     * 设置 [徽章内部左边]
     */
    public String getBadge_innerleft();

    /**
     * 获取 [徽章内部左边]脏标记
     */
    public boolean getBadge_innerleftDirtyFlag();
    /**
     * 获取 [徽章内部右边]
     */
    public void setBadge_innerright(String badge_innerright);
    
    /**
     * 设置 [徽章内部右边]
     */
    public String getBadge_innerright();

    /**
     * 获取 [徽章内部右边]脏标记
     */
    public boolean getBadge_innerrightDirtyFlag();
    /**
     * 获取 [看板颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [看板颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [看板颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [国家]
     */
    public Integer getCountry_id();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [国家]
     */
    public String getCountry_id_text();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_begin(Timestamp date_begin);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_begin();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_beginDirtyFlag();
    /**
     * 获取 [定位开始日期]
     */
    public void setDate_begin_located(String date_begin_located);
    
    /**
     * 设置 [定位开始日期]
     */
    public String getDate_begin_located();

    /**
     * 获取 [定位开始日期]脏标记
     */
    public boolean getDate_begin_locatedDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setDate_end(Timestamp date_end);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getDate_end();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getDate_endDirtyFlag();
    /**
     * 获取 [定位最后日期]
     */
    public void setDate_end_located(String date_end_located);
    
    /**
     * 设置 [定位最后日期]
     */
    public String getDate_end_located();

    /**
     * 获取 [定位最后日期]脏标记
     */
    public boolean getDate_end_locatedDirtyFlag();
    /**
     * 获取 [时区]
     */
    public void setDate_tz(String date_tz);
    
    /**
     * 设置 [时区]
     */
    public String getDate_tz();

    /**
     * 获取 [时区]脏标记
     */
    public boolean getDate_tzDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [事件图标]
     */
    public void setEvent_logo(String event_logo);
    
    /**
     * 设置 [事件图标]
     */
    public String getEvent_logo();

    /**
     * 获取 [事件图标]脏标记
     */
    public boolean getEvent_logoDirtyFlag();
    /**
     * 获取 [邮件排程]
     */
    public void setEvent_mail_ids(String event_mail_ids);
    
    /**
     * 设置 [邮件排程]
     */
    public String getEvent_mail_ids();

    /**
     * 获取 [邮件排程]脏标记
     */
    public boolean getEvent_mail_idsDirtyFlag();
    /**
     * 获取 [活动入场券]
     */
    public void setEvent_ticket_ids(String event_ticket_ids);
    
    /**
     * 设置 [活动入场券]
     */
    public String getEvent_ticket_ids();

    /**
     * 获取 [活动入场券]脏标记
     */
    public boolean getEvent_ticket_idsDirtyFlag();
    /**
     * 获取 [类别]
     */
    public void setEvent_type_id(Integer event_type_id);
    
    /**
     * 设置 [类别]
     */
    public Integer getEvent_type_id();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getEvent_type_idDirtyFlag();
    /**
     * 获取 [类别]
     */
    public void setEvent_type_id_text(String event_type_id_text);
    
    /**
     * 设置 [类别]
     */
    public String getEvent_type_id_text();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getEvent_type_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [在线活动]
     */
    public void setIs_online(String is_online);
    
    /**
     * 设置 [在线活动]
     */
    public String getIs_online();

    /**
     * 获取 [在线活动]脏标记
     */
    public boolean getIs_onlineDirtyFlag();
    /**
     * 获取 [正在参加]
     */
    public void setIs_participating(String is_participating);
    
    /**
     * 设置 [正在参加]
     */
    public String getIs_participating();

    /**
     * 获取 [正在参加]脏标记
     */
    public boolean getIs_participatingDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setIs_published(String is_published);
    
    /**
     * 设置 [已发布]
     */
    public String getIs_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getIs_publishedDirtyFlag();
    /**
     * 获取 [SEO优化]
     */
    public void setIs_seo_optimized(String is_seo_optimized);
    
    /**
     * 设置 [SEO优化]
     */
    public String getIs_seo_optimized();

    /**
     * 获取 [SEO优化]脏标记
     */
    public boolean getIs_seo_optimizedDirtyFlag();
    /**
     * 获取 [活动菜单]
     */
    public void setMenu_id(Integer menu_id);
    
    /**
     * 设置 [活动菜单]
     */
    public Integer getMenu_id();

    /**
     * 获取 [活动菜单]脏标记
     */
    public boolean getMenu_idDirtyFlag();
    /**
     * 获取 [活动菜单]
     */
    public void setMenu_id_text(String menu_id_text);
    
    /**
     * 设置 [活动菜单]
     */
    public String getMenu_id_text();

    /**
     * 获取 [活动菜单]脏标记
     */
    public boolean getMenu_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setName(String name);
    
    /**
     * 设置 [活动]
     */
    public String getName();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [组织者]
     */
    public void setOrganizer_id(Integer organizer_id);
    
    /**
     * 设置 [组织者]
     */
    public Integer getOrganizer_id();

    /**
     * 获取 [组织者]脏标记
     */
    public boolean getOrganizer_idDirtyFlag();
    /**
     * 获取 [组织者]
     */
    public void setOrganizer_id_text(String organizer_id_text);
    
    /**
     * 设置 [组织者]
     */
    public String getOrganizer_id_text();

    /**
     * 获取 [组织者]脏标记
     */
    public boolean getOrganizer_id_textDirtyFlag();
    /**
     * 获取 [与会者]
     */
    public void setRegistration_ids(String registration_ids);
    
    /**
     * 设置 [与会者]
     */
    public String getRegistration_ids();

    /**
     * 获取 [与会者]脏标记
     */
    public boolean getRegistration_idsDirtyFlag();
    /**
     * 获取 [与会者最多人数]
     */
    public void setSeats_availability(String seats_availability);
    
    /**
     * 设置 [与会者最多人数]
     */
    public String getSeats_availability();

    /**
     * 获取 [与会者最多人数]脏标记
     */
    public boolean getSeats_availabilityDirtyFlag();
    /**
     * 获取 [可用席位]
     */
    public void setSeats_available(Integer seats_available);
    
    /**
     * 设置 [可用席位]
     */
    public Integer getSeats_available();

    /**
     * 获取 [可用席位]脏标记
     */
    public boolean getSeats_availableDirtyFlag();
    /**
     * 获取 [预期的与会人员数量]
     */
    public void setSeats_expected(Integer seats_expected);
    
    /**
     * 设置 [预期的与会人员数量]
     */
    public Integer getSeats_expected();

    /**
     * 获取 [预期的与会人员数量]脏标记
     */
    public boolean getSeats_expectedDirtyFlag();
    /**
     * 获取 [与会者最多人数]
     */
    public void setSeats_max(Integer seats_max);
    
    /**
     * 设置 [与会者最多人数]
     */
    public Integer getSeats_max();

    /**
     * 获取 [与会者最多人数]脏标记
     */
    public boolean getSeats_maxDirtyFlag();
    /**
     * 获取 [与会者最少数量]
     */
    public void setSeats_min(Integer seats_min);
    
    /**
     * 设置 [与会者最少数量]
     */
    public Integer getSeats_min();

    /**
     * 获取 [与会者最少数量]脏标记
     */
    public boolean getSeats_minDirtyFlag();
    /**
     * 获取 [预订席位]
     */
    public void setSeats_reserved(Integer seats_reserved);
    
    /**
     * 设置 [预订席位]
     */
    public Integer getSeats_reserved();

    /**
     * 获取 [预订席位]脏标记
     */
    public boolean getSeats_reservedDirtyFlag();
    /**
     * 获取 [未确认的席位预订]
     */
    public void setSeats_unconfirmed(Integer seats_unconfirmed);
    
    /**
     * 设置 [未确认的席位预订]
     */
    public Integer getSeats_unconfirmed();

    /**
     * 获取 [未确认的席位预订]脏标记
     */
    public boolean getSeats_unconfirmedDirtyFlag();
    /**
     * 获取 [参与者数目]
     */
    public void setSeats_used(Integer seats_used);
    
    /**
     * 设置 [参与者数目]
     */
    public Integer getSeats_used();

    /**
     * 获取 [参与者数目]脏标记
     */
    public boolean getSeats_usedDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [Twitter主题标签]
     */
    public void setTwitter_hashtag(String twitter_hashtag);
    
    /**
     * 设置 [Twitter主题标签]
     */
    public String getTwitter_hashtag();

    /**
     * 获取 [Twitter主题标签]脏标记
     */
    public boolean getTwitter_hashtagDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [专用菜单]
     */
    public void setWebsite_menu(String website_menu);
    
    /**
     * 设置 [专用菜单]
     */
    public String getWebsite_menu();

    /**
     * 获取 [专用菜单]脏标记
     */
    public boolean getWebsite_menuDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [网站元说明]
     */
    public void setWebsite_meta_description(String website_meta_description);
    
    /**
     * 设置 [网站元说明]
     */
    public String getWebsite_meta_description();

    /**
     * 获取 [网站元说明]脏标记
     */
    public boolean getWebsite_meta_descriptionDirtyFlag();
    /**
     * 获取 [网站meta关键词]
     */
    public void setWebsite_meta_keywords(String website_meta_keywords);
    
    /**
     * 设置 [网站meta关键词]
     */
    public String getWebsite_meta_keywords();

    /**
     * 获取 [网站meta关键词]脏标记
     */
    public boolean getWebsite_meta_keywordsDirtyFlag();
    /**
     * 获取 [网站opengraph图像]
     */
    public void setWebsite_meta_og_img(String website_meta_og_img);
    
    /**
     * 设置 [网站opengraph图像]
     */
    public String getWebsite_meta_og_img();

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    public boolean getWebsite_meta_og_imgDirtyFlag();
    /**
     * 获取 [网站meta标题]
     */
    public void setWebsite_meta_title(String website_meta_title);
    
    /**
     * 设置 [网站meta标题]
     */
    public String getWebsite_meta_title();

    /**
     * 获取 [网站meta标题]脏标记
     */
    public boolean getWebsite_meta_titleDirtyFlag();
    /**
     * 获取 [在当前网站显示]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [在当前网站显示]
     */
    public String getWebsite_published();

    /**
     * 获取 [在当前网站显示]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
