package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_page] 服务对象接口
 */
@Component
public class website_pageFallback implements website_pageFeignClient{

    public Page<Website_page> searchDefault(Website_pageSearchContext context){
            return null;
     }



    public Website_page update(Integer id, Website_page website_page){
            return null;
     }
    public Boolean updateBatch(List<Website_page> website_pages){
            return false;
     }


    public Website_page get(Integer id){
            return null;
     }


    public Website_page create(Website_page website_page){
            return null;
     }
    public Boolean createBatch(List<Website_page> website_pages){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Website_page> select(){
            return null;
     }

    public Website_page getDraft(){
            return null;
    }



}
