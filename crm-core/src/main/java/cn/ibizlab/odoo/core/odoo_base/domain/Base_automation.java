package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [自动动作] 对象
 */
@Data
public class Base_automation extends EntityClient implements Serializable {

    /**
     * 绑定模型
     */
    @JSONField(name = "binding_model_id")
    @JsonProperty("binding_model_id")
    private Integer bindingModelId;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 更新前域表达式
     */
    @DEField(name = "filter_pre_domain")
    @JSONField(name = "filter_pre_domain")
    @JsonProperty("filter_pre_domain")
    private String filterPreDomain;

    /**
     * 变化字段的触发器
     */
    @DEField(name = "on_change_fields")
    @JSONField(name = "on_change_fields")
    @JsonProperty("on_change_fields")
    private String onChangeFields;

    /**
     * 摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 用户字段名字
     */
    @JSONField(name = "activity_user_field_name")
    @JsonProperty("activity_user_field_name")
    private String activityUserFieldName;

    /**
     * 可用于网站
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 用途
     */
    @JSONField(name = "usage")
    @JsonProperty("usage")
    private String usage;

    /**
     * 目标模型
     */
    @JSONField(name = "crud_model_name")
    @JsonProperty("crud_model_name")
    private String crudModelName;

    /**
     * 延迟类型
     */
    @DEField(name = "trg_date_range_type")
    @JSONField(name = "trg_date_range_type")
    @JsonProperty("trg_date_range_type")
    private String trgDateRangeType;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 映射的值
     */
    @JSONField(name = "fields_lines")
    @JsonProperty("fields_lines")
    private String fieldsLines;

    /**
     * 备注
     */
    @JSONField(name = "activity_note")
    @JsonProperty("activity_note")
    private String activityNote;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 活动
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * Python 代码
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 活动用户类型
     */
    @JSONField(name = "activity_user_type")
    @JsonProperty("activity_user_type")
    private String activityUserType;

    /**
     * 网站路径
     */
    @JSONField(name = "website_path")
    @JsonProperty("website_path")
    private String websitePath;

    /**
     * 添加关注者
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 触发日期
     */
    @DEField(name = "trg_date_id")
    @JSONField(name = "trg_date_id")
    @JsonProperty("trg_date_id")
    private Integer trgDateId;

    /**
     * 外部 ID
     */
    @JSONField(name = "xml_id")
    @JsonProperty("xml_id")
    private String xmlId;

    /**
     * 最后运行
     */
    @DEField(name = "last_run")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_run" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_run")
    private Timestamp lastRun;

    /**
     * EMail模板
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;

    /**
     * 绑定类型
     */
    @JSONField(name = "binding_type")
    @JsonProperty("binding_type")
    private String bindingType;

    /**
     * 动作名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 模型名称
     */
    @JSONField(name = "model_name")
    @JsonProperty("model_name")
    private String modelName;

    /**
     * 待办的行动
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 添加频道
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 模型
     */
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Integer modelId;

    /**
     * 动作说明
     */
    @JSONField(name = "help")
    @JsonProperty("help")
    private String help;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 负责人
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 截止日期至
     */
    @JSONField(name = "activity_date_deadline_range")
    @JsonProperty("activity_date_deadline_range")
    private Integer activityDateDeadlineRange;

    /**
     * 触发日期后的延迟
     */
    @DEField(name = "trg_date_range")
    @JSONField(name = "trg_date_range")
    @JsonProperty("trg_date_range")
    private Integer trgDateRange;

    /**
     * 动作类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 应用于
     */
    @DEField(name = "filter_domain")
    @JSONField(name = "filter_domain")
    @JsonProperty("filter_domain")
    private String filterDomain;

    /**
     * 到期类型
     */
    @JSONField(name = "activity_date_deadline_range_type")
    @JsonProperty("activity_date_deadline_range_type")
    private String activityDateDeadlineRangeType;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 下级动作
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 链接使用字段
     */
    @JSONField(name = "link_field_id")
    @JsonProperty("link_field_id")
    private Integer linkFieldId;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 触发条件
     */
    @JSONField(name = "trigger")
    @JsonProperty("trigger")
    private String trigger;

    /**
     * 服务器动作
     */
    @DEField(name = "action_server_id")
    @JSONField(name = "action_server_id")
    @JsonProperty("action_server_id")
    private Integer actionServerId;

    /**
     * 创建/写目标模型
     */
    @JSONField(name = "crud_model_id")
    @JsonProperty("crud_model_id")
    private Integer crudModelId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 使用日历
     */
    @JSONField(name = "trg_date_calendar_id_text")
    @JsonProperty("trg_date_calendar_id_text")
    private String trgDateCalendarIdText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 使用日历
     */
    @DEField(name = "trg_date_calendar_id")
    @JSONField(name = "trg_date_calendar_id")
    @JsonProperty("trg_date_calendar_id")
    private Integer trgDateCalendarId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odootrgdatecalendar")
    @JsonProperty("odootrgdatecalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooTrgDateCalendar;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [更新前域表达式]
     */
    public void setFilterPreDomain(String filterPreDomain){
        this.filterPreDomain = filterPreDomain ;
        this.modify("filter_pre_domain",filterPreDomain);
    }
    /**
     * 设置 [变化字段的触发器]
     */
    public void setOnChangeFields(String onChangeFields){
        this.onChangeFields = onChangeFields ;
        this.modify("on_change_fields",onChangeFields);
    }
    /**
     * 设置 [延迟类型]
     */
    public void setTrgDateRangeType(String trgDateRangeType){
        this.trgDateRangeType = trgDateRangeType ;
        this.modify("trg_date_range_type",trgDateRangeType);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [触发日期]
     */
    public void setTrgDateId(Integer trgDateId){
        this.trgDateId = trgDateId ;
        this.modify("trg_date_id",trgDateId);
    }
    /**
     * 设置 [最后运行]
     */
    public void setLastRun(Timestamp lastRun){
        this.lastRun = lastRun ;
        this.modify("last_run",lastRun);
    }
    /**
     * 设置 [触发日期后的延迟]
     */
    public void setTrgDateRange(Integer trgDateRange){
        this.trgDateRange = trgDateRange ;
        this.modify("trg_date_range",trgDateRange);
    }
    /**
     * 设置 [应用于]
     */
    public void setFilterDomain(String filterDomain){
        this.filterDomain = filterDomain ;
        this.modify("filter_domain",filterDomain);
    }
    /**
     * 设置 [触发条件]
     */
    public void setTrigger(String trigger){
        this.trigger = trigger ;
        this.modify("trigger",trigger);
    }
    /**
     * 设置 [服务器动作]
     */
    public void setActionServerId(Integer actionServerId){
        this.actionServerId = actionServerId ;
        this.modify("action_server_id",actionServerId);
    }
    /**
     * 设置 [使用日历]
     */
    public void setTrgDateCalendarId(Integer trgDateCalendarId){
        this.trgDateCalendarId = trgDateCalendarId ;
        this.modify("trg_date_calendar_id",trgDateCalendarId);
    }

}


