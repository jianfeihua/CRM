package cn.ibizlab.odoo.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;


/**
 * 实体[Resource_calendar] 服务对象接口
 */
public interface IResource_calendarService{

    Resource_calendar getDraft(Resource_calendar et) ;
    boolean create(Resource_calendar et) ;
    void createBatch(List<Resource_calendar> list) ;
    Resource_calendar get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Resource_calendar et) ;
    void updateBatch(List<Resource_calendar> list) ;
    Page<Resource_calendar> searchDefault(Resource_calendarSearchContext context) ;

}



