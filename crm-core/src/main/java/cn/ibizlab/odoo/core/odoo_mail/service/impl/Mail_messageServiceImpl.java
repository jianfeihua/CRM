package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_messageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_messageFeignClient;

/**
 * 实体[消息] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_messageServiceImpl implements IMail_messageService {

    @Autowired
    mail_messageFeignClient mail_messageFeignClient;


    @Override
    public boolean create(Mail_message et) {
        Mail_message rt = mail_messageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_message> list){
        mail_messageFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_message get(Integer id) {
		Mail_message et=mail_messageFeignClient.get(id);
        if(et==null){
            et=new Mail_message();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean checkKey(Mail_message et) {
        return mail_messageFeignClient.checkKey(et);
    }
    @Override
    public Mail_message getDraft(Mail_message et) {
        et=mail_messageFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_messageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_messageFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Mail_message et) {
        Mail_message rt = mail_messageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_message> list){
        mail_messageFeignClient.updateBatch(list) ;
    }

    @Override
    @Transactional
    public boolean save(Mail_message et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!mail_messageFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Mail_message> list) {
        mail_messageFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_message> searchDefault(Mail_messageSearchContext context) {
        Page<Mail_message> mail_messages=mail_messageFeignClient.searchDefault(context);
        return mail_messages;
    }


}


