package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_shortcodeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_shortcode] 服务对象接口
 */
@Component
public class mail_shortcodeFallback implements mail_shortcodeFeignClient{


    public Mail_shortcode get(Integer id){
            return null;
     }


    public Page<Mail_shortcode> searchDefault(Mail_shortcodeSearchContext context){
            return null;
     }



    public Mail_shortcode create(Mail_shortcode mail_shortcode){
            return null;
     }
    public Boolean createBatch(List<Mail_shortcode> mail_shortcodes){
            return false;
     }


    public Mail_shortcode update(Integer id, Mail_shortcode mail_shortcode){
            return null;
     }
    public Boolean updateBatch(List<Mail_shortcode> mail_shortcodes){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Mail_shortcode> select(){
            return null;
     }

    public Mail_shortcode getDraft(){
            return null;
    }



}
