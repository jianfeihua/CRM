package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [讨论频道] 对象
 */
@Data
public class Mail_channel extends EntityClient implements Serializable {

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 中等尺寸照片
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 管理EMail账户
     */
    @JSONField(name = "moderation_count")
    @JsonProperty("moderation_count")
    private Integer moderationCount;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 自动通知
     */
    @DEField(name = "moderation_notify")
    @JSONField(name = "moderation_notify")
    @JsonProperty("moderation_notify")
    private String moderationNotify;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 管理频道
     */
    @JSONField(name = "moderation")
    @JsonProperty("moderation")
    private String moderation;

    /**
     * 管理员
     */
    @JSONField(name = "is_moderator")
    @JsonProperty("is_moderator")
    private String isModerator;

    /**
     * 通知消息
     */
    @DEField(name = "moderation_notify_msg")
    @JSONField(name = "moderation_notify_msg")
    @JsonProperty("moderation_notify_msg")
    private String moderationNotifyMsg;

    /**
     * 管理EMail
     */
    @JSONField(name = "moderation_ids")
    @JsonProperty("moderation_ids")
    private String moderationIds;

    /**
     * 最新图像评级
     */
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 向新用户发送订阅指南
     */
    @DEField(name = "moderation_guidelines")
    @JSONField(name = "moderation_guidelines")
    @JsonProperty("moderation_guidelines")
    private String moderationGuidelines;

    /**
     * 最新反馈评级
     */
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    private String ratingLastFeedback;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 人力资源部门
     */
    @JSONField(name = "subscription_department_ids")
    @JsonProperty("subscription_department_ids")
    private String subscriptionDepartmentIds;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 隐私
     */
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private String ibizpublic;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 评级
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * 匿名用户姓名
     */
    @DEField(name = "anonymous_name")
    @JSONField(name = "anonymous_name")
    @JsonProperty("anonymous_name")
    private String anonymousName;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 成员
     */
    @JSONField(name = "is_member")
    @JsonProperty("is_member")
    private String isMember;

    /**
     * 已订阅
     */
    @JSONField(name = "is_subscribed")
    @JsonProperty("is_subscribed")
    private String isSubscribed;

    /**
     * 方针
     */
    @DEField(name = "moderation_guidelines_msg")
    @JSONField(name = "moderation_guidelines_msg")
    @JsonProperty("moderation_guidelines_msg")
    private String moderationGuidelinesMsg;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 照片
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 小尺寸照片
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 错误个数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 监听器
     */
    @JSONField(name = "channel_partner_ids")
    @JsonProperty("channel_partner_ids")
    private String channelPartnerIds;

    /**
     * 管理员
     */
    @JSONField(name = "moderator_ids")
    @JsonProperty("moderator_ids")
    private String moderatorIds;

    /**
     * 自动订阅
     */
    @JSONField(name = "group_ids")
    @JsonProperty("group_ids")
    private String groupIds;

    /**
     * 以邮件形式发送
     */
    @DEField(name = "email_send")
    @JSONField(name = "email_send")
    @JsonProperty("email_send")
    private String emailSend;

    /**
     * 最近一次查阅
     */
    @JSONField(name = "channel_last_seen_partner_ids")
    @JsonProperty("channel_last_seen_partner_ids")
    private String channelLastSeenPartnerIds;

    /**
     * 前置操作
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * UUID
     */
    @JSONField(name = "uuid")
    @JsonProperty("uuid")
    private String uuid;

    /**
     * 评级数
     */
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 操作次数
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 渠道消息
     */
    @JSONField(name = "channel_message_ids")
    @JsonProperty("channel_message_ids")
    private String channelMessageIds;

    /**
     * 渠道类型
     */
    @DEField(name = "channel_type")
    @JSONField(name = "channel_type")
    @JsonProperty("channel_type")
    private String channelType;

    /**
     * 是聊天
     */
    @JSONField(name = "is_chat")
    @JsonProperty("is_chat")
    private String isChat;

    /**
     * 最新值评级
     */
    @DEField(name = "rating_last_value")
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;

    /**
     * 记录线索ID
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 默认值
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;

    /**
     * 所有者
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Integer aliasUserId;

    /**
     * 上级模型
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 经授权的群组
     */
    @JSONField(name = "group_public_id_text")
    @JsonProperty("group_public_id_text")
    private String groupPublicIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 上级记录ID
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 别名
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 模型别名
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;

    /**
     * 渠道
     */
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    private String livechatChannelIdText;

    /**
     * 网域别名
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 安全联系人别名
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 经授权的群组
     */
    @DEField(name = "group_public_id")
    @JSONField(name = "group_public_id")
    @JsonProperty("group_public_id")
    private Integer groupPublicId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 渠道
     */
    @DEField(name = "livechat_channel_id")
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    private Integer livechatChannelId;


    /**
     * 
     */
    @JSONField(name = "odoolivechatchannel")
    @JsonProperty("odoolivechatchannel")
    private cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel odooLivechatChannel;

    /**
     * 
     */
    @JSONField(name = "odooalias")
    @JsonProperty("odooalias")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JSONField(name = "odoogrouppublic")
    @JsonProperty("odoogrouppublic")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_groups odooGroupPublic;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [自动通知]
     */
    public void setModerationNotify(String moderationNotify){
        this.moderationNotify = moderationNotify ;
        this.modify("moderation_notify",moderationNotify);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [管理频道]
     */
    public void setModeration(String moderation){
        this.moderation = moderation ;
        this.modify("moderation",moderation);
    }
    /**
     * 设置 [通知消息]
     */
    public void setModerationNotifyMsg(String moderationNotifyMsg){
        this.moderationNotifyMsg = moderationNotifyMsg ;
        this.modify("moderation_notify_msg",moderationNotifyMsg);
    }
    /**
     * 设置 [向新用户发送订阅指南]
     */
    public void setModerationGuidelines(String moderationGuidelines){
        this.moderationGuidelines = moderationGuidelines ;
        this.modify("moderation_guidelines",moderationGuidelines);
    }
    /**
     * 设置 [隐私]
     */
    public void setIbizpublic(String ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.modify("ibizpublic",ibizpublic);
    }
    /**
     * 设置 [匿名用户姓名]
     */
    public void setAnonymousName(String anonymousName){
        this.anonymousName = anonymousName ;
        this.modify("anonymous_name",anonymousName);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [方针]
     */
    public void setModerationGuidelinesMsg(String moderationGuidelinesMsg){
        this.moderationGuidelinesMsg = moderationGuidelinesMsg ;
        this.modify("moderation_guidelines_msg",moderationGuidelinesMsg);
    }
    /**
     * 设置 [以邮件形式发送]
     */
    public void setEmailSend(String emailSend){
        this.emailSend = emailSend ;
        this.modify("email_send",emailSend);
    }
    /**
     * 设置 [UUID]
     */
    public void setUuid(String uuid){
        this.uuid = uuid ;
        this.modify("uuid",uuid);
    }
    /**
     * 设置 [渠道类型]
     */
    public void setChannelType(String channelType){
        this.channelType = channelType ;
        this.modify("channel_type",channelType);
    }
    /**
     * 设置 [最新值评级]
     */
    public void setRatingLastValue(Double ratingLastValue){
        this.ratingLastValue = ratingLastValue ;
        this.modify("rating_last_value",ratingLastValue);
    }
    /**
     * 设置 [经授权的群组]
     */
    public void setGroupPublicId(Integer groupPublicId){
        this.groupPublicId = groupPublicId ;
        this.modify("group_public_id",groupPublicId);
    }
    /**
     * 设置 [别名]
     */
    public void setAliasId(Integer aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }
    /**
     * 设置 [渠道]
     */
    public void setLivechatChannelId(Integer livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }

}


