package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_threadSearchContext;


/**
 * 实体[Mail_thread] 服务对象接口
 */
public interface IMail_threadService{

    Mail_thread getDraft(Mail_thread et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_thread et) ;
    void createBatch(List<Mail_thread> list) ;
    Mail_thread get(Integer key) ;
    boolean update(Mail_thread et) ;
    void updateBatch(List<Mail_thread> list) ;
    Page<Mail_thread> searchDefault(Mail_threadSearchContext context) ;

}



