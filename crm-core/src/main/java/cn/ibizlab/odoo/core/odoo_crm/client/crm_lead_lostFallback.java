package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_lostSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_lead_lost] 服务对象接口
 */
@Component
public class crm_lead_lostFallback implements crm_lead_lostFeignClient{



    public Crm_lead_lost get(Integer id){
            return null;
     }


    public Page<Crm_lead_lost> searchDefault(Crm_lead_lostSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Crm_lead_lost update(Integer id, Crm_lead_lost crm_lead_lost){
            return null;
     }
    public Boolean updateBatch(List<Crm_lead_lost> crm_lead_losts){
            return false;
     }



    public Crm_lead_lost create(Crm_lead_lost crm_lead_lost){
            return null;
     }
    public Boolean createBatch(List<Crm_lead_lost> crm_lead_losts){
            return false;
     }

    public Page<Crm_lead_lost> select(){
            return null;
     }

    public Crm_lead_lost getDraft(){
            return null;
    }



}
