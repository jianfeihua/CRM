package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_abstract_paymentSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_abstract_paymentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_abstract_paymentFeignClient;

/**
 * 实体[包含在允许登记付款的模块之间共享的逻辑] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_abstract_paymentServiceImpl implements IAccount_abstract_paymentService {

    @Autowired
    account_abstract_paymentFeignClient account_abstract_paymentFeignClient;


    @Override
    public Account_abstract_payment getDraft(Account_abstract_payment et) {
        et=account_abstract_paymentFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_abstract_payment et) {
        Account_abstract_payment rt = account_abstract_paymentFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_abstract_payment> list){
        account_abstract_paymentFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_abstract_payment get(Integer id) {
		Account_abstract_payment et=account_abstract_paymentFeignClient.get(id);
        if(et==null){
            et=new Account_abstract_payment();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_abstract_paymentFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_abstract_paymentFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_abstract_payment et) {
        Account_abstract_payment rt = account_abstract_paymentFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_abstract_payment> list){
        account_abstract_paymentFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_abstract_payment> searchDefault(Account_abstract_paymentSearchContext context) {
        Page<Account_abstract_payment> account_abstract_payments=account_abstract_paymentFeignClient.searchDefault(context);
        return account_abstract_payments;
    }


}


