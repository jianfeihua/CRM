package cn.ibizlab.odoo.core.odoo_base_import.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_complex;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_complexSearchContext;


/**
 * 实体[Base_import_tests_models_complex] 服务对象接口
 */
public interface IBase_import_tests_models_complexService{

    Base_import_tests_models_complex get(Integer key) ;
    Base_import_tests_models_complex getDraft(Base_import_tests_models_complex et) ;
    boolean create(Base_import_tests_models_complex et) ;
    void createBatch(List<Base_import_tests_models_complex> list) ;
    boolean update(Base_import_tests_models_complex et) ;
    void updateBatch(List<Base_import_tests_models_complex> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_import_tests_models_complex> searchDefault(Base_import_tests_models_complexSearchContext context) ;

}



