package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_module_upgrade;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.base_module_upgrade.Base_module_upgradeModule_infoDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_module_upgrade
 * 属性：Module_info
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_module_upgradeModule_infoDefaultValidator.class})
public @interface Base_module_upgradeModule_infoDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
