package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[resource_test] 服务对象接口
 */
@Component
public class resource_testFallback implements resource_testFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Resource_test create(Resource_test resource_test){
            return null;
     }
    public Boolean createBatch(List<Resource_test> resource_tests){
            return false;
     }

    public Page<Resource_test> searchDefault(Resource_testSearchContext context){
            return null;
     }


    public Resource_test update(Integer id, Resource_test resource_test){
            return null;
     }
    public Boolean updateBatch(List<Resource_test> resource_tests){
            return false;
     }


    public Resource_test get(Integer id){
            return null;
     }




    public Page<Resource_test> select(){
            return null;
     }

    public Resource_test getDraft(){
            return null;
    }



}
