package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_importSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_bank_statement_import] 服务对象接口
 */
@Component
public class account_bank_statement_importFallback implements account_bank_statement_importFeignClient{

    public Account_bank_statement_import update(Integer id, Account_bank_statement_import account_bank_statement_import){
            return null;
     }
    public Boolean updateBatch(List<Account_bank_statement_import> account_bank_statement_imports){
            return false;
     }


    public Account_bank_statement_import get(Integer id){
            return null;
     }


    public Account_bank_statement_import create(Account_bank_statement_import account_bank_statement_import){
            return null;
     }
    public Boolean createBatch(List<Account_bank_statement_import> account_bank_statement_imports){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Account_bank_statement_import> searchDefault(Account_bank_statement_importSearchContext context){
            return null;
     }


    public Page<Account_bank_statement_import> select(){
            return null;
     }

    public Account_bank_statement_import getDraft(){
            return null;
    }



}
