package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_notification] 服务对象接口
 */
@Component
public class mail_notificationFallback implements mail_notificationFeignClient{



    public Mail_notification create(Mail_notification mail_notification){
            return null;
     }
    public Boolean createBatch(List<Mail_notification> mail_notifications){
            return false;
     }

    public Page<Mail_notification> searchDefault(Mail_notificationSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mail_notification get(Integer id){
            return null;
     }


    public Mail_notification update(Integer id, Mail_notification mail_notification){
            return null;
     }
    public Boolean updateBatch(List<Mail_notification> mail_notifications){
            return false;
     }



    public Page<Mail_notification> select(){
            return null;
     }

    public Mail_notification getDraft(){
            return null;
    }



}
