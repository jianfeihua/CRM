package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_tracking_value] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-tracking-value", fallback = mail_tracking_valueFallback.class)
public interface mail_tracking_valueFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values/searchdefault")
    Page<Mail_tracking_value> searchDefault(@RequestBody Mail_tracking_valueSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_tracking_values/{id}")
    Mail_tracking_value get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_tracking_values/{id}")
    Mail_tracking_value update(@PathVariable("id") Integer id,@RequestBody Mail_tracking_value mail_tracking_value);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_tracking_values/batch")
    Boolean updateBatch(@RequestBody List<Mail_tracking_value> mail_tracking_values);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_tracking_values/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_tracking_values/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values")
    Mail_tracking_value create(@RequestBody Mail_tracking_value mail_tracking_value);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values/batch")
    Boolean createBatch(@RequestBody List<Mail_tracking_value> mail_tracking_values);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_tracking_values/select")
    Page<Mail_tracking_value> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_tracking_values/getdraft")
    Mail_tracking_value getDraft();


}
