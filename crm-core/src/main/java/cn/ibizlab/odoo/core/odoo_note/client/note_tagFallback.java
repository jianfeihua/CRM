package cn.ibizlab.odoo.core.odoo_note.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[note_tag] 服务对象接口
 */
@Component
public class note_tagFallback implements note_tagFeignClient{


    public Note_tag get(Integer id){
            return null;
     }


    public Note_tag create(Note_tag note_tag){
            return null;
     }
    public Boolean createBatch(List<Note_tag> note_tags){
            return false;
     }

    public Page<Note_tag> searchDefault(Note_tagSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Note_tag update(Integer id, Note_tag note_tag){
            return null;
     }
    public Boolean updateBatch(List<Note_tag> note_tags){
            return false;
     }


    public Page<Note_tag> select(){
            return null;
     }

    public Note_tag getDraft(){
            return null;
    }



}
