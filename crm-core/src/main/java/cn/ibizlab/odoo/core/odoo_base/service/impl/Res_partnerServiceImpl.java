package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partnerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_partnerFeignClient;

/**
 * 实体[客户] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partnerServiceImpl implements IRes_partnerService {

    @Autowired
    res_partnerFeignClient res_partnerFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=res_partnerFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_partnerFeignClient.removeBatch(idList);
    }

    @Override
    public Res_partner get(Integer id) {
		Res_partner et=res_partnerFeignClient.get(id);
        if(et==null){
            et=new Res_partner();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean checkKey(Res_partner et) {
        return res_partnerFeignClient.checkKey(et);
    }
    @Override
    @Transactional
    public boolean save(Res_partner et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!res_partnerFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Res_partner> list) {
        res_partnerFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean update(Res_partner et) {
        Res_partner rt = res_partnerFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_partner> list){
        res_partnerFeignClient.updateBatch(list) ;
    }

    @Override
    public Res_partner getDraft(Res_partner et) {
        et=res_partnerFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Res_partner et) {
        Res_partner rt = res_partnerFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner> list){
        res_partnerFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 联系人（人）
     */
    @Override
    public Page<Res_partner> searchContacts(Res_partnerSearchContext context) {
        Page<Res_partner> res_partners=res_partnerFeignClient.searchContacts(context);
        return res_partners;
    }

    /**
     * 查询集合 联系人（公司）
     */
    @Override
    public Page<Res_partner> searchCompany(Res_partnerSearchContext context) {
        Page<Res_partner> res_partners=res_partnerFeignClient.searchCompany(context);
        return res_partners;
    }

    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner> searchDefault(Res_partnerSearchContext context) {
        Page<Res_partner> res_partners=res_partnerFeignClient.searchDefault(context);
        return res_partners;
    }


}


