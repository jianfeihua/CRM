package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendarService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_resource.client.resource_calendarFeignClient;

/**
 * 实体[资源工作时间] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_calendarServiceImpl implements IResource_calendarService {

    @Autowired
    resource_calendarFeignClient resource_calendarFeignClient;


    @Override
    public Resource_calendar getDraft(Resource_calendar et) {
        et=resource_calendarFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Resource_calendar et) {
        Resource_calendar rt = resource_calendarFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_calendar> list){
        resource_calendarFeignClient.createBatch(list) ;
    }

    @Override
    public Resource_calendar get(Integer id) {
		Resource_calendar et=resource_calendarFeignClient.get(id);
        if(et==null){
            et=new Resource_calendar();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=resource_calendarFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        resource_calendarFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Resource_calendar et) {
        Resource_calendar rt = resource_calendarFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Resource_calendar> list){
        resource_calendarFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_calendar> searchDefault(Resource_calendarSearchContext context) {
        Page<Resource_calendar> resource_calendars=resource_calendarFeignClient.searchDefault(context);
        return resource_calendars;
    }


}


