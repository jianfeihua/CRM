package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_settingsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_config_settingsFeignClient;

/**
 * 实体[配置设定] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_config_settingsServiceImpl implements IRes_config_settingsService {

    @Autowired
    res_config_settingsFeignClient res_config_settingsFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=res_config_settingsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_config_settingsFeignClient.removeBatch(idList);
    }

    @Override
    public Res_config_settings get(Integer id) {
		Res_config_settings et=res_config_settingsFeignClient.get(id);
        if(et==null){
            et=new Res_config_settings();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Res_config_settings getDraft(Res_config_settings et) {
        et=res_config_settingsFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Res_config_settings et) {
        Res_config_settings rt = res_config_settingsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_config_settings> list){
        res_config_settingsFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Res_config_settings et) {
        Res_config_settings rt = res_config_settingsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_config_settings> list){
        res_config_settingsFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_config_settings> searchDefault(Res_config_settingsSearchContext context) {
        Page<Res_config_settings> res_config_settingss=res_config_settingsFeignClient.searchDefault(context);
        return res_config_settingss;
    }


}


