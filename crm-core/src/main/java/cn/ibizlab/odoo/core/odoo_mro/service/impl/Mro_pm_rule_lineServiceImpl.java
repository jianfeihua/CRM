package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_rule_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_rule_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_rule_lineFeignClient;

/**
 * 实体[Rule for Task] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_rule_lineServiceImpl implements IMro_pm_rule_lineService {

    @Autowired
    mro_pm_rule_lineFeignClient mro_pm_rule_lineFeignClient;


    @Override
    public Mro_pm_rule_line get(Integer id) {
		Mro_pm_rule_line et=mro_pm_rule_lineFeignClient.get(id);
        if(et==null){
            et=new Mro_pm_rule_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mro_pm_rule_line getDraft(Mro_pm_rule_line et) {
        et=mro_pm_rule_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mro_pm_rule_line et) {
        Mro_pm_rule_line rt = mro_pm_rule_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_pm_rule_line> list){
        mro_pm_rule_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_pm_rule_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_pm_rule_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mro_pm_rule_line et) {
        Mro_pm_rule_line rt = mro_pm_rule_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_rule_line> list){
        mro_pm_rule_lineFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_rule_line> searchDefault(Mro_pm_rule_lineSearchContext context) {
        Page<Mro_pm_rule_line> mro_pm_rule_lines=mro_pm_rule_lineFeignClient.searchDefault(context);
        return mro_pm_rule_lines;
    }


}


