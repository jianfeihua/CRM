package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-expense-sheet-register-payment-wizard", fallback = hr_expense_sheet_register_payment_wizardFallback.class)
public interface hr_expense_sheet_register_payment_wizardFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheet_register_payment_wizards/{id}")
    Hr_expense_sheet_register_payment_wizard update(@PathVariable("id") Integer id,@RequestBody Hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheet_register_payment_wizards/batch")
    Boolean updateBatch(@RequestBody List<Hr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards")
    Hr_expense_sheet_register_payment_wizard create(@RequestBody Hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards/batch")
    Boolean createBatch(@RequestBody List<Hr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards/searchdefault")
    Page<Hr_expense_sheet_register_payment_wizard> searchDefault(@RequestBody Hr_expense_sheet_register_payment_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheet_register_payment_wizards/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheet_register_payment_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheet_register_payment_wizards/{id}")
    Hr_expense_sheet_register_payment_wizard get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheet_register_payment_wizards/select")
    Page<Hr_expense_sheet_register_payment_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheet_register_payment_wizards/getdraft")
    Hr_expense_sheet_register_payment_wizard getDraft();


}
