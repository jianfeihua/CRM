package cn.ibizlab.odoo.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[purchase_order] 服务对象接口
 */
@Component
public class purchase_orderFallback implements purchase_orderFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Purchase_order> searchDefault(Purchase_orderSearchContext context){
            return null;
     }


    public Purchase_order update(Integer id, Purchase_order purchase_order){
            return null;
     }
    public Boolean updateBatch(List<Purchase_order> purchase_orders){
            return false;
     }





    public Purchase_order get(Integer id){
            return null;
     }


    public Purchase_order create(Purchase_order purchase_order){
            return null;
     }
    public Boolean createBatch(List<Purchase_order> purchase_orders){
            return false;
     }

    public Page<Purchase_order> select(){
            return null;
     }

    public Purchase_order getDraft(){
            return null;
    }



}
