package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_model_brandService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_model_brandFeignClient;

/**
 * 实体[车辆品牌] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_model_brandServiceImpl implements IFleet_vehicle_model_brandService {

    @Autowired
    fleet_vehicle_model_brandFeignClient fleet_vehicle_model_brandFeignClient;


    @Override
    public boolean create(Fleet_vehicle_model_brand et) {
        Fleet_vehicle_model_brand rt = fleet_vehicle_model_brandFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_model_brand> list){
        fleet_vehicle_model_brandFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Fleet_vehicle_model_brand et) {
        Fleet_vehicle_model_brand rt = fleet_vehicle_model_brandFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_model_brand> list){
        fleet_vehicle_model_brandFeignClient.updateBatch(list) ;
    }

    @Override
    public Fleet_vehicle_model_brand get(Integer id) {
		Fleet_vehicle_model_brand et=fleet_vehicle_model_brandFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_model_brand();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_model_brandFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_model_brandFeignClient.removeBatch(idList);
    }

    @Override
    public Fleet_vehicle_model_brand getDraft(Fleet_vehicle_model_brand et) {
        et=fleet_vehicle_model_brandFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_model_brand> searchDefault(Fleet_vehicle_model_brandSearchContext context) {
        Page<Fleet_vehicle_model_brand> fleet_vehicle_model_brands=fleet_vehicle_model_brandFeignClient.searchDefault(context);
        return fleet_vehicle_model_brands;
    }


}


