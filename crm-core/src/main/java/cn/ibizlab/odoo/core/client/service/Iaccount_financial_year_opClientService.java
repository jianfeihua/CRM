package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_financial_year_op;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_financial_year_op] 服务对象接口
 */
public interface Iaccount_financial_year_opClientService{

    public Iaccount_financial_year_op createModel() ;

    public void remove(Iaccount_financial_year_op account_financial_year_op);

    public void removeBatch(List<Iaccount_financial_year_op> account_financial_year_ops);

    public void updateBatch(List<Iaccount_financial_year_op> account_financial_year_ops);

    public void createBatch(List<Iaccount_financial_year_op> account_financial_year_ops);

    public Page<Iaccount_financial_year_op> fetchDefault(SearchContext context);

    public void get(Iaccount_financial_year_op account_financial_year_op);

    public void update(Iaccount_financial_year_op account_financial_year_op);

    public void create(Iaccount_financial_year_op account_financial_year_op);

    public Page<Iaccount_financial_year_op> select(SearchContext context);

    public void getDraft(Iaccount_financial_year_op account_financial_year_op);

}
