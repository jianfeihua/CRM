package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_userSearchContext;


/**
 * 实体[Gamification_badge_user] 服务对象接口
 */
public interface IGamification_badge_userService{

    Gamification_badge_user getDraft(Gamification_badge_user et) ;
    Gamification_badge_user get(Integer key) ;
    boolean update(Gamification_badge_user et) ;
    void updateBatch(List<Gamification_badge_user> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Gamification_badge_user et) ;
    void createBatch(List<Gamification_badge_user> list) ;
    Page<Gamification_badge_user> searchDefault(Gamification_badge_userSearchContext context) ;

}



