package cn.ibizlab.odoo.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;


/**
 * 实体[Resource_calendar_attendance] 服务对象接口
 */
public interface IResource_calendar_attendanceService{

    Resource_calendar_attendance getDraft(Resource_calendar_attendance et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Resource_calendar_attendance get(Integer key) ;
    boolean update(Resource_calendar_attendance et) ;
    void updateBatch(List<Resource_calendar_attendance> list) ;
    boolean create(Resource_calendar_attendance et) ;
    void createBatch(List<Resource_calendar_attendance> list) ;
    Page<Resource_calendar_attendance> searchDefault(Resource_calendar_attendanceSearchContext context) ;

}



