package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_product] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-product", fallback = product_productFallback.class)
public interface product_productFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/product_products")
    Product_product create(@RequestBody Product_product product_product);

    @RequestMapping(method = RequestMethod.POST, value = "/product_products/batch")
    Boolean createBatch(@RequestBody List<Product_product> product_products);


    @RequestMapping(method = RequestMethod.GET, value = "/product_products/{id}")
    Product_product get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/product_products/searchdefault")
    Page<Product_product> searchDefault(@RequestBody Product_productSearchContext context);




    @RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_products/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/product_products/{id}")
    Product_product update(@PathVariable("id") Integer id,@RequestBody Product_product product_product);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_products/batch")
    Boolean updateBatch(@RequestBody List<Product_product> product_products);


    @RequestMapping(method = RequestMethod.GET, value = "/product_products/select")
    Page<Product_product> select();


    @RequestMapping(method = RequestMethod.POST, value = "/product_products/save")
    Boolean save(@RequestBody Product_product product_product);

    @RequestMapping(method = RequestMethod.POST, value = "/product_products/save")
    Boolean saveBatch(@RequestBody List<Product_product> product_products);


    @RequestMapping(method = RequestMethod.POST, value = "/product_products/checkkey")
    Boolean checkKey(@RequestBody Product_product product_product);


    @RequestMapping(method = RequestMethod.GET, value = "/product_products/getdraft")
    Product_product getDraft();


}
