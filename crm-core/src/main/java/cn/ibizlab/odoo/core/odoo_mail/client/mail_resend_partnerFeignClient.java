package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_partnerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_resend_partner] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-resend-partner", fallback = mail_resend_partnerFallback.class)
public interface mail_resend_partnerFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_partners/{id}")
    Mail_resend_partner get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_partners/{id}")
    Mail_resend_partner update(@PathVariable("id") Integer id,@RequestBody Mail_resend_partner mail_resend_partner);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_partners/batch")
    Boolean updateBatch(@RequestBody List<Mail_resend_partner> mail_resend_partners);





    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners/searchdefault")
    Page<Mail_resend_partner> searchDefault(@RequestBody Mail_resend_partnerSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners")
    Mail_resend_partner create(@RequestBody Mail_resend_partner mail_resend_partner);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners/batch")
    Boolean createBatch(@RequestBody List<Mail_resend_partner> mail_resend_partners);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_partners/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_partners/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_partners/select")
    Page<Mail_resend_partner> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_partners/getdraft")
    Mail_resend_partner getDraft();


}
