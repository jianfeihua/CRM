package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_bankSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_bank] 服务对象接口
 */
@Component
public class res_bankFallback implements res_bankFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Res_bank> searchDefault(Res_bankSearchContext context){
            return null;
     }




    public Res_bank update(Integer id, Res_bank res_bank){
            return null;
     }
    public Boolean updateBatch(List<Res_bank> res_banks){
            return false;
     }



    public Res_bank get(Integer id){
            return null;
     }


    public Res_bank create(Res_bank res_bank){
            return null;
     }
    public Boolean createBatch(List<Res_bank> res_banks){
            return false;
     }

    public Page<Res_bank> select(){
            return null;
     }

    public Res_bank getDraft(){
            return null;
    }



}
