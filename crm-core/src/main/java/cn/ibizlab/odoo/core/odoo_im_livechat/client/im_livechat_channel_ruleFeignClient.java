package cn.ibizlab.odoo.core.odoo_im_livechat.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[im_livechat_channel_rule] 服务对象接口
 */
@FeignClient(value = "odoo-im-livechat", contextId = "im-livechat-channel-rule", fallback = im_livechat_channel_ruleFallback.class)
public interface im_livechat_channel_ruleFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules/searchdefault")
    Page<Im_livechat_channel_rule> searchDefault(@RequestBody Im_livechat_channel_ruleSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channel_rules/{id}")
    Im_livechat_channel_rule get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channel_rules/{id}")
    Im_livechat_channel_rule update(@PathVariable("id") Integer id,@RequestBody Im_livechat_channel_rule im_livechat_channel_rule);

    @RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channel_rules/batch")
    Boolean updateBatch(@RequestBody List<Im_livechat_channel_rule> im_livechat_channel_rules);


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules")
    Im_livechat_channel_rule create(@RequestBody Im_livechat_channel_rule im_livechat_channel_rule);

    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules/batch")
    Boolean createBatch(@RequestBody List<Im_livechat_channel_rule> im_livechat_channel_rules);


    @RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channel_rules/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channel_rules/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channel_rules/select")
    Page<Im_livechat_channel_rule> select();


    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channel_rules/getdraft")
    Im_livechat_channel_rule getDraft();


}
