package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_model_brand] 服务对象接口
 */
@Component
public class fleet_vehicle_model_brandFallback implements fleet_vehicle_model_brandFeignClient{




    public Page<Fleet_vehicle_model_brand> searchDefault(Fleet_vehicle_model_brandSearchContext context){
            return null;
     }


    public Fleet_vehicle_model_brand get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Fleet_vehicle_model_brand update(Integer id, Fleet_vehicle_model_brand fleet_vehicle_model_brand){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_model_brand> fleet_vehicle_model_brands){
            return false;
     }


    public Fleet_vehicle_model_brand create(Fleet_vehicle_model_brand fleet_vehicle_model_brand){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_model_brand> fleet_vehicle_model_brands){
            return false;
     }

    public Page<Fleet_vehicle_model_brand> select(){
            return null;
     }

    public Fleet_vehicle_model_brand getDraft(){
            return null;
    }



}
