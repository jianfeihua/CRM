package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statementService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statementFeignClient;

/**
 * 实体[银行对账单] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statementServiceImpl implements IAccount_bank_statementService {

    @Autowired
    account_bank_statementFeignClient account_bank_statementFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_bank_statementFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_bank_statementFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_bank_statement et) {
        Account_bank_statement rt = account_bank_statementFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement> list){
        account_bank_statementFeignClient.createBatch(list) ;
    }

    @Override
    public Account_bank_statement get(Integer id) {
		Account_bank_statement et=account_bank_statementFeignClient.get(id);
        if(et==null){
            et=new Account_bank_statement();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_bank_statement getDraft(Account_bank_statement et) {
        et=account_bank_statementFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_bank_statement et) {
        Account_bank_statement rt = account_bank_statementFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_bank_statement> list){
        account_bank_statementFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement> searchDefault(Account_bank_statementSearchContext context) {
        Page<Account_bank_statement> account_bank_statements=account_bank_statementFeignClient.searchDefault(context);
        return account_bank_statements;
    }


}


