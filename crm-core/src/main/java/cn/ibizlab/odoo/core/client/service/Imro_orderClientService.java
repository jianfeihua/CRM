package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_order] 服务对象接口
 */
public interface Imro_orderClientService{

    public Imro_order createModel() ;

    public void update(Imro_order mro_order);

    public void createBatch(List<Imro_order> mro_orders);

    public void removeBatch(List<Imro_order> mro_orders);

    public void create(Imro_order mro_order);

    public void updateBatch(List<Imro_order> mro_orders);

    public void get(Imro_order mro_order);

    public void remove(Imro_order mro_order);

    public Page<Imro_order> fetchDefault(SearchContext context);

    public Page<Imro_order> select(SearchContext context);

    public void getDraft(Imro_order mro_order);

}
