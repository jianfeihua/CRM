package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_full_reconcile] 对象
 */
public interface Iaccount_full_reconcile {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [外汇交易]
     */
    public void setExchange_move_id(Integer exchange_move_id);
    
    /**
     * 设置 [外汇交易]
     */
    public Integer getExchange_move_id();

    /**
     * 获取 [外汇交易]脏标记
     */
    public boolean getExchange_move_idDirtyFlag();
    /**
     * 获取 [外汇交易]
     */
    public void setExchange_move_id_text(String exchange_move_id_text);
    
    /**
     * 设置 [外汇交易]
     */
    public String getExchange_move_id_text();

    /**
     * 获取 [外汇交易]脏标记
     */
    public boolean getExchange_move_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [号码]
     */
    public void setName(String name);
    
    /**
     * 设置 [号码]
     */
    public String getName();

    /**
     * 获取 [号码]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [ 核销部分]
     */
    public void setPartial_reconcile_ids(String partial_reconcile_ids);
    
    /**
     * 设置 [ 核销部分]
     */
    public String getPartial_reconcile_ids();

    /**
     * 获取 [ 核销部分]脏标记
     */
    public boolean getPartial_reconcile_idsDirtyFlag();
    /**
     * 获取 [匹配的日记账项目]
     */
    public void setReconciled_line_ids(String reconciled_line_ids);
    
    /**
     * 设置 [匹配的日记账项目]
     */
    public String getReconciled_line_ids();

    /**
     * 获取 [匹配的日记账项目]脏标记
     */
    public boolean getReconciled_line_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
