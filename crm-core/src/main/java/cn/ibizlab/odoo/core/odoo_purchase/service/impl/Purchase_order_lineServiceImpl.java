package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_order_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_order_lineFeignClient;

/**
 * 实体[采购订单行] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_order_lineServiceImpl implements IPurchase_order_lineService {

    @Autowired
    purchase_order_lineFeignClient purchase_order_lineFeignClient;


    @Override
    public boolean create(Purchase_order_line et) {
        Purchase_order_line rt = purchase_order_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_order_line> list){
        purchase_order_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Purchase_order_line getDraft(Purchase_order_line et) {
        et=purchase_order_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Purchase_order_line et) {
        Purchase_order_line rt = purchase_order_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Purchase_order_line> list){
        purchase_order_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public Purchase_order_line get(Integer id) {
		Purchase_order_line et=purchase_order_lineFeignClient.get(id);
        if(et==null){
            et=new Purchase_order_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=purchase_order_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        purchase_order_lineFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_order_line> searchDefault(Purchase_order_lineSearchContext context) {
        Page<Purchase_order_line> purchase_order_lines=purchase_order_lineFeignClient.searchDefault(context);
        return purchase_order_lines;
    }


}


