package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_upgradeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_module_upgrade] 服务对象接口
 */
@Component
public class base_module_upgradeFallback implements base_module_upgradeFeignClient{

    public Base_module_upgrade update(Integer id, Base_module_upgrade base_module_upgrade){
            return null;
     }
    public Boolean updateBatch(List<Base_module_upgrade> base_module_upgrades){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_module_upgrade get(Integer id){
            return null;
     }


    public Page<Base_module_upgrade> searchDefault(Base_module_upgradeSearchContext context){
            return null;
     }



    public Base_module_upgrade create(Base_module_upgrade base_module_upgrade){
            return null;
     }
    public Boolean createBatch(List<Base_module_upgrade> base_module_upgrades){
            return false;
     }


    public Page<Base_module_upgrade> select(){
            return null;
     }

    public Base_module_upgrade getDraft(){
            return null;
    }



}
