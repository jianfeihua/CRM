package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goal_definitionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_goal_definitionFeignClient;

/**
 * 实体[游戏化目标定义] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_goal_definitionServiceImpl implements IGamification_goal_definitionService {

    @Autowired
    gamification_goal_definitionFeignClient gamification_goal_definitionFeignClient;


    @Override
    public Gamification_goal_definition get(Integer id) {
		Gamification_goal_definition et=gamification_goal_definitionFeignClient.get(id);
        if(et==null){
            et=new Gamification_goal_definition();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Gamification_goal_definition getDraft(Gamification_goal_definition et) {
        et=gamification_goal_definitionFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Gamification_goal_definition et) {
        Gamification_goal_definition rt = gamification_goal_definitionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_goal_definition> list){
        gamification_goal_definitionFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Gamification_goal_definition et) {
        Gamification_goal_definition rt = gamification_goal_definitionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_goal_definition> list){
        gamification_goal_definitionFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_goal_definitionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_goal_definitionFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_goal_definition> searchDefault(Gamification_goal_definitionSearchContext context) {
        Page<Gamification_goal_definition> gamification_goal_definitions=gamification_goal_definitionFeignClient.searchDefault(context);
        return gamification_goal_definitions;
    }


}


