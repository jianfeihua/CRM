package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_tagFeignClient;

/**
 * 实体[群发邮件标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_tagServiceImpl implements IMail_mass_mailing_tagService {

    @Autowired
    mail_mass_mailing_tagFeignClient mail_mass_mailing_tagFeignClient;


    @Override
    public Mail_mass_mailing_tag getDraft(Mail_mass_mailing_tag et) {
        et=mail_mass_mailing_tagFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_mass_mailing_tag et) {
        Mail_mass_mailing_tag rt = mail_mass_mailing_tagFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mass_mailing_tag> list){
        mail_mass_mailing_tagFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_mass_mailing_tag et) {
        Mail_mass_mailing_tag rt = mail_mass_mailing_tagFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_tag> list){
        mail_mass_mailing_tagFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mass_mailing_tagFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mass_mailing_tagFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_mass_mailing_tag get(Integer id) {
		Mail_mass_mailing_tag et=mail_mass_mailing_tagFeignClient.get(id);
        if(et==null){
            et=new Mail_mass_mailing_tag();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_tag> searchDefault(Mail_mass_mailing_tagSearchContext context) {
        Page<Mail_mass_mailing_tag> mail_mass_mailing_tags=mail_mass_mailing_tagFeignClient.searchDefault(context);
        return mail_mass_mailing_tags;
    }


}


