package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_change_product_qty] 对象
 */
public interface Istock_change_product_qty {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [新的在手数量]
     */
    public void setNew_quantity(Double new_quantity);
    
    /**
     * 设置 [新的在手数量]
     */
    public Double getNew_quantity();

    /**
     * 获取 [新的在手数量]脏标记
     */
    public boolean getNew_quantityDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [模板]
     */
    public void setProduct_tmpl_id_text(String product_tmpl_id_text);
    
    /**
     * 设置 [模板]
     */
    public String getProduct_tmpl_id_text();

    /**
     * 获取 [模板]脏标记
     */
    public boolean getProduct_tmpl_id_textDirtyFlag();
    /**
     * 获取 [变体计数]
     */
    public void setProduct_variant_count(Integer product_variant_count);
    
    /**
     * 设置 [变体计数]
     */
    public Integer getProduct_variant_count();

    /**
     * 获取 [变体计数]脏标记
     */
    public boolean getProduct_variant_countDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
