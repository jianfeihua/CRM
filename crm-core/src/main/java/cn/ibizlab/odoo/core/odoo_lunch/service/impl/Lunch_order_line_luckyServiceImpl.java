package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_line_luckyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_order_line_luckyFeignClient;

/**
 * 实体[幸运工作餐订单明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_order_line_luckyServiceImpl implements ILunch_order_line_luckyService {

    @Autowired
    lunch_order_line_luckyFeignClient lunch_order_line_luckyFeignClient;


    @Override
    public Lunch_order_line_lucky get(Integer id) {
		Lunch_order_line_lucky et=lunch_order_line_luckyFeignClient.get(id);
        if(et==null){
            et=new Lunch_order_line_lucky();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Lunch_order_line_lucky et) {
        Lunch_order_line_lucky rt = lunch_order_line_luckyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_order_line_lucky> list){
        lunch_order_line_luckyFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Lunch_order_line_lucky et) {
        Lunch_order_line_lucky rt = lunch_order_line_luckyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Lunch_order_line_lucky> list){
        lunch_order_line_luckyFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=lunch_order_line_luckyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        lunch_order_line_luckyFeignClient.removeBatch(idList);
    }

    @Override
    public Lunch_order_line_lucky getDraft(Lunch_order_line_lucky et) {
        et=lunch_order_line_luckyFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_order_line_lucky> searchDefault(Lunch_order_line_luckySearchContext context) {
        Page<Lunch_order_line_lucky> lunch_order_line_luckys=lunch_order_line_luckyFeignClient.searchDefault(context);
        return lunch_order_line_luckys;
    }


}


