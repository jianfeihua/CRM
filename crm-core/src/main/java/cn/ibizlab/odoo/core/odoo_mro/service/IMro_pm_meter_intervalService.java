package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;


/**
 * 实体[Mro_pm_meter_interval] 服务对象接口
 */
public interface IMro_pm_meter_intervalService{

    Mro_pm_meter_interval getDraft(Mro_pm_meter_interval et) ;
    boolean create(Mro_pm_meter_interval et) ;
    void createBatch(List<Mro_pm_meter_interval> list) ;
    Mro_pm_meter_interval get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Mro_pm_meter_interval et) ;
    void updateBatch(List<Mro_pm_meter_interval> list) ;
    Page<Mro_pm_meter_interval> searchDefault(Mro_pm_meter_intervalSearchContext context) ;

}



