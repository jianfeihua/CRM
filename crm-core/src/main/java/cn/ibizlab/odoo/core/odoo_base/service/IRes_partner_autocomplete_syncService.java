package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;


/**
 * 实体[Res_partner_autocomplete_sync] 服务对象接口
 */
public interface IRes_partner_autocomplete_syncService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Res_partner_autocomplete_sync et) ;
    void updateBatch(List<Res_partner_autocomplete_sync> list) ;
    Res_partner_autocomplete_sync getDraft(Res_partner_autocomplete_sync et) ;
    Res_partner_autocomplete_sync get(Integer key) ;
    boolean create(Res_partner_autocomplete_sync et) ;
    void createBatch(List<Res_partner_autocomplete_sync> list) ;
    Page<Res_partner_autocomplete_sync> searchDefault(Res_partner_autocomplete_syncSearchContext context) ;

}



