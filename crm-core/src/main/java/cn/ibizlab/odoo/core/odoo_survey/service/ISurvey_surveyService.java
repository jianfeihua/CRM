package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;


/**
 * 实体[Survey_survey] 服务对象接口
 */
public interface ISurvey_surveyService{

    Survey_survey get(Integer key) ;
    boolean create(Survey_survey et) ;
    void createBatch(List<Survey_survey> list) ;
    boolean update(Survey_survey et) ;
    void updateBatch(List<Survey_survey> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Survey_survey getDraft(Survey_survey et) ;
    Page<Survey_survey> searchDefault(Survey_surveySearchContext context) ;

}



