package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_partner_merge_line] 服务对象接口
 */
public interface Ibase_partner_merge_lineClientService{

    public Ibase_partner_merge_line createModel() ;

    public void create(Ibase_partner_merge_line base_partner_merge_line);

    public void update(Ibase_partner_merge_line base_partner_merge_line);

    public void updateBatch(List<Ibase_partner_merge_line> base_partner_merge_lines);

    public void remove(Ibase_partner_merge_line base_partner_merge_line);

    public void createBatch(List<Ibase_partner_merge_line> base_partner_merge_lines);

    public void removeBatch(List<Ibase_partner_merge_line> base_partner_merge_lines);

    public Page<Ibase_partner_merge_line> fetchDefault(SearchContext context);

    public void get(Ibase_partner_merge_line base_partner_merge_line);

    public Page<Ibase_partner_merge_line> select(SearchContext context);

    public void getDraft(Ibase_partner_merge_line base_partner_merge_line);

}
