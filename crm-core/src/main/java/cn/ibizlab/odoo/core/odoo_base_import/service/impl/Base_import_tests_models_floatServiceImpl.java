package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_float;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_floatSearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_floatService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_floatFeignClient;

/**
 * 实体[测试:基本导入模型浮动] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_floatServiceImpl implements IBase_import_tests_models_floatService {

    @Autowired
    base_import_tests_models_floatFeignClient base_import_tests_models_floatFeignClient;


    @Override
    public Base_import_tests_models_float get(Integer id) {
		Base_import_tests_models_float et=base_import_tests_models_floatFeignClient.get(id);
        if(et==null){
            et=new Base_import_tests_models_float();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Base_import_tests_models_float getDraft(Base_import_tests_models_float et) {
        et=base_import_tests_models_floatFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_tests_models_floatFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_tests_models_floatFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Base_import_tests_models_float et) {
        Base_import_tests_models_float rt = base_import_tests_models_floatFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_tests_models_float> list){
        base_import_tests_models_floatFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Base_import_tests_models_float et) {
        Base_import_tests_models_float rt = base_import_tests_models_floatFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_float> list){
        base_import_tests_models_floatFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_float> searchDefault(Base_import_tests_models_floatSearchContext context) {
        Page<Base_import_tests_models_float> base_import_tests_models_floats=base_import_tests_models_floatFeignClient.searchDefault(context);
        return base_import_tests_models_floats;
    }


}


