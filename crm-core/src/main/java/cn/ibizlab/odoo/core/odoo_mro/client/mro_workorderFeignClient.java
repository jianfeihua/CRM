package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_workorderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_workorder] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-workorder", fallback = mro_workorderFallback.class)
public interface mro_workorderFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mro_workorders")
    Mro_workorder create(@RequestBody Mro_workorder mro_workorder);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_workorders/batch")
    Boolean createBatch(@RequestBody List<Mro_workorder> mro_workorders);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_workorders/{id}")
    Mro_workorder update(@PathVariable("id") Integer id,@RequestBody Mro_workorder mro_workorder);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_workorders/batch")
    Boolean updateBatch(@RequestBody List<Mro_workorder> mro_workorders);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_workorders/{id}")
    Mro_workorder get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_workorders/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_workorders/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/mro_workorders/searchdefault")
    Page<Mro_workorder> searchDefault(@RequestBody Mro_workorderSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_workorders/select")
    Page<Mro_workorder> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_workorders/getdraft")
    Mro_workorder getDraft();


}
