package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_tagFeignClient;

/**
 * 实体[分析标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_tagServiceImpl implements IAccount_analytic_tagService {

    @Autowired
    account_analytic_tagFeignClient account_analytic_tagFeignClient;


    @Override
    public Account_analytic_tag getDraft(Account_analytic_tag et) {
        et=account_analytic_tagFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_analytic_tag get(Integer id) {
		Account_analytic_tag et=account_analytic_tagFeignClient.get(id);
        if(et==null){
            et=new Account_analytic_tag();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_analytic_tagFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_analytic_tagFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_analytic_tag et) {
        Account_analytic_tag rt = account_analytic_tagFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_tag> list){
        account_analytic_tagFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_analytic_tag et) {
        Account_analytic_tag rt = account_analytic_tagFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_analytic_tag> list){
        account_analytic_tagFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_tag> searchDefault(Account_analytic_tagSearchContext context) {
        Page<Account_analytic_tag> account_analytic_tags=account_analytic_tagFeignClient.searchDefault(context);
        return account_analytic_tags;
    }


}


