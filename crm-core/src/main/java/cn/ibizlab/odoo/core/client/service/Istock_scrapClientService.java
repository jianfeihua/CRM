package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_scrap;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_scrap] 服务对象接口
 */
public interface Istock_scrapClientService{

    public Istock_scrap createModel() ;

    public Page<Istock_scrap> fetchDefault(SearchContext context);

    public void updateBatch(List<Istock_scrap> stock_scraps);

    public void createBatch(List<Istock_scrap> stock_scraps);

    public void get(Istock_scrap stock_scrap);

    public void remove(Istock_scrap stock_scrap);

    public void create(Istock_scrap stock_scrap);

    public void removeBatch(List<Istock_scrap> stock_scraps);

    public void update(Istock_scrap stock_scrap);

    public Page<Istock_scrap> select(SearchContext context);

    public void getDraft(Istock_scrap stock_scrap);

}
