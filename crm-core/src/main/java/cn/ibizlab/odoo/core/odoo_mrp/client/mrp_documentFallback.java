package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_document] 服务对象接口
 */
@Component
public class mrp_documentFallback implements mrp_documentFeignClient{

    public Page<Mrp_document> searchDefault(Mrp_documentSearchContext context){
            return null;
     }



    public Mrp_document create(Mrp_document mrp_document){
            return null;
     }
    public Boolean createBatch(List<Mrp_document> mrp_documents){
            return false;
     }



    public Mrp_document get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mrp_document update(Integer id, Mrp_document mrp_document){
            return null;
     }
    public Boolean updateBatch(List<Mrp_document> mrp_documents){
            return false;
     }


    public Page<Mrp_document> select(){
            return null;
     }

    public Mrp_document getDraft(){
            return null;
    }



}
