package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_cashbox_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_cashbox_line] 服务对象接口
 */
public interface Iaccount_cashbox_lineClientService{

    public Iaccount_cashbox_line createModel() ;

    public void update(Iaccount_cashbox_line account_cashbox_line);

    public void createBatch(List<Iaccount_cashbox_line> account_cashbox_lines);

    public Page<Iaccount_cashbox_line> fetchDefault(SearchContext context);

    public void get(Iaccount_cashbox_line account_cashbox_line);

    public void create(Iaccount_cashbox_line account_cashbox_line);

    public void updateBatch(List<Iaccount_cashbox_line> account_cashbox_lines);

    public void removeBatch(List<Iaccount_cashbox_line> account_cashbox_lines);

    public void remove(Iaccount_cashbox_line account_cashbox_line);

    public Page<Iaccount_cashbox_line> select(SearchContext context);

    public void getDraft(Iaccount_cashbox_line account_cashbox_line);

}
