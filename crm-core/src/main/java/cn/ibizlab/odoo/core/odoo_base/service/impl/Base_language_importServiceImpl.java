package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_importSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_importService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_language_importFeignClient;

/**
 * 实体[语言导入] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_language_importServiceImpl implements IBase_language_importService {

    @Autowired
    base_language_importFeignClient base_language_importFeignClient;


    @Override
    public boolean create(Base_language_import et) {
        Base_language_import rt = base_language_importFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_language_import> list){
        base_language_importFeignClient.createBatch(list) ;
    }

    @Override
    public Base_language_import getDraft(Base_language_import et) {
        et=base_language_importFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_language_importFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_language_importFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Base_language_import et) {
        Base_language_import rt = base_language_importFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_language_import> list){
        base_language_importFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_language_import get(Integer id) {
		Base_language_import et=base_language_importFeignClient.get(id);
        if(et==null){
            et=new Base_language_import();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_language_import> searchDefault(Base_language_importSearchContext context) {
        Page<Base_language_import> base_language_imports=base_language_importFeignClient.searchDefault(context);
        return base_language_imports;
    }


}


