package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_full_reconcileSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_full_reconcileService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_full_reconcileFeignClient;

/**
 * 实体[完全核销] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_full_reconcileServiceImpl implements IAccount_full_reconcileService {

    @Autowired
    account_full_reconcileFeignClient account_full_reconcileFeignClient;


    @Override
    public boolean create(Account_full_reconcile et) {
        Account_full_reconcile rt = account_full_reconcileFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_full_reconcile> list){
        account_full_reconcileFeignClient.createBatch(list) ;
    }

    @Override
    public Account_full_reconcile getDraft(Account_full_reconcile et) {
        et=account_full_reconcileFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_full_reconcileFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_full_reconcileFeignClient.removeBatch(idList);
    }

    @Override
    public Account_full_reconcile get(Integer id) {
		Account_full_reconcile et=account_full_reconcileFeignClient.get(id);
        if(et==null){
            et=new Account_full_reconcile();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Account_full_reconcile et) {
        Account_full_reconcile rt = account_full_reconcileFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_full_reconcile> list){
        account_full_reconcileFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_full_reconcile> searchDefault(Account_full_reconcileSearchContext context) {
        Page<Account_full_reconcile> account_full_reconciles=account_full_reconcileFeignClient.searchDefault(context);
        return account_full_reconciles;
    }


}


