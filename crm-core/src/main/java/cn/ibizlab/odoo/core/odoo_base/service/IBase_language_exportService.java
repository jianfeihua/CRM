package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_exportSearchContext;


/**
 * 实体[Base_language_export] 服务对象接口
 */
public interface IBase_language_exportService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Base_language_export get(Integer key) ;
    boolean update(Base_language_export et) ;
    void updateBatch(List<Base_language_export> list) ;
    boolean create(Base_language_export et) ;
    void createBatch(List<Base_language_export> list) ;
    Base_language_export getDraft(Base_language_export et) ;
    Page<Base_language_export> searchDefault(Base_language_exportSearchContext context) ;

}



