package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_price_history;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_price_history] 服务对象接口
 */
public interface Iproduct_price_historyClientService{

    public Iproduct_price_history createModel() ;

    public Page<Iproduct_price_history> fetchDefault(SearchContext context);

    public void updateBatch(List<Iproduct_price_history> product_price_histories);

    public void createBatch(List<Iproduct_price_history> product_price_histories);

    public void remove(Iproduct_price_history product_price_history);

    public void create(Iproduct_price_history product_price_history);

    public void get(Iproduct_price_history product_price_history);

    public void update(Iproduct_price_history product_price_history);

    public void removeBatch(List<Iproduct_price_history> product_price_histories);

    public Page<Iproduct_price_history> select(SearchContext context);

    public void getDraft(Iproduct_price_history product_price_history);

}
