package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_moderationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_moderation] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-moderation", fallback = mail_moderationFallback.class)
public interface mail_moderationFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_moderations/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_moderations/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_moderations/searchdefault")
    Page<Mail_moderation> searchDefault(@RequestBody Mail_moderationSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_moderations/{id}")
    Mail_moderation update(@PathVariable("id") Integer id,@RequestBody Mail_moderation mail_moderation);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_moderations/batch")
    Boolean updateBatch(@RequestBody List<Mail_moderation> mail_moderations);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_moderations/{id}")
    Mail_moderation get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_moderations")
    Mail_moderation create(@RequestBody Mail_moderation mail_moderation);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_moderations/batch")
    Boolean createBatch(@RequestBody List<Mail_moderation> mail_moderations);




    @RequestMapping(method = RequestMethod.GET, value = "/mail_moderations/select")
    Page<Mail_moderation> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_moderations/getdraft")
    Mail_moderation getDraft();


}
