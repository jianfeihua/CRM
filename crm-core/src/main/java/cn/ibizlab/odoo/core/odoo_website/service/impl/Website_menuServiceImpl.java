package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_menuService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_website.client.website_menuFeignClient;

/**
 * 实体[网站菜单] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_menuServiceImpl implements IWebsite_menuService {

    @Autowired
    website_menuFeignClient website_menuFeignClient;


    @Override
    public boolean update(Website_menu et) {
        Website_menu rt = website_menuFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Website_menu> list){
        website_menuFeignClient.updateBatch(list) ;
    }

    @Override
    public Website_menu get(Integer id) {
		Website_menu et=website_menuFeignClient.get(id);
        if(et==null){
            et=new Website_menu();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=website_menuFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        website_menuFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Website_menu et) {
        Website_menu rt = website_menuFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_menu> list){
        website_menuFeignClient.createBatch(list) ;
    }

    @Override
    public Website_menu getDraft(Website_menu et) {
        et=website_menuFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_menu> searchDefault(Website_menuSearchContext context) {
        Page<Website_menu> website_menus=website_menuFeignClient.searchDefault(context);
        return website_menus;
    }


}


