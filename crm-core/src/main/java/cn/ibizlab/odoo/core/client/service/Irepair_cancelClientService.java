package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Irepair_cancel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_cancel] 服务对象接口
 */
public interface Irepair_cancelClientService{

    public Irepair_cancel createModel() ;

    public void create(Irepair_cancel repair_cancel);

    public void createBatch(List<Irepair_cancel> repair_cancels);

    public void get(Irepair_cancel repair_cancel);

    public Page<Irepair_cancel> fetchDefault(SearchContext context);

    public void update(Irepair_cancel repair_cancel);

    public void updateBatch(List<Irepair_cancel> repair_cancels);

    public void remove(Irepair_cancel repair_cancel);

    public void removeBatch(List<Irepair_cancel> repair_cancels);

    public Page<Irepair_cancel> select(SearchContext context);

    public void getDraft(Irepair_cancel repair_cancel);

}
