package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_challenge] 服务对象接口
 */
@FeignClient(value = "odoo-gamification", contextId = "gamification-challenge", fallback = gamification_challengeFallback.class)
public interface gamification_challengeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges")
    Gamification_challenge create(@RequestBody Gamification_challenge gamification_challenge);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges/batch")
    Boolean createBatch(@RequestBody List<Gamification_challenge> gamification_challenges);



    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenges/{id}")
    Gamification_challenge update(@PathVariable("id") Integer id,@RequestBody Gamification_challenge gamification_challenge);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenges/batch")
    Boolean updateBatch(@RequestBody List<Gamification_challenge> gamification_challenges);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_challenges/{id}")
    Gamification_challenge get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges/searchdefault")
    Page<Gamification_challenge> searchDefault(@RequestBody Gamification_challengeSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenges/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenges/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/gamification_challenges/select")
    Page<Gamification_challenge> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_challenges/getdraft")
    Gamification_challenge getDraft();


}
