package cn.ibizlab.odoo.core.odoo_rating.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[rating_mixin] 服务对象接口
 */
@FeignClient(value = "odoo-rating", contextId = "rating-mixin", fallback = rating_mixinFallback.class)
public interface rating_mixinFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/rating_mixins")
    Rating_mixin create(@RequestBody Rating_mixin rating_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/rating_mixins/batch")
    Boolean createBatch(@RequestBody List<Rating_mixin> rating_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/rating_mixins/{id}")
    Rating_mixin get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/rating_mixins/{id}")
    Rating_mixin update(@PathVariable("id") Integer id,@RequestBody Rating_mixin rating_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/rating_mixins/batch")
    Boolean updateBatch(@RequestBody List<Rating_mixin> rating_mixins);




    @RequestMapping(method = RequestMethod.POST, value = "/rating_mixins/searchdefault")
    Page<Rating_mixin> searchDefault(@RequestBody Rating_mixinSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/rating_mixins/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/rating_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/rating_mixins/select")
    Page<Rating_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/rating_mixins/getdraft")
    Rating_mixin getDraft();


}
