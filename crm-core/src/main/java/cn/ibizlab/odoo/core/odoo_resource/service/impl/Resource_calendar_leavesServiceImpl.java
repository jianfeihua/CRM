package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendar_leavesService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_resource.client.resource_calendar_leavesFeignClient;

/**
 * 实体[休假详情] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_calendar_leavesServiceImpl implements IResource_calendar_leavesService {

    @Autowired
    resource_calendar_leavesFeignClient resource_calendar_leavesFeignClient;


    @Override
    public boolean create(Resource_calendar_leaves et) {
        Resource_calendar_leaves rt = resource_calendar_leavesFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_calendar_leaves> list){
        resource_calendar_leavesFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=resource_calendar_leavesFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        resource_calendar_leavesFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Resource_calendar_leaves et) {
        Resource_calendar_leaves rt = resource_calendar_leavesFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Resource_calendar_leaves> list){
        resource_calendar_leavesFeignClient.updateBatch(list) ;
    }

    @Override
    public Resource_calendar_leaves getDraft(Resource_calendar_leaves et) {
        et=resource_calendar_leavesFeignClient.getDraft();
        return et;
    }

    @Override
    public Resource_calendar_leaves get(Integer id) {
		Resource_calendar_leaves et=resource_calendar_leavesFeignClient.get(id);
        if(et==null){
            et=new Resource_calendar_leaves();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_calendar_leaves> searchDefault(Resource_calendar_leavesSearchContext context) {
        Page<Resource_calendar_leaves> resource_calendar_leavess=resource_calendar_leavesFeignClient.searchDefault(context);
        return resource_calendar_leavess;
    }


}


