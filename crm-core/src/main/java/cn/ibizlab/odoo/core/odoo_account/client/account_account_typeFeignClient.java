package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_account_type] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-account-type", fallback = account_account_typeFallback.class)
public interface account_account_typeFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/account_account_types")
    Account_account_type create(@RequestBody Account_account_type account_account_type);

    @RequestMapping(method = RequestMethod.POST, value = "/account_account_types/batch")
    Boolean createBatch(@RequestBody List<Account_account_type> account_account_types);



    @RequestMapping(method = RequestMethod.POST, value = "/account_account_types/searchdefault")
    Page<Account_account_type> searchDefault(@RequestBody Account_account_typeSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_account_types/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_account_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_account_types/{id}")
    Account_account_type update(@PathVariable("id") Integer id,@RequestBody Account_account_type account_account_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_account_types/batch")
    Boolean updateBatch(@RequestBody List<Account_account_type> account_account_types);


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_types/{id}")
    Account_account_type get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_types/select")
    Page<Account_account_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_types/getdraft")
    Account_account_type getDraft();


}
