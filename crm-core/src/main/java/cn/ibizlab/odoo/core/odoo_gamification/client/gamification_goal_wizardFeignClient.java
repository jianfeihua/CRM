package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_goal_wizard] 服务对象接口
 */
@FeignClient(value = "odoo-gamification", contextId = "gamification-goal-wizard", fallback = gamification_goal_wizardFallback.class)
public interface gamification_goal_wizardFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_wizards/{id}")
    Gamification_goal_wizard update(@PathVariable("id") Integer id,@RequestBody Gamification_goal_wizard gamification_goal_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_wizards/batch")
    Boolean updateBatch(@RequestBody List<Gamification_goal_wizard> gamification_goal_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards/searchdefault")
    Page<Gamification_goal_wizard> searchDefault(@RequestBody Gamification_goal_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards")
    Gamification_goal_wizard create(@RequestBody Gamification_goal_wizard gamification_goal_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards/batch")
    Boolean createBatch(@RequestBody List<Gamification_goal_wizard> gamification_goal_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_wizards/{id}")
    Gamification_goal_wizard get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_wizards/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_wizards/select")
    Page<Gamification_goal_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_wizards/getdraft")
    Gamification_goal_wizard getDraft();


}
