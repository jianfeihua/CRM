package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing_list_contact_rel] 服务对象接口
 */
public interface Imail_mass_mailing_list_contact_relClientService{

    public Imail_mass_mailing_list_contact_rel createModel() ;

    public Page<Imail_mass_mailing_list_contact_rel> fetchDefault(SearchContext context);

    public void create(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

    public void remove(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

    public void removeBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels);

    public void createBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels);

    public void update(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

    public void updateBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels);

    public void get(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

    public Page<Imail_mass_mailing_list_contact_rel> select(SearchContext context);

    public void getDraft(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

}
