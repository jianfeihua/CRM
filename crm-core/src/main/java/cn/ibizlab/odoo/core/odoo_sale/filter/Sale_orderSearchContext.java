package cn.ibizlab.odoo.core.odoo_sale.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Sale_order] 查询条件对象
 */
@Slf4j
@Data
public class Sale_orderSearchContext extends SearchContextBase {
	private String n_picking_policy_eq;//[送货策略]

	private String n_activity_state_eq;//[活动状态]

	private String n_invoice_status_eq;//[发票状态]

	private String n_state_eq;//[状态]

	private String n_name_like;//[订单关联]

	private String n_partner_invoice_id_text_eq;//[发票地址]

	private String n_partner_invoice_id_text_like;//[发票地址]

	private String n_incoterm_text_eq;//[贸易条款]

	private String n_incoterm_text_like;//[贸易条款]

	private String n_team_id_text_eq;//[销售团队]

	private String n_team_id_text_like;//[销售团队]

	private String n_analytic_account_id_text_eq;//[分析账户]

	private String n_analytic_account_id_text_like;//[分析账户]

	private String n_partner_id_text_eq;//[客户]

	private String n_partner_id_text_like;//[客户]

	private String n_campaign_id_text_eq;//[营销]

	private String n_campaign_id_text_like;//[营销]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private String n_partner_shipping_id_text_eq;//[送货地址]

	private String n_partner_shipping_id_text_like;//[送货地址]

	private String n_warehouse_id_text_eq;//[仓库]

	private String n_warehouse_id_text_like;//[仓库]

	private String n_user_id_text_eq;//[销售员]

	private String n_user_id_text_like;//[销售员]

	private String n_opportunity_id_text_eq;//[商机]

	private String n_opportunity_id_text_like;//[商机]

	private String n_source_id_text_eq;//[来源]

	private String n_source_id_text_like;//[来源]

	private String n_medium_id_text_eq;//[媒体]

	private String n_medium_id_text_like;//[媒体]

	private String n_fiscal_position_id_text_eq;//[税科目调整]

	private String n_fiscal_position_id_text_like;//[税科目调整]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_pricelist_id_text_eq;//[价格表]

	private String n_pricelist_id_text_like;//[价格表]

	private String n_payment_term_id_text_eq;//[付款条款]

	private String n_payment_term_id_text_like;//[付款条款]

	private String n_sale_order_template_id_text_eq;//[报价单模板]

	private String n_sale_order_template_id_text_like;//[报价单模板]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_team_id_eq;//[销售团队]

	private Integer n_partner_invoice_id_eq;//[发票地址]

	private Integer n_fiscal_position_id_eq;//[税科目调整]

	private Integer n_partner_id_eq;//[客户]

	private Integer n_campaign_id_eq;//[营销]

	private Integer n_source_id_eq;//[来源]

	private Integer n_sale_order_template_id_eq;//[报价单模板]

	private Integer n_user_id_eq;//[销售员]

	private Integer n_company_id_eq;//[公司]

	private Integer n_medium_id_eq;//[媒体]

	private Integer n_analytic_account_id_eq;//[分析账户]

	private Integer n_payment_term_id_eq;//[付款条款]

	private Integer n_partner_shipping_id_eq;//[送货地址]

	private Integer n_opportunity_id_eq;//[商机]

	private Integer n_incoterm_eq;//[贸易条款]

	private Integer n_pricelist_id_eq;//[价格表]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_warehouse_id_eq;//[仓库]

}



