package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_resend_cancel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_resend_cancel] 服务对象接口
 */
public interface Imail_resend_cancelClientService{

    public Imail_resend_cancel createModel() ;

    public void remove(Imail_resend_cancel mail_resend_cancel);

    public void update(Imail_resend_cancel mail_resend_cancel);

    public void createBatch(List<Imail_resend_cancel> mail_resend_cancels);

    public void removeBatch(List<Imail_resend_cancel> mail_resend_cancels);

    public void updateBatch(List<Imail_resend_cancel> mail_resend_cancels);

    public Page<Imail_resend_cancel> fetchDefault(SearchContext context);

    public void create(Imail_resend_cancel mail_resend_cancel);

    public void get(Imail_resend_cancel mail_resend_cancel);

    public Page<Imail_resend_cancel> select(SearchContext context);

    public void getDraft(Imail_resend_cancel mail_resend_cancel);

}
