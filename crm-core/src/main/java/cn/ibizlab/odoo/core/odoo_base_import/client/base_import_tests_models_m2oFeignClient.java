package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2oSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_m2o] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-m2o", fallback = base_import_tests_models_m2oFallback.class)
public interface base_import_tests_models_m2oFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os")
    Base_import_tests_models_m2o create(@RequestBody Base_import_tests_models_m2o base_import_tests_models_m2o);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_m2o> base_import_tests_models_m2os);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2os/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2os/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2os/{id}")
    Base_import_tests_models_m2o get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2os/{id}")
    Base_import_tests_models_m2o update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_m2o base_import_tests_models_m2o);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2os/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_m2o> base_import_tests_models_m2os);





    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os/searchdefault")
    Page<Base_import_tests_models_m2o> searchDefault(@RequestBody Base_import_tests_models_m2oSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2os/select")
    Page<Base_import_tests_models_m2o> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2os/getdraft")
    Base_import_tests_models_m2o getDraft();


}
