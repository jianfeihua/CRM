package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;


/**
 * 实体[Hr_recruitment_source] 服务对象接口
 */
public interface IHr_recruitment_sourceService{

    Hr_recruitment_source get(Integer key) ;
    Hr_recruitment_source getDraft(Hr_recruitment_source et) ;
    boolean update(Hr_recruitment_source et) ;
    void updateBatch(List<Hr_recruitment_source> list) ;
    boolean create(Hr_recruitment_source et) ;
    void createBatch(List<Hr_recruitment_source> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Hr_recruitment_source> searchDefault(Hr_recruitment_sourceSearchContext context) ;

}



