package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_lineSearchContext;


/**
 * 实体[Mro_pm_meter_line] 服务对象接口
 */
public interface IMro_pm_meter_lineService{

    boolean update(Mro_pm_meter_line et) ;
    void updateBatch(List<Mro_pm_meter_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mro_pm_meter_line get(Integer key) ;
    Mro_pm_meter_line getDraft(Mro_pm_meter_line et) ;
    boolean create(Mro_pm_meter_line et) ;
    void createBatch(List<Mro_pm_meter_line> list) ;
    Page<Mro_pm_meter_line> searchDefault(Mro_pm_meter_lineSearchContext context) ;

}



