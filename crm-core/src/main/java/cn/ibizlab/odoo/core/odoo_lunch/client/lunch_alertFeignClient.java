package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_alert] 服务对象接口
 */
@FeignClient(value = "odoo-lunch", contextId = "lunch-alert", fallback = lunch_alertFallback.class)
public interface lunch_alertFeignClient {



    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_alerts/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_alerts/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_alerts/{id}")
    Lunch_alert get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts/searchdefault")
    Page<Lunch_alert> searchDefault(@RequestBody Lunch_alertSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_alerts/{id}")
    Lunch_alert update(@PathVariable("id") Integer id,@RequestBody Lunch_alert lunch_alert);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_alerts/batch")
    Boolean updateBatch(@RequestBody List<Lunch_alert> lunch_alerts);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts")
    Lunch_alert create(@RequestBody Lunch_alert lunch_alert);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts/batch")
    Boolean createBatch(@RequestBody List<Lunch_alert> lunch_alerts);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_alerts/select")
    Page<Lunch_alert> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_alerts/getdraft")
    Lunch_alert getDraft();


}
