package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_abstract_payment;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_abstract_payment] 服务对象接口
 */
public interface Iaccount_abstract_paymentClientService{

    public Iaccount_abstract_payment createModel() ;

    public Page<Iaccount_abstract_payment> fetchDefault(SearchContext context);

    public void create(Iaccount_abstract_payment account_abstract_payment);

    public void removeBatch(List<Iaccount_abstract_payment> account_abstract_payments);

    public void get(Iaccount_abstract_payment account_abstract_payment);

    public void remove(Iaccount_abstract_payment account_abstract_payment);

    public void updateBatch(List<Iaccount_abstract_payment> account_abstract_payments);

    public void update(Iaccount_abstract_payment account_abstract_payment);

    public void createBatch(List<Iaccount_abstract_payment> account_abstract_payments);

    public Page<Iaccount_abstract_payment> select(SearchContext context);

    public void getDraft(Iaccount_abstract_payment account_abstract_payment);

}
