package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_image;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_image] 服务对象接口
 */
public interface Iproduct_imageClientService{

    public Iproduct_image createModel() ;

    public void create(Iproduct_image product_image);

    public void remove(Iproduct_image product_image);

    public void update(Iproduct_image product_image);

    public void updateBatch(List<Iproduct_image> product_images);

    public Page<Iproduct_image> fetchDefault(SearchContext context);

    public void removeBatch(List<Iproduct_image> product_images);

    public void get(Iproduct_image product_image);

    public void createBatch(List<Iproduct_image> product_images);

    public Page<Iproduct_image> select(SearchContext context);

    public void getDraft(Iproduct_image product_image);

}
