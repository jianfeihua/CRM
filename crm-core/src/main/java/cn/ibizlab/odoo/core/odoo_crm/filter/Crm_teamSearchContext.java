package cn.ibizlab.odoo.core.odoo_crm.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Crm_team] 查询条件对象
 */
@Slf4j
@Data
public class Crm_teamSearchContext extends SearchContextBase {
	private String n_dashboard_graph_type_eq;//[类型]

	private String n_team_type_eq;//[团队类型]

	private String n_dashboard_graph_model_eq;//[内容]

	private String n_dashboard_graph_period_pipeline_eq;//[预计关闭]

	private String n_dashboard_graph_group_pos_eq;//[POS分组]

	private String n_dashboard_graph_group_eq;//[分组]

	private String n_dashboard_graph_period_eq;//[比例]

	private String n_dashboard_graph_group_pipeline_eq;//[分组方式]

	private String n_name_like;//[销售团队]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_user_id_text_eq;//[团队负责人]

	private String n_user_id_text_like;//[团队负责人]

	private String n_write_uid_text_eq;//[最后更新]

	private String n_write_uid_text_like;//[最后更新]

	private Integer n_write_uid_eq;//[最后更新]

	private Integer n_alias_id_eq;//[别名]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_company_id_eq;//[公司]

	private Integer n_user_id_eq;//[团队负责人]

}



