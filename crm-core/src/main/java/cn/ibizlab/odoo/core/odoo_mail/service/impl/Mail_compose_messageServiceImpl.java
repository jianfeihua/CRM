package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_compose_messageSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_compose_messageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_compose_messageFeignClient;

/**
 * 实体[邮件撰写向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_compose_messageServiceImpl implements IMail_compose_messageService {

    @Autowired
    mail_compose_messageFeignClient mail_compose_messageFeignClient;


    @Override
    public Mail_compose_message getDraft(Mail_compose_message et) {
        et=mail_compose_messageFeignClient.getDraft();
        return et;
    }

    @Override
    public Mail_compose_message get(Integer id) {
		Mail_compose_message et=mail_compose_messageFeignClient.get(id);
        if(et==null){
            et=new Mail_compose_message();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mail_compose_message et) {
        Mail_compose_message rt = mail_compose_messageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_compose_message> list){
        mail_compose_messageFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_compose_message et) {
        Mail_compose_message rt = mail_compose_messageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_compose_message> list){
        mail_compose_messageFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_compose_messageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_compose_messageFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_compose_message> searchDefault(Mail_compose_messageSearchContext context) {
        Page<Mail_compose_message> mail_compose_messages=mail_compose_messageFeignClient.searchDefault(context);
        return mail_compose_messages;
    }


}


