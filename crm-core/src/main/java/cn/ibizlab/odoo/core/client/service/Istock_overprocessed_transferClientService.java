package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_overprocessed_transfer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_overprocessed_transfer] 服务对象接口
 */
public interface Istock_overprocessed_transferClientService{

    public Istock_overprocessed_transfer createModel() ;

    public void create(Istock_overprocessed_transfer stock_overprocessed_transfer);

    public void get(Istock_overprocessed_transfer stock_overprocessed_transfer);

    public void removeBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers);

    public void update(Istock_overprocessed_transfer stock_overprocessed_transfer);

    public void updateBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers);

    public void remove(Istock_overprocessed_transfer stock_overprocessed_transfer);

    public void createBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers);

    public Page<Istock_overprocessed_transfer> fetchDefault(SearchContext context);

    public Page<Istock_overprocessed_transfer> select(SearchContext context);

    public void getDraft(Istock_overprocessed_transfer stock_overprocessed_transfer);

}
