package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_ruleSearchContext;


/**
 * 实体[Stock_rule] 服务对象接口
 */
public interface IStock_ruleService{

    Stock_rule get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_rule et) ;
    void updateBatch(List<Stock_rule> list) ;
    boolean create(Stock_rule et) ;
    void createBatch(List<Stock_rule> list) ;
    Stock_rule getDraft(Stock_rule et) ;
    Page<Stock_rule> searchDefault(Stock_ruleSearchContext context) ;

}



