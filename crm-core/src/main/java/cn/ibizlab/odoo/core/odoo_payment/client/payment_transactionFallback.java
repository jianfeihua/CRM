package cn.ibizlab.odoo.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_transaction] 服务对象接口
 */
@Component
public class payment_transactionFallback implements payment_transactionFeignClient{


    public Payment_transaction create(Payment_transaction payment_transaction){
            return null;
     }
    public Boolean createBatch(List<Payment_transaction> payment_transactions){
            return false;
     }

    public Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context){
            return null;
     }




    public Payment_transaction get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Payment_transaction update(Integer id, Payment_transaction payment_transaction){
            return null;
     }
    public Boolean updateBatch(List<Payment_transaction> payment_transactions){
            return false;
     }


    public Page<Payment_transaction> select(){
            return null;
     }

    public Payment_transaction getDraft(){
            return null;
    }



}
