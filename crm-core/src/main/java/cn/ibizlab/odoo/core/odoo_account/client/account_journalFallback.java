package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_journalSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_journal] 服务对象接口
 */
@Component
public class account_journalFallback implements account_journalFeignClient{


    public Account_journal get(Integer id){
            return null;
     }


    public Account_journal create(Account_journal account_journal){
            return null;
     }
    public Boolean createBatch(List<Account_journal> account_journals){
            return false;
     }


    public Page<Account_journal> searchDefault(Account_journalSearchContext context){
            return null;
     }


    public Account_journal update(Integer id, Account_journal account_journal){
            return null;
     }
    public Boolean updateBatch(List<Account_journal> account_journals){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Account_journal> select(){
            return null;
     }

    public Account_journal getDraft(){
            return null;
    }



}
