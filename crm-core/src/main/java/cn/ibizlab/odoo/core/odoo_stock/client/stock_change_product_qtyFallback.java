package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_change_product_qty] 服务对象接口
 */
@Component
public class stock_change_product_qtyFallback implements stock_change_product_qtyFeignClient{



    public Stock_change_product_qty update(Integer id, Stock_change_product_qty stock_change_product_qty){
            return null;
     }
    public Boolean updateBatch(List<Stock_change_product_qty> stock_change_product_qties){
            return false;
     }


    public Page<Stock_change_product_qty> searchDefault(Stock_change_product_qtySearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Stock_change_product_qty get(Integer id){
            return null;
     }


    public Stock_change_product_qty create(Stock_change_product_qty stock_change_product_qty){
            return null;
     }
    public Boolean createBatch(List<Stock_change_product_qty> stock_change_product_qties){
            return false;
     }

    public Page<Stock_change_product_qty> select(){
            return null;
     }

    public Stock_change_product_qty getDraft(){
            return null;
    }



}
