package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attributeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_attributeFeignClient;

/**
 * 实体[产品属性] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_attributeServiceImpl implements IProduct_attributeService {

    @Autowired
    product_attributeFeignClient product_attributeFeignClient;


    @Override
    public boolean create(Product_attribute et) {
        Product_attribute rt = product_attributeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_attribute> list){
        product_attributeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Product_attribute et) {
        Product_attribute rt = product_attributeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_attribute> list){
        product_attributeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_attributeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_attributeFeignClient.removeBatch(idList);
    }

    @Override
    public Product_attribute getDraft(Product_attribute et) {
        et=product_attributeFeignClient.getDraft();
        return et;
    }

    @Override
    public Product_attribute get(Integer id) {
		Product_attribute et=product_attributeFeignClient.get(id);
        if(et==null){
            et=new Product_attribute();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_attribute> searchDefault(Product_attributeSearchContext context) {
        Page<Product_attribute> product_attributes=product_attributeFeignClient.searchDefault(context);
        return product_attributes;
    }


}


