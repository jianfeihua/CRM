package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheetSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_sheetService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_expense_sheetFeignClient;

/**
 * 实体[费用报表] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_expense_sheetServiceImpl implements IHr_expense_sheetService {

    @Autowired
    hr_expense_sheetFeignClient hr_expense_sheetFeignClient;


    @Override
    public boolean update(Hr_expense_sheet et) {
        Hr_expense_sheet rt = hr_expense_sheetFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_expense_sheet> list){
        hr_expense_sheetFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_expense_sheet get(Integer id) {
		Hr_expense_sheet et=hr_expense_sheetFeignClient.get(id);
        if(et==null){
            et=new Hr_expense_sheet();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Hr_expense_sheet getDraft(Hr_expense_sheet et) {
        et=hr_expense_sheetFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Hr_expense_sheet et) {
        Hr_expense_sheet rt = hr_expense_sheetFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_expense_sheet> list){
        hr_expense_sheetFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_expense_sheetFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_expense_sheetFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_expense_sheet> searchDefault(Hr_expense_sheetSearchContext context) {
        Page<Hr_expense_sheet> hr_expense_sheets=hr_expense_sheetFeignClient.searchDefault(context);
        return hr_expense_sheets;
    }


}


