package cn.ibizlab.odoo.core.odoo_note.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_note;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_noteSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[note_note] 服务对象接口
 */
@FeignClient(value = "odoo-note", contextId = "note-note", fallback = note_noteFallback.class)
public interface note_noteFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/note_notes/{id}")
    Note_note update(@PathVariable("id") Integer id,@RequestBody Note_note note_note);

    @RequestMapping(method = RequestMethod.PUT, value = "/note_notes/batch")
    Boolean updateBatch(@RequestBody List<Note_note> note_notes);



    @RequestMapping(method = RequestMethod.POST, value = "/note_notes/searchdefault")
    Page<Note_note> searchDefault(@RequestBody Note_noteSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/note_notes/{id}")
    Note_note get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/note_notes/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/note_notes/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/note_notes")
    Note_note create(@RequestBody Note_note note_note);

    @RequestMapping(method = RequestMethod.POST, value = "/note_notes/batch")
    Boolean createBatch(@RequestBody List<Note_note> note_notes);


    @RequestMapping(method = RequestMethod.GET, value = "/note_notes/select")
    Page<Note_note> select();


    @RequestMapping(method = RequestMethod.GET, value = "/note_notes/getdraft")
    Note_note getDraft();


}
