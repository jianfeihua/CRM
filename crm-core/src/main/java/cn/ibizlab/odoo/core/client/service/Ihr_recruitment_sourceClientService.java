package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_source;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_recruitment_source] 服务对象接口
 */
public interface Ihr_recruitment_sourceClientService{

    public Ihr_recruitment_source createModel() ;

    public Page<Ihr_recruitment_source> fetchDefault(SearchContext context);

    public void createBatch(List<Ihr_recruitment_source> hr_recruitment_sources);

    public void get(Ihr_recruitment_source hr_recruitment_source);

    public void update(Ihr_recruitment_source hr_recruitment_source);

    public void remove(Ihr_recruitment_source hr_recruitment_source);

    public void updateBatch(List<Ihr_recruitment_source> hr_recruitment_sources);

    public void removeBatch(List<Ihr_recruitment_source> hr_recruitment_sources);

    public void create(Ihr_recruitment_source hr_recruitment_source);

    public Page<Ihr_recruitment_source> select(SearchContext context);

    public void getDraft(Ihr_recruitment_source hr_recruitment_source);

}
