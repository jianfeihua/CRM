package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employee_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_employee_category] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-employee-category", fallback = hr_employee_categoryFallback.class)
public interface hr_employee_categoryFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories/searchdefault")
    Page<Hr_employee_category> searchDefault(@RequestBody Hr_employee_categorySearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_employee_categories/{id}")
    Hr_employee_category get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_categories/{id}")
    Hr_employee_category update(@PathVariable("id") Integer id,@RequestBody Hr_employee_category hr_employee_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_categories/batch")
    Boolean updateBatch(@RequestBody List<Hr_employee_category> hr_employee_categories);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories")
    Hr_employee_category create(@RequestBody Hr_employee_category hr_employee_category);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories/batch")
    Boolean createBatch(@RequestBody List<Hr_employee_category> hr_employee_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_employee_categories/select")
    Page<Hr_employee_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_employee_categories/getdraft")
    Hr_employee_category getDraft();


}
