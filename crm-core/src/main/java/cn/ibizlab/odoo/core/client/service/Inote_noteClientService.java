package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Inote_note;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[note_note] 服务对象接口
 */
public interface Inote_noteClientService{

    public Inote_note createModel() ;

    public void updateBatch(List<Inote_note> note_notes);

    public void update(Inote_note note_note);

    public Page<Inote_note> fetchDefault(SearchContext context);

    public void get(Inote_note note_note);

    public void createBatch(List<Inote_note> note_notes);

    public void remove(Inote_note note_note);

    public void removeBatch(List<Inote_note> note_notes);

    public void create(Inote_note note_note);

    public Page<Inote_note> select(SearchContext context);

    public void getDraft(Inote_note note_note);

}
