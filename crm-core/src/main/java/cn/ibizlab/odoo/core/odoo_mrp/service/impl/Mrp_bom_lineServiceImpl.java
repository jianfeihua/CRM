package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_bom_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_bom_lineFeignClient;

/**
 * 实体[物料清单明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_bom_lineServiceImpl implements IMrp_bom_lineService {

    @Autowired
    mrp_bom_lineFeignClient mrp_bom_lineFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_bom_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_bom_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Mrp_bom_line et) {
        Mrp_bom_line rt = mrp_bom_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_bom_line> list){
        mrp_bom_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mrp_bom_line et) {
        Mrp_bom_line rt = mrp_bom_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_bom_line> list){
        mrp_bom_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Mrp_bom_line getDraft(Mrp_bom_line et) {
        et=mrp_bom_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public Mrp_bom_line get(Integer id) {
		Mrp_bom_line et=mrp_bom_lineFeignClient.get(id);
        if(et==null){
            et=new Mrp_bom_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_bom_line> searchDefault(Mrp_bom_lineSearchContext context) {
        Page<Mrp_bom_line> mrp_bom_lines=mrp_bom_lineFeignClient.searchDefault(context);
        return mrp_bom_lines;
    }


}


