package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_operator;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[im_livechat_report_operator] 服务对象接口
 */
public interface Iim_livechat_report_operatorClientService{

    public Iim_livechat_report_operator createModel() ;

    public Page<Iim_livechat_report_operator> fetchDefault(SearchContext context);

    public void createBatch(List<Iim_livechat_report_operator> im_livechat_report_operators);

    public void updateBatch(List<Iim_livechat_report_operator> im_livechat_report_operators);

    public void removeBatch(List<Iim_livechat_report_operator> im_livechat_report_operators);

    public void remove(Iim_livechat_report_operator im_livechat_report_operator);

    public void create(Iim_livechat_report_operator im_livechat_report_operator);

    public void update(Iim_livechat_report_operator im_livechat_report_operator);

    public void get(Iim_livechat_report_operator im_livechat_report_operator);

    public Page<Iim_livechat_report_operator> select(SearchContext context);

    public void getDraft(Iim_livechat_report_operator im_livechat_report_operator);

}
