package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_lead_test;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_lead_testSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_automation_lead_test] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "base-automation-lead-test", fallback = base_automation_lead_testFallback.class)
public interface base_automation_lead_testFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests/searchdefault")
    Page<Base_automation_lead_test> searchDefault(@RequestBody Base_automation_lead_testSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/base_automation_lead_tests/{id}")
    Base_automation_lead_test update(@PathVariable("id") Integer id,@RequestBody Base_automation_lead_test base_automation_lead_test);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_automation_lead_tests/batch")
    Boolean updateBatch(@RequestBody List<Base_automation_lead_test> base_automation_lead_tests);


    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests")
    Base_automation_lead_test create(@RequestBody Base_automation_lead_test base_automation_lead_test);

    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests/batch")
    Boolean createBatch(@RequestBody List<Base_automation_lead_test> base_automation_lead_tests);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_lead_tests/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_lead_tests/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_automation_lead_tests/{id}")
    Base_automation_lead_test get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/base_automation_lead_tests/select")
    Page<Base_automation_lead_test> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_automation_lead_tests/getdraft")
    Base_automation_lead_test getDraft();


}
