package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_resend_cancel] 服务对象接口
 */
@Component
public class mail_resend_cancelFallback implements mail_resend_cancelFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mail_resend_cancel update(Integer id, Mail_resend_cancel mail_resend_cancel){
            return null;
     }
    public Boolean updateBatch(List<Mail_resend_cancel> mail_resend_cancels){
            return false;
     }





    public Page<Mail_resend_cancel> searchDefault(Mail_resend_cancelSearchContext context){
            return null;
     }


    public Mail_resend_cancel create(Mail_resend_cancel mail_resend_cancel){
            return null;
     }
    public Boolean createBatch(List<Mail_resend_cancel> mail_resend_cancels){
            return false;
     }

    public Mail_resend_cancel get(Integer id){
            return null;
     }


    public Page<Mail_resend_cancel> select(){
            return null;
     }

    public Mail_resend_cancel getDraft(){
            return null;
    }



}
