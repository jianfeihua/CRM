package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leaveSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leaveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_leaveFeignClient;

/**
 * 实体[休假] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leaveServiceImpl implements IHr_leaveService {

    @Autowired
    hr_leaveFeignClient hr_leaveFeignClient;


    @Override
    public boolean update(Hr_leave et) {
        Hr_leave rt = hr_leaveFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_leave> list){
        hr_leaveFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Hr_leave et) {
        Hr_leave rt = hr_leaveFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave> list){
        hr_leaveFeignClient.createBatch(list) ;
    }

    @Override
    public Hr_leave getDraft(Hr_leave et) {
        et=hr_leaveFeignClient.getDraft();
        return et;
    }

    @Override
    public Hr_leave get(Integer id) {
		Hr_leave et=hr_leaveFeignClient.get(id);
        if(et==null){
            et=new Hr_leave();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_leaveFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_leaveFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave> searchDefault(Hr_leaveSearchContext context) {
        Page<Hr_leave> hr_leaves=hr_leaveFeignClient.searchDefault(context);
        return hr_leaves;
    }


}


