package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_warehouse_orderpoint] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-warehouse-orderpoint", fallback = stock_warehouse_orderpointFallback.class)
public interface stock_warehouse_orderpointFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouse_orderpoints/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouse_orderpoints/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_warehouse_orderpoints/{id}")
    Stock_warehouse_orderpoint get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouse_orderpoints/{id}")
    Stock_warehouse_orderpoint update(@PathVariable("id") Integer id,@RequestBody Stock_warehouse_orderpoint stock_warehouse_orderpoint);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouse_orderpoints/batch")
    Boolean updateBatch(@RequestBody List<Stock_warehouse_orderpoint> stock_warehouse_orderpoints);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints")
    Stock_warehouse_orderpoint create(@RequestBody Stock_warehouse_orderpoint stock_warehouse_orderpoint);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints/batch")
    Boolean createBatch(@RequestBody List<Stock_warehouse_orderpoint> stock_warehouse_orderpoints);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints/searchdefault")
    Page<Stock_warehouse_orderpoint> searchDefault(@RequestBody Stock_warehouse_orderpointSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warehouse_orderpoints/select")
    Page<Stock_warehouse_orderpoint> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warehouse_orderpoints/getdraft")
    Stock_warehouse_orderpoint getDraft();


}
