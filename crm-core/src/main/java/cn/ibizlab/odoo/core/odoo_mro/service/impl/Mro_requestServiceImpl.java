package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_requestSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_requestService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_requestFeignClient;

/**
 * 实体[保养请求] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_requestServiceImpl implements IMro_requestService {

    @Autowired
    mro_requestFeignClient mro_requestFeignClient;


    @Override
    public Mro_request getDraft(Mro_request et) {
        et=mro_requestFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mro_request et) {
        Mro_request rt = mro_requestFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_request> list){
        mro_requestFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_requestFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_requestFeignClient.removeBatch(idList);
    }

    @Override
    public Mro_request get(Integer id) {
		Mro_request et=mro_requestFeignClient.get(id);
        if(et==null){
            et=new Mro_request();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mro_request et) {
        Mro_request rt = mro_requestFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_request> list){
        mro_requestFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_request> searchDefault(Mro_requestSearchContext context) {
        Page<Mro_request> mro_requests=mro_requestFeignClient.searchDefault(context);
        return mro_requests;
    }


}


