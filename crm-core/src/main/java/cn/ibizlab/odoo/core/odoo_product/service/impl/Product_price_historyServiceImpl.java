package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_historySearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_price_historyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_price_historyFeignClient;

/**
 * 实体[产品价格历史列表] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_price_historyServiceImpl implements IProduct_price_historyService {

    @Autowired
    product_price_historyFeignClient product_price_historyFeignClient;


    @Override
    public boolean create(Product_price_history et) {
        Product_price_history rt = product_price_historyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_price_history> list){
        product_price_historyFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_price_historyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_price_historyFeignClient.removeBatch(idList);
    }

    @Override
    public Product_price_history get(Integer id) {
		Product_price_history et=product_price_historyFeignClient.get(id);
        if(et==null){
            et=new Product_price_history();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Product_price_history getDraft(Product_price_history et) {
        et=product_price_historyFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Product_price_history et) {
        Product_price_history rt = product_price_historyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_price_history> list){
        product_price_historyFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_price_history> searchDefault(Product_price_historySearchContext context) {
        Page<Product_price_history> product_price_historys=product_price_historyFeignClient.searchDefault(context);
        return product_price_historys;
    }


}


