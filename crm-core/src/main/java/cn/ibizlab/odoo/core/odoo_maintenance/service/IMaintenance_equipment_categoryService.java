package cn.ibizlab.odoo.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;


/**
 * 实体[Maintenance_equipment_category] 服务对象接口
 */
public interface IMaintenance_equipment_categoryService{

    Maintenance_equipment_category getDraft(Maintenance_equipment_category et) ;
    boolean update(Maintenance_equipment_category et) ;
    void updateBatch(List<Maintenance_equipment_category> list) ;
    boolean create(Maintenance_equipment_category et) ;
    void createBatch(List<Maintenance_equipment_category> list) ;
    Maintenance_equipment_category get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Maintenance_equipment_category> searchDefault(Maintenance_equipment_categorySearchContext context) ;

}



