package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_activity_typeDTO]
 */
@Data
public class Mail_activity_typeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RES_MODEL_CHANGE]
     *
     */
    @JSONField(name = "res_model_change")
    @JsonProperty("res_model_change")
    private String resModelChange;

    /**
     * 属性 [SUMMARY]
     *
     */
    @JSONField(name = "summary")
    @JsonProperty("summary")
    private String summary;

    /**
     * 属性 [FORCE_NEXT]
     *
     */
    @JSONField(name = "force_next")
    @JsonProperty("force_next")
    private String forceNext;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CATEGORY]
     *
     */
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;

    /**
     * 属性 [ICON]
     *
     */
    @JSONField(name = "icon")
    @JsonProperty("icon")
    private String icon;

    /**
     * 属性 [DELAY_COUNT]
     *
     */
    @JSONField(name = "delay_count")
    @JsonProperty("delay_count")
    private Integer delayCount;

    /**
     * 属性 [PREVIOUS_TYPE_IDS]
     *
     */
    @JSONField(name = "previous_type_ids")
    @JsonProperty("previous_type_ids")
    private String previousTypeIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [DELAY_FROM]
     *
     */
    @JSONField(name = "delay_from")
    @JsonProperty("delay_from")
    private String delayFrom;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MAIL_TEMPLATE_IDS]
     *
     */
    @JSONField(name = "mail_template_ids")
    @JsonProperty("mail_template_ids")
    private String mailTemplateIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DECORATION_TYPE]
     *
     */
    @JSONField(name = "decoration_type")
    @JsonProperty("decoration_type")
    private String decorationType;

    /**
     * 属性 [NEXT_TYPE_IDS]
     *
     */
    @JSONField(name = "next_type_ids")
    @JsonProperty("next_type_ids")
    private String nextTypeIds;

    /**
     * 属性 [DELAY_UNIT]
     *
     */
    @JSONField(name = "delay_unit")
    @JsonProperty("delay_unit")
    private String delayUnit;

    /**
     * 属性 [INITIAL_RES_MODEL_ID]
     *
     */
    @JSONField(name = "initial_res_model_id")
    @JsonProperty("initial_res_model_id")
    private Integer initialResModelId;

    /**
     * 属性 [DEFAULT_NEXT_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "default_next_type_id_text")
    @JsonProperty("default_next_type_id_text")
    private String defaultNextTypeIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [DEFAULT_NEXT_TYPE_ID]
     *
     */
    @JSONField(name = "default_next_type_id")
    @JsonProperty("default_next_type_id")
    private Integer defaultNextTypeId;


    /**
     * 设置 [SUMMARY]
     */
    public void setSummary(String  summary){
        this.summary = summary ;
        this.modify("summary",summary);
    }

    /**
     * 设置 [FORCE_NEXT]
     */
    public void setForceNext(String  forceNext){
        this.forceNext = forceNext ;
        this.modify("force_next",forceNext);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [CATEGORY]
     */
    public void setCategory(String  category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [ICON]
     */
    public void setIcon(String  icon){
        this.icon = icon ;
        this.modify("icon",icon);
    }

    /**
     * 设置 [DELAY_COUNT]
     */
    public void setDelayCount(Integer  delayCount){
        this.delayCount = delayCount ;
        this.modify("delay_count",delayCount);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [DELAY_FROM]
     */
    public void setDelayFrom(String  delayFrom){
        this.delayFrom = delayFrom ;
        this.modify("delay_from",delayFrom);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    public void setResModelId(Integer  resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [DECORATION_TYPE]
     */
    public void setDecorationType(String  decorationType){
        this.decorationType = decorationType ;
        this.modify("decoration_type",decorationType);
    }

    /**
     * 设置 [DELAY_UNIT]
     */
    public void setDelayUnit(String  delayUnit){
        this.delayUnit = delayUnit ;
        this.modify("delay_unit",delayUnit);
    }

    /**
     * 设置 [DEFAULT_NEXT_TYPE_ID]
     */
    public void setDefaultNextTypeId(Integer  defaultNextTypeId){
        this.defaultNextTypeId = defaultNextTypeId ;
        this.modify("default_next_type_id",defaultNextTypeId);
    }


}

