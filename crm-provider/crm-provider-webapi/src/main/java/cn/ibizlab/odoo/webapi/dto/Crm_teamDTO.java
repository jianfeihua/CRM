package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Crm_teamDTO]
 */
@Data
public class Crm_teamDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [OPPORTUNITIES_AMOUNT]
     *
     */
    @JSONField(name = "opportunities_amount")
    @JsonProperty("opportunities_amount")
    private Integer opportunitiesAmount;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [USE_OPPORTUNITIES]
     *
     */
    @JSONField(name = "use_opportunities")
    @JsonProperty("use_opportunities")
    private String useOpportunities;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [QUOTATIONS_COUNT]
     *
     */
    @JSONField(name = "quotations_count")
    @JsonProperty("quotations_count")
    private Integer quotationsCount;

    /**
     * 属性 [DASHBOARD_GRAPH_TYPE]
     *
     */
    @JSONField(name = "dashboard_graph_type")
    @JsonProperty("dashboard_graph_type")
    private String dashboardGraphType;

    /**
     * 属性 [IS_FAVORITE]
     *
     */
    @JSONField(name = "is_favorite")
    @JsonProperty("is_favorite")
    private String isFavorite;

    /**
     * 属性 [OPPORTUNITIES_COUNT]
     *
     */
    @JSONField(name = "opportunities_count")
    @JsonProperty("opportunities_count")
    private Integer opportunitiesCount;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [USE_QUOTATIONS]
     *
     */
    @JSONField(name = "use_quotations")
    @JsonProperty("use_quotations")
    private String useQuotations;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 属性 [FAVORITE_USER_IDS]
     *
     */
    @JSONField(name = "favorite_user_ids")
    @JsonProperty("favorite_user_ids")
    private String favoriteUserIds;

    /**
     * 属性 [TEAM_TYPE]
     *
     */
    @JSONField(name = "team_type")
    @JsonProperty("team_type")
    private String teamType;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [DASHBOARD_GRAPH_MODEL]
     *
     */
    @JSONField(name = "dashboard_graph_model")
    @JsonProperty("dashboard_graph_model")
    private String dashboardGraphModel;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [DASHBOARD_GRAPH_PERIOD_PIPELINE]
     *
     */
    @JSONField(name = "dashboard_graph_period_pipeline")
    @JsonProperty("dashboard_graph_period_pipeline")
    private String dashboardGraphPeriodPipeline;

    /**
     * 属性 [ABANDONED_CARTS_COUNT]
     *
     */
    @JSONField(name = "abandoned_carts_count")
    @JsonProperty("abandoned_carts_count")
    private Integer abandonedCartsCount;

    /**
     * 属性 [POS_CONFIG_IDS]
     *
     */
    @JSONField(name = "pos_config_ids")
    @JsonProperty("pos_config_ids")
    private String posConfigIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [USE_LEADS]
     *
     */
    @JSONField(name = "use_leads")
    @JsonProperty("use_leads")
    private String useLeads;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [WEBSITE_IDS]
     *
     */
    @JSONField(name = "website_ids")
    @JsonProperty("website_ids")
    private String websiteIds;

    /**
     * 属性 [QUOTATIONS_AMOUNT]
     *
     */
    @JSONField(name = "quotations_amount")
    @JsonProperty("quotations_amount")
    private Integer quotationsAmount;

    /**
     * 属性 [INVOICED]
     *
     */
    @JSONField(name = "invoiced")
    @JsonProperty("invoiced")
    private Integer invoiced;

    /**
     * 属性 [ABANDONED_CARTS_AMOUNT]
     *
     */
    @JSONField(name = "abandoned_carts_amount")
    @JsonProperty("abandoned_carts_amount")
    private Integer abandonedCartsAmount;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [SALES_TO_INVOICE_COUNT]
     *
     */
    @JSONField(name = "sales_to_invoice_count")
    @JsonProperty("sales_to_invoice_count")
    private Integer salesToInvoiceCount;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [UNASSIGNED_LEADS_COUNT]
     *
     */
    @JSONField(name = "unassigned_leads_count")
    @JsonProperty("unassigned_leads_count")
    private Integer unassignedLeadsCount;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [DASHBOARD_GRAPH_GROUP_POS]
     *
     */
    @JSONField(name = "dashboard_graph_group_pos")
    @JsonProperty("dashboard_graph_group_pos")
    private String dashboardGraphGroupPos;

    /**
     * 属性 [MEMBER_IDS]
     *
     */
    @JSONField(name = "member_ids")
    @JsonProperty("member_ids")
    private String memberIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [INVOICED_TARGET]
     *
     */
    @JSONField(name = "invoiced_target")
    @JsonProperty("invoiced_target")
    private Integer invoicedTarget;

    /**
     * 属性 [DASHBOARD_GRAPH_GROUP]
     *
     */
    @JSONField(name = "dashboard_graph_group")
    @JsonProperty("dashboard_graph_group")
    private String dashboardGraphGroup;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [DASHBOARD_GRAPH_DATA]
     *
     */
    @JSONField(name = "dashboard_graph_data")
    @JsonProperty("dashboard_graph_data")
    private String dashboardGraphData;

    /**
     * 属性 [DASHBOARD_GRAPH_PERIOD]
     *
     */
    @JSONField(name = "dashboard_graph_period")
    @JsonProperty("dashboard_graph_period")
    private String dashboardGraphPeriod;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [POS_SESSIONS_OPEN_COUNT]
     *
     */
    @JSONField(name = "pos_sessions_open_count")
    @JsonProperty("pos_sessions_open_count")
    private Integer posSessionsOpenCount;

    /**
     * 属性 [USE_INVOICES]
     *
     */
    @JSONField(name = "use_invoices")
    @JsonProperty("use_invoices")
    private String useInvoices;

    /**
     * 属性 [DASHBOARD_BUTTON_NAME]
     *
     */
    @JSONField(name = "dashboard_button_name")
    @JsonProperty("dashboard_button_name")
    private String dashboardButtonName;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [DASHBOARD_GRAPH_GROUP_PIPELINE]
     *
     */
    @JSONField(name = "dashboard_graph_group_pipeline")
    @JsonProperty("dashboard_graph_group_pipeline")
    private String dashboardGraphGroupPipeline;

    /**
     * 属性 [POS_ORDER_AMOUNT_TOTAL]
     *
     */
    @JSONField(name = "pos_order_amount_total")
    @JsonProperty("pos_order_amount_total")
    private Double posOrderAmountTotal;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Integer aliasUserId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;


    /**
     * 设置 [USE_OPPORTUNITIES]
     */
    public void setUseOpportunities(String  useOpportunities){
        this.useOpportunities = useOpportunities ;
        this.modify("use_opportunities",useOpportunities);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [USE_QUOTATIONS]
     */
    public void setUseQuotations(String  useQuotations){
        this.useQuotations = useQuotations ;
        this.modify("use_quotations",useQuotations);
    }

    /**
     * 设置 [REPLY_TO]
     */
    public void setReplyTo(String  replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }

    /**
     * 设置 [TEAM_TYPE]
     */
    public void setTeamType(String  teamType){
        this.teamType = teamType ;
        this.modify("team_type",teamType);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [DASHBOARD_GRAPH_MODEL]
     */
    public void setDashboardGraphModel(String  dashboardGraphModel){
        this.dashboardGraphModel = dashboardGraphModel ;
        this.modify("dashboard_graph_model",dashboardGraphModel);
    }

    /**
     * 设置 [USE_LEADS]
     */
    public void setUseLeads(String  useLeads){
        this.useLeads = useLeads ;
        this.modify("use_leads",useLeads);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [DASHBOARD_GRAPH_GROUP_POS]
     */
    public void setDashboardGraphGroupPos(String  dashboardGraphGroupPos){
        this.dashboardGraphGroupPos = dashboardGraphGroupPos ;
        this.modify("dashboard_graph_group_pos",dashboardGraphGroupPos);
    }

    /**
     * 设置 [INVOICED_TARGET]
     */
    public void setInvoicedTarget(Integer  invoicedTarget){
        this.invoicedTarget = invoicedTarget ;
        this.modify("invoiced_target",invoicedTarget);
    }

    /**
     * 设置 [DASHBOARD_GRAPH_GROUP]
     */
    public void setDashboardGraphGroup(String  dashboardGraphGroup){
        this.dashboardGraphGroup = dashboardGraphGroup ;
        this.modify("dashboard_graph_group",dashboardGraphGroup);
    }

    /**
     * 设置 [DASHBOARD_GRAPH_PERIOD]
     */
    public void setDashboardGraphPeriod(String  dashboardGraphPeriod){
        this.dashboardGraphPeriod = dashboardGraphPeriod ;
        this.modify("dashboard_graph_period",dashboardGraphPeriod);
    }

    /**
     * 设置 [USE_INVOICES]
     */
    public void setUseInvoices(String  useInvoices){
        this.useInvoices = useInvoices ;
        this.modify("use_invoices",useInvoices);
    }

    /**
     * 设置 [DASHBOARD_GRAPH_GROUP_PIPELINE]
     */
    public void setDashboardGraphGroupPipeline(String  dashboardGraphGroupPipeline){
        this.dashboardGraphGroupPipeline = dashboardGraphGroupPipeline ;
        this.modify("dashboard_graph_group_pipeline",dashboardGraphGroupPipeline);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ALIAS_ID]
     */
    public void setAliasId(Integer  aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


}

