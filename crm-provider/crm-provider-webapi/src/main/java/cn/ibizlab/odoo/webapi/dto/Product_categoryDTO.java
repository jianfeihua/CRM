package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Product_categoryDTO]
 */
@Data
public class Product_categoryDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     *
     */
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Integer propertyStockValuationAccountId;

    /**
     * 属性 [CHILD_ID]
     *
     */
    @JSONField(name = "child_id")
    @JsonProperty("child_id")
    private String childId;

    /**
     * 属性 [PRODUCT_COUNT]
     *
     */
    @JSONField(name = "product_count")
    @JsonProperty("product_count")
    private Integer productCount;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [PROPERTY_VALUATION]
     *
     */
    @JSONField(name = "property_valuation")
    @JsonProperty("property_valuation")
    private String propertyValuation;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    private String parentPath;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [COMPLETE_NAME]
     *
     */
    @JSONField(name = "complete_name")
    @JsonProperty("complete_name")
    private String completeName;

    /**
     * 属性 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE_CATEG]
     *
     */
    @JSONField(name = "property_account_creditor_price_difference_categ")
    @JsonProperty("property_account_creditor_price_difference_categ")
    private Integer propertyAccountCreditorPriceDifferenceCateg;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Integer propertyStockAccountOutputCategId;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [TOTAL_ROUTE_IDS]
     *
     */
    @JSONField(name = "total_route_ids")
    @JsonProperty("total_route_ids")
    private String totalRouteIds;

    /**
     * 属性 [PROPERTY_COST_METHOD]
     *
     */
    @JSONField(name = "property_cost_method")
    @JsonProperty("property_cost_method")
    private String propertyCostMethod;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     *
     */
    @JSONField(name = "property_account_income_categ_id")
    @JsonProperty("property_account_income_categ_id")
    private Integer propertyAccountIncomeCategId;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     *
     */
    @JSONField(name = "property_account_expense_categ_id")
    @JsonProperty("property_account_expense_categ_id")
    private Integer propertyAccountExpenseCategId;

    /**
     * 属性 [PROPERTY_STOCK_JOURNAL]
     *
     */
    @JSONField(name = "property_stock_journal")
    @JsonProperty("property_stock_journal")
    private Integer propertyStockJournal;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Integer propertyStockAccountInputCategId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [REMOVAL_STRATEGY_ID_TEXT]
     *
     */
    @JSONField(name = "removal_strategy_id_text")
    @JsonProperty("removal_strategy_id_text")
    private String removalStrategyIdText;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [REMOVAL_STRATEGY_ID]
     *
     */
    @JSONField(name = "removal_strategy_id")
    @JsonProperty("removal_strategy_id")
    private Integer removalStrategyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PARENT_PATH]
     */
    public void setParentPath(String  parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }

    /**
     * 设置 [COMPLETE_NAME]
     */
    public void setCompleteName(String  completeName){
        this.completeName = completeName ;
        this.modify("complete_name",completeName);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Integer  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID]
     */
    public void setRemovalStrategyId(Integer  removalStrategyId){
        this.removalStrategyId = removalStrategyId ;
        this.modify("removal_strategy_id",removalStrategyId);
    }


}

