package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_teamService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;




@Slf4j
@Api(tags = {"Crm_team" })
@RestController("WebApi-crm_team")
@RequestMapping("")
public class Crm_teamResource {

    @Autowired
    private ICrm_teamService crm_teamService;

    @Autowired
    @Lazy
    private Crm_teamMapping crm_teamMapping;




    @PreAuthorize("hasPermission(#crm_team_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_team" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{crm_team_id}")
    public ResponseEntity<Crm_teamDTO> get(@PathVariable("crm_team_id") Integer crm_team_id) {
        Crm_team domain = crm_teamService.get(crm_team_id);
        Crm_teamDTO dto = crm_teamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "CheckKey", tags = {"Crm_team" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_teamDTO crm_teamdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_teamService.checkKey(crm_teamMapping.toDomain(crm_teamdto)));
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Crm_team" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/getdraft")
    public ResponseEntity<Crm_teamDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_teamMapping.toDto(crm_teamService.getDraft(new Crm_team())));
    }










    @PreAuthorize("hasPermission(#crm_team_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_team" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}")

    public ResponseEntity<Crm_teamDTO> update(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_teamDTO crm_teamdto) {
		Crm_team domain = crm_teamMapping.toDomain(crm_teamdto);
        domain.setId(crm_team_id);
		crm_teamService.update(domain);
		Crm_teamDTO dto = crm_teamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_team_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_team" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        crm_teamService.updateBatch(crm_teamMapping.toDomain(crm_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#crm_team_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_team" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_team_id") Integer crm_team_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_teamService.remove(crm_team_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_team" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_teamService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_team" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams")

    public ResponseEntity<Crm_teamDTO> create(@RequestBody Crm_teamDTO crm_teamdto) {
        Crm_team domain = crm_teamMapping.toDomain(crm_teamdto);
		crm_teamService.create(domain);
        Crm_teamDTO dto = crm_teamMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_team" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        crm_teamService.createBatch(crm_teamMapping.toDomain(crm_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @ApiOperation(value = "Save", tags = {"Crm_team" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_teamDTO crm_teamdto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_teamService.save(crm_teamMapping.toDomain(crm_teamdto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Crm_team" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        crm_teamService.saveBatch(crm_teamMapping.toDomain(crm_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_team-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_team" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_teams/fetchdefault")
	public ResponseEntity<List<Crm_teamDTO>> fetchDefault(Crm_teamSearchContext context) {
        Page<Crm_team> domains = crm_teamService.searchDefault(context) ;
        List<Crm_teamDTO> list = crm_teamMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_team-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_team" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_teams/searchdefault")
	public ResponseEntity<Page<Crm_teamDTO>> searchDefault(@RequestBody Crm_teamSearchContext context) {
        Page<Crm_team> domains = crm_teamService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_teamMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_team getEntity(){
        return new Crm_team();
    }

}
