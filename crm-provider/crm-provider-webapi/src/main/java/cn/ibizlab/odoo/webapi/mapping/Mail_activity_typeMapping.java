package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.webapi.dto.Mail_activity_typeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_activity_typeMapping extends MappingBase<Mail_activity_typeDTO, Mail_activity_type> {


}

