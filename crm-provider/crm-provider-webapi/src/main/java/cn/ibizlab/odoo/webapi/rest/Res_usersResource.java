package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_usersService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;




@Slf4j
@Api(tags = {"Res_users" })
@RestController("WebApi-res_users")
@RequestMapping("")
public class Res_usersResource {

    @Autowired
    private IRes_usersService res_usersService;

    @Autowired
    @Lazy
    private Res_usersMapping res_usersMapping;




    @PreAuthorize("hasPermission(#res_users_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_users" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/{res_users_id}")

    public ResponseEntity<Res_usersDTO> update(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
		Res_users domain = res_usersMapping.toDomain(res_usersdto);
        domain.setId(res_users_id);
		res_usersService.update(domain);
		Res_usersDTO dto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_users_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_users" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.updateBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "Save", tags = {"Res_users" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_usersDTO res_usersdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_usersService.save(res_usersMapping.toDomain(res_usersdto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Res_users" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.saveBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_users" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users")

    public ResponseEntity<Res_usersDTO> create(@RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersMapping.toDomain(res_usersdto);
		res_usersService.create(domain);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_users" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.createBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "CheckKey", tags = {"Res_users" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_usersDTO res_usersdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_usersService.checkKey(res_usersMapping.toDomain(res_usersdto)));
    }




    @PreAuthorize("hasPermission(#res_users_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_users" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> get(@PathVariable("res_users_id") Integer res_users_id) {
        Res_users domain = res_usersService.get(res_users_id);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('Remove',{#res_users_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_users" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/{res_users_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_users_id") Integer res_users_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_usersService.remove(res_users_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_users" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_usersService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Res_users" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users/getdraft")
    public ResponseEntity<Res_usersDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_usersMapping.toDto(res_usersService.getDraft(new Res_users())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_users-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_users" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_users/fetchdefault")
	public ResponseEntity<List<Res_usersDTO>> fetchDefault(Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
        List<Res_usersDTO> list = res_usersMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_users-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Res_users" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/res_users/searchdefault")
	public ResponseEntity<Page<Res_usersDTO>> searchDefault(@RequestBody Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_usersMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @ApiOperation(value = "更新数据ByCrm_team", tags = {"Res_users" },  notes = "更新数据ByCrm_team")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}")

    public ResponseEntity<Res_usersDTO> updateByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersMapping.toDomain(res_usersdto);
        domain.setSaleTeamId(crm_team_id);
        domain.setId(res_users_id);
		res_usersService.update(domain);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "UpdateBatchByCrm_team", tags = {"Res_users" },  notes = "UpdateBatchByCrm_team")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}/res_users/batch")
    public ResponseEntity<Boolean> updateBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody List<Res_usersDTO> res_usersdtos) {
        List<Res_users> domainlist=res_usersMapping.toDomain(res_usersdtos);
        for(Res_users domain:domainlist){
            domain.setSaleTeamId(crm_team_id);
        }
        res_usersService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "SaveByCrm_team", tags = {"Res_users" },  notes = "SaveByCrm_team")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users/save")
    public ResponseEntity<Boolean> saveByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersMapping.toDomain(res_usersdto);
        domain.setSaleTeamId(crm_team_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_usersService.save(domain));
    }

    @ApiOperation(value = "SaveBatchByCrm_team", tags = {"Res_users" },  notes = "SaveBatchByCrm_team")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users/savebatch")
    public ResponseEntity<Boolean> saveBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody List<Res_usersDTO> res_usersdtos) {
        List<Res_users> domainlist=res_usersMapping.toDomain(res_usersdtos);
        for(Res_users domain:domainlist){
             domain.setSaleTeamId(crm_team_id);
        }
        res_usersService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据ByCrm_team", tags = {"Res_users" },  notes = "建立数据ByCrm_team")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users")

    public ResponseEntity<Res_usersDTO> createByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersMapping.toDomain(res_usersdto);
        domain.setSaleTeamId(crm_team_id);
		res_usersService.create(domain);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "createBatchByCrm_team", tags = {"Res_users" },  notes = "createBatchByCrm_team")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users/batch")
    public ResponseEntity<Boolean> createBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody List<Res_usersDTO> res_usersdtos) {
        List<Res_users> domainlist=res_usersMapping.toDomain(res_usersdtos);
        for(Res_users domain:domainlist){
            domain.setSaleTeamId(crm_team_id);
        }
        res_usersService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "CheckKeyByCrm_team", tags = {"Res_users" },  notes = "CheckKeyByCrm_team")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users/checkkey")
    public ResponseEntity<Boolean> checkKeyByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_usersDTO res_usersdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_usersService.checkKey(res_usersMapping.toDomain(res_usersdto)));
    }

    @ApiOperation(value = "获取数据ByCrm_team", tags = {"Res_users" },  notes = "获取数据ByCrm_team")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> getByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) {
        Res_users domain = res_usersService.get(res_users_id);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据ByCrm_team", tags = {"Res_users" },  notes = "删除数据ByCrm_team")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}")

    public ResponseEntity<Boolean> removeByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) {
		return ResponseEntity.status(HttpStatus.OK).body(res_usersService.remove(res_users_id));
    }

    @ApiOperation(value = "RemoveBatchByCrm_team", tags = {"Res_users" },  notes = "RemoveBatchByCrm_team")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}/res_users/batch")
    public ResponseEntity<Boolean> removeBatchByCrm_team(@RequestBody List<Integer> ids) {
        res_usersService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据ByCrm_team", tags = {"Res_users" },  notes = "获取草稿数据ByCrm_team")
    @RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{crm_team_id}/res_users/getdraft")
    public ResponseEntity<Res_usersDTO> getDraftByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id) {
        Res_users domain = new Res_users();
        domain.setSaleTeamId(crm_team_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_usersMapping.toDto(res_usersService.getDraft(domain)));
    }

	@ApiOperation(value = "fetch默认查询ByCrm_team", tags = {"Res_users" } ,notes = "fetch默认查询ByCrm_team")
    @RequestMapping(method= RequestMethod.GET , value="/crm_teams/{crm_team_id}/res_users/fetchdefault")
	public ResponseEntity<List<Res_usersDTO>> fetchRes_usersDefaultByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id,Res_usersSearchContext context) {
        context.setN_sale_team_id_eq(crm_team_id);
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
        List<Res_usersDTO> list = res_usersMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

	@ApiOperation(value = "search默认查询ByCrm_team", tags = {"Res_users" } ,notes = "search默认查询ByCrm_team")
    @RequestMapping(method= RequestMethod.POST , value="/crm_teams/{crm_team_id}/res_users/searchdefault")
	public ResponseEntity<Page<Res_usersDTO>> searchRes_usersDefaultByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_usersSearchContext context) {
        context.setN_sale_team_id_eq(crm_team_id);
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_usersMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_users getEntity(){
        return new Res_users();
    }

}
