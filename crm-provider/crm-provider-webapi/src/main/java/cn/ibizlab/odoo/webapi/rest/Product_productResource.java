package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_productService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;




@Slf4j
@Api(tags = {"Product_product" })
@RestController("WebApi-product_product")
@RequestMapping("")
public class Product_productResource {

    @Autowired
    private IProduct_productService product_productService;

    @Autowired
    @Lazy
    private Product_productMapping product_productMapping;







    @PreAuthorize("hasPermission(#product_product_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_product" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}")
    public ResponseEntity<Product_productDTO> get(@PathVariable("product_product_id") Integer product_product_id) {
        Product_product domain = product_productService.get(product_product_id);
        Product_productDTO dto = product_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "Save", tags = {"Product_product" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_productDTO product_productdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_productService.save(product_productMapping.toDomain(product_productdto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Product_product" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_productDTO> product_productdtos) {
        product_productService.saveBatch(product_productMapping.toDomain(product_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_product" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products")

    public ResponseEntity<Product_productDTO> create(@RequestBody Product_productDTO product_productdto) {
        Product_product domain = product_productMapping.toDomain(product_productdto);
		product_productService.create(domain);
        Product_productDTO dto = product_productMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_product" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_productDTO> product_productdtos) {
        product_productService.createBatch(product_productMapping.toDomain(product_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "CheckKey", tags = {"Product_product" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_productDTO product_productdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_productService.checkKey(product_productMapping.toDomain(product_productdto)));
    }




    @PreAuthorize("hasPermission(#product_product_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_product" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}")

    public ResponseEntity<Product_productDTO> update(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_productDTO product_productdto) {
		Product_product domain = product_productMapping.toDomain(product_productdto);
        domain.setId(product_product_id);
		product_productService.update(domain);
		Product_productDTO dto = product_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_product_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_product" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_productDTO> product_productdtos) {
        product_productService.updateBatch(product_productMapping.toDomain(product_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @ApiOperation(value = "获取草稿数据", tags = {"Product_product" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/getdraft")
    public ResponseEntity<Product_productDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_productMapping.toDto(product_productService.getDraft(new Product_product())));
    }




    @PreAuthorize("hasPermission('Remove',{#product_product_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_product" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_product_id") Integer product_product_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_productService.remove(product_product_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_product" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_productService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_product-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_product" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_products/fetchdefault")
	public ResponseEntity<List<Product_productDTO>> fetchDefault(Product_productSearchContext context) {
        Page<Product_product> domains = product_productService.searchDefault(context) ;
        List<Product_productDTO> list = product_productMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_product-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Product_product" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/product_products/searchdefault")
	public ResponseEntity<Page<Product_productDTO>> searchDefault(@RequestBody Product_productSearchContext context) {
        Page<Product_product> domains = product_productService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_productMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_product getEntity(){
        return new Product_product();
    }

}
