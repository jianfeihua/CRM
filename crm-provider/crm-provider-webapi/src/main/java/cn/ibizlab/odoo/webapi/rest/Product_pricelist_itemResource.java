package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelist_itemService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;




@Slf4j
@Api(tags = {"Product_pricelist_item" })
@RestController("WebApi-product_pricelist_item")
@RequestMapping("")
public class Product_pricelist_itemResource {

    @Autowired
    private IProduct_pricelist_itemService product_pricelist_itemService;

    @Autowired
    @Lazy
    private Product_pricelist_itemMapping product_pricelist_itemMapping;




    @PreAuthorize("hasPermission(#product_pricelist_item_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_pricelist_item" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/{product_pricelist_item_id}")
    public ResponseEntity<Product_pricelist_itemDTO> get(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) {
        Product_pricelist_item domain = product_pricelist_itemService.get(product_pricelist_item_id);
        Product_pricelist_itemDTO dto = product_pricelist_itemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_pricelist_item" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items")

    public ResponseEntity<Product_pricelist_itemDTO> create(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        Product_pricelist_item domain = product_pricelist_itemMapping.toDomain(product_pricelist_itemdto);
		product_pricelist_itemService.create(domain);
        Product_pricelist_itemDTO dto = product_pricelist_itemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_pricelist_item" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        product_pricelist_itemService.createBatch(product_pricelist_itemMapping.toDomain(product_pricelist_itemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#product_pricelist_item_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_pricelist_item" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/{product_pricelist_item_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemService.remove(product_pricelist_item_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_pricelist_item" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_pricelist_itemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Product_pricelist_item" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/getdraft")
    public ResponseEntity<Product_pricelist_itemDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemMapping.toDto(product_pricelist_itemService.getDraft(new Product_pricelist_item())));
    }













    @PreAuthorize("hasPermission(#product_pricelist_item_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_pricelist_item" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/{product_pricelist_item_id}")

    public ResponseEntity<Product_pricelist_itemDTO> update(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
		Product_pricelist_item domain = product_pricelist_itemMapping.toDomain(product_pricelist_itemdto);
        domain.setId(product_pricelist_item_id);
		product_pricelist_itemService.update(domain);
		Product_pricelist_itemDTO dto = product_pricelist_itemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_pricelist_item_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_pricelist_item" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        product_pricelist_itemService.updateBatch(product_pricelist_itemMapping.toDomain(product_pricelist_itemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "CheckKey", tags = {"Product_pricelist_item" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemService.checkKey(product_pricelist_itemMapping.toDomain(product_pricelist_itemdto)));
    }




    @ApiOperation(value = "Save", tags = {"Product_pricelist_item" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemService.save(product_pricelist_itemMapping.toDomain(product_pricelist_itemdto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Product_pricelist_item" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        product_pricelist_itemService.saveBatch(product_pricelist_itemMapping.toDomain(product_pricelist_itemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_pricelist_item-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_pricelist_item" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_pricelist_items/fetchdefault")
	public ResponseEntity<List<Product_pricelist_itemDTO>> fetchDefault(Product_pricelist_itemSearchContext context) {
        Page<Product_pricelist_item> domains = product_pricelist_itemService.searchDefault(context) ;
        List<Product_pricelist_itemDTO> list = product_pricelist_itemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_pricelist_item-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Product_pricelist_item" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/product_pricelist_items/searchdefault")
	public ResponseEntity<Page<Product_pricelist_itemDTO>> searchDefault(@RequestBody Product_pricelist_itemSearchContext context) {
        Page<Product_pricelist_item> domains = product_pricelist_itemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_pricelist_itemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_pricelist_item getEntity(){
        return new Product_pricelist_item();
    }

}
