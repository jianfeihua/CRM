package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.webapi.dto.Crm_lost_reasonDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_lost_reasonMapping extends MappingBase<Crm_lost_reasonDTO, Crm_lost_reason> {


}

