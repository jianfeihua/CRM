package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoiceService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;




@Slf4j
@Api(tags = {"Account_invoice" })
@RestController("WebApi-account_invoice")
@RequestMapping("")
public class Account_invoiceResource {

    @Autowired
    private IAccount_invoiceService account_invoiceService;

    @Autowired
    @Lazy
    private Account_invoiceMapping account_invoiceMapping;




    @PreAuthorize("hasPermission('Remove',{#account_invoice_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/{account_invoice_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_id") Integer account_invoice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.remove(account_invoice_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "CheckKey", tags = {"Account_invoice" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoiceDTO account_invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.checkKey(account_invoiceMapping.toDomain(account_invoicedto)));
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices")

    public ResponseEntity<Account_invoiceDTO> create(@RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
		account_invoiceService.create(domain);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.createBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/{account_invoice_id}")

    public ResponseEntity<Account_invoiceDTO> update(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
		Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
        domain.setId(account_invoice_id);
		account_invoiceService.update(domain);
		Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.updateBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_invoice_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoices/{account_invoice_id}")
    public ResponseEntity<Account_invoiceDTO> get(@PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoice domain = account_invoiceService.get(account_invoice_id);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @ApiOperation(value = "获取草稿数据", tags = {"Account_invoice" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoices/getdraft")
    public ResponseEntity<Account_invoiceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoiceMapping.toDto(account_invoiceService.getDraft(new Account_invoice())));
    }




    @ApiOperation(value = "Save", tags = {"Account_invoice" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoiceDTO account_invoicedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.save(account_invoiceMapping.toDomain(account_invoicedto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Account_invoice" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.saveBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_invoice-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoices/fetchdefault")
	public ResponseEntity<List<Account_invoiceDTO>> fetchDefault(Account_invoiceSearchContext context) {
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
        List<Account_invoiceDTO> list = account_invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_invoice-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoices/searchdefault")
	public ResponseEntity<Page<Account_invoiceDTO>> searchDefault(@RequestBody Account_invoiceSearchContext context) {
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @ApiOperation(value = "删除数据ByRes_partner", tags = {"Account_invoice" },  notes = "删除数据ByRes_partner")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")

    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) {
		return ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.remove(account_invoice_id));
    }

    @ApiOperation(value = "RemoveBatchByRes_partner", tags = {"Account_invoice" },  notes = "RemoveBatchByRes_partner")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/account_invoices/batch")
    public ResponseEntity<Boolean> removeBatchByRes_partner(@RequestBody List<Integer> ids) {
        account_invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "CheckKeyByRes_partner", tags = {"Account_invoice" },  notes = "CheckKeyByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.checkKey(account_invoiceMapping.toDomain(account_invoicedto)));
    }

    @ApiOperation(value = "建立数据ByRes_partner", tags = {"Account_invoice" },  notes = "建立数据ByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices")

    public ResponseEntity<Account_invoiceDTO> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
        domain.setPartnerId(res_partner_id);
		account_invoiceService.create(domain);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "createBatchByRes_partner", tags = {"Account_invoice" },  notes = "createBatchByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices/batch")
    public ResponseEntity<Boolean> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        List<Account_invoice> domainlist=account_invoiceMapping.toDomain(account_invoicedtos);
        for(Account_invoice domain:domainlist){
            domain.setPartnerId(res_partner_id);
        }
        account_invoiceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据ByRes_partner", tags = {"Account_invoice" },  notes = "更新数据ByRes_partner")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")

    public ResponseEntity<Account_invoiceDTO> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
        domain.setPartnerId(res_partner_id);
        domain.setId(account_invoice_id);
		account_invoiceService.update(domain);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "UpdateBatchByRes_partner", tags = {"Account_invoice" },  notes = "UpdateBatchByRes_partner")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/account_invoices/batch")
    public ResponseEntity<Boolean> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        List<Account_invoice> domainlist=account_invoiceMapping.toDomain(account_invoicedtos);
        for(Account_invoice domain:domainlist){
            domain.setPartnerId(res_partner_id);
        }
        account_invoiceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据ByRes_partner", tags = {"Account_invoice" },  notes = "获取数据ByRes_partner")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    public ResponseEntity<Account_invoiceDTO> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoice domain = account_invoiceService.get(account_invoice_id);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据ByRes_partner", tags = {"Account_invoice" },  notes = "获取草稿数据ByRes_partner")
    @RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/account_invoices/getdraft")
    public ResponseEntity<Account_invoiceDTO> getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id) {
        Account_invoice domain = new Account_invoice();
        domain.setPartnerId(res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoiceMapping.toDto(account_invoiceService.getDraft(domain)));
    }

    @ApiOperation(value = "SaveByRes_partner", tags = {"Account_invoice" },  notes = "SaveByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices/save")
    public ResponseEntity<Boolean> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
        domain.setPartnerId(res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.save(domain));
    }

    @ApiOperation(value = "SaveBatchByRes_partner", tags = {"Account_invoice" },  notes = "SaveBatchByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        List<Account_invoice> domainlist=account_invoiceMapping.toDomain(account_invoicedtos);
        for(Account_invoice domain:domainlist){
             domain.setPartnerId(res_partner_id);
        }
        account_invoiceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "fetch默认查询ByRes_partner", tags = {"Account_invoice" } ,notes = "fetch默认查询ByRes_partner")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/account_invoices/fetchdefault")
	public ResponseEntity<List<Account_invoiceDTO>> fetchAccount_invoiceDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Account_invoiceSearchContext context) {
        context.setN_partner_id_eq(res_partner_id);
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
        List<Account_invoiceDTO> list = account_invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

	@ApiOperation(value = "search默认查询ByRes_partner", tags = {"Account_invoice" } ,notes = "search默认查询ByRes_partner")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/{res_partner_id}/account_invoices/searchdefault")
	public ResponseEntity<Page<Account_invoiceDTO>> searchAccount_invoiceDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoiceSearchContext context) {
        context.setN_partner_id_eq(res_partner_id);
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice getEntity(){
        return new Account_invoice();
    }

}
