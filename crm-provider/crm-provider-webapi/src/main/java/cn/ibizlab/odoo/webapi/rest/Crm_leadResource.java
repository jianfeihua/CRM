package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_leadService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;




@Slf4j
@Api(tags = {"Crm_lead" })
@RestController("WebApi-crm_lead")
@RequestMapping("")
public class Crm_leadResource {

    @Autowired
    private ICrm_leadService crm_leadService;

    @Autowired
    @Lazy
    private Crm_leadMapping crm_leadMapping;




    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_leads/getdraft")
    public ResponseEntity<Crm_leadDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_leadMapping.toDto(crm_leadService.getDraft(new Crm_lead())));
    }




    @PreAuthorize("hasPermission('Remove',{#crm_lead_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_lead" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/{crm_lead_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_id") Integer crm_lead_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_leadService.remove(crm_lead_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_lead" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "Save", tags = {"Crm_lead" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_leadDTO crm_leaddto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_leadService.save(crm_leadMapping.toDomain(crm_leaddto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Crm_lead" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.saveBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#crm_lead_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_lead" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_leads/{crm_lead_id}")
    public ResponseEntity<Crm_leadDTO> get(@PathVariable("crm_lead_id") Integer crm_lead_id) {
        Crm_lead domain = crm_leadService.get(crm_lead_id);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_lead" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads")

    public ResponseEntity<Crm_leadDTO> create(@RequestBody Crm_leadDTO crm_leaddto) {
        Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
		crm_leadService.create(domain);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_lead" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.createBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "CheckKey", tags = {"Crm_lead" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_leadDTO crm_leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_leadService.checkKey(crm_leadMapping.toDomain(crm_leaddto)));
    }




    @PreAuthorize("hasPermission(#crm_lead_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_lead" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/{crm_lead_id}")

    public ResponseEntity<Crm_leadDTO> update(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_leadDTO crm_leaddto) {
		Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
        domain.setId(crm_lead_id);
		crm_leadService.update(domain);
		Crm_leadDTO dto = crm_leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_lead_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_lead" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.updateBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_lead" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_leads/fetchdefault")
	public ResponseEntity<List<Crm_leadDTO>> fetchDefault(Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
        List<Crm_leadDTO> list = crm_leadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_lead" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_leads/searchdefault")
	public ResponseEntity<Page<Crm_leadDTO>> searchDefault(@RequestBody Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead-Default2-all')")
	@ApiOperation(value = "fetch默认查询(商机类型排序)", tags = {"Crm_lead" } ,notes = "fetch默认查询(商机类型排序)")
    @RequestMapping(method= RequestMethod.GET , value="/crm_leads/fetchdefault2")
	public ResponseEntity<List<Crm_leadDTO>> fetchDefault2(Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault2(context) ;
        List<Crm_leadDTO> list = crm_leadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead-Default2-all')")
	@ApiOperation(value = "search默认查询(商机类型排序)", tags = {"Crm_lead" } ,notes = "search默认查询(商机类型排序)")
    @RequestMapping(method= RequestMethod.POST , value="/crm_leads/searchdefault2")
	public ResponseEntity<Page<Crm_leadDTO>> searchDefault2(@RequestBody Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault2(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @ApiOperation(value = "获取草稿数据ByRes_partner", tags = {"Crm_lead" },  notes = "获取草稿数据ByRes_partner")
    @RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/crm_leads/getdraft")
    public ResponseEntity<Crm_leadDTO> getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id) {
        Crm_lead domain = new Crm_lead();
        domain.setPartnerId(res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_leadMapping.toDto(crm_leadService.getDraft(domain)));
    }

    @ApiOperation(value = "删除数据ByRes_partner", tags = {"Crm_lead" },  notes = "删除数据ByRes_partner")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")

    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(crm_leadService.remove(crm_lead_id));
    }

    @ApiOperation(value = "RemoveBatchByRes_partner", tags = {"Crm_lead" },  notes = "RemoveBatchByRes_partner")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/crm_leads/batch")
    public ResponseEntity<Boolean> removeBatchByRes_partner(@RequestBody List<Integer> ids) {
        crm_leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "SaveByRes_partner", tags = {"Crm_lead" },  notes = "SaveByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads/save")
    public ResponseEntity<Boolean> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_leadDTO crm_leaddto) {
        Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
        domain.setPartnerId(res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_leadService.save(domain));
    }

    @ApiOperation(value = "SaveBatchByRes_partner", tags = {"Crm_lead" },  notes = "SaveBatchByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Crm_leadDTO> crm_leaddtos) {
        List<Crm_lead> domainlist=crm_leadMapping.toDomain(crm_leaddtos);
        for(Crm_lead domain:domainlist){
             domain.setPartnerId(res_partner_id);
        }
        crm_leadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据ByRes_partner", tags = {"Crm_lead" },  notes = "获取数据ByRes_partner")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    public ResponseEntity<Crm_leadDTO> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id) {
        Crm_lead domain = crm_leadService.get(crm_lead_id);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据ByRes_partner", tags = {"Crm_lead" },  notes = "建立数据ByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads")

    public ResponseEntity<Crm_leadDTO> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_leadDTO crm_leaddto) {
        Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
        domain.setPartnerId(res_partner_id);
		crm_leadService.create(domain);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "createBatchByRes_partner", tags = {"Crm_lead" },  notes = "createBatchByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads/batch")
    public ResponseEntity<Boolean> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Crm_leadDTO> crm_leaddtos) {
        List<Crm_lead> domainlist=crm_leadMapping.toDomain(crm_leaddtos);
        for(Crm_lead domain:domainlist){
            domain.setPartnerId(res_partner_id);
        }
        crm_leadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "CheckKeyByRes_partner", tags = {"Crm_lead" },  notes = "CheckKeyByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_leadDTO crm_leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_leadService.checkKey(crm_leadMapping.toDomain(crm_leaddto)));
    }

    @ApiOperation(value = "更新数据ByRes_partner", tags = {"Crm_lead" },  notes = "更新数据ByRes_partner")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")

    public ResponseEntity<Crm_leadDTO> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_leadDTO crm_leaddto) {
        Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
        domain.setPartnerId(res_partner_id);
        domain.setId(crm_lead_id);
		crm_leadService.update(domain);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "UpdateBatchByRes_partner", tags = {"Crm_lead" },  notes = "UpdateBatchByRes_partner")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/crm_leads/batch")
    public ResponseEntity<Boolean> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Crm_leadDTO> crm_leaddtos) {
        List<Crm_lead> domainlist=crm_leadMapping.toDomain(crm_leaddtos);
        for(Crm_lead domain:domainlist){
            domain.setPartnerId(res_partner_id);
        }
        crm_leadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "fetch默认查询ByRes_partner", tags = {"Crm_lead" } ,notes = "fetch默认查询ByRes_partner")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/crm_leads/fetchdefault")
	public ResponseEntity<List<Crm_leadDTO>> fetchCrm_leadDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Crm_leadSearchContext context) {
        context.setN_partner_id_eq(res_partner_id);
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
        List<Crm_leadDTO> list = crm_leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

	@ApiOperation(value = "search默认查询ByRes_partner", tags = {"Crm_lead" } ,notes = "search默认查询ByRes_partner")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/{res_partner_id}/crm_leads/searchdefault")
	public ResponseEntity<Page<Crm_leadDTO>> searchCrm_leadDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_leadSearchContext context) {
        context.setN_partner_id_eq(res_partner_id);
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "fetch默认查询(商机类型排序)ByRes_partner", tags = {"Crm_lead" } ,notes = "fetch默认查询(商机类型排序)ByRes_partner")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/crm_leads/fetchdefault2")
	public ResponseEntity<List<Crm_leadDTO>> fetchCrm_leadDefault2ByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Crm_leadSearchContext context) {
        context.setN_partner_id_eq(res_partner_id);
        Page<Crm_lead> domains = crm_leadService.searchDefault2(context) ;
        List<Crm_leadDTO> list = crm_leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

	@ApiOperation(value = "search默认查询(商机类型排序)ByRes_partner", tags = {"Crm_lead" } ,notes = "search默认查询(商机类型排序)ByRes_partner")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/{res_partner_id}/crm_leads/searchdefault2")
	public ResponseEntity<Page<Crm_leadDTO>> searchCrm_leadDefault2ByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_leadSearchContext context) {
        context.setN_partner_id_eq(res_partner_id);
        Page<Crm_lead> domains = crm_leadService.searchDefault2(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_lead getEntity(){
        return new Crm_lead();
    }

}
