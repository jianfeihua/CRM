package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_acquirerService;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirerSearchContext;




@Slf4j
@Api(tags = {"Payment_acquirer" })
@RestController("WebApi-payment_acquirer")
@RequestMapping("")
public class Payment_acquirerResource {

    @Autowired
    private IPayment_acquirerService payment_acquirerService;

    @Autowired
    @Lazy
    private Payment_acquirerMapping payment_acquirerMapping;




    @PreAuthorize("hasPermission('Remove',{#payment_acquirer_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Payment_acquirer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirers/{payment_acquirer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_acquirer_id") Integer payment_acquirer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_acquirerService.remove(payment_acquirer_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Payment_acquirer" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        payment_acquirerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#payment_acquirer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Payment_acquirer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirers/{payment_acquirer_id}")

    public ResponseEntity<Payment_acquirerDTO> update(@PathVariable("payment_acquirer_id") Integer payment_acquirer_id, @RequestBody Payment_acquirerDTO payment_acquirerdto) {
		Payment_acquirer domain = payment_acquirerMapping.toDomain(payment_acquirerdto);
        domain.setId(payment_acquirer_id);
		payment_acquirerService.update(domain);
		Payment_acquirerDTO dto = payment_acquirerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#payment_acquirer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Payment_acquirer" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {
        payment_acquirerService.updateBatch(payment_acquirerMapping.toDomain(payment_acquirerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#payment_acquirer_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Payment_acquirer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_acquirers/{payment_acquirer_id}")
    public ResponseEntity<Payment_acquirerDTO> get(@PathVariable("payment_acquirer_id") Integer payment_acquirer_id) {
        Payment_acquirer domain = payment_acquirerService.get(payment_acquirer_id);
        Payment_acquirerDTO dto = payment_acquirerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Payment_acquirer" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_acquirers/getdraft")
    public ResponseEntity<Payment_acquirerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(payment_acquirerMapping.toDto(payment_acquirerService.getDraft(new Payment_acquirer())));
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Payment_acquirer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers")

    public ResponseEntity<Payment_acquirerDTO> create(@RequestBody Payment_acquirerDTO payment_acquirerdto) {
        Payment_acquirer domain = payment_acquirerMapping.toDomain(payment_acquirerdto);
		payment_acquirerService.create(domain);
        Payment_acquirerDTO dto = payment_acquirerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Payment_acquirer" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {
        payment_acquirerService.createBatch(payment_acquirerMapping.toDomain(payment_acquirerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Payment_acquirer-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Payment_acquirer" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_acquirers/fetchdefault")
	public ResponseEntity<List<Payment_acquirerDTO>> fetchDefault(Payment_acquirerSearchContext context) {
        Page<Payment_acquirer> domains = payment_acquirerService.searchDefault(context) ;
        List<Payment_acquirerDTO> list = payment_acquirerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Payment_acquirer-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Payment_acquirer" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/payment_acquirers/searchdefault")
	public ResponseEntity<Page<Payment_acquirerDTO>> searchDefault(@RequestBody Payment_acquirerSearchContext context) {
        Page<Payment_acquirer> domains = payment_acquirerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_acquirerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Payment_acquirer getEntity(){
        return new Payment_acquirer();
    }

}
