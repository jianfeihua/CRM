package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activityService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;




@Slf4j
@Api(tags = {"Mail_activity" })
@RestController("WebApi-mail_activity")
@RequestMapping("")
public class Mail_activityResource {

    @Autowired
    private IMail_activityService mail_activityService;

    @Autowired
    @Lazy
    private Mail_activityMapping mail_activityMapping;




    @ApiOperation(value = "获取草稿数据", tags = {"Mail_activity" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activities/getdraft")
    public ResponseEntity<Mail_activityDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activityMapping.toDto(mail_activityService.getDraft(new Mail_activity())));
    }







    @PreAuthorize("hasPermission(#mail_activity_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_activity" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/{mail_activity_id}")

    public ResponseEntity<Mail_activityDTO> update(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activityDTO mail_activitydto) {
		Mail_activity domain = mail_activityMapping.toDomain(mail_activitydto);
        domain.setId(mail_activity_id);
		mail_activityService.update(domain);
		Mail_activityDTO dto = mail_activityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_activity_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_activity" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        mail_activityService.updateBatch(mail_activityMapping.toDomain(mail_activitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @ApiOperation(value = "CheckKey", tags = {"Mail_activity" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activityDTO mail_activitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_activityService.checkKey(mail_activityMapping.toDomain(mail_activitydto)));
    }




    @ApiOperation(value = "Save", tags = {"Mail_activity" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_activityDTO mail_activitydto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activityService.save(mail_activityMapping.toDomain(mail_activitydto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Mail_activity" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        mail_activityService.saveBatch(mail_activityMapping.toDomain(mail_activitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_activity_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_activity" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/{mail_activity_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_id") Integer mail_activity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_activityService.remove(mail_activity_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_activity" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_activityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_activity_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_activity" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activities/{mail_activity_id}")
    public ResponseEntity<Mail_activityDTO> get(@PathVariable("mail_activity_id") Integer mail_activity_id) {
        Mail_activity domain = mail_activityService.get(mail_activity_id);
        Mail_activityDTO dto = mail_activityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_activity" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities")

    public ResponseEntity<Mail_activityDTO> create(@RequestBody Mail_activityDTO mail_activitydto) {
        Mail_activity domain = mail_activityMapping.toDomain(mail_activitydto);
		mail_activityService.create(domain);
        Mail_activityDTO dto = mail_activityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_activity" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        mail_activityService.createBatch(mail_activityMapping.toDomain(mail_activitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Mail_activity-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_activity" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_activities/fetchdefault")
	public ResponseEntity<List<Mail_activityDTO>> fetchDefault(Mail_activitySearchContext context) {
        Page<Mail_activity> domains = mail_activityService.searchDefault(context) ;
        List<Mail_activityDTO> list = mail_activityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Mail_activity-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Mail_activity" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/mail_activities/searchdefault")
	public ResponseEntity<Page<Mail_activityDTO>> searchDefault(@RequestBody Mail_activitySearchContext context) {
        Page<Mail_activity> domains = mail_activityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_activityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_activity getEntity(){
        return new Mail_activity();
    }

}
