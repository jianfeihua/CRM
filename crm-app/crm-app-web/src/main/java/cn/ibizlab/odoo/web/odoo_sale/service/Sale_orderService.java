package cn.ibizlab.odoo.web.odoo_sale.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.web.odoo_sale.filter.*;
import cn.ibizlab.odoo.web.odoo_sale.feign.Sale_orderFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Sale_orderService {

    Sale_orderFeignClient client;

    @Autowired
    public Sale_orderService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Sale_orderFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Sale_orderFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Sale_order updateBatch(Integer sale_order_id, Sale_order sale_order) {
        return client.updateBatch(sale_order_id, sale_order);
    }

    public Sale_order removeBatch(Integer sale_order_id, Sale_order sale_order) {
        return client.removeBatch(sale_order_id, sale_order);
    }

    public Sale_order update(Integer sale_order_id, Sale_order sale_order) {
        return client.update(sale_order_id, sale_order);
    }

    public Sale_order get( Integer sale_order_id) {
        return client.get( sale_order_id);
    }

    public Sale_order save(Integer sale_order_id, Sale_order sale_order) {
        return client.save(sale_order_id, sale_order);
    }

    public boolean checkKey(Sale_order sale_order) {
        return client.checkKey(sale_order);
    }

    public boolean remove( Integer sale_order_id) {
        return client.remove( sale_order_id);
    }

    public Sale_order getDraft(Integer sale_order_id, Sale_order sale_order) {
        return client.getDraft(sale_order_id, sale_order);
    }

    public Sale_order createBatch(Integer sale_order_id, Sale_order sale_order) {
        return client.createBatch(sale_order_id, sale_order);
    }

	public Sale_order create(Sale_order sale_order) {
        return client.create(sale_order);
    }

	public Page<Sale_order> fetchDefault(Sale_orderSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }


    public Sale_order updateBatchByRes_partner(Integer res_partner_id, Integer sale_order_id, Sale_order sale_order) {
        return client.updateBatchByRes_partner(res_partner_id, sale_order_id, sale_order);
    }

    public Sale_order removeBatchByRes_partner(Integer res_partner_id, Integer sale_order_id, Sale_order sale_order) {
        return client.removeBatchByRes_partner(res_partner_id, sale_order_id, sale_order);
    }

    public Sale_order updateByRes_partner(Integer res_partner_id, Integer sale_order_id, Sale_order sale_order) {
        return client.updateByRes_partner(res_partner_id, sale_order_id, sale_order);
    }

    public Sale_order getByRes_partner(Integer res_partner_id,  Integer sale_order_id) {
        return client.getByRes_partner(res_partner_id,  sale_order_id);
    }

    public Sale_order saveByRes_partner(Integer res_partner_id, Integer sale_order_id, Sale_order sale_order) {
        return client.saveByRes_partner(res_partner_id, sale_order_id, sale_order);
    }

    public boolean checkKeyByRes_partner(Integer res_partner_id, Sale_order sale_order) {
        return client.checkKeyByRes_partner(res_partner_id, sale_order);
    }

    public boolean removeByRes_partner(Integer res_partner_id,  Integer sale_order_id) {
        return client.removeByRes_partner(res_partner_id,  sale_order_id);
    }

    public Sale_order getDraftByRes_partner(Integer res_partner_id, Integer sale_order_id, Sale_order sale_order) {
        return client.getDraftByRes_partner(res_partner_id, sale_order_id, sale_order);
    }

    public Sale_order createBatchByRes_partner(Integer res_partner_id, Integer sale_order_id, Sale_order sale_order) {
        return client.createBatchByRes_partner(res_partner_id, sale_order_id, sale_order);
    }

	public Sale_order createByRes_partner(Integer res_partner_id, Sale_order sale_order) {
        return client.createByRes_partner(res_partner_id, sale_order);
    }

	public Page<Sale_order> fetchDefaultByRes_partner(Integer res_partner_id , Sale_orderSearchContext searchContext) {
        return client.fetchDefaultByRes_partner(res_partner_id , searchContext);
    }

}
