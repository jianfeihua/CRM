package cn.ibizlab.odoo.web.odoo_base.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_base.service.Res_partnerService;
import cn.ibizlab.odoo.web.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.web.odoo_base.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Res_partnerController {
	@Autowired
    Res_partnerService res_partnerservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_id") Integer res_partner_id) {
        boolean b = res_partnerservice.remove( res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'READ')")
    public ResponseEntity<Res_partner> get(@PathVariable("res_partner_id") Integer res_partner_id) {
        Res_partner res_partner = res_partnerservice.get( res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/checkkey")
    @PreAuthorize("@res_partner_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner res_partner) {
        boolean b = res_partnerservice.checkKey(res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/save")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'')")
    public ResponseEntity<Res_partner> save(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) {
        Res_partner res_partner2 = res_partnerservice.save(res_partner_id, res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'UPDATE')")
    public ResponseEntity<Res_partner> update(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) {
        Res_partner res_partner2 = res_partnerservice.update(res_partner_id, res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/res_partners/{res_partner_id}")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'UPDATE')")
    public ResponseEntity<Res_partner> api_update(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) {
        Res_partner res_partner2 = res_partnerservice.update(res_partner_id, res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/createbatch")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'CREATE')")
    public ResponseEntity<Res_partner> createBatch(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) {
        Res_partner res_partner2 = res_partnerservice.createBatch(res_partner_id, res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/updatebatch")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'UPDATE')")
    public ResponseEntity<Res_partner> updateBatch(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) {
        Res_partner res_partner2 = res_partnerservice.updateBatch(res_partner_id, res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/getdraft")
    @PreAuthorize("@res_partner_pms.check('CREATE')")
    public ResponseEntity<Res_partner> getDraft() {
        //Res_partner res_partner = res_partnerservice.getDraft( res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Res_partner());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners")
    @PreAuthorize("@res_partner_pms.check('CREATE')")
    public ResponseEntity<Res_partner> create(@RequestBody Res_partner res_partner) {
        Res_partner res_partner2 = res_partnerservice.create(res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/removebatch")
    @PreAuthorize("@res_partner_pms.check(#res_partner_id,'DELETE')")
    public ResponseEntity<Res_partner> removeBatch(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) {
        Res_partner res_partner2 = res_partnerservice.removeBatch(res_partner_id, res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/fetchcontacts")
    @PreAuthorize("@res_partner_pms.check('READ')")
	public ResponseEntity<List<Res_partner>> fetchContacts(Res_partnerSearchContext searchContext,Pageable pageable) {
        
        Page<Res_partner> page = res_partnerservice.fetchContacts(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/fetchcompany")
    @PreAuthorize("@res_partner_pms.check('READ')")
	public ResponseEntity<List<Res_partner>> fetchCompany(Res_partnerSearchContext searchContext,Pageable pageable) {
        
        Page<Res_partner> page = res_partnerservice.fetchCompany(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/fetchdefault")
    @PreAuthorize("@res_partner_pms.check('READ')")
	public ResponseEntity<List<Res_partner>> fetchDefault(Res_partnerSearchContext searchContext,Pageable pageable) {
        
        Page<Res_partner> page = res_partnerservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
