package cn.ibizlab.odoo.web.security.userdetail;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import cn.ibizlab.odoo.util.security.userdetail.LoginUser;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
public class LoginUserDetailService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>() ;
        SimpleGrantedAuthority userAuthority = new SimpleGrantedAuthority("USER");
        authorities.add(userAuthority);
        if(username.equals("admin")){
            SimpleGrantedAuthority adminAuthority = new SimpleGrantedAuthority("ADMIN");
            authorities.add(adminAuthority);
        }
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder() ;
        LoginUser user = new LoginUser(username,passwordEncoder.encode("123456"),authorities) ;
        return user;

    }
}
