package cn.ibizlab.odoo.web.security.permission.odoo_calendar;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_calendar.service.Calendar_eventService;
import cn.ibizlab.odoo.web.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("calendar_event_pms")
public class Calendar_eventPermission {

    @Autowired
    Calendar_eventService calendar_eventservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer calendar_event_id, String action){
        return true ;
    }


}
