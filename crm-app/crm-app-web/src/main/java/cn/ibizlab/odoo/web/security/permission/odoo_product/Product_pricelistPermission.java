package cn.ibizlab.odoo.web.security.permission.odoo_product;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_product.service.Product_pricelistService;
import cn.ibizlab.odoo.web.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("product_pricelist_pms")
public class Product_pricelistPermission {

    @Autowired
    Product_pricelistService product_pricelistservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer product_pricelist_id, String action){
        return true ;
    }


}
