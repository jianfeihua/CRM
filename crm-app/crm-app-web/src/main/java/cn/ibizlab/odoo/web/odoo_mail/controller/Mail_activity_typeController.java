package cn.ibizlab.odoo.web.odoo_mail.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_mail.service.Mail_activity_typeService;
import cn.ibizlab.odoo.web.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.web.odoo_mail.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Mail_activity_typeController {
	@Autowired
    Mail_activity_typeService mail_activity_typeservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/{mail_activity_type_id}/removebatch")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'DELETE')")
    public ResponseEntity<Mail_activity_type> removeBatch(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) {
        Mail_activity_type mail_activity_type2 = mail_activity_typeservice.removeBatch(mail_activity_type_id, mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/{mail_activity_type_id}/createbatch")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'CREATE')")
    public ResponseEntity<Mail_activity_type> createBatch(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) {
        Mail_activity_type mail_activity_type2 = mail_activity_typeservice.createBatch(mail_activity_type_id, mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/{mail_activity_type_id}")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) {
        boolean b = mail_activity_typeservice.remove( mail_activity_type_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/{mail_activity_type_id}")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'READ')")
    public ResponseEntity<Mail_activity_type> get(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) {
        Mail_activity_type mail_activity_type = mail_activity_typeservice.get( mail_activity_type_id);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/getdraft")
    @PreAuthorize("@mail_activity_type_pms.check('CREATE')")
    public ResponseEntity<Mail_activity_type> getDraft() {
        //Mail_activity_type mail_activity_type = mail_activity_typeservice.getDraft( mail_activity_type_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Mail_activity_type());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/checkkey")
    @PreAuthorize("@mail_activity_type_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activity_type mail_activity_type) {
        boolean b = mail_activity_typeservice.checkKey(mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types")
    @PreAuthorize("@mail_activity_type_pms.check('CREATE')")
    public ResponseEntity<Mail_activity_type> create(@RequestBody Mail_activity_type mail_activity_type) {
        Mail_activity_type mail_activity_type2 = mail_activity_typeservice.create(mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/{mail_activity_type_id}/updatebatch")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'UPDATE')")
    public ResponseEntity<Mail_activity_type> updateBatch(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) {
        Mail_activity_type mail_activity_type2 = mail_activity_typeservice.updateBatch(mail_activity_type_id, mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/{mail_activity_type_id}")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'UPDATE')")
    public ResponseEntity<Mail_activity_type> update(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) {
        Mail_activity_type mail_activity_type2 = mail_activity_typeservice.update(mail_activity_type_id, mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/mail_activity_types/{mail_activity_type_id}")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'UPDATE')")
    public ResponseEntity<Mail_activity_type> api_update(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) {
        Mail_activity_type mail_activity_type2 = mail_activity_typeservice.update(mail_activity_type_id, mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/{mail_activity_type_id}/save")
    @PreAuthorize("@mail_activity_type_pms.check(#mail_activity_type_id,'')")
    public ResponseEntity<Mail_activity_type> save(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) {
        Mail_activity_type mail_activity_type2 = mail_activity_typeservice.save(mail_activity_type_id, mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_type2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/mail_activity_types/fetchdefault")
    @PreAuthorize("@mail_activity_type_pms.check('READ')")
	public ResponseEntity<List<Mail_activity_type>> fetchDefault(Mail_activity_typeSearchContext searchContext,Pageable pageable) {
        
        Page<Mail_activity_type> page = mail_activity_typeservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
