package cn.ibizlab.odoo.web.odoo_account.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.web.odoo_account.filter.*;


public interface Account_invoice_lineFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoice_lines/{account_invoice_line_id}")
    public Account_invoice_line update(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines/checkkey")
    public Boolean checkKey(@RequestBody Account_invoice_line account_invoice_line) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoice_lines/{account_invoice_line_id}/updatebatch")
    public Account_invoice_line updateBatch(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines/{account_invoice_line_id}/createbatch")
    public Account_invoice_line createBatch(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoice_lines/{account_invoice_line_id}/removebatch")
    public Account_invoice_line removeBatch(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoice_lines/{account_invoice_line_id}")
    public Account_invoice_line get(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoice_lines/{account_invoice_line_id}/getdraft")
    public Account_invoice_line getDraft(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines/{account_invoice_line_id}/save")
    public Account_invoice_line save(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoice_lines/{account_invoice_line_id}")
    public Boolean remove(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoice_lines")
    public Account_invoice_line create(@RequestBody Account_invoice_line account_invoice_line) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/account_invoice_lines/fetchdefault")
	public Page<Account_invoice_line> fetchDefault(Account_invoice_lineSearchContext searchContext) ;
}
