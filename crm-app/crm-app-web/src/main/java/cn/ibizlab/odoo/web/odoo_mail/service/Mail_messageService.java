package cn.ibizlab.odoo.web.odoo_mail.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.web.odoo_mail.filter.*;
import cn.ibizlab.odoo.web.odoo_mail.feign.Mail_messageFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Mail_messageService {

    Mail_messageFeignClient client;

    @Autowired
    public Mail_messageService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Mail_messageFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Mail_messageFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

	public Mail_message create(Mail_message mail_message) {
        return client.create(mail_message);
    }

    public Mail_message get( Integer mail_message_id) {
        return client.get( mail_message_id);
    }

    public boolean checkKey(Mail_message mail_message) {
        return client.checkKey(mail_message);
    }

    public Mail_message updateBatch(Integer mail_message_id, Mail_message mail_message) {
        return client.updateBatch(mail_message_id, mail_message);
    }

    public Mail_message getDraft(Integer mail_message_id, Mail_message mail_message) {
        return client.getDraft(mail_message_id, mail_message);
    }

    public Mail_message createBatch(Integer mail_message_id, Mail_message mail_message) {
        return client.createBatch(mail_message_id, mail_message);
    }

    public Mail_message removeBatch(Integer mail_message_id, Mail_message mail_message) {
        return client.removeBatch(mail_message_id, mail_message);
    }

    public boolean remove( Integer mail_message_id) {
        return client.remove( mail_message_id);
    }

    public Mail_message update(Integer mail_message_id, Mail_message mail_message) {
        return client.update(mail_message_id, mail_message);
    }

    public Mail_message save(Integer mail_message_id, Mail_message mail_message) {
        return client.save(mail_message_id, mail_message);
    }

	public Page<Mail_message> fetchDefault(Mail_messageSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
