package cn.ibizlab.odoo.mob.odoo_calendar.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.mob.odoo_calendar.filter.*;


public interface Calendar_eventFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/calendar_events/{calendar_event_id}")
    public Calendar_event update(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/calendar_events/{calendar_event_id}/updatebatch")
    public Calendar_event updateBatch(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events/{calendar_event_id}/createbatch")
    public Calendar_event createBatch(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/calendar_events/{calendar_event_id}")
    public Calendar_event get(@PathVariable("calendar_event_id") Integer calendar_event_id) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/calendar_events/{calendar_event_id}/removebatch")
    public Calendar_event removeBatch(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/calendar_events/{calendar_event_id}")
    public Boolean remove(@PathVariable("calendar_event_id") Integer calendar_event_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/calendar_events/{calendar_event_id}/getdraft")
    public Calendar_event getDraft(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events")
    public Calendar_event create(@RequestBody Calendar_event calendar_event) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events/{calendar_event_id}/save")
    public Calendar_event save(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events/checkkey")
    public Boolean checkKey(@RequestBody Calendar_event calendar_event) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/calendar_events/fetchdefault")
	public Page<Calendar_event> fetchDefault(Calendar_eventSearchContext searchContext) ;
}
