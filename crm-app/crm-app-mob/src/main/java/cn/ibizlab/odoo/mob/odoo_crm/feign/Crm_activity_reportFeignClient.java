package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_activity_reportFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_activity_reports/{crm_activity_report_id}/updatebatch")
    public Crm_activity_report updateBatch(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_activity_reports")
    public Crm_activity_report create(@RequestBody Crm_activity_report crm_activity_report) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_activity_reports/{crm_activity_report_id}")
    public Crm_activity_report update(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_activity_reports/{crm_activity_report_id}/removebatch")
    public Crm_activity_report removeBatch(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_activity_reports/{crm_activity_report_id}")
    public Boolean remove(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_activity_reports/{crm_activity_report_id}")
    public Crm_activity_report get(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_activity_reports/{crm_activity_report_id}/createbatch")
    public Crm_activity_report createBatch(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_activity_reports/{crm_activity_report_id}/getdraft")
    public Crm_activity_report getDraft(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_activity_reports/fetchdefault")
	public Page<Crm_activity_report> fetchDefault(Crm_activity_reportSearchContext searchContext) ;
}
