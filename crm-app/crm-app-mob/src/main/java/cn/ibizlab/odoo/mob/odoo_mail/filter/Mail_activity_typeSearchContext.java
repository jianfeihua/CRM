package cn.ibizlab.odoo.mob.odoo_mail.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Mail_activity_typeSearchContext extends SearchContext implements Serializable {

	public String n_category_eq;//[类别]
	public String n_delay_from_eq;//[延迟类型]
	public String n_name_like;//[名称]
	public String n_decoration_type_eq;//[排版类型]
	public String n_delay_unit_eq;//[延迟单位]
	public String n_default_next_type_id_text_eq;//[设置默认下一个活动]
	public String n_default_next_type_id_text_like;//[设置默认下一个活动]
	public String n_write_uid_text_eq;//[最后更新者]
	public String n_write_uid_text_like;//[最后更新者]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_write_uid_eq;//[最后更新者]
	public Integer n_default_next_type_id_eq;//[设置默认下一个活动]

}