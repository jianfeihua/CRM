package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_lead_lostService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_lead_lostController {
	@Autowired
    Crm_lead_lostService crm_lead_lostservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/{crm_lead_lost_id}/updatebatch")
    @PreAuthorize("@crm_lead_lost_pms.check(#crm_lead_lost_id,'UPDATE')")
    public ResponseEntity<Crm_lead_lost> updateBatch(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) {
        Crm_lead_lost crm_lead_lost2 = crm_lead_lostservice.updateBatch(crm_lead_lost_id, crm_lead_lost);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lost2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/{crm_lead_lost_id}")
    @PreAuthorize("@crm_lead_lost_pms.check(#crm_lead_lost_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) {
        boolean b = crm_lead_lostservice.remove( crm_lead_lost_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/{crm_lead_lost_id}")
    @PreAuthorize("@crm_lead_lost_pms.check(#crm_lead_lost_id,'UPDATE')")
    public ResponseEntity<Crm_lead_lost> update(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) {
        Crm_lead_lost crm_lead_lost2 = crm_lead_lostservice.update(crm_lead_lost_id, crm_lead_lost);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lost2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_lead_losts/{crm_lead_lost_id}")
    @PreAuthorize("@crm_lead_lost_pms.check(#crm_lead_lost_id,'UPDATE')")
    public ResponseEntity<Crm_lead_lost> api_update(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) {
        Crm_lead_lost crm_lead_lost2 = crm_lead_lostservice.update(crm_lead_lost_id, crm_lead_lost);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lost2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/getdraft")
    @PreAuthorize("@crm_lead_lost_pms.check('CREATE')")
    public ResponseEntity<Crm_lead_lost> getDraft() {
        //Crm_lead_lost crm_lead_lost = crm_lead_lostservice.getDraft( crm_lead_lost_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_lead_lost());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/{crm_lead_lost_id}")
    @PreAuthorize("@crm_lead_lost_pms.check(#crm_lead_lost_id,'READ')")
    public ResponseEntity<Crm_lead_lost> get(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) {
        Crm_lead_lost crm_lead_lost = crm_lead_lostservice.get( crm_lead_lost_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lost);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/{crm_lead_lost_id}/removebatch")
    @PreAuthorize("@crm_lead_lost_pms.check(#crm_lead_lost_id,'DELETE')")
    public ResponseEntity<Crm_lead_lost> removeBatch(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) {
        Crm_lead_lost crm_lead_lost2 = crm_lead_lostservice.removeBatch(crm_lead_lost_id, crm_lead_lost);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lost2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts")
    @PreAuthorize("@crm_lead_lost_pms.check('CREATE')")
    public ResponseEntity<Crm_lead_lost> create(@RequestBody Crm_lead_lost crm_lead_lost) {
        Crm_lead_lost crm_lead_lost2 = crm_lead_lostservice.create(crm_lead_lost);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lost2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/{crm_lead_lost_id}/createbatch")
    @PreAuthorize("@crm_lead_lost_pms.check(#crm_lead_lost_id,'CREATE')")
    public ResponseEntity<Crm_lead_lost> createBatch(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) {
        Crm_lead_lost crm_lead_lost2 = crm_lead_lostservice.createBatch(crm_lead_lost_id, crm_lead_lost);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lost2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead_losts/fetchdefault")
    @PreAuthorize("@crm_lead_lost_pms.check('READ')")
	public ResponseEntity<List<Crm_lead_lost>> fetchDefault(Crm_lead_lostSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_lead_lost> page = crm_lead_lostservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
