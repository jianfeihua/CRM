package cn.ibizlab.odoo.mob.odoo_account.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[登记付款]
 */
public class Account_register_payments implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 显示评论字段
     */
    private String show_communication_field;

    @JsonIgnore
    private boolean show_communication_fieldDirtyFlag;
    
    /**
     * 付款类型
     */
    private String payment_type;

    @JsonIgnore
    private boolean payment_typeDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 业务伙伴类型
     */
    private String partner_type;

    @JsonIgnore
    private boolean partner_typeDirtyFlag;
    
    /**
     * 备忘
     */
    private String communication;

    @JsonIgnore
    private boolean communicationDirtyFlag;
    
    /**
     * 付款差异
     */
    private Double payment_difference;

    @JsonIgnore
    private boolean payment_differenceDirtyFlag;
    
    /**
     * 显示合作伙伴银行账户
     */
    private String show_partner_bank_account;

    @JsonIgnore
    private boolean show_partner_bank_accountDirtyFlag;
    
    /**
     * 发票分组
     */
    private String group_invoices;

    @JsonIgnore
    private boolean group_invoicesDirtyFlag;
    
    /**
     * 隐藏付款方式
     */
    private String hide_payment_method;

    @JsonIgnore
    private boolean hide_payment_methodDirtyFlag;
    
    /**
     * 发票
     */
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 付款日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp payment_date;

    @JsonIgnore
    private boolean payment_dateDirtyFlag;
    
    /**
     * 日记账项目标签
     */
    private String writeoff_label;

    @JsonIgnore
    private boolean writeoff_labelDirtyFlag;
    
    /**
     * 多
     */
    private String multi;

    @JsonIgnore
    private boolean multiDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 付款差异处理
     */
    private String payment_difference_handling;

    @JsonIgnore
    private boolean payment_difference_handlingDirtyFlag;
    
    /**
     * 付款金额
     */
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;
    
    /**
     * 币种
     */
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 代码
     */
    private String payment_method_code;

    @JsonIgnore
    private boolean payment_method_codeDirtyFlag;
    
    /**
     * 付款方法类型
     */
    private String payment_method_id_text;

    @JsonIgnore
    private boolean payment_method_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 付款日记账
     */
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;
    
    /**
     * 业务伙伴
     */
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;
    
    /**
     * 差异科目
     */
    private String writeoff_account_id_text;

    @JsonIgnore
    private boolean writeoff_account_id_textDirtyFlag;
    
    /**
     * 差异科目
     */
    private Integer writeoff_account_id;

    @JsonIgnore
    private boolean writeoff_account_idDirtyFlag;
    
    /**
     * 收款银行账号
     */
    private Integer partner_bank_account_id;

    @JsonIgnore
    private boolean partner_bank_account_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 业务伙伴
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 付款日记账
     */
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;
    
    /**
     * 币种
     */
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;
    
    /**
     * 付款方法类型
     */
    private Integer payment_method_id;

    @JsonIgnore
    private boolean payment_method_idDirtyFlag;
    

    /**
     * 获取 [显示评论字段]
     */
    @JsonProperty("show_communication_field")
    public String getShow_communication_field(){
        return this.show_communication_field ;
    }

    /**
     * 设置 [显示评论字段]
     */
    @JsonProperty("show_communication_field")
    public void setShow_communication_field(String  show_communication_field){
        this.show_communication_field = show_communication_field ;
        this.show_communication_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [显示评论字段]脏标记
     */
    @JsonIgnore
    public boolean getShow_communication_fieldDirtyFlag(){
        return this.show_communication_fieldDirtyFlag ;
    }

    /**
     * 获取 [付款类型]
     */
    @JsonProperty("payment_type")
    public String getPayment_type(){
        return this.payment_type ;
    }

    /**
     * 设置 [付款类型]
     */
    @JsonProperty("payment_type")
    public void setPayment_type(String  payment_type){
        this.payment_type = payment_type ;
        this.payment_typeDirtyFlag = true ;
    }

    /**
     * 获取 [付款类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_typeDirtyFlag(){
        return this.payment_typeDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴类型]
     */
    @JsonProperty("partner_type")
    public String getPartner_type(){
        return this.partner_type ;
    }

    /**
     * 设置 [业务伙伴类型]
     */
    @JsonProperty("partner_type")
    public void setPartner_type(String  partner_type){
        this.partner_type = partner_type ;
        this.partner_typeDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴类型]脏标记
     */
    @JsonIgnore
    public boolean getPartner_typeDirtyFlag(){
        return this.partner_typeDirtyFlag ;
    }

    /**
     * 获取 [备忘]
     */
    @JsonProperty("communication")
    public String getCommunication(){
        return this.communication ;
    }

    /**
     * 设置 [备忘]
     */
    @JsonProperty("communication")
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.communicationDirtyFlag = true ;
    }

    /**
     * 获取 [备忘]脏标记
     */
    @JsonIgnore
    public boolean getCommunicationDirtyFlag(){
        return this.communicationDirtyFlag ;
    }

    /**
     * 获取 [付款差异]
     */
    @JsonProperty("payment_difference")
    public Double getPayment_difference(){
        return this.payment_difference ;
    }

    /**
     * 设置 [付款差异]
     */
    @JsonProperty("payment_difference")
    public void setPayment_difference(Double  payment_difference){
        this.payment_difference = payment_difference ;
        this.payment_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [付款差异]脏标记
     */
    @JsonIgnore
    public boolean getPayment_differenceDirtyFlag(){
        return this.payment_differenceDirtyFlag ;
    }

    /**
     * 获取 [显示合作伙伴银行账户]
     */
    @JsonProperty("show_partner_bank_account")
    public String getShow_partner_bank_account(){
        return this.show_partner_bank_account ;
    }

    /**
     * 设置 [显示合作伙伴银行账户]
     */
    @JsonProperty("show_partner_bank_account")
    public void setShow_partner_bank_account(String  show_partner_bank_account){
        this.show_partner_bank_account = show_partner_bank_account ;
        this.show_partner_bank_accountDirtyFlag = true ;
    }

    /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    @JsonIgnore
    public boolean getShow_partner_bank_accountDirtyFlag(){
        return this.show_partner_bank_accountDirtyFlag ;
    }

    /**
     * 获取 [发票分组]
     */
    @JsonProperty("group_invoices")
    public String getGroup_invoices(){
        return this.group_invoices ;
    }

    /**
     * 设置 [发票分组]
     */
    @JsonProperty("group_invoices")
    public void setGroup_invoices(String  group_invoices){
        this.group_invoices = group_invoices ;
        this.group_invoicesDirtyFlag = true ;
    }

    /**
     * 获取 [发票分组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_invoicesDirtyFlag(){
        return this.group_invoicesDirtyFlag ;
    }

    /**
     * 获取 [隐藏付款方式]
     */
    @JsonProperty("hide_payment_method")
    public String getHide_payment_method(){
        return this.hide_payment_method ;
    }

    /**
     * 设置 [隐藏付款方式]
     */
    @JsonProperty("hide_payment_method")
    public void setHide_payment_method(String  hide_payment_method){
        this.hide_payment_method = hide_payment_method ;
        this.hide_payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [隐藏付款方式]脏标记
     */
    @JsonIgnore
    public boolean getHide_payment_methodDirtyFlag(){
        return this.hide_payment_methodDirtyFlag ;
    }

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [付款日期]
     */
    @JsonProperty("payment_date")
    public Timestamp getPayment_date(){
        return this.payment_date ;
    }

    /**
     * 设置 [付款日期]
     */
    @JsonProperty("payment_date")
    public void setPayment_date(Timestamp  payment_date){
        this.payment_date = payment_date ;
        this.payment_dateDirtyFlag = true ;
    }

    /**
     * 获取 [付款日期]脏标记
     */
    @JsonIgnore
    public boolean getPayment_dateDirtyFlag(){
        return this.payment_dateDirtyFlag ;
    }

    /**
     * 获取 [日记账项目标签]
     */
    @JsonProperty("writeoff_label")
    public String getWriteoff_label(){
        return this.writeoff_label ;
    }

    /**
     * 设置 [日记账项目标签]
     */
    @JsonProperty("writeoff_label")
    public void setWriteoff_label(String  writeoff_label){
        this.writeoff_label = writeoff_label ;
        this.writeoff_labelDirtyFlag = true ;
    }

    /**
     * 获取 [日记账项目标签]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_labelDirtyFlag(){
        return this.writeoff_labelDirtyFlag ;
    }

    /**
     * 获取 [多]
     */
    @JsonProperty("multi")
    public String getMulti(){
        return this.multi ;
    }

    /**
     * 设置 [多]
     */
    @JsonProperty("multi")
    public void setMulti(String  multi){
        this.multi = multi ;
        this.multiDirtyFlag = true ;
    }

    /**
     * 获取 [多]脏标记
     */
    @JsonIgnore
    public boolean getMultiDirtyFlag(){
        return this.multiDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [付款差异处理]
     */
    @JsonProperty("payment_difference_handling")
    public String getPayment_difference_handling(){
        return this.payment_difference_handling ;
    }

    /**
     * 设置 [付款差异处理]
     */
    @JsonProperty("payment_difference_handling")
    public void setPayment_difference_handling(String  payment_difference_handling){
        this.payment_difference_handling = payment_difference_handling ;
        this.payment_difference_handlingDirtyFlag = true ;
    }

    /**
     * 获取 [付款差异处理]脏标记
     */
    @JsonIgnore
    public boolean getPayment_difference_handlingDirtyFlag(){
        return this.payment_difference_handlingDirtyFlag ;
    }

    /**
     * 获取 [付款金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [付款金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [付款金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [代码]
     */
    @JsonProperty("payment_method_code")
    public String getPayment_method_code(){
        return this.payment_method_code ;
    }

    /**
     * 设置 [代码]
     */
    @JsonProperty("payment_method_code")
    public void setPayment_method_code(String  payment_method_code){
        this.payment_method_code = payment_method_code ;
        this.payment_method_codeDirtyFlag = true ;
    }

    /**
     * 获取 [代码]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_codeDirtyFlag(){
        return this.payment_method_codeDirtyFlag ;
    }

    /**
     * 获取 [付款方法类型]
     */
    @JsonProperty("payment_method_id_text")
    public String getPayment_method_id_text(){
        return this.payment_method_id_text ;
    }

    /**
     * 设置 [付款方法类型]
     */
    @JsonProperty("payment_method_id_text")
    public void setPayment_method_id_text(String  payment_method_id_text){
        this.payment_method_id_text = payment_method_id_text ;
        this.payment_method_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [付款方法类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_id_textDirtyFlag(){
        return this.payment_method_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [差异科目]
     */
    @JsonProperty("writeoff_account_id_text")
    public String getWriteoff_account_id_text(){
        return this.writeoff_account_id_text ;
    }

    /**
     * 设置 [差异科目]
     */
    @JsonProperty("writeoff_account_id_text")
    public void setWriteoff_account_id_text(String  writeoff_account_id_text){
        this.writeoff_account_id_text = writeoff_account_id_text ;
        this.writeoff_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [差异科目]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_id_textDirtyFlag(){
        return this.writeoff_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [差异科目]
     */
    @JsonProperty("writeoff_account_id")
    public Integer getWriteoff_account_id(){
        return this.writeoff_account_id ;
    }

    /**
     * 设置 [差异科目]
     */
    @JsonProperty("writeoff_account_id")
    public void setWriteoff_account_id(Integer  writeoff_account_id){
        this.writeoff_account_id = writeoff_account_id ;
        this.writeoff_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [差异科目]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_idDirtyFlag(){
        return this.writeoff_account_idDirtyFlag ;
    }

    /**
     * 获取 [收款银行账号]
     */
    @JsonProperty("partner_bank_account_id")
    public Integer getPartner_bank_account_id(){
        return this.partner_bank_account_id ;
    }

    /**
     * 设置 [收款银行账号]
     */
    @JsonProperty("partner_bank_account_id")
    public void setPartner_bank_account_id(Integer  partner_bank_account_id){
        this.partner_bank_account_id = partner_bank_account_id ;
        this.partner_bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [收款银行账号]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_account_idDirtyFlag(){
        return this.partner_bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }

    /**
     * 获取 [付款方法类型]
     */
    @JsonProperty("payment_method_id")
    public Integer getPayment_method_id(){
        return this.payment_method_id ;
    }

    /**
     * 设置 [付款方法类型]
     */
    @JsonProperty("payment_method_id")
    public void setPayment_method_id(Integer  payment_method_id){
        this.payment_method_id = payment_method_id ;
        this.payment_method_idDirtyFlag = true ;
    }

    /**
     * 获取 [付款方法类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_idDirtyFlag(){
        return this.payment_method_idDirtyFlag ;
    }



}
