package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_stageService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_stageController {
	@Autowired
    Crm_stageService crm_stageservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages")
    @PreAuthorize("@crm_stage_pms.check('CREATE')")
    public ResponseEntity<Crm_stage> create(@RequestBody Crm_stage crm_stage) {
        Crm_stage crm_stage2 = crm_stageservice.create(crm_stage);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stage2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/{crm_stage_id}/updatebatch")
    @PreAuthorize("@crm_stage_pms.check(#crm_stage_id,'UPDATE')")
    public ResponseEntity<Crm_stage> updateBatch(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) {
        Crm_stage crm_stage2 = crm_stageservice.updateBatch(crm_stage_id, crm_stage);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stage2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_stages/getdraft")
    @PreAuthorize("@crm_stage_pms.check('CREATE')")
    public ResponseEntity<Crm_stage> getDraft() {
        //Crm_stage crm_stage = crm_stageservice.getDraft( crm_stage_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_stage());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/{crm_stage_id}")
    @PreAuthorize("@crm_stage_pms.check(#crm_stage_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_stage_id") Integer crm_stage_id) {
        boolean b = crm_stageservice.remove( crm_stage_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/{crm_stage_id}")
    @PreAuthorize("@crm_stage_pms.check(#crm_stage_id,'UPDATE')")
    public ResponseEntity<Crm_stage> update(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) {
        Crm_stage crm_stage2 = crm_stageservice.update(crm_stage_id, crm_stage);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stage2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_stages/{crm_stage_id}")
    @PreAuthorize("@crm_stage_pms.check(#crm_stage_id,'UPDATE')")
    public ResponseEntity<Crm_stage> api_update(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) {
        Crm_stage crm_stage2 = crm_stageservice.update(crm_stage_id, crm_stage);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stage2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_stages/{crm_stage_id}")
    @PreAuthorize("@crm_stage_pms.check(#crm_stage_id,'READ')")
    public ResponseEntity<Crm_stage> get(@PathVariable("crm_stage_id") Integer crm_stage_id) {
        Crm_stage crm_stage = crm_stageservice.get( crm_stage_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stage);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages/{crm_stage_id}/createbatch")
    @PreAuthorize("@crm_stage_pms.check(#crm_stage_id,'CREATE')")
    public ResponseEntity<Crm_stage> createBatch(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) {
        Crm_stage crm_stage2 = crm_stageservice.createBatch(crm_stage_id, crm_stage);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stage2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/{crm_stage_id}/removebatch")
    @PreAuthorize("@crm_stage_pms.check(#crm_stage_id,'DELETE')")
    public ResponseEntity<Crm_stage> removeBatch(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) {
        Crm_stage crm_stage2 = crm_stageservice.removeBatch(crm_stage_id, crm_stage);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stage2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_stages/fetchdefault")
    @PreAuthorize("@crm_stage_pms.check('READ')")
	public ResponseEntity<List<Crm_stage>> fetchDefault(Crm_stageSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_stage> page = crm_stageservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
