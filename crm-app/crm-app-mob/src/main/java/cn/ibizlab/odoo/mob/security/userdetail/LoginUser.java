package cn.ibizlab.odoo.mob.security.userdetail;

import lombok.extern.slf4j.Slf4j;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Slf4j
public class LoginUser extends org.springframework.security.core.userdetails.User {

    private String userId ;
    private String orgUserId ;
    private String orgId ;
    private String orgName ;
    private String orgDeptId ;
    private String orgDeptName ;
    private boolean isSuperUser ;

    public LoginUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

}
