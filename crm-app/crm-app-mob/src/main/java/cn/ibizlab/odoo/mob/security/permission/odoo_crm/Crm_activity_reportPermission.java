package cn.ibizlab.odoo.mob.security.permission.odoo_crm;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_activity_reportService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("crm_activity_report_pms")
public class Crm_activity_reportPermission {

    @Autowired
    Crm_activity_reportService crm_activity_reportservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer crm_activity_report_id, String action){
        return true ;
    }


}
