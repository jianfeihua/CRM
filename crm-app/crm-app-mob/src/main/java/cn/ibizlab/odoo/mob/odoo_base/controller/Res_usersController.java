package cn.ibizlab.odoo.mob.odoo_base.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_base.service.Res_usersService;
import cn.ibizlab.odoo.mob.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.mob.odoo_base.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Res_usersController {
	@Autowired
    Res_usersService res_usersservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'UPDATE')")
    public ResponseEntity<Res_users> update(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.update(res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'UPDATE')")
    public ResponseEntity<Res_users> api_update(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.update(res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/{res_users_id}/save")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'')")
    public ResponseEntity<Res_users> save(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.save(res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_users")
    @PreAuthorize("@res_users_pms.check('CREATE')")
    public ResponseEntity<Res_users> create(@RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.create(res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/checkkey")
    @PreAuthorize("@res_users_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_users res_users) {
        boolean b = res_usersservice.checkKey(res_users);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'READ')")
    public ResponseEntity<Res_users> get(@PathVariable("res_users_id") Integer res_users_id) {
        Res_users res_users = res_usersservice.get( res_users_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_users);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/{res_users_id}/updatebatch")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'UPDATE')")
    public ResponseEntity<Res_users> updateBatch(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.updateBatch(res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/{res_users_id}/createbatch")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'CREATE')")
    public ResponseEntity<Res_users> createBatch(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.createBatch(res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/{res_users_id}/removebatch")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'DELETE')")
    public ResponseEntity<Res_users> removeBatch(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.removeBatch(res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.check(#res_users_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("res_users_id") Integer res_users_id) {
        boolean b = res_usersservice.remove( res_users_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_users/getdraft")
    @PreAuthorize("@res_users_pms.check('CREATE')")
    public ResponseEntity<Res_users> getDraft() {
        //Res_users res_users = res_usersservice.getDraft( res_users_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Res_users());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_users/fetchdefault")
    @PreAuthorize("@res_users_pms.check('READ')")
	public ResponseEntity<List<Res_users>> fetchDefault(Res_usersSearchContext searchContext,Pageable pageable) {
        
        Page<Res_users> page = res_usersservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'UPDATE')")
    public ResponseEntity<Res_users> updateByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.updateByCrm_team(crm_team_id, res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/api/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'UPDATE')")
    public ResponseEntity<Res_users> api_updateByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.updateByCrm_team(crm_team_id, res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}/save")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'')")
    public ResponseEntity<Res_users> saveByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.saveByCrm_team(crm_team_id, res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,'CREATE')")
    public ResponseEntity<Res_users> createByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.createByCrm_team(crm_team_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users/checkkey")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,'')")
    public ResponseEntity<Boolean> checkKeyByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_users res_users) {
        boolean b = res_usersservice.checkKeyByCrm_team(crm_team_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'READ')")
    public ResponseEntity<Res_users> getByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) {
        Res_users res_users = res_usersservice.getByCrm_team(crm_team_id,  res_users_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_users);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}/updatebatch")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'UPDATE')")
    public ResponseEntity<Res_users> updateBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.updateBatchByCrm_team(crm_team_id, res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}/createbatch")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'CREATE')")
    public ResponseEntity<Res_users> createBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.createBatchByCrm_team(crm_team_id, res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}/removebatch")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'DELETE')")
    public ResponseEntity<Res_users> removeBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) {
        Res_users res_users2 = res_usersservice.removeBatchByCrm_team(crm_team_id, res_users_id, res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_users2);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,#res_users_id,'DELETE')")
    public ResponseEntity<Boolean> removeByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) {
        boolean b = res_usersservice.removeByCrm_team(crm_team_id,  res_users_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{crm_team_id}/res_users/getdraft")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,'CREATE')")
    public ResponseEntity<Res_users> getDraftByCrm_team() {
        //Res_users res_users = res_usersservice.getDraftByCrm_team(crm_team_id,  res_users_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Res_users());
    }

    @RequestMapping(method= RequestMethod.GET , value="/crm_teams/{crm_team_id}/res_users/fetchdefault")
    @PreAuthorize("@res_users_pms.checkByCrm_team(#crm_team_id,'READ')")
	public ResponseEntity<List<Res_users>> fetchDefaultByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id,Res_usersSearchContext searchContext,Pageable pageable) {
        searchContext.setN_sale_team_id_eq(crm_team_id);
        Page<Res_users> page = res_usersservice.fetchDefaultByCrm_team(crm_team_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
