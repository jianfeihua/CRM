package cn.ibizlab.odoo.mob.security.permission.odoo_account;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_account.service.Account_invoice_lineService;
import cn.ibizlab.odoo.mob.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("account_invoice_line_pms")
public class Account_invoice_linePermission {

    @Autowired
    Account_invoice_lineService account_invoice_lineservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer account_invoice_line_id, String action){
        return true ;
    }


}
