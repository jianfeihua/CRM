package cn.ibizlab.odoo.mob.odoo_base.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.mob.odoo_base.filter.*;


public interface Res_usersFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_users/{res_users_id}")
    public Res_users update(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users/{res_users_id}/save")
    public Res_users save(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users")
    public Res_users create(@RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users/checkkey")
    public Boolean checkKey(@RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_users/{res_users_id}")
    public Res_users get(@PathVariable("res_users_id") Integer res_users_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_users/{res_users_id}/updatebatch")
    public Res_users updateBatch(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users/{res_users_id}/createbatch")
    public Res_users createBatch(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_users/{res_users_id}/removebatch")
    public Res_users removeBatch(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_users/{res_users_id}")
    public Boolean remove(@PathVariable("res_users_id") Integer res_users_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_users/{res_users_id}/getdraft")
    public Res_users getDraft(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_users/fetchdefault")
	public Page<Res_users> fetchDefault(Res_usersSearchContext searchContext) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    public Res_users updateByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}/save")
    public Res_users saveByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users")
    public Res_users createByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users/checkkey")
    public Boolean checkKeyByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    public Res_users getByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}/updatebatch")
    public Res_users updateBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}/createbatch")
    public Res_users createBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}/removebatch")
    public Res_users removeBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    public Boolean removeByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}/getdraft")
    public Res_users getDraftByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_users res_users) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_teams/{crm_team_id}/res_users/fetchdefault")
	public Page<Res_users> fetchDefaultByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id , Res_usersSearchContext searchContext) ;
}
