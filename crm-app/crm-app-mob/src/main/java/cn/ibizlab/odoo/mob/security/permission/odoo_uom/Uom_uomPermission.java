package cn.ibizlab.odoo.mob.security.permission.odoo_uom;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_uom.service.Uom_uomService;
import cn.ibizlab.odoo.mob.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("uom_uom_pms")
public class Uom_uomPermission {

    @Autowired
    Uom_uomService uom_uomservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer uom_uom_id, String action){
        return true ;
    }


}
