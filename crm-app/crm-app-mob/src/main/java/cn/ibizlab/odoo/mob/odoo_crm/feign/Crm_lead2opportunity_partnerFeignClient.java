package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_lead2opportunity_partnerFeignClient {



	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead2opportunity_partners")
    public Crm_lead2opportunity_partner create(@RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public Boolean remove(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public Crm_lead2opportunity_partner get(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public Crm_lead2opportunity_partner update(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}/removebatch")
    public Crm_lead2opportunity_partner removeBatch(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}/createbatch")
    public Crm_lead2opportunity_partner createBatch(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}/updatebatch")
    public Crm_lead2opportunity_partner updateBatch(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}/getdraft")
    public Crm_lead2opportunity_partner getDraft(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lead2opportunity_partners/fetchdefault")
	public Page<Crm_lead2opportunity_partner> fetchDefault(Crm_lead2opportunity_partnerSearchContext searchContext) ;
}
