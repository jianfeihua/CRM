package cn.ibizlab.odoo.mob.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.mob.odoo_crm.feign.Crm_lost_reasonFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_lost_reasonService {

    Crm_lost_reasonFeignClient client;

    @Autowired
    public Crm_lost_reasonService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_lost_reasonFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_lost_reasonFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_lost_reason get( Integer crm_lost_reason_id) {
        return client.get( crm_lost_reason_id);
    }

    public boolean remove( Integer crm_lost_reason_id) {
        return client.remove( crm_lost_reason_id);
    }

    public Crm_lost_reason update(Integer crm_lost_reason_id, Crm_lost_reason crm_lost_reason) {
        return client.update(crm_lost_reason_id, crm_lost_reason);
    }

    public Crm_lost_reason removeBatch(Integer crm_lost_reason_id, Crm_lost_reason crm_lost_reason) {
        return client.removeBatch(crm_lost_reason_id, crm_lost_reason);
    }

    public Crm_lost_reason updateBatch(Integer crm_lost_reason_id, Crm_lost_reason crm_lost_reason) {
        return client.updateBatch(crm_lost_reason_id, crm_lost_reason);
    }

	public Crm_lost_reason create(Crm_lost_reason crm_lost_reason) {
        return client.create(crm_lost_reason);
    }

    public Crm_lost_reason getDraft(Integer crm_lost_reason_id, Crm_lost_reason crm_lost_reason) {
        return client.getDraft(crm_lost_reason_id, crm_lost_reason);
    }

    public Crm_lost_reason createBatch(Integer crm_lost_reason_id, Crm_lost_reason crm_lost_reason) {
        return client.createBatch(crm_lost_reason_id, crm_lost_reason);
    }

	public Page<Crm_lost_reason> fetchDefault(Crm_lost_reasonSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
