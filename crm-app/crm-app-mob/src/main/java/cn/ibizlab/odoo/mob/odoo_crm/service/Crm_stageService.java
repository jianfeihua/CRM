package cn.ibizlab.odoo.mob.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.mob.odoo_crm.feign.Crm_stageFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_stageService {

    Crm_stageFeignClient client;

    @Autowired
    public Crm_stageService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_stageFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_stageFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

	public Crm_stage create(Crm_stage crm_stage) {
        return client.create(crm_stage);
    }

    public Crm_stage updateBatch(Integer crm_stage_id, Crm_stage crm_stage) {
        return client.updateBatch(crm_stage_id, crm_stage);
    }

    public Crm_stage getDraft(Integer crm_stage_id, Crm_stage crm_stage) {
        return client.getDraft(crm_stage_id, crm_stage);
    }

    public boolean remove( Integer crm_stage_id) {
        return client.remove( crm_stage_id);
    }

    public Crm_stage update(Integer crm_stage_id, Crm_stage crm_stage) {
        return client.update(crm_stage_id, crm_stage);
    }

    public Crm_stage get( Integer crm_stage_id) {
        return client.get( crm_stage_id);
    }

    public Crm_stage createBatch(Integer crm_stage_id, Crm_stage crm_stage) {
        return client.createBatch(crm_stage_id, crm_stage);
    }

    public Crm_stage removeBatch(Integer crm_stage_id, Crm_stage crm_stage) {
        return client.removeBatch(crm_stage_id, crm_stage);
    }

	public Page<Crm_stage> fetchDefault(Crm_stageSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
