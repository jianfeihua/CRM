package cn.ibizlab.odoo.mob.odoo_uom.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.mob.odoo_uom.filter.*;
import cn.ibizlab.odoo.mob.odoo_uom.feign.Uom_uomFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Uom_uomService {

    Uom_uomFeignClient client;

    @Autowired
    public Uom_uomService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Uom_uomFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Uom_uomFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean remove( Integer uom_uom_id) {
        return client.remove( uom_uom_id);
    }

    public Uom_uom getDraft(Integer uom_uom_id, Uom_uom uom_uom) {
        return client.getDraft(uom_uom_id, uom_uom);
    }

    public Uom_uom update(Integer uom_uom_id, Uom_uom uom_uom) {
        return client.update(uom_uom_id, uom_uom);
    }

    public Uom_uom createBatch(Integer uom_uom_id, Uom_uom uom_uom) {
        return client.createBatch(uom_uom_id, uom_uom);
    }

    public Uom_uom get( Integer uom_uom_id) {
        return client.get( uom_uom_id);
    }

    public Uom_uom updateBatch(Integer uom_uom_id, Uom_uom uom_uom) {
        return client.updateBatch(uom_uom_id, uom_uom);
    }

    public Uom_uom removeBatch(Integer uom_uom_id, Uom_uom uom_uom) {
        return client.removeBatch(uom_uom_id, uom_uom);
    }

	public Uom_uom create(Uom_uom uom_uom) {
        return client.create(uom_uom);
    }

	public Page<Uom_uom> fetchDefault(Uom_uomSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
