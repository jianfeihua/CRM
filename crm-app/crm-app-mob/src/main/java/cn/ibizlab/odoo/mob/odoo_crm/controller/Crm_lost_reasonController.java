package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_lost_reasonService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_lost_reasonController {
	@Autowired
    Crm_lost_reasonService crm_lost_reasonservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/{crm_lost_reason_id}")
    @PreAuthorize("@crm_lost_reason_pms.check(#crm_lost_reason_id,'READ')")
    public ResponseEntity<Crm_lost_reason> get(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) {
        Crm_lost_reason crm_lost_reason = crm_lost_reasonservice.get( crm_lost_reason_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reason);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/{crm_lost_reason_id}")
    @PreAuthorize("@crm_lost_reason_pms.check(#crm_lost_reason_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) {
        boolean b = crm_lost_reasonservice.remove( crm_lost_reason_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/{crm_lost_reason_id}")
    @PreAuthorize("@crm_lost_reason_pms.check(#crm_lost_reason_id,'UPDATE')")
    public ResponseEntity<Crm_lost_reason> update(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) {
        Crm_lost_reason crm_lost_reason2 = crm_lost_reasonservice.update(crm_lost_reason_id, crm_lost_reason);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reason2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_lost_reasons/{crm_lost_reason_id}")
    @PreAuthorize("@crm_lost_reason_pms.check(#crm_lost_reason_id,'UPDATE')")
    public ResponseEntity<Crm_lost_reason> api_update(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) {
        Crm_lost_reason crm_lost_reason2 = crm_lost_reasonservice.update(crm_lost_reason_id, crm_lost_reason);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reason2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/{crm_lost_reason_id}/removebatch")
    @PreAuthorize("@crm_lost_reason_pms.check(#crm_lost_reason_id,'DELETE')")
    public ResponseEntity<Crm_lost_reason> removeBatch(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) {
        Crm_lost_reason crm_lost_reason2 = crm_lost_reasonservice.removeBatch(crm_lost_reason_id, crm_lost_reason);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reason2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/{crm_lost_reason_id}/updatebatch")
    @PreAuthorize("@crm_lost_reason_pms.check(#crm_lost_reason_id,'UPDATE')")
    public ResponseEntity<Crm_lost_reason> updateBatch(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) {
        Crm_lost_reason crm_lost_reason2 = crm_lost_reasonservice.updateBatch(crm_lost_reason_id, crm_lost_reason);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reason2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons")
    @PreAuthorize("@crm_lost_reason_pms.check('CREATE')")
    public ResponseEntity<Crm_lost_reason> create(@RequestBody Crm_lost_reason crm_lost_reason) {
        Crm_lost_reason crm_lost_reason2 = crm_lost_reasonservice.create(crm_lost_reason);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reason2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/getdraft")
    @PreAuthorize("@crm_lost_reason_pms.check('CREATE')")
    public ResponseEntity<Crm_lost_reason> getDraft() {
        //Crm_lost_reason crm_lost_reason = crm_lost_reasonservice.getDraft( crm_lost_reason_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_lost_reason());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/{crm_lost_reason_id}/createbatch")
    @PreAuthorize("@crm_lost_reason_pms.check(#crm_lost_reason_id,'CREATE')")
    public ResponseEntity<Crm_lost_reason> createBatch(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) {
        Crm_lost_reason crm_lost_reason2 = crm_lost_reasonservice.createBatch(crm_lost_reason_id, crm_lost_reason);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reason2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_lost_reasons/fetchdefault")
    @PreAuthorize("@crm_lost_reason_pms.check('READ')")
	public ResponseEntity<List<Crm_lost_reason>> fetchDefault(Crm_lost_reasonSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_lost_reason> page = crm_lost_reasonservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
